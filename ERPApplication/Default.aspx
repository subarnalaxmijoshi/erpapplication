﻿<%@ Page Title="Home Page" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeBehind="Default.aspx.vb" Inherits="School._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="/assets/plugins/jquery.flip.js" type="text/javascript"></script>
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
           
            var flip_options = { axis: 'x', trigger: 'click' };
            $('.flip_card').each(function () {
                $(this).flip(flip_options);
            });

            $('#so_options').on('shown.bs.collapse', function () {
                $('a[href="#so_options"] i').removeClass('fa-angle-down').addClass('fa-angle-up');
            });
            $('#so_options').on('hidden.bs.collapse', function () {
                $('a[href="#so_options"] i').removeClass('fa-angle-up').addClass('fa-angle-down');
            });

            $('#so_options input').attr('checked', true);
            $('#so_options input').click(function () {
                if ($(this).is(':checked')) {
                    $($(this).attr('data-target')).removeClass('hidden');
                } else {
                    $($(this).attr('data-target')).addClass('hidden');
                }
            });

        });
    </script>
    <style type="text/css">
        .ss
        {
            font-size: 20px;
        }
        .Dashbottom
        {
            padding-top: 28px;
            text-align: center;
        }
        .DashbottomBig
        {
            font-size: 60px;
            text-align: center;
        }
    </style>
    <%--<script type="text/javascript">
    var checkFromValidation = false;

    $(document).ready(function () {
        $('#form').validate();
        $("#<%=txtOldPassword.ClientID %>").rules('add', { required: true, messages: { required: 'Old password is required.'} });
        $("#<%=txtNewPassword.ClientID %>").rules('add', { required: true, messages: { required: 'New password is required.'} });
        $("#<%=txtConfirmPassword.ClientID %>").rules('add', { equalTo: "#txtNewPassword", messages: { equalTo: 'Confirm Password and New Password do not match.'} });

        checkFromValidation = function () {
            var bool = true;
            if ($('#<%=txtOldPassword.ClientID %>').valid() == false) bool = false;
            if ($('#<%=txtNewPassword.ClientID %>').valid() == false) bool = false;
            if ($('#<%=txtConfirmPassword.ClientID %>').valid() == false) bool = false;
            if (!bool) $('#form').validate().focusInvalid();
            return bool;
        };
    });
    </script> 
    <script type="text/javascript">
       
        
        $(document).ready(function () {
           
            var sessionUserid = <%= getSessionUserID() %>;
            $("#ChangePassword").click(function () {
            
            if(confirm("Are you sure to change your password?")==true){
                
                var oldpwd = $('#<%=txtOldPassword.ClientID %>').val();
                var newpwd = $('#<%=txtNewPassword.ClientID %>').val();
                var confirmpwd = $('#<%=txtConfirmPassword.ClientID %>').val();

                if (newpwd !== confirmpwd || newpwd === null || newpwd === '' || confirmpwd === ''|| confirmpwd === null) {
                    
                     alert("Confirm Password and New Password do not match or password is empty!");
                } 
                else {
                 $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/ChangePassword',
                        data: '{"userId" : "' + sessionUserid + '","oldpassword" : "' + oldpwd + '","newpassword" : "' + newpwd + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            alert(data);
                            $('#<%=txtOldPassword.ClientID %>').val('');
                            $('#<%=txtNewPassword.ClientID %>').val('');
                            $('#<%=txtConfirmPassword.ClientID %>').val('');
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                   
                }
               }

            });
        });
    </script>--%>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="page-content">
        <%--<div class="modal fade" id="portlet-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Old Password :</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtOldPassword" runat="server" CssClass="form-control" placeholder="Enter your old password" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    New Password :</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtNewPassword" runat="server" CssClass="form-control" placeholder="Enter your new password" TextMode="Password"
                                    ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Confirm Password :</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" placeholder="Confirm your new password" TextMode="Password" 
                                    ></asp:TextBox>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue" id="ChangePassword" onclick="return checkFromValidation();">
                            Change Password</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>--%>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Dashboard <small>Find the info you need. It's quick, easy and Interative</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group"><a class="btn blue" data-toggle="collapse" data-parent="#screen_options"
                        href="#so_options"><span>Screen Options</span> <i class="fa fa-angle-down"></i></a>
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Reports</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Student</a></li>
                            <li><a href="#">HR</a></li>
                            <li><a href="#">Account</a></li>
                            <li><a href="#">Settings</a></li>
                            <li><a href="#">Setup</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Advanced Reports</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> </li>
                </ul>
                <div id="screen_options" class="panel-group">
                    <div class="panel so_panel">
                        <div style="" id="so_options" class="panel-collapse collapse">
                            <div class="panel-body">
                                <label>
                                    <input type="checkbox" data-target="#box_student" />
                                    Administration</label>
                                <label>
                                    <input type="checkbox" data-target="#box_weather" />
                                    Exam</label>
                                <label>
                                    <input type="checkbox" data-target="#box_driver" />
                                    Account</label>
                                <label>
                                    <input type="checkbox" data-target="#box_passenger" />
                                    HR</label>
                                <label>
                                    <input type="checkbox" data-target="#box_result" />
                                    Management</label>
                                <label>
                                    <input type="checkbox" data-target="#box_error" />
                                    Time Table</label>
                                <label>
                                    <input type="checkbox" data-target="#box_transaction" />
                                    Payroll</label>
                                <label>
                                    <input type="checkbox" data-target="#box_driver_info" />
                                    Exam Control</label>
                                <label>
                                    <input type="checkbox" data-target="#box_passenger_info" />Settings</label>
                                <label>
                                    <input type="checkbox" data-target="#box_settings" />
                                    Setup</label>
                                <label>
                                    <input type="checkbox" data-target="#box_credit" />
                                    Library</label>
                                <label>
                                    <input type="checkbox" data-target="#box_referral" />
                                    Hostal</label>
                                <label>
                                    <input type="checkbox" data-target="#box_pencil" />
                                    Attendance</label>
                                <label>
                                    <input type="checkbox" data-target="#box_virtual_class" />
                                    Virtual Class</label>
                                <label>
                                    <input type="checkbox" data-target="#box_frontdesk" />
                                    Front Desk</label>
                                <label>
                                    <input type="checkbox" data-target="#box_messaging" />
                                    Messaging</label>
                                <label>
                                    <input type="checkbox" data-target="#box_documentmanagement" />
                                    Attendance</label>
                            </div>
                            <%--                          <div class="panel-title">
                            <a class="" data-toggle="collapse" data-parent="#screen_options" href="#so_options">
                                Screen Options <i class="fa fa-caret-down"></i>
                            </a>
                          </div>--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row dashboard_boxes">
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_student">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/Student/Studentmenu.aspx"><i class="fa fa-user"></i>Administration </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/students.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <div class="DashbottomBig">
                                        <i class=""><span id="totalstudent" runat="Server" class="cs"></span></i>
                                    </div>
                                    <div class="Dashbottom">
                                        <span id="male" runat="Server" class="ss fa fa-male"></span>&nbsp;&nbsp; <span id="female"
                                            runat="Server" class="ss fa fa-female"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_weather">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/Exam/Exammenu.aspx"><i class="fa fa-file-o"></i>Exam </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/exam.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Total : 223 </i><i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_driver">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/Accounts/Accountsmenu.aspx"><i class="fa fa-book"></i>Accounts </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/accounts.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Last Bill : <span id="LastMonth" runat="Server" class="cs"></span>
                                    </i>
                                    <%--<i class="fa fa-calendar">September: 122 </i>
                                <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_passenger">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/HR/EmployeeDetailsMenu.aspx"><i class="fa fa-user"></i>HR </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img alt="HR" src="/assets/img/student51.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <div class="DashbottomBig">
                                        <i class=""><span id="totalemployee" runat="Server" class="cs"></span></i>
                                    </div>
                                    <div class="Dashbottom">
                                        <span id="emale" runat="Server" class="ss fa fa-male"></span>&nbsp;&nbsp; <span id="efemale"
                                            runat="Server" class="ss fa fa-female"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_result">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/Management/Managementmenu.aspx"><i class="fa fa-gears"></i>Management
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/Management.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available</i>
                                    <%-- <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_error">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/TimeTable/TimeTablemenu.aspx"><i class="fa fa-calendar"></i>Time Table
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/timetable.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Holiday : <span id="holiday" runat="Server" class="cs"></span>
                                    </i>
                                    <%--<i class="fa fa-calendar">September: 122 </i>
                                <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_transaction">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="#"><i class="fa fa-credit-card"></i>Payroll </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/payroll.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Last Payment on <span id="paymentmonth" runat="Server" class="cs">
                                    </span></i>
                                    <%--<i class="fa fa-calendar">September: 122 </i>
                                <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_driver_info">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="#"><i class="fa fa-file"></i>Exam Control </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="assets/img/online2.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort"><span id="examname" runat="Server" class="cs"></span></i><i
                                        class="fa fa-calendar">Start: <span id="start" runat="Server" class="cs"></span>
                                    </i><i class="fa fa-calendar">End : <span id="ending" runat="Server" class="cs"></span>
                                    </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_passenger_info">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/Setting/Settingmenu.aspx"><i class="fa fa-gears"></i>Settings </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/settings.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Total : 223 </i><i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_settings">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/Setup/Setupmenu.aspx"><i class="fa fa-gear"></i>Setup </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/setup.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available </i>
                                    <%--    <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_credit">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="#"><i class="fa fa-book"></i>Library </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/library.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Total Book : <span id="books" runat="Server" class="cs"></span>
                                    </i>
                                    <%--<i class="fa fa-calendar">September: 122 </i>
                                <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_referral">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="#"><i class="fa fa-home"></i>Hostel </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/hostel.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Total Student Assign : <span id="assignstudent" runat="Server"
                                        class="cs"></span></i>
                                    <%--<i class="fa fa-calendar">September: 122 </i>
                                <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_pencil">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/AttendanceManagement/AttendanceManagementMenu.aspx"><i class="fa fa-pencil">
                                </i>Attendance </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/teacher.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available </i>
                                    <%--  <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_administration">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/AttendanceManagement/AttendanceManagementMenu.aspx"><i class="fa fa-pencil">
                                </i>Administration</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/student51.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Total : 223 </i><i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_virtual_class">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/AttendanceManagement/AttendanceManagementMenu.aspx"><i class="fa fa-fa-exchange">
                                </i>Virtual Class</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/virtualClass.jpg" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available</i>
                                    <%-- <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_frontdesk">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/AttendanceManagement/AttendanceManagementMenu.aspx"><i class="fa fa-star">
                                </i>Front Desk</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/frontdesk.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available </i>
                                    <%--    <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_messaging">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/AttendanceManagement/AttendanceManagementMenu.aspx"><i class="fa fa-envolpe">
                                </i>Messaging</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/message.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available </i>
                                    <%--  <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" id="box_documentmanagement">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <a href="/AttendanceManagement/AttendanceManagementMenu.aspx"><i class="fa fa-list-group">
                                </i>Documents</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="flip_card">
                                <div class="front">
                                    <div class="portlet-image">
                                        <img src="/assets/img/document_management.png" />
                                    </div>
                                </div>
                                <div class="back">
                                    <i class="fa fa-sort">Not Available </i>
                                    <%--          <i class="fa fa-calendar">September: 122 </i>
                                    <i class="fa fa-dashboard">Today : 23 </i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
