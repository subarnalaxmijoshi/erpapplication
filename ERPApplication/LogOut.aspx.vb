﻿Public Class LogOut
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session.Abandon()
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Redirect("\Login.aspx")

    End Sub

End Class