﻿Imports System.ServiceModel
Imports System.Web.Services
Imports System.Runtime.Serialization
Imports System.ServiceModel.Web
Imports System.IO
Imports System.Net
Imports System.ServiceModel.Description
Imports System.Text
Imports System
Imports System.Collections.Generic
Imports System.ServiceModel.Activation
Imports System.Web.Script.Services

' NOTE: You can use the "Rename" command on the context menu to change the interface name "ISetupServices" in both code and config file together.
<ServiceContract()>
Public Interface ISetupServices
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SingleMarksEntryAssessment(ByVal obj As SingleMarksAssessment()) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDemandChatHistory(ByVal DemandID As String) As ChatHistory()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetLeaveChatHistory(ByVal LeaveRequestID As String) As ChatHistory()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetCategoriesForDemand() As CategoriesForDemand()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSubCategoriesForDemand(ByVal CategoriesID As String) As SubCategoriesForDemand()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DemandRequestDelete(ByVal DemandRequestID As String, ByVal flag As String, ByVal Status As String) As Message()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveDemandForm(ByVal Item_id As String, ByVal Quantity As String, ByVal User_Id As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CreatNewItem(ByVal Categori_id As String, ByVal SubCategori_Id As String, ByVal ItemName As String, ByVal Unit As String, ByVal Specification As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function LeaveRequestDelete(ByVal LeaveRequestID As String, ByVal flag As String, ByVal Status As String) As Message()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentBoardResults(ByVal StudentID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function EditSubjectRegd(ByVal value As String, ByVal StudentID As String, ByVal SubjectID As String, ByVal CourseID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetLeaveManager() As EmployeeByJobTitle()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DemandNotes(ByVal Demand_id As String, ByVal Notes As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ForwardDemandtoManager(ByVal Demand_ID As String, ByVal Transfer_to As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ForwardLeavetoManager(ByVal LeaveReq_id As String, ByVal Tranfer_To_id As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveLeaveNote(ByVal Leavereq_id As String, ByVal Notes As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ValidateInventoryItemCode(ByVal ItemCode As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delTableRow(ByVal SchemaName As String, ByVal tableName As String, ByVal ColumnID As String, ByVal ID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateAnyTable(ByVal SchemaName As String, ByVal tableName As String, ByVal ColumnName As String, ByVal ColumnID As String, ByVal ID As String, ByVal Value As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AdjustSinglePayrollInMonth(ByVal EmployeeId As String, ByVal MonthId As String, ByVal Type As String, ByVal BranchId As String, ByVal FiscalyearId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delPCExamMarks_New(ByVal ID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePCExamMarks_New(ByVal ID As String, ByVal Marks As String, ByVal columnsName As String) As String

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AdjustAddablesDeductibles(ByVal EmployeeId As String, ByVal Amount As String, ByVal MonthId As String, ByVal Type As String, ByVal TypeId As String, ByVal BranchId As String, ByVal UserId As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PostDaywiseIncomeVoucher(ByVal ReferenceNo As String, ByVal MonthID As String, ByVal DayID As String, ByVal VType As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CreateEmployeeLogin(ByVal employeeLogins As EmployeeLogins()) As EmployeeLogins()

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function StudentAttendanceSubjectwise(ByVal stdAtt As StudentAttendances()) As StudentAttendances

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStockDetails(ByVal keyValue As String, ByVal searchValue As String, ByVal header As String) As String

    <OperationContract()> _
     <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PostIncomeVoucher_ByProgram(ByVal ReferenceNo As String, ByVal MonthID As String, ByVal VType As String, ByVal ProgramID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
     <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateBillProgram(ByVal ReferenceNo As String, ByVal ProgramID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
      <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateEmployeeHierarchy(ByVal EmployeeID As String, ByVal Value As String) As String

    <OperationContract()> _
       <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, bodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AlphabeticalStudentSearch(ByVal obtainFirstLetter As String, ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePracticalEvaluation(ByVal ID As String, ByVal Marks As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PractialEvaluation(ByVal obj As PUSeminarMarks()) As PUSeminarMarks

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateTeacherEvaluation(ByVal ID As String, ByVal Marks As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function TeacerEvaluation(ByVal obj As PUSeminarMarks()) As PUSeminarMarks


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateSeminarObtainMarks(ByVal ID As String, ByVal Marks As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ExamSeminar(ByVal obj As PUSeminarMarks()) As PUSeminarMarks


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function FetchFromSetupVoucher_new(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ApprovedPayrollForThisMonth_new(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function empTransferOut(ByVal EmployeeID As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal UserID As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollProfileAllEmp(ByVal ColumnTitle As String, ByVal values As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollProfile(ByVal ProfileID As String, ByVal ColumnTitle As String, ByVal values As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateExamReg(ByVal value As String, ByVal SubjectID As String, ByVal StudentID As String, ByVal BatchID As String, ByVal SemesterID As String, ByVal ExamID As String, ByVal UserID As String, ByVal BranchID As String, ByVal AcademicYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SubjectwiseMarksEntry(ByVal obj As SubjectwiseMarks()) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateCampusOtherInfo(ByVal ColumnToModify As String, ByVal ValueToSet As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveSubjectAttendance(ByVal StudentID As String, ByVal SubjectID As String, ByVal UserID As String, ByVal subAtt As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PostIncomeVoucher(ByVal ReferenceNo As String, ByVal MonthID As String, ByVal VType As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateParticularAmount(ByVal CreditBillParticularID As String, ByVal Amount As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateChequeNoInTempJV(ByVal jvId As String, ByVal ChequeNo As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetChequePayee(ByVal SessionID As String, ByVal UserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ReconciliateCheques(ByVal ChequeNos As String, ByVal UserID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetChartData(ByVal BranchID As String, ByVal Flag As String, ByVal FiscalYearID As String) As Array

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ValidateMobileNo(ByVal MobileNo As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ValidateApplicationFormNo(ByVal ApplicationFormNo As String, ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PostSendSMS(ByVal [to] As String, ByVal text As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetMenusByModuleID(ByVal ModuleID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateMenuByModuleID(ByVal UserName As String, ByVal isActive As String, ByVal ModuleID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateMenuID(ByVal MenuID As String, ByVal UserID As String, ByVal isActive As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateAcademicYear(ByVal AcademicInfoID As String, ByVal AcademicYear As String, ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PostMergeIncomeVoucher(ByVal ReferenceNo As String, ByVal UserID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function InventryReturn(ByVal ReferenceNo As String, ByVal ItemID As String, ByVal AssigneeID As String, ByVal UserID As String) As String

    '    <OperationContract()> _
    '<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    '    Function PostDataFromAPI(ByVal AttendeeID As String, ByVal AttDateTime As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ChangeStaffLoginPassword(ByVal EmployeeId As String, ByVal oldpassword As String, ByVal newpassword As String) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ChangeStudentLoginPassword(ByVal StudentId As String, ByVal oldpassword As String, ByVal newpassword As String) As ErrorMessage

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CreateMiscAccount(ByVal AccountName As String, ByVal Amount As String, ByVal BranchID As String, ByVal UserID As String) As String

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentInfoDetails(ByVal studentID As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delete_MisAmount(ByVal assignId As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function edit_MiscAmount(ByVal assignId As String, ByVal val As String, ByVal sUserId As String) As String

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetMiscParticularAmount(ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AssignMiscAmount(ByVal AccountID As String, ByVal Amount As String, ByVal BranchID As String, ByVal UserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AsignSal(ByVal EmployeeID As String, ByVal MonthID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SetAttendance(ByVal EmployeeID As String, ByVal sUserId As String, ByVal MonthID As String, ByVal sFiscalID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ReviseDueAmount(ByVal FineDetailID As String, ByVal Amount As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SendDuesToAccount(ByVal MemberCode As String, ByVal AccessionNo As String, ByVal Amount As Decimal, ByVal UserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateProposalInfo(ByVal ProposalID As String, ByVal Title As String, ByVal RegdNo As String, ByVal RegdDate As String, ByVal LogInUser As String) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetProposalInfo(ByVal ProposalID As String) As ProposalInfo()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteVivaMarks(ByVal VivaExamRecordId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteVivaInfo(ByVal AssignedVivaListId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteInterviewMarks_Project(ByVal InterviewMarksId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteInterviewMarks(ByVal InterviewMarksId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveNewRegisteredProposal(ByVal ProposalTitle As String, ByVal StudentId As Integer, ByVal ProposalRegistrationNumber As String, ByVal LogInUser As String, ByVal RegistrationDate As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveNewRoaster(ByVal RoasterName As String, ByVal Expertise As String, ByVal MobileNumber As String, ByVal Email As String, ByVal Remarks As String, ByVal LogInUser As String) As String



    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AssignRoaster(ByVal ResearchHead As Integer,
                              ByVal RoasterId As Integer,
                              ByVal ProposalId As Integer, ByVal InterviewDate As String, ByVal LogInUser As String) As String




    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateVivaResult(ByVal MarksAwarded As String, ByVal CotForEdit As String, ByVal RorForEdit As String, ByVal LanguageForEdit As String, ByVal VivaExamRecordId As Integer, ByVal LogInUser As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function getVivaResultEntryById(ByVal VivaResultEntryId As Integer) As VivaResultSet()


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveVivaResult(ByVal MarksAwarded As String, ByVal CotReview As String, ByVal ReviewOfRelatedStudies As String, ByVal Language As String, ByVal ProposalID As String, ByVal LogInUser As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateInterviewResult(ByVal ConceptualAndTheoreticalReview As String,
                                   ByVal ReviewOfRelatedStudies As String,
                                   ByVal Language As String,
                                   ByVal UserID As String,
                                   ByVal InterviewResultEntryId As Integer) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function getInterviewResultById(ByVal InterviewResultEntryId As Integer) As ResultSet()


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveInterviewResult(ByVal ConceptualAndTheoreticalReview As String,
                              ByVal ReviewOfRelatedStudies As String,
                              ByVal Language As String,
                              ByVal UserID As String,
                              ByVal Result As String,
                              ByVal ProposalRegNumber As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function getVivaResultById(ByVal VivaResultEntryId As Integer) As VivaResultSet()


    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AssignVivaDate(ByVal headofresearchdepartment As String, ByVal thesisSupervisior As String, ByVal secondthesisSupervisior As String, ByVal externalExpert As String, ByVal vivaDate As String, ByVal ProposalId As Integer, ByVal LogInUser As String) As String



    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateVivaDate(ByVal HOR As String, ByVal FirstThSupervisior As String, ByVal SecondThSupervisior As String, ByVal ExternalExpert As String, ByVal VivaDate As String, ByVal VivaListId As Integer, ByVal LogInUser As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveNewRoasterForProject(ByVal RoasterName As String,
                              ByVal Expertise As String,
                              ByVal MobileNumber As String,
                              ByVal Email As String,
                              ByVal Remarks As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AssignRoasterForProject(ByVal txtHORName As Integer,
                                  ByVal RoasterId As Integer,
                                  ByVal ProposalId As Integer, ByVal InterviewDate As String, ByVal LogInUser As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateInterviewResultForProject(
                                   ByVal ConceptualAndTheoreticalReview As String,
                                   ByVal ReviewOfRelatedStudies As String,
                                   ByVal Language As String,
                                   ByVal UserID As String,
                                   ByVal InterviewResultEntryId As Integer) As String




    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function getInterviewResultByIdForProject(ByVal InterviewResultEntryId As Integer) As ResultSet()


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveInterviewResultForProject(ByVal ConceptualAndTheoreticalReview As String,
                              ByVal ReviewOfRelatedStudies As String,
                              ByVal Language As String,
                              ByVal UserID As String,
                              ByVal ProjectID As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveNewRegisteredProject(ByVal ProjectTitle As String, ByVal StudentId As Integer, ByVal ProjRegNo As String, ByVal RegistrationDate As String, ByVal LogInUser As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateApplicationId(ByVal EmpId As String, ByVal ApplicationId As String, ByVal sUserId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetEmployeeByJobTitle(ByVal jobTitleIds As String, ByVal branchId As String) As EmployeeByJobTitle()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteJournalVoucher(ByVal voucherId As String) As String

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function PhotoGallery(ByVal ImageName As String) As String

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateExamSubjectAttendancePr(ByVal EditID As String, ByVal Ispresent As String, ByVal UserID As String) As String

    '    <OperationContract()> _
    '<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    '    Function SendMail_ACLOCK(ByVal UserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function FetchFromSetupVoucher(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ApprovedPayrollForThisMonth(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CheckLockStatus(ByVal UserID As String) As Boolean

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeactivateLock(ByVal UserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UnLockAccount(ByVal UserID As String, ByVal LockCode As String) As LoginMessage()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SetAccountLock(ByVal UserID As String) As LockMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ChangePassword(ByVal userId As String, ByVal oldpassword As String, ByVal newpassword As String) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AssignStudentFeeGroup(ByVal FeeGroupID As String, ByVal StudentID As String, ByVal BatchID As String, ByVal SemesterID As String) As String

    '    <OperationContract()> _
    '<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    '    Function SendMail(ByVal EmployeeID As String, ByVal MonthID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ApplyFormulaForAll(ByVal MonthID As String, ByVal Flag As String, ByVal value As String) As Boolean

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ApplyFormulaForMe(ByVal MonthID As String, ByVal EmployeeID As String, ByVal Flag As String, ByVal value As String) As Boolean



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AddRemarksOnPayroll(ByVal ID As String, ByVal Remarks As String, ByVal Flag As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DateConverterNew(ByVal Convertdate As String, ByVal type As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetVoucherDetails_forReport(ByVal accHead As String, ByVal sBranchID As String, ByVal sFiscalID As String, ByVal dbName As String) As String

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function edit_vourcher_no(ByVal refNo As String, ByVal modifyVal As String, ByVal refID As String, ByVal jeID1 As String, ByVal jeID2 As String, ByVal sUserId As String, ByVal BranchID As String, ByVal sFiscalyearID As String) As String

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function IL_EditSymbolNo(ByVal StudentId As String, ByVal SymbolNo As String) As String

    <OperationContract()> _
     <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function IL_BulkMarksEdit(ByVal ObtainedMarkId As String, ByVal modifyitem As String, ByVal val As String) As String

    <OperationContract()> _
     <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function IL_BulkMarksEntry(ByVal obj As BulkMarks(), ByVal AcademicYearID As String, ByVal LanguageID As String, ByVal SemesterID As String, ByVal LevelID As String, ByVal CourseDurationID As String, ByVal UserID As String, ByVal isOverwirte As Boolean, ByVal isPartial As Boolean) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeletePayrollByDeductibleType(ByVal DeductibleTypeID As String, ByVal MonthID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeletePayrollByAddableType(ByVal AddableTypeID As String, ByVal MonthID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollAttendance(ByVal EditID As String, ByVal Ispresent As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollExcludedEmployee(ByVal EditID As String, ByVal IsExcluded As String, ByVal MonthID As String) As String
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollPeriod(ByVal EditID As String, ByVal Period As String, ByVal MonthID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollRate(ByVal EditID As String, ByVal Rate As String, ByVal MonthID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function updatePayrollValues(ByVal id As String, ByVal value As String, ByVal flag As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delete_JournalVoucher(ByVal bulkGuid As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function edit_JournalVoucher(ByVal jvId As String, ByVal modifyitem As String, ByVal val As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetJournalVoucherDetailByBulkID(ByVal BranchID As String, ByVal bulkGUID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDueSummaryList(ByVal studentID As String, ByVal branchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePrintingInfo(ByVal printdocumentId As String, ByVal modifyitem As String, ByVal val As String) As String

    <OperationContract()> _
         <WebInvoke(Method:="GET", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetStudentsForPrint(ByVal branchID As String) As <System.ServiceModel.MessageParameterAttribute(Name:="data")> printList()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentPrintingInfo(ByVal studentID As String, ByVal branchID As String, ByVal docTypeID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateBudgeting(ByVal BudgetId As String, ByVal modifyitem As String, ByVal val As String, ByVal GroupID As String, ByVal SubGroupID As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function TempJournalVoucher_AddNotes(ByVal jvId As String, ByVal Notes As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetVoucherDetails_ByAccount(ByVal AccountID As String, ByVal sFiscalID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateVoucherDetails_Summary(ByVal DetailId As String, ByVal Summary As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function VoucherDetails_DR_CR_Add(ByVal DetailId As String, ByVal GroupID As String, ByVal SubGroupID As String, ByVal AccountID As String, ByVal DR As String, ByVal CR As String, ByVal Summary As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String, ByVal sFiscalID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Allocate_BudgetAmount(ByVal BudgetId As String, ByVal AllocatedAmount As String, ByVal GroupID As String, ByVal SubGroupID As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String, ByVal FiscalYearID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveScholarshipOrDiscount(ByVal StudentID As String, ByVal SchemeID As String, ByVal discountPercent As String, ByVal discountAmt As String, ByVal ParticularID As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetParticulars_Batch_SemWise(ByVal BatchID As String, ByVal SemesterID As String) As ParticularParameter()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function edit_TempJournalVourcher(ByVal jvId As String, ByVal modifyitem As String, ByVal val As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function GetFineCollectReport() As <System.ServiceModel.MessageParameterAttribute(Name:="data")> LibraryFineCollect()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CheckIfExistsISBN(ByVal ISBN As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CheckIfExistsBookAccessionNo(ByVal Accession As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delete_counter_bill(ByVal ref As String, ByVal sUserId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateAcademicStatus(ByVal AcademicInfoID As String, ByVal StatusVal As String, ByVal StatusName As String) As String
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function VerifyVoucher(ByVal sessionId As String, ByVal sUserId As String, ByVal BranchID As String, ByVal bulkGUID As String, ByVal sFiscalID As String) As String
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function approveVoucher(ByVal sessionId As String, ByVal sUserId As String) As String
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteVoucher(ByVal voucherId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function JournalVoucherDetail(ByVal sessionId As String, ByVal BranchID As String, ByVal bulkGUID As String) As JournalVoucherDetail()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ExamAttendance(ByVal obj As StudentExamAttendance()) As StudentExamAttendance

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ExamSubjectAttendance(ByVal obj As StudentExamSubjectAttendance()) As StudentExamSubjectAttendance
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ExamWeight(ByVal obj As StudentExamWeight()) As StudentExamWeight

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetExamAttendance(ByVal examId As String, ByVal batchId As String, ByVal sortBy As String,
                                      ByVal semesterId As String, ByVal sectionId As String) As StudentExamAttendance()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateAttendance(ByVal attendanceId As String, ByVal studentId As String, ByVal attendance As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CashPayment(ByVal obj As CashPayments()) As CashPayments

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CashPaymentNew(ByVal sCompanyID As String, ByVal sBranchID As String, ByVal sUserid As String, ByVal obj As CashPaymentsNew(), ByVal ReceiveAmount As String, Optional ByVal latefee As String = "0", Optional ByVal due As String = "0", Optional ByVal schemediscount As String = "0", Optional ByVal ManualBillDate As String = "") As CashPaymentsNew

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ChangeDate(ByVal Convertdate As String, ByVal type As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ChangeDate2(ByVal Convertdate As String, ByVal type As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeactivateStudent(ByVal studentId As String, ByVal opt As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function FatherDetail(ByVal id As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function MotherDetail(ByVal id As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GuardianDetail(ByVal id As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function StudentExamTimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String,
                                  ByVal batchId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Notice(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function StudentTimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String, ByVal searchOption As String, ByVal searchValue As String,
                              ByVal batchId As String, ByVal semesterId As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function StudentCalendarDays(ByVal currentYear As String, ByVal currentMonth As String, ByVal batchId As String,
                                        ByVal semesterId As String) As String

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDevice() As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetIncomeDetails(ByVal groupId As String, ByVal subgroupId As String, ByVal grouptype As String) As IncomeDetails()



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveDeviceUsers(ByVal deviceUser As DeviceUser()) As DeviceUser

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDeviceUsers(ByVal deviceId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function StudentAttendance(ByVal stdAtt As StudentAttendances()) As StudentAttendances

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function EmployeeAttendance(ByVal empAttendance As EmployeeAttendances()) As EmployeeAttendances


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDeviceAttendanceRecord(ByVal deviceId As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ConnectDevice(ByVal ip As String, ByVal port As Integer) As Boolean
    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Months(ByVal yearId As String) As MonthResponse()
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DisconnectDevice(ByVal ip As String, ByVal port As Integer) As Boolean



    <OperationContract()> <WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Calendar() As String



    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveEmployee(ByVal employeeNo As String,
                          ByVal firstName As String,
                          ByVal lastName As String,
                          ByVal gender As String,
                          ByVal doB As String,
                          ByVal nationality As String,
                          ByVal religion As String,
                          ByVal maritalStatus As String,
                          ByVal spouseName As String,
                          ByVal numberOfChild As String,
                          ByVal fatherName As String,
                          ByVal motherName As String,
                          ByVal email As String,
                          ByVal phone As String,
                          ByVal bloodGroup As String,
                          ByVal webSite As String,
                          ByVal permanentAddress As String,
                          ByVal contactAddress As String,
                          ByVal pan As String,
                          ByVal joiningDate As String,
                          ByVal employeeCategories As String,
                          ByVal department As String,
                          ByVal jobTitleId As String,
                          ByVal workingStatus As String,
                          ByVal shift As String,
                          ByVal isActive As String,
                          ByVal basicSalary As String,
                          ByVal photo As String,
                          ByVal modifiedBy As String,
                          ByVal employeeRole As String,
                          ByVal branchId As String,
                          ByVal jobTitle As String,
                          ByVal dateFrom As String,
                          ByVal dateTo As String,
                          ByVal institution As String,
                          ByVal remarks As String,
                          ByVal awardName As String,
                          ByVal awardDate As String,
                          ByVal awardInstitution As String,
                          ByVal awardRemarks As String,
                          ByVal qualificationName As String,
                          ByVal passedYear As String,
                          ByVal percentage As String,
                          ByVal qualificationInstitution As String,
                          ByVal qualificationRemarks As String,
                          ByVal trainingName As String,
                          ByVal trainingFrom As String,
                          ByVal trainingTo As String,
                          ByVal trainingInstitution As String,
                          ByVal trainingRemarks As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveSubjectRegistration(ByVal obj As SaveSubjectRegistration()) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveExamSubjectRegistration(ByVal obj As SaveExamSubjectRegistration()) As ErrorMessage


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ObtainMarkEntry(ByVal obj As ObtainMarkEntry()) As ObtainMarkEntry

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ObtainMarkEntryForTrainee(ByVal obj As ObtainMarkEntry()) As ObtainMarkEntry


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetExamStudents(ByVal examId As String, ByVal batchId As String, ByVal semester As String, ByVal section As String,
                                    ByVal sortyBy As String, ByVal subject As String, ByVal studentId As String) As ExamStudents()
    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetEvaluationTrainee(ByVal examId As String, ByVal sortyBy As String, ByVal subject As String, ByVal studentId As String) As ExamTrainee()
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetExamStudentsW(ByVal examId As String, ByVal batchId As String, ByVal semester As String, ByVal section As String,
                                    ByVal sortyBy As String, ByVal subject As String, ByVal studentId As String) As ExamStudents()


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CalendarDays(ByVal currentYear As String, ByVal currentMonth As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CalendarEvent(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function TimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String, ByVal searchOption As String, ByVal searchValue As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Holiday(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Leave(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ExamTimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function TimeTableFilterOptions(ByVal filterOption As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetIndividualMarksCompareAcrossTerms(ByVal batchId As String, ByVal StudentID As String) As Array
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetIndividualMarksCompareAcrossTermsST(ByVal StudentID As String) As Array

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ComparisonMarksPercentVsStudentNumber(ByVal slabPerentage As String) As Array

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDivisionComparisonReport(ByVal batchId As String) As Array

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateParticular(ByVal Amount As String, ByVal FeesDetailsID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteFees(ByVal FeesDetailsID As String) As String


#Region " Service Method for calling Exam module developer || Pralhad "

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ObtainGradeEntry(ByVal obj As ObtainGradeEntry()) As ObtainGradeEntry

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSubjectGradeFroEdit(ByVal batchId As String, ByVal semester As String, ByVal section As String,
                                    ByVal sortyBy As String, ByVal subject As String) As GetSubjectGradeFroEdit()


    'Function UpdateObtainGradeEntry(ByVal SubjectGradeEntryID As String, ByVal ObtainedGrade As String, ByVal User As String) As ErrorMessage()
    '    <OperationContract()> _
    '<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    '    Function UpdateObtainGradeEntry(ByVal obj As School.UpdateObtainGradeEntry()) As ErrorMessage

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetExamGroupingList(ByVal fromYearID As String, ByVal toYearID As String, ByVal flag As String, ByVal HaveWeightage As String) As GetExamGroupingList()

    '    <OperationContract()> _
    '<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    '    Function UpdateGroupingExam(ByVal obj As School.UpdateGroupingExam()) As ErrorMessage

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ViewExamGroupingList() As ViewExamGroupingList()

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ViewExamGroupingByID(ByVal ExamGroupID As String) As ViewExamGroupingByID()


    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteExamGroup(ByVal ExamGroupID As String, ByVal DeletedBy As String) As ErrorMessage

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveStudentsGroupMarks(ByVal ExamGroupID As String, ByVal BatchID As String, ByVal UserID As String) As ErrorMessage

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentsGroupMarks(ByVal ExamGroupID As String, ByVal BatchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePraticalMarks(ByVal obtainMarkId As String, ByVal ObtainedPratical As String, ByVal UserID As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateTag(ByVal StudentID As String, ByVal TagID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateTheoryMarks(ByVal obtainMarkId As String, ByVal ObtainedTheory As String, ByVal UserID As String) As String

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateTheoryMarksSub(ByVal sn As String, ByVal FullTheory As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePracticalMarksSub(ByVal sn As String, ByVal FullPractical As String) As String

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePassMarksSub(ByVal sn As String, ByVal PassTheory As String) As String
    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePracticalPassMarksSub(ByVal sn As String, ByVal PassPractical As String) As String
    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateCAS(ByVal DirectAssementID As String, ByVal ObtainedMarks As String) As String
#Region "Direct Assessment module || developer : Pralhad "
    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetAssessmentStudents(ByVal batchId As String, ByVal examID As String, ByVal StudentName As String, ByVal SectionID As String) As ExamStudents()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetFinalStudents(ByVal batchId As String, ByVal StudentName As String, ByVal SectionID As String) As ExamStudents()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDirectAssessmentStudents(ByVal batchId As String, ByVal sortyBy As String, ByVal SubjectID As String, ByVal SectionID As String, ByVal studentId As String) As ExamStudents()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentsForSheetPlan(ByVal ExamID As String, ByVal batchId As String, ByVal sortyBy As String, ByVal SectionIDs As String) As StudentsForSheetPlan()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetGroupCASEntryStudents(ByVal batchId As String, ByVal sortyBy As String, ByVal SubjectID As String, ByVal SectionID As String, ByVal studentId As String) As GroupCASEntryStudents()


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveDirectAssessment(ByVal obj As SaveDirectAssessment()) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveExamAttendanceSheet(ByVal obj As SaveExamAttendanceSheet()) As ErrorMessage


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveStudentsForSheetPlan(ByVal obj As SaveStudentsForSheetPlan()) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveGroupCASEntry(ByVal obj As SaveDirectAssessment()) As ErrorMessage


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveContinuousAssessment(ByVal obj As SaveContinuousAssessment()) As ErrorMessage


#End Region

#End Region


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentBatch(ByVal studentId As Integer) As StudentBatch()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetCategories() As Categories()


    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetFaculty(ByVal sBranchID As String) As FacultyParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function Getaccount() As AccountParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSemester() As SemesterParameter()



    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetGroup() As GroupParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSubGroup() As SubGroupParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetAccountUnder() As AccountUnder()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentsWithoutAccount() As StudentsWithouAccount()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetProgrammeAppliedFor() As ProgrammeAppliedFor()



    <OperationContract()> _
    <WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetAcademicYear() As AcademicYearParameter()


    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetBatch(ByVal sBranchID As String) As BatchParameter()



    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetAdmissionStatus() As AdmissionStatusParameter()



    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetBoard() As BoardParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetGrade() As GradeParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSection() As SectionService()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetLanguage() As LanguageParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetLevel() As LevelParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetReligious() As ReligiousParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStream() As StreamParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetFeesCategories() As FeesCategoriesParameter()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SubjectDelete(ByVal SubjectID As String) As Message()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function LocalResource(ByVal counterID As String, ByVal resourceType As String, ByVal resourceFor As String, ByVal resourceName As String) As LocalResource()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetEmployeeCategories() As EmployeeCategoriesParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDepartment() As DepartmentParameter()

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetJobTitle(ByVal sBranchID As String) As JobTitleParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetShift() As ShiftParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSubject() As SubjectParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSubjectAllotment() As SubjectAllotmentParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetTeacherAllotment() As TeacherAllotmentParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetWeekDaysAllotment() As WeekDaysAllotmentParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetEmployee() As EmployeeParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetLeave() As LeavePatameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetComplaintType() As ComplainTypeParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetDisciplineCategories() As DisciplineCategoriesParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudent() As StudentParameter()


    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetStudentInfo() As StudentParameter()


    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetYear(ByVal sBranchID As String) As YearParameter()

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetCustomers(ByVal BatchID As String) As List(Of ListItem)

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetFiscalYear(ByVal sBranchID As String) As FiscalYearParameter()

    <OperationContract()> _
<WebGet(BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetBusStation() As BusStationParameter()


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function StudentLogin(ByVal username As String, ByVal password As String) As LoginMessage()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UserLogin(ByVal username As String, ByVal password As String, Optional ByVal db_connectionString As String = "") As LoginMessage()

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetClass() As List(Of ListItem)

    <OperationContract()> _
      <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveMiniSchoolStudent(ByVal FirstName As String,
                              ByVal MiddleName As String,
                              ByVal LastName As String,
                              ByVal Gender As String,
                              ByVal DateOfBirth As String,
                              ByVal Address As String,
                              ByVal Status As String,
                              ByVal UserID As String,
                              ByVal FatherName As String,
                              ByVal FatherMobile As String,
                              ByVal RollNo As String,
                              ByVal S_Class As String,
                              ByVal Section As String,
                              ByVal RegistrationNumber As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function RegisterUser(ByVal SymbolNo As String, ByVal MobileNo As String, ByVal Name As String) As String


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetUser(ByVal RegistrationID As String) As Profile()

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetVoucherDetails(ByVal ref As String, ByVal sBranchID As String, ByVal sFiscalyearID As String) As String

    <OperationContract()> _
      <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function edit_vourcher(ByVal jid As String, ByVal modi As String, ByVal val As String, ByVal sUserId As String) As String

    <OperationContract()> _
          <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delete_v_single_row(ByVal jid As String, ByVal sUserId As String) As String

    <OperationContract()> _
          <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function edit_multi_rows_vourcher(ByVal ref As String, ByVal modi As String, ByVal val As String, ByVal sUserId As String, ByVal BranchID As String, ByVal sFiscalyearID As String) As String

    <OperationContract()> _
      <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function delete_multi_rows_vourcher_no(ByVal ref As String, ByVal sUserId As String, ByVal BranchID As String, ByVal sFiscalyearID As String) As String

    <OperationContract()> _
      <WebInvoke(Method:="GET", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function vouchersList(ByVal branchid As String, ByVal fyid As String) As <System.ServiceModel.MessageParameterAttribute(Name:="data")> vouchersList()

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function vouchersListByID(ByVal ref As String) As String

    <OperationContract()> _
      <WebInvoke(Method:="GET", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)>
    Function CancelvouchersList(ByVal branchid As String, ByVal fyid As String) As <System.ServiceModel.MessageParameterAttribute(Name:="data")> vouchersList()

    <OperationContract()> _
         <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function CancelvouchersListByID(ByVal ref As String, ByVal sBranchID As String) As String


    <OperationContract()> _
       <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePreference(ByVal obj As String) As String

    <OperationContract()> _
       <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveBankVoucher(ByVal StudentID As String, ByVal VoucherNo As String, ByVal BankName As String, ByVal BankAddress As String, ByVal VoucherAmount As String, ByVal VoucherDate As String, ByVal DepositBy As String, ByVal Remarks As String,
                             ByVal sCompanyID As String, ByVal sBranchID As String, ByVal sFiscalYearID As String, ByVal sAcademicYearID As String, ByVal sUserId As String) As String

    <OperationContract()> _
       <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveNote(ByVal StudentID As String, ByVal Note As String, ByVal Isdisplay As String,
                      ByVal sCompanyID As String, ByVal sBranchID As String, ByVal sFiscalYearID As String, ByVal sAcademicYearID As String, ByVal sUserId As String) As String

    <OperationContract()> _
   <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteNote(ByVal NoteID As String) As String

    <OperationContract()> _
      <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetBookClassification(ByVal isbn As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SymbolNumber(ByVal SymbolNumberID As String) As String


    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SymbolNumberInsert(ByVal BatchID As String, ByVal SemesterID As String, ByVal SectionID As String, ByVal ExamID As String, ByVal StudentID As String, ByVal SymberNumber As String, ByVal sUserId As String, ByVal BranchID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateRollNo(ByVal RollNo As String, ByVal StudentID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateUniversityReg(ByVal reg As String, ByVal studentID As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateExamSubjectAttendanceTh(ByVal EditID As String, ByVal Ispresent As String, ByVal UserID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateDeductiveAmount(ByVal DeductiblesToPayrollID As String, ByVal DeductiveAmount As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateAddableAmount(ByVal AddablesToPayrollID As String, ByVal AddableAmount As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetEmployeeInfo(ByVal YearID As String, ByVal MonthID As String, ByVal AddableID As String, ByVal JobID As String, ByVal IsZero As Boolean, ByVal sBranchID As String, Optional ByVal type As String = "") As EmployeeInfos()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AddAddableToPayRoll(ByVal obj As AddableToPayroll()) As AddableToPayroll

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AddDeductiveToPayRoll(ByVal obj As AddableToPayroll()) As AddableToPayroll

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateBasicSalary(ByVal EmployeeID As String, ByVal Salary As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateKoshInfo(ByVal EmployeeID As String, ByVal ChangeInfo As String, ByVal Flag As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetAssignedStudentToUniversityExam(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList()

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetAssignedStudentToUniversityExamBackPaper(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList()

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteAssignedStudentToUniversityExam(ByVal AssignStudentToSubjectID As String) As ErrorMessage

    <OperationContract()> _
     <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteAssignedStudentToUniversityExamBackPaper(ByVal AssignStudentToSubjectID As String) As ErrorMessage

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdateAssignedStudentToUniversityExamIsBackPaper(ByVal AssignStudentToSubjectID As String, ByVal IsBackPaper As String) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ListSubjectAssignToStudent(ByVal AcademicYearID As String, ByVal SemesterID As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function ListSubjectOfSingleStudent(ByVal StudentID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As String

    <OperationContract()> _
    <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AddNewSubject(ByVal StudentID As String, ByVal SubjectID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetSubjectDropDownList(ByVal SemesterID As String) As Subjectlist()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteOneStudentAllSubjectInOneSemesterOneAcademicYear(ByVal StudentID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteOneStudentOneSubjectInOneSemesterOneAcademicYear(ByVal ExamSubjectRegistrationID As String) As ErrorMessage


    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetMarksEntryStudent(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function GetMarksEntryStudentBackPaper(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList()

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteAllSubjectBySingleStudent(ByVal StudentID As String, ByVal SemeseterID As String, ByVal AcademicYearID As String) As ErrorMessage

    <OperationContract()> _
      <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function BulkMarksEntry(ByVal obj As BulkMarks(), ByVal AcademicYearID As String, ByVal CourseID As String, ByVal SemesterID As String, ByVal UserID As String, ByVal isOverwirte As Boolean, ByVal isPartial As Boolean, ByVal isBackPaper As String, ByVal BranchID As String) As ErrorMessage

    <OperationContract()> _
 <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePCExamMarks(ByVal ID As String, ByVal Marks As String, ByVal flag As String) As String

    <OperationContract()> _
  <WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePCExamSymbol(ByVal ID As String, ByVal Symbol As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function SaveSubjectRegistrationNew(ByVal obj As SaveSubjectRegistration()) As ErrorMessage

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function DeleteRecordsOfPayroll(ByVal EmployeeID As String, ByVal MonthID As String, ByVal Flag As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollSingleEmployee(ByVal EmployeeID As String, ByVal MonthID As String, ByVal Flag As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function UpdatePayrollSingleEmployeeRefresh(ByVal ID As String, ByVal Flag As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AddHostelStudent(ByVal StudentIDs As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function AssignBedToHostelStudent(ByVal HostelStudentIDs As String, ByVal RoomIDs As String, ByVal BedIDs As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function RemoveBedToStudent(ByVal BedIds As String) As String

    <OperationContract()> _
<WebInvoke(Method:="POST", ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.WrappedRequest)>
    Function IsActiveHostelStudent(ByVal ISHostelIDs As String, ByVal ISHostelStudentIDs As String) As String
End Interface
