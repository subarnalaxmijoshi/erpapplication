﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="UserMenu.aspx.vb" Inherits="School.UserMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var checkFromValidation = false;
        $(document).ready(function () {
            $('#form').validate();
            $("#<%=ddlUserRole.ClientID %>").rules('add', { required: true, messages: { required: 'User Role is required.'} });
            $("#<%=ddlUser.ClientID %>").rules('add', { required: true, messages: { required: 'User Role is required.'} });
            checkFromValidation = function () {
                var bool = true;
                if ($('#<%=ddlUserRole.ClientID %>').valid() == false) bool = false;
                if ($('#<%=ddlUser.ClientID %>').valid() == false) bool = false;
                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };

            $("#MainContent_grdUserMenu th:last-child, #MainContent_grdUserMenu td:last-child").hide();
            $("#MainContent_grdUserMenu th:first-child, #MainContent_grdUserMenu td:first-child").hide();

            $('#MainContent_grdUserMenu').find('tbody tr').each(function () {
                var a = $(this).find("td:first").text();
              
                console.log(a);
                if (a == "True") {
                    var b = $(this).children("td").next("td").next("td").children("span.isActive");
                    var c = $(b).closest("div.checker");
                    var d = $(c).closest("span");
                    $(d).addClass("checked");
                   // alert("This is active");
                } else if (a == "False") {
                    //alert("This is not active");
                }
            });

        });
    </script>
    <style type="text/css">
        .chk
        {
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    User Menu <small>Assign the menu for users</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/SideBar/UserMenu.aspx">User Menu</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
            </div>
        </div>
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#create" aria-controls="create" role="tab"
                    data-toggle="tab">Add</a></li>
                <li role="presentation"><a href="#display" aria-controls="display" role="tab" data-toggle="tab">
                    View</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="create">
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>Create User Menu</div>
                            <div class="tools">
                                <a href="" class="collapse"></a><a href="" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-horizontal">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            User :</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                <asp:DropDownList ID="ddlUser" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <label for="<%=ddlUser.ClientID%>" class="error" style="display: none;">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            User Role :</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                <asp:DropDownList ID="ddlUserRole" runat="server" CssClass="form-control" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                            <label for="<%=ddlUserRole.ClientID%>" class="error" style="display: none;">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:GridView ID="grdUserMenu" CssClass="table table-bordered table-striped table-condensed flip-content"
                                                runat="server" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField HeaderText="IsActive" DataField="IsActive" />
                                                    <asp:BoundField HeaderText="S.N" DataField="Row" />
                                                    <asp:TemplateField HeaderText="Select">
                                                        <ItemTemplate>
                                                            <asp:CheckBox CssClass="isActive" ID="chkIsActive" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Module" DataField="ModuleName" />
                                                    <asp:BoundField HeaderText="Sub Module" DataField="SubModuleName" />
                                                    <asp:BoundField HeaderText="Menu Text" HtmlEncode="False" DataField="MenuText" />
                                                    <asp:BoundField HeaderText="Menu Link" DataField="MenuLink" />
                                                    <asp:BoundField HeaderText="MenuId" DataField="MenuId" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-9">
                                            <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkFromValidation();"
                                                runat="server" Text="Submit" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="display">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>User Menu Information</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                            class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                                class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body flip-scroll" id="listData" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
