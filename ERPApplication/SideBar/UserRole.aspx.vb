﻿Public Class UserRole
    Inherits System.Web.UI.Page

    Private ReadOnly Dao As New DatabaseDao
    Private sql As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            'Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            'Response.Redirect(redirectUrl)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("RoleId") <> Nothing Then
                Dim key = Request.QueryString("RoleId").ToString()
                GetById(key)
            ElseIf Request.QueryString("DeleteRoleId") <> Nothing Then
                Dim key = Request.QueryString("DeleteRoleId").ToString()
                Delete(key)
            End If
            GetAll()

        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text.ToLower() = "update" Then
            If (Request.QueryString("RoleId") <> Nothing) Then
                Dim key = Request.QueryString("RoleId").ToString()
                Update(key, txtUserRole.Text.Trim())
            End If
        Else
            Save(txtUserRole.Text.Trim())
        End If
        GetAll()
    End Sub


    Private Sub Save(ByVal userRole As String)
        sql = "exec [Users].[usp_UserRole] @Flag='i', @UserRole='" & userRole & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then

                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-danger'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString())
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub Update(ByVal id As String, ByVal userRole As String)
        sql = "exec [Users].[usp_UserRole] @Flag='u', @UserRole='" & userRole & "', @UserRoleID='" & id & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim sb As New StringBuilder
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1") Then
                    sb.AppendLine("<div class='note note-success'>")
                    btnSubmit.Text = "Submit"
                Else
                    sb.AppendLine("<div class='note note-danger'>")
                End If
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString())
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
            
        End If
    End Sub

    Private Sub Delete(ByVal id As String)
        sql = "exec [Users].[usp_UserRole] @Flag='d',  @UserRoleID='" & id & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim sb As New StringBuilder
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1") Then
                    sb.AppendLine("<div class='note note-success'>")
                Else
                    sb.AppendLine("<div class='note note-danger'>")
                End If
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString())
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString

            End If
        End If

    End Sub

    Private Sub GetAll()
        sql = "exec [Users].[usp_UserRole] @Flag='s'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>User Role</th>")
                sb.AppendLine("<th colspan=2>Action</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim userRoleId = ds.Tables(0).Rows(i).Item("UserRoleId").ToString
                    Dim userRoleName = ds.Tables(0).Rows(i).Item("UserRole").ToString
                  

                    sb.AppendLine("<tr>")
                    sb.Append("<td class='numeric'>" & i + 1 & "</td>")
                    sb.AppendLine("<td>" & userRoleName & "</td>")
                    sb.AppendLine("<td style='text-align:center'><a href='?RoleId=" & userRoleId & "'> Edit </a></td>")
                    sb.AppendLine("<td style='text-align:center'><a href='?DeleteRoleId=" & userRoleId & "'> Delete </a></td>")

                    sb.AppendLine("</tr>")
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                listData.InnerHtml = sb.ToString()
            End If
        End If
    End Sub

    Private Sub GetById(ByVal id As String)
        sql = "exec [Users].[usp_UserRole] @Flag='s',  @UserRoleID='" & id & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtUserRole.Text = ds.Tables(0).Rows(0).Item("UserRole").ToString()
                btnSubmit.Text = "Update"
            End If
        End If
    End Sub

End Class