﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SubModule.aspx.vb" Inherits="School.SubModule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
            <script type="text/javascript">
                var checkFromValidation = false;
                $(document).ready(function () {
                    $('#form').validate();
                    $("#<%=ddlUserRole.ClientID %>").rules('add', { required: true, messages: { required: 'User Role is required.'} });
                    $("#<%=ddlModule.ClientID %>").rules('add', { required: true, messages: { required: 'Module Name is required.'} });
                    $("#<%=txtSubModule.ClientID %>").rules('add', { required: true, messages: { required: 'Sub-Module Name is required.'} });

                    checkFromValidation = function () {
                        var bool = true;
                        if ($('#<%=ddlUserRole.ClientID %>').valid() == false) bool = false;
                        if ($('#<%=ddlModule.ClientID %>').valid() == false) bool = false;
                        if ($('#<%=txtSubModule.ClientID %>').valid() == false) bool = false;
                        if (!bool) $('#form').validate().focusInvalid();
                        return bool;
                    };

                    $("#<%= ddlUserRole.ClientID %>").change(function () {
                        $("#<%= ddlModule.ClientID %>").html("<option value=''>Loading...</option>");
                    });
                });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>
    <div class="page-content">
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                   Sub-Module <small>Create the sub module for application</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/SideBar/SubModule.aspx">Sub Module</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Create User Sub Module
                        </div>
                        <div class="tools">
                            <a href="" class="collapse"></a>
                            <a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        
                                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">
                                       User Role:</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <asp:DropDownList ID="ddlUserRole" runat="server" CssClass="form-control" 
                                                AutoPostBack="True"></asp:DropDownList>
                                        </div>
                                        <label for="<%=ddlUserRole.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">
                                       Module Name:</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                            <asp:DropDownList ID="ddlModule" runat="server" CssClass="form-control" 
                                                AutoPostBack="True"></asp:DropDownList>
                                        </div>
                                        <label for="<%=ddlModule.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">
                                       Sub-Module Name:</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                            <asp:TextBox ID="txtSubModule" runat="server" CssClass="form-control" placeholder="Enter Sub Menu Name"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtSubModule.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">
                                       Sub-Module Class:</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                            <asp:TextBox ID="txtSubModuleClass" runat="server" CssClass="form-control" placeholder="Enter Module Name"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtSubModuleClass.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                               
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkFromValidation();"
                                            runat="server" Text="Submit" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>User Sub Module Information</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                        class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                            class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll" id="listData" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
