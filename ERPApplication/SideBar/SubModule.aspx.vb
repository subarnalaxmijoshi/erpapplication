﻿Public Class SubModule
    Inherits System.Web.UI.Page


    Private ReadOnly Dao As New DatabaseDao
    Private sql As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            'Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            'Response.Redirect(redirectUrl)
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Request.QueryString("SubModuleId") <> Nothing Then
                Dim key = Request.QueryString("SubModuleId")
                GetById(key)
            ElseIf Request.QueryString("DeleteSubModuleId") <> Nothing Then
                Dim key = Request.QueryString("DeleteSubModuleId")
                Delete(key)
            End If
            GetUserRole()
            ddlUserRole_SelectedIndexChanged(sender, e)
            GetAll()
        End If
    End Sub

    Protected Sub ddlUserRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlUserRole.SelectedIndexChanged
        GetModuleByUser(ddlUserRole.SelectedValue)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text.ToLower() = "update" Then
            If Request.QueryString("SubModuleId") <> Nothing Then
                Dim key = Request.QueryString("SubModuleId")
                Update(ddlModule.SelectedValue, txtSubModule.Text, txtSubModuleClass.Text, key)
            End If
        Else
            Save(ddlModule.SelectedValue, txtSubModule.Text, txtSubModuleClass.Text)
        End If
        GetAll()
    End Sub

    Private Sub GetUserRole()
        sql = "EXEC Users.usp_UserRole @Flag ='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlUserRole.DataSource = ds.Tables(0)
                ddlUserRole.DataValueField = "UserRoleID"
                ddlUserRole.DataTextField = "UserRole"
                ddlUserRole.DataBind()
            Else
                ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        End If
    End Sub

    Private Sub GetModuleByUser(ByVal userRole As String)
        sql = "EXEC [Users].[usp_Module] @Flag='f', @UserRoleID='" & userRole & "'"
        Dim ds As New DataSet
        ddlModule.Items.Clear()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlModule.DataSource = ds.Tables(0)
                ddlModule.DataValueField = "ModuleID"
                ddlModule.DataTextField = "ModuleName"
                ddlModule.DataBind()
            Else
                ddlModule.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        End If
    End Sub

    

    Private Sub Save(ByVal moduleId As String, ByVal subModuleName As String, ByVal subModuleClass As String)
        sql = "EXEC [Users].[usp_SubModule] @Flag='i', @ModuleId='" & moduleId & "', @SubModuleName='" & subModuleName &
                "', @SubModuleClass='" & subModuleClass & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode") = "1" Then
                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub Update(ByVal moduleId As String, ByVal subModuleName As String, ByVal subModuleClass As String,
                       ByVal subModuleId As String)
        sql = "EXEC [Users].[usp_SubModule] @Flag='u', @ModuleId='" & moduleId & "', @SubModuleName='" & subModuleName &
               "', @SubModuleClass='" & subModuleClass & "', @SubModuleID = '" & subModuleId & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode") = "1" Then
                btnSubmit.Text = "Submit"
                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub Delete(ByVal subModuleId As String)
        sql = "EXEC [Users].[usp_SubModule] @Flag='d', @SubModuleID = '" & subModuleId & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode") = "1" Then
                btnSubmit.Text = "Submit"
                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub GetAll()
        sql = "EXEC [Users].[usp_SubModule] @Flag='s'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>S.N</th>")
            sb.AppendLine("<th>User Role</th>")
            sb.AppendLine("<th>Module</th>")
            sb.AppendLine("<th>Sub Module</th>")
            sb.AppendLine("<th>Class</th>")
            sb.AppendLine("<th colspan=2 style='text-align:center'>Actions</th>")


            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim id = ds.Tables(0).Rows(i).Item("SubModuleID").ToString
                Dim userRole = ds.Tables(0).Rows(i).Item("UserRole").ToString
                Dim moduleName = ds.Tables(0).Rows(i).Item("ModuleName").ToString
                Dim subModuleName = ds.Tables(0).Rows(i).Item("SubModuleName").ToString
                Dim subModuleClass = ds.Tables(0).Rows(i).Item("SubModuleClass").ToString

                sb.AppendLine("<tr>")
                sb.Append("<td class='numeric'>" & i + 1 & "</td>")
                sb.AppendLine("<td>" & userRole & "</td>")
                sb.AppendLine("<td>" & moduleName & "</td>")
                sb.AppendLine("<td>" & subModuleName & "</td>")
                sb.AppendLine("<td>" & subModuleClass & "<span style='padding-left: 10px;' class='" & subModuleClass & "'</span></td>")
                sb.AppendLine("<td style='text-align:center'><a href='?SubModuleId=" & id & "'> Edit </a></td>")
                sb.AppendLine("<td style='text-align:center'><a href='?DeleteSubModuleId=" & id & "'> Delete </a></td>")

                sb.AppendLine("</tr>")
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            listData.InnerHtml = sb.ToString()
        End If
    End Sub

    Private Sub GetById(ByVal subModuleId As String)
        sql = "EXEC [Users].[usp_SubModule] @Flag='s', @SubModuleID='" & subModuleId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.SelectedValue = ds.Tables(0).Rows(0).Item("UserRoleID")
            ddlModule.SelectedValue = ds.Tables(0).Rows(0).Item("ModuleId")
            txtSubModule.Text = ds.Tables(0).Rows(0).Item("SubModuleName")
            txtSubModuleClass.Text = ds.Tables(0).Rows(0).Item("SubModuleClass")
            btnSubmit.Text = "Update"
        End If
    End Sub


End Class