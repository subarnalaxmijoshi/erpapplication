﻿Public Class _Module
    Inherits System.Web.UI.Page

    Private ReadOnly Dao As New DatabaseDao
    Private sql As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            ' Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            ' Response.Redirect(redirectUrl)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Request.QueryString("ModuleId") <> Nothing Then
                GetById(Request.QueryString("ModuleId"))

            ElseIf Request.QueryString("DeleteModuleID") <> Nothing Then
                Delete(Request.QueryString("DeleteModuleID"))
            End If
            GetUserRole()
            GetAll()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        If (btnSubmit.Text.ToLower() = "update") Then
            Update(ddlUserRole.SelectedValue.ToString(), txtModuleName.Text, txtModuleClass.Text, "1")
        Else
            Save(ddlUserRole.SelectedValue.ToString(), txtModuleName.Text, txtModuleClass.Text)
        End If
        btnSubmit.Text = "Submit"
        GetAll()
        Clear()
    End Sub


    Private Sub GetUserRole()
        sql = "EXEC Users.usp_UserRole @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.DataSource = ds.Tables(0)
            ddlUserRole.DataValueField = "UserRoleId"
            ddlUserRole.DataTextField = "UserRole"
            ddlUserRole.DataBind()
        Else
            ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
        End If
    End Sub

    Private Sub Save(ByVal userRole As String, ByVal moduleName As String, ByVal moduleClass As String)
        sql = "EXEC Users.usp_Module @Flag = 'i', @UserRoleID='" & userRole & "', @ModuleName = '" & moduleName & "', @ModuleClass='" & moduleClass & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString())
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If
    End Sub

    Private Sub Update(ByVal userRole As String, ByVal moduleName As String, ByVal moduleClass As String, ByVal id As String)
        sql = "EXEC Users.usp_Module @Flag = 'u', @UserRoleID='" & userRole & "', @ModuleName = '" & moduleName & "', @ModuleClass='" & moduleClass & "', @ModuleID = '" & id & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If
    End Sub

    Private Sub Delete(ByVal id As String)
        sql = "EXEC Users.usp_Module @Flag = 'd',  @ModuleID = '" & id & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If
    End Sub

    Private Sub GetAll()
        sql = "EXEC Users.usp_Module @Flag = 's'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        listData.InnerHtml = ""
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            'sb.AppendLine("<th>Board ID </th>")
            sb.AppendLine("<th>S.N</th>")
            sb.AppendLine("<th>User Role</th>")
            sb.AppendLine("<th>Module</th>")
            sb.AppendLine("<th>Class</th>")
            sb.AppendLine("<th colspan=2 style='text-align:center'>Actions</th>")


            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim moduleName = ds.Tables(0).Rows(i).Item("ModuleName").ToString
                Dim moduleClass = ds.Tables(0).Rows(i).Item("ModuleClass").ToString
                Dim userRole = ds.Tables(0).Rows(i).Item("UserRole").ToString
                Dim id = ds.Tables(0).Rows(i).Item("ModuleID").ToString

                sb.AppendLine("<tr>")
                sb.Append("<td class='numeric'>" & i + 1 & "</td>")
                sb.AppendLine("<td>" & userRole & "</td>")
                sb.AppendLine("<td>" & moduleName & "</td>")
                sb.AppendLine("<td>" & moduleClass & "<span style='padding-left: 10px;' class='" & moduleClass & "'</span></td>")
                sb.AppendLine("<td style='text-align:center'><a href='?ModuleId=" & id & "'> Edit </a></td>")

                sb.AppendLine("<td style='text-align:center'><a href='?DeleteModuleID=" & id & "'> Delete </a></td>")

                sb.AppendLine("</tr>")
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            listData.InnerHtml = sb.ToString()
        End If
    End Sub

    Private Sub GetById(ByVal id As String)
        sql = "EXEC Users.usp_Module @Flag = 's',  @ModuleID = '" & id & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.SelectedValue = ds.Tables(0).Rows(0).Item("UserRoleID").ToString()
            txtModuleName.Text = ds.Tables(0).Rows(0).Item("ModuleName").ToString()
            txtModuleClass.Text = ds.Tables(0).Rows(0).Item("ModuleClass").ToString()
            btnSubmit.Text = "Update"
        End If
    End Sub

    Private Sub Clear()
        txtModuleName.Text = String.Empty
        txtModuleClass.Text = String.Empty
    End Sub
End Class