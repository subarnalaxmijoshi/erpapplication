﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Public Class MenusTreeView
    Inherits System.Web.UI.Page
    Private ReadOnly Dao As New DatabaseDao

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            GetUserRole()
        End If
    End Sub

    Private Sub PopulateRootLevel()
        TreeView1.Nodes.Clear()
        Dim dt As New DataTable()
        Dim query = "exec users.usp_TreeViewMenus @flag='a',@UserRoleID='" & ddlUserRole.SelectedValue & "',@UserId='" & ddl_user_list.SelectedValue & "'"
        dt = Dao.ExecuteDataTable(query)


        PopulateNodes(dt, TreeView1.Nodes)

    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
  ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("ModuleName").ToString()
            tn.Value = dr("id").ToString()
            If Not String.IsNullOrWhiteSpace(dr("IsActive").ToString()) Then
                tn.Checked = dr("IsActive").ToString()
            Else
                tn.Checked = False
            End If
            ''tn.NavigateUrl = "/Accounts/SearchDiscount.aspx"




            nodes.Add(tn)

            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As Integer, _
  ByVal parentNode As TreeNode)
        Dim dt As New DataTable()
        Dim query = "exec users.usp_TreeViewMenus @flag='b', @ParrentID='" & parentid & "' "
        dt = Dao.ExecuteDataTable(query)
        PopulateNodes(dt, parentNode.ChildNodes)
    End Sub

    Protected Sub TreeView1_TreeNodePopulate(ByVal sender As Object, _
  ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeView1.TreeNodePopulate
        PopulateSubLevel(CInt(e.Node.Value), e.Node)
    End Sub



    Sub updateMenu(ByVal userid As String, ByVal menuid As String, ByVal userstatus As String)
        Dim Sql = "exec users.usp_menuUpdate @menuID='" & menuid & "', @UserID='" & userid & "',@IsActive='" & userstatus & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            message.InnerHtml = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

    End Sub

    Sub save()
        If TreeView1.CheckedNodes.Count > 0 Then
            Dim node As TreeNode
            Dim menuids As String = ""
            For Each node In TreeView1.CheckedNodes
                If node.Value < 300001 And node.Value > 200001 Then
                    menuids &= node.Value - 200001 & ","
                ElseIf node.Value > 300001 Then
                    menuids &= node.Value - 300001 & ","
                End If



            Next
            menuids = menuids.Remove(menuids.Length - 1, 1)
            updateMenu(ddl_user_list.SelectedValue, menuids, 1)
        Else
            message.InnerHtml = "No items selected."
        End If
    End Sub

    Private Sub GetUserRole()
        Dim Sql = "EXEC Users.usp_UserRole @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.DataSource = ds.Tables(0)
            ddlUserRole.DataValueField = "UserRoleId"
            ddlUserRole.DataTextField = "UserRole"
            ddlUserRole.DataBind()
            ddlUserRole.Items.Insert(0, New ListItem("-- Select a user type -- ", ""))
        Else
            ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
        End If
    End Sub

    Private Sub GetUserByRoleID(ByVal RoleID)
        Dim Sql = "EXEC users.usp_menu_to_user @UserRoleID='" & RoleID & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables.Count > 0 Then
            ddl_user_list.DataSource = ds.Tables(0)
            ddl_user_list.DataValueField = "UserID"
            ddl_user_list.DataTextField = "UserName"
            ddl_user_list.DataBind()
            ddl_user_list.Items.Insert(0, New ListItem("-- Select a user  -- ", ""))
        Else
            ddl_user_list.Items.Insert(0, New ListItem("-- No Record Found --", ""))
        End If
    End Sub

    Protected Sub ddlUserRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlUserRole.SelectedIndexChanged
        GetUserByRoleID(ddlUserRole.SelectedValue)
    End Sub

    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_save.Click
        PopulateRootLevel()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        save()
    End Sub
End Class