﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class UserMenu

    '''<summary>
    '''message control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents message As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlUser As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlUserRole control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlUserRole As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''grdUserMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdUserMenu As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''listData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents listData As Global.System.Web.UI.HtmlControls.HtmlGenericControl
End Class
