﻿Public Class UserRoleDefaultMenu
    Inherits System.Web.UI.Page


    Private ReadOnly Dao As New DatabaseDao
    Private sql As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            'Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            'Response.Redirect(redirectUrl)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetUserRole()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        SaveRoleDefaultMenu(ddlUserRole.SelectedValue)
    End Sub

    Protected Sub ddlUserRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlUserRole.SelectedIndexChanged
        GetMenuByUserRole(ddlUserRole.SelectedValue)
    End Sub


    Private Sub GetUserRole()
        sql = "EXEC Users.usp_UserRole @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.DataSource = ds.Tables(0)
            ddlUserRole.DataValueField = "UserRoleId"
            ddlUserRole.DataTextField = "UserRole"
            ddlUserRole.DataBind()
            ddlUserRole.Items.Insert(0, New ListItem("-- Select a user type -- ", ""))
        Else
            ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
        End If
    End Sub


    Private Sub GetMenuByUserRole(ByVal userRoleId As String)
        sql = "EXEC [Users].[usp_Menu] @Flag = 'roleMenu', @UserRoleID = '" & userRoleId & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            grdDefaultMenu.DataSource = ds.Tables(0)
            grdDefaultMenu.DataBind()
        End If
    End Sub


    Private Sub SaveRoleDefaultMenu(ByVal userRoleId As String)
        message.InnerHtml = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        For i As Integer = 0 To grdDefaultMenu.Rows.Count - 1
            Dim chkMenu As CheckBox = TryCast(grdDefaultMenu.Rows(i).FindControl("chkSelectMenu"), CheckBox)
            Dim isActive As Boolean = chkMenu.Checked
            Dim menuId As String = grdDefaultMenu.Rows(i).Cells(6).Text.ToString()
            sql = "EXEC Users.usp_DefaultMenu @Flag='i', @UserRoleID = '" & userRoleId & "', @MenuID = '" & menuId & "', @IsActive = '" & isActive & "'"
            ds.Clear()
            ds = Dao.ExecuteDataset(sql)
        Next
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
                    sb.AppendLine("<div class='note note-success'>")
                Else
                    sb.AppendLine("<div class='note note-danger'>")
                End If
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString())
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            End If

    End Sub

End Class