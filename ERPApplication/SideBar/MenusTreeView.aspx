﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="MenusTreeView.aspx.vb" Inherits="School.MenusTreeView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {

            $(".MyTreeView").find(":checkbox").change(function () {
                //check or uncheck childs
                var nextele = $(this).closest("table").next()[0];
                if (nextele && nextele.tagName == "DIV") {
                    $(nextele).find(":checkbox").prop("checked", $(this).prop("checked"));

                }
                //check nodes all with the recursive method
                CheckChildNodes($(".MyTreeView").find(":checkbox").first());

            });
            //method check filial nodes
            function CheckChildNodes(Parentnode) {

                var nextele = $(Parentnode).closest("table").next()[0];

                if (nextele && nextele.tagName == "DIV") {
                    $(nextele).find(":checkbox").each(function () {
                        CheckChildNodes($(this));
                    });

                    if ($(nextele).find("input:checked").length == 0) {
                        $(Parentnode).removeAttr("checked");
                    }
                    if ($(nextele).find("input:checked").length > 0) {
                        $(Parentnode).prop("checked", "checked");
                    }

                }
                else { return; }

            }

        });
        //        $(document).ready(function () {
        //            $("#MainContent_btn_save").live('click', function () {
        //                alert('a');
        //                $("#form").removeClass("modal");
        //            });
        //        });

        //        window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {

        //            $("#MainContent_btn_save").live('click', function () {
        //                alert('a');
        //                $("#form").removeClass("modal");
        //            });
        //        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                   Menus Tree View Setup<small>Assign menus to users by role.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/SideBar/MenusTreeView.aspx">Menus Tree View</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Menus Tree view</div>
                        <div class="tools">
                            <a href="" class="collapse"></a><a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                User Role :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlUserRole" runat="server" CssClass="form-control" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%=ddlUserRole.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <%--      </div>
                                 <div class="form-group">--%>
                                            <label class="col-md-2 control-label">
                                                User :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddl_user_list" runat="server" CssClass="form-control" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%=ddlUserRole.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="form-group">
                                    <div class="col-md-2">
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="btn_save" runat="server" Text="Get Menu List" class="btn blue" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">
                                    </label>
                                    <div class="col-md-10 pull-center">
                                        <label class="col-md-2 control-label">
                                            Menus Chart
                                        </label>
                                        <br />
                                        <asp:TreeView ID="TreeView1" runat="server" ImageSet="Arrows" NodeIndent="15" ShowLines="True"
                                            ShowExpandCollapse="True" ExpandDepth="12" ShowCheckBoxes="All" CssClass="MyTreeView">
                                            <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
                                            <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                                                NodeSpacing="0px" VerticalPadding="2px" ChildNodesPadding="10"></NodeStyle>
                                            <ParentNodeStyle Font-Bold="true" />
                                            <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px"
                                                VerticalPadding="0px" />
                                        </asp:TreeView>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="btnSubmit" class="btn purple" runat="server" Text="Submit" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
