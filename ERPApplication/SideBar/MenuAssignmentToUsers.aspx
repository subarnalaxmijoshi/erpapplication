﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="MenuAssignmentToUsers.aspx.vb" Inherits="School.MenuAssignmentToUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"
        rel="stylesheet">
      <link href="../../assets/datatableplugins/css/buttons.dataTables.min.css" rel="stylesheet"
        type="text/css" />

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>

    <script src="../../assets/datatableplugins/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

         window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {

             $(document).on('change', '.assign', function () {
                var thisItem = $(this);
                var userid = $(this).attr("user-id");
                var menuid = $(this).attr("menu-id");
                var isactive = $(this).prop('checked');
//                alert(isactive);
//                alert(userid);
//                alert(menuid);
                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/UpdateMenuID',
                    data: '{"MenuID" : "' + menuid + '","UserID" : "' + userid + '","isActive" : "' + isactive + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        thisItem.html(newTheory);

                        // location.href = '/Exam/EditObtainedMark.aspx';

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });

            });
            });
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
//            $(document).on('click', ".menutitle").click(function () {
            $(document).on('click', '.menutitle', function () {
                var thisItem = $(this);
                var username = $(this).html();
                var moduleid = $(this).attr("module-id");


                //                alert(username);
                //                alert(moduleid);
                var newTheory = prompt("Enter 0 or 1 for active or deactive menus", "0");
                console.log(newTheory);
                if (newTheory === null || newTheory === false) {
                    console.log("Field is empty");
                    return;
                } else {

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/UpdateMenuByModuleID',
                        data: '{"UserName" : "' + username + '","isActive" : "' + newTheory + '","ModuleID" : "' + moduleid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            // thisItem.html(newTheory);
                            alert("Please wait... Page is going to reload.");
                            location.href = '/SideBar/MenuAssignmentToUsers.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }

            });
        });
    </script>
     <script type="text/javascript">
         $(document).ready(function () {


             function initDataTable() {
                 return $("table.menutable").DataTable({
                     "retrieve": true,
                     dom: 'Bfrtip',
                     "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     "ordering": false,
                     buttons: [
                        {
                            extend: 'colvis',
                            postfixButtons: ['colvisRestore'],
                            collectionLayout: 'fixed four-column',
                            columns: ':not(.noVis)'
                        }, 'pageLength'
                        ]
                 });
             }

             $(document).on('click', '.accordion-toggle', function () {
                 //             $('.accordion-toggle').on('click', function () {
                 var thisItem = $(this);
                 var old = $(this).html();
                 var moduleid = $(this).attr("data-id");
                 thisItem.html("Loading...");
                 $.ajax({
                     type: 'POST',
                     url: '/SetupServices.svc/GetMenusByModuleID',
                     data: '{"ModuleID" : "' + moduleid + '"}',
                     dataType: 'json',
                     contentType: 'application/json',
                     success: function (data) {

                         $("#panel_" + moduleid).html(data);
                        
                         $("[data-toggle='toggle']").bootstrapToggle('destroy');
                         $("[data-toggle='toggle']").bootstrapToggle();
                         thisItem.html(old);

                         var table = initDataTable();
                         //                         $("table.menutable").dataTable().fnClearTable();
                         //                         $("table.menutable").dataTable().fnDestroy();
                         //                         $("table.menutable").DataTable({});


                     },
                     error: function (data) {
                         alert(data.statusText);
                     },
                     failure: function (data) {
                         alert(data.statusText);
                     }

                 });
             });

         });
      </script>
        <script type="text/javascript">
//            $(document).ready(function () {
//                $('#displaytable').DataTable({
//                   
//                    
//                });
//            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Menu Assignment To Users <small>Assign menus to users.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/SideBar/MenuAssignmentToUsers.aspx">Menu Assignment</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
           
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Menu Assignment</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel-group accordion" id="accordion1" runat="server">
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
</asp:Content>
