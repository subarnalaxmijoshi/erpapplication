﻿Public Class MenuAssignmentToUsers
    Inherits System.Web.UI.Page

    Private ReadOnly Dao As New DatabaseDao
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetAssignDetails()
        End If
    End Sub

    Private Sub GetAssignDetails()
        Dim sql, sql2 As String
        Dim ds, ds2 As New DataSet
        Dim sb As New StringBuilder
        sql = String.Empty
        sql2 = String.Empty

        sql = "exec [Users].[usp_AssignMenusToUser] @flag='loadmodule',@UserID='" & Session("userID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1


                Dim collapseID As String = "collapse_" & (i + 1).ToString
                Dim moduleName As String = ds.Tables(0).Rows(i).Item("ModuleName").ToString.Trim
                Dim moduleId As String = ds.Tables(0).Rows(i).Item("ModuleID").ToString.Trim

                sb.AppendLine("<div class='panel panel-default'>")
                sb.AppendLine("<div class='panel-heading'>")
                sb.AppendLine("<h4 class='panel-title'>")
                sb.AppendLine("<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion1' href='#" & collapseID & "' data-id='" & moduleId & "'>")
                sb.AppendLine(moduleName)
                sb.AppendLine("</a></h4></div>")
                sb.AppendLine("<div id='" & collapseID & "' class='panel-collapse collapse'>")
                sb.AppendLine("<div class='panel-body' id='panel_" & moduleId & "'>")

                'sql2 = "exec [Users].[usp_AssignMenusToUser] @flag='userpermission',@ModuleId='" & moduleId & "'"
                'ds2 = Dao.ExecuteDataset(sql2)
                'If ds2.Tables(0).Rows.Count > 0 Then
                '    sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed' cellspacing='0' width='80%'>")
                '    sb.AppendLine("<thead class='flip-content'>")
                '    sb.AppendLine("<tr>")
                '    sb.AppendLine("<th>SN</th>")
                '    For c As Integer = 0 To ds2.Tables(0).Columns.Count - 1
                '        sb.AppendLine("<th class='text-center'><a  href='javascript:;' module-id='" & moduleId & "' class='menutitle' >" & ds2.Tables(0).Columns(c).ColumnName.ToString & "</a></th>")
                '    Next
                '    sb.AppendLine("</tr>")
                '    sb.AppendLine("</thead>")
                '    sb.AppendLine("<tbody>")

                '    For x As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                '        sb.AppendLine("<tr>")
                '        sb.Append("<td class='text-center'>" & x + 1 & "</td>")
                '        For y As Integer = 0 To ds2.Tables(0).Columns.Count - 1
                '            If y > 0 Then

                '                If Not String.IsNullOrWhiteSpace(ds2.Tables(0).Rows(x).Item(y).ToString()) Then
                '                    Dim arrIsActive As String() = ds2.Tables(0).Rows(x).Item(y).ToString().Split("/")
                '                    If arrIsActive(2) = "1" Then
                '                        sb.AppendLine("<td class='text-center'><input type='checkbox' class='assign' checked  data-toggle='toggle' data-size='mini' user-id='" & arrIsActive(0).ToString & "' menu-id='" & arrIsActive(1).ToString & "'></td>")
                '                    Else
                '                        sb.AppendLine("<td class='text-center'><input type='checkbox' class='assign' data-toggle='toggle' data-size='mini'  user-id='" & arrIsActive(0).ToString & "' menu-id='" & arrIsActive(1).ToString & "'></td>")
                '                    End If

                '                Else
                '                    ' sb.AppendLine("<td class='text-center'><input type='checkbox' id='toggle-trigger'  data-toggle='toggle' data-size='mini'  user-id='" & arrIsActive(0).ToString & "' menu-id='" & arrIsActive(1).ToString & "'></td>")
                '                End If
                '            Else
                '                sb.AppendLine("<td class='text-left'>" & ds2.Tables(0).Rows(x).Item(y).ToString().Trim & "</td>")
                '            End If

                '        Next
                '        sb.AppendLine("</tr>")
                '    Next
                '    sb.AppendLine("</tbody>")
                '    sb.AppendLine("</table>")


                'End If

                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")

            Next
            accordion1.InnerHtml = sb.ToString()
           
        End If

    End Sub

   
End Class