﻿Public Class Menu
    Inherits System.Web.UI.Page

    Private ReadOnly Dao As New DatabaseDao


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim currentUri = Request.Url.AbsolutePath
        'Dim pa As New PageAuthority
        'Dim uid As String
        'If (Session("userId") <> Nothing) Then
        '    uid = Session("userID").ToString()
        'Else
        '    'Response.Redirect("/Logout.aspx")
        'End If
        'Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        'If Not hasRightToView Then
        '    Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
        '    'Response.Redirect(redirectUrl)
        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetUserRole()
            If Request.QueryString("MenuId") <> Nothing Then
                Dim key = Request.QueryString("MenuId")
                GetById(key)
            ElseIf Request.QueryString("DeleteMenuId") <> Nothing Then
                Dim key = Request.QueryString("DeleteMenuId")
                Delete(key)
            End If

            GetAll()
        End If
    End Sub

    Protected Sub BtnSubmitClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim opt As Boolean
        If (rdoUnderModule.Checked) Then
            opt = True
        Else
            opt = False
        End If
        If btnSubmit.Text.ToLower() = "update" Then
            If (Request.QueryString("MenuId") <> Nothing) Then
                Dim id = Request.QueryString("MenuId").ToString()

                Update(ddlModule.SelectedValue, txtMenuName.Text, txtMenuLink.Text, txtMenuClass.Text, id, opt, ddlSubModule.SelectedValue)
            End If
        Else
            Save(ddlModule.SelectedValue, txtMenuName.Text, txtMenuLink.Text, txtMenuClass.Text, opt, ddlSubModule.SelectedValue)
        End If
        GetAll()
    End Sub

    Protected Sub ddlUserRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlUserRole.SelectedIndexChanged
        GetModuleByUser(ddlUserRole.SelectedValue)
    End Sub

    Private Sub GetUserRole()
        Dim sql As String = ""
        sql = "EXEC Users.usp_UserRole @Flag ='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlUserRole.DataSource = ds.Tables(0)
                ddlUserRole.DataValueField = "UserRoleID"
                ddlUserRole.DataTextField = "UserRole"
                ddlUserRole.DataBind()
                ddlUserRole.Items.Insert(0, New ListItem("-- Select a User Role --", ""))
            Else
                ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        End If
    End Sub

    Private Sub GetModuleByUser(ByVal userRole As String)
        Dim sql As String = ""
        sql = "EXEC [Users].[usp_Module] @Flag='f', @UserRoleID='" & userRole & "'"
        Dim ds As New DataSet
        ddlModule.Items.Clear()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlModule.DataSource = ds.Tables(0)
                ddlModule.DataValueField = "ModuleID"
                ddlModule.DataTextField = "ModuleName"
                ddlModule.DataBind()
                ddlModule.Items.Insert(0, New ListItem("-- Select a Module Role --", ""))
            Else
                ddlModule.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        End If
    End Sub

    Private Sub GetSubModuleByModule(ByVal moduleId As String)
        Dim sql As String = ""
        sql = "EXEC [Users].[usp_SubModule] @Flag='f', @ModuleID='" & moduleId & "'"
        Dim ds As New DataSet
        ddlSubModule.Items.Clear()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSubModule.DataSource = ds.Tables(0)
                ddlSubModule.DataValueField = "SubModuleID"
                ddlSubModule.DataTextField = "SubModuleName"
                ddlSubModule.DataBind()
                ddlSubModule.Items.Insert(0, New ListItem("-- Select SubModule --", ""))
            Else
                ddlSubModule.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        End If
    End Sub


    Private Sub Save(ByVal moduleId As String, ByVal menuText As String, ByVal menuLink As String, ByVal menuClass As String,
                     ByVal keepUnderModule As Boolean, ByVal subModuleId As String)
        Dim sql As String = ""
        If keepUnderModule Then
            sql = "EXEC [Users].[usp_Menu] @Flag='i', @ModuleId='" & moduleId & "', @MenuLink='" & menuLink &
               "', @MenuText='" & menuText & "', @MenuClass='" & menuClass & "'"
        Else
            sql = "EXEC [Users].[usp_Menu] @Flag='i', @ModuleId='" & moduleId & "', @MenuLink='" & menuLink &
               "', @MenuText='" & menuText & "', @MenuClass='" & menuClass & "', @SubModuleId = '" & subModuleId & "'"
        End If
        sql += ",@Visibility='" & chkIsActive.Checked & "',@Order='" & Dao.FilterQuote(txtOrder.Text.Trim) & "'"

        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode") = "1" Then
                sb.AppendLine("<div class='note note-success'>")
                GetAll()
            Else
                sb.AppendLine("<div class='note note-danger'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub Update(ByVal moduleId As String, ByVal menuText As String, ByVal menuLink As String, ByVal menuClass As String,
                       ByVal menuId As String, ByVal keepUnderModule As Boolean, ByVal subModuleId As String)
        Dim sql As String = ""
        If keepUnderModule Then
            sql = "EXEC [Users].[usp_Menu] @Flag='u', @ModuleId='" & moduleId & "', @MenuLink='" & menuLink &
               "', @MenuText='" & menuText & "', @MenuClass='" & menuClass & "', @MenuId ='" & menuId & "'"
        Else
            sql = "EXEC [Users].[usp_Menu] @Flag='u', @ModuleId='" & moduleId & "', @MenuLink='" & menuLink &
               "', @MenuText='" & menuText & "', @MenuClass='" & menuClass & "', @SubModuleId = '" & subModuleId & "', @MenuId ='" & menuId & "'"
        End If
        sql += ",@Visibility='" & chkIsActive.Checked & "',@Order='" & Dao.FilterQuote(txtOrder.Text.Trim) & "'"


        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode") = "1" Then
                btnSubmit.Text = "Submit"
                sb.AppendLine("<div class='note note-success'>")
                GetAll()
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub Delete(ByVal menuId As String)
        Dim sql As String = ""
        sql = "EXEC [Users].[usp_Menu] @Flag='d', @MenuId='" & menuId & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ErrorCode") = "1" Then
                btnSubmit.Text = "Submit"
                sb.AppendLine("<div class='note note-success'>")
                GetAll()
            Else
                sb.AppendLine("<div class='note note-success'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes"))
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Private Sub GetAll()
        Dim sql As String = ""
        sql = "EXEC [Users].[usp_Menu] @Flag='s'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>S.N</th>")
            sb.AppendLine("<th>User Role</th>")
            sb.AppendLine("<th>Module</th>")
            sb.AppendLine("<th>Sub Module</th>")
            sb.AppendLine("<th>Menu Text</th>")
            sb.AppendLine("<th>Menu Link</th>")
            sb.AppendLine("<th>Class</th>")
            sb.AppendLine("<th>IsActive</th>")
            sb.AppendLine("<th>Menu Order</th>")
            sb.AppendLine("<th>Actions</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim id = ds.Tables(0).Rows(i).Item("MenuId").ToString
                Dim userRole = ds.Tables(0).Rows(i).Item("UserRole").ToString
                Dim moduleName = ds.Tables(0).Rows(i).Item("ModuleName").ToString
                Dim submoduleName = ds.Tables(0).Rows(i).Item("SubModuleName").ToString
                Dim menuText = ds.Tables(0).Rows(i).Item("MenuText").ToString
                Dim menuLink = ds.Tables(0).Rows(i).Item("MenuLink").ToString
                Dim menuClass = ds.Tables(0).Rows(i).Item("MenuClass").ToString
                Dim menuVisibility = Convert.ToBoolean(ds.Tables(0).Rows(i).Item("Visibility").ToString)
                Dim menuOrder = ds.Tables(0).Rows(i).Item("MenuOrder").ToString

                sb.AppendLine("<tr>")
                sb.Append("<td class='text-center'>" & i + 1 & "</td>")
                sb.AppendLine("<td class='text-center'>" & userRole & "</td>")
                sb.AppendLine("<td class='text-center'>" & moduleName & "</td>")
                sb.AppendLine("<td class='text-center'>" & submoduleName & "</td>")
                sb.AppendLine("<td>" & menuText & "</td>")
                sb.AppendLine("<td>" & menuLink & "</td>")
                sb.AppendLine("<td>" & menuClass & "<span style='padding-left: 10px;' class='" & menuClass & "'</span></td>")
                sb.AppendLine("<td class='text-center'>" & menuVisibility & "</td>")
                sb.AppendLine("<td class='text-center'>" & menuOrder & "</td>")
                sb.AppendLine("<td class='text-center'><a href='?MenuId=" & id & "'> Edit </a> | ")
                sb.AppendLine("<a href='?DeleteMenuId=" & id & "'> Delete </a></td>")
                sb.AppendLine("</tr>")
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            listData.InnerHtml = sb.ToString()
        End If
    End Sub

    Private Sub GetById(ByVal id As String)
        Dim sql As String = ""
        sql = "EXEC [Users].[usp_Menu] @Flag='s', @MenuId='" & id & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.SelectedValue = ds.Tables(0).Rows(0).Item("UserRoleID")
            GetModuleByUser(ddlUserRole.SelectedValue)
            ddlModule.SelectedValue = ds.Tables(0).Rows(0).Item("ModuleId")
            GetSubModuleByModule(ddlModule.SelectedValue)
            ddlSubModule.SelectedValue = ds.Tables(0).Rows(0).Item("SubModuleId")
            txtMenuName.Text = ds.Tables(0).Rows(0).Item("MenuText")
            txtMenuLink.Text = ds.Tables(0).Rows(0).Item("MenuLink")
            txtMenuClass.Text = ds.Tables(0).Rows(0).Item("MenuClass")
            chkIsActive.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("Visibility").ToString.Trim)
            txtOrder.Text = ds.Tables(0).Rows(0).Item("MenuOrder")
            btnSubmit.Text = "Update"
        End If
    End Sub


    Protected Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlModule.SelectedIndexChanged
        GetSubModuleByModule(ddlModule.SelectedValue)
    End Sub
End Class