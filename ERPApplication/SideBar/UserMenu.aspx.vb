﻿Public Class UserMenu
    Inherits System.Web.UI.Page

    Private ReadOnly Dao As New DatabaseDao
    Private sql As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            'Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            'Response.Redirect(redirectUrl)
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetUserRole()
            GetUsers()
            GetUserMenu()

            Dim currentUri = Request.Url.AbsolutePath
            Response.Write(currentUri)
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        SaveMeu(ddlUser.SelectedValue.ToString())
    End Sub

    Private Sub GetUserRole()
        sql = "EXEC Users.usp_UserRole @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.DataSource = ds.Tables(0)
            ddlUserRole.DataValueField = "UserRoleId"
            ddlUserRole.DataTextField = "UserRole"
            ddlUserRole.DataBind()
            ddlUserRole.Items.Insert(0, New ListItem("-- Select a user type -- ", ""))
        Else
            ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
        End If
    End Sub
    
    Private Sub GetUsers()
        sql = "EXEC Management.usp_UserInfo @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlUser.DataSource = ds.Tables(0)
                ddlUser.DataValueField = "UserID"
                ddlUser.DataTextField = "FullName"
                ddlUser.DataBind()
            Else
                ddlUser.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        End If
    End Sub

 

    Private Sub GetUserMenu()
        sql = "EXEC [Users].[usp_UserMenu] @Flag='s'"
        listData.InnerHtml = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>User</th>")
                sb.AppendLine("<th>Module</th>")
                sb.AppendLine("<th>Sub-Module</th>")
                sb.AppendLine("<th>Menu Text</th>")
                sb.AppendLine("<th>Menu Link</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim id = ds.Tables(0).Rows(i).Item("MenuId").ToString
                    Dim userName = ds.Tables(0).Rows(i).Item("User").ToString
                    Dim moduleName = ds.Tables(0).Rows(i).Item("ModuleName").ToString
                    Dim subModuleName = ds.Tables(0).Rows(i).Item("SubModuleName").ToString
                    Dim menuText = ds.Tables(0).Rows(i).Item("MenuText").ToString
                    Dim menuLink = ds.Tables(0).Rows(i).Item("MenuLink").ToString
                    Dim menuClass = ds.Tables(0).Rows(i).Item("MenuClass").ToString


                    sb.AppendLine("<tr>")
                    sb.Append("<td class='numeric'>" & i + 1 & "</td>")
                    sb.AppendLine("<td>" & userName & "</td>")
                    sb.AppendLine("<td>" & moduleName & "</td>")
                    sb.AppendLine("<td>" & subModuleName & "</td>")
                    sb.AppendLine("<td>" & menuText & "<span style='padding-left: 10px;' class='" & menuClass & "'</span></td>")
                    sb.AppendLine("<td>" & menuLink & "</td>")
                    'sb.AppendLine("<td style='text-align:center'><a href='?MenuId=" & id & "'> Edit </a></td>")
                    'sb.AppendLine("<td style='text-align:center'><a href='?DeleteMenuId=" & id & "'> Delete </a></td>")

                    sb.AppendLine("</tr>")
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                listData.InnerHtml = sb.ToString()
            End If
        End If
    End Sub

    Private Sub SaveMeu(ByVal userId As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        For i As Integer = 0 To grdUserMenu.Rows.Count-1
            Dim chkIsGiven As CheckBox = TryCast(grdUserMenu.Rows(i).FindControl("chkIsActive"), CheckBox)
            Dim menuId = grdUserMenu.Rows(i).Cells(7).Text.ToString()
            sql = "EXEC [Users].[usp_UserMenu] @Flag='i', @UserID='" & userId & "', @MenuId='" & menuId & "', @IsActive = '" & chkIsGiven.Checked & "'"
            ds.Clear()
            ds = Dao.ExecuteDataset(sql)
        Next

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then

                sb.AppendLine("<div class='note note-success'>")
            Else
                sb.AppendLine("<div class='note note-danger'>")
            End If
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString())
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

        End If
    End Sub



    Protected Sub ddlUserRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlUserRole.SelectedIndexChanged
        GetMenuByUserRole(ddlUserRole.SelectedValue)
    End Sub

    Private Sub GetMenuByUserRole(ByVal userRoleId As String)
        sql = "EXEC [Users].[usp_Menu] @Flag = 'roleDefaultMenu', @UserRoleID = '" & userRoleId & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            grdUserMenu.DataSource = ds.Tables(0)
            grdUserMenu.DataBind()
        End If



    End Sub
End Class