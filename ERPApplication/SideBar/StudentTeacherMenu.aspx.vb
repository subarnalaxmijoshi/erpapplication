﻿Public Class StudentTeacherMenu
    Inherits System.Web.UI.Page

    Private ReadOnly Dao As New DatabaseDao
    Private sql As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            'Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            ' Response.Redirect(redirectUrl)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetMenu()
            GetUserRole()
            GetUserMenu()
        End If
    End Sub


    Private Sub GetMenu()
        sql = "EXEC Users.usp_Menu @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                chkMenuList.DataSource = ds.Tables(0)
                chkMenuList.DataValueField = "MenuID"
                chkMenuList.DataTextField = "MenuText"
                chkMenuList.DataBind()
            End If
        End If
    End Sub

    Private Sub GetUserRole()
        sql = "EXEC Users.usp_UserRole @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            ddlUserRole.DataSource = ds.Tables(0)
            ddlUserRole.DataValueField = "UserRoleId"
            ddlUserRole.DataTextField = "UserRole"
            ddlUserRole.DataBind()
            ddlUserRole.Items.Insert(0, New ListItem("-- Select a user type -- ", ""))
        Else
            ddlUserRole.Items.Insert(0, New ListItem("-- No Record Found --", ""))
        End If
    End Sub




    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkSelectAll.CheckedChanged
        If (chkSelectAll.Checked) Then
            SelectAll()
            chkSelectAll.Text = "Diselect"
        Else
            SelectAll(False)
            chkSelectAll.Text = "Select All Menu"
        End If
    End Sub

    Protected Sub chkMenuList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkMenuList.SelectedIndexChanged
        SelectSpecific()
    End Sub

    Sub SelectSpecific()
        selectedMenu.InnerHtml = "<ol>"
        For i As Integer = 0 To chkMenuList.Items.Count - 1
            If chkMenuList.Items(i).Selected Then
                selectedMenu.InnerHtml += "<li>" + chkMenuList.Items(i).Text + "</li>"
            End If
        Next
        selectedMenu.InnerHtml += "</ol>"
    End Sub

    Sub SelectAll(Optional ByVal isTrue As Boolean = True)

        If (isTrue) Then
            selectedMenu.InnerHtml = "<ol>"
            For i As Integer = 0 To chkMenuList.Items.Count - 1
                chkMenuList.Items(i).Selected = True
                selectedMenu.InnerHtml += "<li>" + chkMenuList.Items(i).Text + "</li>"
            Next
            selectedMenu.InnerHtml += "</ol>"
        Else
            For i As Integer = 0 To chkMenuList.Items.Count - 1
                chkMenuList.Items(i).Selected = False
            Next
            selectedMenu.InnerHtml = ""
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Save(ddlUserRole.SelectedValue)
        GetUserMenu()
    End Sub


    Private Sub Save(ByVal userRole As String)
        Dim ds As New DataSet
        For i As Integer = 0 To chkMenuList.Items.Count-1
            If chkMenuList.Items(i).Selected Then
                sql = "EXEC [Users].[usp_StudentAndTeacherMenu] @Flag='i', @UserRoleID = '" & userRole & "', @MenuID='" & chkMenuList.Items(i).Value & "'"

                ds = Dao.ExecuteDataset(sql)
            End If
        Next
        
        If ds.Tables.Count > 0 Then

        End If
    End Sub

    Private Sub GetUserMenu()
        sql = "EXEC  [Users].[usp_StudentAndTeacherMenu] @Flag='s'"
        listData.InnerHtml = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>User Role</th>")
                sb.AppendLine("<th>Module</th>")
                sb.AppendLine("<th>Sub-Module</th>")
                sb.AppendLine("<th>Menu Text</th>")
                sb.AppendLine("<th>Menu Link</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim id = ds.Tables(0).Rows(i).Item("SATID").ToString
                    Dim userName = ds.Tables(0).Rows(i).Item("UserRole").ToString
                    Dim moduleName = ds.Tables(0).Rows(i).Item("ModuleName").ToString
                    Dim subModuleName = ds.Tables(0).Rows(i).Item("SubModuleName").ToString
                    Dim menuText = ds.Tables(0).Rows(i).Item("MenuText").ToString
                    Dim menuLink = ds.Tables(0).Rows(i).Item("MenuLink").ToString
                    Dim menuClass = ds.Tables(0).Rows(i).Item("MenuClass").ToString


                    sb.AppendLine("<tr>")
                    sb.Append("<td class='numeric'>" & i + 1 & "</td>")
                    sb.AppendLine("<td>" & userName & "</td>")
                    sb.AppendLine("<td>" & moduleName & "</td>")
                    sb.AppendLine("<td>" & subModuleName & "</td>")
                    sb.AppendLine("<td>" & menuText & "<span style='padding-left: 10px;' class='" & menuClass & "'</span></td>")
                    sb.AppendLine("<td>" & menuLink & "</td>")
                    'sb.AppendLine("<td style='text-align:center'><a href='?MenuId=" & id & "'> Edit </a></td>")
                    'sb.AppendLine("<td style='text-align:center'><a href='?DeleteMenuId=" & id & "'> Delete </a></td>")

                    sb.AppendLine("</tr>")
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                listData.InnerHtml = sb.ToString()
            End If
        End If
    End Sub

End Class