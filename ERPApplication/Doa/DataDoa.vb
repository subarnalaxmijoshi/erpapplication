﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Security.Cryptography


Public Class DatabaseDao

    ''' <summary>
    ''' Make setup.MasterFiscalYear active and keep databasename same as web.config file key name. first login from current database or default database and check to active database
    ''' from setup.masterfiscalyear. in sql server there are many database of single organization.
    ''' </summary>
    ''' <remarks></remarks>
    'Dim LoginString As String = ConfigurationSettings.AppSettings(Global_Active_db_fy)

    'ReadOnly _connection As SqlConnection = New SqlConnection(LoginString)
    ' Dim FyString As String = ConfigurationSettings.AppSettings(Global_Active_db_fy)

    '  ReadOnly _connection As SqlConnection = New SqlConnection(LoginString)

    ReadOnly _connection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("connectionString"))

    ' Dim t As String = _connection.ConnectionString
    Private Sub OpenConnection()


        If _connection.State = ConnectionState.Open Then
            _connection.Close()
            _connection.Open()
        End If

    End Sub

    Private Sub CloseConnection()
        _connection.Close()
    End Sub

    Public Function ExecuteDataset(ByVal sql As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim da As SqlDataAdapter

        Try
            OpenConnection()
            da = New SqlDataAdapter(sql, _connection)
            da.Fill(ds)
            da.Dispose()
            CloseConnection()
        Catch ex As Exception
            Message.InnerHtml = ex.Message
        Finally
            CloseConnection()
        End Try

        Return ds

    End Function

    Public Function ExecuteDataTable(ByVal sql As String) As DataTable
        'dim dt as DataTable=new DataTable
        Dim ds As DataSet = New DataSet
        Dim da As SqlDataAdapter

        Try
            OpenConnection()
            da = New SqlDataAdapter(sql, _connection)
            da.Fill(ds)
            da.Dispose()
            CloseConnection()
        Catch ex As Exception
            Throw

        Finally
            CloseConnection()

        End Try

        Return ds.Tables(0)

    End Function
    Public Function ExecuteQuery(ByVal sql As String)

        Dim ds As DataSet = New DataSet
        Dim da As SqlDataAdapter

        Try
            OpenConnection()
            da = New SqlDataAdapter(sql, _connection)
            da.Fill(ds)
            da.Dispose()
            CloseConnection()
        Catch ex As Exception
            Throw

        Finally
            CloseConnection()

        End Try

    End Function

    Public Function FilterString(ByVal val As String) As String
        If String.IsNullOrWhiteSpace(Val) Then
            Return "''"
        End If
        val = val.ToUpper()
        Val = Val.Replace("&", "")
        'val = val.Replace("AND", "")
        'val = val.Replace("OR", "")
        Val = Val.Replace("SELECT", "")
        Val = Val.Replace("WHERE", "")
        Val = Val.Replace("HAVING", "")
        Val = Val.Replace("UNION", "")
        Val = Val.Replace("DROP", "")
        Val = Val.Replace("CREATE", "")
        Val = Val.Replace("DELETE", "")
        Val = Val.Replace("TRUNCATE", "")
        Val = Val.Replace("ALTER", "")
        Return "'" + val + "'"
    End Function
    Public Function FilterQuote(ByVal strVal As String) As [String]
        Dim str = If(strVal, "")
        If Not String.IsNullOrEmpty(str) Then
            str = str.Replace(";", "")
            'str = str.Replace(",", "")
            str = str.Replace("--", "")
            str = str.Replace("'", "''")
            str = str.Replace("&", "")
            'str = "'" + str + "'"
        Else
            str = ""
        End If
        Return str
    End Function

    Public Function ParseDbResult(ByVal sql As String) As DbResult
        Dim dr = ExecuteDataTable(sql).Rows(0)
        Return ParseDbResult(dr)
    End Function

    Public Function ParseDbResult(ByVal dr As DataRow) As DbResult
        Dim dbResult = New DbResult()
        dbResult.SetMessage(dr(0).ToString(), dr(1).ToString(), dr(2).ToString())
        If (dr.Table.Columns.Count > 3) Then
            dbResult.Extra = dr(3).ToString()
        End If
        Return dbResult
    End Function


#Region "Encryption"



    Public Function EncryptData(ByVal message As String) As String
        Dim results As Byte()
        Dim utf8 As UTF8Encoding = New UTF8Encoding()
        Dim hashProvider As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
        Dim tdesKey As Byte() = hashProvider.ComputeHash(utf8.GetBytes("password"))
        Dim tdesAlgorithm As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
        tdesAlgorithm.Key = tdesKey
        tdesAlgorithm.Mode = CipherMode.ECB
        tdesAlgorithm.Padding = PaddingMode.PKCS7
        Dim dataToEncrypt As Byte() = utf8.GetBytes(message)
        Try

            Dim encryptor As ICryptoTransform = tdesAlgorithm.CreateEncryptor()
            results = encryptor.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length)

        Finally

            tdesAlgorithm.Clear()
            hashProvider.Clear()
        End Try
        Return Convert.ToBase64String(results)
    End Function


    Public Function DecryptString(ByVal message As String) As String
        Dim results As Byte()
        Dim utf8 As UTF8Encoding = New UTF8Encoding()
        Dim hashProvider As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
        Dim tdesKey As Byte() = hashProvider.ComputeHash(utf8.GetBytes("password"))
        Dim tdesAlgorithm As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
        tdesAlgorithm.Key = tdesKey
        tdesAlgorithm.Mode = CipherMode.ECB
        tdesAlgorithm.Padding = PaddingMode.PKCS7
        Dim dataToDecrypt As Byte() = Convert.FromBase64String(message)
        Try
            Dim decryptor As ICryptoTransform = tdesAlgorithm.CreateEncryptor()
            results = decryptor.TransformFinalBlock(dataToDecrypt, 0, dataToDecrypt.Length)
        Finally
            tdesAlgorithm.Clear()
            hashProvider.Clear()
        End Try
        Return utf8.GetString(results)
    End Function


#End Region


End Class


Public Class DbResult

    Public ErrorCode As String = "1"
    Public Msg As String = ""
    Public ID As String = ""
    Public Extra As String = ""

    Public Sub SetMessage(ByVal errorCode As String, ByVal msg As String, ByVal id As String)
        Me.ErrorCode = errorCode
        Me.Msg = msg
        Me.ID = id
    End Sub
End Class
