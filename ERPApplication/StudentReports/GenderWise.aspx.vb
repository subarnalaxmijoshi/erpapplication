﻿Imports System.IO
Imports System.Web.HttpContext
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.DataVisualization.Charting
Imports System.Drawing

Public Class GenderWise
    Inherits System.Web.UI.Page
    Dim ds As New DataSet
    Private Dao As New DatabaseDao
    Dim sql As String = ""
    Dim i As Integer = 0

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            Response.Redirect(redirectUrl)
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
        Else
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrWhiteSpace(Session("userID")) And
            String.IsNullOrWhiteSpace(Session("username")) And
            String.IsNullOrWhiteSpace(Session("Role")) And
            String.IsNullOrWhiteSpace(Session("Branch")) Then
            Response.Redirect("/Login.aspx")
        End If


        If Not IsPostBack Then
            GetAcademicYear()
            dropAcademicYear.SelectedValue = Session("AcademicYearID").ToString.Trim
            FilterStudent()
            AllStudent()
        End If
    End Sub

    Private Sub GetAcademicYear()
        Dim sql As String
        dropAcademicYear.Items.Clear()
        sql = "exec [Setup].[usp_AcademicYear] @flag='s' ,@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropAcademicYear.DataSource = ds.Tables(0)
                dropAcademicYear.DataTextField = "AcademicTitle"
                dropAcademicYear.DataValueField = "AcademicYearId"
                dropAcademicYear.DataBind()
                dropAcademicYear.Items.Insert(0, New ListItem(" All Academic Year ", ""))

            Else
                dropAcademicYear.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        FilterStudent()
        AllStudent()
    End Sub

    Private Sub FilterStudent()
        sql = "exec [Students].[usp_Dashboard] @BranchID='" & Session("BranchID") & "',@AcademicYearID='" & dropAcademicYear.SelectedValue & "'"
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        message.InnerHtml = ""
        listData.InnerHtml = ""
        ds = Dao.ExecuteDataset(sql)
        If (ds.Tables(5).Rows.Count > 0) Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>Gender-wise Student Enrollment " & " " & dropAcademicYear.SelectedItem.Text & "</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")

            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            sb.AppendLine("<th>Batch</th>")
            sb.AppendLine("<th>Total</th>")
            sb.AppendLine("<th>Male</th>")
            sb.AppendLine("<th>Female</th>")
            sb.AppendLine("<th>Male(%)</th>")
            sb.AppendLine("<th>Female(%)</th>")
            sb.AppendLine("<th>Ratio(M:F)</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            Dim totalcount, tMale, tFemale As Integer
            Dim tMalePercent, tFemalePercent As Double
            For Me.i = 0 To ds.Tables(5).Rows.Count - 1

                Dim BatchTitle = ds.Tables(5).Rows(i).Item("BatchTitle").ToString
                Dim TotalStudents = ds.Tables(5).Rows(i).Item("TotalStudents").ToString
                Dim Male = ds.Tables(5).Rows(i).Item("Male").ToString
                Dim Female = ds.Tables(5).Rows(i).Item("Female").ToString
                Dim MaleStudentPercentage = ds.Tables(5).Rows(i).Item("MaleStudentPercentage").ToString
                Dim FemaleStudentPercentage = ds.Tables(5).Rows(i).Item("FemaleStudentPercentage").ToString
                Dim Ratio = ds.Tables(5).Rows(i).Item("Ratio").ToString
                totalcount += TotalStudents
                tMale += Male
                tFemale += Female
                tMalePercent += MaleStudentPercentage
                tFemalePercent += FemaleStudentPercentage

                sb.AppendLine("<tr>")
                sb.AppendLine("<td class='text-center'>" & (i + 1) & "</td>")
                sb.AppendLine("<td class='text-center'> " & BatchTitle & " </td>")
                sb.AppendLine("<td class='text-center'>" & TotalStudents & "</td>")
                sb.AppendLine("<td class='text-center'>" & Male & "</td>")
                sb.AppendLine("<td class='text-center'>" & Female & "</td>")
                sb.AppendLine("<td class='text-center'>" & MaleStudentPercentage & "</td>")
                sb.AppendLine("<td class='text-center'>" & FemaleStudentPercentage & "</td>")
                sb.AppendLine("<td class='text-center'>" & Ratio & "</td>")            
                sb.AppendLine("</tr>")
            Next
            sb.AppendLine("<tr><td  colspan='2' class='text-right bold'>Total</td><td class='text-center bold'>" & totalcount & "</td>")
            sb.AppendLine("<td class='text-center bold'>" & tMale & "</td>")
            sb.AppendLine("<td class='text-center bold'>" & tFemale & "</td>")
            sb.AppendLine("<td class='text-center bold'></td>")
            sb.AppendLine("<td class='text-center bold'></td>")
            sb.AppendLine("<td></td></tr>")
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            listData.InnerHtml = sb.ToString()


            'Dim x As String() = New String(ds.Tables(5).Rows.Count - 1) {}
            'Dim y As Integer() = New Integer(ds.Tables(5).Rows.Count - 1) {}
            'For i As Integer = 0 To ds.Tables(5).Rows.Count - 1
            '    x(i) = ds.Tables(5).Rows(i)(0).ToString()
            '    y(i) = Convert.ToInt32(ds.Tables(5).Rows(i)(1))
            'Next
            'Chart1.Series(0).Points.DataBindXY(x, y)
            'Chart1.Series(0).ChartType = SeriesChartType.Column
            ''Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True
            ''Chart1.Legends(0).Enabled = True

            'Draw Chart
            Dim dt As New DataTable
            dt = ds.Tables(5)
            Dim xValues As String() = New String(dt.Rows.Count - 1) {}
            Dim yValues As Integer() = New Integer(dt.Rows.Count - 1) {}
            Dim zValues As Integer() = New Integer(dt.Rows.Count - 1) {}
            For i As Integer = 0 To dt.Rows.Count - 1
                xValues(i) = dt.Rows(i)(0).ToString()
                yValues(i) = Convert.ToInt32(dt.Rows(i)(2))
                zValues(i) = Convert.ToInt32(dt.Rows(i)(3))
            Next
            Dim titleForChart As String = "Bar-Chart of Student Enrollment Gender-wise as of Academic Year " & dropAcademicYear.SelectedItem.Text
            CreateChart(SeriesChartType.StackedColumn, xValues, yValues, zValues, titleForChart, "Legends : Gender-wise")

        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Record Found for the search text")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If


    End Sub

    Public Sub CreateChart(ByVal chartType As SeriesChartType, ByVal xValues As String(), ByVal yValues As Integer(), ByVal zValues As Integer(), ByVal chartTitle As String, ByVal legendName As String)

        Dim seriesName As String = Nothing
        Chart1.Series.Clear()
        Chart1.Titles.Clear()

        

        Dim s1 As New Series
        Dim s2 As New Series
        Dim s3 As New Series
        With s1
            .ChartArea = "ChartArea1"
            .Name = "Male"
            .ChartType = SeriesChartType.StackedColumn
            .XValueType = ChartValueType.String
            .YValueType = ChartValueType.Int32
            .IsValueShownAsLabel = True
            With .Points
                ' Bind X and Y values
                .DataBindXY(xValues, yValues)
            End With
        End With
        With s2
            .ChartArea = "ChartArea1"
            .Name = "Female"
            .ChartType = SeriesChartType.StackedColumn
            .XValueType = ChartValueType.String
            .YValueType = ChartValueType.Int32
            .IsValueShownAsLabel = True
            With .Points
                .DataBindXY(xValues, zValues)
            End With
        End With
       
        Chart1.Series.Add(s1)
        Chart1.Series.Add(s2)




        ' Chart Area Modification (Optional)
        Dim CArea As ChartArea = Chart1.ChartAreas(0)
        CArea.BackColor = Color.WhiteSmoke
        CArea.ShadowColor = Color.Red
        CArea.Area3DStyle.Enable3D = True
        CArea.AxisX.Interval = 1
        CArea.AxisX.LabelStyle.Angle = -90

        ' Define Custom Chart Colors
        'Chart1.Series(seriesName).Points(0).Color = Color.MediumSeaGreen
        'Chart1.Series(seriesName).Points(1).Color = Color.PaleGreen
        'Chart1.Series(seriesName).Points(2).Color = Color.LawnGreen

        ' Define Chart Type
        'Chart1.Series(seriesName).ChartType = chartType

        Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True

        ' Formatting the Chart Title
        Dim T As Title = Chart1.Titles.Add(chartTitle)
        With T
            .ForeColor = Color.Black
            .BackColor = Color.GhostWhite
            .Font = New System.Drawing.Font("Verdana", 10.0F, System.Drawing.FontStyle.Bold)
            '.BorderColor = Color.Black
            .Alignment = ContentAlignment.MiddleCenter
            .Docking = Docking.Bottom
        End With

        ' If you want to show Chart Legends
        Chart1.Legends(0).Enabled = True
        Chart1.Legends(0).Title = legendName
        Chart1.Legends(0).BackColor = Color.WhiteSmoke

        ' If you don't want to show data values and headings as label inside each Pie in chart
        'Chart1.Series(seriesName)("PieLabelStyle") = "Disabled"
        'Chart1.Series(seriesName).IsValueShownAsLabel = False

        ' If you want to show datavalues as label inside each Pie in chart
        'Chart1.Series(seriesName).IsValueShownAsLabel = True

    End Sub

    Private Sub AllStudent()
        sql = "exec [Students].[usp_Dashboard] @BranchID='" & Session("BranchID") & "',@AcademicYearID='" & dropAcademicYear.SelectedValue & "'"
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        message.InnerHtml = ""
        Div1.InnerHtml = ""
        ds = Dao.ExecuteDataset(sql)
        If (ds.Tables(6).Rows.Count > 0) Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>Over-All Student Enrollment " & " " & dropAcademicYear.SelectedItem.Text & "</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")

            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            'sb.AppendLine("<th>Batch</th>")
            sb.AppendLine("<th>Total Students</th>")
            sb.AppendLine("<th>Male</th>")
            sb.AppendLine("<th>Female</th>")
            sb.AppendLine("<th>Male(%)</th>")
            sb.AppendLine("<th>Female(%)</th>")
            sb.AppendLine("<th>Ratio(M:F)</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            For Me.i = 0 To ds.Tables(6).Rows.Count - 1

                Dim TotalStudents = ds.Tables(6).Rows(i).Item("TotalStudents").ToString
                Dim Male = ds.Tables(6).Rows(i).Item("Male").ToString
                Dim Female = ds.Tables(6).Rows(i).Item("Female").ToString

                Dim MaleStudentPercentage = ds.Tables(6).Rows(i).Item("MaleStudentPercentage").ToString
                Dim FemaleStudentPercentage = ds.Tables(6).Rows(i).Item("FemaleStudentPercentage").ToString
                Dim Ratio = ds.Tables(6).Rows(i).Item("Ratio").ToString

                sb.AppendLine("<tr>")
                sb.AppendLine("<tr><td class='numeric'>" & (i + 1) & "</td>")
                sb.AppendLine(" <td class='numeric'><a href='/StudentReports/GenderWise.aspx'> </a> " & TotalStudents & " </td>")


                sb.AppendLine("<td>" & Male & "</td>")
                sb.AppendLine("<td>" & Female & "</td>")
                sb.AppendLine("<td>" & MaleStudentPercentage & "</td>")
                sb.AppendLine("<td>" & FemaleStudentPercentage & "</td>")
                sb.AppendLine("<td>" & Ratio & "</td>")

                sb.AppendLine("</tr>")
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            Div1.InnerHtml = sb.ToString()


        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Record Found for the search text")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            Div1.InnerHtml = ""
        End If


    End Sub




    Private Sub StudentDetail(ByVal studentID As String)



    End Sub

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Gender_Wise_Enrollment" & "_" & dropAcademicYear.SelectedItem.Text)
    End Sub
End Class