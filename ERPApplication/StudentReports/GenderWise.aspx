﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="GenderWise.aspx.vb" Inherits="School.GenderWise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function print(divId) {
        var contents = $("#" + divId).html();
        var win = window.open("", "", "height=500,width=900");
        win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
        win.document.write("</head><body >");
        win.document.write(contents);
        win.document.write("</body></html>");
        win.print();
        win.close();
        return true;
    }
    $(document).ready(function () {
        $("#toExcel").click(function () {
            $("#<%= btnToExcel.ClientID %>").click();
        });
    });
    </script>
  <%--  <script type="text/javascript">
        var checkFromValidation = false;
        $(document).ready(function () {
            $("#<%=lblLoading.ClientID %>").hide();

            $('#form').validate();

            $("#<%=dropAcademicYear.ClientID %>").rules('add', { required: true, messages: { required: 'Please select any one academic year.'} });

            checkFromValidation = function () {
                $("#<%=lblLoading.ClientID %>").show();
                $("#<%=btnSearch.ClientID %>").hide();
                var bool = true;
                if ($('#<%=dropAcademicYear.ClientID %>').valid() == false) bool = false;
                if (!bool) {
                    $("#<%=lblLoading.ClientID %>").hide();
                    $("#<%=btnSearch.ClientID %>").show();

                    $('#form').validate().focusInvalid();
                }
                return bool;
            };
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Gender-wise Student Enrollment <small>Number of the students on the basis of Gender</small></h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/StudentReports/GenderWise.aspx">Gender-Wise Student Enrollment</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Filter Report</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="Academic Year" class="col-md-2 control-label">
                                        Academic Year :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:DropDownList ID="dropAcademicYear" runat="server" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Button ID="btnSearch" CssClass="btn purple" OnClientClick="return checkFromValidation();"
                                            runat="server" Text="Show" />
                                       <%-- <asp:Label ID="lblLoading" runat="server" Text="Loading ...." class="btn purple"
                                            disabled></asp:Label>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="listData" runat="server">
                </div>
                <div id="Div1" runat="server"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Bar-Chart : Gender-wise Student Enrollment</div>
                        <div class="tools">
                            <a href="" class="collapse"></a><a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="clearfix">
                                    <div class="col-md-12">
                                        <div id="Div3" runat="server" align="left">
                                            <asp:Chart ID="Chart1" runat="server" BackColor="WhiteSmoke" Height="495px" Width="1096px">
                                                <Series>
                                                    <asp:Series Name="Series1">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1">
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                                <Legends>
                                                    <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                                        LegendStyle="Row" />
                                                </Legends>
                                            </asp:Chart>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" style="display:none;" />
</asp:Content>
