﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ChartsDashboard.aspx.vb" Inherits="School.ChartsDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--    <script src="Scripts/highcharts/highcharts.js" type="text/javascript"></script>
    <script src="Scripts/highcharts/modules/exporting.js" type="text/javascript"></script>--%>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<style type="text/css">
   .chartcontainer
   {
       width:100%; 
       height:200px;
      <%-- padding-top: 30px;--%>
   }
    .highcharts-figure, .highcharts-data-table table {
    min-width: 230px; 
    max-width: 800px;
    margin: 0.1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 0.1px 0;
    font-size: 0.1px;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}


input[type="number"] {
	min-width: 30px;
}

.highcharts-title {
    font-size: 14px ! important;
}
.boxes
{
    padding-bottom:50px;
    }
</style>
     
     <script>
//     import Highcharts from '../parts/Globals.js';
//Highcharts.createElement('link', {
//    href: 'https://fonts.googleapis.com/css?family=Unica+One',
//    rel: 'stylesheet',
//    type: 'text/css'
//}, null, document.getElementsByTagName('head')[0]);
Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
        '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
        backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
            stops: [
                [0, '#2a2a2b'],
                [1, '#3e3e40']
            ]
        },
        style: {
            fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063'
    },
    title: {
        style: {
            color: '#E0E0E3',
            textTransform: 'uppercase',
            fontSize: '20px'
        }
    },
    subtitle: {
        style: {
            color: '#E0E0E3',
            textTransform: 'uppercase'
        }
    },
    xAxis: {
        gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
            style: {
                color: '#A0A0A3'
            }
        }
    },
    yAxis: {
        gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
            style: {
                color: '#A0A0A3'
            }
        }
    },
    tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
            color: '#F0F0F0'
        }
    },
    plotOptions: {
        series: {
            dataLabels: {
                color: '#F0F0F3',
                style: {
                    fontSize: '13px'
                }
            },
            marker: {
                lineColor: '#333'
            }
        },
        boxplot: {
            fillColor: '#505053'
        },
        candlestick: {
            lineColor: 'white'
        },
        errorbar: {
            color: 'white'
        }
    },
    legend: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        itemStyle: {
            color: '#E0E0E3'
        },
        itemHoverStyle: {
            color: '#FFF'
        },
        itemHiddenStyle: {
            color: '#606063'
        },
        title: {
            style: {
                color: '#C0C0C0'
            }
        }
    },
    credits: {
        style: {
            color: '#666'
        }
    },
    labels: {
        style: {
            color: '#707073'
        }
    },
    drilldown: {
        activeAxisLabelStyle: {
            color: '#F0F0F3'
        },
        activeDataLabelStyle: {
            color: '#F0F0F3'
        }
    },
    navigation: {
        buttonOptions: {
            symbolStroke: '#DDDDDD',
            theme: {
                fill: '#505053'
            }
        }
    },
    // scroll charts
    rangeSelector: {
        buttonTheme: {
            fill: '#505053',
            stroke: '#000000',
            style: {
                color: '#CCC'
            },
            states: {
                hover: {
                    fill: '#707073',
                    stroke: '#000000',
                    style: {
                        color: 'white'
                    }
                },
                select: {
                    fill: '#000003',
                    stroke: '#000000',
                    style: {
                        color: 'white'
                    }
                }
            }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
            backgroundColor: '#333',
            color: 'silver'
        },
        labelStyle: {
            color: 'silver'
        }
    },
    navigator: {
        handles: {
            backgroundColor: '#666',
            borderColor: '#AAA'
        },
        outlineColor: '#CCC',
        maskFill: 'rgba(255,255,255,0.1)',
        series: {
            color: '#7798BF',
            lineColor: '#A6C7ED'
        },
        xAxis: {
            gridLineColor: '#505053'
        }
    },
    scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
    }
};
// Apply the theme
Highcharts.setOptions(Highcharts.theme);
     </script>

     <script type="text/javascript" language="javascript">
        
             function LoadChartData(chartData,chartype,charttitle,chartdivID) {
             debugger;
             Highcharts.chart(chartdivID, {
                     chart: {
                         plotBackgroundColor: null,
                         plotBorderWidth: null,
                         plotShadow: false,
                         type: chartype,
//                         options3d: {
//                             enabled: true,
//                             alpha: 45,
//                             beta: 0
//                            
//                         }
                     },
                     title: {
                         text: charttitle
                     },
                     tooltip: {
                         pointFormat: '{series.name}: <b>{point.y}</b>'
                     },
                     plotOptions: {
                         pie: {
                             allowPointSelect: true,
                             cursor: 'pointer',
                             dataLabels: {
                                 enabled: true,
                                 format: '<b>{point.name}</b>: {point.y}'
                             },
                             showInLegend: false
                         }
                     },
                     series: chartData,
                     exporting: {
                      buttons: {
                        contextButton: {
                            menuItems: ["viewFullscreen", "printChart",'downloadPNG', "downloadJPEG", "downloadPDF",'viewData']
                        }
                        }
                        }
                 });
             }

             $(document).ready(function () {
                 var branchID = "<%= getSessionBranchID() %>";
                 var fiscalID = "<%= getSessionFiscalID() %>";
                 var i;
                 for (i = 1; i < 19; i++) {
//                     debugger;
                     var dataToSend = '{"BranchID": "' + branchID + '","Flag": "' + i + '","FiscalYearID": "' + fiscalID + '"}';
                     console.log(dataToSend);
                     SubmitData(dataToSend,i);
                 }
             });


          var SubmitData = function (submitData,chartNo) {
           $.ajax({
                 type: 'POST',
                 url: '/SetupServices.svc/GetChartData',
                 data: submitData,
                 dataType: "json",
                 contentType: "application/json",
                 success: function (data) {
                  debugger;
                     if (data[0] == null) {
                         $('#container').html("");
                         return;
                     }
                     var json = $.parseJSON(data[0]);
                     //                     var json2 = $.parseJSON(data[1]);

                     var chartDivID = 'container' + chartNo
                     LoadChartData(json.chartData, data[1], data[2], chartDivID);

                 },
                 error: function (data) {
                     alert(data);
                 },
                 failure: function () {
                     alert('Failed to load!!');
                 }

             });
            
          };

     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-panel hidden-xs hidden-sm hidden">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Dashboard <small>Find the info you need. It's quick, easy and Interative</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group"><a class="btn blue" data-toggle="collapse" data-parent="#screen_options"
                        href="#so_options"><span>Screen Options</span> <i class="fa fa-angle-down"></i></a>
                      
                    </li>
                    <li><i class="fa fa-home"></i><a href="/ChartsDashboard.aspx">Home</a> </li>
                </ul>
                <div id="screen_options" class="panel-group">
                    <div class="panel so_panel">
                        <div style="" id="so_options" class="panel-collapse collapse">
                            <div class="panel-body">
                                <label>
                                    <input type="checkbox" data-target="#box_1" />
                                    Chart 1</label>
                                <label>
                                    <input type="checkbox" data-target="#box_2" />
                                    Chart 2</label>
                                <label>
                                    <input type="checkbox" data-target="#box_3" />
                                    Chart 3</label>
                                <label>
                                    <input type="checkbox" data-target="#box_4" />
                                    Chart 4</label>
                                <label>
                                    <input type="checkbox" data-target="#box_5" />
                                    Chart 5</label>
                                <label>
                                    <input type="checkbox" data-target="#box_6" />
                                    Chart 6</label>
                               
                            </div>
                        </div>
                          
                    </div>
                </div>
            </div>
        </div>
        <div class="row dashboard_boxes">
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_1">
                <div class="portlet box">
                  
                    <div class="portlet-body form">
                     
                            <div id="container1" class="chartcontainer">
                          
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_2">
                <div class="portlet box">
                    <div class="portlet-body form">
                     
                         <div id="container2" class="chartcontainer"></div>
                         </div>
                  
                </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_3">
                <div class="portlet box">
                    <div class="portlet-body form">
                    
                         <div id="container3" class="chartcontainer"></div>
                         </div>
                    
                </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_4">
                <div class="portlet box">
                    <div class="portlet-body form">
              
                         <div id="container4" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_5">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container5" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

               <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_6">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container6" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

              <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_7">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container7" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

              <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_8">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container8" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

             <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_9">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container9" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

                <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_10">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container10" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

               <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_11">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container11" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

               <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_12">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container12" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>


                 <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_13">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container13" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>


                <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_14">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container14" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

                 <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_15">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container15" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

              <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_16">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container16" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

              <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_17">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container17" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>

               <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 boxes" id="box_18">
                <div class="portlet box">
                    <div class="portlet-body form">
                 
                         <div id="container18" class="chartcontainer"></div>
                         </div>
                   
                </div>
            </div>


        </div>
    </div>
</asp:Content>
