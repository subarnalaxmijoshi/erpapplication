﻿<%@ Page Language="vb" AutoEventWireup="false" EnableSessionState="True" CodeBehind="fp.aspx.vb" Inherits="School.fp" %>


<html >
<head runat="server">
     <meta charset="utf-8" />
    <title>Forget password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="MobileOptimized" content="320">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/pages/login.css" rel="stylesheet" type="text/css" />
    <link href=/assets/css/custom.css" rel="stylesheet" type="text/css" />
  
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">
        .up
        {
            position: relative;
        }
    </style>
    <script src="/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    
</head>
<body class="login">
          
        <div class="form-group">    <!-- BEGIN LOGO -->
    <div class="logo" style="color: #FFFFFF;">
             <img src="assets/InstitutionLogo/tulogo1.png" alt="Logo" />
        <div runat="server" id="schoolName" style=" font-size: 24px;">
            Mitra ERP 2077</div>
        <div runat="server" id="schoolAddress" style=" font-size: 16px;">Kathmandu,Nepal</div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form id="Form1" class="login-form" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
             <h4 class="form-title " style='text-align: center'>
            Recover your account by Email
        </h4>
      
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">
                Email</label>
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <asp:TextBox class="form-control placeholder-no-fix" type="text" autocomplete="off"
                    runat="server" placeholder="Enter Your authorized email." name="Email" ID="Email">
                </asp:TextBox>
                    <asp:HiddenField ID="hdn_userID" runat="server" />
            </div>
        </div>  
            <asp:Button ID="btn_login2" runat="server" class="btn green pull-right" Text="Submit"
                UseSubmitBehavior="true" />
            <input type="hidden" name="id" id="id" /><br />
            <br/>
             </form>
        </div>
        </div>      
       
</body>
</html>
