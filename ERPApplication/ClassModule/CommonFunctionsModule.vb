﻿Module CommonFunctionsModule
    Private Dao As New DatabaseDao

    Public Function GetCurrentFiscalYearId(ByVal branchid As Integer) As Integer
        Dim sql As String
        Dim fy As Integer
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_FiscalYearMaster]  @flag='c',@BranchID=" & branchid
        ds = Dao.ExecuteDataset(sql)
        fy = 0
        If ds.Tables(0).Rows.Count > 0 Then
            fy = Convert.ToInt32(ds.Tables(0).Rows(0).Item("FiscalYearID").ToString())
        End If
        Return fy
    End Function

    Public Function GetCurrentAcademicYearId(ByVal branchid As Integer) As Integer
        Dim sql As String
        Dim ay As Integer
        Dim ds As New DataSet

        sql = "exec [Setup].[usp_AcademicYear]  @flag='c',@BranchID=" & branchid
        ds = Dao.ExecuteDataset(sql)
        ay = 0
        If ds.Tables(0).Rows.Count > 0 Then
            ay = Convert.ToInt32(ds.Tables(0).Rows(0).Item("AcademicYearID").ToString())
        End If
        Return ay
    End Function

    Public Function IsPublicCampus() As Boolean
        Dim sql As String
        Dim ispublic As Boolean
        Dim ds As New DataSet

        sql = "select top 1 IsPublicCampus from setup.Preference"
        ds = Dao.ExecuteDataset(sql)
        ispublic = False
        If ds.Tables(0).Rows.Count > 0 Then
            ispublic = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("IsPublicCampus").ToString)
        End If
        Return ispublic
    End Function

    Public Function IsSubAccountInVoucherExcluded() As Boolean
        Dim sql As String
        Dim isexcluded As Boolean
        Dim ds As New DataSet

        sql = "select top 1 isnull(IsSubAccountInVoucherExcluded,0) IsSubAccountInVoucherExcluded from setup.Preference"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            isexcluded = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("IsSubAccountInVoucherExcluded").ToString)
        End If
        Return isexcluded
    End Function

    Public Function IsSimpleVoucher() As Boolean
        Dim sql As String
        Dim isSimple As Boolean
        Dim ds As New DataSet

        sql = "select top 1 isnull(IsSimpleVoucher,0) IsSimpleVoucher from setup.Preference"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            isSimple = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("IsSimpleVoucher").ToString)
        End If
        Return isSimple

    End Function

End Module
