Module nepali_date
    Dim bs(,) As Integer = {{2000, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
                        {2001, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2002, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2003, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2004, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2005, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2006, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2007, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2008, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31}, _
   {2009, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2010, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2011, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2012, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30}, _
   {2013, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2014, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2015, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2016, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30}, _
   {2017, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2018, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2019, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2020, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2021, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2022, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30}, _
   {2023, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2024, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2025, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2026, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2027, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2028, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2029, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30}, _
   {2030, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2031, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2032, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2033, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2034, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2035, 30, 32, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31}, _
   {2036, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2037, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2038, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2039, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30}, _
   {2040, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2041, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2042, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2043, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30}, _
   {2044, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2045, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2046, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2047, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2048, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2049, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30}, _
   {2050, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2051, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2052, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2053, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30}, _
   {2054, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2055, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2056, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30}, _
   {2057, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2058, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2059, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2060, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2061, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2062, 30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31}, _
   {2063, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2064, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2065, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2066, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31}, _
   {2067, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2068, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2069, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2070, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30}, _
   {2071, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2072, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30}, _
   {2073, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31}, _
   {2074, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2075, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2076, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30}, _
   {2077, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31}, _
   {2078, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2079, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30}, _
   {2080, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30}, _
   {2081, 31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30}, _
   {2082, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30}, _
   {2083, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30}, _
   {2084, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30}, _
   {2085, 31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30}, _
   {2086, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30}, _
   {2087, 31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30}, _
   {2088, 30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30}, _
   {2089, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30}, _
   {2090, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30}}


    Public debug_info = ""


    '/**
    ' * Calculates wheather english year is leap year or not
    ' *
    ' * @param integer $year
    ' * @return boolean
    ' */
#Region "leap"
    Function is_leap_year(ByVal year) As Boolean

        Dim a As Integer = year

        If a Mod 100 = 0 Then
            If a Mod 400 = 0 Then
                Return True
            Else
                Return False
            End If

        Else
            If a Mod 4 = 0 Then

                Return True
            Else
                Return False

            End If
        End If
    End Function
#End Region


    Function get_nepali_month(ByVal m) As String

        Dim n_month As String = ""

        Select Case (m)

            Case 1
                n_month = "Baishak"

            Case 2
                n_month = "Jestha"


            Case 3
                n_month = "Ashad"


            Case 4
                n_month = "Shrawn"


            Case 5
                n_month = "Bhadra"


            Case 6
                n_month = "Ashwin"


            Case 7
                n_month = "kartik"


            Case 8
                n_month = "Mangshir"


            Case 9
                n_month = "Poush"


            Case 10
                n_month = "Magh"


            Case 11
                n_month = "Falgun"


            Case 12
                n_month = "Chaitra"
        End Select
        Return n_month
    End Function

    Function get_english_month(ByVal m) As String
        Dim eMonth As String = ""
        Select Case (m)
            Case 1
                eMonth = "January"

            Case 2
                eMonth = "February"

            Case 3
                eMonth = "March"

            Case 4
                eMonth = "April"

            Case 5
                eMonth = "May"

            Case 6
                eMonth = "June"

            Case 7
                eMonth = "July"

            Case 8
                eMonth = "August"

            Case 9
                eMonth = "September"

            Case 10
                eMonth = "October"

            Case 11
                eMonth = "November"

            Case 12
                eMonth = "December"

        End Select
        Return eMonth
    End Function

    Function get_day_of_week(ByVal day) As String
        Select Case (day)
            Case 1
                day = "Sunday"
            Case 2
                day = "Monday"
            Case 3
                day = "Tuesday"
            Case 4
                day = "Wednesday"
            Case 5
                day = "Thursday"
            Case 6
                day = "Friday"
            Case 7
                day = "Saturday"
        End Select
        Return day

    End Function

    Function is_range_eng(ByVal yy, ByVal mm, ByVal dd) As Boolean

        If yy < 1944 Or yy > 2033 Then
            debug_info = "Supported only between 1944-2022"
            Return False
        End If
        If mm < 1 Or mm > 12 Then
            debug_info = "Error! value 1-12 only"
            Return False
        End If
        If dd < 1 Or dd > 31 Then
            debug_info = "Error! value 1-31 only"
            Return False
        End If
        Return True
    End Function

    Function is_range_nep(ByVal yy, ByVal mm, ByVal dd) As Boolean


        If yy < 2000 Or yy > 2089 Then
            debug_info = "Supported only between 2000-2089"
            Return False
        End If

        If mm < 1 Or mm > 12 Then
            debug_info = "Error! value 1-12 only"
            Return False
        End If

        If dd < 1 Or dd > 32 Then
            debug_info = "Error! value 1-31 only"
            Return False
        End If

        Return True


    End Function
    '/**
    ' * currently can only calculate the date between AD 1944-2033...
    ' *


    Public Function eng_to_nep(ByVal yy, ByVal mm, ByVal dd) As String

        If is_range_eng(yy, mm, dd) = False Then
            Return False
        Else
            Dim DC As New DateConverter()
            Return DC.ToBS(dd, mm, yy)
            '    ' english month data.
            '    Dim Month() As Integer = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
            '    Dim lmonth() As Integer = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

            '    Dim def_eyy As Integer = 1944                                   '//spear head english date...
            '    Dim def_nyy As Integer = 2000, def_nmm As Integer = 9, def_ndd As Integer = 17      '//spear head nepali date...
            '    Dim total_eDays As Long = 0, total_nDays As Integer = 0, a As Integer = 0, day As Integer = 7       '//all the initializations...
            '    Dim m As Integer = 0, y As Integer = 0, i As Long = 0, j As Long = 0
            '    Dim numDay As Integer = 0

            '    '// count total no. of days in-terms of year
            '    For i = 1 To (yy - def_eyy) Step 1   '//total days for month calculation...(english)

            '        If is_leap_year(def_eyy + i) = True Then

            '            For j = 0 To 11 Step 1
            '                total_eDays += lmonth(j)
            '            Next
            '        Else
            '            For j = 0 To 11 Step 1
            '                total_eDays += Month(j)
            '            Next

            '        End If

            '    Next
            '    '// count total no. of days in-terms of month					
            '    For i = 0 To mm - 2 Step 1


            '        If (is_leap_year(yy) = True) Then
            '            total_eDays += lmonth(i)
            '        Else
            '            total_eDays += Month(i)
            '        End If
            '    Next
            '    '// count total no. of days in-terms of date
            '    total_eDays += dd


            '    i = 0
            '    j = def_nmm
            '    total_nDays = def_ndd
            '    m = def_nmm
            '    y = def_nyy

            '    '// count nepali date from array
            '    While total_eDays <> 0

            '        a = bs(i, j)
            '        total_nDays += 1               '//count the days
            '        day += 1                           '//count the days interms of 7 days
            '        If total_nDays > a Then


            '            m = m + 1
            '            total_nDays = 1
            '            j += 1
            '        End If
            '        If day > 7 Then
            '            day = 1
            '        End If
            '        If m > 12 Then
            '            y += 1
            '            m = 1
            '        End If
            '        If (j > 12) Then
            '            j = 1
            '            i += 1
            '        End If

            '        total_eDays -= 1
            '    End While
            '    numDay = day
            '    'MsgBox(y & "\" & m & "\" & total_nDays & "\" & Me.get_day_of_week(day) & "\" & Me.get_nepali_month(m) & "\" & day)

            '    '   If y = 2072 Then
            '    total_nDays = total_nDays
            '    '  End If


            'Return y & "/" & m & "/" & total_nDays '& "\" & get_day_of_week(day) & "\" & get_nepali_month(m) & "\" & day
        End If
    End Function

    '	/**
    '	 * currently can only calculate the date between BS 2000-2089
    '	 *
    '	 * @param unknown_type $yy
    '	 * @param unknown_type $mm
    '	 * @param unknown_type $dd
    '	 * @return unknown
    '	 */
    Public Function nep_to_eng(ByVal yy, ByVal mm, ByVal dd) As String

        Dim def_eyy As Integer = 1943, def_emm As Integer = 4, def_edd As Integer = 14 - 1    '// init english date.
        Dim def_nyy As Integer = 2000, def_nmm As Integer = 1, def_ndd As Integer = 1      '// equivalent nepali date.
        Dim total_eDays As Integer = 0, total_nDays As Integer = 0, a As Integer = 0, day As Integer = 4 - 1    '// initializations...
        Dim m As Integer = 0, y As Integer = 0, i As Integer = 0
        Dim k As Integer = 0, numDay As Integer = 0

        Dim month() As Integer = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
        Dim lmonth() As Integer = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

        If is_range_nep(yy, mm, dd) = False Then
            Return False

        Else
            Dim DC As New DateConverter()
            Return DC.ToAD(yy, mm, dd)
            ''// count total days in-terms of year
            'For i = 1 To (yy - def_nyy) Step 1
            '    For j As Integer = 1 To 12 Step 1
            '        total_nDays += bs(k, j)
            '    Next
            '    k += 1
            'Next

            ''// count total days in-terms of month			
            'For j As Integer = 1 To mm - 1 Step 1
            '    total_nDays += bs(k, j)
            'Next

            ''// count total days in-terms of dat
            'total_nDays += dd

            ''//calculation of equivalent english date...
            'total_eDays = def_edd
            'm = def_emm
            'y = def_eyy

            'While total_nDays <> 0

            '    If is_leap_year(y) Then
            '        a = lmonth(m)
            '    Else
            '        a = month(m)
            '    End If

            '    total_eDays += 1
            '    day += 1

            '    If total_eDays > a Then
            '        m += 1
            '        total_eDays = 1

            '        If m > 12 Then
            '            y += 1
            '            m = 1
            '        End If
            '    End If
            '    If day > 7 Then
            '        day = 1
            '    End If

            '    total_nDays -= 1
            'End While
            'numDay = day
            '' MsgBox(y & "\" & m & "\" & total_eDays & "\" & Me.get_day_of_week(day) & "\" & Me.get_english_month(m) & "\" & day)


            'Return y & "/" & m & "/" & total_eDays '& "\" & get_day_of_week(day) & "\" & get_english_month(m) & "\" & day
        End If
    End Function
    Public Function nepali_now() As String
        Dim dateTimeInfo As DateTime = DateTime.Now
        Dim year As Integer = dateTimeInfo.Year()
        Dim month As Integer = dateTimeInfo.Month()
        Dim day As Integer = dateTimeInfo.Day()
        Return eng_to_nep(year, month, day)
    End Function
    Public Function eng_date_now() As String
        Dim datetimeinfo1 As DateTime = DateTime.Now
        Dim year1 As Integer = datetimeinfo1.Year()
        Dim month1 As Integer = datetimeinfo1.Month()
        Dim day1 As Integer = datetimeinfo1.Day()
        Return year1 & "/" & month1 & "/" & day1
    End Function

End Module


