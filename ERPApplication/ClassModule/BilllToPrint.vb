﻿
Imports System.Data
Imports System.IO
Imports System.Globalization
Imports System.Threading
Public Class BilllToPrint
    Private Dao As New DatabaseDao
    Dim DC As New DateConverter()
    Public Sub PublicCampusReceiptPrintDotMatrix(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal ManualBillDate As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 5
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)
        sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(28) & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(22) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(18))

        Dim CreatedDate As String() = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
        Dim IsMitiPrintInBill As Boolean = ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString().Trim()

        If IsMitiPrintInBill = True Then
            ManualBillDate = ManualBillDate.Replace("-", "/")

            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                Dim mydate As String() = ManualBillDate.ToString().Trim().Split("/")
                Dim y As String = mydate(0)
                Dim m As String = mydate(1)
                Dim d As String = mydate(2)

                sb.AppendLine(" ".PadRight(14) & DC.ToBS(d, m, y))
            Else
                Dim y As String = CreatedDate(2)
                Dim d As String = CreatedDate(1)
                Dim m As String = CreatedDate(0)
                sb.AppendLine(" ".PadRight(14) & DC.ToBS(d, m, y))

            End If

        Else

            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                sb.AppendLine(" ".PadRight(14) & ManualBillDate)
            Else
                sb.AppendLine(" ".PadRight(14) & ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim())
            End If


        End If

        'Dim CreatedDate As String() = {}
        'Dim mydate = ""
        'Dim y As String = ""
        'Dim d As String = ""
        'Dim m As String = ""



        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(29) & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        sb.Append(" ".PadRight(17) & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(19) & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(28) & ds.Tables(0).Rows(0)("section").ToString().Trim())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(4) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(40))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If


        While counter < 15
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)

        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(7) & due_adv & "                   User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub


    Public Function Creditbill_print(ByVal MonthID As String, ByVal YearID As String, ByVal studentID As String, ByVal SectionID As String, ByVal Batchid As String, ByVal sBranchid As String) As String
        Try



            Dim sql2 = "exec accounts.usp_creditbillNewMonthwise @SectionID = '" & SectionID & "',@BatchID = '" & Batchid & "' , @Flag = 's', @MonthID='" & MonthID & "',@BranchID='" & sBranchid & "'"
            Dim ds2 As New DataSet
            ds2 = Dao.ExecuteDataset(sql2)
            Dim sWriter As StreamWriter
            If ds2.Tables(0).Rows.Count > 0 Then
                For j As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                    studentID = ds2.Tables(0).Rows(j).Item(0).ToString()
                    Dim sql = "exec accounts.usp_creditbillNewMonthwise @MonthId = '" & MonthID & "',@YearID = '" & YearID & "' , @Flag = 'b',@studentID='" & studentID & "',@BranchID='" & sBranchid & "',@SectionID = '" & SectionID & "'"
                    Dim ds As New DataSet
                    Dim AutoBillNumber As String = 1
                    ds = Dao.ExecuteDataset(sql)
                    Dim i As Integer
                    Dim studentDetails As String = " "
                    If j = 0 Then
                        For i = 0 To 5
                            studentDetails &= Environment.NewLine & " "
                        Next
                    Else
                        For i = 0 To 2
                            studentDetails &= Environment.NewLine & " "
                        Next
                    End If

                    Dim sb As New StringBuilder("")


                    sb.AppendLine(studentDetails)
                    sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(18))
                    sb.AppendLine(" ".PadRight(30) & AutoBillNumber)

                    '    sb.AppendLine(" ")
                    sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(18))
                    sb.AppendLine(" ".PadRight(30) & eng_to_nep(Now.Year, Now.Month, Now.Day))

                    '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

                    '  sb.AppendLine(" ")
                    sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(18))
                    sb.AppendLine(" ".PadRight(30) & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


                    ' sb.AppendLine(" ")
                    'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
                    sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(18))
                    sb.AppendLine(" ".PadRight(30) & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

                    ' sb.AppendLine(" ")
                    sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(18))
                    sb.AppendLine(" ".PadRight(30) & ds.Tables(0).Rows(0)("section").ToString().Trim())

                    sb.AppendLine(" ")
                    sb.AppendLine(" ")
                    sb.AppendLine(" ")
                    sb.AppendLine(" ")

                    Dim itemAmount As Decimal
                    Dim particularAmount As Double = 0
                    Dim DiscountName As String = ""
                    Dim DiscountValue As Decimal
                    Dim ThisMonthTotal As Decimal
                    Dim GrandTotal As Decimal
                    Dim k As Integer = 0
                    Dim counter As Integer = 2

                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If i < 9 Then
                            sb.Append("".PadRight(4) & "0" & (i + 1) & ".".ToString.PadRight(6))
                        Else
                            sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
                        End If

                        'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
                        sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(40))
                        If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("Amount").ToString) Then
                            ThisMonthTotal += ds.Tables(0).Rows(i).Item("Amount")
                        End If
                        sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

                        'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

                        'ThisMonthTotal += Convert.ToDouble(itemAmount)
                        counter += 1

                    Next
                    sb.AppendLine(" ")
                    sb.AppendLine(" ")
                    sb.AppendLine(" ")
                    If (DiscountValue > 0) Then
                        sb.Append("".PadRight(19) & DiscountName.PadRight(31))
                        sb.AppendLine(DiscountValue.ToString)
                        counter += 1
                    End If

                    While counter < 15
                        sb.AppendLine(" ")
                        counter += 1
                    End While

                    '  ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
                    'Dim PreTotal As Decimal = 0
                    ''If ds.Tables(2).Rows.Count > 0 Then
                    ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
                    ''End If
                    'GrandTotal = ThisMonthTotal + PreTotal

                    ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
                    'PreTotal = FormatNumber(CDbl(PreTotal), 2)
                    'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)

                    sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
                    'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
                    'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
                    Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
                    sb.AppendLine(" ")
                    sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

                    sb.AppendLine(" ")
                    sb.AppendLine(" ")
                    sb.AppendLine(" ".PadRight(7) & "--")


                    If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/All.txt"))) Then
                        sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/All.txt"))
                    Else
                        If j = 0 Then
                            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/All.txt"), "")
                        End If

                        sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/All.txt"))
                    End If

                    sWriter.WriteLine(sb.ToString)
                    sWriter.Close()
                    ThisMonthTotal = 0
                Next
                Return "OK"
            End If
        Catch ex As Exception

            Throw New Exception(ex.Message)

        End Try
    End Function


    Public Sub PublicCampusReceiptPrintDotMatrixNationalLawCollege(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID=" & sCompanyid
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 5
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)
        sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If


        ' sb.AppendLine("Date : " & eng_to_nep(y, m, d))
        sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        '   sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())

        sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If

        While counter < 16
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)

        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine("".PadRight(6) & "Due/Adv. :  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

#Region "Koteshwor Campus"

    Public Sub PublicCampusReceiptPrintDotMatrixKoteshworCampusWin10(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub
    Public Sub PublicCampusReceiptPrintDotMatrixKoteshworCampusWin7(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @Flag = 'receipt',@ReferenceNo = '" & ReferenceNo & "',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub


    '---------OTHER INCOME BILLS--------
    Public Sub MiscIncomeBILL_KoteshworCampus(ByVal ReferenceNO As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal CompanyID As String, Optional ByVal UserID As String = "", Optional ByVal username As String = "")

        Dim sql = "exec [Accounts].[usp_CounterBill] @rowID = '" & ReferenceNO & "' ,@Flag = 'receipt',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "',@USERID='" & UserID & "',@CompanyID=" & CompanyID
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)


        sb.AppendLine("")
        Dim arr() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arr.Length - 1
            sb.AppendLine("".PadRight(5) & "" & arr(j).Trim().PadRight(32))
        Next

        'sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "" & ds.Tables(0).Rows(0)("summary").ToString().Trim().PadRight(32))
        sb.Append("".PadRight(5) & "Bill No.: " & ds.Tables(0).Rows(0)("ReferenceNo").ToString().Trim().PadRight(32))
        '  sb.AppendLine(" ")
        '  sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""



        CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString().Trim().Split("/") '"02/26/2018"
        mydate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString()
        y = CreatedDate(2)
        d = CreatedDate(1)
        m = CreatedDate(0)



        ' sb.AppendLine("Date : " & eng_to_nep(y, m, d))
        sb.AppendLine("".PadRight(10) & "Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        ' sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        'sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        'sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())

        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("AccountName").ToString().Trim().PadRight(43).Replace("A/C", ""))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
            End If

            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")


        While counter < 17
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ParticularSum
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & "      User : " & username.Trim & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        '    ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub
#End Region

#Region "Pulchowk Campus"
    Public Sub ReceiptPrintDotMatrixPulchowkCampusWin7(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.Append("".PadRight(5) & "Batch : ".PadRight(10) & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        '  sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        'sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        '  sb.AppendLine("Section : -")
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 17
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        'sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'If IsNumeric(due_adv) Then
        '    If due_adv > 0 Then
        '        due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
        '    Else
        '        due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
        '    End If
        'Else
        '    due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        'End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If
        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

    Public Sub ReceiptPrintDotMatrixPulchowkCampusWin10(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        'sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'If IsNumeric(due_adv) Then
        '    If due_adv > 0 Then
        '        due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
        '    Else
        '        due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
        '    End If
        'Else
        '    due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        'End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub


    ' ---------------Other Incom Bill---------------------------------------------------   
    Public Sub MiscIncomeBILL_PulchowkCampus(ByVal ReferenceNO As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal CompanyID As String, Optional ByVal UserID As String = "", Optional ByVal username As String = "")

        Dim sql = "exec [Accounts].[usp_CounterBill] @rowID = '" & ReferenceNO & "' ,@Flag = 'receipt',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "',@USERID='" & UserID & "',@CompanyID=" & CompanyID
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)


        sb.AppendLine("")
        Dim arr() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arr.Length - 1
            sb.AppendLine("".PadRight(5) & "" & arr(j).Trim().PadRight(32))
        Next

        'sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "" & ds.Tables(0).Rows(0)("summary").ToString().Trim().PadRight(32))
        sb.Append("".PadRight(5) & "Bill No.: " & ds.Tables(0).Rows(0)("ReferenceNo").ToString().Trim().PadRight(25))
        '  sb.AppendLine(" ")
        '  sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""



        CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString().Trim().Split("/") '"02/26/2018"
        mydate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString()
        y = CreatedDate(2)
        d = CreatedDate(1)
        m = CreatedDate(0)



        ' sb.AppendLine("Date : " & eng_to_nep(y, m, d))
        sb.AppendLine("".PadRight(10) & "Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        ' sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        'sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        'sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())

        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("AccountName").ToString().Trim().PadRight(43).Replace("A/C", ""))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
            End If

            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")


        While counter < 16
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ParticularSum
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & "      User : " & username.Trim & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If
        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        '    ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub
#End Region

#Region "Jana Bhawana Campus"

    Public Sub PublicCampusReceiptPrintDotMatrixJanaBhawanaWin8(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @Flag = 'receipt',@ReferenceNo = '" & ReferenceNo & "',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then


            Dim NoOfCopy As String = ""
            If Not String.IsNullOrWhiteSpace(copyText) Then
                Dim Reprint As New DataSet
                sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
                Reprint = Dao.ExecuteDataset(sql)
                NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
            End If


            Dim i As Integer
            Dim studentDetails As String = " "
            For i = 0 To 3
                studentDetails &= Environment.NewLine & " "
            Next
            Dim sb As New StringBuilder("")


            'sb.AppendLine(" ")
            'sb.AppendLine(" ")


            sb.AppendLine(studentDetails)
            If String.IsNullOrWhiteSpace(copyText) Then
                sb.AppendLine("")
            Else
                sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
            End If

            ' sb.AppendLine("")
            sb.AppendLine(" ")
            sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("FullName").ToString().Trim().PadRight(27))
            '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
            sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
            ' sb.AppendLine(" ")

            Dim CreatedDate As String() = {}
            Dim mydate = ""
            Dim y As String = ""
            Dim d As String = ""
            Dim m As String = ""

            ManualBillDate = ManualBillDate.Replace("-", "/")
            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
                mydate = ManualBillDate.ToString().Trim()
                y = CreatedDate(0)
                m = CreatedDate(1)
                d = CreatedDate(2)
            Else
                CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
                mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
                y = CreatedDate(2)
                d = CreatedDate(1)
                m = CreatedDate(0)

            End If

            sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

            If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
                sb.AppendLine("Date : " & DC.ToBS(d, m, y))
            Else
                sb.AppendLine("Date : " & mydate)
            End If


            'sb.AppendLine("Date : " & mydate)
            '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

            ' sb.AppendLine(" ")
            sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
            sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


            ' sb.AppendLine(" ")
            'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
            'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
            'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

            ' sb.AppendLine(" ")
            sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
            sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
            sb.AppendLine(" ")
            ' sb.AppendLine("PAN : 600896380")


            sb.AppendLine(" ")
            sb.AppendLine(" ")
            sb.AppendLine(" ")
            'sb.AppendLine(" ")
            'sb.AppendLine(" ")

            Dim itemAmount As Decimal
            Dim particularAmount As Double = 0
            Dim DiscountName As String = ""
            Dim DiscountValue As Decimal
            Dim ThisMonthTotal As Decimal
            Dim GrandTotal As Decimal

            'If (ds.Tables(1).Rows.Count > 0) Then
            '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
            '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
            '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
            'Else
            '    DiscountValue = 0
            'End If

            '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
            Dim k As Integer = 0
            Dim counter As Integer = 2
            Dim ParticularSum As Decimal = 0.0

            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i < 9 Then
                    sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
                Else
                    sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
                End If

                'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
                sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
                sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

                If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                    If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                        ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                    Else
                        ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                    End If

                End If
                'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
                '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
                'Else
                '    sb.AppendLine("-").ToString().Trim().PadRight(30)

                'End If


                'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

                'ThisMonthTotal += Convert.ToDouble(itemAmount)
                counter += 1

            Next
            sb.AppendLine(" ")
            ' sb.AppendLine(" ")
            'sb.AppendLine(" ")
            ' sb.AppendLine(" ")
            If (DiscountValue > 0) Then
                sb.Append("".PadRight(19) & DiscountName.PadRight(31))
                sb.AppendLine(DiscountValue.ToString)
                counter += 1
            End If
            If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
                sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
                counter += 1
            End If
            While counter < 19
                sb.AppendLine(" ")
                counter += 1
            End While

            ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
            'Dim PreTotal As Decimal = 0
            ''If ds.Tables(2).Rows.Count > 0 Then
            ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
            ''End If
            'GrandTotal = ThisMonthTotal + PreTotal

            ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
            'PreTotal = FormatNumber(CDbl(PreTotal), 2)
            'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
            sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
            sb.AppendLine(" ")
            sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
            'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
            'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
            Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
            sb.AppendLine(" ")
            sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

            ' sb.AppendLine(" ")
            ' sb.AppendLine(" ")
            If IsNumeric(due_adv) Then
                If due_adv > 0 Then
                    due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
                Else
                    due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
                End If
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
            End If

            sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

            Dim sWriter As StreamWriter
            'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
            '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
            'Else
            '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
            '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
            'End If



            If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
                sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
            Else
                File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
                sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
            End If
            sWriter.WriteLine(sb.ToString)
            sWriter.Close()
        Else
            'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", "alert('Unable to print bill !!');", True)
        End If
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

    Public Sub MiscIncomeBILL_JanaBhawanaCampus(ByVal ReferenceNO As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal CompanyID As String, Optional ByVal UserID As String = "", Optional ByVal username As String = "")

        Dim sql = "exec [Accounts].[usp_CounterBill] @rowID = '" & ReferenceNO & "' ,@Flag = 'receipt',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "',@USERID='" & UserID & "',@CompanyID=" & CompanyID
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)


        sb.AppendLine("")
        Dim arr() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arr.Length - 1
            sb.AppendLine("".PadRight(5) & "" & arr(j).Trim().PadRight(32))
        Next

        'sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "" & ds.Tables(0).Rows(0)("summary").ToString().Trim().PadRight(32))
        sb.Append("".PadRight(5) & "Bill No.: " & ds.Tables(0).Rows(0)("ReferenceNo").ToString().Trim().PadRight(32))
        '  sb.AppendLine(" ")
        '  sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""



        CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString().Trim().Split("/") '"02/26/2018"
        mydate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString()
        y = CreatedDate(2)
        d = CreatedDate(1)
        m = CreatedDate(0)



        ' sb.AppendLine("Date : " & eng_to_nep(y, m, d))
        sb.AppendLine("".PadRight(10) & "Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        ' sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        'sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        'sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())

        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("AccountName").ToString().Trim().PadRight(43).Replace("A/C", ""))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
            End If

            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")


        While counter < 15
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ParticularSum
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & "      User : " & username.Trim & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        '    ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub
#End Region

    '---------Aadhikavi Campus
    Public Sub PublicCampusReceiptPrintDotMatrixAadikaviCampus(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "

        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If

        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")

        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(7) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        Dim monthName As String() = ds.Tables(0).Rows(0)("monthList").ToString().Trim.Split(" ")

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(19) & monthName(0).PadRight(18))
        sb.AppendLine(" ".PadRight(22) & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(19) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(18))

        Dim CreatedDate As String() = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
        Dim IsMitiPrintInBill As Boolean = False 'ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString().Trim()

        Dim nepaliDate As String = DC.ToBS(Convert.ToDateTime(ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim()), "n")

        If IsMitiPrintInBill = True Then
            ManualBillDate = ManualBillDate.Replace("-", "/")

            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                Dim mydate As String() = ManualBillDate.ToString().Trim().Split("/")
                Dim y As String = mydate(0)
                Dim m As String = mydate(1)
                Dim d As String = mydate(2)

                'sb.AppendLine(" ".PadRight(28) & eng_to_nep(y, m, d))
                sb.AppendLine(" ".PadRight(28) & DC.ToBS(d, m, y))
            Else
                Dim y As String = CreatedDate(2)
                Dim d As String = CreatedDate(1)
                Dim m As String = CreatedDate(0)
                'sb.AppendLine(" ".PadRight(28) & eng_to_nep(y, m, d))
                sb.AppendLine(" ".PadRight(28) & DC.ToBS(d, m, y))

            End If

        Else

            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                'sb.AppendLine(" ".PadRight(19) & ManualBillDate)
                sb.AppendLine(" ".PadRight(19) & nepaliDate.Trim)
            Else
                'sb.AppendLine(" ".PadRight(19) & ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim())
                sb.AppendLine(" ".PadRight(19) & nepaliDate.Trim)
            End If


        End If

        'Dim CreatedDate As String() = {}
        'Dim mydate = ""
        'Dim y As String = ""
        'Dim d As String = ""
        'Dim m As String = ""



        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(19) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(22) & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        sb.Append(" ".PadRight(19) & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(22) & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(19) & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(18) & ds.Tables(0).Rows(0)("section").ToString().Trim())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(4) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(40))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If


        While counter < 15
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)

        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(7) & due_adv & "                   User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

#Region "Nepal Commerce Campus"
    Public Sub PublicCampusReceiptPrintDotMatrixNCC(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        'Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@DirectFiscalYear='" & fy & "' "
        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "',@DirectFiscalYear='" & fy & "' "


        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 5
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If
        sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(28) & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(22) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(18))

        Dim CreatedDate As String() = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
        Dim IsMitiPrintInBill As Boolean = ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString().Trim()

        Dim nepaliDate As String = DC.ToBS(Convert.ToDateTime(ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim()), "n")

        If IsMitiPrintInBill = True Then
            ManualBillDate = ManualBillDate.Replace("-", "/")

            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                Dim mydate As String() = ManualBillDate.ToString().Trim().Split("/")
                Dim y As String = mydate(0)
                Dim m As String = mydate(1)
                Dim d As String = mydate(2)

                'sb.AppendLine(" ".PadRight(14) & eng_to_nep(y, m, d))
                sb.AppendLine(" ".PadRight(14) & DC.ToBS(d, m, y))
            Else
                Dim y As String = CreatedDate(2)
                Dim d As String = CreatedDate(1)
                Dim m As String = CreatedDate(0)
                'sb.AppendLine(" ".PadRight(14) & eng_to_nep(y, m, d))
                sb.AppendLine(" ".PadRight(14) & DC.ToBS(d, m, y))
            End If

        Else

            If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
                'sb.AppendLine(" ".PadRight(14) & ManualBillDate)
                sb.AppendLine(" ".PadRight(14) & nepaliDate.Trim)
            Else
                'sb.AppendLine(" ".PadRight(14) & ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim())
                sb.AppendLine(" ".PadRight(14) & nepaliDate.Trim)
            End If


        End If

        'Dim CreatedDate As String() = {}
        'Dim mydate = ""
        'Dim y As String = ""
        'Dim d As String = ""
        'Dim m As String = ""



        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(29) & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        sb.Append(" ".PadRight(17) & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(19) & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        sb.AppendLine(" ")
        sb.Append(" ".PadRight(10) & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(18))
        sb.AppendLine(" ".PadRight(28) & ds.Tables(0).Rows(0)("section").ToString().Trim())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(4) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(40))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If


        While counter < 15
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)

        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(7) & due_adv & "                   User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

    Public Sub ReceiptPrintDotMatrixNCC_newprintdesign(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "',@DirectFiscalYear='" & fy & "' "

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If

        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next

        Dim sb As New StringBuilder("")

        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If


        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        sb.AppendLine("Bill No. : " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        sb.AppendLine(" ")


        Dim voucherDate As String = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim()
        Dim nepaliDate As String = DC.ToBS(Convert.ToDateTime(ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim()), "n")

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & nepaliDate.PadRight(34))
        sb.AppendLine("Date : " & voucherDate)
        sb.AppendLine("")


        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No. :" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())
        sb.AppendLine(" ")

        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        Dim k As Integer = 0
        Dim counter As Integer = 2

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(4) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            End If

            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(40))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            counter += 1
        Next

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If

        While counter < 16
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)

        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))

        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(7) & due_adv & "               User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

    End Sub
#End Region


    '---------Adarsha Multiple Campus ,Gajuri
    Public Sub PublicCampusReceiptPrintDotMatrixAMCGajuri(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        'sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & eng_to_nep(y, m, d).PadRight(34))
        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If



        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If
        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub


#Region "Pokhara Nursing Campus"
    Public Sub ReceiptPrintDotMatrixPokharaNursingCampusWin7(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

    Public Sub MiscIncomeBILL_PNC(ByVal ReferenceNO As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal CompanyID As String, Optional ByVal UserID As String = "", Optional ByVal username As String = "")

        Dim sql = "exec [Accounts].[usp_CounterBill] @rowID = '" & ReferenceNO & "' ,@Flag = 'receipt',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "',@USERID='" & UserID & "',@CompanyID=" & CompanyID
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)


        sb.AppendLine("")
        Dim arr() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arr.Length - 1
            sb.AppendLine("".PadRight(5) & "" & arr(j).Trim().PadRight(32))
        Next

        'sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "" & ds.Tables(0).Rows(0)("summary").ToString().Trim().PadRight(32))
        sb.Append("".PadRight(5) & "Bill No.: " & ds.Tables(0).Rows(0)("ReferenceNo").ToString().Trim().PadRight(32))
        '  sb.AppendLine(" ")
        '  sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""



        CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString().Trim().Split("/") '"02/26/2018"
        mydate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString()
        y = CreatedDate(2)
        d = CreatedDate(1)
        m = CreatedDate(0)



        sb.AppendLine("".PadRight(5) & "Miti : " & DC.ToBS(d, m, y))
        'sb.AppendLine("".PadRight(10) & "Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        'sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        'sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        'sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())

        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("AccountName").ToString().Trim().PadRight(43).Replace("A/C", ""))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
            End If

            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")


        While counter < 15
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ParticularSum
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & "      User : " & username.Trim & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        '    ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub
#End Region


    '----Bagiswori---------------
    Public Sub ReceiptPrintDotMatrixBagisworiWin7(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 4
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If
        sb.AppendLine("")

        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Class : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 16
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

    '---------New Misc Bill
    Public Sub MiscIncomeBILL_new(ByVal ReferenceNO As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal CompanyID As String, Optional ByVal UserID As String = "", Optional ByVal username As String = "", Optional ByVal IsNotesPrint As String = "")

        Dim sql = "exec [Accounts].[usp_MiscIncome] @RefNo = '" & ReferenceNO & "' ,@Flag = 'receipt',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "',@USERID='" & UserID & "',@CompanyID=" & CompanyID
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 5
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        sb.AppendLine(studentDetails)


        sb.AppendLine("")
        Dim arr() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arr.Length - 1
            sb.AppendLine("".PadRight(5) & "Name : " & arr(j).Trim().PadRight(32))
        Next


        sb.Append("".PadRight(5) & "Bill No.: " & ds.Tables(0).Rows(0)("ReferenceNo").ToString().Trim().PadRight(32))
        '  sb.AppendLine(" ")
        '  sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))

        Dim CreatedDate As String() = {}
        Dim mydate = ""

        CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString().Trim().Split("/") '"02/26/2018"
        mydate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString().Trim()

        Dim nepaliDate As String = DC.ToBS(Convert.ToDateTime(mydate), "n")

        sb.AppendLine("".PadRight(10) & "Date : " & nepaliDate)

        sb.AppendLine(" ")
        sb.AppendLine(" ")

        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("AccountName").ToString().Trim().PadRight(43).Replace("A/C", ""))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
            End If

            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        If (IsNotesPrint = "true") Then
            Dim Notes As String = ds.Tables(0).Rows(0).Item("Notes").ToString().Trim
            sb.AppendLine(" ".PadRight(7) & "Notes : " & Notes)
        End If

        While counter < 20
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ParticularSum
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine(" ".PadRight(51) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)
        sb.AppendLine(" ")


        ' sb.AppendLine(" ")
        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & "      User : " & username.ToString.Trim & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

    End Sub

    '----butwal-------
    Public Sub ReceiptPrintDotMatrixButwal(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @Flag = 'receipt',@ReferenceNo = '" & ReferenceNo & "',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal



        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0
        Dim MaskingSum As Decimal = 0.0
        Dim maskingAmount As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            'If i < 9 Then
            '    sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            'Else
            '    sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            'End If

            ''sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            'sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            'sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
          
            'counter += 1
        Next
        MaskingSum = ParticularSum

        Dim sql2 = "exec [Accounts].[usp_MaskingBill] @flag='bill'"
        Dim ds2 As New DataSet
        ds2 = Dao.ExecuteDataset(sql2)

        For x = 0 To ds2.Tables(0).Rows.Count - 1
            maskingAmount = ds2.Tables(0).Rows(x).Item("Amount")
            If (ds2.Tables(0).Rows(x).Item("IsSum").ToString().Trim().ToLower() = "false") Then
                MaskingSum = MaskingSum - maskingAmount
            End If
        Next

        For j = 0 To ds2.Tables(0).Rows.Count - 1
            If j < 9 Then
                sb.Append("".PadRight(5) & "0" & (j + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (j + 1) & ".".ToString.PadRight(6))
            End If

            sb.Append(ds2.Tables(0).Rows(j).Item("ParticularName").ToString().Trim().PadRight(43))


            If (ds2.Tables(0).Rows(j).Item("IsSum").ToString().Trim().ToLower() = "true") Then
                sb.AppendLine(MaskingSum.ToString().Trim().PadLeft(10))
            Else
                sb.AppendLine(ds2.Tables(0).Rows(j).Item("Amount").ToString().Trim().PadLeft(10))

            End If
            counter += 1
        Next


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")

        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub


#Region "Diktel" '----Diktel-----
    Public Sub ReceiptPrintDotMatrixDiktelWin10(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @ReferenceNo = '" & ReferenceNo & "' ,@Flag = 'receipt',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.AppendLine(" ")--last modified
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 18
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub

    Public Sub ReceiptPrintDotMatrixDiktelWin7(ByVal ReferenceNo As String, ByVal AutoBillNumber As String, ByVal sBranchid As String, ByVal sCompanyid As String, Optional ByVal due_adv As String = "--", Optional ByVal username As String = "", Optional ByVal ManualBillDate As String = "", Optional ByVal copyText As String = "", Optional ByVal NotesToPrint As String = "", Optional ByVal fy As String = "", Optional ByVal UserID As String = "")

        Dim sql = "exec [Accounts].[usp_CashPayment] @Flag = 'receipt',@ReferenceNo = '" & ReferenceNo & "',@BranchID='" & sBranchid & "',@CompanyID='" & sCompanyid & "'"
        sql += ",@DirectFiscalYear='" & fy & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim NoOfCopy As String = ""
        If Not String.IsNullOrWhiteSpace(copyText) Then
            Dim Reprint As New DataSet
            sql = "exec accounts.usp_ReprintBill @BillNo='" & ReferenceNo & "',@PrintedBy='" & username.ToString & "'"
            Reprint = Dao.ExecuteDataset(sql)
            NoOfCopy = Reprint.Tables(0).Rows(0).Item(0).ToString
        End If


        Dim i As Integer
        Dim studentDetails As String = " "
        For i = 0 To 3
            studentDetails &= Environment.NewLine & " "
        Next
        Dim sb As New StringBuilder("")


        'sb.AppendLine(" ")
        'sb.AppendLine(" ")


        sb.AppendLine(studentDetails)
        If String.IsNullOrWhiteSpace(copyText) Then
            sb.AppendLine("")
        Else
            sb.AppendLine("".PadRight(5) & GetNumberPosion(NoOfCopy) & " " & copyText)
        End If

        ' sb.AppendLine("")
        sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Student's Name : ".PadRight(10) & ds.Tables(0).Rows(0).Item("fullName").ToString().Trim().PadRight(27))
        '   sb.Append("".PadRight(5) & "Month Name: " & ds.Tables(0).Rows(0)("monthList").ToString().Trim().PadRight(32))
        sb.AppendLine("Bill No.: " & ds.Tables(0).Rows(0)("receiptNo").ToString().Trim())
        ' sb.AppendLine(" ")

        Dim CreatedDate As String() = {}
        Dim mydate = ""
        Dim y As String = ""
        Dim d As String = ""
        Dim m As String = ""

        ManualBillDate = ManualBillDate.Replace("-", "/")
        If Not String.IsNullOrWhiteSpace(ManualBillDate) Then
            CreatedDate = ManualBillDate.ToString().Trim().Split("/") '"02/26/2018"
            mydate = ManualBillDate.ToString().Trim()
            y = CreatedDate(0)
            m = CreatedDate(1)
            d = CreatedDate(2)
        Else
            CreatedDate = ds.Tables(0).Rows(0).Item("voucherDate").ToString().Trim().Split("/") '"02/26/2018"
            mydate = ds.Tables(0).Rows(0).Item("voucherDate").ToString()
            y = CreatedDate(2)
            d = CreatedDate(1)
            m = CreatedDate(0)

        End If

        sb.Append("".PadRight(5) & "Miti : ".PadRight(10) & DC.ToBS(d, m, y).PadRight(34))

        If ds.Tables(0).Rows(0).Item("IsMitiPrintInBill").ToString = "1" Then
            sb.AppendLine("Date : " & DC.ToBS(d, m, y))
        Else
            sb.AppendLine("Date : " & mydate)
        End If


        'sb.AppendLine("Date : " & mydate)
        '   sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Regd# : ".PadRight(10) & ds.Tables(0).Rows(0)("RegistrationNumber").ToString().Trim().PadRight(34))
        sb.AppendLine("Roll No.:" & ds.Tables(0).Rows(0)("RollNo").ToString().Trim())


        ' sb.AppendLine(" ")
        'sb.AppendLine(" ".PadRight(20) & "".ToString().ToString())
        'sb.Append("".PadRight(5) & "Faculty : " & ds.Tables(0).Rows(0)("FacultyTitle").ToString().Trim().PadRight(34))
        'sb.AppendLine("Level : " & ds.Tables(0).Rows(0).Item("LevelTitle").ToString().Trim())

        ' sb.AppendLine(" ")
        sb.Append("".PadRight(5) & "Batch : " & ds.Tables(0).Rows(0)("batchTitle").ToString().Trim().PadRight(36))
        sb.AppendLine("Section : " & ds.Tables(0).Rows(0)("section").ToString().Trim())
        sb.AppendLine(" ")
        ' sb.AppendLine("PAN : 600896380")


        sb.AppendLine(" ")
        sb.AppendLine(" ")
        sb.AppendLine(" ")
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")

        Dim itemAmount As Decimal
        Dim particularAmount As Double = 0
        Dim DiscountName As String = ""
        Dim DiscountValue As Decimal
        Dim ThisMonthTotal As Decimal
        Dim GrandTotal As Decimal

        'If (ds.Tables(1).Rows.Count > 0) Then
        '    DiscountName = ds.Tables(1).Rows(0).Item("DiscountName").ToString
        '    DiscountValue = ds.Tables(1).Rows(0).Item("DiscountAmount").ToString
        '    DiscountValue = FormatNumber(CDbl(DiscountValue), 2)
        'Else
        '    DiscountValue = 0
        'End If

        '  Dim rows As Integer = ds.Tables(0).Rows.Count - 1
        Dim k As Integer = 0
        Dim counter As Integer = 2
        Dim ParticularSum As Decimal = 0.0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If i < 9 Then
                sb.Append("".PadRight(5) & "0" & (i + 1) & ".".ToString.PadRight(6))
            Else
                sb.Append("".PadRight(5) & (i + 1) & ".".ToString.PadRight(6))
            End If

            'sb.Append("".PadRight(4) & (i + 1) & ".".ToString.PadRight(6))
            sb.Append(ds.Tables(0).Rows(i).Item("ParticularName").ToString().Trim().PadRight(43))
            sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))

            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("amount").ToString) Then
                If ds.Tables(0).Rows(i).Item("ParticularName").ToString().Contains("Discount") Then
                    ParticularSum = ParticularSum - ds.Tables(0).Rows(i).Item("amount")
                Else
                    ParticularSum = ParticularSum + ds.Tables(0).Rows(i).Item("amount")
                End If

            End If
            'If ds.Tables(0).Rows(i).Item("Amount") > 0 Then
            '    sb.AppendLine(ds.Tables(0).Rows(i).Item("Amount").ToString().Trim().PadLeft(10))
            'Else
            '    sb.AppendLine("-").ToString().Trim().PadRight(30)

            'End If


            'itemAmount = (Convert.ToDecimal(ds.Tables(0).Rows(i).Item("Amount")) + Convert.ToDecimal(ds.Tables(0).Rows(i).Item("TaxableAmount"))).ToString

            'ThisMonthTotal += Convert.ToDouble(itemAmount)
            counter += 1

        Next
        ' sb.AppendLine(" ")----------modified
        'sb.AppendLine(" ")
        'sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If (DiscountValue > 0) Then
            sb.Append("".PadRight(19) & DiscountName.PadRight(31))
            sb.AppendLine(DiscountValue.ToString)
            counter += 1
        End If
        If Not String.IsNullOrWhiteSpace(NotesToPrint) Then
            sb.AppendLine("  ".PadRight(4) & "Note: " & NotesToPrint)
            counter += 1
        End If
        While counter < 19
            sb.AppendLine(" ")
            counter += 1
        End While

        ThisMonthTotal = ds.Tables(0).Rows(0).Item("TotalAmt").ToString().Trim()
        'Dim PreTotal As Decimal = 0
        ''If ds.Tables(2).Rows.Count > 0 Then
        ''    PreTotal = ds.Tables(2).Rows(0).Item("PreviousDue").ToString()
        ''End If
        'GrandTotal = ThisMonthTotal + PreTotal

        ThisMonthTotal = FormatNumber(CDbl(ThisMonthTotal), 2)
        'PreTotal = FormatNumber(CDbl(PreTotal), 2)
        'GrandTotal = FormatNumber(CDbl(GrandTotal), 2)
        sb.AppendLine("".PadRight(10) & "Total Fees : ".PadRight(10) & ds.Tables(0).Rows(0).Item("TotalDues").ToString().Trim().PadRight(28) & ParticularSum.ToString.PadLeft(10))
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(51) & ThisMonthTotal.ToString.PadLeft(10))
        'sb.AppendLine(" ".PadRight(51) & PreTotal.ToString)
        'sb.AppendLine(" ".PadRight(51) & GrandTotal.ToString)
        Dim amountinWord As String = ConvertNumberToRupees(ThisMonthTotal)
        sb.AppendLine(" ")
        sb.AppendLine(" ".PadRight(21) & amountinWord.ToString)

        ' sb.AppendLine(" ")
        ' sb.AppendLine(" ")
        If IsNumeric(due_adv) Then
            If due_adv > 0 Then
                due_adv = due_adv + ds.Tables(0).Rows(0).Item("RemainingFee")
            Else
                due_adv = ds.Tables(0).Rows(0).Item("RemainingFee") - due_adv
            End If
        Else
            due_adv = ds.Tables(0).Rows(0).Item("RemainingFee")
        End If

        sb.AppendLine("".PadRight(6) & "  ".PadRight(7) & due_adv & "      User : " & username & "  " & DateTime.Now.ToString("h:mm"))

        Dim sWriter As StreamWriter
        'If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))) Then
        '    sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'Else
        '    File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"), "")
        '    sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/Specific.txt"))
        'End If

        If (Not File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))) Then
            sWriter = New StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        Else
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"), "")
            sWriter = File.AppendText(System.Web.HttpContext.Current.Server.MapPath("~/Reports/Rpt/ProcessingBill" & UserID & ".txt"))
        End If

        sWriter.WriteLine(sb.ToString)
        sWriter.Close()

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)

        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintCreditBill", " $('#prntRcpt').click();", True)
        'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "CloseThisWindow1", "var creditBill = window.open('','_self');creditBill.close();", True)

    End Sub
#End Region


    Private Function GetNumberPosion(ByRef Digit As String)
        Select Case Integer.Parse(Digit)
            Case 1 : GetNumberPosion = "1st"
            Case 2 : GetNumberPosion = "2nd"
            Case 3 : GetNumberPosion = "3rd"
            Case 4 : GetNumberPosion = "4th"
            Case 5 : GetNumberPosion = "5th"
            Case 6 : GetNumberPosion = "6th"
            Case 7 : GetNumberPosion = "7th"
            Case 8 : GetNumberPosion = "8th"
            Case 9 : GetNumberPosion = "9th"
            Case 9 : GetNumberPosion = "10th"
            Case Else : GetNumberPosion = Digit
        End Select

    End Function

    Private Function ConvertNumberToRupees(ByVal amount As String) As String

        Dim rupees = "", paisa = "", temp = ""
        Dim decimalPlace, count
        Dim place(9) As String
        place(2) = " Thousand "
        place(3) = " Million "
        place(4) = " Billion "
        place(5) = " Trillion "

        ' String representation of amount.
        amount = amount.Trim()
        amount = amount.Replace(",", "")
        ' Position of decimal place 0 if none.
        decimalPlace = amount.IndexOf(".")
        ' Convert cents and set string amount to dollar amount.
        If decimalPlace > 0 Then
            paisa = GetTens(amount.Substring(decimalPlace + 1).PadRight(2, "0").Substring(0, 2))
            amount = amount.Substring(0, decimalPlace).Trim()
        End If

        count = 1
        Do While amount <> ""
            temp = GetHundreds(amount.Substring(Math.Max(amount.Length, 3) - 3))
            If temp <> "" Then rupees = temp & place(count) & rupees
            If amount.Length > 3 Then
                amount = amount.Substring(0, amount.Length - 3)
            Else
                amount = ""
            End If
            count = count + 1
        Loop

        Select Case rupees
            Case ""
                rupees = "No Rupees"
            Case "One"
                rupees = "One Rupees"
            Case Else
                rupees = rupees
        End Select

        If (Not String.IsNullOrWhiteSpace(paisa)) Then
            Select Case paisa
                Case ""
                    paisa = " and No Paisa"
                Case "One"
                    paisa = " and One Paisa"
                Case Else
                    paisa = " and " & paisa & " Paisa"
            End Select
        Else
            paisa = " "
        End If


        ConvertNumberToRupees = rupees & paisa & "Only."
    End Function

    ' Converts a number from 100-999 into text
    Function GetHundreds(ByVal amount As String) As String
        Dim Result As String = ""
        If Not Integer.Parse(amount) = 0 Then
            amount = amount.PadLeft(3, "0")
            ' Convert the hundreds place.
            If amount.Substring(0, 1) <> "0" Then
                Result = GetDigit(amount.Substring(0, 1)) & " Hundred "
            End If
            ' Convert the tens and ones place.
            If amount.Substring(1, 1) <> "0" Then
                Result = Result & GetTens(amount.Substring(1))
            Else
                Result = Result & GetDigit(amount.Substring(2))
            End If
            GetHundreds = Result
        End If
        Return Result
    End Function

    ' Converts a number from 10 to 99 into text.
    Private Function GetTens(ByRef TensText As String) As String
        Dim Result As String
        Result = ""           ' Null out the temporary function value.
        If TensText.StartsWith("1") Then   ' If value between 10-19...
            Select Case Integer.Parse(TensText)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else                                 ' If value between 20-99...
            Select Case Integer.Parse(TensText.Substring(0, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select
            Result = Result & GetDigit(TensText.Substring(1, 1))  ' Retrieve ones place.
        End If
        GetTens = Result
    End Function

    ' Converts a number from 1 to 9 into text.
    Private Function GetDigit(ByRef Digit As String) As String
        Select Case Integer.Parse(Digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function
End Class
