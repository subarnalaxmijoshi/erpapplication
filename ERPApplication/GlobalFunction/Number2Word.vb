﻿Imports System.Globalization

Public Class Number2Word


    Public Function ConvertNumberToRupees(ByVal amount As String) As String

        Dim rupees = "", paisa = "", temp = ""
        Dim decimalPlace, count
        Dim place(9) As String
        place(2) = " Thousand "
        place(3) = " Million "
        place(4) = " Billion "
        place(5) = " Trillion "

        ' String representation of amount.
        amount = amount.Trim()
        amount = amount.Replace(",", "")
        ' Position of decimal place 0 if none.
        decimalPlace = amount.IndexOf(".")
        ' Convert cents and set string amount to dollar amount.
        If decimalPlace > 0 Then
            paisa = GetTens(amount.Substring(decimalPlace + 1).PadRight(2, "0").Substring(0, 2))
            amount = amount.Substring(0, decimalPlace).Trim()
        End If

        count = 1
        Do While amount <> ""
            temp = GetHundreds(amount.Substring(Math.Max(amount.Length, 3) - 3))
            If temp <> "" Then rupees = temp & place(count) & rupees
            If amount.Length > 3 Then
                amount = amount.Substring(0, amount.Length - 3)
            Else
                amount = ""
            End If
            count = count + 1
        Loop

        Select Case rupees
            Case ""
                rupees = "No Rupees"
            Case "One"
                rupees = "One Rupees"
            Case Else
                rupees = rupees
        End Select

        If (Not String.IsNullOrWhiteSpace(paisa)) Then
            Select Case paisa
                Case ""
                    paisa = " and No Paisa"
                Case "One"
                    paisa = " and One Paisa"
                Case Else
                    paisa = " and " & paisa & " Paisa"
            End Select
        Else
            paisa = " "
        End If


        ConvertNumberToRupees = rupees & paisa & "Only."
    End Function

    ' Converts a number from 100-999 into text
    Function GetHundreds(ByVal amount As String) As String
        Dim result As String = ""
        If Not Integer.Parse(amount) = 0 Then
            amount = amount.PadLeft(3, "0")
            ' Convert the hundreds place.
            If amount.Substring(0, 1) <> "0" Then
                result = GetDigit(amount.Substring(0, 1)) & " Hundred "
            End If
            ' Convert the tens and ones place.
            If amount.Substring(1, 1) <> "0" Then
                result = result & GetTens(amount.Substring(1))
            Else
                result = result & GetDigit(amount.Substring(2))
            End If
            GetHundreds = result
        End If
        Return result
    End Function

    ' Converts a number from 10 to 99 into text.
    Private Function GetTens(ByRef tensText As String) As String
        Dim result As String
        result = ""           ' Null out the temporary function value.
        If tensText.StartsWith("1") Then   ' If value between 10-19...
            Select Case Integer.Parse(tensText)
                Case 10 : result = "Ten"
                Case 11 : result = "Eleven"
                Case 12 : result = "Twelve"
                Case 13 : result = "Thirteen"
                Case 14 : result = "Fourteen"
                Case 15 : result = "Fifteen"
                Case 16 : result = "Sixteen"
                Case 17 : result = "Seventeen"
                Case 18 : result = "Eighteen"
                Case 19 : result = "Nineteen"
                Case Else
            End Select
        Else                                 ' If value between 20-99...
            Select Case Integer.Parse(tensText.Substring(0, 1))
                Case 2 : result = "Twenty "
                Case 3 : result = "Thirty "
                Case 4 : result = "Forty "
                Case 5 : result = "Fifty "
                Case 6 : result = "Sixty "
                Case 7 : result = "Seventy "
                Case 8 : result = "Eighty "
                Case 9 : result = "Ninety "
                Case Else
            End Select
            result = result & GetDigit(tensText.Substring(1, 1))  ' Retrieve ones place.
        End If
        GetTens = result
    End Function

    ' Converts a number from 1 to 9 into text.
    Private Function GetDigit(ByRef digit As String) As String
        Select Case Integer.Parse(digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function


End Class
