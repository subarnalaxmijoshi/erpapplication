﻿Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Globalization
Imports System.IO
Imports System.Web.HttpContext

Public Class CellTextAlignment

    Public Shared Function ToCenter(ByVal value As String, ByVal font As iTextSharp.text.Font) As PdfPCell
        Dim cell As PdfPCell
        cell = New PdfPCell(New Phrase(String.Format("{0:0.##}", value), font))
        cell.HorizontalAlignment = Element.ALIGN_CENTER
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        Return cell
    End Function


    Public Shared Function ToLeft(ByVal value As String, ByVal font As iTextSharp.text.Font) As PdfPCell
        Dim cell As PdfPCell
        cell = New PdfPCell(New Phrase(String.Format("{0:0.##}", value), font))
        cell.HorizontalAlignment = Element.ALIGN_LEFT
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        cell.PaddingLeft = 5
        Return cell
    End Function


    Public Shared Function ToRight(ByVal value As String, ByVal font As iTextSharp.text.Font) As PdfPCell
        Dim cell As PdfPCell
        cell = New PdfPCell(New Phrase(value, font))
        cell.HorizontalAlignment = Element.ALIGN_RIGHT
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        cell.PaddingRight = 5
        Return cell
    End Function


    Public Shared Function ToCenter(ByVal value As Paragraph) As PdfPCell
        Dim cell As PdfPCell
        cell = New PdfPCell(value)
        cell.HorizontalAlignment = Element.ALIGN_CENTER
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        cell.PaddingLeft = 5
        Return cell
    End Function

End Class
