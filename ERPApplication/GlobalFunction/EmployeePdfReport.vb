﻿
Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Globalization
Imports System.IO
Imports System.Web.HttpContext
Public Class EmployeePdfReport
    Private Dao As New DatabaseDao
    Dim DC As New DateConverter()

    Public Sub SingleHRLeaveProfile(ByVal employeeId As String, ByVal branchId As String)
        Dim ds As New DataSet
        Dim sql, sql2 As String

        sql = "select EmployeeNo,FullName,JobTitleName [Designation] from hr.View_Employee where EmployeeID ='" & employeeId & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim raisedFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.0, iTextSharp.text.Font.NORMAL)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.NORMAL)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.BOLD)
            Dim headerFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, iTextSharp.text.Font.BOLD)
            Dim italicFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, iTextSharp.text.Font.BOLDITALIC)

            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 1)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.4, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1)

            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 4.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, 0)
            Dim spacer3 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 0)
            Dim spacer4 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 30.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width

            'Report Header -Institution Info
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim reportHeader As PdfPTable = New PdfPTable(1)
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 10.0F)
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk("Tel:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & ",Email:" & institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString() & ",Website:" & institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString() & Environment.NewLine, SchoolContact)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk("EMPLOYEE LEAVE DETAILS" & Environment.NewLine, PageHeader)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportHeader.AddCell(cell)
            pdfDoc.Add(reportHeader)

            'Image Logo insertion
            Try
                Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/profilelogo.png")
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                logo.ScaleToFit(76.0F, 76.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 50, pdfDoc.PageSize.Height - 88.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception
                Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/img_not_found.gif")
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - logo.Width - 185, pdfDoc.PageSize.Height - 85.0F)
                pdfDoc.Add(logo)
            End Try
            'End of report header


            'Employee Info
            Dim PhotoEmp As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString().Trim
            Dim EmployeeName As String = ds.Tables(0).Rows(0).Item("FullName").ToString().Trim
            Dim Designation As String = ds.Tables(0).Rows(0).Item("Designation").ToString().Trim
            Dim EmpNo As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString().Trim

            'Photo insertion
            Try
                Dim imgpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/HR/Images/" & PhotoEmp & ".jpg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/HR/Images/" & PhotoEmp & ".jpeg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpeg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/HR/Images/" & PhotoEmp & ".png")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                photo.ScaleToFit(80.0F, 80.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 130, pdfDoc.PageSize.Height - 200.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            Dim topTable As PdfPTable = New PdfPTable(1)
            topTable.SpacingBefore = 4.0F
            topTable.SpacingAfter = 20.0F
            topTable.TotalWidth = pageWidth - 65
            topTable.LockedWidth = True

            Dim p, p1, p2, p3, p4 As New Paragraph
            p.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(0.8F, 100.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))

            p1.SetLeading(0.0F, 2.0F)
            p1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)

            cell = CellTextAlignment.ToCenter(p)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            topTable.AddCell(cell)

            p2.Add(New Chunk("Name of the Employee" & " : ", headerFont))
            p2.Add(New Chunk(EmployeeName, italicFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)


            p3.Add(New Chunk("Designation" & " : ", headerFont))
            p3.Add(New Chunk(Designation, italicFont))

            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)


            p4.Add(New Chunk("Employee No. " & " : ", headerFont))
            p4.Add(New Chunk(EmpNo, italicFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            'Leave Details
            sql2 = "exec [Attendance].[usp_LeaveProfile] @EmployeeID='" & employeeId & "',@BranchID='" & branchId & "'"
            ds.Reset()
            ds = Dao.ExecuteDataset(sql2)
            If ds.Tables.Count > 0 Then
                For t As Integer = 0 To ds.Tables.Count - 1
                    If (ds.Tables(t).Rows.Count > 0) Then
                        Dim noofrows As Integer = ds.Tables(t).Rows.Count
                        Dim noofcols As Integer = ds.Tables(t).Columns.Count

                        'define column widths
                        Dim colwidths As Integer()
                        columnWidth.Clear()
                        columnWidth.Add(10.0F)
                        For w As Integer = 0 To noofcols - 2
                            columnWidth.Add(35.0F)
                        Next
                        colwidths = columnWidth.ToArray()

                        Dim tblLeaveInfo As PdfPTable = New PdfPTable(noofcols)
                        tblLeaveInfo.SpacingBefore = 15.0F
                        tblLeaveInfo.SpacingAfter = 3.0F
                        tblLeaveInfo.TotalWidth = pageWidth - 60
                        tblLeaveInfo.LockedWidth = True
                        tblLeaveInfo.SetWidths(colwidths)

                        'Title header
                        cell = CellTextAlignment.ToLeft("Table " & t + 1 & " : " & ds.Tables(t).Rows(0).Item("TableName").ToString, headerFont)
                        cell.FixedHeight = 28
                        cell.Colspan = noofcols
                        cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                        cell.HorizontalAlignment = Element.ALIGN_LEFT
                        tblLeaveInfo.AddCell(cell)

                        cell = CellTextAlignment.ToCenter("S.N", boldFont)
                        cell.FixedHeight = 25
                        cell.Border = iTextSharp.text.Rectangle.BOX
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY
                        tblLeaveInfo.AddCell(cell)

                        For c As Integer = 0 To noofcols - 2
                            cell = CellTextAlignment.ToCenter(ds.Tables(t).Columns(c).ColumnName.ToString(), boldFont)
                            cell.Rowspan = 1
                            cell.FixedHeight = 25
                            cell.Border = iTextSharp.text.Rectangle.BOX
                            cell.HorizontalAlignment = Element.ALIGN_CENTER
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY
                            tblLeaveInfo.AddCell(cell)
                        Next
                        'tblLeaveInfo.CompleteRow()

                        For i As Integer = 0 To noofrows - 1
                            Dim sn = i + 1
                            cell = CellTextAlignment.ToCenter(sn, boldFont)
                            cell.FixedHeight = 20
                            cell.Rowspan = 1
                            cell.Colspan = 1
                            cell.Border = iTextSharp.text.Rectangle.BOX
                            cell.HorizontalAlignment = Element.ALIGN_CENTER
                            tblLeaveInfo.AddCell(cell)
                            For j As Integer = 0 To noofcols - 2
                                cell = CellTextAlignment.ToCenter(ds.Tables(t).Rows(i)(j).ToString.Trim, normalFont)
                                cell.FixedHeight = 20
                                cell.Rowspan = 1
                                cell.Colspan = 1
                                cell.Border = iTextSharp.text.Rectangle.BOX
                                cell.HorizontalAlignment = Element.ALIGN_CENTER
                                tblLeaveInfo.AddCell(cell)
                            Next
                        Next
                        pdfDoc.Add(tblLeaveInfo)
                    End If
                Next
            End If


            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()

            Dim rptApproval As PdfPTable = New PdfPTable(3)
            rptApproval.TotalWidth = pageWidth - 60
            rptApproval.SpacingBefore = 50.0F
            rptApproval.SpacingAfter = 10.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths)

            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 2
            cell.Colspan = 3
            rptApproval.AddCell(cell)

            Dim nepDate As String = DC.ToBS(Date.Now, "n")
            cell = CellTextAlignment.ToLeft("Printed Date : " & nepDate, normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Verified By", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToRight("Approved By", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            pdfDoc.Add(rptApproval)


            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Leave_Profile.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub
End Class
