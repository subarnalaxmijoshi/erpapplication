﻿Public Class PageAuthority

    Private Shared ReadOnly Dao As New DatabaseDao
    Private Shared sql As String
    ''' <summary>
    ''' Shared Function To check whether the loading page can be access by requesting user.
    ''' 
    ''' </summary>
    ''' <param name="userId">Login ID of a user</param>
    ''' <param name="absolutePath">Absolute path of a page being requested</param>
    ''' <returns>True if user has right to access else false</returns>
    ''' <remarks>Implementation still not done</remarks>

    Public Shared Function IsAuthorized(ByVal userId As String, ByVal absolutePath As String) As Boolean
        absolutePath = absolutePath.Replace("%20", " ")
        Dim hasAccess As Boolean = False
        Dim userType As String = String.Empty
        If String.IsNullOrWhiteSpace(userId) Then
            Return False
        End If

        If userId.Length > 0 Then
            userType = userId.Substring(0, 1)
        End If
        sql = "EXEC [Users].[usp_Menu] @Flag='authorization', @UserID='" & userId & "', @MenuLink='" & absolutePath & "', @UserType='" & userType & "'"
        Try
            Dim ds As New DataSet
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("URLExists") = "1" Then
                        hasAccess = True
                    Else
                        hasAccess = False
                    End If
                End If
            End If
            ds.Dispose()
        Catch ex As Exception
            hasAccess = False
        End Try
        Return hasAccess
    End Function

End Class
