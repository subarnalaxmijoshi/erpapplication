﻿Friend NotInheritable Class Helper
    Shared isBSDate As String = System.Configuration.ConfigurationManager.AppSettings("isBSDate").ToString

    Public Shared Function GetFrontToDbDate(ByVal ParamDate As String)

        If String.IsNullOrWhiteSpace(ParamDate) Then
            Return ParamDate
        End If

        Dim dd, mm, yy As String
        Dim dt As String = String.Empty
        Dim val As Array
        If isBSDate.ToUpper = "Y" Then
            val = ParamDate.Split("/")

            dd = val(0)
            mm = val(1)
            yy = val(2)
            dt = nep_to_eng(yy, mm, dd)
        Else
            dt = ParamDate
        End If

        Return dt
    End Function

    Public Shared Function GetDbToFrontDate(ByVal ParamDate As String)
        If String.IsNullOrWhiteSpace(ParamDate) Then
            Return ParamDate
        End If
        Dim dd, mm, yy, time As String
        Dim dt As String = String.Empty
        Dim val As Array
        If isBSDate.ToUpper = "Y" Then

            val = ParamDate.Split(" ")
            If val.Length > 1 Then
                time = val(1)
            End If

            val = ParamDate.Split("-")

            If val.Length > 1 Then
                dd = val(2)
                mm = val(1)
                yy = val(0)
            ElseIf val.Length = 1 Then
                val = ParamDate.Split("/")

                mm = val(0)
                dd = val(1)
                yy = val(2)
            End If
            If val.Length = 1 Then
                val = ParamDate.Split(".")
                dd = val(2)
                mm = val(1)
                yy = val(0)

            End If

            dt = eng_to_nep(yy, mm, dd)
        Else
            dt = ParamDate
        End If
        Dim arr() = dt.Split("/")
        Dim _dd = arr(2).ToString
        Dim _mm = arr(1).ToString
        Dim _yy = arr(0).ToString
        Return _dd & "/" & _mm & "/" & _yy

        '  Return dt & " " & time
    End Function
    Public Shared Function ConvertEngtoNepDate(ByVal ParamDate As String)
        If String.IsNullOrWhiteSpace(ParamDate) Then
            Return ParamDate
        End If
        Dim dd, mm, yy, time As String
        Dim dt As String = String.Empty
        Dim val As Array


        val = ParamDate.Split(" ")
        If val.Length > 1 Then
            time = val(1)
        End If

        val = ParamDate.Split("-")

        If val.Length > 1 Then
            dd = val(2)
            mm = val(1)
            yy = val(0)
        ElseIf val.Length = 1 Then
            val = ParamDate.Split("/")

            mm = val(0)
            dd = val(1)
            yy = val(2)
        End If
        If val.Length = 1 Then
            val = ParamDate.Split(".")
            dd = val(2)
            mm = val(1)
            yy = val(0)

        End If

        dt = eng_to_nep(yy, mm, dd)

        Return dt & " " & time
    End Function
    Public Shared Function GetNepali_now()
        Dim dd, mm, yy, time As String
        Dim dt As String = String.Empty
        Dim val As Array
        val = nepali_now().Split("/")
        mm = val(1)
        dd = val(2)
        yy = val(0)

        dt = dd & "/" & mm & "/" & yy
        Return dt
    End Function

    Public Shared Function ConvertNumberToRupees(ByVal amount As String) As String

        Dim rupees = "", paisa = "", temp = ""
        Dim decimalPlace, count
        Dim place(9) As String
        place(2) = " Thousand "
        place(3) = " Million "
        place(4) = " Billion "
        place(5) = " Trillion "

        ' String representation of amount.
        amount = amount.Trim()
        amount = amount.Replace(",", "")
        ' Position of decimal place 0 if none.
        decimalPlace = amount.IndexOf(".")
        ' Convert cents and set string amount to dollar amount.
        If decimalPlace > 0 Then
            paisa = GetTens(amount.Substring(decimalPlace + 1).PadRight(2, "0").Substring(0, 2))
            amount = amount.Substring(0, decimalPlace).Trim()
        End If

        count = 1
        Do While amount <> ""
            temp = GetHundreds(amount.Substring(Math.Max(amount.Length, 3) - 3))
            If temp <> "" Then rupees = temp & place(count) & rupees
            If amount.Length > 3 Then
                amount = amount.Substring(0, amount.Length - 3)
            Else
                amount = ""
            End If
            count = count + 1
        Loop

        Select Case rupees
            Case ""
                rupees = "No Rupees"
            Case "One"
                rupees = "One Rupees"
            Case Else
                rupees = rupees
        End Select

        If (Not String.IsNullOrWhiteSpace(paisa)) Then
            Select Case paisa
                Case ""
                    paisa = " and No Paisa"
                Case "One"
                    paisa = " and One Paisa"
                Case Else
                    paisa = " and " & paisa & " Paisa"
            End Select
        Else
            'paisa = " "
        End If


        ConvertNumberToRupees = rupees & paisa & "Only."
    End Function
    Private Shared Function GetHundreds(ByVal amount As String) As String
        Dim Result As String = ""
        If Not Integer.Parse(amount) = 0 Then
            amount = amount.PadLeft(3, "0")
            ' Convert the hundreds place.
            If amount.Substring(0, 1) <> "0" Then
                Result = GetDigit(amount.Substring(0, 1)) & " Hundred "
            End If
            ' Convert the tens and ones place.
            If amount.Substring(1, 1) <> "0" Then
                Result = Result & GetTens(amount.Substring(1))
            Else
                Result = Result & GetDigit(amount.Substring(2))
            End If
            GetHundreds = Result
        End If
        Return Result
    End Function

    ' Converts a number from 10 to 99 into text.
    Private Shared Function GetTens(ByRef TensText As String) As String
        Dim Result As String
        Result = ""           ' Null out the temporary function value.
        If TensText.StartsWith("1") Then   ' If value between 10-19...
            Select Case Integer.Parse(TensText)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else                                 ' If value between 20-99...
            Select Case Integer.Parse(TensText.Substring(0, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select
            Result = Result & GetDigit(TensText.Substring(1, 1))  ' Retrieve ones place.
        End If
        GetTens = Result
    End Function

    ' Converts a number from 1 to 9 into text.
    Private Shared Function GetDigit(ByRef Digit As String) As String
        Select Case Integer.Parse(Digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function

End Class
