﻿Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Globalization
Imports System.IO
Imports System.Web.HttpContext

Public Class GlobalStudentCertificate
    Private Dao As New DatabaseDao
    Dim DC As New DateConverter()

    Public Sub GenerateTransferPDF(ByVal StudentID As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim ds As New DataSet
        Dim sql As String
        Dim printstatus As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)
        End If

        sql = "exec [Students].[usp_CharacterCertificate] @Flag='s',@StudentID='" & StudentID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then

            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4.Rotate(), 20.0F, 20.0F, 20.0F, 20.0F)
            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.8, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14.0, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 28.0, 1)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 26.0, 1)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 3.0, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width

            'Report Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim reportHeader As PdfPTable = New PdfPTable(1)
            reportHeader.SpacingBefore = 35.0F
            reportHeader.SpacingAfter = 25.0F
            reportHeader.TotalWidth = pageWidth - 100
            reportHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 20.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Slogan").ToString() & Environment.NewLine, SchoolSlogan)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Tel:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & ",Email:" & institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString() & Environment.NewLine, SchoolContact)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Website:" & institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString() & Environment.NewLine, SchoolContact)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk("Transfer Certificate" & Environment.NewLine, PageHeader).SetUnderline(2, -3)))
            'parag.Add(New Paragraph(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(0.0F, 25.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportHeader.AddCell(cell)
            pdfDoc.Add(reportHeader)

            'Image Logo insertion
            Try
                'Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(35.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 70, pdfDoc.PageSize.Height - 110.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception
                Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/img_not_found.gif")
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(25.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - logo.Width - 185, pdfDoc.PageSize.Height - 109.0F)
                pdfDoc.Add(logo)
            End Try

            'Bar code display
            Try
                'Dim sql2 As String = "EXEC Library.usp_PrintBarcode @Flag='Barcode',@SearchBy='Date',@Blank='0',@Value1='2014-03-12',@Value2='2018-04-03',@Mark='1'"
                'Dim ds1 As New DataSet
                'ds1 = Dao.ExecuteDataset(sql2)
                'If (ds1.Tables(0).Rows.Count > 0) Then

                'Dim FontSize As Double = ds1.Tables(1).Rows(0)("BarcodeFontSize")
                'Dim CellHeight As Double = ds1.Tables(1).Rows(0)("BarcodeCellHeight")
                'Dim PaddingLeft As Integer = ds1.Tables(1).Rows(0)("BarcodePaddingLeft")
                'Dim PaddingRight As Integer = ds1.Tables(1).Rows(0)("BarcodePaddingRight")
                'Dim PaddingTop As Integer = ds1.Tables(1).Rows(0)("BarcodePaddingTop")
                'Dim PaddingBottom As Integer = ds1.Tables(1).Rows(0)("BarcodePaddingBottom")
                'Dim TableWidth As Integer = ds1.Tables(1).Rows(0)("BarcodeTableWidth")
                'Dim PageLeft As Double = ds1.Tables(1).Rows(0)("BarcodePageLeft")
                'Dim PageRight As Double = ds1.Tables(1).Rows(0)("BarcodePageRight")
                'Dim PageTop As Double = ds1.Tables(1).Rows(0)("BarcodePageTop")
                'Dim PageBottom As Double = ds1.Tables(1).Rows(0)("BarcodePageBottom")
                'Dim Border As Boolean = ds1.Tables(1).Rows(0)("BarcodeBorder")
                'Dim BHeight As Integer = ds1.Tables(1).Rows(0)("BarcodeHeight")
                'Dim BWidth As Integer = ds1.Tables(1).Rows(0)("BarcodeWidth")
                'Dim label As String = ds1.Tables(1).Rows(0)("DisplayLabel")
                'Dim barnormalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 4.0F, 0)
                'Dim barcodevalue As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString

                'Dim fname As String = HttpContext.Current.Server.MapPath("/Library/Barcode/Barcode.pdf")

                'Dim barTable As PdfPTable = New PdfPTable(5)
                'barTable.HorizontalAlignment = Element.ALIGN_RIGHT
                'barTable.TotalWidth = pageWidth - 120
                'barTable.SpacingBefore = 5.0F
                'barTable.SpacingAfter = 2.0F
                ''barTable.WidthPercentage = 90

                'For i = 0 To 3
                '    cell = CellTextAlignment.ToLeft("", spacer)
                '    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                '    cell.BorderColorTop = BaseColor.WHITE
                '    barTable.AddCell(cell)
                'Next

                Dim code128 As Barcode128 = New Barcode128()
                code128.Code = ""
                code128.CodeType = Barcode128.CODE128
                Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
                'img.ScaleToFit(600.0F, 30.0F)
                img.ScaleToFitLineWhenOverflow = True
                img.ScaleToFitHeight = True
                img.ScalePercent(100.0F)
                img.SetAbsolutePosition(pdfDoc.PageSize.Width - 110, pdfDoc.PageSize.Height - 175.0F)
                pdfDoc.Add(img)
                'If String.IsNullOrEmpty(barcodevalue) Then
                '    cell.AddElement(New Phrase(""))
                'Else
                '    'cell.AddElement(New Phrase(label, barnormalFont))
                '    cell.AddElement(img)
                'End If

                'cell.FixedHeight = 15.0F
                'cell.PaddingLeft = 5.0F
                'cell.PaddingRight = 0.0F
                'cell.PaddingTop = 0.0F
                'cell.PaddingBottom = 0.0F
                ''cell.HorizontalAlignment = Element.ALIGN_CENTER
                'If Not Border Then
                '    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                '    cell.BorderColorTop = BaseColor.WHITE
                'End If

                'barTable.AddCell(img)
                'pdfDoc.Add(barTable)
                'End If
            Catch ex As Exception

            End Try

            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(3.8F)
                photo.ScaleToFit(80.0F, 80.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 130, pdfDoc.PageSize.Height - 130.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try


            Dim reportTitle As PdfPTable = New PdfPTable(1)
            reportTitle.SpacingBefore = 10.0F
            reportTitle.SpacingAfter = 20.0F
            reportTitle.TotalWidth = pageWidth - 100
            reportTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 20.0F)

            parag3.Add(New Paragraph(New Chunk("TRANSFER CERTIFICATE" & Environment.NewLine, PageHeader).SetUnderline(2, -3)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportTitle.AddCell(cell)
            pdfDoc.Add(reportTitle)
            'End of report header

            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()

            Dim regNo As PdfPTable = New PdfPTable(3)
            Dim phrase1 As New Phrase()
            Dim para1 As New Paragraph
            regNo.HorizontalAlignment = Element.ALIGN_LEFT
            regNo.TotalWidth = pageWidth - 100
            regNo.SetWidths(columnwidths)
            para1.SetLeading(0.0F, 10.0F)
            para1.Add(New Paragraph(New Chunk(" TU.Regd.No ", normalFont)))
            para1.Add(New Phrase(New Chunk(ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString, raiseFont)))
            para1.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer1)))
            para1.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(para1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            regNo.AddCell(cell)
            cell = CellTextAlignment.ToLeft("", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            regNo.AddCell(cell)
            cell = CellTextAlignment.ToLeft("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            regNo.AddCell(cell)
            cell = CellTextAlignment.ToLeft("", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            regNo.AddCell(cell)
            cell = CellTextAlignment.ToLeft("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            regNo.AddCell(cell)
            cell = CellTextAlignment.ToLeft("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            regNo.AddCell(cell)
            pdfDoc.Add(regNo)


            'Content
            Dim greeting As String = ds.Tables(0).Rows(0).Item("Greeting").ToString.Trim()
            Dim studentName As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim()
            Dim child As String = ds.Tables(0).Rows(0).Item("child").ToString.Trim()
            Dim fatherName As String = ds.Tables(0).Rows(0).Item("FathersName").ToString.Trim()
            Dim address As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim dateFrom As String = ds.Tables(0).Rows(0).Item("DateFrom").ToString.Trim()
            Dim dateTo As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim aName As String = ds.Tables(0).Rows(0).Item("Aname").ToString.Trim()
            Dim levelTitle As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim facultyTitle As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim datePassed As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim division As String = "First" 'no field in db
            Dim dueAddressing As String = ds.Tables(0).Rows(0).Item("Naddressing").ToString.Trim()
            Dim pronounAddressing As String = ds.Tables(0).Rows(0).Item("wish").ToString.Trim()

            Dim rptContent As PdfPTable = New PdfPTable(1)
            rptContent.TotalWidth = pageWidth - 100
            rptContent.SpacingBefore = 10.0F
            rptContent.SpacingAfter = 20.0F
            Dim ph1 As New Phrase()
            Dim para2 As New Paragraph
            rptContent.HorizontalAlignment = Element.ALIGN_CENTER
            para2.SetLeading(0.0F, 40.0F)
            'ph1.SetLeading(5.0F, 4.0F)
            ph1.Add(New Chunk("Certified that ", normalFont1))
            ph1.Add(New Chunk(greeting & " " & studentName & " " & child, certified))
            ph1.Add(New Chunk(" of shree ", normalFont1))
            ph1.Add(New Chunk(fatherName, certified))
            ph1.Add(New Chunk(" an inhabitant of ", normalFont1))
            ph1.Add(New Chunk(address & ",", certified))
            ph1.Add(New Chunk(" was the student of this college/school from ", normalFont1))
            ph1.Add(New Chunk(dateFrom, certified))
            ph1.Add(New Chunk(" to ", normalFont1))
            ph1.Add(New Chunk(dateTo, certified))
            ph1.Add(New Chunk(". ", normalFont1))
            ph1.Add(New Chunk(aName, certified))
            ph1.Add(New Chunk(" passed the ", normalFont1))
            para2.Add(ph1)
            'para2.Add(New Phrase(New Chunk("Certified that ", normalFont1)))
            'para2.Add(New Phrase(New Chunk(greeting & " " & studentName & " " & child, certified)))
            'para2.Add(New Phrase(New Chunk(" of shree ", normalFont1)))
            'para2.Add(New Phrase(New Chunk(fatherName, certified)))
            'para2.Add(New Phrase(New Chunk(" an inhabitant of ", normalFont1)))

            'para2.Add(New Phrase(New Chunk(address & ",", certified)))
            'para2.Add(New Phrase(New Chunk(" was the student of this college/school from ", normalFont1)))
            'para2.Add(New Phrase(New Chunk(dateFrom, certified)))
            'para2.Add(New Phrase(New Chunk(" to ", normalFont1)))
            'para2.Add(New Phrase(New Chunk(dateTo, certified)))
            'para2.Add(New Phrase(New Chunk(". ", normalFont1)))
            'para2.Add(New Phrase(New Chunk(aName, certified)))
            'para2.Add(New Phrase(New Chunk(" passed the ", normalFont1)))

            para2.Add(New Phrase(New Chunk(levelTitle & " level in ", normalFont1)))
            para2.Add(New Phrase(New Chunk(facultyTitle, certified)))
            para2.Add(New Phrase(New Chunk(" in the year ", normalFont1)))
            para2.Add(New Phrase(New Chunk(datePassed, certified)))
            para2.Add(New Phrase(New Chunk(" in ", normalFont1)))
            para2.Add(New Phrase(New Chunk(division, certified)))
            para2.Add(New Phrase(New Chunk(" division. All the sum due by ", normalFont1)))

            para2.Add(New Phrase(New Chunk(dueAddressing, certified)))
            para2.Add(New Phrase(New Chunk(" to the institution have been paid. The college has no objection upon ", normalFont1)))
            para2.Add(New Phrase(New Chunk(pronounAddressing, certified)))
            para2.Add(New Phrase(New Chunk(" transfer. ", normalFont1)))
            para2.Add(New Phrase(New Chunk(aName, certified)))
            'para2.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            para2.Add(New Phrase(New Chunk(" bears a good moral character.", normalFont1)))

            cell = CellTextAlignment.ToCenter(para2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED
            rptContent.AddCell(cell)
            pdfDoc.Add(rptContent)

            columnWidth.Clear()
            columnWidth.Add(20.0F)
            columnWidth.Add(60.0F)
            columnWidth.Add(20.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()


            Dim rptApproval As PdfPTable = New PdfPTable(3)
            rptApproval.TotalWidth = pageWidth - 200
            rptApproval.SpacingBefore = 15.0F
            rptApproval.SpacingAfter = 20.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)
            Dim p As New Paragraph
            p.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))))
            cell = CellTextAlignment.ToCenter(p)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter(" ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter(p)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToLeft("Checked By", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter(" ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Campus Chief ", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            pdfDoc.Add(rptApproval)

            Dim pageBorderRectangle As New iTextSharp.text.Rectangle(pdfDoc.PageSize)
            pageBorderRectangle.Left += pdfDoc.LeftMargin
            pageBorderRectangle.Right -= pdfDoc.RightMargin
            pageBorderRectangle.Top -= pdfDoc.TopMargin
            pageBorderRectangle.Bottom += pdfDoc.BottomMargin
            contentByte.SetColorStroke(BaseColor.BLACK)
            contentByte.Rectangle(pageBorderRectangle.Left, pageBorderRectangle.Bottom, pageBorderRectangle.Width, pageBorderRectangle.Height)
            contentByte.Stroke()

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Transfer.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

#Region "Character Certificate Designs"
    Public Sub GenerateCharacterPDF(ByVal cStudenID As String, Optional ByVal branchId As String = "", Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim ds As New DataSet
        Dim sql As String
        Dim printstatus As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)
        End If

        sql = "exec [Students].[usp_CharacterCertificate] @Flag='s',@StudentID='" & cStudenID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4.Rotate(), 20.0F, 20.0F, 20.0F, 20.0F)
            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.8, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14.0, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 28.0, 1)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 26.0, 1)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 3.0, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width
            'Report Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(60.0F)
            columnWidth.Add(30.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            Dim topTable As PdfPTable = New PdfPTable(3)
            topTable.SpacingBefore = 30.0F
            topTable.SpacingAfter = 0.0F
            topTable.TotalWidth = pageWidth - 100
            topTable.LockedWidth = True
            cell = CellTextAlignment.ToLeft("S.No 5284", SchoolContact)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            cell = CellTextAlignment.ToLeft("", SchoolContact)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            cell = CellTextAlignment.ToRight("Date:" & System.DateTime.Now.ToString("yyyy/MM/dd"), SchoolContact)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            Dim reportHeader As PdfPTable = New PdfPTable(1)
            reportHeader.SpacingBefore = 20.0F
            reportHeader.SpacingAfter = 25.0F
            reportHeader.TotalWidth = pageWidth - 100
            reportHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 20.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Slogan").ToString() & Environment.NewLine, SchoolSlogan)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Tel:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & ",Email:" & institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString() & Environment.NewLine, SchoolContact)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Website:" & institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString() & Environment.NewLine, SchoolContact)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportHeader.AddCell(cell)
            pdfDoc.Add(reportHeader)

            'Image Logo insertion
            Try
                'Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(35.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 70, pdfDoc.PageSize.Height - 150.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception
                Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/img_not_found.gif")
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(25.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - logo.Width - 185, pdfDoc.PageSize.Height - 109.0F)
                pdfDoc.Add(logo)
            End Try

            'Bar code display
            Try
                'Dim barnormalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0F, 0)
                'Dim barcodevalue As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                'Dim barTable As PdfPTable = New PdfPTable(5)
                'barTable.HorizontalAlignment = Element.ALIGN_RIGHT
                'barTable.TotalWidth = pageWidth - 90
                'barTable.SpacingBefore = 2.0F
                'barTable.SpacingAfter = 2.0F
                ''barTable.WidthPercentage = 90
                'For i = 0 To 3
                '    cell = CellTextAlignment.ToLeft("", PageHeader)
                '    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                '    cell.BorderColorTop = BaseColor.WHITE
                '    barTable.AddCell(cell)
                'Next

                Dim code128 As Barcode128 = New Barcode128()
                code128.Code = ""
                code128.CodeType = Barcode128.CODE128
                Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
                img.ScaleToFit(600.0F, 30.0F)
                img.ScaleToFitLineWhenOverflow = True
                img.ScaleToFitHeight = True
                'img.ScalePercent(100.0F)
                img.SetAbsolutePosition(pdfDoc.PageSize.Width - 110, pdfDoc.PageSize.Height - 195.0F)
                pdfDoc.Add(img)

                'If String.IsNullOrEmpty(barcodevalue) Then
                '    cell.AddElement(New Phrase(""))
                'Else
                '    'cell.AddElement(New Phrase("ERA", barnormalFont))
                '    cell.AddElement(img)
                'End If
                'cell.FixedHeight = 60.0F
                'cell.PaddingLeft = 15.0F
                'cell.PaddingRight = 10.0F
                'cell.PaddingTop = 0.0F
                'cell.PaddingBottom = 0.0F
                'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                'cell.BorderColorTop = BaseColor.WHITE
                'barTable.AddCell(cell)
                'pdfDoc.Add(barTable)
            Catch ex As Exception

            End Try
            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(3.5F)
                photo.ScaleToFit(80.0F, 80.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 130, pdfDoc.PageSize.Height - 150.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try
            Dim reportTitle As PdfPTable = New PdfPTable(1)
            reportTitle.SpacingBefore = 10.0F
            reportTitle.SpacingAfter = 20.0F
            reportTitle.TotalWidth = pageWidth - 100
            reportTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 20.0F)

            parag3.Add(New Paragraph(New Chunk("CHARACTER CERTIFICATE" & Environment.NewLine, PageHeader).SetUnderline(2, -3)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportTitle.AddCell(cell)
            pdfDoc.Add(reportTitle)
            'End of report header

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(40.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()



            'Content
            Dim greeting As String = ds.Tables(0).Rows(0).Item("Greeting").ToString.Trim()
            Dim studentName As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim()
            Dim child As String = ds.Tables(0).Rows(0).Item("child").ToString.Trim()
            Dim fatherName As String = ds.Tables(0).Rows(0).Item("FathersName").ToString.Trim()
            Dim address As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim dateFrom As String = ds.Tables(0).Rows(0).Item("DateFrom").ToString.Trim()
            Dim dateTo As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim aName As String = ds.Tables(0).Rows(0).Item("Aname").ToString.Trim()
            Dim levelTitle As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim facultyTitle As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim datePassed As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim division As String = "First" 'no field in db
            Dim dueAddressing As String = ds.Tables(0).Rows(0).Item("Naddressing").ToString.Trim()
            Dim pronounAddressing As String = ds.Tables(0).Rows(0).Item("wish").ToString.Trim()
            Dim dateOfBirth As String = ds.Tables(0).Rows(0).Item("DateOfBirth").ToString
            Dim tuRegNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString

            Dim sql2 As String = "Exec [Setup].[usp_Board] @flag='p',@BranchID='" & branchId & "'"
            Dim ds2 As New DataSet
            ds2 = Dao.ExecuteDataset(sql2)
            Dim boardTitle As String = ds2.Tables(0).Rows(0).Item("BoardTitle").ToString

            Dim rptContent As PdfPTable = New PdfPTable(1)
            rptContent.TotalWidth = pageWidth - 100
            rptContent.SpacingBefore = 10.0F
            rptContent.SpacingAfter = 20.0F
            Dim ph1 As New Phrase()
            Dim para2 As New Paragraph
            rptContent.HorizontalAlignment = Element.ALIGN_CENTER
            para2.SetLeading(0.0F, 40.0F)
            'ph1.SetLeading(60.0F,0.0F)
            ph1.Add(New Chunk("This is to cetify that on the successful completion of all the requirement prescribed by the ", normalFont1))
            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                ph1.Add(New Chunk(boardTitle, certified))
            Else
                ph1.Add(New Chunk("Tribhuvan University", certified))
            End If
            ph1.Add(New Chunk(", " & greeting & " " & studentName & " " & child, certified))
            ph1.Add(New Chunk(" of ", normalFont1))
            ph1.Add(New Chunk(fatherName, certified))
            ph1.Add(New Chunk(" has been declared to have passed the ", normalFont1))
            ph1.Add(New Chunk(levelTitle, certified))
            ph1.Add(New Chunk(" level in ", normalFont1))
            ph1.Add(New Chunk(facultyTitle, certified))
            ph1.Add(New Chunk(" in ", normalFont1))
            ph1.Add(New Chunk(datePassed, certified))
            ph1.Add(New Chunk(" and placed in ", normalFont1))
            ph1.Add(New Chunk(division, certified))
            ph1.Add(New Chunk(" division.", normalFont1))
            ph1.Add(New Chunk(" According to the campus record ", normalFont1))
            ph1.Add(New Chunk(pronounAddressing, certified))
            ph1.Add(New Chunk(" date of birth is ", normalFont1))
            ph1.Add(New Chunk(dateOfBirth, certified))
            ph1.Add(New Chunk(" and ", normalFont1))

            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                If boardTitle.Contains("National") Then
                    ph1.Add(New Chunk("N.E.B. Regd. No. ", normalFont1))
                Else
                    ph1.Add(New Chunk("T.U. Regd. No. ", normalFont1))
                End If
            Else
                ph1.Add(New Chunk("T.U. Regd. No. ", normalFont1))
            End If

            ph1.Add(New Chunk(tuRegNo, certified))
            ph1.Add(New Chunk("." & Environment.NewLine, normalFont1))
            ph1.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph1.Add(New Chunk("I know nothing against ", normalFont1))
            ph1.Add(New Chunk(pronounAddressing, certified))
            ph1.Add(New Chunk(" moral character.I wish ", normalFont1))
            ph1.Add(New Chunk(dueAddressing, certified))
            ph1.Add(New Chunk(" every success.", normalFont1))
            para2.Add(ph1)

            cell = CellTextAlignment.ToCenter(para2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED
            rptContent.AddCell(cell)
            pdfDoc.Add(rptContent)

            columnWidth.Clear()
            columnWidth.Add(20.0F)
            columnWidth.Add(40.0F)
            columnWidth.Add(20.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()


            Dim rptApproval As PdfPTable = New PdfPTable(3)
            rptApproval.TotalWidth = pageWidth - 200
            rptApproval.SpacingBefore = 15.0F
            rptApproval.SpacingAfter = 20.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)
            Dim p As New Paragraph
            Dim p2 As New Paragraph
            p.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))))

            cell = CellTextAlignment.ToCenter(p)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            p2.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 50.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))
            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter(p)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToLeft("Prepared By", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Checked By", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Campus Chief ", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            pdfDoc.Add(rptApproval)


            Dim pageBorderRectangle As New iTextSharp.text.Rectangle(pdfDoc.PageSize)
            pageBorderRectangle.Left += pdfDoc.LeftMargin
            pageBorderRectangle.Right -= pdfDoc.RightMargin
            pageBorderRectangle.Top -= pdfDoc.TopMargin
            pageBorderRectangle.Bottom += pdfDoc.BottomMargin
            contentByte.SetColorStroke(BaseColor.BLACK)
            contentByte.Rectangle(pageBorderRectangle.Left, pageBorderRectangle.Bottom, pageBorderRectangle.Width, pageBorderRectangle.Height)
            contentByte.Stroke()

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Character.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

    Public Sub JanaBhawanaCharacterPDF(ByVal cStudenID As String, Optional ByVal branchId As String = "", Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim ds, ds4 As New DataSet
        Dim sql, sql4 As String
        Dim printstatus, SymbolNo As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)
            sql4 = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            SymbolNo = ds4.Tables(0).Rows(0).Item("SymbolNo").ToString
        End If


        sql = "exec [Students].[usp_CharacterCertificate] @Flag='s',@StudentID='" & cStudenID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4.Rotate(), 20.0F, 20.0F, 20.0F, 20.0F)
            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1, BaseColor.BLUE)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.8, 0, BaseColor.BLUE)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, iTextSharp.text.Font.ITALIC, BaseColor.BLUE)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14.0, 1, BaseColor.BLUE)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 30.0, iTextSharp.text.Font.BOLD, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1, BaseColor.BLUE)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13.0, 1, BaseColor.BLUE)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11.0, 1, BaseColor.BLUE)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 26.0, iTextSharp.text.Font.BOLDITALIC, BaseColor.ORANGE)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 3, BaseColor.BLUE)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1, BaseColor.BLUE)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 3.0, 0, BaseColor.BLUE)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 0, BaseColor.BLUE)

            Dim pageWidth = pdfDoc.PageSize.Width
            'Report Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(60.0F)
            columnWidth.Add(30.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            Dim topTable As PdfPTable = New PdfPTable(3)
            topTable.SpacingBefore = 30.0F
            topTable.SpacingAfter = 0.0F
            topTable.TotalWidth = pageWidth - 100
            topTable.LockedWidth = True
            'cell = CellTextAlignment.ToLeft("Ref. No. ...........", SchoolContact)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'topTable.AddCell(cell)
            cell = CellTextAlignment.ToLeft("", SchoolContact)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            cell = CellTextAlignment.ToRight("Date:" & System.DateTime.Now.ToString("yyyy/MM/dd"), SchoolContact)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            Dim reportHeader As PdfPTable = New PdfPTable(1)
            reportHeader.SpacingBefore = 10.0F
            reportHeader.SpacingAfter = 25.0F
            reportHeader.TotalWidth = pageWidth - 100
            reportHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 20.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString().ToUpper() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Slogan").ToString() & Environment.NewLine, SchoolSlogan)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Estd. 1991" & Environment.NewLine, SchoolAddress)))
            'parag.Add(New Paragraph(New Chunk("Tel:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & ",Email:" & institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString() & ",Website:" & institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString() & Environment.NewLine, SchoolContact)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportHeader.AddCell(cell)
            pdfDoc.Add(reportHeader)

            'Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(35.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 70, pdfDoc.PageSize.Height - 150.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception
                Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/img_not_found.gif")
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(25.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - logo.Width - 185, pdfDoc.PageSize.Height - 109.0F)
                pdfDoc.Add(logo)
            End Try

            'Bar code display
            Try

                'Dim code128 As Barcode128 = New Barcode128()
                'code128.Code = ""
                'code128.CodeType = Barcode128.CODE128
                'Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
                'img.ScaleToFit(600.0F, 30.0F)
                'img.ScaleToFitLineWhenOverflow = True
                'img.ScaleToFitHeight = True
                ''img.ScalePercent(100.0F)
                'img.SetAbsolutePosition(pdfDoc.PageSize.Width - 110, pdfDoc.PageSize.Height - 195.0F)
                'pdfDoc.Add(img)
            Catch ex As Exception

            End Try
            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(3.5F)
                photo.ScaleToFit(80.0F, 80.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 130, pdfDoc.PageSize.Height - 150.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try
            Dim reportTitle As PdfPTable = New PdfPTable(1)
            reportTitle.SpacingBefore = 10.0F
            reportTitle.SpacingAfter = 20.0F
            reportTitle.TotalWidth = pageWidth - 100
            reportTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 20.0F)

            parag3.Add(New Paragraph(New Chunk(" CHARACTER CERTIFICATE " & Environment.NewLine, PageHeader).SetUnderline(2, -4)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportTitle.AddCell(cell)
            pdfDoc.Add(reportTitle)
            'End of report header

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(40.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()



            'Content
            Dim greeting As String = ds.Tables(0).Rows(0).Item("Greeting").ToString.Trim()
            Dim studentName As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim()
            Dim child As String = ds.Tables(0).Rows(0).Item("child").ToString.Trim()
            Dim fatherName As String = ds.Tables(0).Rows(0).Item("FathersName").ToString.Trim()
            Dim address As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim dateFrom As String = ds.Tables(0).Rows(0).Item("DateFrom").ToString.Trim()
            Dim dateTo As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim aName As String = ds.Tables(0).Rows(0).Item("Aname").ToString.Trim()
            Dim levelTitle As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim facultyTitle As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim courseTitle As String = ds.Tables(0).Rows(0).Item("CourseTitle").ToString.Trim()
            Dim datePassed As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim division As String = "First" 'no field in db
            Dim dueAddressing As String = ds.Tables(0).Rows(0).Item("Naddressing").ToString.Trim()
            Dim pronounAddressing As String = ds.Tables(0).Rows(0).Item("wish").ToString.Trim()
            Dim dateOfBirth As String = ds.Tables(0).Rows(0).Item("DateOfBirth").ToString
            Dim tuRegNo As String = ds.Tables(0).Rows(0).Item("UniversityRegdNo").ToString

            If String.IsNullOrWhiteSpace(tuRegNo) Then
                tuRegNo = "--------------"
            End If

            Dim sql2 As String = "Exec [Setup].[usp_Board] @flag='p',@BranchID='" & branchId & "'"
            Dim ds2 As New DataSet
            ds2 = Dao.ExecuteDataset(sql2)
            Dim boardTitle As String = ds2.Tables(0).Rows(0).Item("BoardTitle").ToString


            Dim rptContent As PdfPTable = New PdfPTable(1)
            rptContent.TotalWidth = pageWidth - 100
            rptContent.SpacingBefore = 10.0F
            rptContent.SpacingAfter = 20.0F
            Dim ph1, ph2 As New Phrase()
            Dim para2 As New Paragraph
            rptContent.HorizontalAlignment = Element.ALIGN_CENTER
            para2.SetLeading(0.0F, 40.0F)
            'ph1.SetLeading(60.0F,0.0F)
            ph1.Add(New Chunk("This is to cetify that Mr./Ms. ", normalFont1))
            ph1.Add(New Chunk(studentName, certified))
            ph1.Add(New Chunk(" , the son/daughter of ", normalFont1))
            ph1.Add(New Chunk(fatherName, certified))
            ph1.Add(New Chunk(" , a resident of ", normalFont1))
            ph1.Add(New Chunk(address, certified))
            ph1.Add(New Chunk(" has duly passed ", normalFont1))
            ph1.Add(New Chunk(courseTitle, certified))
            'ph1.Add(New Chunk(levelTitle, certified))
            'ph1.Add(New Chunk(" level in ", normalFont1))
            'ph1.Add(New Chunk(facultyTitle, certified))
            'ph1.Add(New Chunk(" in ", normalFont1))
            ph1.Add(New Chunk(" examination of ", normalFont1))

            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                ph1.Add(New Chunk(boardTitle, certified))
            Else
                ph1.Add(New Chunk("Tribhuvan University", certified))
            End If
            ph1.Add(New Chunk(" held in ", normalFont1))
            ph1.Add(New Chunk(datePassed, certified))
            ph1.Add(New Chunk(" in ", normalFont1))
            ph1.Add(New Chunk(division, certified))
            ph1.Add(New Chunk(" division. ", normalFont1))

            ph1.Add(New Chunk("His/Her conduct as the student of this campus was ", normalFont1))
            ph1.Add(New Chunk("good. ", certified))
            ph1.Add(New Chunk("His/Her ", normalFont1))
            ph1.Add(New Chunk(" date of birth, according to campus record is ", normalFont1))
            ph1.Add(New Chunk(dateOfBirth, certified))
            ph1.Add(New Chunk("." & Environment.NewLine, normalFont1))

            ph1.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph1.Add(New Chunk("    I wish  every success in his/her ", normalFont1))
            ph1.Add(New Chunk(" future endeavors.", normalFont1))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph2.Add(New Chunk("Symbol No. ", certified))
            ph2.Add(New Chunk(SymbolNo, certified))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer1))
            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                If boardTitle.Contains("National") Then
                    ph2.Add(New Chunk("N.E.B. Regd. No. ", certified))
                Else
                    ph2.Add(New Chunk("T.U. Regd. No. ", certified))
                End If
            Else
                ph2.Add(New Chunk("T.U. Regd. No. ", certified))
            End If

            'ph2.Add(New Chunk(" ......................... ", normalFont1))
            ph2.Add(New Chunk(tuRegNo, certified))

            para2.Add(ph1)
            para2.Add(ph2)

            cell = CellTextAlignment.ToCenter(para2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED
            rptContent.AddCell(cell)
            pdfDoc.Add(rptContent)

            columnWidth.Clear()
            columnWidth.Add(20.0F)
            'columnWidth.Add(40.0F)
            columnWidth.Add(20.0F)
            columnWidth.Add(20.0F)
            columnWidth.Add(20.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()


            Dim rptApproval As PdfPTable = New PdfPTable(4)
            rptApproval.TotalWidth = pageWidth - 200
            rptApproval.SpacingBefore = 20.0F
            rptApproval.SpacingAfter = 15.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 4
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 4
            rptApproval.AddCell(cell)
            Dim p As New Paragraph
            Dim p2 As New Paragraph
            p.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 80.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))))
            p2.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 80.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)


            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToLeft("Date of Issue", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Prepared By", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Campus Seal", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Campus Chief ", normalFont1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            pdfDoc.Add(rptApproval)


            Dim pageBorderRectangle As New iTextSharp.text.Rectangle(pdfDoc.PageSize)
            pageBorderRectangle.Left += pdfDoc.LeftMargin
            pageBorderRectangle.Right -= pdfDoc.RightMargin
            pageBorderRectangle.Top -= pdfDoc.TopMargin
            pageBorderRectangle.Bottom += pdfDoc.BottomMargin
            contentByte.SetColorStroke(BaseColor.BLACK)
            contentByte.Rectangle(pageBorderRectangle.Left, pageBorderRectangle.Bottom, pageBorderRectangle.Width, pageBorderRectangle.Height)
            contentByte.Stroke()

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Character.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

    Public Sub CharacterPDF_Portrait(ByVal cStudenID As String, Optional ByVal branchId As String = "", Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim ds As New DataSet
        Dim ds4 As New DataSet
        Dim sql As String
        Dim printstatus, printedDate, dateofIssue, stdDivision As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            printedDate = ds4.Tables(0).Rows(0).Item("PrintedDate").ToString.Replace("-", "/")
            Dim issueDate As String() = printedDate.Split("/")
            dateofIssue = eng_to_nep(issueDate(0), issueDate(1), issueDate(2))
            stdDivision = ds4.Tables(0).Rows(0).Item("Division").ToString
        End If

        sql = "exec [Students].[usp_CharacterCertificate] @Flag='s',@StudentID='" & cStudenID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14.0, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 28.0, 1)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 26.0, 1)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, iTextSharp.text.Font.BOLDITALIC)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 35.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 0)
            Dim spacer3 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 110.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            'Background Image
            Dim pgWidth = pdfDoc.PageSize.Width
            Dim pgHeight = pdfDoc.PageSize.Height
            Dim imgbackgroundUrl As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CharacterBackground.jpg")) Then
                imgbackgroundUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CharacterBackground.jpg")
            End If
            Dim backgroundImg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgbackgroundUrl))
            'backgroundImg.Alignment = iTextSharp.text.Image.UNDERLYING
            backgroundImg.ScaleToFit(pgWidth, pgHeight)
            backgroundImg.SetAbsolutePosition(0.0F, 0.0F)
            pdfDoc.Add(backgroundImg)


            columnWidth.Clear()
            'columnWidth.Add(30.0F)
            'columnWidth.Add(60.0F)
            'columnWidth.Add(30.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            Dim topTable As PdfPTable = New PdfPTable(1)
            topTable.SpacingBefore = 60.0F
            topTable.SpacingAfter = 0.0F
            topTable.TotalWidth = pageWidth - 100
            topTable.LockedWidth = True

            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            cell = CellTextAlignment.ToLeft("S.N. :08-074-075", SchoolContact)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)

            'cell = CellTextAlignment.ToLeft("", SchoolContact)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'topTable.AddCell(cell)
            'cell = CellTextAlignment.ToRight("Date:" & System.DateTime.Now.ToString("yyyy/MM/dd"), SchoolContact)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'topTable.AddCell(cell)
            pdfDoc.Add(topTable)


            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                photo.ScaleToFit(84.0F, 98.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 146, pdfDoc.PageSize.Height - 604.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(40.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()

            'Content
            Dim greeting As String = ds.Tables(0).Rows(0).Item("Greeting").ToString.Trim()
            Dim studentName As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim()
            Dim child As String = ds.Tables(0).Rows(0).Item("child").ToString.Trim()
            Dim fatherName As String = ds.Tables(0).Rows(0).Item("FathersName").ToString.Trim()
            Dim address As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim dateFrom As String = ds.Tables(0).Rows(0).Item("DateFrom").ToString.Trim()
            Dim dateTo As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim aName As String = ds.Tables(0).Rows(0).Item("Aname").ToString.Trim()
            Dim levelTitle As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim facultyTitle As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim courseTitle As String = ds.Tables(0).Rows(0).Item("CourseTitle").ToString.Trim()
            Dim courseCode As String = ds.Tables(0).Rows(0).Item("CourseCode").ToString.Trim()
            Dim datePassed As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim division As String = stdDivision
            Dim dueAddressing As String = ds.Tables(0).Rows(0).Item("Naddressing").ToString.Trim()
            Dim pronounAddressing As String = ds.Tables(0).Rows(0).Item("wish").ToString.Trim()
            Dim dateOfBirth As String = ds.Tables(0).Rows(0).Item("DateOfBirth").ToString.Replace("-", "/")
            Dim birthmiti As String

            If Not String.IsNullOrWhiteSpace(dateOfBirth) Then
                Dim miti As String() = dateOfBirth.Split("/")
                birthmiti = eng_to_nep(miti(0), miti(1), miti(2))
                If birthmiti.Contains("False") Or IsNothing(birthmiti) Then
                    birthmiti = " "
                End If
            End If

            Dim tuRegNo As String = ds.Tables(0).Rows(0).Item("UniversityRegdNo").ToString
            Dim rollNo As String = ds.Tables(0).Rows(0).Item("RollNo").ToString

            Dim sql2 As String = "Exec [Setup].[usp_Board] @flag='p',@BranchID='" & branchId & "'"
            Dim ds2 As New DataSet
            ds2 = Dao.ExecuteDataset(sql2)
            Dim boardTitle As String = ds2.Tables(0).Rows(0).Item("BoardTitle").ToString

            Dim rptContent As PdfPTable = New PdfPTable(1)
            rptContent.TotalWidth = pageWidth - 100
            rptContent.SpacingBefore = 120.0F
            rptContent.SpacingAfter = 40.0F
            Dim ph1, ph2 As New Phrase()
            Dim para2 As New Paragraph
            rptContent.HorizontalAlignment = Element.ALIGN_CENTER
            para2.SetLeading(0.0F, 35.0F)
            'ph1.SetLeading(60.0F,0.0F)
            'ph1.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph1.Add(New Chunk("This is to cetify that " & greeting, normalFont1))
            ph1.Add(New Chunk(" " & studentName & " ", certified))
            ph1.Add(New Chunk(child & " of Mr. ", normalFont1))
            ph1.Add(New Chunk(fatherName, certified))
            ph1.Add(New Chunk(" permanent resident of " & address, normalFont1))
            ph1.Add(New Chunk(" ,was a student of this campus from ", normalFont1))
            ph1.Add(New Chunk(dateFrom & " A.D. to " & dateTo & " A.D.", certified))
            ph1.Add(New Chunk(" in the faculty of ", normalFont1))
            ph1.Add(New Chunk(facultyTitle & ". ", certified))
            ph1.Add(New Chunk(aName & " has passed the final examination of ", normalFont1))

            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                If boardTitle.Contains("National") Then
                    ph1.Add(New Chunk(levelTitle & " (10+2)", certified))
                Else
                    ph1.Add(New Chunk(courseTitle & " (" & courseCode & ")", certified))
                End If
            End If
            ph1.Add(New Chunk(" level held in ", normalFont1))
            ph1.Add(New Chunk(datePassed & " A.D.", certified))
            ph1.Add(New Chunk(" under ", normalFont1))

            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                If boardTitle.Contains("National") Then
                    ph1.Add(New Chunk(boardTitle, certified))
                    ph1.Add(New Chunk(" with Cumulative Grade Point Average(CGPA) ", normalFont1))
                    ph1.Add(New Chunk(division & ". ", certified))
                Else
                    ph1.Add(New Chunk(boardTitle, certified))
                    ph1.Add(New Chunk(" and was placed in ", normalFont1))
                    ph1.Add(New Chunk(division, certified))
                    ph1.Add(New Chunk(" division. ", normalFont1))
                End If
            Else
                ph1.Add(New Chunk("Tribhuvan University", certified))
                ph1.Add(New Chunk(" and was placed in ", normalFont1))
                ph1.Add(New Chunk(division, certified))
                ph1.Add(New Chunk(" division. ", normalFont1))
            End If



            ph1.Add(New Chunk("During ", normalFont1))
            ph1.Add(New Chunk(pronounAddressing, normalFont1))
            ph1.Add(New Chunk(" studies at this campus, ", normalFont1))
            ph1.Add(New Chunk(pronounAddressing, normalFont1))
            ph1.Add(New Chunk(" character was good. We know nothing against ", normalFont1))
            ph1.Add(New Chunk(pronounAddressing & " character.", normalFont1))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            ph2.Add(New Chunk("We wish ", normalFont1))
            ph2.Add(New Chunk(dueAddressing, normalFont1))
            ph2.Add(New Chunk(" bright future.", normalFont1))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer))
            ph2.Add(New Chunk("Date of birth: ", normalFont1))
            ph2.Add(New Chunk(birthmiti & " B.S. (" & dateOfBirth & ") A.D.", certified))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer))
            If Not (String.IsNullOrWhiteSpace(boardTitle)) Then
                If boardTitle.Contains("National") Then
                    ph2.Add(New Chunk("N.E.B. Registration No.: ", normalFont1))
                Else
                    ph2.Add(New Chunk("T.U. Registration No.: ", normalFont1))
                End If
            Else
                ph2.Add(New Chunk("T.U. Registration No.: ", normalFont1))
            End If
            ph2.Add(New Chunk(tuRegNo, certified))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk("Campus Roll No.: ", normalFont1))
            ph2.Add(New Chunk(rollNo, certified))

            para2.Add(ph1)
            para2.Add(ph2)


            cell = CellTextAlignment.ToCenter(para2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED
            rptContent.AddCell(cell)
            pdfDoc.Add(rptContent)

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()


            Dim rptApproval As PdfPTable = New PdfPTable(3)
            rptApproval.TotalWidth = pageWidth - 190
            rptApproval.SpacingBefore = 0.0F
            rptApproval.SpacingAfter = 5.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths)

            cell = CellTextAlignment.ToRight("    ", spacer1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("Prepared By", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("Checked By", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("Campus Chief ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            'cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("    ", spacer2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter("   Date of Issue: " & dateofIssue, normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 3
            rptApproval.AddCell(cell)

            pdfDoc.Add(rptApproval)


            'Dim pageBorderRectangle As New iTextSharp.text.Rectangle(pdfDoc.PageSize)
            'pageBorderRectangle.Left += pdfDoc.LeftMargin
            'pageBorderRectangle.Right -= pdfDoc.RightMargin
            'pageBorderRectangle.Top -= pdfDoc.TopMargin
            'pageBorderRectangle.Bottom += pdfDoc.BottomMargin
            'contentByte.SetColorStroke(BaseColor.BLACK)
            'contentByte.Rectangle(pageBorderRectangle.Left, pageBorderRectangle.Bottom, pageBorderRectangle.Width, pageBorderRectangle.Height)
            'contentByte.Stroke()

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Character.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

    Public Sub Character_Butwal(ByVal cStudenID As String, Optional ByVal branchId As String = "", Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim ds As New DataSet
        Dim ds4 As New DataSet
        Dim sql As String
        Dim printstatus, printedDate, dateofIssue, stdDivision As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            printedDate = ds4.Tables(0).Rows(0).Item("PrintedDate").ToString.Trim
            'printedDate = ds4.Tables(0).Rows(0).Item("PrintedDate").ToString.Replace("-", "/")
            'Dim issueDate As String() = printedDate.Split("/")
            'dateofIssue = eng_to_nep(issueDate(0), issueDate(1), issueDate(2))
            dateofIssue = DC.ToBS(Convert.ToDateTime(printedDate), "n")
            stdDivision = ds4.Tables(0).Rows(0).Item("Division").ToString
        End If

        sql = "exec [Students].[usp_CharacterCertificate] @Flag='s',@StudentID='" & cStudenID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, 0)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, 1)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14.0, iTextSharp.text.Font.NORMAL)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 28.0, 1)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 26.0, 1)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14.0, iTextSharp.text.Font.BOLD)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 35.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 0)
            Dim spacer3 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 30.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width


            'Details
            Dim greeting As String = ds.Tables(0).Rows(0).Item("Greeting").ToString.Trim()
            Dim studentName As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim()
            Dim child As String = ds.Tables(0).Rows(0).Item("child").ToString.Trim()
            Dim fatherName As String = ds.Tables(0).Rows(0).Item("FathersName").ToString.Trim()
            Dim address As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim dateFrom As String = ds.Tables(0).Rows(0).Item("DateFrom").ToString.Trim()
            Dim dateTo As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim mitiFrom As String = ds.Tables(0).Rows(0).Item("MitiFrom").ToString.Trim()
            Dim mitiTo As String = ds.Tables(0).Rows(0).Item("MitiTo").ToString.Trim()
            Dim aName As String = ds.Tables(0).Rows(0).Item("Aname").ToString.Trim()
            Dim levelTitle As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim facultyTitle As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim courseTitle As String = ds.Tables(0).Rows(0).Item("CourseTitle").ToString.Trim()
            Dim courseCode As String = ds.Tables(0).Rows(0).Item("CourseCode").ToString.Trim()
            Dim datePassed As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim division As String = stdDivision
            Dim dueAddressing As String = ds.Tables(0).Rows(0).Item("Naddressing").ToString.Trim()
            Dim pronounAddressing As String = ds.Tables(0).Rows(0).Item("wish").ToString.Trim()
            Dim tuRegNo As String = ds.Tables(0).Rows(0).Item("UniversityRegdNo").ToString
            Dim rollNo As String = ds.Tables(0).Rows(0).Item("RollNo").ToString

            Dim dateOfBirth As String = ds.Tables(0).Rows(0).Item("DateOfBirth").ToString.Trim
            Dim birthmiti As String
            If Not String.IsNullOrWhiteSpace(dateOfBirth) Then
                birthmiti = DC.ToBS(Convert.ToDateTime(dateOfBirth), "n")
                If birthmiti.Contains("False") Or IsNothing(birthmiti) Then
                    birthmiti = " "
                End If
            End If
            Dim serialNo As String = printId

            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(40.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()

            Dim topTable As PdfPTable = New PdfPTable(3)
            topTable.SpacingBefore = 10.0F
            topTable.SpacingAfter = 10.0F
            topTable.TotalWidth = pageWidth - 100
            topTable.LockedWidth = True

            Dim p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18 As New Paragraph
            p2.SetLeading(0.0F, 10.0F)
            'p1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer3)))
            'cell = CellTextAlignment.ToCenter(p1)
            'cell.Rowspan = 1
            'cell.Colspan = 3
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'topTable.AddCell(cell)

            p2.Add(New Chunk("Campus Roll. No." & " : ", boldFont))
            p2.Add(New Chunk(rollNo, normalFont))
            cell = CellTextAlignment.ToCenter(p2)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p3.Add(New Chunk("Transcript No. : ", boldFont))
            p3.Add(New Chunk("93244062", normalFont))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p4.Add(New Chunk("T.U. Reg No. : ", boldFont))
            p4.Add(New Chunk(tuRegNo, normalFont))
            cell.Rowspan = 1
            cell.Colspan = 1
            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p5.Add(New Chunk("Name : ", boldFont))
            p5.Add(New Chunk(studentName.ToUpper(), normalFont))
            cell.Rowspan = 1
            cell.Colspan = 2
            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p6.Add(New Chunk("Parent's Name : ", boldFont))
            p6.Add(New Chunk(fatherName.ToUpper(), normalFont))
            cell = CellTextAlignment.ToCenter(p6)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p7.Add(New Chunk("Address : ", boldFont))
            p7.Add(New Chunk(address, normalFont))
            cell.Rowspan = 1
            cell.Colspan = 3
            cell = CellTextAlignment.ToCenter(p7)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p8.Add(New Chunk("Admission From  : ", boldFont))
            p8.Add(New Chunk(mitiFrom, normalFont))
            'p8.Add(New Chunk("          To : ", boldFont))
            'p8.Add(New Chunk(mitiTo, normalFont))
            cell = CellTextAlignment.ToCenter(p8)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p1.Add(New Chunk("To : ", boldFont))
            p1.Add(New Chunk(mitiTo, normalFont))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p9.Add(New Chunk("Class/Level : ", boldFont))
            p9.Add(New Chunk(levelTitle, normalFont))
            cell = CellTextAlignment.ToCenter(p9)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p10.Add(New Chunk("Passed Level : ", boldFont))
            p10.Add(New Chunk(levelTitle.Trim, normalFont))
            cell = CellTextAlignment.ToCenter(p10)
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p11.Add(New Chunk("Held in : ", boldFont))
            p11.Add(New Chunk(mitiTo & " B.S.", normalFont))
            p11.Add(New Chunk("    " & datePassed & " A.D", normalFont))
            cell = CellTextAlignment.ToCenter(p11)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p12.Add(New Chunk("Division : ", boldFont))
            p12.Add(New Chunk(division, normalFont))
            cell = CellTextAlignment.ToCenter(p12)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p13.Add(New Chunk("Date of birth : ", boldFont))
            p13.Add(New Chunk(birthmiti & " B.S.", normalFont))
            p13.Add(New Chunk("    (" & dateOfBirth & ") A.D", normalFont))
            cell = CellTextAlignment.ToCenter(p13)
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p14.Add(New Chunk("Date of Issue : ", boldFont))
            p14.Add(New Chunk(dateofIssue, normalFont))
            cell = CellTextAlignment.ToCenter(p14)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p15.Add(New Chunk("Serial No. : ", boldFont))
            p15.Add(New Chunk(serialNo, normalFont))
            cell = CellTextAlignment.ToCenter(p15)
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p16.Add(New Chunk("Prepared By : ", boldFont))
            p16.Add(New Chunk(" ", normalFont))
            cell = CellTextAlignment.ToCenter(p16)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p17.Add(New Chunk("Verified By : ", boldFont))
            p17.Add(New Chunk(" ", normalFont))
            cell = CellTextAlignment.ToCenter(p17)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p18.Add(New Chunk("Campus Chief : ", boldFont))
            p18.Add(New Chunk(" ", normalFont))
            cell = CellTextAlignment.ToCenter(p18)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            columnWidth.Clear()
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            Dim serialnoTable As PdfPTable = New PdfPTable(1)
            serialnoTable.SpacingBefore = 10.0F
            serialnoTable.SpacingAfter = 0.0F
            serialnoTable.TotalWidth = pageWidth - 150
            serialnoTable.LockedWidth = True

            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            serialnoTable.AddCell(cell)
            cell = CellTextAlignment.ToLeft(serialNo, boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            serialnoTable.AddCell(cell)
            pdfDoc.Add(serialnoTable)

            Dim rptContent As PdfPTable = New PdfPTable(1)
            rptContent.TotalWidth = pageWidth - 120
            rptContent.SpacingBefore = 200.0F
            rptContent.SpacingAfter = 10.0F
            rptContent.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL

            Dim ph1, ph2 As New Phrase()
            Dim para2 As New Paragraph
            ph1.SetLeading(30.0F, 0.0F)
            ph2.SetLeading(20.0F, 0.0F)

            ph1.Add(New Chunk("This is to cetify that " & greeting & " ", normalFont1))
            ph1.Add(New Chunk("    " & studentName.ToUpper & "         ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" " & child & " of Mr./Mrs. ", normalFont1))
            ph1.Add(New Chunk("    " & fatherName.ToUpper & "          ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" an inhabitant of ", normalFont1))
            ph1.Add(New Chunk("    " & address & "            ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" has/had attended this institution from B.S. ", normalFont1))
            ph1.Add(New Chunk("   " & mitiFrom & "   ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" to ", normalFont1))
            ph1.Add(New Chunk("   " & mitiTo & "   ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" A.D. ", normalFont1))
            ph1.Add(New Chunk("   " & dateFrom & "   ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" to ", normalFont1))
            ph1.Add(New Chunk("   " & dateTo & "   ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" . " & aName & " was  a student in the faculty of ", normalFont1))
            ph1.Add(New Chunk("         " & facultyTitle.ToUpper & "           ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" and passed the annual/semester examination of level ", normalFont1))
            ph1.Add(New Chunk("          " & levelTitle.ToUpper & "          ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" held in B.S. ", normalFont1))
            ph1.Add(New Chunk("   " & mitiTo & "   ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" A.D. ", normalFont1))
            ph1.Add(New Chunk("   " & datePassed & "   ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" and was place in ", normalFont1))
            ph1.Add(New Chunk("      " & division & "      ", certified).SetUnderline(0.8, -2))
            ph1.Add(New Chunk(" division. ", normalFont1))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            ph2.Add(New Chunk("      According to the campus record, ", normalFont1))
            ph2.Add(New Chunk(pronounAddressing, normalFont1))
            ph2.Add(New Chunk(" date of birth is B.S. ", normalFont1))
            ph2.Add(New Chunk("  " & birthmiti & "  ", certified).SetUnderline(0.8, -2))
            ph2.Add(New Chunk(" A.D. ", normalFont1))
            ph2.Add(New Chunk("  " & dateOfBirth & "", certified).SetUnderline(0.8, -2))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk("While in the Campus ", normalFont1))
            ph2.Add(New Chunk(pronounAddressing, normalFont1))
            ph2.Add(New Chunk(" character was good and satisfactory. ", normalFont1))


            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk("We wish ", normalFont1))
            ph2.Add(New Chunk(dueAddressing, normalFont1))
            ph2.Add(New Chunk(" success in future.", normalFont1))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            ph2.Add(New Chunk("Date of Issue : ", normalFont1))
            ph2.Add(New Chunk(dateofIssue, certified))
            ph2.Add(New Chunk("                                Campus Roll No. : ", normalFont1))
            ph2.Add(New Chunk(rollNo, certified))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk("Major Subject(s) : ", normalFont1))
            ph2.Add(New Chunk("Nepali,Education", certified))
            ph2.Add(New Chunk("                     T.U. Regd No. : ", normalFont1))
            ph2.Add(New Chunk(tuRegNo, certified))

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer3))
            ph2.Add(New Chunk("Transcript No.: ", normalFont1))
            ph2.Add(New Chunk("93244062", certified))

            para2.Add(ph1)
            para2.Add(ph2)

            cell = CellTextAlignment.ToCenter(para2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED
            rptContent.AddCell(cell)
            pdfDoc.Add(rptContent)


            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            Dim tblcolwidths2 As Integer()
            tblcolwidths2 = columnWidth.ToArray()
            Dim rptApproval As PdfPTable = New PdfPTable(4)
            rptApproval.TotalWidth = pageWidth - 120
            rptApproval.SpacingBefore = 0.0F
            rptApproval.SpacingAfter = 5.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths2)

            cell = CellTextAlignment.ToRight("    ", spacer1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 4
            rptApproval.AddCell(cell)

            Dim para3 As New Paragraph
            para3.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 80.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))

            cell = CellTextAlignment.ToCenter(para3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)


            cell = CellTextAlignment.ToCenter(para3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter(para3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter(para3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)


            cell = CellTextAlignment.ToCenter("Prepared By", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Verified By", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Asst. Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            'cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            pdfDoc.Add(rptApproval)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=CharacterCertificate.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

    Public Sub Character_SHAS(ByVal cStudenID As String, Optional ByVal branchId As String = "", Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim ds, ds2 As New DataSet
        Dim sql, sql2 As String
        Dim printstatus, printdate, dateofIssue, issueMitit, stdDivision, symbolNo, approvedDocNo As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)
            sql2 = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds2 = Dao.ExecuteDataset(sql2)
            printdate = ds2.Tables(0).Rows(0).Item("PrintedDate").ToString.Trim
            dateofIssue = Convert.ToDateTime(printdate).ToString("dd MMM yyyy")
            issueMitit = DC.ToBS(Convert.ToDateTime(dateofIssue), "n")
            stdDivision = ds2.Tables(0).Rows(0).Item("Division").ToString
            symbolNo = ds2.Tables(0).Rows(0).Item("SymbolNo").ToString
            approvedDocNo = ds2.Tables(0).Rows(0).Item("ApprovedDocNo").ToString
        End If

        sql = "exec [Students].[usp_CharacterCertificate] @Flag='s',@StudentID='" & cStudenID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            pdfDoc = New Document(PageSize.A4.Rotate(), 50.0F, 50.0F, 50.0F, 50.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent




            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15.4, 0)
            Dim italicboldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15.0, iTextSharp.text.Font.BOLDITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 15.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 20.0, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1, BaseColor.BLUE)
            Dim universityFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1, BaseColor.BLUE)
            Dim instituteFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 1, BaseColor.BLUE)
            Dim boldFont2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 26.0, 1)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 4.0, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 6.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 40.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width

            'Report Header
            Dim reportHeader As PdfPTable = New PdfPTable(1)
            reportHeader.SpacingBefore = 15.0F
            reportHeader.SpacingAfter = 15.0F
            reportHeader.TotalWidth = pageWidth - 100
            reportHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 20.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk("POKHARA UNIVERSITY" & Environment.NewLine, universityFont)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("FACULTY OF HEALTH SCIENCES" & Environment.NewLine, instituteFont)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("SCHOOL OF HEALTH AND ALLIED SCIENCES (SHAS)" & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("POKHARA-30, KASKI, NEPAL" & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportHeader.AddCell(cell)
            pdfDoc.Add(reportHeader)

            'Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(35.0F)
                logo.ScaleToFit(100.0F, 100.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 60, pdfDoc.PageSize.Height - 155.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception
                Dim imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/img_not_found.gif")
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(25.0F)
                logo.ScaleToFit(80.0F, 80.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - logo.Width - 185, pdfDoc.PageSize.Height - 90.0F)
                pdfDoc.Add(logo)
            End Try


            columnWidth.Clear()
            columnWidth.Add(30.0F)
            columnWidth.Add(60.0F)
            columnWidth.Add(30.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            Dim topTable As PdfPTable = New PdfPTable(3)
            topTable.SpacingBefore = 2.0F
            topTable.SpacingAfter = 5.0F
            topTable.TotalWidth = pageWidth - 120
            topTable.SetWidths(columnwidths)
            topTable.LockedWidth = True
            cell = CellTextAlignment.ToLeft("S.N.: " & printId, boldFont2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            cell = CellTextAlignment.ToLeft("", boldFont2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            cell = CellTextAlignment.ToRight("", boldFont2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(3.5F)
                photo.ScaleToFit(90.0F, 100.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 170, pdfDoc.PageSize.Height - 235.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try
            Dim reportTitle As PdfPTable = New PdfPTable(1)
            reportTitle.SpacingBefore = 5.0F
            reportTitle.SpacingAfter = 5.0F
            reportTitle.TotalWidth = pageWidth - 100
            reportTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 20.0F)

            parag3.Add(New Paragraph(New Chunk("CHARACTER CERTIFICATE" & Environment.NewLine, PageHeader)))
            parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            reportTitle.AddCell(cell)
            pdfDoc.Add(reportTitle)

            'Content
            Dim greeting As String = ds.Tables(0).Rows(0).Item("Greeting").ToString.Trim().ToUpper
            Dim studentName As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim().ToUpper
            Dim child As String = ds.Tables(0).Rows(0).Item("child").ToString.Trim()
            Dim fatherName As String = ds.Tables(0).Rows(0).Item("FathersName").ToString.Trim()
            Dim address As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim dateFrom As String = ds.Tables(0).Rows(0).Item("DateFrom").ToString.Trim()
            Dim dateTo As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim aName As String = ds.Tables(0).Rows(0).Item("Aname").ToString.Trim()
            Dim cAddressing As String = ds.Tables(0).Rows(0).Item("Caddressing").ToString.Trim()
            Dim levelTitle As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim facultyTitle As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim courseTitle As String = ds.Tables(0).Rows(0).Item("CourseTitle").ToString.Trim().ToUpper
            Dim datePassed As String = ds.Tables(0).Rows(0).Item("DateTo").ToString.Trim()
            Dim division As String = "First" 'no field in db
            Dim dueAddressing As String = ds.Tables(0).Rows(0).Item("Naddressing").ToString.Trim()
            Dim pronounAddressing As String = ds.Tables(0).Rows(0).Item("wish").ToString.Trim()
            Dim dateOfBirth As String = ds.Tables(0).Rows(0).Item("DateOfBirth").ToString.Trim
            Dim RegNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim
            Dim tuRegNo As String = ds.Tables(0).Rows(0).Item("UniversityRegdNo").ToString.Trim

            If String.IsNullOrWhiteSpace(stdDivision) Then
                stdDivision = ".... "
            End If

            columnWidth.Clear()
            columnWidth.Add(26.0F)
            columnWidth.Add(40.0F)
            columnWidth.Add(17.0F)
            columnWidth.Add(36.0F)
            Dim contentcolwidths As Integer()
            contentcolwidths = columnWidth.ToArray()

            Dim rptContent As PdfPTable = New PdfPTable(4)
            rptContent.TotalWidth = pageWidth - 122
            rptContent.SpacingBefore = 5.0F
            rptContent.SpacingAfter = 5.0F
            rptContent.HorizontalAlignment = Element.ALIGN_CENTER
            rptContent.SetWidths(contentcolwidths)
            rptContent.LockedWidth = True

            Dim para1, pg1, pg2 As New Paragraph
            Dim para2, pg3, pg10 As New Paragraph
            Dim para3, pg4, pg5 As New Paragraph
            Dim para4, pg6, pg7 As New Paragraph
            Dim para5, pg8, pg9 As New Paragraph

            para1.Add(New Chunk("This is to certify that ", normalFont))
            cell = CellTextAlignment.ToCenter(para1)
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptContent.AddCell(cell)

            pg1.Add(New Chunk(greeting & " " & studentName, italicboldFont))
            cell = CellTextAlignment.ToCenter(pg1)
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptContent.AddCell(cell)

            pg2.Add(New Chunk(" was a student of this school from ", normalFont))
            'pg2.Add(New Chunk(dateFrom, italicboldFont).SetUnderline(1.0F, -1))
            pg2.Add(New Chunk(" " & Environment.NewLine, spacer2))
            cell = CellTextAlignment.ToCenter(pg2)
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptContent.AddCell(cell)


            para2.Add(New Chunk(" " & dateFrom & "  ", italicboldFont).SetUnderline(0.5F, -2))
            cell = CellTextAlignment.ToCenter(para2)
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptContent.AddCell(cell)

            pg9.Add(New Chunk(" to         ", normalFont))
            pg9.Add(New Chunk("  " & dateTo & "  ", italicboldFont).SetUnderline(0.5F, -2))
            pg9.Add(New Chunk(". " & aName & " has completed all the ", normalFont))
            cell = CellTextAlignment.ToCenter(pg9)
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptContent.AddCell(cell)

            pg3.Add(New Chunk("requirements ", normalFont))
            pg3.Add(New Chunk("of the degree of ", normalFont))
            pg3.Add(New Chunk(" " & Environment.NewLine, spacer2))
            cell = CellTextAlignment.ToCenter(pg3)
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptContent.AddCell(cell)


            'pg10.Add(New Chunk("of the degree of ", normalFont))
            'cell = CellTextAlignment.ToCenter(pg10)
            'cell.Colspan = 1
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.BorderColorTop = BaseColor.WHITE
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'rptContent.AddCell(cell)

            para3.Add(New Chunk(courseTitle, italicboldFont))
            cell = CellTextAlignment.ToCenter(para3)
            cell.Colspan = 4
            cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptContent.AddCell(cell)

            pg4.Add(New Chunk("with a CGPA ", normalFont))
            pg4.Add(New Chunk(stdDivision & "/4.00. ", italicboldFont))
            pg4.Add(New Chunk(" " & Environment.NewLine, spacer2))
            cell = CellTextAlignment.ToCenter(pg4)
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptContent.AddCell(cell)

            para4.Add(New Chunk(cAddressing, normalFont))
            para4.Add(New Chunk(" date of birth according to record of this school is ", normalFont))
            cell = CellTextAlignment.ToCenter(para4)
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptContent.AddCell(cell)


            pg6.Add(New Chunk(dateOfBirth & " A.D.", italicboldFont).SetUnderline(0.5F, -2))
            pg6.Add(New Chunk(" " & cAddressing & " character ", normalFont))
            pg6.Add(New Chunk(" " & Environment.NewLine, spacer2))
            cell = CellTextAlignment.ToCenter(pg6)
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptContent.AddCell(cell)

            'pg7.Add(New Chunk(". " & cAddressing & " character as a student in ", normalFont))
            'pg7.Add(New Chunk(" " & Environment.NewLine, spacer2))
            'cell = CellTextAlignment.ToCenter(pg7)
            'cell.Colspan = 1
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.BorderColorTop = BaseColor.WHITE
            'cell.HorizontalAlignment = Element.ALIGN_RIGHT
            'rptContent.AddCell(cell)


            para5.Add(New Chunk("as a student in this school was ", normalFont))
            para5.Add(New Chunk("Good", italicboldFont))
            para5.Add(New Chunk(". I wish all the best for " & pronounAddressing & " bright future.", normalFont))
            cell = CellTextAlignment.ToCenter(para5)
            cell.Colspan = 4
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptContent.AddCell(cell)



            Dim ph1, ph2, ph3 As New Phrase()
            Dim para6 As New Paragraph

            ph1.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph1.Add(New Chunk("Transcript No.: ", boldFont))
            ph1.Add(New Chunk(approvedDocNo, normalFont))
            para6.Add(ph1)

            ph2.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph2.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph2.Add(New Chunk("Registration No.: ", boldFont))
            ph2.Add(New Chunk(tuRegNo, normalFont))
            para6.Add(ph2)

            ph3.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph3.Add(New Chunk(" " & Environment.NewLine, spacer1))
            ph3.Add(New Chunk("Exam Roll No.: ", boldFont))
            ph3.Add(New Chunk(symbolNo, normalFont))
            para6.Add(ph3)
            cell = CellTextAlignment.ToCenter(para6)
            cell.Colspan = 4
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            rptContent.AddCell(cell)

            pdfDoc.Add(rptContent)

            columnWidth.Clear()
            columnWidth.Add(20.0F)
            columnWidth.Add(20.0F)
            columnWidth.Add(20.0F)
            columnWidth.Add(20.0F)
            Dim tblcolwidths As Integer()
            tblcolwidths = columnWidth.ToArray()
            Dim rptApproval As PdfPTable = New PdfPTable(4)
            rptApproval.TotalWidth = pageWidth - 100
            rptApproval.SpacingBefore = 5.0F
            rptApproval.SpacingAfter = 5.0F
            rptApproval.LockedWidth = True
            rptApproval.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.SetWidths(tblcolwidths)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 4
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToRight("    ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 4
            rptApproval.AddCell(cell)
            Dim p As New Paragraph
            Dim p1, p2 As New Paragraph
            p.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 80.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))))
            p2.Add(New Phrase(New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(1.0F, 80.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1))))

            p1.Add(New Phrase(New Chunk(dateofIssue, normalFont)))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)


            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            rptApproval.AddCell(cell)

            cell = CellTextAlignment.ToLeft("Date of Issue", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Prepared By", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("School Seal", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            rptApproval.AddCell(cell)
            cell = CellTextAlignment.ToCenter("Director ", normalFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            rptApproval.AddCell(cell)
            pdfDoc.Add(rptApproval)





            Dim pageBorderRectangle As New iTextSharp.text.Rectangle(pdfDoc.PageSize)
            pageBorderRectangle.Left += pdfDoc.LeftMargin
            pageBorderRectangle.Right -= pdfDoc.RightMargin
            pageBorderRectangle.Top -= pdfDoc.TopMargin
            pageBorderRectangle.Bottom += pdfDoc.BottomMargin
            contentByte.SetColorStroke(BaseColor.BLUE)
            contentByte.Rectangle(pageBorderRectangle.Left, pageBorderRectangle.Bottom, pageBorderRectangle.Width, pageBorderRectangle.Height)
            contentByte.Stroke()
            contentByte.Rectangle(pageBorderRectangle.Left + 5.0F, pageBorderRectangle.Bottom + 5.0F, pageBorderRectangle.Width - 10.0F, pageBorderRectangle.Height - 10.0F)
            contentByte.Stroke()

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=CharacterCertificate.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

            ''Watermark
            ''Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
            ''Dim pdfStamper As PdfStamper
            'Dim underContent As iTextSharp.text.pdf.PdfContentByte = Nothing
            ''reader = New iTextSharp.text.pdf.PdfReader("C:/Users/user/Downloads/CharacterCertificate.pdf")
            ''pdfStamper = New iTextSharp.text.pdf.PdfStamper(reader, New System.IO.FileStream("C:/Users/user/Downloads/CharacterCertificate.pdf", System.IO.FileMode.Create))
            'Dim imageUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
            'Dim watermark As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl))
            'watermark.ScalePercent(20.0F)
            'watermark.ScaleToFit(80.0F, 80.0F)
            'watermark.SetAbsolutePosition(96.0F, 60.0F)
            ''contentByte = PdfStamper.GetUnderContent(1)
            'contentByte.AddImage(watermark)
            ''pdfStamper.Close()
            ''reader.Close()
        End If
    End Sub


#End Region

    
#Region "Student ID Card Designs"
    Public Sub StudentIDCard_PDF(ByVal studentId As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim printstatus As String
        Dim ds4 As New DataSet
        Dim validfrom, validto, barcode As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
            validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
            barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
        End If

        Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            'Dim pdfStamper As PdfStamper

            Dim mycard As New iTextSharp.text.Rectangle(192.0F, 120.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent

            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2.0, 0)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            'cardHeader.SpacingBefore = 1.0F
            'cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 28
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Ph:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))         
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(30.0F, 30.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'TU logo
            Try
                Dim imageUrl2 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                Dim logo2 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl2))
                'logo2.ScalePercent(35.0F)
                logo2.ScaleToFit(30.0F, 30.0F)
                logo2.SetAbsolutePosition(pdfDoc.PageSize.Width - 29.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo2)
            Catch ex As Exception

            End Try

            'Watermark
            'Dim imageUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
            'Dim watermark As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl))
            'watermark.ScalePercent(12.5F)
            'watermark.SetAbsolutePosition(96.0F, 60.0F)
            'contentByte = pdfStamper.GetUnderContent(1)
            'contentByte.AddImage(watermark)



            Dim cardTitle As PdfPTable = New PdfPTable(1)
            'cardTitle.SpacingBefore = 2.0F
            cardTitle.SpacingAfter = 1.5F
            cardTitle.TotalWidth = pageWidth - 28
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD" & Environment.NewLine, PageHeader).SetUnderline(1, -3)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

            'Details of student
            Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
            Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
            Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
            Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()


            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            columnWidth.Add(180.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5 As New Paragraph
            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(studentName, normalFont))


            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(stdAddress, normalFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p3.Add(New Chunk("Campus Regd No." & " : ", boldFont))
            p3.Add(New Chunk(RegdNo, normalFont))

            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p4.Add(New Chunk("Level" & " : ", boldFont))
            p4.Add(New Chunk(stdBatch, normalFont))
            p4.Add(New Chunk("  Faculty" & " : ", boldFont))
            p4.Add(New Chunk(stdFaculty, normalFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'cell = CellTextAlignment.ToLeft("Faculty" & " : " & stdFaculty, normalFont)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'stdtable.AddCell(cell)

            'cell = CellTextAlignment.ToLeft("Batch" & " : " & stdBatch, normalFont)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'stdtable.AddCell(cell)

            'p5.Add(New Chunk("Valid From" & " : ", boldFont))
            'p5.Add(New Chunk(validfrom, normalFont))
            p5.Add(New Chunk("Semester" & " : ", boldFont))
            p5.Add(New Chunk(stdSemester, normalFont))
            p5.Add(New Chunk(" Valid Upto" & " : ", boldFont))
            p5.Add(New Chunk(validto, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)

            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(20.0F)
                photo.ScaleToFit(40.0F, 50.0F)

                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 46.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 42.0F, pdfDoc.PageSize.Height - 110.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try

            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = barcode
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(80.0F, 18.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(60.0F, pdfDoc.PageSize.Height - 118.0F)
            pdfDoc.Add(img)




            columnWidth.Clear()
            Dim lastTbl As New PdfPTable(1)
            lastTbl.TotalWidth = pageWidth - 5
            lastTbl.LockedWidth = True
            columnWidth.Add(185.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()
            lastTbl.SetWidths(columnwidths2)
            lastTbl.HorizontalAlignment = Element.ALIGN_RIGHT

            'Dim pg As New Paragraph
            'pg.Add(New Chunk(" " & Environment.NewLine, spacer))
            'pg.Add(New Chunk("Campus Chief", boldFont))

            cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            lastTbl.AddCell(cell)
            pdfDoc.Add(lastTbl)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StudentIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    Public Sub IDCard_Portrait_Koteshwor(ByVal studentId As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim printstatus As String
        Dim ds4 As New DataSet
        Dim validfrom, validto, barcode, validDate As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
            validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
            Dim validity As String() = validto.Split("-")
            validDate = eng_to_nep(validity(0), validity(1), validity(2))
            barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
        End If

        Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            'Dim pdfStamper As PdfStamper

            Dim mycard As New iTextSharp.text.Rectangle(120.0F, 192.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            mycard.BackgroundColor = BaseColor.PINK

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent




            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 30.0, 0)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/IDCardLogo.png")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/IDCardLogo.png")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/logo.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(110.0F, 48.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            cardHeader.SpacingBefore = 1.0F
            cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 2
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & ", Tel.:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolAddress)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk("Ph:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))         
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)



            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(20.0F)
                photo.ScaleToFit(38.0F, 48.0F)

                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 80.0F, pdfDoc.PageSize.Height - 87.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 82.0F, pdfDoc.PageSize.Height - 97.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try


            'Stamp Logo
            Dim stampPath As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/Campus_Stamp.png")) Then
                stampPath = "~/assets/InstitutionLogo/Campus_Stamp.png"
            End If
            Dim stampUrl = HttpContext.Current.Server.MapPath(stampPath)
            If Not String.IsNullOrWhiteSpace(stampUrl) Then
                Dim stamp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(stampUrl))
                stamp.RotationDegrees = -30
                stamp.ScaleToFit(45.0F, 52.0F)
                stamp.SetAbsolutePosition(pdfDoc.PageSize.Width - 57.0F, pdfDoc.PageSize.Height - 86.0F)
                pdfDoc.Add(stamp)
            End If

            'columnWidth.Clear()
            'Dim ccTbl As New PdfPTable(1)
            'ccTbl.SpacingBefore = 40.0F
            ''ccTbl.SpacingAfter = 0.1F
            'ccTbl.TotalWidth = pageWidth - 28
            'ccTbl.LockedWidth = True
            'columnWidth.Add(110.0F)
            'Dim columnwidths2 As Integer()
            'columnwidths2 = columnWidth.ToArray()
            'ccTbl.SetWidths(columnwidths2)
            'ccTbl.HorizontalAlignment = Element.ALIGN_CENTER
            ''Dim pg As New Paragraph
            ''pg.Add(New Chunk(" " & Environment.NewLine, spacer))
            ''pg.Add(New Chunk("Campus Chief", boldFont))
            ''cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            'cell = CellTextAlignment.ToCenter("Campus Chief", boldFont)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_CENTER
            'ccTbl.AddCell(cell)
            'pdfDoc.Add(ccTbl)

            'Watermark
            'Dim imageUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
            'Dim watermark As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl))
            'watermark.ScalePercent(12.5F)
            'watermark.SetAbsolutePosition(96.0F, 60.0F)
            'contentByte = pdfStamper.GetUnderContent(1)
            'contentByte.AddImage(watermark)





            'Details of student
            Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
            Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
            Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyCode").ToString.Trim()
            Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
            Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()
            Dim stdRollNo As String = ds.Tables(0).Rows(0).Item("RollNo").ToString.Trim()
            Dim stdMobile As String = ds.Tables(0).Rows(0).Item("MobileNo").ToString.Trim()

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 52.0F
            stdtable.SpacingAfter = 0.1F
            stdtable.TotalWidth = pageWidth - 2
            stdtable.LockedWidth = True
            columnWidth.Add(118.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(studentName, normalFont))


            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(stdAddress, normalFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p3.Add(New Chunk("Campus Regd No." & " : ", boldFont))
            'p3.Add(New Chunk(RegdNo, normalFont))


            p4.Add(New Chunk("Level" & " : ", boldFont))
            p4.Add(New Chunk(stdBatch, normalFont))
            p4.Add(New Chunk(" " & stdSemester, normalFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk("Faculty" & " : ", boldFont))
            p6.Add(New Chunk(stdFaculty, normalFont))
            p6.Add(New Chunk(" Roll No." & ": ", boldFont))
            p6.Add(New Chunk(stdRollNo, normalFont))

            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p7.Add(New Chunk("Year/Semester" & " : ", boldFont))
            'p7.Add(New Chunk(stdSemester, normalFont))

            'cell = CellTextAlignment.ToCenter(p7)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'stdtable.AddCell(cell)

            p3.Add(New Chunk("Mobile No." & " : ", boldFont))
            p3.Add(New Chunk(stdMobile, normalFont))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p5.Add(New Chunk("Valid Till" & " : ", boldFont))
            p5.Add(New Chunk(validDate, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)



            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = barcode
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(80.0F, 18.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(40.0F, pdfDoc.PageSize.Height - 174.0F)
            pdfDoc.Add(img)

            'QRCode
            Dim registrationNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
            'Dim ed As New Dao.EncryptDecrypt()
            Dim url As String = registrationNo + 1010 'ed.Encrypt(registrationNo, "k") 'ed.Encrypt("222137")


            Dim hints As New Dictionary(Of iTextSharp.text.pdf.qrcode.EncodeHintType, Object)
            hints.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.L)
            Dim qrcontent As String = "http://kmc.emis.com.np/qr.aspx?u=" & url & ""
            Dim QR As iTextSharp.text.pdf.BarcodeQRCode = New iTextSharp.text.pdf.BarcodeQRCode(qrcontent, 50, 50, hints)
            Dim QRimg As iTextSharp.text.Image = QR.GetImage()
            Dim mask As iTextSharp.text.Image = QR.GetImage()
            mask.MakeMask()
            QRimg.ImageMask = mask
            QRimg.ScaleToFit(50.0F, 50.0F)
            QRimg.SetAbsolutePosition(75.0F, pdfDoc.PageSize.Height - 185.0F)
            pdfDoc.Add(QRimg)


            'CARD TITLE
            Dim cardTitle As PdfPTable = New PdfPTable(1)
            cardTitle.SpacingBefore = 24.0F
            'cardTitle.SpacingAfter = 0.2F
            cardTitle.TotalWidth = pageWidth
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            'parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD", PageHeader)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.TOP_BORDER
            cell.BorderColorTop = BaseColor.BLACK
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = BaseColor.PINK
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StudentIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    Public Sub IDCard_Portrait_Butwal(ByVal studentId As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim printstatus As String
        Dim ds4 As New DataSet
        Dim validfrom, validto, barcode, validDate As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
            validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
            Dim validity As String() = validto.Split("-")
            validDate = eng_to_nep(validity(0), validity(1), validity(2))
            barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
        End If

        Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            'Dim pdfStamper As PdfStamper

            Dim mycard As New iTextSharp.text.Rectangle(120.0F, 192.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            mycard.BackgroundColor = BaseColor.PINK

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent




            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 0)
            Dim namefont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.5, 1)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 30.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10.0, 0)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/logo.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                logo.ScaleToFit(22.0F, 22.0F)
                'logo.SetAbsolutePosition(pdfDoc.PageSize.Width - 72.0F, pdfDoc.PageSize.Height - 24.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 28.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            cardHeader.SpacingBefore = 1.0F
            cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 2
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Tribhuvan University" & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Estd. 2030 B.S." & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & ".  Tel.:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer2)))
            'parag.Add(New Paragraph(New Chunk("Ph:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)



            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(20.0F)
                photo.ScaleToFit(38.0F, 48.0F)

                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 80.0F, pdfDoc.PageSize.Height - 87.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 82.0F, pdfDoc.PageSize.Height - 97.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try


            'Stamp Logo
            Dim stampPath As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/Campus_Stamp.png")) Then
                stampPath = "~/assets/InstitutionLogo/Campus_Stamp.png"
            End If
            Dim stampUrl = HttpContext.Current.Server.MapPath(stampPath)
            If Not String.IsNullOrWhiteSpace(stampUrl) Then
                Dim stamp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(stampUrl))
                stamp.RotationDegrees = -30
                stamp.ScaleToFit(45.0F, 52.0F)
                stamp.SetAbsolutePosition(pdfDoc.PageSize.Width - 57.0F, pdfDoc.PageSize.Height - 86.0F)
                pdfDoc.Add(stamp)
            End If



            'Watermark
            'Dim imageUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
            'Dim watermark As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl))
            'watermark.ScalePercent(12.5F)
            'watermark.SetAbsolutePosition(96.0F, 60.0F)
            'contentByte = pdfStamper.GetUnderContent(1)
            'contentByte.AddImage(watermark)





            'Details of student
            Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
            Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
            Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyCode").ToString.Trim()
            Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
            Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()
            Dim stdRollNo As String = ds.Tables(0).Rows(0).Item("RollNo").ToString.Trim()
            Dim stdMobile As String = ds.Tables(0).Rows(0).Item("MobileNo").ToString.Trim()

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 52.0F
            stdtable.SpacingAfter = 0.1F
            stdtable.TotalWidth = pageWidth - 2
            stdtable.LockedWidth = True
            columnWidth.Add(118.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
            'p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(studentName.ToUpper(), namefont))


            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(stdAddress, normalFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p3.Add(New Chunk("Campus Regd No." & " : ", boldFont))
            'p3.Add(New Chunk(RegdNo, normalFont))


            p4.Add(New Chunk("Level" & " : ", boldFont))
            p4.Add(New Chunk(stdBatch, normalFont))
            p4.Add(New Chunk(" " & stdSemester, normalFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk("Faculty" & " : ", boldFont))
            p6.Add(New Chunk(stdFaculty, normalFont))
            p6.Add(New Chunk(" Roll No." & ": ", boldFont))
            p6.Add(New Chunk(stdRollNo, normalFont))

            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p7.Add(New Chunk("Year/Semester" & " : ", boldFont))
            'p7.Add(New Chunk(stdSemester, normalFont))

            'cell = CellTextAlignment.ToCenter(p7)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'stdtable.AddCell(cell)

            p3.Add(New Chunk("Mobile No." & " : ", boldFont))
            p3.Add(New Chunk(stdMobile, normalFont))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p5.Add(New Chunk("Valid Till" & " : ", boldFont))
            p5.Add(New Chunk(validDate, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)



            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = barcode
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(80.0F, 18.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(40.0F, pdfDoc.PageSize.Height - 174.0F)
            pdfDoc.Add(img)

            'QRCode
            Dim registrationNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
            'Dim ed As New Dao.EncryptDecrypt()
            Dim url As String = registrationNo + 1010 'ed.Encrypt(registrationNo, "k") 'ed.Encrypt("222137")


            Dim hints As New Dictionary(Of iTextSharp.text.pdf.qrcode.EncodeHintType, Object)
            hints.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.L)
            Dim qrcontent As String = "http://bmctu.emis.com.np/qr.aspx?u=" & url & ""
            Dim QR As iTextSharp.text.pdf.BarcodeQRCode = New iTextSharp.text.pdf.BarcodeQRCode(qrcontent, 50, 50, hints)
            Dim QRimg As iTextSharp.text.Image = QR.GetImage()
            Dim mask As iTextSharp.text.Image = QR.GetImage()
            mask.MakeMask()
            QRimg.ImageMask = mask
            QRimg.ScaleToFit(50.0F, 50.0F)
            QRimg.SetAbsolutePosition(75.0F, pdfDoc.PageSize.Height - 185.0F)
            pdfDoc.Add(QRimg)


            'CARD TITLE
            Dim cardTitle As PdfPTable = New PdfPTable(1)
            cardTitle.SpacingBefore = 24.0F
            'cardTitle.SpacingAfter = 0.2F
            cardTitle.TotalWidth = pageWidth
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            'parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD", PageHeader)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.TOP_BORDER
            cell.BorderColorTop = BaseColor.BLACK
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = BaseColor.PINK
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StudentIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    Public Sub IDCard_Tahachal(ByVal studentId As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim printstatus As String
        Dim ds4 As New DataSet
        Dim validfrom, validto, barcode As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
            validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
            barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
        End If

        Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            'Dim pdfStamper As PdfStamper

            Dim mycard As New iTextSharp.text.Rectangle(192.0F, 120.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            'mycard.BackgroundColor = BaseColor.ORANGE

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent

            'Background Image
            Dim pgWidth = pdfDoc.PageSize.Width
            Dim pgHeight = pdfDoc.PageSize.Height
            Dim imgbackgroundUrl As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/IDCardBackground.png")) Then
                imgbackgroundUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/IDCardBackground.png")
            End If
            Dim backgroundImg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgbackgroundUrl))
            'backgroundImg.Alignment = iTextSharp.text.Image.UNDERLYING
            'backgroundImg.ScaleToFit(150.0F, 200.0F)
            backgroundImg.ScaleAbsoluteWidth(192.0F)
            backgroundImg.ScaleAbsoluteHeight(120.0F)
            backgroundImg.SetAbsolutePosition(0.0F, 0.0F)
            pdfDoc.Add(backgroundImg)

            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10.0, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.8, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7.8, 1, BaseColor.WHITE)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.0, 0)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            'Details of student
            Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
            Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
            Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
            Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()
            Dim stdRoll As String = ds.Tables(0).Rows(0).Item("RollNo").ToString.Trim()
            Dim stdShift As String = ds.Tables(0).Rows(0).Item("ShiftName").ToString.Trim()
            Dim DOB As String = ds.Tables(0).Rows(0).Item("DOB").ToString.Trim()
            Dim birthmiti As String = ds.Tables(0).Rows(0).Item("BirthMiti").ToString.Trim()

            Dim pageWidth = pdfDoc.PageSize.Width
            columnWidth.Clear()
            columnWidth.Add(180.0F)
            columnWidth.Add(50.0F)

            Dim cardHeader As PdfPTable = New PdfPTable(2)
            'cardHeader.SpacingBefore = 1.0F
            'cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 18
            Dim columnwidths1 As Integer()
            columnwidths1 = columnWidth.ToArray()
            cardHeader.SetWidths(columnwidths1)
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag, parag1, parag2, para4 As New Paragraph
            parag2.SetLeading(0.0F, 2.0F)

            'parag1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            'parag1.Add(New Paragraph(New Chunk("TRIBHUVAN UNIVERSITY" & Environment.NewLine, SchoolSlogan)))
            'parag1.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))

            cell = CellTextAlignment.ToRight("TRIBHUVAN UNIVERSITY" & Environment.NewLine, SchoolSlogan)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cardHeader.AddCell(cell)

            'Dim contact As String() = institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString().Split(",")
            'parag2.Add(New Paragraph(New Chunk("Tel: " & contact(0).Trim & Environment.NewLine, SchoolContact)))
            'parag2.Add(New Phrase(New Chunk("           " & contact(1).Trim & Environment.NewLine, SchoolContact)))

            parag2.Add(New Paragraph(New Chunk("Tel: " & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString().Trim & Environment.NewLine, SchoolContact)))
            cell = CellTextAlignment.ToCenter(parag2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.Colspan = 1
            cardHeader.AddCell(cell)



            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString().ToUpper & Environment.NewLine, SchoolName)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))

            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.Colspan = 2
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardHeader.AddCell(cell)

            'para4.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            'para4.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))

            cell = CellTextAlignment.ToRight(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cardHeader.AddCell(cell)

            parag1.Add(New Chunk("Card No." & " : ", SchoolContact))
            parag1.Add(New Chunk(RegdNo, SchoolContact))

            cell = CellTextAlignment.ToCenter(parag1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_BOTTOM
            cell.Colspan = 1
            cardHeader.AddCell(cell)


            pdfDoc.Add(cardHeader)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/logo.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(28.0F, 28.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 2.0F, pdfDoc.PageSize.Height - 28.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'TU logo
            'Try
            '    Dim imageUrl2 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
            '    Dim logo2 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl2))
            '    'logo2.ScalePercent(35.0F)
            '    logo2.ScaleToFit(30.0F, 30.0F)
            '    logo2.SetAbsolutePosition(pdfDoc.PageSize.Width - 29.0F, pdfDoc.PageSize.Height - 33.0F)
            '    pdfDoc.Add(logo2)
            'Catch ex As Exception

            'End Try





            Dim cardTitle As PdfPTable = New PdfPTable(1)
            'cardTitle.SpacingBefore = 2.0F
            cardTitle.SpacingAfter = 1.5F
            cardTitle.TotalWidth = pageWidth
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD" & Environment.NewLine, PageHeader)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.BackgroundColor = BaseColor.RED
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

          

            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            columnWidth.Clear()
            columnWidth.Add(180.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph

            'p3.Add(New Chunk("Card No." & " : ", boldFont))
            'p3.Add(New Chunk(RegdNo, normalFont))

            'cell = CellTextAlignment.ToCenter(p3)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'stdtable.AddCell(cell)

            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(studentName, normalFont))


            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

           
            p4.Add(New Chunk("Level" & " : ", boldFont))
            p4.Add(New Chunk(stdBatch, normalFont))
            p4.Add(New Chunk(" " & stdSemester, normalFont))


            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk("Roll No." & " : ", boldFont))
            p6.Add(New Chunk(stdRoll, normalFont))
            p6.Add(New Chunk(" Shift" & " : ", boldFont))
            p6.Add(New Chunk(stdShift, normalFont))

            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(stdAddress, normalFont))
          

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

          

            p5.Add(New Chunk("D.O.B" & " : ", boldFont))
            p5.Add(New Chunk(birthmiti, normalFont))
            p5.Add(New Chunk(" Valid Till" & " : ", boldFont))
            p5.Add(New Chunk(validto, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)

            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(20.0F)
                photo.ScaleToFit(40.0F, 50.0F)

                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 46.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 42.0F, pdfDoc.PageSize.Height - 110.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try

            'Stamp Logo
            Dim stampPath As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/Campus_Stamp.png")) Then
                stampPath = "~/assets/InstitutionLogo/Campus_Stamp.png"
            End If
            Dim stampUrl = HttpContext.Current.Server.MapPath(stampPath)
            If Not String.IsNullOrWhiteSpace(stampUrl) Then
                Dim stamp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(stampUrl))
                stamp.RotationDegrees = 10
                stamp.ScaleToFit(30.0F, 32.0F)
                stamp.SetAbsolutePosition(pdfDoc.PageSize.Width - 64.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(stamp)
            End If


            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = barcode
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(80.0F, 18.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(60.0F, pdfDoc.PageSize.Height - 118.0F)
            pdfDoc.Add(img)




            columnWidth.Clear()
            Dim lastTbl As New PdfPTable(1)
            lastTbl.TotalWidth = pageWidth - 5
            lastTbl.LockedWidth = True
            columnWidth.Add(185.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()
            lastTbl.SetWidths(columnwidths2)
            lastTbl.HorizontalAlignment = Element.ALIGN_RIGHT

            'Dim pg As New Paragraph
            'pg.Add(New Chunk(" " & Environment.NewLine, spacer))
            'pg.Add(New Chunk("Campus Chief", boldFont))

            cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            lastTbl.AddCell(cell)
            pdfDoc.Add(lastTbl)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StudentIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    Public Sub IDCard_Thapathali(ByVal studentId As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim printstatus As String
        Dim ds4 As New DataSet
        Dim validfrom, validto, barcode, validDate As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
            validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
            Dim validity As String() = validto.Split("-")
            validDate = eng_to_nep(validity(0), validity(1), validity(2))
            barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
        End If

        Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(122.0F, 184.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            'mycard.BackgroundColor = BaseColor.PINK

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent




            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.4, 0)
            Dim namefont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.3, 1)
            Dim normalFontRed As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.3, 0, BaseColor.RED)
            Dim boldFontBlue As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.6, 1, BaseColor.BLUE)
            Dim cardFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.8, 1, BaseColor.RED)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1, BaseColor.RED)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1, BaseColor.BLUE)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.0, 1, BaseColor.BLUE)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.8, 2)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 27.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2.0, 0)


            'Background Image
            Dim pgWidth = pdfDoc.PageSize.Width
            Dim pgHeight = pdfDoc.PageSize.Height
            Dim imgbackgroundUrl As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/IDCardBackground.jpg")) Then
                imgbackgroundUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/IDCardBackground.jpg")
            End If
            Dim backgroundImg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgbackgroundUrl))
            'backgroundImg.Alignment = iTextSharp.text.Image.UNDERLYING
            backgroundImg.ScaleToFit(126.0F, 188.0F)
            backgroundImg.SetAbsolutePosition(0.0F, 2.0F)
            pdfDoc.Add(backgroundImg)

            'Institution Image Logo insertion
            'Try
            '    Dim imageUrl1 As String
            '    If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
            '        imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
            '    ElseIf File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.png")) Then
            '        imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.png")
            '    Else
            '        imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/logo.png")
            '    End If
            '    Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
            '    logo.ScaleToFit(22.0F, 22.0F)
            '    'logo.SetAbsolutePosition(pdfDoc.PageSize.Width - 72.0F, pdfDoc.PageSize.Height - 24.0F)
            '    logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 2.4F, pdfDoc.PageSize.Height - 28.0F)
            '    pdfDoc.Add(logo)

            'Catch ex As Exception

            'End Try

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim address As String = institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString().Trim
            Dim website As String = institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString().Trim
            Dim contactNo As String = institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString().Trim
            Dim email As String = institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString().Trim


            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            cardHeader.SpacingBefore = 1.0F
            cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 2
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            'parag.SetLeading(0.0F, 2.0F)
            'parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk("TRIBHUVAN UNIVERSITY" & Environment.NewLine, SchoolAddress)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk("INSTITUTE OF ENGINEERING" & Environment.NewLine, SchoolAddress)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk(" THAPATHALI CAMPUS " & Environment.NewLine, SchoolName)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk(address & ", Tel.:" & contactNo & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer2)))
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)



            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                photo.ScaleToFit(38.0F, 48.0F)
                photo.Border = iTextSharp.text.Rectangle.BOX
                photo.BorderColor = BaseColor.BLUE
                photo.BorderWidth = 1.0F
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 80.0F, pdfDoc.PageSize.Height - 82.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try



            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 82.0F, pdfDoc.PageSize.Height - 88.0F)
                    pdfDoc.Add(sign)
                    'cell.AddElement(sign)
                End If

            Catch ex As Exception

            End Try


            'Stamp Logo
            'Dim stampPath As String
            'If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/Campus_Stamp.png")) Then
            '    stampPath = "~/assets/InstitutionLogo/Campus_Stamp.png"
            'End If
            'Dim stampUrl = HttpContext.Current.Server.MapPath(stampPath)
            'If Not String.IsNullOrWhiteSpace(stampUrl) Then
            '    Dim stamp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(stampUrl))
            '    stamp.RotationDegrees = 20
            '    stamp.ScaleToFit(45.0F, 50.0F)
            '    stamp.SetAbsolutePosition(pdfDoc.PageSize.Width - 58.0F, pdfDoc.PageSize.Height - 98.0F)
            '    pdfDoc.Add(stamp)
            'End If


            'Details of student
            Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
            Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim DoB As String = ds.Tables(0).Rows(0).Item("DOB").ToString.Trim()
            Dim birthmiti As String = ds.Tables(0).Rows(0).Item("BirthMiti").ToString.Trim()
            'If Not String.IsNullOrEmpty(DoB) Then
            '    birthmiti = DC.ToBS(Convert.ToDateTime(DoB), "n")
            'End If

            Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
            Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyCode").ToString.Trim()
            Dim stdCourse As String = ds.Tables(0).Rows(0).Item("CourseTitle").ToString.Trim()
            Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
            Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()
            Dim stdRollNo As String = ds.Tables(0).Rows(0).Item("RollNo").ToString.Trim()
            Dim stdMobile As String = ds.Tables(0).Rows(0).Item("MobileNo").ToString.Trim()
            Dim citizenorPPNo As String = ds.Tables(0).Rows(0).Item("PPorCitizenNo").ToString.Trim()

            Dim tblCenter As New PdfPTable(4)
            'tblCenter.SpacingBefore = 25.5F
            tblCenter.SpacingBefore = 1.0F
            tblCenter.SpacingAfter = 0.1F
            tblCenter.TotalWidth = pageWidth - 1
            tblCenter.LockedWidth = True
            columnWidth.Clear()
            columnWidth.Add(18.0F)
            columnWidth.Add(10.0F)
            columnWidth.Add(50.0F)
            columnWidth.Add(10.0F)
            Dim columnwidths3 As Integer()
            columnwidths3 = columnWidth.ToArray()
            tblCenter.SetWidths(columnwidths3)
            tblCenter.HorizontalAlignment = Element.ALIGN_LEFT

            Dim pg1, pg2, pg3 As New Paragraph
            'pg1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            pg1.Add(New Chunk("Email: " & email, boldFontBlue))
            pg1.Add(New Chunk(" " & Environment.NewLine, boldFontBlue))
            pg1.Add(New Chunk(" " & Environment.NewLine, boldFontBlue))
            pg1.Add(New Chunk("Expiry: " & validto, boldFontBlue))
            cell = CellTextAlignment.ToCenter(pg1)
            cell.Rotation = 90
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_TOP
            tblCenter.AddCell(cell)



            pg2.Add(New Chunk("STUDENT", cardFont))
            cell = CellTextAlignment.ToCenter(pg2)
            cell.Rotation = 90
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.VerticalAlignment = Element.ALIGN_TOP
            tblCenter.AddCell(cell)


            pg3.Add(New Chunk("Campus Chief" & Environment.NewLine, certified).SetUnderline(0.6, 5))
            cell = CellTextAlignment.ToCenter(pg3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_BOTTOM
            tblCenter.AddCell(cell)



            cell = CellTextAlignment.ToRight("ID CARD", cardFont)
            cell.Rotation = 90
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_CENTER
            tblCenter.AddCell(cell)
            pdfDoc.Add(tblCenter)

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 0.4F
            stdtable.SpacingAfter = 0.0F
            stdtable.TotalWidth = pageWidth - 4
            stdtable.LockedWidth = True
            columnWidth.Add(100.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph

            'p1.Add(New Chunk("Campus Chief" & Environment.NewLine, certified).SetUnderline(0.6, 5))
            'cell = CellTextAlignment.ToCenter(p1)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_CENTER
            'stdtable.AddCell(cell)

            p2.Add(New Chunk(studentName.ToUpper() & Environment.NewLine, namefont))
            p2.Add(New Chunk(stdRollNo, boldFont))
            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            stdtable.AddCell(cell)


            p3.Add(New Chunk(stdCourse, normalFontRed))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            stdtable.AddCell(cell)


            p5.Add(New Chunk(" Date of Birth" & " : ", normalFont))
            p5.Add(New Chunk(birthmiti, normalFont))


            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p4.Add(New Chunk(" Address" & " : ", normalFont))
            p4.Add(New Chunk(stdAddress, normalFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk(" Contact No." & " : ", normalFont))
            p6.Add(New Chunk(stdMobile, normalFont))
            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p7.Add(New Chunk(" PP/LID/Ctz No." & " : ", normalFont))
            p7.Add(New Chunk(citizenorPPNo, normalFont))


            cell = CellTextAlignment.ToCenter(p7)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)



            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            'code128.Code = barcode
            code128.Code = stdRollNo
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            code128.Font = Nothing
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(60.0F, 12.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(28.0F, pdfDoc.PageSize.Height - 168.0F)
            pdfDoc.Add(img)

            'QRCode
            Dim registrationNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
            'Dim ed As New Dao.EncryptDecrypt()
            Dim url As String = registrationNo + 1010 'ed.Encrypt(registrationNo, "k") 'ed.Encrypt("222137")


            Dim hints As New Dictionary(Of iTextSharp.text.pdf.qrcode.EncodeHintType, Object)
            hints.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.L)
            Dim qrcontent As String = "http://tcioe.emis.com.np/qr.aspx?u=" & url & ""
            Dim QR As iTextSharp.text.pdf.BarcodeQRCode = New iTextSharp.text.pdf.BarcodeQRCode(qrcontent, 50, 50, hints)
            Dim QRimg As iTextSharp.text.Image = QR.GetImage()
            Dim mask As iTextSharp.text.Image = QR.GetImage()
            mask.MakeMask()
            QRimg.ImageMask = mask
            QRimg.ScaleToFit(50.0F, 50.0F)
            QRimg.SetAbsolutePosition(75.0F, pdfDoc.PageSize.Height - 80.0F)
            pdfDoc.Add(QRimg)


            'CARD TITLE
            Dim cardTitle As PdfPTable = New PdfPTable(1)
            cardTitle.SpacingBefore = 12.0F
            'cardTitle.SpacingAfter = 0.2F
            cardTitle.TotalWidth = pageWidth
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph

            'parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD", PageHeader)))
            parag3.Add(New Paragraph(New Chunk(website, boldFontBlue)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.BorderColorTop = BaseColor.BLACK
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            'cell.BackgroundColor = BaseColor.PINK
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StudentIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    Public Sub IDCard_Aadhikavi(ByVal studentId As String, Optional ByVal printId As String = "", Optional ByVal userId As String = "")
        Dim printstatus As String
        Dim ds4 As New DataSet
        Dim validfrom, validto, barcode As String

        If Not String.IsNullOrWhiteSpace(printId) Then
            printstatus = UpdatePrintingStatus(printId, userId)

            Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
            ds4 = Dao.ExecuteDataset(sql4)
            validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
            validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
            barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
        End If

        Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(192.0F, 120.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent

            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8.8, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9.6, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.5, 3, BaseColor.BLUE)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2.0, 0)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            cardHeader.TotalWidth = pageWidth - 28
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Slogan").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Ph: " & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            'parag.Add(New Paragraph(New Chunk("URL: " & institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString() & ", Email: " & institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString() & Environment.NewLine, certified)))      
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(30.0F, 30.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'TU logo
            'Try
            '    Dim imageUrl2 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
            '    Dim logo2 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl2))
            '    'logo2.ScalePercent(35.0F)
            '    logo2.ScaleToFit(30.0F, 30.0F)
            '    logo2.SetAbsolutePosition(pdfDoc.PageSize.Width - 29.0F, pdfDoc.PageSize.Height - 33.0F)
            '    pdfDoc.Add(logo2)
            'Catch ex As Exception

            'End Try

            'Details of student
            Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
            Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
            Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
            Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
            Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyTitle").ToString.Trim()
            Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
            Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()
            Dim birthMiti As String = ds.Tables(0).Rows(0).Item("DOB").ToString.Trim()


            'QRCode
            Dim registrationNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
            Dim url As String = registrationNo + 1010 'ed.Encrypt(registrationNo, "k") 'ed.Encrypt("222137")
            Dim hints As New Dictionary(Of iTextSharp.text.pdf.qrcode.EncodeHintType, Object)
            hints.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.L)
            Dim qrcontent As String = "http://abc.emis.com.np/qr.aspx?u=" & url & ""
            Dim QR As iTextSharp.text.pdf.BarcodeQRCode = New iTextSharp.text.pdf.BarcodeQRCode(qrcontent, 50, 50, hints)
            Dim QRimg As iTextSharp.text.Image = QR.GetImage()
            Dim mask As iTextSharp.text.Image = QR.GetImage()
            mask.MakeMask()
            QRimg.ImageMask = mask
            QRimg.ScaleToFit(36.0F, 40.0F)
            QRimg.SetAbsolutePosition(pdfDoc.PageSize.Width - 32.0F, pdfDoc.PageSize.Height - 33.0F)
            pdfDoc.Add(QRimg)


            Dim cardTitle As PdfPTable = New PdfPTable(1)
            'cardTitle.SpacingBefore = 2.0F
            cardTitle.SpacingAfter = 1.0F
            cardTitle.TotalWidth = pageWidth - 28
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD" & Environment.NewLine, PageHeader).SetUnderline(1, -3)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

         


            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            stdtable.SpacingBefore = 3.0F
            columnWidth.Add(180.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5 As New Paragraph
            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(studentName, boldFont))


            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(stdAddress, boldFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p3.Add(New Chunk("DOB" & " : ", boldFont))
            p3.Add(New Chunk(birthMiti & " B.S.", boldFont))
            p3.Add(New Chunk(" Validity" & " : ", boldFont))
            p3.Add(New Chunk(validto, boldFont))
         

            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p4.Add(New Chunk("Level" & " : ", boldFont))
            p4.Add(New Chunk(stdBatch, boldFont))
            p4.Add(New Chunk(" / " & stdSemester, boldFont))
            'p4.Add(New Chunk("  Faculty" & " : ", boldFont))
            'p4.Add(New Chunk(stdFaculty, normalFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'cell = CellTextAlignment.ToLeft("Faculty" & " : " & stdFaculty, normalFont)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'stdtable.AddCell(cell)

            'cell = CellTextAlignment.ToLeft("Batch" & " : " & stdBatch, normalFont)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'stdtable.AddCell(cell)

            'p5.Add(New Chunk("Valid From" & " : ", boldFont))
            'p5.Add(New Chunk(validfrom, normalFont))
            'p5.Add(New Chunk("Semester" & " : ", boldFont))
            'p5.Add(New Chunk(stdSemester, boldFont))
            'p5.Add(New Chunk(" Valid Upto" & " : ", boldFont))
            'p5.Add(New Chunk(validto, boldFont))

            p5.Add(New Chunk(" ", spacer1))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)

            'Photo insertion
            Try
                Dim imgpath As String
                GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                    imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScalePercent(20.0F)
                photo.ScaleToFit(40.0F, 50.0F)

                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 46.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 42.0F, pdfDoc.PageSize.Height - 110.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try

            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = barcode
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 10
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(120.0F, 16.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(60.0F, pdfDoc.PageSize.Height - 112.0F)
            pdfDoc.Add(img)




            columnWidth.Clear()
            Dim lastTbl As New PdfPTable(1)
            lastTbl.TotalWidth = pageWidth - 5
            lastTbl.LockedWidth = True
            columnWidth.Add(185.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()
            lastTbl.SetWidths(columnwidths2)
            lastTbl.HorizontalAlignment = Element.ALIGN_RIGHT

            'Dim pg As New Paragraph
            'pg.Add(New Chunk(" " & Environment.NewLine, spacer))
            'pg.Add(New Chunk("Campus Chief", boldFont))

            cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            lastTbl.AddCell(cell)
            pdfDoc.Add(lastTbl)

            columnWidth.Clear()
            Dim webEmailtbl As New PdfPTable(1)
            webEmailtbl.TotalWidth = pageWidth - 5
            webEmailtbl.LockedWidth = True
            webEmailtbl.SpacingAfter = 5.0F
            columnWidth.Add(185.0F)
            Dim webmailwidth As Integer()
            webmailwidth = columnWidth.ToArray()
            webEmailtbl.SetWidths(webmailwidth)
            webEmailtbl.HorizontalAlignment = Element.ALIGN_CENTER

            Dim wpg As New Paragraph
            wpg.Add(New Paragraph(New Chunk("URL: " & institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString() & " , Email: " & institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString() & Environment.NewLine, certified)))

            cell = CellTextAlignment.ToCenter(wpg)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            webEmailtbl.AddCell(cell)
            pdfDoc.Add(webEmailtbl)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StudentIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    'print ID card in bulk
    Public Sub StudentIDCard_Portrait_All(ByVal studentList As DataSet, Optional ByVal userId As String = "")

        If studentList.Tables.Count > 0 AndAlso studentList.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(120.0F, 192.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            mycard.BackgroundColor = BaseColor.PINK

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            contentByte = pdfWriter.DirectContent

            For s As Integer = 0 To studentList.Tables(0).Rows.Count - 1
                Dim studentId As String = studentList.Tables(0).Rows(s).Item("StudentID").ToString()
                Dim printId As String = studentList.Tables(0).Rows(s).Item("PrintingDocumentID").ToString()
                Dim printstatus As String
                Dim ds4 As New DataSet
                Dim validfrom, validto, barcode, validDate As String

                If Not String.IsNullOrWhiteSpace(printId) Then
                    printstatus = UpdatePrintingStatus(printId, userId)

                    Dim sql4 As String = "exec students.usp_PrintingDocuments @Flag='c',@PrintingDocumentID='" & printId & "'"
                    ds4 = Dao.ExecuteDataset(sql4)
                    validfrom = ds4.Tables(0).Rows(0).Item("ValidFrom").ToString
                    validto = ds4.Tables(0).Rows(0).Item("ValidTo").ToString
                    Dim validity As String() = validto.Split("-")
                    validDate = eng_to_nep(validity(0), validity(1), validity(2))
                    barcode = ds4.Tables(0).Rows(0).Item("Barcode").ToString
                End If

                Dim sql As String = "exec [Students].[usp_CharacterCertificate] @Flag='c',@StudentID='" & studentId & "'"
                Dim ds As New DataSet
                ds = Dao.ExecuteDataset(sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    pdfDoc.NewPage()
                    Dim cell As PdfPCell
                    Dim lightGrey As New BaseColor(245, 245, 245)
                    Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
                    Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 0)
                    Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
                    Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
                    Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1, BaseColor.BLUE)
                    Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1)
                    Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1)
                    Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
                    Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8.0, 1, BaseColor.RED)
                    Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
                    Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
                    Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
                    Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 30.0, 0)

                    'Institution Image Logo insertion
                    Try
                        Dim imageUrl1 As String
                        If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/IDCardLogo.png")) Then
                            imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/IDCardLogo.png")
                        Else
                            imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/logo.png")
                        End If
                        Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                        'logo.ScalePercent(12.5F)
                        logo.ScaleToFit(110.0F, 48.0F)
                        logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 33.0F)
                        pdfDoc.Add(logo)

                    Catch ex As Exception

                    End Try

                    'Card Header
                    Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
                    Dim institutionInfoDS As New DataSet
                    institutionInfoDS = Dao.ExecuteDataset(sqlquery)

                    Dim pageWidth = pdfDoc.PageSize.Width
                    Dim cardHeader As PdfPTable = New PdfPTable(1)
                    cardHeader.SpacingBefore = 1.0F
                    cardHeader.SpacingAfter = 1.0F
                    cardHeader.TotalWidth = pageWidth - 2
                    cardHeader.LockedWidth = True
                    Dim phrase As New Phrase()
                    Dim parag As New Paragraph
                    parag.SetLeading(0.0F, 2.0F)
                    parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
                    'parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
                    'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
                    parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & ", Tel.:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolAddress)))
                    'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
                    'parag.Add(New Paragraph(New Chunk("Ph:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
                    'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))         
                    cell = CellTextAlignment.ToCenter(parag)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.BorderColorTop = BaseColor.WHITE
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    cardHeader.AddCell(cell)
                    pdfDoc.Add(cardHeader)



                    'Photo insertion
                    Try
                        Dim imgpath As String
                        GlobalRollNo = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                        If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Photos/" & GlobalRollNo & ".jpg")) Then
                            imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".jpg"
                        ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/photos/" & ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString() & ".png")) Then
                            imgpath = "~/Student/Images/Photos/" & GlobalRollNo & ".png"
                        Else
                            imgpath = "~/Student/Images/Photos/avatar.png"
                        End If
                        Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                        Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                        'photo.ScalePercent(20.0F)
                        photo.ScaleToFit(38.0F, 48.0F)

                        photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 80.0F, pdfDoc.PageSize.Height - 87.0F)
                        pdfDoc.Add(photo)
                    Catch ex As Exception

                    End Try

                    'Signature
                    Try
                        Dim signpath As String
                        If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                            signpath = "~/Student/Images/Signature/signature.png"
                        ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                            signpath = "~/Student/Images/Signature/signature.png"
                        End If
                        Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                        If Not String.IsNullOrWhiteSpace(signUrl) Then
                            Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                            sign.ScaleToFit(40.0F, 25.0F)
                            sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 82.0F, pdfDoc.PageSize.Height - 97.0F)
                            pdfDoc.Add(sign)
                        End If

                    Catch ex As Exception

                    End Try

                    Dim stampPath As String
                    If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/Campus_Stamp.png")) Then
                        stampPath = "~/assets/InstitutionLogo/Campus_Stamp.png"
                    End If
                    Dim stampUrl = HttpContext.Current.Server.MapPath(stampPath)
                    If Not String.IsNullOrWhiteSpace(stampUrl) Then
                        Dim stamp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(stampUrl))
                        stamp.RotationDegrees = -30
                        stamp.ScaleToFit(45.0F, 52.0F)
                        stamp.SetAbsolutePosition(pdfDoc.PageSize.Width - 57.0F, pdfDoc.PageSize.Height - 86.0F)
                        pdfDoc.Add(stamp)
                    End If

                    'Details of student
                    Dim studentName As String = ds.Tables(0).Rows(0).Item("FullName").ToString.Trim()
                    Dim stdAddress As String = ds.Tables(0).Rows(0).Item("Address1").ToString.Trim()
                    Dim RegdNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString.Trim()
                    Dim stdLevel As String = ds.Tables(0).Rows(0).Item("LevelTitle").ToString.Trim()
                    Dim stdFaculty As String = ds.Tables(0).Rows(0).Item("FacultyCode").ToString.Trim()
                    Dim stdBatch As String = ds.Tables(0).Rows(0).Item("BatchTitle").ToString.Trim()
                    Dim stdSemester As String = ds.Tables(0).Rows(0).Item("SemesterName").ToString.Trim()
                    Dim stdRollNo As String = ds.Tables(0).Rows(0).Item("RollNo").ToString.Trim()
                    Dim stdMobile As String = ds.Tables(0).Rows(0).Item("MobileNo").ToString.Trim()

                    columnWidth.Clear()
                    Dim stdtable As PdfPTable = New PdfPTable(1)
                    stdtable.SpacingBefore = 52.0F
                    stdtable.SpacingAfter = 0.1F
                    stdtable.TotalWidth = pageWidth - 2
                    stdtable.LockedWidth = True
                    columnWidth.Add(118.0F)
                    'columnWidth.Add(40.0F)
                    Dim columnwidths As Integer()
                    columnwidths = columnWidth.ToArray()
                    stdtable.SetWidths(columnwidths)
                    stdtable.HorizontalAlignment = Element.ALIGN_LEFT


                    Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
                    p1.Add(New Chunk("Name" & " : ", boldFont))
                    p1.Add(New Chunk(studentName, normalFont))


                    cell = CellTextAlignment.ToCenter(p1)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    stdtable.AddCell(cell)

                    p2.Add(New Chunk("Address" & " : ", boldFont))
                    p2.Add(New Chunk(stdAddress, normalFont))

                    cell = CellTextAlignment.ToCenter(p2)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    stdtable.AddCell(cell)

                    'p3.Add(New Chunk("Campus Regd No." & " : ", boldFont))
                    'p3.Add(New Chunk(RegdNo, normalFont))


                    p4.Add(New Chunk("Level" & " : ", boldFont))
                    p4.Add(New Chunk(stdBatch, normalFont))
                    p4.Add(New Chunk(" " & stdSemester, normalFont))

                    cell = CellTextAlignment.ToCenter(p4)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    stdtable.AddCell(cell)

                    p6.Add(New Chunk("Faculty" & " : ", boldFont))
                    p6.Add(New Chunk(stdFaculty, normalFont))
                    p6.Add(New Chunk(" Roll No." & ": ", boldFont))
                    p6.Add(New Chunk(stdRollNo, normalFont))

                    cell = CellTextAlignment.ToCenter(p6)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    stdtable.AddCell(cell)

                    'p7.Add(New Chunk("Year/Semester" & " : ", boldFont))
                    'p7.Add(New Chunk(stdSemester, normalFont))

                    'cell = CellTextAlignment.ToCenter(p7)
                    'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    'cell.HorizontalAlignment = Element.ALIGN_LEFT
                    'stdtable.AddCell(cell)

                    p3.Add(New Chunk("Mobile No." & " : ", boldFont))
                    p3.Add(New Chunk(stdMobile, normalFont))
                    cell = CellTextAlignment.ToCenter(p3)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    stdtable.AddCell(cell)

                    p5.Add(New Chunk("Valid Till" & " : ", boldFont))
                    p5.Add(New Chunk(validDate, normalFont))

                    cell = CellTextAlignment.ToCenter(p5)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    stdtable.AddCell(cell)

                    pdfDoc.Add(stdtable)



                    'BarCode
                    Dim code128 As Barcode128 = New Barcode128()
                    code128.Code = barcode
                    code128.CodeType = Barcode128.CODE128
                    code128.Baseline = 8
                    code128.Size = 8
                    Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
                    img.ScaleToFit(80.0F, 18.0F)
                    img.ScaleToFitLineWhenOverflow = True
                    img.ScaleToFitHeight = True
                    'img.ScalePercent(100.0F)
                    img.SetAbsolutePosition(40.0F, pdfDoc.PageSize.Height - 174.0F)
                    pdfDoc.Add(img)


                    'QRCode
                    Dim registrationNo As String = ds.Tables(0).Rows(0).Item("RegistrationNumber").ToString
                    ' Dim ed As New Dao.EncryptDecrypt()
                    ' Dim url As String = ed.Encrypt(registrationNo, "k") 'ed.Encrypt("222137")
                    Dim url As String = registrationNo + 1010
                    Dim hints As New Dictionary(Of iTextSharp.text.pdf.qrcode.EncodeHintType, Object)

                    hints.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.H)
                    Dim qrcontent As String = "http://kmc.emis.com.np/qr.aspx?u=" & url
                    Dim QR As iTextSharp.text.pdf.BarcodeQRCode = New iTextSharp.text.pdf.BarcodeQRCode(qrcontent, 50, 50, hints)
                    Dim QRimg As iTextSharp.text.Image = QR.GetImage()
                    Dim mask As iTextSharp.text.Image = QR.GetImage()
                    mask.MakeMask()
                    QRimg.ImageMask = mask
                    QRimg.ScaleToFit(50.0F, 50.0F)
                    QRimg.SetAbsolutePosition(75.0F, pdfDoc.PageSize.Height - 185.0F)
                    pdfDoc.Add(QRimg)


                    'CARD TITLE
                    Dim cardTitle As PdfPTable = New PdfPTable(1)
                    cardTitle.SpacingBefore = 24.0F
                    'cardTitle.SpacingAfter = 0.2F
                    cardTitle.TotalWidth = pageWidth
                    cardTitle.LockedWidth = True
                    Dim parag3 As New Paragraph
                    'parag3.SetLeading(0.0F, 2.0F)

                    parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD", PageHeader)))
                    'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

                    cell = CellTextAlignment.ToCenter(parag3)
                    cell.Border = iTextSharp.text.Rectangle.TOP_BORDER
                    cell.BorderColorTop = BaseColor.BLACK
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    cell.BackgroundColor = BaseColor.PINK
                    cardTitle.AddCell(cell)
                    pdfDoc.Add(cardTitle)
                End If
            Next
            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=AllPending_StudentIDCards.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If

    End Sub
#End Region


    Private Function UpdatePrintingStatus(ByVal printId As String, ByVal printedBy As String)
        Dim sql, msg As String
        Dim ds As New DataSet
        Try
            sql = "exec students.usp_PrintingDocuments @flag='p' ,@PrintingDocumentID='" & printId & "',@PrintedBy='" & printedBy & "'"
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                msg = ds.Tables(0).Rows(0).Item("mes").ToString()
                Return "1"
            End If
            Return "0"
        Catch ex As Exception
            Return "0"
        End Try
    End Function

#Region "HR ID Card Designs"
    Public Sub HR_IDCard_Janabhawana(ByVal employeeId As String)
        Dim sql As String = "exec [HR].[usp_EmployeeCard] @EmployeeID='" & employeeId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(192.0F, 120.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            'mycard.BackgroundColor = BaseColor.ORANGE

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent




            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2.0, 0)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            'cardHeader.SpacingBefore = 1.0F
            'cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 28
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Ph:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))         
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(30.0F, 30.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'TU logo
            Try
                Dim imageUrl2 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                Dim logo2 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl2))
                'logo2.ScalePercent(35.0F)
                logo2.ScaleToFit(30.0F, 30.0F)
                logo2.SetAbsolutePosition(pdfDoc.PageSize.Width - 29.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo2)
            Catch ex As Exception

            End Try


            Dim cardTitle As PdfPTable = New PdfPTable(1)
            'cardTitle.SpacingBefore = 2.0F
            cardTitle.SpacingAfter = 1.5F
            cardTitle.TotalWidth = pageWidth - 28
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk(" IDENTITY CARD " & Environment.NewLine, PageHeader).SetUnderline(1, -3)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)


           


            'Details of employee
            Dim empName As String = ds.Tables(0).Rows(0).Item("EmployeeName").ToString.Trim()
            Dim empAddress As String = ds.Tables(0).Rows(0).Item("PermanentAddress").ToString.Trim()
            Dim empNo As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString.Trim()
            Dim empJoinDate As String = ds.Tables(0).Rows(0).Item("JoiningDate").ToString.Trim()
            Dim joinDate As String = DC.ToBS(Convert.ToDateTime(empJoinDate), "n")
            Dim empJobTitle As String = ds.Tables(0).Rows(0).Item("Designation").ToString.Trim()
            Dim empMobile As String = ds.Tables(0).Rows(0).Item("Phone").ToString.Trim()
            Dim issuedate As String = DC.ToBS(Convert.ToDateTime(Date.Now), "n")

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 2.0F
            stdtable.SpacingAfter = 0.2F
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            columnWidth.Add(180.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(empName.ToUpper(), normalFont))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk("Designation" & " : ", boldFont))
            p6.Add(New Chunk(empJobTitle, normalFont))
            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(empAddress, normalFont))
            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p3.Add(New Chunk("Contact No." & " : ", boldFont))
            p3.Add(New Chunk(empMobile, normalFont))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p4.Add(New Chunk("Joining Date" & " : ", boldFont))
            'p4.Add(New Chunk(joinDate, normalFont))
            'cell = CellTextAlignment.ToCenter(p4)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'stdtable.AddCell(cell)

           
            p5.Add(New Chunk("Date of Issue" & " : ", boldFont))
            p5.Add(New Chunk(issuedate, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)


            'Photo insertion
            Try
                Dim imgpath As String
                Dim PhotoEmp As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString()
                If File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpeg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpeg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".png")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScaleToFit(38.0F, 48.0F)
                photo.ScaleToFit(40.0F, 50.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 46.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 42.0F, pdfDoc.PageSize.Height - 110.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try

            columnWidth.Clear()
            Dim lastTbl As New PdfPTable(1)
            lastTbl.TotalWidth = pageWidth - 5
            lastTbl.LockedWidth = True
            columnWidth.Add(185.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()
            lastTbl.SetWidths(columnwidths2)
            lastTbl.HorizontalAlignment = Element.ALIGN_RIGHT

            cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            lastTbl.AddCell(cell)
            pdfDoc.Add(lastTbl)




            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=EmployeeIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If

    End Sub

    Public Sub HR_IDCard_Tahachal(ByVal employeeId As String)
        Dim sql As String = "exec [HR].[usp_EmployeeCard] @EmployeeID='" & employeeId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(192.0F, 120.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            'mycard.BackgroundColor = BaseColor.ORANGE

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent

            'Background Image
            Dim pgWidth = pdfDoc.PageSize.Width
            Dim pgHeight = pdfDoc.PageSize.Height
            Dim imgbackgroundUrl As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/IDCardBackground.png")) Then
                imgbackgroundUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/IDCardBackground.png")
            End If
            Dim backgroundImg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgbackgroundUrl))
            'backgroundImg.Alignment = iTextSharp.text.Image.UNDERLYING
            'backgroundImg.ScaleToFit(150.0F, 200.0F)
            backgroundImg.ScaleAbsoluteWidth(192.0F)
            backgroundImg.ScaleAbsoluteHeight(120.0F)
            backgroundImg.SetAbsolutePosition(0.0F, 0.0F)
            pdfDoc.Add(backgroundImg)


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 0)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.8, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10.0, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.8, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7.8, 1, BaseColor.WHITE)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.0, 0)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            'Details of employee
            Dim empName As String = ds.Tables(0).Rows(0).Item("EmployeeName").ToString.Trim()
            Dim empAddress As String = ds.Tables(0).Rows(0).Item("PermanentAddress").ToString.Trim()
            Dim empNo As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString.Trim()
            Dim empJoinDate As String = ds.Tables(0).Rows(0).Item("JoiningDate").ToString.Trim()
            Dim joinDate As String = DC.ToBS(Convert.ToDateTime(empJoinDate), "n")
            Dim empJobTitle As String = ds.Tables(0).Rows(0).Item("Designation").ToString.Trim()
            Dim empMobile As String = ds.Tables(0).Rows(0).Item("Phone").ToString.Trim()
            Dim issuedate As String = DC.ToBS(Convert.ToDateTime(Date.Now), "n")

            Dim pageWidth = pdfDoc.PageSize.Width
            columnWidth.Clear()
            columnWidth.Add(180.0F)
            columnWidth.Add(50.0F)

            Dim cardHeader As PdfPTable = New PdfPTable(2)
            cardHeader.TotalWidth = pageWidth - 18
            Dim columnwidths1 As Integer()
            columnwidths1 = columnWidth.ToArray()
            cardHeader.SetWidths(columnwidths1)
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag, parag1, parag2, para4 As New Paragraph
            parag2.SetLeading(0.0F, 2.0F)

            cell = CellTextAlignment.ToRight("TRIBHUVAN UNIVERSITY" & Environment.NewLine, SchoolSlogan)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cardHeader.AddCell(cell)

            'Dim contact As String() = institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString().Split(",")
            'parag2.Add(New Paragraph(New Chunk("Tel: " & contact(0).Trim & Environment.NewLine, SchoolContact)))
            'parag2.Add(New Phrase(New Chunk("           " & contact(1).Trim & Environment.NewLine, SchoolContact)))

            parag2.Add(New Paragraph(New Chunk("Tel: " & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString().Trim & Environment.NewLine, SchoolContact)))
            cell = CellTextAlignment.ToCenter(parag2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.Colspan = 1
            cardHeader.AddCell(cell)

            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString().ToUpper & Environment.NewLine, SchoolName)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.Colspan = 2
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardHeader.AddCell(cell)

            cell = CellTextAlignment.ToRight(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString(), SchoolAddress)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.Colspan = 1
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cardHeader.AddCell(cell)

            parag1.Add(New Chunk("Card No." & " : ", SchoolContact))
            parag1.Add(New Chunk(empNo, SchoolContact))

            cell = CellTextAlignment.ToCenter(parag1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_BOTTOM
            cell.Colspan = 1
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/logo.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(28.0F, 30.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 2.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try


            Dim cardTitle As PdfPTable = New PdfPTable(1)
            cardTitle.SpacingAfter = 1.5F
            cardTitle.TotalWidth = pageWidth
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 2.0F)
            parag3.Add(New Paragraph(New Chunk("STAFF IDENTITY CARD" & Environment.NewLine, PageHeader)))
            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.BackgroundColor = BaseColor.RED
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 2.0F
            stdtable.SpacingAfter = 0.1F
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            columnWidth.Add(180.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(empName.ToUpper(), normalFont))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk("Designation" & " : ", boldFont))
            p6.Add(New Chunk(empJobTitle, normalFont))
            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(empAddress, normalFont))
            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p3.Add(New Chunk("Contact No." & " : ", boldFont))
            'p3.Add(New Chunk(empMobile, normalFont))
            'cell = CellTextAlignment.ToCenter(p3)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'stdtable.AddCell(cell)

            
            p5.Add(New Chunk("Date of Issue" & " : ", boldFont))
            p5.Add(New Chunk(issuedate, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p4.Add(New Chunk("Card Holder's Signature" & " : ", boldFont))
            p4.Add(New Chunk("", normalFont))
            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)


            'Photo insertion
            Try
                Dim imgpath As String
                Dim PhotoEmp As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString()
                If File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpeg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpeg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".png")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScaleToFit(38.0F, 48.0F)
                photo.ScaleToFit(40.0F, 50.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 46.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 42.0F, pdfDoc.PageSize.Height - 110.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try

            'Stamp Logo
            Dim stampPath As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/Campus_Stamp.png")) Then
                stampPath = "~/assets/InstitutionLogo/Campus_Stamp.png"
            End If
            Dim stampUrl = HttpContext.Current.Server.MapPath(stampPath)
            If Not String.IsNullOrWhiteSpace(stampUrl) Then
                Dim stamp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(stampUrl))
                stamp.RotationDegrees = 10
                stamp.ScaleToFit(30.0F, 32.0F)
                stamp.SetAbsolutePosition(pdfDoc.PageSize.Width - 64.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(stamp)
            End If

            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = empNo
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            code128.Font = Nothing
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(60.0F, 12.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(60.0F, pdfDoc.PageSize.Height - 114.0F)
            pdfDoc.Add(img)

            columnWidth.Clear()
            Dim lastTbl As New PdfPTable(1)
            lastTbl.TotalWidth = pageWidth - 5
            lastTbl.LockedWidth = True
            columnWidth.Add(185.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()
            lastTbl.SetWidths(columnwidths2)
            lastTbl.HorizontalAlignment = Element.ALIGN_RIGHT

            cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            lastTbl.AddCell(cell)
            pdfDoc.Add(lastTbl)




            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=StaffIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If

    End Sub

    Public Sub HR_IDCard_Thapathali(ByVal employeeId As String)
        Dim sql As String = "exec [HR].[usp_EmployeeCard] @EmployeeID='" & employeeId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(122.0F, 184.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            'mycard.BackgroundColor = BaseColor.PINK

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent

            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.0, 0)
            Dim namefont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.4, 1)
            Dim normalFontBlue As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.0, 0, BaseColor.BLUE)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.3, 1)
            Dim deptFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.8, 0, BaseColor.RED)

            Dim boldFontRedS As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.4, 1, BaseColor.RED)
            Dim boldFontRedM As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.2, 1, BaseColor.RED)
            Dim boldFontRedL As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.7, 1, BaseColor.RED)

            Dim boldFontBlue As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.6, 1, BaseColor.BLUE)
            Dim cardFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.6, 1, BaseColor.RED)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1, BaseColor.RED)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.6, 1, BaseColor.BLUE)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 3.0, 1, BaseColor.BLUE)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 4.8, 2)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 27.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2.0, 0)


            'Background Image
            Dim pgWidth = pdfDoc.PageSize.Width
            Dim pgHeight = pdfDoc.PageSize.Height
            Dim imgbackgroundUrl As String
            If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/IDCardBackground.jpg")) Then
                imgbackgroundUrl = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/IDCardBackground.jpg")
            End If
            Dim backgroundImg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgbackgroundUrl))
            'backgroundImg.Alignment = iTextSharp.text.Image.UNDERLYING
            backgroundImg.ScaleToFit(126.0F, 188.0F)
            backgroundImg.SetAbsolutePosition(0.0F, 2.0F)
            pdfDoc.Add(backgroundImg)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim address As String = institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString().Trim
            Dim website As String = institutionInfoDS.Tables(0).Rows(0).Item("Website").ToString().Trim
            Dim contactNo As String = institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString().Trim
            Dim email As String = institutionInfoDS.Tables(0).Rows(0).Item("Email").ToString().Trim


            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            cardHeader.SpacingBefore = 1.0F
            cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 2
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
           
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)

            'Photo insertion
            Try
                Dim imgpath As String
                Dim PhotoEmp As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString()
                If File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpeg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpeg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".png")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                photo.ScaleToFit(38.0F, 48.0F)
                photo.Border = iTextSharp.text.Rectangle.BOX
                photo.BorderColor = BaseColor.BLUE
                photo.BorderWidth = 1.0F
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 80.0F, pdfDoc.PageSize.Height - 82.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    'sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 82.0F, pdfDoc.PageSize.Height - 88.0F)
                    sign.SetAbsolutePosition(78.0F, pdfDoc.PageSize.Height - 150.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try

            'Details of employee
            Dim empName As String = ds.Tables(0).Rows(0).Item("EmployeeName").ToString.Trim()
            Dim empAddress As String = ds.Tables(0).Rows(0).Item("PermanentAddress").ToString.Trim()
            Dim empNo As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString.Trim()
            Dim department As String = ds.Tables(0).Rows(0).Item("Department").ToString.Trim()
            Dim empEmail As String = ds.Tables(0).Rows(0).Item("Email").ToString.Trim()
            Dim empJoinDate As String = ds.Tables(0).Rows(0).Item("JoiningDate").ToString.Trim()
            Dim birth() As String = ds.Tables(0).Rows(0).Item("DOB").ToString.Trim().Split("/")
            Dim dob As String = DC.ToBS(birth(1), birth(0), birth(2))

            'Dim joinDate As String = DC.ToBS(Convert.ToDateTime(empJoinDate), "n")
            Dim empJobTitle As String = ds.Tables(0).Rows(0).Item("Designation").ToString.Trim()
            Dim empMobile As String = ds.Tables(0).Rows(0).Item("Phone").ToString.Trim()
            'Dim issuedate As String = DC.ToBS(Convert.ToDateTime(Date.Now), "n")
            'Dim validto As String = "2020-12-31"
            Dim citizenorPPNo As String = ds.Tables(0).Rows(0).Item("PPorCitizenNo").ToString.Trim()

            Dim tblCenter As New PdfPTable(4)
            'tblCenter.SpacingBefore = 25.5F
            tblCenter.SpacingBefore = 1.0F
            tblCenter.SpacingAfter = 0.03F
            tblCenter.TotalWidth = pageWidth - 1
            tblCenter.LockedWidth = True
            columnWidth.Clear()
            columnWidth.Add(18.0F)
            columnWidth.Add(10.0F)
            columnWidth.Add(50.0F)
            columnWidth.Add(10.0F)
            Dim columnwidths3 As Integer()
            columnwidths3 = columnWidth.ToArray()
            tblCenter.SetWidths(columnwidths3)
            tblCenter.HorizontalAlignment = Element.ALIGN_LEFT

            Dim pg1, pg2, pg3 As New Paragraph
            'pg1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            If (empEmail.Length > 22) Then
                pg1.Add(New Chunk(empEmail, boldFontRedS))
            ElseIf empEmail.Length < 20 Then
                pg1.Add(New Chunk(empEmail, boldFontRedL))
            Else
                pg1.Add(New Chunk(empEmail, boldFontRedM))
            End If

           
            cell = CellTextAlignment.ToCenter(pg1)
            cell.Rotation = 90
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_TOP
            tblCenter.AddCell(cell)



            pg2.Add(New Chunk("STAFF:" & empNo, cardFont))
            cell = CellTextAlignment.ToCenter(pg2)
            cell.Rotation = 90
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.VerticalAlignment = Element.ALIGN_TOP
            tblCenter.AddCell(cell)


            'pg3.Add(New Chunk("Campus Chief" & Environment.NewLine, certified).SetUnderline(0.6, 5))
            pg3.Add(New Chunk(" " & Environment.NewLine, certified))
            cell = CellTextAlignment.ToCenter(pg3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_BOTTOM
            tblCenter.AddCell(cell)



            cell = CellTextAlignment.ToRight("ID CARD", cardFont)
            cell.Rotation = 90
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.VerticalAlignment = Element.ALIGN_CENTER
            tblCenter.AddCell(cell)
            pdfDoc.Add(tblCenter)

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 0.1F
            stdtable.SpacingAfter = 0.0F
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            columnWidth.Add(100.0F)
            'columnWidth.Add(40.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim pc, p1, p2, p3, p4, p5, p6, p7 As New Paragraph


            'pc.Add(New Chunk("Campus Chief" & Environment.NewLine, certified).SetUnderline(0.6, 5))
            'cell = CellTextAlignment.ToCenter(pc)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_CENTER
            'cell.VerticalAlignment = Element.ALIGN_BOTTOM
            'stdtable.AddCell(cell)

            p2.Add(New Chunk(empName.ToUpper(), namefont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            stdtable.AddCell(cell)


            p3.Add(New Chunk(department, deptFont))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            stdtable.AddCell(cell)


            p1.Add(New Chunk(" Designation" & " : ", normalFontBlue))
            p1.Add(New Chunk(empJobTitle, normalFont))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)



            p4.Add(New Chunk(" Address" & " : ", normalFontBlue))
            p4.Add(New Chunk(empAddress, normalFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk(" Contact No." & " : ", normalFontBlue))
            p6.Add(New Chunk(empMobile, normalFont))
            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)


            p5.Add(New Chunk(" D.O.B." & " : ", normalFontBlue))
            p5.Add(New Chunk(dob, normalFont))
            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p7.Add(New Chunk(" Citizenship No." & " : ", normalFontBlue))
            p7.Add(New Chunk(citizenorPPNo, normalFont))


            cell = CellTextAlignment.ToCenter(p7)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'pc.Add(New Chunk(" " & Environment.NewLine, certified))
            pc.Add(New Chunk("Campus Chief" & Environment.NewLine, certified).SetUnderline(0.6, 5))
            cell = CellTextAlignment.ToCenter(pc)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.VerticalAlignment = Element.ALIGN_BOTTOM
            stdtable.AddCell(cell)


            pdfDoc.Add(stdtable)



            'BarCode
            Dim code128 As Barcode128 = New Barcode128()
            'code128.Code = barcode
            code128.Code = empNo
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            code128.Font = Nothing
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(80.0F, 12.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(28.0F, pdfDoc.PageSize.Height - 164.0F)
            pdfDoc.Add(img)

            'QRCode
            Dim registrationNo As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString
            'Dim ed As New Dao.EncryptDecrypt()
            Dim url As String = registrationNo + 1000010 'ed.Encrypt(registrationNo, "k") 'ed.Encrypt("222137")


            Dim hints As New Dictionary(Of iTextSharp.text.pdf.qrcode.EncodeHintType, Object)
            hints.Add(iTextSharp.text.pdf.qrcode.EncodeHintType.ERROR_CORRECTION, iTextSharp.text.pdf.qrcode.ErrorCorrectionLevel.L)
            Dim qrcontent As String = "http://tcioe.emis.com.np/qr.aspx?u=" & url & ""
            Dim QR As iTextSharp.text.pdf.BarcodeQRCode = New iTextSharp.text.pdf.BarcodeQRCode(qrcontent, 50, 50, hints)
            Dim QRimg As iTextSharp.text.Image = QR.GetImage()
            Dim mask As iTextSharp.text.Image = QR.GetImage()
            mask.MakeMask()
            QRimg.ImageMask = mask
            QRimg.ScaleToFit(45.0F, 45.0F)
            QRimg.SetAbsolutePosition(75.0F, pdfDoc.PageSize.Height - 76.0F)
            pdfDoc.Add(QRimg)


            'CARD TITLE
            Dim cardTitle As PdfPTable = New PdfPTable(1)
            cardTitle.SpacingBefore = 12.0F
            cardTitle.TotalWidth = pageWidth
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph

            'parag3.Add(New Paragraph(New Chunk("STUDENT IDENTITY CARD", PageHeader)))
            parag3.Add(New Paragraph(New Chunk(website, boldFontRedM)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.BorderColorTop = BaseColor.BLACK
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            'cell.BackgroundColor = BaseColor.PINK
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Employee_IDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

    Public Sub HR_IDCard_Aadhikavi(ByVal employeeId As String)
        Dim sql As String = "exec [HR].[usp_EmployeeCard] @EmployeeID='" & employeeId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte

            Dim mycard As New iTextSharp.text.Rectangle(192.0F, 120.0F)
            pdfDoc = New Document(mycard, 1.0F, 1.0F, 1.0F, 1.0F)
            'mycard.BackgroundColor = BaseColor.ORANGE

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent




            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim sectionHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 2, BaseColor.BLUE)
            Dim normalFont1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, iTextSharp.text.Font.ITALIC)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.4, 1)
            Dim SchoolName As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8.8, 1, BaseColor.BLUE)
            Dim SchoolAddress As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.0, 1)
            Dim SchoolContact As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5.0, 1)
            Dim SchoolSlogan As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6.0, 1)
            Dim PageHeader As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9.0, 1, BaseColor.RED)
            Dim certified As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16.0, 3)
            Dim raiseFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, 1)
            Dim spacer As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1.2, 0)
            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2.0, 0)

            'Card Header
            Dim sqlquery As String = "Exec  [Management].[usp_InstitutionalInfo]  @flag='f'"
            Dim institutionInfoDS As New DataSet
            institutionInfoDS = Dao.ExecuteDataset(sqlquery)

            Dim pageWidth = pdfDoc.PageSize.Width
            Dim cardHeader As PdfPTable = New PdfPTable(1)
            'cardHeader.SpacingBefore = 1.0F
            'cardHeader.SpacingAfter = 1.0F
            cardHeader.TotalWidth = pageWidth - 28
            cardHeader.LockedWidth = True
            Dim phrase As New Phrase()
            Dim parag As New Paragraph
            parag.SetLeading(0.0F, 2.0F)
            parag.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Name").ToString() & Environment.NewLine, SchoolName)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Slogan").ToString() & Environment.NewLine, SchoolSlogan)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk(institutionInfoDS.Tables(0).Rows(0).Item("Address").ToString() & Environment.NewLine, SchoolAddress)))
            parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))
            parag.Add(New Paragraph(New Chunk("Ph:" & institutionInfoDS.Tables(0).Rows(0).Item("Telephone").ToString() & Environment.NewLine, SchoolContact)))
            'parag.Add(New Phrase(New Chunk(" " & Environment.NewLine, spacer)))         
            cell = CellTextAlignment.ToCenter(parag)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardHeader.AddCell(cell)
            pdfDoc.Add(cardHeader)

            'Institution Image Logo insertion
            Try
                Dim imageUrl1 As String
                If File.Exists(HttpContext.Current.Server.MapPath("/assets/InstitutionLogo/CampusLogo.jpg")) Then
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/CampusLogo.jpg")
                Else
                    imageUrl1 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                End If
                Dim logo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl1))
                'logo.ScalePercent(12.5F)
                logo.ScaleToFit(30.0F, 30.0F)
                logo.SetAbsolutePosition(pdfDoc.PageSize.Width - pdfDoc.PageSize.Width + 4.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo)

            Catch ex As Exception

            End Try

            'TU logo
            Try
                Dim imageUrl2 = HttpContext.Current.Server.MapPath("~/assets/InstitutionLogo/tu.png")
                Dim logo2 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imageUrl2))
                'logo2.ScalePercent(35.0F)
                logo2.ScaleToFit(30.0F, 30.0F)
                logo2.SetAbsolutePosition(pdfDoc.PageSize.Width - 29.0F, pdfDoc.PageSize.Height - 33.0F)
                pdfDoc.Add(logo2)
            Catch ex As Exception

            End Try


            Dim cardTitle As PdfPTable = New PdfPTable(1)
            'cardTitle.SpacingBefore = 2.0F
            cardTitle.SpacingAfter = 1.5F
            cardTitle.TotalWidth = pageWidth - 28
            cardTitle.LockedWidth = True
            Dim parag3 As New Paragraph
            parag3.SetLeading(0.0F, 2.0F)

            parag3.Add(New Paragraph(New Chunk("STAFF IDENTITY CARD " & Environment.NewLine, PageHeader).SetUnderline(1, -3)))
            'parag3.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))

            cell = CellTextAlignment.ToCenter(parag3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.BorderColorTop = BaseColor.WHITE
            cardTitle.AddCell(cell)
            pdfDoc.Add(cardTitle)





            'Details of employee
            Dim empName As String = ds.Tables(0).Rows(0).Item("EmployeeName").ToString.Trim()
            Dim empAddress As String = ds.Tables(0).Rows(0).Item("PermanentAddress").ToString.Trim()
            Dim empNo As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString.Trim()
            Dim empJoinDate As String = ds.Tables(0).Rows(0).Item("JoiningDate").ToString.Trim()
            Dim joinDate As String = DC.ToBS(Convert.ToDateTime(empJoinDate), "n")
            Dim empJobTitle As String = ds.Tables(0).Rows(0).Item("Designation").ToString.Trim()
            Dim empMobile As String = ds.Tables(0).Rows(0).Item("Phone").ToString.Trim()
            Dim issuedate As String = DC.ToBS(Convert.ToDateTime(Date.Now), "n")

            columnWidth.Clear()
            Dim stdtable As PdfPTable = New PdfPTable(1)
            stdtable.SpacingBefore = 2.0F
            stdtable.SpacingAfter = 0.2F
            stdtable.TotalWidth = pageWidth - 5
            stdtable.LockedWidth = True
            columnWidth.Add(180.0F)
            Dim columnwidths As Integer()
            columnwidths = columnWidth.ToArray()
            stdtable.SetWidths(columnwidths)
            stdtable.HorizontalAlignment = Element.ALIGN_LEFT


            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
            p1.Add(New Chunk("Name" & " : ", boldFont))
            p1.Add(New Chunk(empName.ToUpper(), normalFont))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p6.Add(New Chunk("Designation" & " : ", boldFont))
            p6.Add(New Chunk(empJobTitle, normalFont))
            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p2.Add(New Chunk("Address" & " : ", boldFont))
            p2.Add(New Chunk(empAddress, normalFont))
            cell = CellTextAlignment.ToCenter(p2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            p3.Add(New Chunk("Contact No." & " : ", boldFont))
            p3.Add(New Chunk(empMobile, normalFont))
            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            'p4.Add(New Chunk("Joining Date" & " : ", boldFont))
            'p4.Add(New Chunk(joinDate, normalFont))
            'cell = CellTextAlignment.ToCenter(p4)
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'stdtable.AddCell(cell)


            p5.Add(New Chunk("Date of Issue" & " : ", boldFont))
            p5.Add(New Chunk(issuedate, normalFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            stdtable.AddCell(cell)

            pdfDoc.Add(stdtable)


            'Photo insertion
            Try
                Dim imgpath As String
                Dim PhotoEmp As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString()
                If File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpeg")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".jpeg"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("~/HR/Images/" & PhotoEmp & ".png")) Then
                    imgpath = "~/HR/Images/" & PhotoEmp & ".png"
                Else
                    imgpath = "~/Student/Images/Photos/avatar.png"
                End If
                Dim imgUrl = HttpContext.Current.Server.MapPath(imgpath)
                Dim photo As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(imgUrl))
                'photo.ScaleToFit(38.0F, 48.0F)
                photo.ScaleToFit(40.0F, 50.0F)
                photo.SetAbsolutePosition(pdfDoc.PageSize.Width - 46.0F, pdfDoc.PageSize.Height - 100.0F)
                pdfDoc.Add(photo)
            Catch ex As Exception

            End Try

            'Signature
            Try
                Dim signpath As String
                If File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.png")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                ElseIf File.Exists(HttpContext.Current.Server.MapPath("/Student/Images/Signature/signature.jpg")) Then
                    signpath = "~/Student/Images/Signature/signature.png"
                End If
                Dim signUrl = HttpContext.Current.Server.MapPath(signpath)
                If Not String.IsNullOrWhiteSpace(signUrl) Then
                    Dim sign As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(signUrl))
                    sign.ScaleToFit(40.0F, 25.0F)
                    sign.SetAbsolutePosition(pdfDoc.PageSize.Width - 42.0F, pdfDoc.PageSize.Height - 110.0F)
                    pdfDoc.Add(sign)
                End If

            Catch ex As Exception

            End Try


            'BarCode
            Dim Barcode As String = "E" + empNo
            Dim code128 As Barcode128 = New Barcode128()
            code128.Code = Barcode
            code128.CodeType = Barcode128.CODE128
            code128.Baseline = 8
            code128.Size = 8
            Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
            img.ScaleToFit(120.0F, 16.0F)
            img.ScaleToFitLineWhenOverflow = True
            img.ScaleToFitHeight = True
            'img.ScalePercent(100.0F)
            img.SetAbsolutePosition(60.0F, pdfDoc.PageSize.Height - 120.0F)
            pdfDoc.Add(img)

            columnWidth.Clear()
            Dim lastTbl As New PdfPTable(1)
            lastTbl.TotalWidth = pageWidth - 5
            lastTbl.LockedWidth = True
            columnWidth.Add(185.0F)
            Dim columnwidths2 As Integer()
            columnwidths2 = columnWidth.ToArray()
            lastTbl.SetWidths(columnwidths2)
            lastTbl.HorizontalAlignment = Element.ALIGN_RIGHT

            cell = CellTextAlignment.ToRight("Campus Chief", boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            lastTbl.AddCell(cell)
            pdfDoc.Add(lastTbl)




            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=EmployeeIDCard.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If

    End Sub
#End Region



#Region "Marksheets and Transcripts of Campus of International Languages"

    Public Sub Generate_MarkSheetPDF(ByVal studentID As String)
        Dim ds As New DataSet
        Dim sql As String
        sql = "exec [IL].[usp_MarkSheet] @flag = N'm',@StudentID ='249',@languageID='9',@levelID = '1',@SemesterID = '1',@YearID = '6'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim raisedFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.0, iTextSharp.text.Font.NORMAL)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.NORMAL)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.BOLD)
            Dim headerFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13.0, iTextSharp.text.Font.BOLD)
            Dim italicFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13.0, iTextSharp.text.Font.BOLDITALIC)

            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 190.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 4.0, 0)
            Dim spacer3 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 0)
            Dim spacer4 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 35.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width

            'Details
            Dim studentName As String = ds.Tables(0).Rows(0).Item("Name").ToString.Trim()
            Dim languages As String = ds.Tables(0).Rows(0).Item("Languages").ToString.Trim()
            Dim symbolNo As String = ds.Tables(0).Rows(0).Item("SymbolNo").ToString.Trim()
            Dim courseDuration As String = ds.Tables(0).Rows(0).Item("CourseDuration").ToString.Trim()
            Dim semester As String = ds.Tables(0).Rows(0).Item("Semester").ToString.Trim()
            Dim levelName As String = ds.Tables(0).Rows(0).Item("LevelName").ToString.Trim()



            Dim topTable As PdfPTable = New PdfPTable(2)
            topTable.SpacingBefore = 30.0F
            topTable.SpacingAfter = 20.0F
            topTable.TotalWidth = pageWidth - 65
            topTable.LockedWidth = True
            Dim columnwidths As Integer()
            columnWidth.Clear()
            columnWidth.Add(80.0F)
            columnWidth.Add(30.0F)
            columnwidths = columnWidth.ToArray()
            topTable.SetWidths(columnwidths)

            Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
            p1.SetLeading(0.0F, 2.0F)
            p1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)

            p2.Add(New Chunk("Name of the Student" & " : ", headerFont))
            p2.Add(New Chunk(studentName, italicFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)


            cell = CellTextAlignment.ToLeft(" ", spacer2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p3.Add(New Chunk("Language" & " : ", headerFont))
            p3.Add(New Chunk(languages, italicFont))

            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p4.Add(New Chunk("Semester" & " : ", headerFont))
            p4.Add(New Chunk(semester, italicFont))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            topTable.AddCell(cell)

            cell = CellTextAlignment.ToLeft(" ", spacer2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p5.Add(New Chunk("Symbol No" & " : ", headerFont))
            p5.Add(New Chunk(symbolNo, italicFont))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p6.Add(New Chunk("Level" & " : ", headerFont))
            p6.Add(New Chunk(levelName, italicFont))

            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            topTable.AddCell(cell)

            cell = CellTextAlignment.ToLeft(" ", spacer2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p7.Add(New Chunk("Course Duration" & " : ", headerFont))
            p7.Add(New Chunk(courseDuration, italicFont))

            cell = CellTextAlignment.ToCenter(p7)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            cell = CellTextAlignment.ToLeft(" ", spacer2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            Dim tblMarkSheet As PdfPTable = New PdfPTable(11)
            tblMarkSheet.SpacingBefore = 10.0F
            tblMarkSheet.SpacingAfter = 10.0F
            tblMarkSheet.TotalWidth = pageWidth - 60
            tblMarkSheet.LockedWidth = True

            Dim colwidths As Integer()
            columnWidth.Clear()
            columnWidth.Add(18.0F)
            columnWidth.Add(66.0F)
            columnWidth.Add(24.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(35.0F)
            colwidths = columnWidth.ToArray()
            tblMarkSheet.SetWidths(colwidths)

            cell = CellTextAlignment.ToCenter("S.N.", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Details of Papers", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)


            cell = CellTextAlignment.ToCenter("Full Marks 100", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Pass Marks 60", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Marks Obtained In", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Total", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Remarks", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Title", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Paper", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            For r As Integer = 0 To 2
                cell = CellTextAlignment.ToCenter("Internal Assmt.", boldFont)
                cell.FixedHeight = 25
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet.AddCell(cell)

                cell = CellTextAlignment.ToCenter("Final", boldFont)
                cell.FixedHeight = 25
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet.AddCell(cell)
            Next

            Dim noofrows As Integer = ds.Tables(1).Rows.Count
            Dim noofcols As Integer = ds.Tables(1).Columns.Count

            For i As Integer = 0 To noofrows - 1
                Dim sn = i + 1
                cell = CellTextAlignment.ToCenter(sn, boldFont)
                cell.FixedHeight = 40
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet.AddCell(cell)
                For j As Integer = 0 To noofcols - 1

                    If j > 1 Then
                        If j = 8 Then
                            Dim totalcol As String = ds.Tables(1).Rows(i).Item("Total").ToString
                            cell = CellTextAlignment.ToCenter(totalcol, normalFont)
                        ElseIf j = 9 Then
                            cell = CellTextAlignment.ToCenter("", normalFont)
                        ElseIf j = 7 Then
                            Dim marks As Integer = Convert.ToInt32(ds.Tables(1).Rows(i)(j).ToString)
                            If marks = 0 Then
                                cell = CellTextAlignment.ToCenter("A", normalFont)
                            ElseIf marks < 48 Then
                                Dim remarks As String = ds.Tables(1).Rows(i).Item("Remarks").ToString
                                Dim p11 As New Paragraph
                                p11.Add(New Chunk(ds.Tables(1).Rows(i)(j).ToString & " ", normalFont))
                                p11.Add(New Chunk(remarks, raisedFont).SetTextRise(4))
                                cell = CellTextAlignment.ToCenter(p11)
                            Else
                                cell = CellTextAlignment.ToCenter(ds.Tables(1).Rows(i)(j).ToString, normalFont)
                            End If
                        Else
                            cell = CellTextAlignment.ToCenter(ds.Tables(1).Rows(i)(j).ToString, normalFont)
                        End If
                    Else
                        cell = CellTextAlignment.ToCenter(ds.Tables(1).Rows(i)(j).ToString, normalFont)
                    End If
                    cell.FixedHeight = 40
                    cell.Rowspan = 1
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    If (j = 0) Then
                        cell.HorizontalAlignment = Element.ALIGN_LEFT
                    Else
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                    End If
                    tblMarkSheet.AddCell(cell)

                Next
            Next

            cell = CellTextAlignment.ToCenter("Grand Total", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 2
            cell.Colspan = 3
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter(ds.Tables(2).Rows(0).Item("FullMarksTotal").ToString, boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter(ds.Tables(2).Rows(0).Item("PassMarksTotal").ToString, boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter(ds.Tables(2).Rows(0).Item("obtainedTotal").ToString, boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            'tblMarkSheet.CompleteRow()
            pdfDoc.Add(tblMarkSheet)

            Dim resultTable As PdfPTable = New PdfPTable(3)
            resultTable.SpacingBefore = 8.0F
            resultTable.SpacingAfter = 10.0F
            resultTable.TotalWidth = pageWidth - 65
            resultTable.LockedWidth = True
            Dim colwidths2 As Integer()
            columnWidth.Clear()
            columnWidth.Add(40.0F)
            columnWidth.Add(40.0F)
            columnWidth.Add(40.0F)
            colwidths2 = columnWidth.ToArray()
            resultTable.SetWidths(colwidths2)

            Dim result As String = ds.Tables(2).Rows(0).Item("Result").ToString.Trim()
            Dim division As String = ds.Tables(2).Rows(0).Item("Division").ToString.Trim()
            Dim percentage As String = ds.Tables(2).Rows(0).Item("Percentage").ToString.Trim()

            If result.Contains("Fail") Then
                percentage = " "
            End If

            cell = CellTextAlignment.ToLeft("Result : " & result, boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            resultTable.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Division : " & division, boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            resultTable.AddCell(cell)

            cell = CellTextAlignment.ToRight("Percentage : " & percentage, boldFont)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            resultTable.AddCell(cell)

            pdfDoc.Add(resultTable)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Student_MarkSheet.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()

        End If
    End Sub

    Public Sub MarkSheetPDF_All(ByVal studentList As DataSet)
        If studentList.Tables.Count > 0 AndAlso studentList.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            contentByte = pdfWriter.DirectContent

            For s As Integer = 0 To studentList.Tables(0).Rows.Count - 1
                Dim studentId As String = studentList.Tables(0).Rows(s).Item("StudentID").ToString()

                Dim ds As New DataSet
                Dim sql As String
                sql = "exec [IL].[usp_MarkSheet] @flag = N'm',@StudentID ='" & studentId & "'"
                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    pdfDoc.NewPage()

                    Dim cell As PdfPCell
                    Dim lightGrey As New BaseColor(245, 245, 245)
                    Dim raisedFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.0, iTextSharp.text.Font.NORMAL)
                    Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.NORMAL)
                    Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.BOLD)
                    Dim headerFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13.0, iTextSharp.text.Font.BOLD)
                    Dim italicFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 13.0, iTextSharp.text.Font.BOLDITALIC)

                    Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 190.0, 0)
                    Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 4.0, 0)
                    Dim spacer3 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 0)
                    Dim spacer4 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 35.0, 0)

                    Dim pageWidth = pdfDoc.PageSize.Width

                    'Details
                    Dim studentName As String = ds.Tables(0).Rows(0).Item("Name").ToString.Trim()
                    Dim languages As String = ds.Tables(0).Rows(0).Item("Languages").ToString.Trim()
                    Dim symbolNo As String = ds.Tables(0).Rows(0).Item("SymbolNo").ToString.Trim()
                    Dim courseDuration As String = ds.Tables(0).Rows(0).Item("CourseDuration").ToString.Trim()
                    Dim semester As String = ds.Tables(0).Rows(0).Item("Semester").ToString.Trim()
                    Dim levelName As String = ds.Tables(0).Rows(0).Item("LevelName").ToString.Trim()



                    Dim topTable As PdfPTable = New PdfPTable(2)
                    topTable.SpacingBefore = 30.0F
                    topTable.SpacingAfter = 20.0F
                    topTable.TotalWidth = pageWidth - 65
                    topTable.LockedWidth = True
                    Dim columnwidths As Integer()
                    columnWidth.Clear()
                    columnWidth.Add(80.0F)
                    columnWidth.Add(30.0F)
                    columnwidths = columnWidth.ToArray()
                    topTable.SetWidths(columnwidths)

                    Dim p1, p2, p3, p4, p5, p6, p7 As New Paragraph
                    p1.SetLeading(0.0F, 2.0F)
                    p1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
                    cell = CellTextAlignment.ToCenter(p1)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    topTable.AddCell(cell)

                    p2.Add(New Chunk("Name of the Student" & " : ", headerFont))
                    p2.Add(New Chunk(studentName, italicFont))

                    cell = CellTextAlignment.ToCenter(p2)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)


                    cell = CellTextAlignment.ToLeft(" ", spacer2)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)

                    p3.Add(New Chunk("Language" & " : ", headerFont))
                    p3.Add(New Chunk(languages, italicFont))

                    cell = CellTextAlignment.ToCenter(p3)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)

                    p4.Add(New Chunk("Semester" & " : ", headerFont))
                    p4.Add(New Chunk(semester, italicFont))

                    cell = CellTextAlignment.ToCenter(p4)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT
                    topTable.AddCell(cell)

                    cell = CellTextAlignment.ToLeft(" ", spacer2)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)

                    p5.Add(New Chunk("Symbol No" & " : ", headerFont))
                    p5.Add(New Chunk(symbolNo, italicFont))

                    cell = CellTextAlignment.ToCenter(p5)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)

                    p6.Add(New Chunk("Level" & " : ", headerFont))
                    p6.Add(New Chunk(levelName, italicFont))

                    cell = CellTextAlignment.ToCenter(p6)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT
                    topTable.AddCell(cell)

                    cell = CellTextAlignment.ToLeft(" ", spacer2)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)

                    p7.Add(New Chunk("Course Duration" & " : ", headerFont))
                    p7.Add(New Chunk(courseDuration, italicFont))

                    cell = CellTextAlignment.ToCenter(p7)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)

                    cell = CellTextAlignment.ToLeft(" ", spacer2)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    topTable.AddCell(cell)
                    pdfDoc.Add(topTable)

                    Dim tblMarkSheet As PdfPTable = New PdfPTable(11)
                    tblMarkSheet.SpacingBefore = 10.0F
                    tblMarkSheet.SpacingAfter = 10.0F
                    tblMarkSheet.TotalWidth = pageWidth - 60
                    tblMarkSheet.LockedWidth = True

                    Dim colwidths As Integer()
                    columnWidth.Clear()
                    columnWidth.Add(18.0F)
                    columnWidth.Add(66.0F)
                    columnWidth.Add(24.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(30.0F)
                    columnWidth.Add(35.0F)
                    colwidths = columnWidth.ToArray()
                    tblMarkSheet.SetWidths(colwidths)

                    cell = CellTextAlignment.ToCenter("S.N.", boldFont)
                    cell.Rowspan = 2
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Details of Papers", boldFont)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)


                    cell = CellTextAlignment.ToCenter("Full Marks 100", boldFont)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Pass Marks 60", boldFont)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Marks Obtained In", boldFont)
                    cell.Rowspan = 1
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Total", boldFont)
                    cell.Rowspan = 2
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Remarks", boldFont)
                    cell.Rowspan = 2
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Title", boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 1
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Paper", boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 1
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    For r As Integer = 0 To 2
                        cell = CellTextAlignment.ToCenter("Internal Assmt.", boldFont)
                        cell.FixedHeight = 25
                        cell.Rowspan = 1
                        cell.Colspan = 1
                        cell.Border = iTextSharp.text.Rectangle.BOX
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                        tblMarkSheet.AddCell(cell)

                        cell = CellTextAlignment.ToCenter("Final", boldFont)
                        cell.FixedHeight = 25
                        cell.Rowspan = 1
                        cell.Colspan = 1
                        cell.Border = iTextSharp.text.Rectangle.BOX
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                        tblMarkSheet.AddCell(cell)
                    Next

                    Dim noofrows As Integer = ds.Tables(1).Rows.Count
                    Dim noofcols As Integer = ds.Tables(1).Columns.Count

                    For i As Integer = 0 To noofrows - 1
                        Dim sn = i + 1
                        cell = CellTextAlignment.ToCenter(sn, boldFont)
                        cell.FixedHeight = 40
                        cell.Rowspan = 1
                        cell.Colspan = 1
                        cell.Border = iTextSharp.text.Rectangle.BOX
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                        tblMarkSheet.AddCell(cell)
                        For j As Integer = 0 To noofcols - 1

                            If j > 1 Then
                                If j = 8 Then
                                    Dim totalcol As String = ds.Tables(1).Rows(i).Item("Total").ToString
                                    cell = CellTextAlignment.ToCenter(totalcol, normalFont)
                                ElseIf j = 9 Then
                                    cell = CellTextAlignment.ToCenter("", normalFont)
                                ElseIf j = 7 Then
                                    Dim marks As Integer = Convert.ToInt32(ds.Tables(1).Rows(i)(j).ToString)
                                    If marks = 0 Then
                                        cell = CellTextAlignment.ToCenter("A", normalFont)
                                    ElseIf marks < 48 Then
                                        Dim remarks As String = ds.Tables(1).Rows(i).Item("Remarks").ToString
                                        Dim p11 As New Paragraph
                                        p11.Add(New Chunk(ds.Tables(1).Rows(i)(j).ToString & " ", normalFont))
                                        p11.Add(New Chunk(remarks, raisedFont).SetTextRise(4))
                                        cell = CellTextAlignment.ToCenter(p11)
                                    Else
                                        cell = CellTextAlignment.ToCenter(ds.Tables(1).Rows(i)(j).ToString, normalFont)
                                    End If
                                Else
                                    cell = CellTextAlignment.ToCenter(ds.Tables(1).Rows(i)(j).ToString, normalFont)
                                End If
                            Else
                                cell = CellTextAlignment.ToCenter(ds.Tables(1).Rows(i)(j).ToString, normalFont)
                            End If
                            cell.FixedHeight = 40
                            cell.Rowspan = 1
                            cell.Colspan = 1
                            cell.Border = iTextSharp.text.Rectangle.BOX
                            If (j = 0) Then
                                cell.HorizontalAlignment = Element.ALIGN_LEFT
                            Else
                                cell.HorizontalAlignment = Element.ALIGN_CENTER
                            End If
                            tblMarkSheet.AddCell(cell)

                        Next
                    Next

                    cell = CellTextAlignment.ToCenter("Grand Total", boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 2
                    cell.Colspan = 3
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter(ds.Tables(2).Rows(0).Item("FullMarksTotal").ToString, boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 2
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter(ds.Tables(2).Rows(0).Item("PassMarksTotal").ToString, boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 2
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("", boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 2
                    cell.Colspan = 2
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter(ds.Tables(2).Rows(0).Item("obtainedTotal").ToString, boldFont)
                    cell.FixedHeight = 20
                    cell.Rowspan = 2
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("", boldFont)
                    cell.Rowspan = 2
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    tblMarkSheet.AddCell(cell)

                    'tblMarkSheet.CompleteRow()
                    pdfDoc.Add(tblMarkSheet)

                    Dim resultTable As PdfPTable = New PdfPTable(3)
                    resultTable.SpacingBefore = 8.0F
                    resultTable.SpacingAfter = 10.0F
                    resultTable.TotalWidth = pageWidth - 65
                    resultTable.LockedWidth = True
                    Dim colwidths2 As Integer()
                    columnWidth.Clear()
                    columnWidth.Add(40.0F)
                    columnWidth.Add(40.0F)
                    columnWidth.Add(40.0F)
                    colwidths2 = columnWidth.ToArray()
                    resultTable.SetWidths(colwidths2)

                    Dim result As String = ds.Tables(2).Rows(0).Item("Result").ToString.Trim()
                    Dim division As String = ds.Tables(2).Rows(0).Item("Division").ToString.Trim()
                    Dim percentage As String = ds.Tables(2).Rows(0).Item("Percentage").ToString.Trim()

                    If result.Contains("Fail") Then
                        percentage = " "
                    End If

                    cell = CellTextAlignment.ToLeft("Result : " & result, boldFont)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    resultTable.AddCell(cell)

                    cell = CellTextAlignment.ToCenter("Division : " & division, boldFont)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_CENTER
                    resultTable.AddCell(cell)

                    cell = CellTextAlignment.ToRight("Percentage : " & percentage, boldFont)
                    cell.Border = iTextSharp.text.Rectangle.NO_BORDER
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT
                    resultTable.AddCell(cell)

                    pdfDoc.Add(resultTable)
                End If
            Next
            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=All_Student_MarkSheet.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub

    Public Sub SingleStudentTranscript(ByVal studentID As String, ByVal languageId As String, ByVal levelId As String)
        Dim ds1 As New DataSet
        Dim sql As String
        sql = "exec [IL].[usp_Transcript] @Student=158,@language=9,@level=1"
        ds1 = Dao.ExecuteDataset(sql)
        If ds1.Tables(0).Rows.Count > 0 Then
            Dim pdfDoc As Document
            Dim columnWidth As New List(Of Integer)
            Dim contentByte As PdfContentByte
            pdfDoc = New Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F)

            Dim pdfWriter As PdfWriter
            pdfWriter = pdfWriter.GetInstance(pdfDoc, Current.Response.OutputStream)
            pdfDoc.Open()
            pdfDoc.NewPage()
            contentByte = pdfWriter.DirectContent


            Dim cell As PdfPCell
            Dim lightGrey As New BaseColor(245, 245, 245)
            Dim raisedFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8.0, iTextSharp.text.Font.NORMAL)
            Dim normalFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.NORMAL)
            Dim boldFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.0, iTextSharp.text.Font.BOLD)
            Dim headerFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12.0, iTextSharp.text.Font.BOLD)
            Dim headerFont2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10.8, iTextSharp.text.Font.BOLD)
            Dim italicFont As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12.0, iTextSharp.text.Font.BOLDITALIC)
            Dim italicFont2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10.8, iTextSharp.text.Font.BOLDITALIC)

            Dim spacer1 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 120.0, 0)
            Dim spacer2 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 2.6, 0)
            Dim spacer3 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20.0, 0)
            Dim spacer4 As New iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 35.0, 0)

            Dim pageWidth = pdfDoc.PageSize.Width

            'Details
            Dim dtInfo1 As DataTable = ds1.Tables(0)
            Dim dtMarks1 As DataTable = ds1.Tables(1)
            Dim dtResult1 As DataTable = ds1.Tables(2)
            Dim dtInfo2 As DataTable = ds1.Tables(3)
            Dim dtMarks2 As DataTable = ds1.Tables(4)
            Dim dtResult2 As DataTable = ds1.Tables(5)

            'Student Name,Language,Level
            Dim topTable As PdfPTable = New PdfPTable(2)
            topTable.SpacingBefore = 10.0F
            topTable.SpacingAfter = 3.0F
            topTable.TotalWidth = pageWidth - 65
            topTable.LockedWidth = True
            Dim columnwidths As Integer()
            columnWidth.Clear()
            columnWidth.Add(80.0F)
            columnWidth.Add(20.0F)
            columnwidths = columnWidth.ToArray()
            topTable.SetWidths(columnwidths)

            Dim p1, p2, p3, p6 As New Paragraph
            p1.SetLeading(0.0F, 1.5F)
            p1.Add(New Paragraph(New Chunk(" " & Environment.NewLine, spacer1)))
            cell = CellTextAlignment.ToCenter(p1)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            topTable.AddCell(cell)

            p2.Add(New Chunk("Name of the Student" & " : ", headerFont))
            p2.Add(New Chunk(dtInfo1.Rows(0).Item("Name").ToString.Trim, italicFont))

            cell = CellTextAlignment.ToCenter(p2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            cell = CellTextAlignment.ToLeft(" ", spacer2)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p3.Add(New Chunk("Language" & " : ", headerFont))
            p3.Add(New Chunk(dtInfo1.Rows(0).Item("Languages").ToString.Trim, italicFont))

            cell = CellTextAlignment.ToCenter(p3)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            topTable.AddCell(cell)

            p6.Add(New Chunk("Level" & " : ", headerFont))
            p6.Add(New Chunk(dtInfo1.Rows(0).Item("LevelName").ToString.Trim, italicFont))

            cell = CellTextAlignment.ToCenter(p6)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            topTable.AddCell(cell)


            'cell = CellTextAlignment.ToLeft(" ", spacer2)
            'cell.Rowspan = 1
            'cell.Colspan = 2
            'cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'topTable.AddCell(cell)
            pdfDoc.Add(topTable)

            'Semester,Symbol,CourseDuration
            Dim tblMarksHeader1 As PdfPTable = New PdfPTable(3)
            tblMarksHeader1.SpacingBefore = 5.0F
            tblMarksHeader1.SpacingAfter = 5.0F
            tblMarksHeader1.TotalWidth = pageWidth - 65
            tblMarksHeader1.LockedWidth = True
            Dim colwidths2 As Integer()
            columnWidth.Clear()
            columnWidth.Add(40.0F)
            columnWidth.Add(40.0F)
            columnWidth.Add(40.0F)
            colwidths2 = columnWidth.ToArray()
            tblMarksHeader1.SetWidths(colwidths2)

            Dim semester As String = dtInfo1.Rows(0).Item("Semester").ToString.Trim
            Dim symbolno As String = dtInfo1.Rows(0).Item("SymbolNo").ToString.Trim
            Dim courseduration As String = dtInfo1.Rows(0).Item("CourseDuration").ToString.Trim

            Dim p4, p5, p7 As New Paragraph
            p4.Add(New Chunk("Semester" & " : ", headerFont2))
            p4.Add(New Chunk(semester, italicFont2))

            cell = CellTextAlignment.ToCenter(p4)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMarksHeader1.AddCell(cell)

            p5.Add(New Chunk("Symbol No" & " : ", headerFont2))
            p5.Add(New Chunk(symbolno, italicFont2))

            cell = CellTextAlignment.ToCenter(p5)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMarksHeader1.AddCell(cell)

            p7.Add(New Chunk("Course Duration" & " : ", headerFont2))
            p7.Add(New Chunk(courseduration, italicFont2))

            cell = CellTextAlignment.ToCenter(p7)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMarksHeader1.AddCell(cell)
            pdfDoc.Add(tblMarksHeader1)

            'Marksheet table 
            Dim tblMarkSheet As PdfPTable = New PdfPTable(11)
            tblMarkSheet.SpacingBefore = 10.0F
            tblMarkSheet.SpacingAfter = 10.0F
            tblMarkSheet.TotalWidth = pageWidth - 60
            tblMarkSheet.LockedWidth = True

            Dim colwidths As Integer()
            columnWidth.Clear()
            columnWidth.Add(18.0F)
            columnWidth.Add(66.0F)
            columnWidth.Add(24.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(35.0F)
            colwidths = columnWidth.ToArray()
            tblMarkSheet.SetWidths(colwidths)

            cell = CellTextAlignment.ToCenter("S.N.", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Details of Papers", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)


            cell = CellTextAlignment.ToCenter("Full Marks 100", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Pass Marks 60", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Marks Obtained In", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Total", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Remarks", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Title", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Paper", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            For r As Integer = 0 To 2
                cell = CellTextAlignment.ToCenter("Internal Assmt.", boldFont)
                cell.FixedHeight = 25
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet.AddCell(cell)

                cell = CellTextAlignment.ToCenter("Final", boldFont)
                cell.FixedHeight = 25
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet.AddCell(cell)
            Next

            Dim noofrows As Integer = dtMarks1.Rows.Count
            Dim noofcols As Integer = dtMarks1.Columns.Count

            For i As Integer = 0 To noofrows - 1
                Dim sn = i + 1
                cell = CellTextAlignment.ToCenter(sn, boldFont)
                cell.FixedHeight = 30
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet.AddCell(cell)
                For j As Integer = 0 To noofcols - 1

                    If j > 1 Then
                        If j = 8 Then
                            Dim totalcol As String = dtMarks1.Rows(i).Item("Total").ToString
                            cell = CellTextAlignment.ToCenter(totalcol, normalFont)
                        ElseIf j = 9 Then
                            cell = CellTextAlignment.ToCenter("", normalFont)
                        ElseIf j = 7 Then
                            Dim marks As Integer = Convert.ToInt32(dtMarks1.Rows(i)(j).ToString)
                            If marks = 0 Then
                                cell = CellTextAlignment.ToCenter("A", normalFont)
                            ElseIf marks < 48 Then
                                Dim remarks As String = dtMarks1.Rows(i).Item("Remarks").ToString
                                Dim p11 As New Paragraph
                                p11.Add(New Chunk(dtMarks1.Rows(i)(j).ToString & " ", normalFont))
                                p11.Add(New Chunk(remarks, raisedFont).SetTextRise(4))
                                cell = CellTextAlignment.ToCenter(p11)
                            Else
                                cell = CellTextAlignment.ToCenter(dtMarks1.Rows(i)(j).ToString.Trim, normalFont)
                            End If
                        Else
                            cell = CellTextAlignment.ToCenter(dtMarks1.Rows(i)(j).ToString.Trim, normalFont)
                        End If
                    Else
                        cell = CellTextAlignment.ToCenter(dtMarks1.Rows(i)(j).ToString.Trim, normalFont)
                    End If
                    cell.FixedHeight = 30
                    cell.Rowspan = 1
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    If (j = 0) Then
                        cell.HorizontalAlignment = Element.ALIGN_LEFT
                    Else
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                    End If
                    tblMarkSheet.AddCell(cell)

                Next
            Next

            cell = CellTextAlignment.ToCenter("Grand Total", boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 3
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter(dtResult1.Rows(0).Item("FullMarksTotal").ToString, boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter(dtResult1.Rows(0).Item("PassMarksTotal").ToString, boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter(dtResult1.Rows(0).Item("obtainedTotal").ToString, boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet.AddCell(cell)

            'tblMarkSheet.CompleteRow()
            pdfDoc.Add(tblMarkSheet)


            'Semester,Symbol,CourseDuration ---2nd sem
            Dim tblMarksHeader2 As PdfPTable = New PdfPTable(3)
            tblMarksHeader2.SpacingBefore = 8.0F
            tblMarksHeader2.SpacingAfter = 5.0F
            tblMarksHeader2.TotalWidth = pageWidth - 65
            tblMarksHeader2.LockedWidth = True
            Dim colwidths3 As Integer()
            columnWidth.Clear()
            columnWidth.Add(40.0F)
            columnWidth.Add(40.0F)
            columnWidth.Add(40.0F)
            colwidths3 = columnWidth.ToArray()
            tblMarksHeader2.SetWidths(colwidths3)

            Dim semester2 As String = dtInfo2.Rows(0).Item("Semester").ToString.Trim
            Dim symbolno2 As String = dtInfo2.Rows(0).Item("SymbolNo").ToString.Trim
            Dim courseduration2 As String = dtInfo2.Rows(0).Item("CourseDuration").ToString.Trim

            Dim p14, p12, p13 As New Paragraph
            p14.Add(New Chunk("Semester" & " : ", headerFont2))
            p14.Add(New Chunk(semester2, italicFont2))

            cell = CellTextAlignment.ToCenter(p14)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMarksHeader2.AddCell(cell)

            p12.Add(New Chunk("Symbol No" & " : ", headerFont2))
            p12.Add(New Chunk(symbolno2, italicFont2))

            cell = CellTextAlignment.ToCenter(p12)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMarksHeader2.AddCell(cell)

            p13.Add(New Chunk("Course Duration" & " : ", headerFont2))
            p13.Add(New Chunk(courseduration2, italicFont2))

            cell = CellTextAlignment.ToCenter(p13)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMarksHeader2.AddCell(cell)
            pdfDoc.Add(tblMarksHeader2)

            'Marksheet table 
            Dim tblMarkSheet2 As PdfPTable = New PdfPTable(11)
            tblMarkSheet2.SpacingBefore = 10.0F
            tblMarkSheet2.SpacingAfter = 10.0F
            tblMarkSheet2.TotalWidth = pageWidth - 60
            tblMarkSheet2.LockedWidth = True

            Dim colwidths4 As Integer()
            columnWidth.Clear()
            columnWidth.Add(18.0F)
            columnWidth.Add(66.0F)
            columnWidth.Add(24.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(30.0F)
            columnWidth.Add(35.0F)
            colwidths4 = columnWidth.ToArray()
            tblMarkSheet2.SetWidths(colwidths4)

            cell = CellTextAlignment.ToCenter("S.N.", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Details of Papers", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)


            cell = CellTextAlignment.ToCenter("Full Marks 100", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Pass Marks 60", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Marks Obtained In", boldFont)
            cell.Rowspan = 1
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Total", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Remarks", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Title", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Paper", boldFont)
            cell.FixedHeight = 20
            cell.Rowspan = 1
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            For r As Integer = 0 To 2
                cell = CellTextAlignment.ToCenter("Internal Assmt.", boldFont)
                cell.FixedHeight = 25
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet2.AddCell(cell)

                cell = CellTextAlignment.ToCenter("Final", boldFont)
                cell.FixedHeight = 25
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet2.AddCell(cell)
            Next

            Dim noofrows2 As Integer = dtMarks2.Rows.Count
            Dim noofcols2 As Integer = dtMarks2.Columns.Count

            For i As Integer = 0 To noofrows - 1
                Dim sn = i + 1
                cell = CellTextAlignment.ToCenter(sn, boldFont)
                cell.FixedHeight = 30
                cell.Rowspan = 1
                cell.Colspan = 1
                cell.Border = iTextSharp.text.Rectangle.BOX
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblMarkSheet2.AddCell(cell)
                For j As Integer = 0 To noofcols - 1

                    If j > 1 Then
                        If j = 8 Then
                            Dim totalcol As String = dtMarks2.Rows(i).Item("Total").ToString
                            cell = CellTextAlignment.ToCenter(totalcol, normalFont)
                        ElseIf j = 9 Then
                            cell = CellTextAlignment.ToCenter("", normalFont)
                        ElseIf j = 7 Then
                            Dim marks As Integer = Convert.ToInt32(dtMarks2.Rows(i)(j).ToString)
                            If marks = 0 Then
                                cell = CellTextAlignment.ToCenter("A", normalFont)
                            ElseIf marks < 48 Then
                                Dim remarks As String = dtMarks2.Rows(i).Item("Remarks").ToString
                                Dim p11 As New Paragraph
                                p11.Add(New Chunk(dtMarks2.Rows(i)(j).ToString & " ", normalFont))
                                p11.Add(New Chunk(remarks, raisedFont).SetTextRise(4))
                                cell = CellTextAlignment.ToCenter(p11)
                            Else
                                cell = CellTextAlignment.ToCenter(dtMarks2.Rows(i)(j).ToString.Trim, normalFont)
                            End If
                        Else
                            cell = CellTextAlignment.ToCenter(dtMarks2.Rows(i)(j).ToString.Trim, normalFont)
                        End If
                    Else
                        cell = CellTextAlignment.ToCenter(dtMarks2.Rows(i)(j).ToString.Trim, normalFont)
                    End If
                    cell.FixedHeight = 30
                    cell.Rowspan = 1
                    cell.Colspan = 1
                    cell.Border = iTextSharp.text.Rectangle.BOX
                    If (j = 0) Then
                        cell.HorizontalAlignment = Element.ALIGN_LEFT
                    Else
                        cell.HorizontalAlignment = Element.ALIGN_CENTER
                    End If
                    tblMarkSheet2.AddCell(cell)

                Next
            Next

            cell = CellTextAlignment.ToCenter("Grand Total", boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 3
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter(dtResult1.Rows(0).Item("FullMarksTotal").ToString, boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter(dtResult1.Rows(0).Item("PassMarksTotal").ToString, boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 2
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter(dtResult1.Rows(0).Item("obtainedTotal").ToString, boldFont)
            cell.FixedHeight = 15
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", boldFont)
            cell.Rowspan = 2
            cell.Colspan = 1
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.BackgroundColor = BaseColor.LIGHT_GRAY
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tblMarkSheet2.AddCell(cell)

            'tblMarkSheet2.CompleteRow()
            pdfDoc.Add(tblMarkSheet2)

            'Final Result
            Dim tableFinal As PdfPTable = New PdfPTable(3)
            tableFinal.SpacingBefore = 10.0F
            tableFinal.SpacingAfter = 10.0F
            tableFinal.TotalWidth = pageWidth - 60
            tableFinal.LockedWidth = True

            Dim colwidths7 As Integer()
            columnWidth.Clear()
            columnWidth.Add(40.0F)
            columnWidth.Add(20.0F)
            columnWidth.Add(50.0F)
            colwidths7 = columnWidth.ToArray()
            tableFinal.SetWidths(colwidths7)

            Dim obtTotal_firstsem As Decimal = Convert.ToDecimal(IIf(dtResult1.Rows(0).Item("obtainedTotal").ToString.Trim = "", 0, dtResult1.Rows(0).Item("obtainedTotal").ToString.Trim))
            Dim obtTotal_secondsem As Decimal = Convert.ToDecimal(IIf(dtResult2.Rows(0).Item("obtainedTotal").ToString.Trim = "", 0, dtResult2.Rows(0).Item("obtainedTotal").ToString.Trim))
            Dim finalGrandTotal As Decimal = obtTotal_firstsem + obtTotal_secondsem
            Dim fullMarksTotal As Decimal = 800
            Dim percentage As Decimal = ((finalGrandTotal / fullMarksTotal) * 100.0)

            cell = CellTextAlignment.ToCenter("Marks in " & dtInfo1.Rows(0).Item("Semester").ToString.Trim & " Semester", boldFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter(obtTotal_firstsem.ToString(), normalFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", spacer2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Marks in " & dtInfo2.Rows(0).Item("Semester").ToString.Trim & " Semester", boldFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter(obtTotal_secondsem.ToString(), normalFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", spacer2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Grand Total", boldFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tableFinal.AddCell(cell)


            cell = CellTextAlignment.ToCenter(finalGrandTotal.ToString(), normalFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", spacer2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("Percentage", boldFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter(percentage.ToString(), normalFont)
            cell.Border = iTextSharp.text.Rectangle.BOX
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)

            cell = CellTextAlignment.ToCenter("", spacer2)
            cell.Border = iTextSharp.text.Rectangle.NO_BORDER
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            tableFinal.AddCell(cell)
            pdfDoc.Add(tableFinal)

            pdfDoc.Close()
            Current.Response.ContentType = "application/pdf"
            Current.Response.AddHeader("content-disposition", "attachment;" + "filename=Student_Transcript.pdf")
            Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Current.Response.Write(pdfDoc)
            Current.Response.End()
        End If
    End Sub
#End Region


End Class
