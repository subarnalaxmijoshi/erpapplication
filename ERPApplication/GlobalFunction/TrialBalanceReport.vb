﻿Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Web.HttpContext
Imports iTextSharp.tool.xml
Imports System.Text
Imports iTextSharp.text.html.simpleparser


Public Class TrialBalanceReport
    


    Public Shared Sub Pdf(ByVal html As String, ByVal fileName As String)
        Dim sb As New StringBuilder("")
        sb.AppendLine(html)
        Dim doc As Document

        'If (Not Directory.Exists(Current.Server.MapPath("~/Reports/Rpt"))) Then
        '    Directory.CreateDirectory(Current.Server.MapPath("~/Reports/Rpt"))
        'End If

        'Dim sWriter As StreamWriter = Nothing
        ''Dim file As File
        'If (Not System.IO.File.Exists(Current.Server.MapPath("~/Reports/Rpt/Html2Pdf.html"))) Then
        '    sWriter = New StreamWriter(Current.Server.MapPath("~/Reports/Rpt/Html2Pdf.html"))
        'Else
        '    System.IO.File.WriteAllText(Current.Server.MapPath("~/Reports/Rpt/Html2Pdf.html"), "")
        'End If
        'sWriter.WriteLine(sb.ToString)
        'sWriter.Close()


        doc = New Document(PageSize.A4, 30.0F, 30.0F, 55.0F, 15.0F)
        Dim pdfWriter As PdfWriter
        pdfWriter = pdfWriter.GetInstance(doc, Current.Response.OutputStream)
        doc.Open()
        Dim contentByte As PdfContentByte
        contentByte = pdfWriter.DirectContent

        'Dim src As StringReader
        'Dim strHtml As String
        'Dim memStream As New MemoryStream
        'Dim strWriter As New StringWriter

        'Current.Server.Execute(Current.Server.MapPath("~/Reports/Rpt/Html2Pdf.html"), strWriter)
        'strHtml = strWriter.ToString()


        'src = New StringReader(strHtml)

        pdfWriter.PageEvent = New PortraitFooter()
        Dim xmlworker As XMLWorkerHelper

        'XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, doc, )

        Using htmlWorker = New iTextSharp.text.html.simpleparser.HTMLWorker(doc)

            'HTMLWorker doesn't read a string directly but instead needs a TextReader (which StringReader subclasses)
            Using sr = New StringReader(sb.ToString())

                'Parse the HTML
                htmlWorker.Parse(sr)
            End Using
        End Using

        'Dim hw As iTextSharp.text.html.simpleparser.HTMLWorker = New HTMLWorker(doc)
        'hw.Parse(New StringReader(html))
        doc.Close()
        Dim file As String = fileName.Replace(" ", "-") & Guid.NewGuid().ToString("N").Substring(0, 8) & ".pdf"
        Current.Response.ContentType = "application/pdf"
        Current.Response.AddHeader("content-disposition", "attachment;" + "filename=" & file)
        Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Current.Response.Write(doc)

    End Sub



    Public Shared Sub Excel(ByVal html As String, ByVal fileName As String, ByVal sBranchid As String)
        Dim title As String = "Trail Balance"
        If fileName = "BalanceSheet" Then
            title = "Balance Sheet"
        ElseIf fileName = "IncomeStatement" Then
            title = "Income Statement"
        ElseIf fileName = "TrialBalanceReport" Then
            title = "Trail Balance"
        Else
            title = fileName
        End If

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" /></head>")
        sw.Write(GetInstitutionInfo.details(5, sBranchid))
        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:660px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} #listData{width:860px;}")
        sw.Write("</style>")
        sw.Write("<table style='width:860px'><tr style='border:0px solid white; width:860px;'><td style='border:0px solid white; font-size:14pt; font-weight:bold; text-align:center;' colspan='5'> <b> " & title & " </b></td></tr><tr><td colspan='6' style='border: 0px white solid;'></td></tr></table>")

        sw.Write(html)
        sw.Write("</html>")

        Current.Response.Output.Write(sw.ToString())
        Current.Response.Flush()
        Current.Response.[End]()
    End Sub


    

End Class
