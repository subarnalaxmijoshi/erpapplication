﻿<%@ Page Language="vb" AutoEventWireup="false" EnableSessionState="True" CodeBehind="fpr.aspx.vb"
    Inherits="School.fpr" %>

<html>
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>password reset</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="MobileOptimized" content="320">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/uniform/css/uniform.default.css"
        rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="/assets/css/style-metronic.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet"
        type="text/css" id="style_color" />
    <link href="/assets/css/pages/login.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />
    
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">
        .up
        {
            position: relative;
        }
    </style>
     <script src="/assets/jquery-ui-1.8.18/jquery-1.7.1.js" type="text/javascript"></script>
      <script src="/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
         
    <script type="text/javascript">
        var NewPasswordValidate = false;

        $(document).ready(function () {
            $('#form').validate();

            $("#txtnewpassword").rules('add', { required: true, messages: { required: 'New password is required.'} });
            $("#txtconfirmpassword").rules('add', { equalTo: "#txtnewpassword", messages: { equalTo: 'Confirm Password and New Password do not match.'} });

            NewPasswordValidate = function () {
                var bool = true;

                if ($('#txtnewpassword').valid() == false) bool = false;
                if ($('#txtconfirmpassword').valid() == false) bool = false;
                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtnewpassword").bind("keyup", function () {
                //TextBox left blank.
                if ($(this).val().length == 0) {
                    $("#password_strength").html("");
                    return;
                }

                //Regular Expressions.
                var regex = new Array();
                regex.push("[A-Z]"); //Uppercase Alphabet.
                regex.push("[a-z]"); //Lowercase Alphabet.
                regex.push("[0-9]"); //Digit.
                regex.push("[$@$!%*#?&]"); //Special Character.

                var passed = 0;

                //Validate for each Regular Expression.
                for (var i = 0; i < regex.length; i++) {
                    if (new RegExp(regex[i]).test($(this).val())) {
                        passed++;
                    }
                }


                //Validate for length of Password.
                if (passed > 2 && $(this).val().length > 8) {
                    passed++;
                }

                //Display status.
                var color = "";
                var strength = "";
                switch (passed) {
                    case 0:
                    case 1:
                        strength = "Weak";
                        color = "red";
                        $("#btn_changePassword").addClass("disabled");
                        break;
                    case 2:
                        strength = "Good";
                        color = "darkorange";
                        $("#btn_changePassword").removeClass("disabled");
                        break;
                    case 3:
                    case 4:
                        strength = "Strong";
                        color = "green";
                        break;
                    case 5:
                        strength = "Very Strong";
                        color = "darkgreen";
                        break;
                }
                $("#password_strength").html(strength);
                $("#password_strength").css("color", color);
            });
        });
    </script>
</head>
<body class="login">
    <div class="form-group">
        <!-- BEGIN LOGO -->
        <div class="logo" style="color: #FFFFFF;">
             <img src="assets/InstitutionLogo/tulogo1.png" alt="Logo" />
            <div runat="server" id="schoolName" style="font-size: 24px;">
                Mitra ERP 2077</div>
            <div runat="server" id="schoolAddress" style="font-size: 16px;">
                Kathmandu,Nepal</div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form id="Form1" class="login-form" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            </asp:ScriptManager>
            <h4 class="form-title " style='text-align: center'>
                Reset your password
            </h4>
        
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">
                    Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <asp:TextBox class="form-control placeholder-no-fix" type="password" 
                        runat="server" placeholder="Enter new password" name="newpassword" ID="txtnewpassword" ClientIDMode="Static">
                    </asp:TextBox>
                     <span id="password_strength"></span>
                    <asp:HiddenField ID="hdn_userID" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">
                    Confirm Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <asp:TextBox class="form-control placeholder-no-fix" type="password" 
                        runat="server" placeholder="Confirm new password" name="confirmpassword" ID="txtconfirmpassword" ClientIDMode="Static">
                    </asp:TextBox>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </div>
            </div>
            <asp:Button ID="btn_changePassword" runat="server" class="btn green pull-right" Text="Submit"
                UseSubmitBehavior="true"  OnClientClick="return NewPasswordValidate();" ClientIDMode="Static"/>
            <input type="hidden" name="id" id="id" /><br />
            <br />
            </form>
        </div>
    </div>
</body>
</html>
