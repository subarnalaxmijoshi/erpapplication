﻿Imports System.IO
Imports AjaxControlToolkit.AutoCompleteExtender

Public Class Site
    Inherits System.Web.UI.MasterPage
    Dim dao As New DatabaseDao

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Dim currentUri = Request.Url.AbsolutePath
        'Dim pa As New PageAuthority
        'Dim uid As String
        'If (Session("userId") <> Nothing) Then
        '    uid = Session("userID").ToString()
        'End If
        'Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        'If Not hasRightToView Then
        '    Response.Redirect("~/Logout.aspx")
        'End If
    End Sub
    


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrWhiteSpace(Session("userID")) And
          String.IsNullOrWhiteSpace(Session("username")) And
          String.IsNullOrWhiteSpace(Session("Role")) And
          String.IsNullOrWhiteSpace(Session("Branch")) Then
            Response.Redirect("/Login.aspx")

        End If
        Global_FY = Session("FY")
        'Global_Active_db_fy = Session("DatabaseName")
        hfUserId.Value = Session("userID").ToString().Trim()
        If Not IsPostBack Then
            GetUserPic()
            getFy()
            'GetFiscalYearForModal()
            Dim usertype = Session("Type").ToString()
            Dim uid = Session("userID").ToString()

            UserRoleMenu(uid, usertype)
        End If

    End Sub

    Protected Function getSessionUserID() As String
        Return Session("userID").ToString
    End Function

    Private Sub getFy()
        ' Dim sql As String = "setup.usp_DisplayFY @FiscalYearID='" & Session("FiscalYearID").ToString & "'"

        Dim sql As String

        sql = "setup.usp_DisplayFY @BranchID='" & Session("BranchID") & "'"
        If Not String.IsNullOrWhiteSpace(Session("FiscalYearID").ToString.Trim) Then

            sql += ",@FiscalYearID='" & Session("FiscalYearID").ToString.Trim & "'"
        End If

        Dim ds As New DataSet
        ds = dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            showFy.InnerHtml = "Fiscal Year : " + ds.Tables(0).Rows(0).Item("FiscalYearName").ToString + " (" + ds.Tables(0).Rows(0).Item("InstitutionName").ToString + "-" + ds.Tables(0).Rows(0).Item("Branch").ToString + ")"
            Session("BranchName") = ds.Tables(0).Rows(0).Item("Branch").ToString
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "message", " alert('" & Session("FiscalYearID") & "');", True)

    End Sub
    Private Sub GetUserPic()
        Dim uId As String = IIf(String.IsNullOrWhiteSpace(Session("userID")) = True, Nothing, Session("userID"))

        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC [Management].[usp_UserInfo] @Flag='p', @UserID='" & uId & "'"
        ds = dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim picSource As String = ""
            Dim pic As String = ds.Tables(0).Rows(0).Item("Photo").ToString
            If Not File.Exists(Server.MapPath("~/HR/Images/" & pic)) Then
                picSource = "/assets/img/avatar.png"
            Else
                picSource = "/HR/Images/" & pic
            End If
            username.InnerHtml = ds.Tables(0).Rows(0).Item("FullName").ToString()
            upic.InnerHtml = "<img alt='" & Session("username") & "' height='28px' width='28px' src='" & picSource & "'/>"
        End If
    End Sub

    Private Sub UserRoleMenu(Optional ByVal userId As String = "1", Optional ByVal userType As String = "user")
        Dim Dao As New DatabaseDao
        Dim sql As String
        sql = "EXEC [Users].[usp_UserMenu] @Flag ='m', @UserID = '" & userId & "', @UserType = '" & userType & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            For i As Integer = 0 To ds.Tables.Count - 1
                If ds.Tables(i).Rows.Count > 0 Then

                    sb.AppendLine("<ul class='page-sidebar-menu'>")
                    sb.AppendLine("<li>")
                    sb.AppendLine("<a href=""javascript:;"">")
                    sb.AppendLine("<i class='" & ds.Tables(i).Rows(0).Item("ModuleClass").ToString() & "'></i>")
                    sb.AppendLine("<span class='title'>" & ds.Tables(i).Rows(0).Item("ModuleName").ToString() & "</span>")
                    sb.AppendLine("<span class='arrow '></span></a>")


                    Dim subModule As String = String.Empty
                    Dim isUlOpen As Boolean = False
                    Dim isUlClosed As Boolean = False
                    sb.AppendLine("<ul class='sub-menu'>")
                    For j As Integer = 0 To ds.Tables(i).Rows.Count - 1
                        If (ds.Tables(i).Rows(j).Item("SubModuleID").ToString() = "") Then
                            sb.AppendLine("<li><a href='" & ds.Tables(i).Rows(j).Item("MenuLink").ToString() &
                                                                      "'><i class='" & ds.Tables(i).Rows(j).Item("MenuClass").ToString() &
                                                                      "'></i>" & ds.Tables(i).Rows(j).Item("MenuText").ToString() & "</a></li>")
                        End If
                    Next

                    Dim moduleName As String = ds.Tables(i).Rows(0).Item("ModuleName").ToString()
                    Dim sql1 = "EXEC [Users].[usp_Menu] @Flag ='subMenu', @UserID = '" & userId & "', @ModuleName='" & moduleName & "'"
                    Dim ds1 As New DataSet
                    ds1 = Dao.ExecuteDataset(sql1)
                    If (ds1.Tables.Count > 0) Then
                        For j As Integer = 0 To ds1.Tables.Count - 1
                            If (ds1.Tables(j).Rows.Count > 0) Then
                                sb.AppendLine("<li>")
                                sb.AppendLine("<a href=""javascript:;"">")
                                sb.AppendLine("<i class='" & ds1.Tables(j).Rows(0).Item("SubModuleClass").ToString() & "'></i>")
                                sb.AppendLine("<span class='title'>" & ds1.Tables(j).Rows(0).Item("SubModuleName").ToString() & "</span><span class='arrow '></span>")
                                sb.AppendLine("</a>")

                                sb.AppendLine("<ul class='sub-menu'>")
                                For k As Integer = 0 To ds1.Tables(j).Rows.Count - 1
                                    sb.AppendLine("<li><a href='" & ds1.Tables(j).Rows(k).Item("MenuLink").ToString() &
                                                           "'><i class='" & ds1.Tables(j).Rows(k).Item("MenuClass").ToString() &
                                                           "'></i>" & ds1.Tables(j).Rows(k).Item("MenuText").ToString() & "</a></li>")

                                Next
                                sb.AppendLine("</ul>")
                            End If

                        Next
                    End If
                    sb.AppendLine("</ul>")

                    sb.AppendLine("</li>" & Environment.NewLine & Environment.NewLine)
                End If
            Next
            sb.AppendLine("</ul>")
        End If
        userMenu.InnerHtml += sb.ToString()
    End Sub

    'Private Sub GetFiscalYearForModal()
    '    Dim sql As String
    '    Sql = "exec [Setup].[usp_FiscalYearMaster] @Flag='a' , @BranchId='" & Session("BranchID") & "'"
    '    Dim ds As New DataSet
    '    Try
    '        ddlFY.Items.Clear()
    '        ds = dao.ExecuteDataset(Sql)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            ddlFY.DataSource = ds.Tables(0)
    '            ddlFY.DataValueField = "FID_StartDate_EndDate"
    '            ddlFY.DataTextField = "FiscalYearName"
    '            ddlFY.DataBind()
    '            ddlFY.Items.Insert(0, New ListItem("-- Select a fiscal year -- ", ""))
    '        Else
    '            ddlFY.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))

    '        End If
    '    Finally
    '        ds.Dispose()
    '    End Try
    'End Sub

    'Protected Sub btnChangeFY_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnChangeFY.Click
    '    If Not String.IsNullOrWhiteSpace(ddlFY.SelectedValue) Then
    '        Dim fy() As String = ddlFY.SelectedValue.Split(",")

    '        Session("FiscalYearID") = fy(0)
    '        hfModalFy.Value = fy(0)
    '        getFy()
    '        'Response.Redirect("/Default.aspx")
    '    End If
    'End Sub
End Class