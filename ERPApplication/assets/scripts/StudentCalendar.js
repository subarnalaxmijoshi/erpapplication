﻿$(document).ready(function () {

    var filter1 = ["Time/Period", "Class/Course", "Teacher"];
    var filter2 = [{ "id": "1", "name": "name1" }, { "id": "2", "name": "name2" }, { "id": "3", "name": "name3" }, { "id": "4", "name": "nam4" }, { "id": "5", "name": "name5" }, { "id": "6", "name": "name6"}];
    function loadCalendar() {
        var callback = function (success, data) {
            if (success.status) {
                //                data = { "weekDays": [{ "id": "1", "name": "Sunday" }, { "id": "2", "name": "Monday" }, { "id": "3", "name": "Tuesday" }, { "id": "4", "name": "Wednesday" }, { "id": "5", "name": "Thursday" }, { "id": "6", "name": "Friday" }, { "id": "7", "name": "Saturday"}], "months": [{ "id": "11", "name": "Jan" }, { "id": "12", "name": "Feb" }, { "id": "13", "name": "Mar" }, { "id": "14", "name": "Apr" }, { "id": "15", "name": "May" }, { "id": "16", "name": "Jun" }, { "id": "17", "name": "Jul" }, { "id": "18", "name": "Aug" }, { "id": "19", "name": "Sep" }, { "id": "20", "name": "Oct" }, { "id": "21", "name": "Nov" }, { "id": "22", "name": "Dec"}], "years": [{ "id": "8", "name": "2071"}], "currentYearId": "8", "currentMonthId": "11" };
                console.log(data);
                data.weekStart = '1';
                data.weekHolidays = ['7'];

                var weekDays = data.weekDays;
                var months = data.months;
                var years = data.years;
                var currentYearId = data.currentYearId;
                var currentMonthId = data.currentMonthId;
                var weekStart = data.weekStart;
                var weekHolidays = data.weekHolidays;

                var month_list = '';
                for (var i = 0; i < months.length; i++) {
                    month_list += '<option value="' + months[i].id + '" ' + (months[i].id == currentMonthId ? 'selected="selected"' : '') + '>' + months[i].name + '</option>';
                }
                $('#month').html(month_list);

                var year_list = '';
                for (var i = 0; i < years.length; i++) {
                    year_list += '<option value="' + years[i].id + '" ' + (years[i].id == currentYearId ? 'selected="selected"' : '') + '>' + years[i].name + '</option>';
                }
                $('#year').html(year_list);

                var start_weekdays = false;
                var index_weekends;
                var final_index = weekDays.length;
                var stop_weekdays = false;
                var col_index = 0;
                for (var i = 0; i < final_index; i++) {
                    if (weekDays[i].id == weekStart) {
                        start_weekdays = true;
                        index_weekends = i - 1;
                    }
                    if (start_weekdays) {
                        var elem = $('.calendar_content thead .th_cell').eq(col_index);
                        elem.attr('data-id', weekDays[i].id).html(weekDays[i].name);
                        if ($.inArray(weekDays[i].id, weekHolidays) > -1) elem.attr('data-week-holiday', 'true');

                        if (i == final_index - 1 && !stop_weekdays) {
                            i = -1;
                            final_index = index_weekends + 1;
                            stop_weekdays = true;
                        }
                        col_index++;
                    }
                }

                loadDays(currentYearId, currentMonthId);
            } else {
                alert('Error occured : ' + data.responseText);
            }
        };
        Main.request("Calendar", {}, callback, "GET");
    };
    loadCalendar();

    function loadDays(yearId, monthId) {

        $('.calendar_container.loader_div > .loader').removeClass('hidden');

        var callback = function (success, data) {
            if (success.status) {
               
                console.log(data);


                var monthStartDayId = data.monthStartDayId;
                var currentDay = data.currentDay;
                var monthDays = data.monthDays;

                var th_objs = $('.calendar_content thead .th_cell');
                var si_obj = $('.calendar_content thead .th_cell[data-id="' + monthStartDayId + '"]');
                var startIndex = $.inArray(si_obj[0], th_objs);

                var hi_obj = $('.calendar_content thead .th_cell[data-week-holiday="true"]');
                var holidayIndex = [];
                for (var i = 0; i < hi_obj.length; i++) {
                    holidayIndex.push($.inArray(hi_obj[i], th_objs));
                }

                var totalWeekDays = 7;
                var totalDays = monthDays.length;
                var totalRows = Math.ceil((totalDays + startIndex) / totalWeekDays);
                var totalCells = totalRows * totalWeekDays;

                var day_list = '';
                var day_count = 0;
                var week_day_index = 0;
                for (var i = 0; i < totalCells; i++) {
                    var calculateTr = (i + 1) % totalWeekDays;
                    if (calculateTr == 1) day_list += "<tr>";
                    if (i < startIndex || day_count >= totalDays) {
                        day_list += '<td></td>';
                    } else {
                        var this_day = monthDays[day_count];
                        var events = $('.events_template').clone();
                        $('.ind_event', events).attr({ 'data-id': this_day.name });

                        if (this_day.events.event != undefined)
                            $('.ind_event.event', events).removeClass('hidden');
                        if (this_day.events.timeTable != undefined)
                            $('.ind_event.timeTable', events).removeClass('hidden');
                        if (this_day.events.holiday != undefined)
                            $('.ind_event.holiday', events).removeClass('hidden');
                        if (this_day.events.leave != undefined)
                            $('.ind_event.leave', events).removeClass('hidden');
                        if (this_day.events.examTimeTable != undefined)
                            $('.ind_event.examTimeTable', events).removeClass('hidden');
                        if (this_day.events.Notice != undefined)
                            $('.ind_event.Notice', events).removeClass('hidden');

                        day_list += '<td class="' + (currentDay == this_day.name ? 'current_day' : '') + ' ' + ($.inArray(week_day_index, holidayIndex) > -1 ?
'week_holiday' : '') + '">' + this_day.name + events.html() + '</td>';
                        day_count++;
                    }
                    if (calculateTr == 0) {
                        day_list += "</tr>";
                        week_day_index = 0;
                    } else {
                        week_day_index++;
                    }
                }
                $('.calendar_content tbody').html(day_list);
                $('.calendar_container').removeClass('hidden');
                $('.calendar_container.loader_div > .loader').addClass('hidden');
            } else {
                alert('Error occured : ' + data.responseText);
            }
        };

        var BatchId = $("#hfBatchID").val();
        var SemesterId= $("#hfSemesterID").val();

        var data = {
            currentYear: yearId,
            currentMonth: monthId,
            batchId : BatchId,
            semesterId : SemesterId
        };
        Main.request("StudentCalendarDays", data, callback);

    };

    $("#year").live('change', function () {

        var yearId = $('#year option:selected').val();
        $.ajax({
            type: 'POST',
            data: '{"yearId" : "' + yearId + '"}',
            url: '/SetupServices.svc/Months',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    if (data[i] === null) data.splice(i, 1);
                }

                var months = "";
                for (var i = 0; i < data.length; i++) {
                    months += "<option value='" + data[i].MonthId + "'>" + data[i].MonthName + "</option>";
                }
                console.log(months);
                $("#month").html(months);
                $('.month_nav').click();
            }
        });
    });

    $('#year, #month').live('change', function () {
        loadDays($('#year').val(), $('#month').val());
    });

    $('.month_nav').live('click', function () {
        var month_val = $('#month').val();
        var year_val = $('#year').val();
        var check_disabled = $(this).hasClass('disabled');
        if ($(this).hasClass('prev_month')) {
            $('.next_month').removeClass('disabled');
            var prev_option = $('#month option[value="' + month_val + '"]').prev('option');
            var prevy_option = $('#year option[value="' + year_val + '"]').prev('option');
            if ((prevy_option.length == 0 && prev_option[0] == $('#month option:first-child')[0]) || (prevy_option.length == 0 && prev_option.length == 0)) {
                $('.prev_month').addClass('disabled');
            } else {
                $('.prev_month').removeClass('disabled');
            }
            if (prev_option.length == 0) {
                if (prevy_option.length > 0) {
                    $('#month option:last-child').attr('selected', 'selected');
                    prevy_option.attr('selected', 'selected');
                }
            } else {
                prev_option.attr('selected', 'selected');
            }
        } else {
            $('.prev_month').removeClass('disabled');
            var next_option = $('#month option[value="' + month_val + '"]').next('option');
            var nexty_option = $('#year option[value="' + year_val + '"]').next('option');
            if ((nexty_option.length == 0 && next_option[0] == $('#month option:last-child')[0]) || (nexty_option.length == 0 && next_option.length == 0)) {
                $('.next_month').addClass('disabled');
            } else {
                $('.next_month').removeClass('disabled');
            }
            if (next_option.length == 0) {
                if (nexty_option.length > 0) {
                    $('#month option:first-child').attr('selected', 'selected');
                    nexty_option.attr('selected', 'selected');
                }
            } else {
                next_option.attr('selected', 'selected');
            }
        }
        if (!check_disabled) {
            loadDays($('#year').val(), $('#month').val());
        }
    });

    function createFilter1() {
        var filter1_options = '';
        for (var i = 0; i < filter1.length; i++) {
            filter1_options += '<option value="' + filter1[i] + '">' + filter1[i] + '</option>';
        }
        $('#filter1').append(filter1_options);
    }

    function createFilter2() {
        if ($("#filter1 option:selected").val() != "") {
            $("#filter2").show();
            var callback = function (success, data) {
                //data = filter2;
                data = data;
                if (success.status) {
                    var filter2_options = '<option value="">Select Option </option>';
                    for (var i = 0; i < data.length; i++) {
                        filter2_options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }
                    $('#filter2').removeClass('hidden').html(filter2_options);
                } else {
                    alert('Error occured : ' + data.responseText);
                }
            };
            var fOption = $('#filter1 option:selected').text();
            Main.request("TimeTableFilterOptions", { filterOption: fOption }, callback, "POST");
        } else {
            $("#filter2").hide();
        }
    }

    function loadEvents(eventNos) {
        var trigger = $('.currentTrigger');
        var target = $('#event-modal');

        var id = trigger.attr('data-id');
        var yearId = $("#year").val();
        var monthId = $("#month").val();
        var requestUrl = trigger.attr('data-request');
        var data = { DayNumber: id, YearId: yearId, MonthId: monthId };
        if (eventNos == 'all') data.eventNos = 'all';

        var callback = function (success, data) {
            if (success.status) {
                console.log(data);

                var events_html = '';
                for (var i = 0; i < data.length; i++) {
                    var event = data[i];
                    if (requestUrl == 'StudentTimeTable') {
                        var elem = $('.timetable_template').clone();
                        $('.name strong', elem).html(event.name);
                        $('.day em', elem).html(event.weekDay + ' - ' + event.shift);
                        $('.period', elem).html(event.period + ' ( ' + event.timeFrom + ' - ' + event.timeTo + ' )');
                        $('.subject', elem).html(event.subject + ', ' + event.batch + ', ' + event.semester + ', ' + event.section);
                    }
                    else if (requestUrl == 'Holiday') {
                        var elem = $('.holiday_template').clone();
                        $('.holiday strong', elem).html(event.holiday);
                    }
                    else if (requestUrl == "CalendarEvent") {
                        var elem = $('.calendarEvent_template').clone();
                        $('.todayevent strong', elem).html(event.dayEvent);
                        $('.todaydate em', elem).html(event.dateFrom + ' - ' + event.dateTo + ' ( ' + event.timeFrom + ' - ' + event.TimeTo + ' )');
                        $('.faculty', elem).html(event.batchTitle + ' ( ' + event.semesterName + ' - ' + event.section + ' )');
                    }
                    else if (requestUrl == "StudentExamTimeTable") {
                        var elem = $('.examTimeTable_template').clone();
                        $('.batch strong', elem).html(event.batchTitle);
                        $('.subject em', elem).html(event.subjectTitle);
                        $('.today', elem).html(event.date + ' ( ' + event.timeFrom + ' - ' + event.timeTo + ' )');
                    }
                    else if (requestUrl == "Leave") {
                        var elem = $('.leave_template').clone();
                        $('.employee strong', elem).html(event.employeeName);
                        $('.leaveType', elem).html(event.leaveType + ' ( ' + event.leaveName + ' )');
                        $('.date', elem).html(event.dateFrom + ' - ' + event.dateTo + ' ( ' + event.totalDays + ' days )');
                        $('.reason', elem).html(event.reason);
                    }
                    else if (requestUrl == "Notice") {
                        var elem = $('.notice_template').clone();
                        $('.title strong', elem).html(event.noticeTitle);
                        $('.description', elem).html(event.noticeDescription);

                    }

                    events_html += elem.html();
                }
                $('.modal-body .data_display', target).html(events_html);
                $('.modal-body .loader', target).addClass('hidden');
            } else {
                alert('Error occured : ' + data.responseText);
            }
            if (success.hideLoader != undefined) success.hideLoader();
        };
        callback.loaderDiv = '.modal-content';
        Main.request(requestUrl, data, callback);

    }

    createFilter1();
    $('#filter1').change(createFilter2);
    //$('#filter1').change(loadEvents);
    $('#filter2').change(loadEvents);
    $('.all_events').click(function () {
        loadEvents('all');
    });

    $('#event-modal').on('show.bs.modal', function (e) {
        var trigger = $(e.relatedTarget);
        var target = $(e.currentTarget);
        trigger.addClass('currentTrigger');
        var requestUrl = trigger.attr('data-request');

        $('.modal-title', target).html($(trigger).attr('data-title'));
        if (requestUrl == 'StudentTimeTable') {
            $('.data_filter').removeClass('hidden');
        } else {
            $('.all_events').removeClass('hidden');
            loadEvents();
        }
    });

    $('#event-modal').on('hidden.bs.modal', function (e) {
        $('.currentTrigger').removeClass('currentTrigger');
        $('.data_filter, .all_events').addClass('hidden');
        var target = $('#event-modal');
        $('#filter1', target).val($('#filter1 option:first').html());
        $('#filter2', target).addClass('hidden');
        $('.modal-body .data_display', target).html('');
    });


});