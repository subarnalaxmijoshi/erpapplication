﻿var Main = function() {

    return {
        mainURL: '/SetupServices.svc/',
        request: function(url, data, callback, requestType) {

            var loaderDiv = callback.loaderDiv;
            if (loaderDiv != undefined) {
                $(loaderDiv).addClass('loader_div').append('<div class="loader"></div>');
            }
            ;
            var hideLoader = function() {
                $(loaderDiv).removeClass('loader_div').children('.loader').remove();
            };

            $.ajax({
                type: requestType == undefined ? "POST" : requestType,
                url: this.mainURL + url,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json",
                dataFilter: function(data) {
                    return JSON.parse(data);
                },
                success: function(data) {
                    var successData = { };
                    successData.status = true;
                    if (loaderDiv != undefined) successData.hideLoader = hideLoader;
                    if (callback != undefined) callback(successData, data);
                },
                error: function(data) {
                    var successData = { };
                    successData.status = false;
                    if (loaderDiv != undefined) successData.hideLoader = hideLoader;
                    if (callback != undefined) callback(successData, data);
                }
            });

        },
        test1: function() {
            alert(this.somevariable);
        },
        somevariable: 'hello'
    };


}();

/*
var callback = function (success, data) {
    if (success.status) {
    } else {
        alert('Error occured : ' + data.responseText);
    }
    if (success.hideLoader != undefined) success.hideLoader();
};
Main.request(url, data, callback, requestType);
*/