﻿Imports System.Data
Imports System.Globalization

Public Class UpdateStudent_New
    Inherits System.Web.UI.Page

    Dim Dao As New DatabaseDao
    Dim sql, studentId, Semester As String

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim currentUri = Request.Url.AbsolutePath
    '    Dim pa As New PageAuthority
    '    Dim uid As String
    '    If (Session("userId") <> Nothing) Then
    '        uid = Session("userID").ToString()
    '    Else
    '        Response.Redirect("/Logout.aspx")
    '    End If
    '    Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
    '    If Not hasRightToView Then
    '        Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
    '        Response.Redirect(redirectUrl)
    '    End If
    'End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If String.IsNullOrWhiteSpace(Session("userID")) And
            String.IsNullOrWhiteSpace(Session("username")) And
            String.IsNullOrWhiteSpace(Session("Role")) And
            String.IsNullOrWhiteSpace(Session("Branch")) Then
                Response.Redirect("/Login.aspx")
            End If

            If Not IsPostBack Then
                Branch()
                GetAcademicYear()

                GetLevel()
                GetStreamByLevelId(dropdownLevel.SelectedValue.ToString)
                GetProgrammeApliedFor(dropFacultyList.SelectedValue.ToString)
                GetBatchByCourse(dropProgrammeList.SelectedValue.ToString)
                GetSubjectGroupByCourse(dropProgrammeList.SelectedValue.ToString())
                GetSemester(dropDownBatch.SelectedValue.ToString)
                GetSectionBySemester(dropDownSemester.SelectedValue.ToString)

                GetCategories()
                GetAdmissionStatus()
                GetLangauge()
                GetNationality()
                '   GetGrade()
                GetReligion()
                GetShift()

                dropCountry.Items.FindByValue("NP").Selected = True
                GetProvince()
                GetZoneByState(dropDownState.SelectedValue.ToString)
                GetDistrictByState(dropDownState.SelectedValue.ToString)
                GetMunicipalByDistrict(dropDownDistrict.SelectedValue.ToString)


                If Not String.IsNullOrWhiteSpace(Request.QueryString("StudentID")) Then
                    studentId = Request.QueryString("StudentID").ToString
                    hfStudentID.Value = studentId
                    If Not String.IsNullOrEmpty(Request.QueryString("Semester")) Then
                        Semester = Request("Semester").ToString.Trim
                        hfSemester.Value = Semester
                    End If
                    GetStudentInformation(studentId, Semester)

                    dropdownLevel_SelectedIndexChanged(sender, e)
                    dropFacultyList_SelectedIndexChanged(sender, e)
                    dropProgrammeList_SelectedIndexChanged(sender, e)
                    dropDownBatch_SelectedIndexChanged(sender, e)
                    dropDownSemester_SelectedIndexChanged(sender, e)

                    dropDownState_SelectedIndexChanged(sender, e)
                    dropDownDistrict_SelectedIndexChanged(sender, e)
                End If
            End If

        Catch ex As Exception
            Dim sb As New StringBuilder("")
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString()
        End Try
    End Sub

    Private Sub Branch()
        Dim sql As String = "exec [Setup].[usp_BranchSetup] @Flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            dropdownBranch.DataSource = ds.Tables(0)
            dropdownBranch.DataValueField = "BranchID"
            dropdownBranch.DataTextField = "BranchName"
            dropdownBranch.DataBind()
            dropdownBranch.Items.Insert(0, New ListItem("-- Select Branch -- ", ""))
        Else
            dropdownBranch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    Private Sub GetShift()
        Dim sql As String
        ddl_shift.Items.Clear()
        sql = "exec [HR].[usp_Shift] @flag='s',@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddl_shift.DataSource = ds.Tables(0)
                ddl_shift.DataTextField = "ShiftName"
                ddl_shift.DataValueField = "ShiftID"
                ddl_shift.DataBind()
                ddl_shift.Items.Insert(0, New ListItem("-- Select Shift Name -- ", ""))

            Else
                ddl_shift.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetReligion()
        Dim sql As String
        DropDownReligion.Items.Clear()
        sql = "exec [Setup].[usp_Religious] @flag='s' ,@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                DropDownReligion.DataSource = ds.Tables(0)
                DropDownReligion.DataTextField = "ReligionName"
                DropDownReligion.DataValueField = "ReligionID"
                DropDownReligion.DataBind()
                DropDownReligion.Items.Insert(0, New ListItem("-- Select Religion Name -- ", ""))

            Else
                DropDownReligion.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetAcademicYear()
        Dim sql As String


        dropAcademicYearList.Items.Clear()
        sql = "exec [Setup].[usp_AcademicYear] @flag='s',@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropAcademicYearList.DataSource = ds.Tables(0)
                dropAcademicYearList.DataTextField = "AcademicTitle"
                dropAcademicYearList.DataValueField = "AcademicYearId"
                dropAcademicYearList.DataBind()
                dropAcademicYearList.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropAcademicYearList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetProgrammeApliedFor()
        Dim sql As String


        dropProgrammeList.Items.Clear()
        sql = "exec [Setup].[usp_Course] @flag='s' "
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropProgrammeList.DataSource = ds.Tables(0)
                dropProgrammeList.DataTextField = "CourseTitle"
                dropProgrammeList.DataValueField = "CourseID"
                dropProgrammeList.DataBind()
                dropProgrammeList.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropProgrammeList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetStream()
        Dim sql As String


        dropFacultyList.Items.Clear()
        sql = "exec [Setup].[usp_Faculty] @flag='s' "
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropFacultyList.DataSource = ds.Tables(0)
                dropFacultyList.DataTextField = "FacultyTitle"

                dropFacultyList.DataValueField = "FacultyID"
                dropFacultyList.DataBind()
                dropFacultyList.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropFacultyList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetLevel()
        Dim sql As String


        dropdownLevel.Items.Clear()
        sql = "exec [Setup].[usp_Level] @Flag='p' ,@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropdownLevel.DataSource = ds.Tables(0)
                dropdownLevel.DataTextField = "LevelTitle"
                dropdownLevel.DataValueField = "LevelID"
                dropdownLevel.DataBind()
                dropdownLevel.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropdownLevel.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSemester()
        Dim sql As String


        dropDownSemester.Items.Clear()
        sql = "exec [Setup].[usp_Semester] @flag='s' "
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownSemester.DataSource = ds.Tables(0)
                dropDownSemester.DataTextField = "SemesterName"
                dropDownSemester.DataValueField = "SemesterID"
                dropDownSemester.DataBind()
                dropDownSemester.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropDownSemester.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSection()
        Dim sql As String


        dropDownSection.Items.Clear()
        sql = "exec [Setup].[usp_Section] @flag='s' "
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownSection.DataSource = ds.Tables(0)
                dropDownSection.DataTextField = "Section"
                dropDownSection.DataValueField = "SectionID"
                dropDownSection.DataBind()
                dropDownSection.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropDownSection.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetCategories()
        Dim sql As String

        dropCategoriesList.Items.Clear()
        sql = "exec [Setup].[usp_Categories] @flag='s',@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropCategoriesList.DataSource = ds.Tables(0)
                dropCategoriesList.DataTextField = "CategoriesTitle"

                dropCategoriesList.DataValueField = "CategoriesID"
                dropCategoriesList.DataBind()
                dropCategoriesList.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropCategoriesList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetAdmissionStatus()
        Dim sql As String

        dropAdmissionStatusList.Items.Clear()
        sql = "exec [Setup].[usp_AdmissionStatus] @flag='s' ,@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropAdmissionStatusList.DataSource = ds.Tables(0)
                dropAdmissionStatusList.DataTextField = "AdmissionEligibleStatusTitle"

                dropAdmissionStatusList.DataValueField = "AdmissionStatusID"
                dropAdmissionStatusList.DataBind()
                dropAdmissionStatusList.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else

                dropAdmissionStatusList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetLangauge()
        Dim sql As String

        dropLanguageList.Items.Clear()
        sql = "exec [Setup].[usp_Language] @flag='s' ,@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropLanguageList.DataSource = ds.Tables(0)
                dropLanguageList.DataTextField = "Language"

                dropLanguageList.DataValueField = "LanguageID"
                dropLanguageList.DataBind()
                dropLanguageList.Items.Insert(0, New ListItem("-- Select One -- ", ""))
            Else
                dropLanguageList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetNationality()
        Dim sql As String

        dropNationality.Items.Clear()
        sql = "exec [Setup].[usp_Nationality] @flag='s' "
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropNationality.DataSource = ds.Tables(0)
                dropNationality.DataTextField = "Nationality"

                dropNationality.DataValueField = "Nationality"
                dropNationality.DataBind()
                dropNationality.Items.Insert(0, New ListItem("-- Select Nationality -- ", ""))
            Else
                dropNationality.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub GetGrade()
    '    Dim sql As String


    '    dropDownCompletedGrade.Items.Clear()
    '    sql = "exec [Setup].[usp_Grade] @flag='s' "
    '    Dim ds As DataSet

    '    Try
    '        ds = Dao.ExecuteDataset(sql)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            dropDownCompletedGrade.DataSource = ds.Tables(0)
    '            dropDownCompletedGrade.DataTextField = "GradeTitle"

    '            dropDownCompletedGrade.DataValueField = "GradeID"
    '            dropDownCompletedGrade.DataBind()
    '            dropDownCompletedGrade.Items.Insert(0, New ListItem("-- Select Obtained Grade -- ", ""))
    '        Else
    '            dropDownCompletedGrade.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub LoadBatch()
        sql = "exec [Setup].[usp_Batch] @Flag='s'"
        dropDownBatch.Items.Clear()
        Try
            Dim ds As New DataSet
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownBatch.DataSource = ds.Tables(0)
                dropDownBatch.DataValueField = "BatchId"
                dropDownBatch.DataTextField = "BatchTitle"
                dropDownBatch.DataBind()
                dropDownBatch.Items.Insert(0, New ListItem("-- Select Batch -- ", ""))
            Else
                dropDownBatch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetStudentInformation(ByVal _studentId As String, ByVal Semester As String)
        sql = "exec [Students].[usp_StudentAllInformation] @Flag='s', @StudentId='" & _studentId & "',@Semester='" & Semester & "'"
        Dim ds As New DataSet
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Or ds.Tables(1).Rows.Count > 0 Then

                'Office use only........................

                dropdownBranch.Text = ds.Tables(1).Rows(0).Item("BranchCode").ToString
                txtApplicationFormNo.Text = ds.Tables(0).Rows(0).Item("ApplicationFormNo").ToString.Trim
                txtRegistrationNo.Text = ds.Tables(1).Rows(0).Item("RegistrationNumber").ToString.Trim
                txtRegistrationDate.Text = Convert.ToDateTime(ds.Tables(1).Rows(0).Item("RegistrationDate").ToString.Trim).ToString("dd MMM yyyy")
                dropAcademicYearList.SelectedValue = ds.Tables(0).Rows(0).Item("AcademicYear").ToString.Trim
                Dim v As String = IIf(ds.Tables(1).Rows(0).Item("IsActive").ToString.Trim = "True", 1, 0)

                'If v = "" Then
                '    v = 0
                'End If
                'dropDownStatus.Items.FindByValue(IIf(v = True, 1, 0)).Selected = True
                dropDownStatus.SelectedIndex = dropDownStatus.Items.IndexOf(dropDownStatus.Items.FindByValue(v))
            End If

            'Academic infrormation............................

            If ds.Tables(0).Rows.Count > 0 Or ds.Tables(1).Rows.Count > 0 Then
                dropdownLevel.SelectedValue = ds.Tables(0).Rows(0).Item("LevelCode").ToString.Trim
                dropFacultyList.SelectedValue = ds.Tables(0).Rows(0).Item("Faculty").ToString.Trim
                dropProgrammeList.SelectedValue = ds.Tables(0).Rows(0).Item("ProgrammeAppliedFor").ToString.Trim
                dropDownBatch.SelectedValue = ds.Tables(0).Rows(0).Item("BatchID").ToString.Trim
                dropDownSemester.SelectedValue = ds.Tables(0).Rows(0).Item("SemesterID").ToString.Trim
                dropDownSection.SelectedValue = ds.Tables(0).Rows(0).Item("SectionID").ToString.Trim
                ddlSubjectGroup.SelectedValue = ds.Tables(0).Rows(0).Item("SubjectGroupID").ToString.Trim

                dropCategoriesList.SelectedValue = ds.Tables(1).Rows(0).Item("Categories").ToString.Trim
                CheckedIsEDJ.Checked = IIf(ds.Tables(1).Rows(0).Item("IsEDJ").ToString = "True", True, False)

                txtUniversityRegdNo.Text = ds.Tables(1).Rows(0).Item("UniversityRegistrationNo").ToString.Trim
                dropAdmissionStatusList.SelectedValue = ds.Tables(0).Rows(0).Item("AdmissionStatus").ToString.Trim

                ddl_shift.SelectedValue = ds.Tables(0).Rows(0).Item("Shift").ToString.Trim
                txtRollNo.Text = ds.Tables(0).Rows(0).Item("RollNo").ToString.Trim
            End If


            'Create Student........................
            If ds.Tables(1).Rows.Count > 0 Then
                txtFirstName.Text = ds.Tables(1).Rows(0).Item("FirstName").ToString.Trim
                txtMiddleName.Text = ds.Tables(1).Rows(0).Item("MiddleName").ToString.Trim
                txtLastName.Text = ds.Tables(1).Rows(0).Item("LastName").ToString.Trim
                dropGender.SelectedValue = ds.Tables(1).Rows(0).Item("Gender").ToString.Trim
                'Dim newDate As Date = DateTime.ParseExact(ds.Tables(1).Rows(0).Item("DateOfBirth").ToString(), "MM/dd/yyyy h:mm:ss tt",
                '           Globalization.CultureInfo.InvariantCulture)
                'txtDateofBirth.Text = 
                txtDateofBirth.Text = Convert.ToDateTime(ds.Tables(1).Rows(0).Item("DateOfBirth")).ToString("dd MMM yyyy")
                txt_miti.Text = ds.Tables(1).Rows(0).Item("birthmiti").ToString.Trim
                dropMartialStatus.SelectedValue = ds.Tables(1).Rows(0).Item("MaritalStatus").ToString.Trim
                dropLanguageList.SelectedValue = ds.Tables(1).Rows(0).Item("Language").ToString.Trim
                DropDownReligion.SelectedValue = ds.Tables(1).Rows(0).Item("Religion").ToString.Trim
                dropNationality.SelectedIndex = dropNationality.Items.IndexOf(dropNationality.Items.FindByText(ds.Tables(1).Rows(0).Item("Nationality").ToString.Trim()))
                txtCitizenshipNo.Text = ds.Tables(1).Rows(0).Item("CitizenshipNo").ToString.Trim
                txtPassportNo.Text = ds.Tables(1).Rows(0).Item("PassportNo").ToString.Trim
                txtProfession.Text = ds.Tables(1).Rows(0).Item("Profession").ToString.Trim
            End If


            'Contact Information....................
            If ds.Tables(2).Rows.Count > 0 Then
                dropCountry.SelectedIndex = dropCountry.Items.IndexOf(dropCountry.Items.FindByValue(ds.Tables(2).Rows(0).Item("Country").ToString.Trim))
                'dropCountry.SelectedValue = ds.Tables(2).Rows(0).Item("Country").ToString.Trim

                Dim state = ds.Tables(2).Rows(0).Item("State").ToString.Trim
                Dim zone = ds.Tables(2).Rows(0).Item("Zone").ToString.Trim
                Dim district = ds.Tables(2).Rows(0).Item("District").ToString.Trim
                Dim municipal = ds.Tables(2).Rows(0).Item("VDC_MUL").ToString.Trim

                'txtState.Text = ds.Tables(2).Rows(0).Item("State").ToString.Trim
                dropDownState.SelectedValue = state
                dropDownZone.SelectedValue = zone
                dropDownDistrict.SelectedValue = district
                dropDownMunicipal.SelectedValue = municipal



                'txtVDCMUL.Text = ds.Tables(2).Rows(0).Item("VDC_MUL").ToString.Trim
                txtAddress1.Text = ds.Tables(2).Rows(0).Item("Address1").ToString.Trim
                txtAddress2.Text = ds.Tables(2).Rows(0).Item("Address2").ToString.Trim
                txtCity.Text = ds.Tables(2).Rows(0).Item("City").ToString.Trim

                txtPINCode.Text = ds.Tables(2).Rows(0).Item("PINCode").ToString.Trim

                txtRoadNo.Text = ds.Tables(2).Rows(0).Item("RoadNo").ToString.Trim

                txtTole.Text = ds.Tables(2).Rows(0).Item("Tole").ToString.Trim
                txtPhoneNo.Text = ds.Tables(2).Rows(0).Item("PhoneNo").ToString.Trim
                txtmobileNo.Text = ds.Tables(2).Rows(0).Item("MobileNo").ToString.Trim
                txtEmail.Text = ds.Tables(2).Rows(0).Item("Email").ToString.Trim
            End If



            'Previous Istitution Information...............................
            If ds.Tables(3).Rows.Count > 0 Then
                txtInstitutionalName.Text = ds.Tables(3).Rows(0).Item("InstitutionalName").ToString.Trim
                txtPreAddress.Text = ds.Tables(3).Rows(0).Item("PreAddress").ToString.Trim
                txtPreviousInstitutionEmail.Text = ds.Tables(3).Rows(0).Item("PreviousInstitutionEmail").ToString.Trim
                txtPreviousPhone.Text = ds.Tables(3).Rows(0).Item("Phone").ToString.Trim
                txtPreviousPostbox.Text = ds.Tables(3).Rows(0).Item("PostboxNo").ToString.Trim
                txtPreviousContactPerson.Text = ds.Tables(3).Rows(0).Item("ContactPerson").ToString.Trim
                txtPreviousPosition.Text = ds.Tables(3).Rows(0).Item("Position").ToString.Trim
                txtPreviousReason.Text = ds.Tables(3).Rows(0).Item("Reason").ToString.Trim
            End If



            'Complete Education.............................
            If ds.Tables(4).Rows.Count > 0 Then
                txtCompletedLevel.Text = ds.Tables(4).Rows(0).Item("Level").ToString.Trim
                txtCompletedCourse.Text = ds.Tables(4).Rows(0).Item("Course").ToString.Trim
                txtCompletedBoard.Text = ds.Tables(4).Rows(0).Item("Board").ToString.Trim
                txtCompletedStream.Text = ds.Tables(4).Rows(0).Item("Stream").ToString.Trim
                txtCompletedInstitutionName.Text = ds.Tables(4).Rows(0).Item("InstitutionName").ToString.Trim
                txtCompletedEducatioalAddress.Text = ds.Tables(4).Rows(0).Item("EducationalAddress").ToString.Trim
                txtCompletedDivision_Or_Merit.Text = ds.Tables(4).Rows(0).Item("Division_Or_Merit").ToString.Trim
                dropDownCompletedGrade.Text = ds.Tables(4).Rows(0).Item("Grade").ToString.Trim
                txtCompleteDate.Text = ds.Tables(4).Rows(0).Item("CompleteDate").ToString.Trim
                txtCompletedMarkPercentage.Text = ds.Tables(4).Rows(0).Item("MarkPercentage").ToString.Trim
            End If




            'Parent Information......................................
            If ds.Tables(5).Rows.Count > 0 Then
                txtFatherName.Text = ds.Tables(5).Rows(0).Item("FathersName").ToString.Trim
                txtFatherOccupation.Text = ds.Tables(5).Rows(0).Item("FOccupation").ToString.Trim
                txtFatherMobile.Text = ds.Tables(5).Rows(0).Item("FMobile").ToString.Trim
                txtFatherEmail.Text = ds.Tables(5).Rows(0).Item("FEmail").ToString.Trim
                txtMotherName.Text = ds.Tables(5).Rows(0).Item("MothersName").ToString.Trim
                txtMotherOccupation.Text = ds.Tables(5).Rows(0).Item("MOccupation").ToString.Trim
                txtMotherMobile.Text = ds.Tables(5).Rows(0).Item("MMobile").ToString.Trim
                txtMotherEmail.Text = ds.Tables(5).Rows(0).Item("MEmail").ToString.Trim

                ddlFQualification.SelectedValue = ds.Tables(5).Rows(0).Item("FQualification").ToString.Trim
                ddlMQualification.SelectedValue = ds.Tables(5).Rows(0).Item("MQualification").ToString.Trim
            End If


            'Guardian Information..........................................
            If ds.Tables(6).Rows.Count > 0 Then
                txtGuardianName.Text = ds.Tables(6).Rows(0).Item("Name").ToString.Trim
                txtGuardianAddress.Text = ds.Tables(6).Rows(0).Item("GAddress").ToString.Trim
                txtGuardianOccupation.Text = ds.Tables(6).Rows(0).Item("Occupation").ToString
                txtGuardianMobile.Text = ds.Tables(6).Rows(0).Item("MobileNo").ToString.Trim
                txtGuardianEmail.Text = ds.Tables(6).Rows(0).Item("Email").ToString.Trim
                txtRelationToStudent.Text = ds.Tables(6).Rows(0).Item("RelationToStudent").ToString.Trim
                txtGuardianPhoneNo.Text = ds.Tables(6).Rows(0).Item("PhoneNo").ToString.Trim
            End If



            'Additional information....................................
            If ds.Tables(7).Rows.Count > 0 Then
                txtHobbies.Text = ds.Tables(7).Rows(0).Item("Hobbies").ToString
                txtOccupation.Text = ds.Tables(7).Rows(0).Item("Occupation").ToString
                txtRelative.Text = ds.Tables(7).Rows(0).Item("Relative").ToString
                txtResidenceNo.Text = ds.Tables(7).Rows(0).Item("ResidenceNo").ToString
                txtSponserBy.Text = ds.Tables(7).Rows(0).Item("SponserBy").ToString
                txtSchoolshipFor.Text = ds.Tables(7).Rows(0).Item("SchoolshipFor").ToString
                txtAwards.Text = ds.Tables(7).Rows(0).Item("Awards").ToString
                txtNotes.Text = ds.Tables(7).Rows(0).Item("Notes").ToString
            End If



            'Health Information.............................
            If ds.Tables(8).Rows.Count > 0 Then
                txtHeight.Text = ds.Tables(8).Rows(0).Item("Height").ToString
                txtWeight.Text = ds.Tables(8).Rows(0).Item("Weight").ToString
                dropMeasurementUnit.SelectedValue = ds.Tables(8).Rows(0).Item("MeasurementUnit").ToString
                txtHospitalName.Text = ds.Tables(8).Rows(0).Item("HospitalName").ToString
                txtDoctorName.Text = ds.Tables(8).Rows(0).Item("DoctorName").ToString
                dropBloodGroup.SelectedValue = ds.Tables(8).Rows(0).Item("BloodGroup").ToString
                txtBloodPressure.Text = ds.Tables(8).Rows(0).Item("BloodPressure").ToString
                txtAllergy.Text = ds.Tables(8).Rows(0).Item("Allergy").ToString
                txtNutritition.Text = ds.Tables(8).Rows(0).Item("Nutritition").ToString
                txtHealthNotes.Text = ds.Tables(8).Rows(0).Item("HealthNotes").ToString
                CheckIswearLens.Checked = Convert.ToBoolean(ds.Tables(8).Rows(0).Item("IswearLans").ToString)
            End If

        Catch ex As Exception
            Dim sb As New StringBuilder("")
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString()
        End Try

    End Sub

    Private Sub SetStudentInformation(ByVal _studentId As String, ByVal Semester As String)
        Dim _mybirthdate As String = ""
        If Not String.IsNullOrWhiteSpace(txtDateofBirth.Text) Then
            'Dim mydate() As String = txtDateofBirth.Text.Split("/")
            '_mybirthdate = mydate(1) + "/" + mydate(0) + "/" + mydate(2)
            _mybirthdate = txtDateofBirth.Text
        Else
            _mybirthdate = Date.Now.ToString("dd MMM yyyy")
        End If


        '  If Not String.IsNullOrWhiteSpace(txtFirstName.Text) Then
        sql = "exec [Students].[usp_StudentAllInformation] @Flag='i', @StudentId='" & _studentId & "', @FirstName='" & Dao.FilterQuote(txtFirstName.Text.Trim) & "', @MiddleName='" & Dao.FilterQuote(txtMiddleName.Text.Trim) & "' , @LastName='" & Dao.FilterQuote(txtLastName.Text.Trim) & "' , @Gender='" & Dao.FilterQuote(dropGender.SelectedValue.ToString) & "' , @DOB='" & Dao.FilterQuote(_mybirthdate) & "' , @MaritalStatus='" & Dao.FilterQuote(dropMartialStatus.SelectedValue) & "' , @Nationality='" & Dao.FilterQuote(dropNationality.SelectedValue) & "' ,@Religion='" & Dao.FilterQuote(DropDownReligion.SelectedValue) & "', @Language='" & Dao.FilterQuote(dropLanguageList.SelectedValue) & "' , @CitizenshipNo='" & Dao.FilterQuote(txtCitizenshipNo.Text.Trim) & "' , @PassportNo='" & Dao.FilterQuote(txtPassportNo.Text.Trim) & "' ,@Profession='" & Dao.FilterQuote(txtProfession.Text.Trim) & "', @RegistrationNo='" & Dao.FilterQuote(txtRegistrationNo.Text.Trim) & "' ,  @RegistrationDate='" & Dao.FilterQuote(txtRegistrationDate.Text.Trim) & "'"
        sql += ",@BranchCode='" & Dao.FilterQuote(dropdownBranch.SelectedValue) & "', @LevelCode='" & Dao.FilterQuote(dropdownLevel.SelectedValue) & "', @FacultyCode='" & Dao.FilterQuote(dropFacultyList.SelectedValue) & "', @Section='" & Dao.FilterQuote(dropDownSection.SelectedValue) & "', @BatchCode='" & Dao.FilterQuote(dropDownBatch.SelectedValue) & "', @ApplicationFormNo='" & Dao.FilterQuote(txtApplicationFormNo.Text) & "', @ProgrammeAppliedFor='" & Dao.FilterQuote(dropProgrammeList.SelectedValue) & "', @UniversityRegdNo='" & txtUniversityRegdNo.Text & "', @AdmissionStatus='" & Dao.FilterQuote(dropAdmissionStatusList.SelectedValue) & "', @RollNo='" & Dao.FilterQuote(txtRollNo.Text) & "', @BatchID='" & Dao.FilterQuote(dropDownBatch.SelectedValue) & "', @SemesterID='" & Dao.FilterQuote(dropDownSemester.SelectedValue) & "', @Categories='" & Dao.FilterQuote(dropCategoriesList.SelectedValue) & "', @Faculty='" & Dao.FilterQuote(dropFacultyList.SelectedValue) & "', @AcademicYear='" & Dao.FilterQuote(dropAcademicYearList.SelectedValue) & "'"
        sql += ", @Hobbies='" & Dao.FilterQuote(txtHobbies.Text) & "', @Occupation='" & Dao.FilterQuote(txtOccupation.Text) & "', @Relative='" & Dao.FilterQuote(txtRelative.Text) & "', @ResidenceNo='" & Dao.FilterQuote(txtResidenceNo.Text) & "', @SponserBy='" & Dao.FilterQuote(txtSponserBy.Text) & "', @SchoolshipFor='" & Dao.FilterQuote(txtSchoolshipFor.Text) & "', @Awards='" & Dao.FilterQuote(txtAwards.Text) & "', @Notes='" & Dao.FilterQuote(txtNotes.Text) & "',  @Level='" & Dao.FilterQuote(txtCompletedLevel.Text) & "', @Course='" & Dao.FilterQuote(txtCompletedCourse.Text) & "', @Board='" & Dao.FilterQuote(txtCompletedBoard.Text) & "', @Stream='" & Dao.FilterQuote(txtCompletedStream.Text) & "', @InstitutionName='" & Dao.FilterQuote(txtCompletedInstitutionName.Text) & "', @EducationalAddress='" & Dao.FilterQuote(txtCompletedEducatioalAddress.Text) & "', @DivisionOrMerit='" & Dao.FilterQuote(txtCompletedDivision_Or_Merit.Text) & "', @Grade='" & Dao.FilterQuote(dropDownCompletedGrade.Text) & "', @CompleteDate='" & Dao.FilterQuote(txtCompleteDate.Text) & "', @MarkPercentage='" & Dao.FilterQuote(txtCompletedMarkPercentage.Text) & "'"
        sql += ", @Address1='" & Dao.FilterQuote(txtAddress1.Text) & "', @Address2='" & Dao.FilterQuote(txtAddress2.Text) & "', @City='" & Dao.FilterQuote(txtCity.Text) & "', @State='" & Dao.FilterQuote(dropDownState.SelectedValue) & "', @PinCode='" & Dao.FilterQuote(txtPINCode.Text) & "', @Country='" & Dao.FilterQuote(dropCountry.SelectedValue) & "', @RoadNo='" & Dao.FilterQuote(txtRoadNo.Text) & "', @Zone='" & Dao.FilterQuote(dropDownZone.SelectedValue) & "',  @District='" & Dao.FilterQuote(dropDownDistrict.SelectedValue) & "', @VDCMul='" & Dao.FilterQuote(dropDownMunicipal.SelectedValue) & "', @Tol='" & Dao.FilterQuote(txtTole.Text) & "', @PhoneNo='" & Dao.FilterQuote(txtPhoneNo.Text) & "', @MobileNo='" & Dao.FilterQuote(txtmobileNo.Text) & "', @Email= '" & Dao.FilterQuote(txtEmail.Text) & "', @GuardianName='" & Dao.FilterQuote(txtGuardianName.Text) & "', @GAddress='" & Dao.FilterQuote(txtGuardianAddress.Text) & "', @GOccupation='" & Dao.FilterQuote(txtGuardianOccupation.Text) & "', @GMobileNo='" & Dao.FilterQuote(txtGuardianMobile.Text) & "', @GEmail='" & Dao.FilterQuote(txtGuardianEmail.Text) & "', @RelationToStudent='" & Dao.FilterQuote(txtRelationToStudent.Text) & "', @GPhoneNo='" & Dao.FilterQuote(txtGuardianPhoneNo.Text) & "'"
        sql += ", @Height='" & Dao.FilterQuote(txtHeight.Text) & "', @Weight='" & Dao.FilterQuote(txtWeight.Text) & "', @MeasurementUnit='" & Dao.FilterQuote(dropMeasurementUnit.SelectedValue) & "', @HospitalName='" & Dao.FilterQuote(txtHospitalName.Text) & "', @DoctorName='" & Dao.FilterQuote(txtDoctorName.Text) & "', @BloodPressure='" & Dao.FilterQuote(txtBloodPressure.Text) & "', @BloodGroup='" & Dao.FilterQuote(dropBloodGroup.SelectedValue) & "', @Allergy='" & Dao.FilterQuote(txtAllergy.Text) & "', @Nutrition='" & Dao.FilterQuote(txtNutritition.Text) & "', @HealthNotes='" & Dao.FilterQuote(txtHealthNotes.Text) & "', @IsWearLens='" & Dao.FilterQuote(CheckIswearLens.Checked) & "', @FathersName='" & Dao.FilterQuote(txtFatherName.Text) & "', @FOccupation='" & Dao.FilterQuote(txtFatherOccupation.Text) & "', @FMobile= '" & Dao.FilterQuote(txtFatherMobile.Text) & "', @FEmail='" & Dao.FilterQuote(txtFatherEmail.Text) & "', @MothersName='" & Dao.FilterQuote(txtMotherName.Text) & "', @MMobile='" & Dao.FilterQuote(txtMotherMobile.Text) & "', @MOccupation='" & Dao.FilterQuote(txtMotherOccupation.Text) & "', @MEmail='" & Dao.FilterQuote(txtMotherEmail.Text) & "',  @InstitutionalName='" & Dao.FilterQuote(txtInstitutionalName.Text) & "', @PreAddress='" & Dao.FilterQuote(txtPreAddress.Text) & "', @PreviousInstitutionEmail='" & Dao.FilterQuote(txtPreviousInstitutionEmail.Text) & "', @PreInstitutionPhone='" & Dao.FilterQuote(txtPreviousPhone.Text) & "', @PreInstitutionPostBoxNo='" & Dao.FilterQuote(txtPreviousPostbox.Text) & "', @PreInstitutionContactPerson='" & Dao.FilterQuote(txtPreviousContactPerson.Text) & "', @PreInstitutionPosition='" & Dao.FilterQuote(txtPreviousPosition.Text) & "', @PreInstitutionReason='" & Dao.FilterQuote(txtPreviousReason.Text) & "',@Shift='" & Dao.FilterQuote(ddl_shift.SelectedValue) & "',@ModifiedBy='" & Session("userID") & "',@miti='" & Dao.FilterQuote(txt_miti.Text.Trim) & "',@IsActive='" & dropDownStatus.SelectedValue & "',@IsEDJ='" & IIf(CheckedIsEDJ.Checked = True, 1, 0) & "',@Semester='" & Semester & "'"

        If ddlSubjectGroup.SelectedIndex > 0 Then
            sql += ",@SubjectGroupID='" & Dao.FilterQuote(ddlSubjectGroup.SelectedValue) & "'"
        End If

        sql += ",@FQualification='" & Dao.FilterQuote(ddlFQualification.SelectedValue) & "',@MQualification='" & Dao.FilterQuote(ddlMQualification.SelectedValue) & "'"
        '   End If


        Dim ds As New DataSet
        Dim sb As New StringBuilder()
        Try
            ds = Dao.ExecuteDataset(sql)

            If ds.Tables(0).Rows.Count > 0 Then

                sb.AppendLine("<div class='note note-success'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
                '    FieldClear()
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('" + ds.Tables(0).Rows(0).Item("mes").ToString + "');", True)
                'studentId = Request.QueryString("StudentID").ToString
                'Semester = Request("Semester").ToString.Trim
                'GetStudentInformation(studentId, Semester)


            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('" + ex.Message + "');", True)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click

        SetStudentInformation(hfStudentID.Value, hfSemester.Value)
    End Sub



    Private Sub GetBatchByCourse(ByVal courseId As String)
        Dim sql As String
        dropDownBatch.Items.Clear()
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Batch] @Flag='c', @CourseID='" & courseId & "',@BranchID='" & Session("BranchID") & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownBatch.DataSource = ds.Tables(0)
                dropDownBatch.DataTextField = "BatchTitle"
                dropDownBatch.DataValueField = "BatchID"
                dropDownBatch.DataBind()
                dropDownBatch.Items.Insert(0, New ListItem("-- Select Batch -- ", ""))
            Else
                dropDownBatch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSubjectGroupByCourse(ByVal courseId As String)
        Dim sql As String
        ddlSubjectGroup.Items.Clear()
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_SubjectGroup] @Flag='a', @CourseID='" & courseId & "',@BranchID=" & Session("BranchID")
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSubjectGroup.DataSource = ds.Tables(0)
                ddlSubjectGroup.DataTextField = "SubjectGroup"
                ddlSubjectGroup.DataValueField = "SubjectGroupID"
                ddlSubjectGroup.DataBind()
                ddlSubjectGroup.Items.Insert(0, New ListItem("-- Select Subject Group -- ", ""))
            Else
                ddlSubjectGroup.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetProgrammeApliedFor(ByVal facultyID As String)
        Dim sql As String


        dropProgrammeList.Items.Clear()
        sql = "exec [Setup].[usp_Course] @flag='g', @FacultyID='" & facultyID & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropProgrammeList.DataSource = ds.Tables(0)
                dropProgrammeList.DataTextField = "CourseTitle"

                dropProgrammeList.DataValueField = "CourseID"
                dropProgrammeList.DataBind()
                dropProgrammeList.Items.Insert(0, New ListItem("-- Select Programe Applied For -- ", ""))
            Else
                dropProgrammeList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetStreamByLevelId(ByVal levelID As String)
        Dim sql As String


        dropFacultyList.Items.Clear()
        sql = "exec [Setup].[usp_Faculty] @flag='l', @LevelId='" & levelID & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropFacultyList.DataSource = ds.Tables(0)
                dropFacultyList.DataTextField = "FacultyTitle"

                dropFacultyList.DataValueField = "FacultyID"
                dropFacultyList.DataBind()
                dropFacultyList.Items.Insert(0, New ListItem("-- Select Faculty -- ", ""))
            Else
                dropFacultyList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetLevelByBoard(ByVal boardId As String)
        Dim sql As String
        dropdownLevel.Items.Clear()
        sql = "exec [Setup].[usp_Level] @Flag='b', @BoardID='" & boardId & "'"
        Dim ds As New DataSet
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropdownLevel.DataSource = ds.Tables(0)
                dropdownLevel.DataTextField = "LevelTitle"
                dropdownLevel.DataValueField = "LevelId"
                dropdownLevel.DataBind()
                dropdownLevel.Items.Insert(0, New ListItem("-- Select Level -- ", ""))
            Else
                dropdownLevel.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSemester(ByVal batchID As String)
        Dim sql As String


        dropDownSemester.Items.Clear()
        sql = "exec [Setup].[usp_Semester] @flag='b', @BatchID='" & batchID & "',@BranchID='" & Session("BranchID") & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownSemester.DataSource = ds.Tables(0)
                dropDownSemester.DataTextField = "SemesterName"

                dropDownSemester.DataValueField = "SemesterID"
                dropDownSemester.DataBind()
                dropDownSemester.Items.Insert(0, New ListItem("-- Select Semester -- ", ""))
            Else
                dropDownSemester.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSectionBySemester(ByVal semesterID As String)
        Dim sql As String


        dropDownSection.Items.Clear()
        sql = "exec [Setup].[usp_Section] @flag='a', @SemesterID='" & semesterID & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownSection.DataSource = ds.Tables(0)
                dropDownSection.DataTextField = "Section"

                dropDownSection.DataValueField = "SectionID"
                dropDownSection.DataBind()
                dropDownSection.Items.Insert(0, New ListItem("-- Select Section -- ", ""))
            Else
                dropDownSection.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub dropDownBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropDownBatch.SelectedIndexChanged
        GetSemester(dropDownBatch.SelectedValue.ToString())
    End Sub

    Protected Sub dropdownLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropdownLevel.SelectedIndexChanged
        GetStreamByLevelId(dropdownLevel.SelectedValue.ToString())
    End Sub

    Protected Sub dropFacultyList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropFacultyList.SelectedIndexChanged
        GetProgrammeApliedFor(dropFacultyList.SelectedValue.ToString)
    End Sub

    Protected Sub dropProgrammeList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropProgrammeList.SelectedIndexChanged
        GetBatchByCourse(dropProgrammeList.SelectedValue.ToString)
        GetSubjectGroupByCourse(dropProgrammeList.SelectedValue.ToString())
    End Sub

    Protected Sub dropDownSemester_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropDownSemester.SelectedIndexChanged
        GetSectionBySemester(dropDownSemester.SelectedValue.ToString)
    End Sub


    Private Sub GetProvince()
        Dim sql As String
        Dim ds As DataSet
        sql = "exec [Setup].[usp_Province]"
        dropDownState.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownState.DataSource = ds.Tables(0)
                dropDownState.DataTextField = "ProvinceName"
                dropDownState.DataValueField = "ProvinceID"
                dropDownState.DataBind()
                dropDownState.Items.Insert(0, New ListItem("-- Select State --", ""))
            Else
                dropDownState.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetZoneByState(ByVal provinceId As String)
        Dim sql As String
        Dim ds As DataSet
        dropDownZone.Items.Clear()
        sql = "exec [Setup].[usp_Zone] @flag='s',@ProvinceID='" & provinceId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownZone.DataSource = ds.Tables(0)
                dropDownZone.DataTextField = "Zone"
                dropDownZone.DataValueField = "ZoneID"
                dropDownZone.DataBind()
                dropDownZone.Items.Insert(0, New ListItem("-- Select Zone --", ""))
            Else
                dropDownZone.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetDistrictByState(ByVal provinceId As String)
        Dim sql As String
        Dim ds As New DataSet

        sql = "exec [Setup].[usp_District] @flag='s' ,@ProvinceID='" & provinceId & "'"
        dropDownDistrict.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownDistrict.DataSource = ds.Tables(0)
                dropDownDistrict.DataTextField = "DistrictName"
                dropDownDistrict.DataValueField = "DistrictID"
                dropDownDistrict.DataBind()
                dropDownDistrict.Items.Insert(0, New ListItem("-- Select District --", ""))
            Else
                dropDownDistrict.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetMunicipalByDistrict(ByVal districtId As String)
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Municipality] @flag='s',@DistrictID='" & districtId & "'"
        dropDownMunicipal.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownMunicipal.DataSource = ds.Tables(0)
                dropDownMunicipal.DataTextField = "Municipality"
                dropDownMunicipal.DataValueField = "MunicipalityID"
                dropDownMunicipal.DataBind()
                dropDownMunicipal.Items.Insert(0, New ListItem("-- Select Municipality --", ""))
            Else
                dropDownMunicipal.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dropDownState_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropDownState.SelectedIndexChanged
        GetZoneByState(dropDownState.SelectedValue)
        GetDistrictByState(dropDownState.SelectedValue)
    End Sub

    Protected Sub dropDownDistrict_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropDownDistrict.SelectedIndexChanged
        GetMunicipalByDistrict(dropDownDistrict.SelectedValue)
    End Sub

    Sub FieldClear()
        txtApplicationFormNo.Text = String.Empty
        txtRegistrationNo.Text = String.Empty
        txtRegistrationDate.Text = String.Empty
        dropAcademicYearList.SelectedIndex = 0

        dropdownLevel.SelectedIndex = 0
        dropFacultyList.SelectedIndex = 0
        dropProgrammeList.SelectedIndex = 0
        dropDownBatch.SelectedIndex = 0
        dropDownSemester.SelectedIndex = 0
        dropDownSection.SelectedIndex = 0
        dropCategoriesList.SelectedIndex = 0
        CheckedIsEDJ.Checked = False
        txtUniversityRegdNo.Text = String.Empty
        dropAdmissionStatusList.SelectedIndex = 0
        ddl_shift.SelectedIndex = 0
        txtRollNo.Text = String.Empty

        txtFirstName.Text = String.Empty
        txtMiddleName.Text = String.Empty
        txtLastName.Text = String.Empty
        dropGender.SelectedIndex = 0
        txtDateofBirth.Text = String.Empty
        txt_miti.Text = String.Empty
        dropMartialStatus.SelectedIndex = 0
        dropLanguageList.SelectedIndex = 0
        DropDownReligion.SelectedIndex = 0
        dropNationality.SelectedIndex = 0
        txtCitizenshipNo.Text = String.Empty
        txtPassportNo.Text = String.Empty
        txtProfession.Text = String.Empty

        dropCountry.SelectedIndex = 0
        dropDownZone.SelectedIndex = 0
        txtAddress1.Text = String.Empty
        txtAddress2.Text = String.Empty
        'txtVDCMUL.Text = String.Empty
        dropDownMunicipal.SelectedIndex = 0
        txtCity.Text = String.Empty
        'txtState.Text = String.Empty
        dropDownState.SelectedIndex = 0
        txtPINCode.Text = String.Empty
        txtRoadNo.Text = String.Empty
        txtTole.Text = String.Empty
        txtPhoneNo.Text = String.Empty
        txtmobileNo.Text = String.Empty
        txtEmail.Text = String.Empty

        txtCompletedBoard.Text = String.Empty
        txtCompletedLevel.Text = String.Empty
        txtCompletedStream.Text = String.Empty
        txtCompletedCourse.Text = String.Empty
        txtCompletedInstitutionName.Text = String.Empty
        txtCompletedEducatioalAddress.Text = String.Empty
        txtCompletedDivision_Or_Merit.Text = String.Empty
        dropDownCompletedGrade.Text = String.Empty
        txtCompleteDate.Text = String.Empty
        txtCompletedMarkPercentage.Text = String.Empty

        txtInstitutionalName.Text = String.Empty
        txtPreAddress.Text = String.Empty
        txtPreviousInstitutionEmail.Text = String.Empty
        txtPreviousPhone.Text = String.Empty
        txtPreviousPostbox.Text = String.Empty
        txtPreviousContactPerson.Text = String.Empty
        txtPreviousPosition.Text = String.Empty
        txtPreviousReason.Text = String.Empty


        txtFatherName.Text = String.Empty
        txtFatherOccupation.Text = String.Empty
        txtFatherMobile.Text = String.Empty
        txtFatherEmail.Text = String.Empty
        txtMotherName.Text = String.Empty
        txtMotherOccupation.Text = String.Empty
        txtMotherMobile.Text = String.Empty
        txtMotherEmail.Text = String.Empty

        txtGuardianName.Text = String.Empty
        txtGuardianAddress.Text = String.Empty
        txtGuardianOccupation.Text = String.Empty
        txtGuardianMobile.Text = String.Empty
        txtGuardianPhoneNo.Text = String.Empty
        txtGuardianEmail.Text = String.Empty
        txtRelationToStudent.Text = String.Empty

        txtHobbies.Text = String.Empty
        txtOccupation.Text = String.Empty
        txtRelative.Text = String.Empty
        txtAwards.Text = String.Empty
        txtNotes.Text = String.Empty
        txtResidenceNo.Text = String.Empty
        txtSponserBy.Text = String.Empty
        txtSchoolshipFor.Text = String.Empty

        txtHeight.Text = String.Empty
        txtWeight.Text = String.Empty
        txtHospitalName.Text = String.Empty
        txtDoctorName.Text = String.Empty
        dropBloodGroup.SelectedIndex = 0
        txtBloodPressure.Text = String.Empty
        txtAllergy.Text = String.Empty
        txtNutritition.Text = String.Empty
        txtHealthNotes.Text = String.Empty
        CheckIswearLens.Checked = False

    End Sub
End Class