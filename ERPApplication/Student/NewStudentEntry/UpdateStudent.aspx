﻿<%@ Page Title="Update Student Info" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="UpdateStudent.aspx.vb" Inherits="School.UpdateStudent_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../../assets/Calendar/jquery.calendars.picker.css">
    <script src="../../assets/Calendar/jquery.plugin.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js" type="text/javascript"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js" type="text/javascript"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
   <%-- <script src="../assets/Calendar/jquery.calendars.nepali.js"></script>--%>
    <script src="../../assets/Calendar/jquery.calendars.nepali.js" type="text/javascript"></script>


    <%--<style type='text/css'>
      /* Style to hide Dates / Months */
      .ui-datepicker-calendar,.ui-datepicker-month { display: none; }​
    </style>--%>
    <script type="text/javascript">
        function formatDate(date, type) {
            console.log("date : " + date + " ,type : " + type);
            var d = new Date(date);
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var year = d.getFullYear();
            var resultdate;
            if (type == "n") {
                resultdate = [day, month, year].join('/');
            }
            else {
                resultdate = [year, month, day].join('/');
            }
            console.log("formatted : " + resultdate);
            return resultdate;

        }



        function ChangeDate(value, obj, element) {
            value = formatDate(value, obj);
            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';
            debugger;
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/DateConverterNew',
                data: dataToSend,
                datatype: 'json',
                contentType: 'application/json',
                success: function (data) {
                    debugger;
                    var dd, mm, yy;
                    var value = [];
                    if (obj == "e") {                      
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                     
                        $("#<%=txtDateofBirth.ClientID %>").val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                    }
                    else if (obj == "n") {
                      
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];

                        $("#<%=txt_miti.ClientID %>").val(yy + "/" + mm + "/" + dd);
                    }
                }
            });

        }
    </script>
    <script type="text/javascript">

//        function ChangeDate(value, obj) {
//            //alert(value);
//            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';

//            $.ajax({
//                type: 'POST',
//                url: '/SetupServices.svc/ChangeDate',
//                data: dataToSend,
//                datatype: 'json',
//                contentType: 'application/json',
//                success: function (data) {
//                    if (obj == "e") {
//                        $("#<%=txtDateofBirth.ClientID %>").val(data);
//                    }
//                    else if (obj == "n") {
//                        $("#<%=txt_miti.ClientID %>").val(data);
//                    }
//                    //alert(data);
//                }
//            });

//        }
        $(document).ready(function () {
            $("#<%=txtRegistrationDate.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            $("#<%=txtDateofBirth.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
         
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {



                $("#<%=txtRegistrationDate.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                $("#<%=txtDateofBirth.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true, yearRange: '1950:2020' }).val();

               
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(function () {
                var calendar = $.calendars.instance('nepali');
//                $('#<%= txt_miti.ClientID %>').calendarsPicker({ calendar: calendar, dateFormat: "mm/dd/yyyy", onSelect: function (selectedDate) {
//                    // custom callback logic here
//                    // alert(selectedDate);
//                    ChangeDate(selectedDate, 'e');
//                }
//                });
            $('#<%= txt_miti.ClientID %>').calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                // custom callback logic here
                // alert(selectedDate);
                ChangeDate(selectedDate, 'e', this);
            }
            });
                //            $('#<%= txtCompleteDate.ClientID %>').calendarsPicker({ calendar: calendar, dateFormat: "yy" });

            });



        });
    </script>
    <script type="text/javascript">

        var checkFromValidation = false;

        $(document).ready(function () {

            $('#<%= txtRegistrationDate.ClientID %>').datepicker();
            $('#form').validate();


            $("#<%=txtApplicationFormNo.ClientID %>").rules('add', { required: true, number: false, messages: { required: 'Application Form number is required.', number: jQuery.format("Enter a number")} });
            $("#<%=txtCompletedMarkPercentage.ClientID %>").rules('add', { required: false, number: true, messages: { required: 'percent is required.', number: jQuery.format("Enter a number")} });
            $("#<%=txtFirstName.ClientID %>").rules('add', { required: true, minlength: 2, messages: { required: 'First Name is required.', minlength: jQuery.format("At least {0} character are required.")} });
            $("#<%=txtLastName.ClientID %>").rules('add', { required: true, minlength: 3, messages: { required: 'Last Name is required.', minlength: jQuery.format("At least {0} character are required.")} });
            $("#<%=txtAddress1.ClientID %>").rules('add', { required: true, minlength: 3, messages: { required: 'Address is required.', minlength: jQuery.format("At least {0} character are required.")} });
            $("#<%=dropDownMunicipal.ClientID %>").rules('add', { required: true, messages: { required: 'Urban/Rural Municipality Name is required.'} });
            $("#<%=txtmobileNo.ClientID %>").rules('add', { required: true, minlength: 10, messages: { required: 'Mobile Number is required.', minlength: jQuery.format("At least {0} character are required.")} });


            $("#<%=dropdownLevel.ClientID %>").rules('add', { required: true, messages: { required: 'Level is required.'} });
            $("#<%=dropFacultyList.ClientID %>").rules('add', { required: true, messages: { required: 'Stream is required.'} });
            $("#<%=dropProgrammeList.ClientID %>").rules('add', { required: true, messages: { required: 'Course is required.'} });
            $("#<%=dropDownBatch.ClientID %>").rules('add', { required: true, messages: { required: 'Batch is Required'} });
            $("#<%=dropDownSemester.ClientID %>").rules('add', { required: true, messages: { required: 'Semester is required'} });
            $("#<%=dropDownSection.ClientID %>").rules('add', { required: true, messages: { required: 'Section is required'} });
            //            $("#<%=txtRollNo.ClientID %>").rules('add', { required: true, messages: { required: 'RollNo is required'} });
            $("#<%=txtDateofBirth.ClientID %>").rules('add', { required: true, messages: { required: 'Date of Birth is required'} });
            checkFromValidation = function () {
                var bool = true;
                if ($('#<%=txtApplicationFormNo.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtCompletedMarkPercentage.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtFirstName.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtLastName.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtAddress1.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtmobileNo.ClientID %>').valid() == false) bool = false;
                if ($('#<%=dropDownMunicipal.ClientID %>').valid() == false) bool = false;


                if ($("#<%=dropdownLevel.ClientID %>").valid() == false) bool = false;
                if ($("#<%=dropFacultyList.ClientID %>").valid() == false) bool = false;
                if ($("#<%=dropProgrammeList.ClientID %>").valid() == false) bool = false;
                if ($("#<%=dropDownBatch.ClientID %>").valid() == false) bool = false;
                if ($("#<%=dropDownSemester.ClientID %>").valid() == false) bool = false;
                if ($("#<%=dropDownSection.ClientID %>").valid() == false) bool = false;
                //                if ($("#<%=txtRollNo.ClientID %>").valid() == false) bool = false;
                if ($("#<%=txtDateofBirth.ClientID %>").valid() == false) bool = false;

                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-content">
                <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                </button>
                                <h4 class="modal-title">
                                    Modal title</h4>
                            </div>
                            <div class="modal-body">
                                Widget settings form goes here
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn blue">
                                    Save changes</button>
                                <button type="button" class="btn default" data-dismiss="modal">
                                    Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="theme-panel hidden-xs hidden-sm hidden">
                    <div class="toggler">
                    </div>
                    <div class="toggler-close">
                    </div>
                    <div class="theme-options">
                        <div class="theme-option theme-colors clearfix">
                            <span>THEME COLOR</span>
                            <ul>
                                <li class="color-black current color-default" data-style="default"></li>
                                <li class="color-blue" data-style="blue"></li>
                                <li class="color-brown" data-style="brown"></li>
                                <li class="color-purple" data-style="purple"></li>
                                <li class="color-grey" data-style="grey"></li>
                                <li class="color-white color-light" data-style="light"></li>
                            </ul>
                        </div>
                        <div class="theme-option">
                            <span>Layout</span>
                            <select class="layout-option form-control input-small">
                                <option value="fluid" selected="selected">Fluid</option>
                                <option value="boxed">Boxed</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>Header</span>
                            <select class="header-option form-control input-small">
                                <option value="fixed" selected="selected">Fixed</option>
                                <option value="default">Default</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>Sidebar</span>
                            <select class="sidebar-option form-control input-small">
                                <option value="fixed">Fixed</option>
                                <option value="default" selected="selected">Default</option>
                            </select>
                        </div>
                        <div class="theme-option">
                            <span>Footer</span>
                            <select class="footer-option form-control input-small">
                                <option value="fixed">Fixed</option>
                                <option value="default" selected="selected">Default</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="page-title">
                            Update Student <small>Update student details</small>
                        </h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li class="btn-group">
                                <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                    data-delay="1000" data-close-others="true">
                                    <span>Actions</span> <i class="fa fa-angle-down"></i>
                                </button>
                                
                            </li>
                            <li><i class="fa fa-home"></i><a href="/index.aspx">Home</a> <i class="fa fa-angle-right">
                            </i></li>
                            <li><i class="fa fa-search"></i><a href="/Student/StudentList.aspx">Student Search</a><i class="fa fa-angle-right">
                            </i></li>
                            <li>Update Student</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                </div>
                <!-- Office Use Only--->
                <div class="row">
                    <div class="col-md-12 ">
                        <div id="Div14" runat="server">
                        </div>
                        <div id="Div11" runat="server">
                        </div>
                        <div id="message" runat="server">
                        </div>
                        <div id="Div12" runat="server">
                        </div>
                        <asp:HiddenField ID="hfStudentID" runat="server" />
                        <asp:HiddenField ID="hfSemester" runat="server" />
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Official Use Only</div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group" style="display: none;">
                                            <label class="col-md-3 control-label">
                                                Branch:</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropdownBranch" runat="server" class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%= dropdownBranch.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Application Form No. :<span style="color:Red;">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    <asp:TextBox ID="txtApplicationFormNo" runat="server" CssClass="form-control" placeholder="Enter Application Form No"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Registration No. :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtRegistrationNo" runat="server" CssClass="form-control" placeholder="Registraton Number generate automatically." ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Registration Date :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txtRegistrationDate" runat="server" CssClass="form-control" placeholder="Enter Registration Date" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Academic Year :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:DropDownList ID="dropAcademicYearList" runat="server" class="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                </div>
                <!--Academic Infrmation-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div id="Div2" runat="server">
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Academic Information</div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Level :<span style="color:Red;">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropdownLevel" CssClass="form-control" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Faculty/Stream :<span style="color:Red;">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropFacultyList" runat="server" class="form-control" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Course/Program :<span style="color:Red;">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropProgrammeList" runat="server" class="form-control" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Batch :<span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropDownBatch" runat="server" CssClass="form-control" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Semester :<span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropDownSemester" CssClass="form-control" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Section :<span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="dropDownSection" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <label class="col-md-3 control-label">
                                                Sub Group :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                                    <asp:DropDownList ID="ddlSubjectGroup" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Categories :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                    <asp:DropDownList ID="dropCategoriesList" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Is EDJ :</label>
                                            <div class="col-md-3">
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline">
                                                        <asp:CheckBox ID="CheckedIsEDJ" runat="server"></asp:CheckBox>
                                                        Tick/Mark if Student is EDJ
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                University Regd No. :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                                    <asp:TextBox ID="txtUniversityRegdNo" runat="server" CssClass="form-control" placeholder="Enter University Reg No"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Admission Status :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-flag-checkered"></i></span>
                                                    <asp:DropDownList ID="dropAdmissionStatusList" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Shift :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-vk"></i></span>
                                                    <asp:DropDownList ID="ddl_shift" runat="server" CssClass="form-control" placeholder="Enter Profession">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Roll No. :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtRollNo" runat="server" class="form-control" placeholder="Enter Roll No" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                              
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Student Personal Info
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                First Name :<span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="Enter First Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        
                                            <label class="col-md-3 control-label">
                                                Middle Name :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control" placeholder="Enter Middle Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Last Name :<span style="color:Red;">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Enter Second Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Gender :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                                    <asp:DropDownList ID="dropGender" runat="server" class="form-control">
                                                        <asp:ListItem Value="" Selected="False">--Select Gender--</asp:ListItem>
                                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                                        <asp:ListItem Value="Others">Others</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Date Of Birth :<span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txtDateofBirth" runat="server" class="form-control" placeholder="(dd MMM yyyy)Enter Date Of Birth"
                                                        onchange="ChangeDate(this.value,'n',this)" />
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Birth Miti :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txt_miti" runat="server" class="form-control" placeholder="(yyyy/mm/dd)Enter Date Of Birth" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Martial Status :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                                    <asp:DropDownList ID="dropMartialStatus" runat="server" class="form-control">
                                                        <asp:ListItem Value="" Selected="False">--Select Martial Status--</asp:ListItem>
                                                        <asp:ListItem Value="Single">Single</asp:ListItem>
                                                        <asp:ListItem Value="Married">Married</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Language :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:DropDownList ID="dropLanguageList" runat="server" class="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Religion :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-star"></i></span>
                                                    <asp:DropDownList ID="DropDownReligion" runat="server" class="form-control">
                                                        <asp:ListItem Value="" Selected="False">--Select Religion--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Nationality :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:DropDownList ID="dropNationality" runat="server" class="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Citizenship No :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    <asp:TextBox ID="txtCitizenshipNo" runat="server" CssClass="form-control" placeholder="Enter Citizenship No"></asp:TextBox>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Passport No :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    <asp:TextBox ID="txtPassportNo" runat="server" CssClass="form-control" placeholder="Enter Passport No"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Profession :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-vk"></i></span>
                                                    <asp:TextBox ID="txtProfession" runat="server" CssClass="form-control" placeholder="Enter Profession"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Status :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    <asp:DropDownList ID="dropDownStatus" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="">-- Select Student Status -- </asp:ListItem>
                                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactive</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Contact-->
               
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div id="Div4" runat="server">
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Contact Information
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Country :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                    <asp:DropDownList ID="dropCountry" runat="server" class="form-control">
                                                        <asp:ListItem Value="" Selected="False">--Select Country--</asp:ListItem>
                                                        <asp:ListItem Value="AF">Afghanistan</asp:ListItem>
                                                        <asp:ListItem Value="AL">Albania</asp:ListItem>
                                                        <asp:ListItem Value="DZ">Algeria</asp:ListItem>
                                                        <asp:ListItem Value="AS">American Samoa</asp:ListItem>
                                                        <asp:ListItem Value="AD">Andorra</asp:ListItem>
                                                        <asp:ListItem Value="AO">Angola</asp:ListItem>
                                                        <asp:ListItem Value="AI">Anguilla</asp:ListItem>
                                                        <asp:ListItem Value="AQ">Antarctica</asp:ListItem>
                                                        <asp:ListItem Value="AR">Argentina</asp:ListItem>
                                                        <asp:ListItem Value="AM">Armenia</asp:ListItem>
                                                        <asp:ListItem Value="AW">Aruba</asp:ListItem>
                                                        <asp:ListItem Value="AU">Australia</asp:ListItem>
                                                        <asp:ListItem Value="AT">Austria</asp:ListItem>
                                                        <asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
                                                        <asp:ListItem Value="BS">Bahamas</asp:ListItem>
                                                        <asp:ListItem Value="BH">Bahrain</asp:ListItem>
                                                        <asp:ListItem Value="BD">Bangladesh</asp:ListItem>
                                                        <asp:ListItem Value="BB">Barbados</asp:ListItem>
                                                        <asp:ListItem Value="BY">Belarus</asp:ListItem>
                                                        <asp:ListItem Value="BE">Belgium</asp:ListItem>
                                                        <asp:ListItem Value="BZ">Belize</asp:ListItem>
                                                        <asp:ListItem Value="BJ">Benin</asp:ListItem>
                                                        <asp:ListItem Value="BM">Bermuda</asp:ListItem>
                                                        <asp:ListItem Value="BT">Bhutan</asp:ListItem>
                                                        <asp:ListItem Value="BO">Bolivia</asp:ListItem>
                                                        <asp:ListItem Value="BA">Bosnia and Herzegowina</asp:ListItem>
                                                        <asp:ListItem Value="BW">Botswana</asp:ListItem>
                                                        <asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
                                                        <asp:ListItem Value="BR">Brazil</asp:ListItem>
                                                        <asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
                                                        <asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
                                                        <asp:ListItem Value="BG">Bulgaria</asp:ListItem>
                                                        <asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
                                                        <asp:ListItem Value="BI">Burundi</asp:ListItem>
                                                        <asp:ListItem Value="KH">Cambodia</asp:ListItem>
                                                        <asp:ListItem Value="CM">Cameroon</asp:ListItem>
                                                        <asp:ListItem Value="CA">Canada</asp:ListItem>
                                                        <asp:ListItem Value="CV">Cape Verde</asp:ListItem>
                                                        <asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
                                                        <asp:ListItem Value="CF">Central African Republic</asp:ListItem>
                                                        <asp:ListItem Value="TD">Chad</asp:ListItem>
                                                        <asp:ListItem Value="CL">Chile</asp:ListItem>
                                                        <asp:ListItem Value="CN">China</asp:ListItem>
                                                        <asp:ListItem Value="CX">Christmas Island</asp:ListItem>
                                                        <asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
                                                        <asp:ListItem Value="CO">Colombia</asp:ListItem>
                                                        <asp:ListItem Value="KM">Comoros</asp:ListItem>
                                                        <asp:ListItem Value="CG">Congo</asp:ListItem>
                                                        <asp:ListItem Value="CD">Congo, the Democratic Republic of the</asp:ListItem>
                                                        <asp:ListItem Value="CK">Cook Islands</asp:ListItem>
                                                        <asp:ListItem Value="CR">Costa Rica</asp:ListItem>
                                                        <asp:ListItem Value="CI">Cote d'Ivoire</asp:ListItem>
                                                        <asp:ListItem Value="HR">Croatia (Hrvatska)</asp:ListItem>
                                                        <asp:ListItem Value="CU">Cuba</asp:ListItem>
                                                        <asp:ListItem Value="CY">Cyprus</asp:ListItem>
                                                        <asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
                                                        <asp:ListItem Value="DK">Denmark</asp:ListItem>
                                                        <asp:ListItem Value="DJ">Djibouti</asp:ListItem>
                                                        <asp:ListItem Value="DM">Dominica</asp:ListItem>
                                                        <asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
                                                        <asp:ListItem Value="EC">Ecuador</asp:ListItem>
                                                        <asp:ListItem Value="EG">Egypt</asp:ListItem>
                                                        <asp:ListItem Value="SV">El Salvador</asp:ListItem>
                                                        <asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
                                                        <asp:ListItem Value="ER">Eritrea</asp:ListItem>
                                                        <asp:ListItem Value="EE">Estonia</asp:ListItem>
                                                        <asp:ListItem Value="ET">Ethiopia</asp:ListItem>
                                                        <asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
                                                        <asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
                                                        <asp:ListItem Value="FJ">Fiji</asp:ListItem>
                                                        <asp:ListItem Value="FI">Finland</asp:ListItem>
                                                        <asp:ListItem Value="FR">France</asp:ListItem>
                                                        <asp:ListItem Value="GF">French Guiana</asp:ListItem>
                                                        <asp:ListItem Value="PF">French Polynesia</asp:ListItem>
                                                        <asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
                                                        <asp:ListItem Value="GA">Gabon</asp:ListItem>
                                                        <asp:ListItem Value="GM">Gambia</asp:ListItem>
                                                        <asp:ListItem Value="GE">Georgia</asp:ListItem>
                                                        <asp:ListItem Value="DE">Germany</asp:ListItem>
                                                        <asp:ListItem Value="GH">Ghana</asp:ListItem>
                                                        <asp:ListItem Value="GI">Gibraltar</asp:ListItem>
                                                        <asp:ListItem Value="GR">Greece</asp:ListItem>
                                                        <asp:ListItem Value="GL">Greenland</asp:ListItem>
                                                        <asp:ListItem Value="GD">Grenada</asp:ListItem>
                                                        <asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
                                                        <asp:ListItem Value="GU">Guam</asp:ListItem>
                                                        <asp:ListItem Value="GT">Guatemala</asp:ListItem>
                                                        <asp:ListItem Value="GN">Guinea</asp:ListItem>
                                                        <asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
                                                        <asp:ListItem Value="GY">Guyana</asp:ListItem>
                                                        <asp:ListItem Value="HT">Haiti</asp:ListItem>
                                                        <asp:ListItem Value="HM">Heard and Mc Donald Islands</asp:ListItem>
                                                        <asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
                                                        <asp:ListItem Value="HN">Honduras</asp:ListItem>
                                                        <asp:ListItem Value="HK">Hong Kong</asp:ListItem>
                                                        <asp:ListItem Value="HU">Hungary</asp:ListItem>
                                                        <asp:ListItem Value="IS">Iceland</asp:ListItem>
                                                        <asp:ListItem Value="IN">India</asp:ListItem>
                                                        <asp:ListItem Value="ID">Indonesia</asp:ListItem>
                                                        <asp:ListItem Value="IR">Iran (Islamic Republic of)</asp:ListItem>
                                                        <asp:ListItem Value="IQ">Iraq</asp:ListItem>
                                                        <asp:ListItem Value="IE">Ireland</asp:ListItem>
                                                        <asp:ListItem Value="IL">Israel</asp:ListItem>
                                                        <asp:ListItem Value="IT">Italy</asp:ListItem>
                                                        <asp:ListItem Value="JM">Jamaica</asp:ListItem>
                                                        <asp:ListItem Value="JP">Japan</asp:ListItem>
                                                        <asp:ListItem Value="JO">Jordan</asp:ListItem>
                                                        <asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
                                                        <asp:ListItem Value="KE">Kenya</asp:ListItem>
                                                        <asp:ListItem Value="KI">Kiribati</asp:ListItem>
                                                        <asp:ListItem Value="KP">Korea, Democratic People's Republic of</asp:ListItem>
                                                        <asp:ListItem Value="KR">Korea, Republic of</asp:ListItem>
                                                        <asp:ListItem Value="KW">Kuwait</asp:ListItem>
                                                        <asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
                                                        <asp:ListItem Value="LA">Lao People's Democratic Republic</asp:ListItem>
                                                        <asp:ListItem Value="LV">Latvia</asp:ListItem>
                                                        <asp:ListItem Value="LB">Lebanon</asp:ListItem>
                                                        <asp:ListItem Value="LS">Lesotho</asp:ListItem>
                                                        <asp:ListItem Value="LR">Liberia</asp:ListItem>
                                                        <asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
                                                        <asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
                                                        <asp:ListItem Value="LT">Lithuania</asp:ListItem>
                                                        <asp:ListItem Value="LU">Luxembourg</asp:ListItem>
                                                        <asp:ListItem Value="MO">Macau</asp:ListItem>
                                                        <asp:ListItem Value="MK">Macedonia, The Former Yugoslav Republic of</asp:ListItem>
                                                        <asp:ListItem Value="MG">Madagascar</asp:ListItem>
                                                        <asp:ListItem Value="MW">Malawi</asp:ListItem>
                                                        <asp:ListItem Value="MY">Malaysia</asp:ListItem>
                                                        <asp:ListItem Value="MV">Maldives</asp:ListItem>
                                                        <asp:ListItem Value="ML">Mali</asp:ListItem>
                                                        <asp:ListItem Value="MT">Malta</asp:ListItem>
                                                        <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                                                        <asp:ListItem Value="MQ">Martinique</asp:ListItem>
                                                        <asp:ListItem Value="MR">Mauritania</asp:ListItem>
                                                        <asp:ListItem Value="MU">Mauritius</asp:ListItem>
                                                        <asp:ListItem Value="YT">Mayotte</asp:ListItem>
                                                        <asp:ListItem Value="MX">Mexico</asp:ListItem>
                                                        <asp:ListItem Value="FM">Micronesia, Federated States of</asp:ListItem>
                                                        <asp:ListItem Value="MD">Moldova, Republic of</asp:ListItem>
                                                        <asp:ListItem Value="MC">Monaco</asp:ListItem>
                                                        <asp:ListItem Value="MN">Mongolia</asp:ListItem>
                                                        <asp:ListItem Value="MS">Montserrat</asp:ListItem>
                                                        <asp:ListItem Value="MA">Morocco</asp:ListItem>
                                                        <asp:ListItem Value="MZ">Mozambique</asp:ListItem>
                                                        <asp:ListItem Value="MM">Myanmar</asp:ListItem>
                                                        <asp:ListItem Value="NA">Namibia</asp:ListItem>
                                                        <asp:ListItem Value="NR">Nauru</asp:ListItem>
                                                        <asp:ListItem Value="NP">Nepal</asp:ListItem>
                                                        <asp:ListItem Value="NL">Netherlands</asp:ListItem>
                                                        <asp:ListItem Value="AN">Netherlands Antilles</asp:ListItem>
                                                        <asp:ListItem Value="NC">New Caledonia</asp:ListItem>
                                                        <asp:ListItem Value="NZ">New Zealand</asp:ListItem>
                                                        <asp:ListItem Value="NI">Nicaragua</asp:ListItem>
                                                        <asp:ListItem Value="NE">Niger</asp:ListItem>
                                                        <asp:ListItem Value="NG">Nigeria</asp:ListItem>
                                                        <asp:ListItem Value="NU">Niue</asp:ListItem>
                                                        <asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
                                                        <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                                                        <asp:ListItem Value="NO">Norway</asp:ListItem>
                                                        <asp:ListItem Value="OM">Oman</asp:ListItem>
                                                        <asp:ListItem Value="PK">Pakistan</asp:ListItem>
                                                        <asp:ListItem Value="PW">Palau</asp:ListItem>
                                                        <asp:ListItem Value="PA">Panama</asp:ListItem>
                                                        <asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
                                                        <asp:ListItem Value="PY">Paraguay</asp:ListItem>
                                                        <asp:ListItem Value="PE">Peru</asp:ListItem>
                                                        <asp:ListItem Value="PH">Philippines</asp:ListItem>
                                                        <asp:ListItem Value="PN">Pitcairn</asp:ListItem>
                                                        <asp:ListItem Value="PL">Poland</asp:ListItem>
                                                        <asp:ListItem Value="PT">Portugal</asp:ListItem>
                                                        <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                                                        <asp:ListItem Value="QA">Qatar</asp:ListItem>
                                                        <asp:ListItem Value="RE">Reunion</asp:ListItem>
                                                        <asp:ListItem Value="RO">Romania</asp:ListItem>
                                                        <asp:ListItem Value="RU">Russian Federation</asp:ListItem>
                                                        <asp:ListItem Value="RW">Rwanda</asp:ListItem>
                                                        <asp:ListItem Value="KN">Saint Kitts and Nevis</asp:ListItem>
                                                        <asp:ListItem Value="LC">Saint LUCIA</asp:ListItem>
                                                        <asp:ListItem Value="VC">Saint Vincent and the Grenadines</asp:ListItem>
                                                        <asp:ListItem Value="WS">Samoa</asp:ListItem>
                                                        <asp:ListItem Value="SM">San Marino</asp:ListItem>
                                                        <asp:ListItem Value="ST">Sao Tome and Principe</asp:ListItem>
                                                        <asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
                                                        <asp:ListItem Value="SN">Senegal</asp:ListItem>
                                                        <asp:ListItem Value="SC">Seychelles</asp:ListItem>
                                                        <asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
                                                        <asp:ListItem Value="SG">Singapore</asp:ListItem>
                                                        <asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
                                                        <asp:ListItem Value="SI">Slovenia</asp:ListItem>
                                                        <asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
                                                        <asp:ListItem Value="SO">Somalia</asp:ListItem>
                                                        <asp:ListItem Value="ZA">South Africa</asp:ListItem>
                                                        <asp:ListItem Value="GS">South Georgia and the South Sandwich Islands</asp:ListItem>
                                                        <asp:ListItem Value="ES">Spain</asp:ListItem>
                                                        <asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
                                                        <asp:ListItem Value="SH">St. Helena</asp:ListItem>
                                                        <asp:ListItem Value="PM">St. Pierre and Miquelon</asp:ListItem>
                                                        <asp:ListItem Value="SD">Sudan</asp:ListItem>
                                                        <asp:ListItem Value="SR">Suriname</asp:ListItem>
                                                        <asp:ListItem Value="SJ">Svalbard and Jan Mayen Islands</asp:ListItem>
                                                        <asp:ListItem Value="SZ">Swaziland</asp:ListItem>
                                                        <asp:ListItem Value="SE">Sweden</asp:ListItem>
                                                        <asp:ListItem Value="CH">Switzerland</asp:ListItem>
                                                        <asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
                                                        <asp:ListItem Value="TW">Taiwan, Province of China</asp:ListItem>
                                                        <asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
                                                        <asp:ListItem Value="TZ">Tanzania, United Republic of</asp:ListItem>
                                                        <asp:ListItem Value="TH">Thailand</asp:ListItem>
                                                        <asp:ListItem Value="TG">Togo</asp:ListItem>
                                                        <asp:ListItem Value="TK">Tokelau</asp:ListItem>
                                                        <asp:ListItem Value="TO">Tonga</asp:ListItem>
                                                        <asp:ListItem Value="TT">Trinidad and Tobago</asp:ListItem>
                                                        <asp:ListItem Value="TN">Tunisia</asp:ListItem>
                                                        <asp:ListItem Value="TR">Turkey</asp:ListItem>
                                                        <asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
                                                        <asp:ListItem Value="TC">Turks and Caicos Islands</asp:ListItem>
                                                        <asp:ListItem Value="TV">Tuvalu</asp:ListItem>
                                                        <asp:ListItem Value="UG">Uganda</asp:ListItem>
                                                        <asp:ListItem Value="UA">Ukraine</asp:ListItem>
                                                        <asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
                                                        <asp:ListItem Value="GB">United Kingdom</asp:ListItem>
                                                        <asp:ListItem Value="US">United States</asp:ListItem>
                                                        <asp:ListItem Value="UM">United States Minor Outlying Islands</asp:ListItem>
                                                        <asp:ListItem Value="UY">Uruguay</asp:ListItem>
                                                        <asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
                                                        <asp:ListItem Value="VU">Vanuatu</asp:ListItem>
                                                        <asp:ListItem Value="VE">Venezuela</asp:ListItem>
                                                        <asp:ListItem Value="VN">Viet Nam</asp:ListItem>
                                                        <asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
                                                        <asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
                                                        <asp:ListItem Value="WF">Wallis and Futuna Islands</asp:ListItem>
                                                        <asp:ListItem Value="EH">Western Sahara</asp:ListItem>
                                                        <asp:ListItem Value="YE">Yemen</asp:ListItem>
                                                        <asp:ListItem Value="ZM">Zambia</asp:ListItem>
                                                        <asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                State :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-road"></i></span>
                                                     <asp:DropDownList ID="dropDownState" runat="server" CssClass="form-control" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    <%--<asp:TextBox ID="txtState" runat="server" class="form-control" placeholder="Enter Your State Name" />--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                District :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                    <asp:DropDownList ID="dropDownDistrict" runat="server" CssClass="form-control" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <label class="col-md-3 control-label">
                                                Zone :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                    <asp:DropDownList ID="dropDownZone" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Municipal : <span style="color:Red;">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:DropDownList ID="dropDownMunicipal" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                   <%-- <asp:TextBox ID="txtVDCMUL" runat="server" CssClass="form-control" placeholder="Enter VDC/MUNCIPALITY Name"></asp:TextBox>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Address1 : <span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control" placeholder="Enter Your First Address"></asp:TextBox>
                                                </div>
                                            </div>
                                        
                                            <label class="col-md-3 control-label">
                                                Address2 :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" placeholder="Enter Your Second Address "></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                City :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeholder="Enter  Your City Name"></asp:TextBox>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                PIN Code :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                                    <asp:TextBox ID="txtPINCode" runat="server" CssClass="form-control" placeholder="Enter PIN Code"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Road No :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-road"></i></span>
                                                    <asp:TextBox ID="txtRoadNo" runat="server" CssClass="form-control" placeholder="Enter Road No"></asp:TextBox>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Tole :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-road"></i></span>
                                                    <asp:TextBox ID="txtTole" runat="server" CssClass="form-control" placeholder="Enter Your Tole"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Phone No. :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="form-control" placeholder="Enter Your Phone Number" MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Mobile No. : <span style="color:Red;">*</span></label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                                    <asp:TextBox ID="txtmobileNo" runat="server" CssClass="form-control" placeholder="Enter Your Mobile Number" MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Email :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Enter Email Address"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Previous Institution-->
                
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div id="Div6" runat="server">
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Previous Institution
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Institutional Name :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                                    <asp:TextBox ID="txtInstitutionalName" runat="server" CssClass="form-control" placeholder="Enter institutional Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Pre-Address :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:TextBox ID="txtPreAddress" runat="server" CssClass="form-control" placeholder="Enter Pre-Address"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Previous Institution Email :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <asp:TextBox ID="txtPreviousInstitutionEmail" runat="server" CssClass="form-control"
                                                        placeholder="Enter Previous Institional Email"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Phone :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <asp:TextBox ID="txtPreviousPhone" runat="server" class="form-control" placeholder="Enter Phone Number" MaxLength="10"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Postbox No. :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <asp:TextBox ID="txtPreviousPostbox" runat="server" CssClass="form-control" placeholder="Enter Post Box No"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Contact Person :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtPreviousContactPerson" runat="server" CssClass="form-control"
                                                        placeholder="Enter Contact Person Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Position :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtPreviousPosition" runat="server" CssClass="form-control" placeholder="Enter Position"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Reason :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                    <asp:TextBox ID="txtPreviousReason" runat="server" CssClass="form-control" placeholder="Enter Reason"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Complete Education-->
                
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div id="Div8" runat="server">
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Complete Education
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Board :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtCompletedBoard" runat="server" class="form-control">
                                                 
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Level :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtCompletedLevel" CssClass="form-control" runat="server">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Course/Program :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtCompletedCourse" runat="server" class="form-control">
                                                 
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                               Faculty/Stream :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtCompletedStream" runat="server" class="form-control">
                                                 
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Institution Name :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                                    <asp:TextBox ID="txtCompletedInstitutionName" runat="server" CssClass="form-control"
                                                        placeholder="Enter Institution Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Educational Address :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-code"></i></span>
                                                    <asp:TextBox ID="txtCompletedEducatioalAddress" runat="server" CssClass="form-control"
                                                        placeholder="Enter Educational Address"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Division\Merit :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                                    <asp:TextBox ID="txtCompletedDivision_Or_Merit" runat="server" CssClass="form-control"
                                                        placeholder="Enter Division/Merit"></asp:TextBox>
                                                </div>
                                            </div>
                                      
                                            <label class="col-md-3 control-label">
                                                Grade :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                                    <asp:TextBox ID="dropDownCompletedGrade" runat="server" CssClass="form-control" placeholder="Enter Complete Grade"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Completed Year:</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txtCompleteDate" runat="server" CssClass="form-control" placeholder="Enter Completed Year"
                                                        MaxLength="4"></asp:TextBox>
                                                </div>
                                            </div>
                                        
                                            <label class="col-md-3 control-label">
                                               Percentage :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon">%</span>
                                                    <asp:TextBox ID="txtCompletedMarkPercentage" runat="server" CssClass="form-control"
                                                        placeholder="Enter Mark Percentage"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- User Login Detail-->
                
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div id="Div10" runat="server">
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Parent's Information</div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Father's Name :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtFatherName" runat="server" CssClass="form-control" placeholder="Enter Fathers Name"></asp:TextBox>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Father's Occupation :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtFatherOccupation" runat="server" CssClass="form-control" placeholder="Enter Fathers Occupation"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Father's Mobile No. :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                                    <asp:TextBox ID="txtFatherMobile" runat="server" CssClass="form-control" placeholder="Enter Fathers Mobile Number"
                                                        MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Father's Email :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <asp:TextBox ID="txtFatherEmail" runat="server" class="form-control" placeholder="Enter Fathes Email Address" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Father's Qualification :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlFQualification" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="" Selected>-- Select qualification --</asp:ListItem>
                                                        <asp:ListItem Value="primary">Primary education</asp:ListItem>
                                                        <asp:ListItem Value="secondary">Secondary education</asp:ListItem>
                                                        <asp:ListItem Value="underSLC">Under S.L.C</asp:ListItem>
                                                        <asp:ListItem Value="intermediate">Intermediate education</asp:ListItem>
                                                        <asp:ListItem Value="bachelor">Bachelor eduation</asp:ListItem>
                                                        <asp:ListItem Value="master">Master education</asp:ListItem>
                                                        <asp:ListItem Value="abovemaster">Above Master</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Mother's Name :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtMotherName" runat="server" class="form-control" placeholder="Enter Mothers Name" />
                                                </div>
                                            </div>
                                       
                                            <label class="col-md-3 control-label">
                                                Mother's Occupation :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtMotherOccupation" runat="server" class="form-control" placeholder="Enter Mothers Occupation" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Mother's Mobile No. :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                                    <asp:TextBox ID="txtMotherMobile" runat="server" class="form-control" placeholder="Enter Mothers Mobile Number"
                                                        MaxLength="10" />
                                                </div>
                                            </div>
                                        
                                            <label class="col-md-3 control-label">
                                                Mother's Email :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <asp:TextBox ID="txtMotherEmail" runat="server" class="form-control" placeholder="Enter Mothers Email Address" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Mother's Qualification :</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlMQualification" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="" Selected>-- Select qualification --</asp:ListItem>
                                                        <asp:ListItem Value="primary">Primary education</asp:ListItem>
                                                        <asp:ListItem Value="secondary">Secondary education</asp:ListItem>
                                                        <asp:ListItem Value="underSLC">Under S.L.C</asp:ListItem>
                                                        <asp:ListItem Value="intermediate">Intermediate education</asp:ListItem>
                                                        <asp:ListItem Value="bachelor">Bachelor eduation</asp:ListItem>
                                                        <asp:ListItem Value="master">Master education</asp:ListItem>
                                                        <asp:ListItem Value="abovemaster">Above Master</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Guardian's Information</div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Guardian Name :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtGuardianName" runat="server" CssClass="form-control" placeholder="Enter Guardian Name"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Address :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
                                                    <asp:TextBox ID="txtGuardianAddress" runat="server" CssClass="form-control" placeholder="Enter Guardian Address"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Occupation :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-hospital"></i></span>
                                                    <asp:TextBox ID="txtGuardianOccupation" runat="server" CssClass="form-control" placeholder="Enter Guardian Occupation"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Mobile No. :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                                    <asp:TextBox ID="txtGuardianMobile" runat="server" class="form-control" placeholder="Enter Mobile Number" MaxLength="10"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Phone No. :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <asp:TextBox ID="txtGuardianPhoneNo" runat="server" class="form-control" placeholder="Enter Phone Number" MaxLength="10"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Email :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <asp:TextBox ID="txtGuardianEmail" runat="server" class="form-control" placeholder="Enter Email Address" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Relation To Student :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtRelationToStudent" runat="server" class="form-control" placeholder="Enter Related To Student" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet box purple ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Additional Information</div>
                                <div class="tools">
                                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Hobbies :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-gavel"></i></span>
                                                    <asp:TextBox ID="txtHobbies" runat="server" CssClass="form-control" placeholder="Enter Hobbies"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Occupation :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-hospital"></i></span>
                                                    <asp:TextBox ID="txtOccupation" runat="server" CssClass="form-control" placeholder="Enter Occupation"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Relative :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtRelative" runat="server" CssClass="form-control" placeholder="Enter Realtives"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Residence No :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    <asp:TextBox ID="txtResidenceNo" runat="server" class="form-control" placeholder="Enter Residence" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Sponser By :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtSponserBy" runat="server" class="form-control" placeholder="Enter Spoonser By" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                SchoolshipFor :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:TextBox ID="txtSchoolshipFor" runat="server" class="form-control" placeholder="Enter SchoolshipFor" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Awards :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-certificate"></i></span>
                                                    <asp:TextBox ID="txtAwards" runat="server" class="form-control" placeholder="Enter Awards" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Notes :</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                    <asp:TextBox ID="txtNotes" runat="server" class="form-control" placeholder="Enter Notes" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div id="Div13" runat="server">
                                </div>
                                <div class="portlet box purple ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-reorder"></i>Health Information
                                        </div>
                                        <div class="tools">
                                            <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                            </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="form-horizontal">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Height :</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-signal"></i></span>
                                                            <asp:TextBox ID="txtHeight" runat="server" CssClass="form-control" placeholder="Enter Height"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                
                                                    <label class="col-md-3 control-label">
                                                        Weight :</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                            <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control" placeholder="Enter Weight"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Measurement Unit :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                            <asp:DropDownList ID="dropMeasurementUnit" runat="server" class="form-control">
                                                                <asp:ListItem Value="" Selected="False">--Select Measurement Unit--</asp:ListItem>
                                                                <asp:ListItem Value="feet">feet</asp:ListItem>
                                                                <asp:ListItem Value="meeter">meeter</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Hospital Name :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-hospital"></i></span>
                                                            <asp:TextBox ID="txtHospitalName" runat="server" class="form-control" placeholder="Enter Hospital Name" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Doctor Name :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                            <asp:TextBox ID="txtDoctorName" runat="server" CssClass="form-control" placeholder="Enter Doctors Name"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Blood Group :</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                                            <asp:DropDownList ID="dropBloodGroup" runat="server" class="form-control">
                                                                <asp:ListItem Value="" Selected="False">--Select Blood Group--</asp:ListItem>
                                                                <asp:ListItem Value="A+">A+</asp:ListItem>
                                                                <asp:ListItem Value="A-">A-</asp:ListItem>
                                                                <asp:ListItem Value="B+">B+</asp:ListItem>
                                                                <asp:ListItem Value="B-">B-</asp:ListItem>
                                                                <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                                                <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                                                <asp:ListItem Value="O+">O+</asp:ListItem>
                                                                <asp:ListItem Value="O-">O-</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                
                                                    <label class="col-md-3 control-label">
                                                        Blood Pressure :</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-code-fork"></i></span>
                                                            <asp:TextBox ID="txtBloodPressure" runat="server" CssClass="form-control" placeholder="Enter text"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Allergy :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                            <asp:TextBox ID="txtAllergy" runat="server" CssClass="form-control" placeholder="Enter Allergy"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Nutritition :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                            <asp:TextBox ID="txtNutritition" runat="server" CssClass="form-control" placeholder="Enter Nutritition"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Health Notes :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                            <asp:TextBox ID="txtHealthNotes" runat="server" CssClass="form-control" placeholder="Enter Health Notes"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Whether Student Wear Lens :</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-check"></i></span>
                                                            <asp:CheckBox type="checkbox" ID="CheckIswearLens" runat="server" CssClass="form-control"
                                                                Text="Is Wear Lens" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions fluid">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <asp:Button ID="btnUpdate" class="btn purple" runat="server" Text="Update" OnClientClick="return checkFromValidation();"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
