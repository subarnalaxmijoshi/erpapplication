﻿Imports System.IO
Imports System.Collections.Generic
Imports AjaxControlToolkit
Public Class Admission_New
    Inherits System.Web.UI.Page
    Private Dao As New DatabaseDao
    Dim sql As String

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim currentUri = Request.Url.AbsolutePath
    '    Dim pa As New PageAuthority
    '    Dim uid As String
    '    If (Session("userId") <> Nothing) Then
    '        uid = Session("userID").ToString()
    '    Else
    '        Response.Redirect("/Logout.aspx")
    '    End If
    '    Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
    '    If Not hasRightToView Then
    '        Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
    '        Response.Redirect(redirectUrl)
    '        UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
    '    Else
    '        UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
    '    End If


    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrWhiteSpace(Session("userID")) And
           String.IsNullOrWhiteSpace(Session("username")) And
           String.IsNullOrWhiteSpace(Session("Role")) And
           String.IsNullOrWhiteSpace(Session("Branch")) Then
            Response.Redirect("/Login.aspx")
        End If

        If (AjaxFileUpload1.IsInFileUploadPostBack) Then

        Else
            If Not IsPostBack Then
                Try
                    GetAcademicYear()
                    GetCategories()
                    GetLangauge()
                    GetReligion()
                    GetNationality()
                    GetShift()

                    GetBoard()
                    GetLevelByBoard(ddlBoard.SelectedValue.ToString())
                    GetStreamByLevelId(ddlLevel.SelectedValue.ToString())
                    GetProgrammeApliedFor(ddlFaculty.SelectedValue.ToString())
                    GetBatchByCourse(ddlCourse.SelectedValue.ToString())
                    GetSubjectGroupByCourse(ddlCourse.SelectedValue.ToString())
                    GetSemesterByBatch(ddlBatch.SelectedValue.ToString())
                    GetSectionBySemester(ddlSemester.SelectedValue.ToString())
                    GetAdmissionStatus()

                    dropCountry.Items.FindByValue("NP").Selected = True
                    GetProvince()
                    GetZoneByState(dropDownState.SelectedValue.ToString)
                    GetDistrictByState(dropDownState.SelectedValue.ToString)
                    GetMunicipalByDistrict(dropDownDistrict.SelectedValue.ToString)


                    ddl_shift.SelectedIndex = ddl_shift.Items.IndexOf(ddl_shift.Items.FindByText("Day"))
                    'ddl_shift.Items.FindByValue("1").Selected = True
                    dropMartialStatus.Items.FindByValue("Single").Selected = True
                    dropLanguageList.Items.FindByValue("15").Selected = True
                    DropDownReligion.Items.FindByValue("27").Selected = True
                    dropNationality.Items.FindByValue("Nepali").Selected = True

                    ddlBoard.SelectedIndex = 1
                    ddlBoard_SelectedIndexChanged(sender, e)
                    ddlLevel.SelectedIndex = 1
                    ddlLevel_SelectedIndexChanged(sender, e)

                Catch ex As Exception

                End Try
            End If
        End If
    End Sub

    Protected Function getSessionBranchID() As String
        Return Session("BranchID").ToString
    End Function

    Private Sub GetAcademicYear()
        Dim sql As String
        dropAcademicYearList.Items.Clear()
        sql = "exec [Setup].[usp_AcademicYear] @flag='s' ,@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropAcademicYearList.DataSource = ds.Tables(0)
                dropAcademicYearList.DataTextField = "AcademicTitle"
                dropAcademicYearList.DataValueField = "AcademicYearId"
                dropAcademicYearList.DataBind()
                dropAcademicYearList.Items.Insert(0, New ListItem("-- Select Academic Year -- ", ""))

            Else
                dropAcademicYearList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetCategories()
        Dim sql As String

        dropCategoriesList.Items.Clear()
        sql = "exec [Setup].[usp_Categories] @flag='s',@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropCategoriesList.DataSource = ds.Tables(0)
                dropCategoriesList.DataTextField = "CategoriesTitle"

                dropCategoriesList.DataValueField = "CategoriesID"
                dropCategoriesList.DataBind()
                dropCategoriesList.Items.Insert(0, New ListItem("-- Select Category -- ", ""))
            Else
                dropCategoriesList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetLangauge()
        Dim sql As String

        dropLanguageList.Items.Clear()
        sql = "exec [Setup].[usp_Language] @flag='s',@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropLanguageList.DataSource = ds.Tables(0)
                dropLanguageList.DataTextField = "Language"

                dropLanguageList.DataValueField = "LanguageID"
                dropLanguageList.DataBind()
                dropLanguageList.Items.Insert(0, New ListItem("-- Select Language -- ", ""))
            Else
                dropLanguageList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetReligion()
        Dim sql As String
        DropDownReligion.Items.Clear()
        sql = "exec [Setup].[usp_Religious] @flag='s' ,@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                DropDownReligion.DataSource = ds.Tables(0)
                DropDownReligion.DataTextField = "ReligionName"
                DropDownReligion.DataValueField = "ReligionID"
                DropDownReligion.DataBind()
                DropDownReligion.Items.Insert(0, New ListItem("-- Select Religion Name -- ", ""))

            Else
                DropDownReligion.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetNationality()
        Dim sql As String

        dropNationality.Items.Clear()
        sql = "exec [Setup].[usp_Nationality] @flag='s' "
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropNationality.DataSource = ds.Tables(0)
                dropNationality.DataTextField = "Nationality"

                dropNationality.DataValueField = "Nationality"
                dropNationality.DataBind()
                dropNationality.Items.Insert(0, New ListItem("-- Select Nationality -- ", ""))
            Else
                dropNationality.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetShift()
        Dim sql As String
        ddl_shift.Items.Clear()
        sql = "exec [HR].[usp_Shift]  @flag='s',@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddl_shift.DataSource = ds.Tables(0)
                ddl_shift.DataTextField = "ShiftName"
                ddl_shift.DataValueField = "ShiftID"
                ddl_shift.DataBind()
                ddl_shift.Items.Insert(0, New ListItem("-- Select Shift Name -- ", ""))

            Else
                ddl_shift.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetBoard()
        Dim sql As String
        ddlBoard.Items.Clear()
        sql = "exec [Setup].[usp_Board] @flag='p',@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlBoard.DataSource = ds.Tables(0)
                ddlBoard.DataTextField = "BoardTitle"
                ddlBoard.DataValueField = "BoardID"
                ddlBoard.DataBind()
                ddlBoard.Items.Insert(0, New ListItem("-- Select Board --", ""))
            Else
                ddlBoard.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetLevelByBoard(ByVal boardId As String)
        Dim sql As String
        ddlLevel.Items.Clear()
        sql = "exec [Setup].[usp_Level] @Flag='b', @BoardID='" & boardId & "'"
        Dim ds As New DataSet
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlLevel.DataSource = ds.Tables(0)
                ddlLevel.DataTextField = "LevelTitle"
                ddlLevel.DataValueField = "LevelId"
                ddlLevel.DataBind()
                ddlLevel.Items.Insert(0, New ListItem("-- Select Level -- ", ""))
            Else
                ddlLevel.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetStreamByLevelId(ByVal levelID As String)
        Dim sql As String


        ddlFaculty.Items.Clear()
        sql = "exec [Setup].[usp_Faculty] @flag='l', @LevelId='" & levelID & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlFaculty.DataSource = ds.Tables(0)
                ddlFaculty.DataTextField = "FacultyTitle"

                ddlFaculty.DataValueField = "FacultyID"
                ddlFaculty.DataBind()
                ddlFaculty.Items.Insert(0, New ListItem("-- Select Faculty -- ", ""))
            Else
                ddlFaculty.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetProgrammeApliedFor(ByVal facultyID As String)
        Dim sql As String
        ddlCourse.Items.Clear()
        sql = "exec [Setup].[usp_Course] @flag='g', @FacultyID='" & facultyID & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCourse.DataSource = ds.Tables(0)
                ddlCourse.DataTextField = "CourseTitle"

                ddlCourse.DataValueField = "CourseID"
                ddlCourse.DataBind()
                ddlCourse.Items.Insert(0, New ListItem("-- Select Programe Applied For -- ", ""))
            Else
                ddlCourse.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetBatchByCourse(ByVal courseId As String)
        Dim sql As String
        ddlBatch.Items.Clear()
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Batch] @Flag='c', @CourseID='" & courseId & "',@BranchID=" & Session("BranchID")
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlBatch.DataSource = ds.Tables(0)
                ddlBatch.DataTextField = "BatchTitle"
                ddlBatch.DataValueField = "BatchID"
                ddlBatch.DataBind()
                ddlBatch.Items.Insert(0, New ListItem("-- Select Batch -- ", ""))
            Else
                ddlBatch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSubjectGroupByCourse(ByVal courseId As String)
        Dim sql As String
        ddlSubjectGroup.Items.Clear()
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_SubjectGroup] @Flag='a', @CourseID='" & courseId & "',@BranchID=" & Session("BranchID")
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSubjectGroup.DataSource = ds.Tables(0)
                ddlSubjectGroup.DataTextField = "SubjectGroup"
                ddlSubjectGroup.DataValueField = "SubjectGroupID"
                ddlSubjectGroup.DataBind()
                ddlSubjectGroup.Items.Insert(0, New ListItem("-- Select Subject Group -- ", ""))
            Else
                ddlSubjectGroup.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSemesterByBatch(ByVal batchID As String)
        Dim sql As String
        ddlSemester.Items.Clear()
        sql = "exec [Setup].[usp_Semester] @flag='b', @BatchID='" & batchID & "',@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSemester.DataSource = ds.Tables(0)
                ddlSemester.DataTextField = "SemesterName"
                ddlSemester.DataValueField = "SemesterID"
                ddlSemester.DataBind()
                ddlSemester.Items.Insert(0, New ListItem("-- Select Semester -- ", ""))
            Else
                ddlSemester.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSectionBySemester(ByVal semesterID As String)
        Dim sql As String

        ddlSection.Items.Clear()
        sql = "exec [Setup].[usp_Section] @flag='a', @SemesterID='" & semesterID & "'"
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "Section"
                ddlSection.DataValueField = "SectionID"
                ddlSection.DataBind()
                ddlSection.Items.Insert(0, New ListItem("-- Select Section -- ", ""))
            Else
                ddlSection.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetAdmissionStatus()
        Dim sql As String

        dropAdmissionStatusList.Items.Clear()
        sql = "exec [Setup].[usp_AdmissionStatus] @flag='s',@BranchID=" & Session("BranchID")
        Dim ds As DataSet

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropAdmissionStatusList.DataSource = ds.Tables(0)
                dropAdmissionStatusList.DataTextField = "AdmissionEligibleStatusTitle"
                dropAdmissionStatusList.DataValueField = "AdmissionStatusID"
                dropAdmissionStatusList.DataBind()
                dropAdmissionStatusList.Items.Insert(0, New ListItem("-- Select Admission Status -- ", ""))
            Else
                dropAdmissionStatusList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlBoard_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBoard.SelectedIndexChanged
        GetLevelByBoard(ddlBoard.SelectedValue.ToString())
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLevel.SelectedIndexChanged
        GetStreamByLevelId(ddlLevel.SelectedValue.ToString())
    End Sub

    Protected Sub ddlFaculty_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFaculty.SelectedIndexChanged
        GetProgrammeApliedFor(ddlFaculty.SelectedValue.ToString())
    End Sub

    Protected Sub ddlCourse_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCourse.SelectedIndexChanged
        GetBatchByCourse(ddlCourse.SelectedValue.ToString())
        GetSubjectGroupByCourse(ddlCourse.SelectedValue.ToString())
    End Sub

    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBatch.SelectedIndexChanged
        GetSemesterByBatch(ddlBatch.SelectedValue.ToString())
    End Sub

    Protected Sub ddlSemester_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSemester.SelectedIndexChanged
        GetSectionBySemester(ddlSemester.SelectedValue.ToString())
    End Sub


    Private Sub GetProvince()
        Dim sql As String
        Dim ds As DataSet
        sql = "exec [Setup].[usp_Province]"
        dropDownState.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownState.DataSource = ds.Tables(0)
                dropDownState.DataTextField = "ProvinceName"
                dropDownState.DataValueField = "ProvinceID"
                dropDownState.DataBind()
                dropDownState.Items.Insert(0, New ListItem("-- Select State --", ""))
            Else
                dropDownState.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetZoneByState(ByVal provinceId As String)
        Dim sql As String
        Dim ds As DataSet
        dropDownZone.Items.Clear()
        sql = "exec [Setup].[usp_Zone] @flag='s',@ProvinceID='" & provinceId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownZone.DataSource = ds.Tables(0)
                dropDownZone.DataTextField = "Zone"
                dropDownZone.DataValueField = "ZoneID"
                dropDownZone.DataBind()
                dropDownZone.Items.Insert(0, New ListItem("-- Select Zone --", ""))
            Else
                dropDownZone.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetDistrictByState(ByVal provinceId As String)
        Dim sql As String
        Dim ds As New DataSet

        sql = "exec [Setup].[usp_District] @flag='s' ,@ProvinceID='" & provinceId & "'"
        dropDownDistrict.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownDistrict.DataSource = ds.Tables(0)
                dropDownDistrict.DataTextField = "DistrictName"
                dropDownDistrict.DataValueField = "DistrictID"
                dropDownDistrict.DataBind()
                dropDownDistrict.Items.Insert(0, New ListItem("-- Select District --", ""))
            Else
                dropDownDistrict.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetMunicipalByDistrict(ByVal districtId As String)
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Municipality]  @flag='s',@DistrictID='" & districtId & "'"
        dropDownMunicipal.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                dropDownMunicipal.DataSource = ds.Tables(0)
                dropDownMunicipal.DataTextField = "Municipality"
                dropDownMunicipal.DataValueField = "MunicipalityID"
                dropDownMunicipal.DataBind()
                dropDownMunicipal.Items.Insert(0, New ListItem("-- Select Municipality --", ""))
            Else
                dropDownMunicipal.Items.Insert(0, New ListItem("-- No Record Found --", ""))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dropDownState_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropDownState.SelectedIndexChanged
        GetZoneByState(dropDownState.SelectedValue)
        GetDistrictByState(dropDownState.SelectedValue)
    End Sub

    Protected Sub dropDownDistrict_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropDownDistrict.SelectedIndexChanged
        GetMunicipalByDistrict(dropDownDistrict.SelectedValue)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        SaveStudent(txtRegistrationNo.Text, txtRegistrationDate.Text,
                   dropAcademicYearList.SelectedValue.ToString(), txtApplicationFormNo.Text,
                   ddlBatch.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString(),
                   ddlFaculty.SelectedValue.ToString().Trim, dropCategoriesList.SelectedValue.ToString().Trim,
                   txtUniversityRegdNo.Text, dropAdmissionStatusList.SelectedValue.ToString(),
                   txtRollNo.Text, txtFirstName.Text, txtMiddleName.Text, txtLastName.Text,
                   dropGender.SelectedValue.ToString(), txtDateofBirth.Text,
                   dropMartialStatus.SelectedValue.ToString(), dropLanguageList.SelectedValue.ToString(), dropNationality.SelectedValue.ToString, DropDownReligion.SelectedValue.ToString,
                    txtCitizenshipNo.Text, txtPassportNo.Text, txtProfession.Text,
                   txtAddress1.Text, txtAddress2.Text, txtCity.Text, dropDownState.SelectedValue.ToString().Trim,
                   txtPINCode.Text, dropCountry.SelectedValue.ToString(), txtRoadNo.Text,
                   dropDownZone.SelectedValue.ToString().Trim, dropDownDistrict.SelectedValue.ToString().Trim,
                   dropDownMunicipal.SelectedValue.ToString().Trim, txtTole.Text, txtPhone.Text,
                   txtmobileNo.Text, txtEmail.Text, txtInstitutionName.Text, txtPreAddress.Text,
                   txtPreviousInstitutionEmail.Text, txtPhone.Text, txtPostboxNo.Text, txtContactPerson.Text,
                   txtPosition.Text, txtReason.Text, txtLevel.Text, txtCorse.Text,
                   txtBoard.Text, txtStream.Text, txtInstitutionName.Text, txtEducatioalAddress.Text,
                   txtDivision_Or_Merit.Text, txtGrade.Text, txtCompleteDate.Text,
                   txtMarkPercentage.Text, ddlLevel.SelectedValue.ToString(), ddlSemester.SelectedValue.ToString(),
                   ddlSection.SelectedValue.ToString(), Session("Branch"), txtDateofBirth_Nep.Text.Trim, Session("BranchID"), Session("CompanyID"), Session("userID"),
                   txtFatherName.Text, txtFatherOccupation.Text, txtFatherMobile.Text, txtFatherEmail.Text, txtMotherName.Text, txtMotherOccupation.Text, txtMotherMobile.Text, txtMotherEmail.Text, txtGuardianName.Text, txtGuardianAddress.Text, txtGuardianOccupation.Text, txtGuardianMobileNo.Text, txtGuardianEmail.Text, txtRelationToStudent.Text, txtGuardianPhoneNo.Text,
                   txtHobbies.Text, txtOccupation.Text, txtRelative.Text, txtResidenceNo.Text, txtSponserBy.Text, txtSchoolshipFor.Text, txtAwards.Text, txtNotes.Text, txtHeight.Text, txtWeight.Text, dropMeasurementUnit.Text.ToString(), txtHospitalName.Text, txtDoctorName.Text, dropBloodGroup.Text.ToString(), txtBloodPressure.Text, txtAllergy.Text, txtNutritition.Text, txtHealthNotes.Text, IIf(CheckIswearLens.Checked, True, False)
               )
    End Sub

    Private Sub SaveStudent(ByVal registrationNo As String, ByVal registrationDate As String, ByVal academicYear As String,
                            ByVal applicationFormNo As String, ByVal batch As String, ByVal programAppliedFor As String,
                            ByVal faculty As String, ByVal categories As String,
                            ByVal universityRegNo As String, ByVal admissionStatus As String, ByVal rollNo As String,
                            ByVal firstName As String, ByVal middleName As String, ByVal lastName As String,
                            ByVal gender As String, ByVal dateOfBirth As String, ByVal maritalStatus As String,
                            ByVal language As String, ByVal nationality As String, ByVal religion As String, ByVal citizenshipNo As String,
                            ByVal passportNo As String, ByVal profession As String, ByVal address1 As String, ByVal address2 As String, ByVal city As String,
                            ByVal state As String, ByVal pinCode As String, ByVal country As String, ByVal roadNo As String,
                            ByVal zone As String, ByVal district As String, ByVal vdcMul As String, ByVal tole As String,
                            ByVal phoneNo As String, ByVal mobileNo As String, ByVal email As String, ByVal institutionalName As String,
                            ByVal preAddress As String, ByVal previousInstitutionEmail As String, ByVal phone As String,
                            ByVal postboxNo As String, ByVal contactPerson As String, ByVal position As String,
                            ByVal reason As String, ByVal level As String, ByVal course As String, ByVal board As String,
                            ByVal stream As String, ByVal institutionName As String, ByVal educationalAddress As String,
                            ByVal deivisionorMerit As String, ByVal grade As String, ByVal completeDate As String,
                            ByVal markPercentage As String,
                            ByVal currentLevel As String, ByVal semesterId As String, ByVal section As String,
                            ByVal branchCode As String, ByVal Birthmiti As String, ByVal branchId As Integer, ByVal companyId As Integer, ByVal UserId As String,
                            ByVal FatherName As String, ByVal FOccupation As String, ByVal FMobile As String,
                            ByVal FEmail As String, ByVal MotherName As String,
                            ByVal MOccupation As String, ByVal MMobile As String,
                            ByVal MEmail As String,
                            ByVal GName As String, ByVal GAddress As String, ByVal GOccupation As String,
                            ByVal GMobileNo As String, ByVal GEmail As String, ByVal RelationToStudent As String,
                            ByVal GPhoneNo As String,
                            ByVal Hobbies As String, ByVal Occupation As String, ByVal Relative As String,
                            ByVal ResidenceNo As String, ByVal SponserBy As String, ByVal SchoolshipFor As String,
                            ByVal Awards As String, ByVal Notes As String,
                            ByVal Height As String, ByVal Weight As String, ByVal MeasurementUnit As String,
                            ByVal HospitalName As String, ByVal DoctorName As String, ByVal BloodGroup2 As String,
                            ByVal BloodPressure As String, ByVal Allergy As String, ByVal Nutritition As String,
                            ByVal HealthNotes As String, ByVal IswearLans As String
                            )

        message.InnerHtml = ""
        applicationFormNo = Dao.FilterQuote(applicationFormNo)
        registrationNo = Dao.FilterQuote(registrationNo)
        registrationDate = DateTime.Parse(registrationDate.Replace("'", "''"))

        academicYear = Dao.FilterQuote(academicYear)
        firstName = Dao.FilterQuote(firstName)
        middleName = Dao.FilterQuote(middleName)
        lastName = Dao.FilterQuote(lastName)
        gender = Dao.FilterQuote(gender)
        categories = Dao.FilterQuote(categories)

        '17/10/2017
        If Not String.IsNullOrWhiteSpace(dateOfBirth) Then
            'Dim miti As String() = dateOfBirth.Split("/")
            'Dim mydatetime As Date = miti(1) & "/" & miti(0) & " /" & miti(2)
            dateOfBirth = dateOfBirth
        Else
            dateOfBirth = Date.Now
        End If
        dateOfBirth = dateOfBirth.Replace("'", "''")

        maritalStatus = Dao.FilterQuote(maritalStatus)
        language = Dao.FilterQuote(language)
        nationality = Dao.FilterQuote(nationality)
        religion = Dao.FilterQuote(religion)
        citizenshipNo = Dao.FilterQuote(citizenshipNo)
        passportNo = Dao.FilterQuote(passportNo)
        profession = Dao.FilterQuote(profession)

        board = Dao.FilterQuote(board)
        level = Dao.FilterQuote(level)
        faculty = Dao.FilterQuote(faculty)
        programAppliedFor = Dao.FilterQuote(programAppliedFor)
        batch = Dao.FilterQuote(batch)
        semesterId = Dao.FilterQuote(semesterId)
        section = Dao.FilterQuote(section)
        rollNo = Dao.FilterQuote(rollNo)
        admissionStatus = Dao.FilterQuote(admissionStatus)
        universityRegNo = Dao.FilterQuote(universityRegNo)


        country = Dao.FilterQuote(country)
        state = Dao.FilterQuote(state)
        zone = Dao.FilterQuote(zone)
        district = Dao.FilterQuote(district)
        vdcMul = Dao.FilterQuote(vdcMul)
        address1 = Dao.FilterQuote(address1)
        address2 = Dao.FilterQuote(address2)
        city = Dao.FilterQuote(city)
        pinCode = Dao.FilterQuote(pinCode)
        roadNo = Dao.FilterQuote(roadNo)
        tole = Dao.FilterQuote(tole)
        phoneNo = Dao.FilterQuote(phoneNo)
        mobileNo = Dao.FilterQuote(mobileNo)
        email = Dao.FilterQuote(email)

        institutionalName = Dao.FilterQuote(institutionalName)
        preAddress = Dao.FilterQuote(preAddress)
        previousInstitutionEmail = Dao.FilterQuote(previousInstitutionEmail)
        phone = Dao.FilterQuote(phone)
        postboxNo = Dao.FilterQuote(postboxNo)
        contactPerson = Dao.FilterQuote(contactPerson)
        position = Dao.FilterQuote(position)
        reason = Dao.FilterQuote(reason)

        level = Dao.FilterQuote(level)
        stream = Dao.FilterQuote(stream)
        institutionName = Dao.FilterQuote(institutionName)
        educationalAddress = Dao.FilterQuote(educationalAddress)
        deivisionorMerit = Dao.FilterQuote(deivisionorMerit)
        grade = Dao.FilterQuote(grade)
        completeDate = Dao.FilterQuote(completeDate)
        markPercentage = Dao.FilterQuote(markPercentage)
       
        currentLevel = Dao.FilterQuote(currentLevel)     
        branchCode = Dao.FilterQuote(branchCode)
        Birthmiti = Dao.FilterQuote(Birthmiti)

        email = Dao.FilterQuote(email)
        phoneNo = Dao.FilterQuote(phoneNo)

        FatherName = Dao.FilterQuote(FatherName)
        FOccupation = Dao.FilterQuote(FOccupation)
        FMobile = Dao.FilterQuote(FMobile)
        FEmail = Dao.FilterQuote(FEmail)
        Dim FQualification As String = Dao.FilterQuote(ddlFQualification.SelectedValue)

        MotherName = Dao.FilterQuote(MotherName)
        MOccupation = Dao.FilterQuote(MOccupation)
        MMobile = Dao.FilterQuote(MMobile)
        MEmail = Dao.FilterQuote(MEmail)
        Dim MQualification As String = Dao.FilterQuote(ddlMQualification.SelectedValue)

        GName = Dao.FilterQuote(GName)
        GAddress = Dao.FilterQuote(GAddress)
        GOccupation = Dao.FilterQuote(GOccupation)
        GMobileNo = Dao.FilterQuote(GMobileNo)
        GEmail = Dao.FilterQuote(GEmail)
        RelationToStudent = Dao.FilterQuote(RelationToStudent)
        GPhoneNo = Dao.FilterQuote(GPhoneNo)

        Hobbies = Dao.FilterQuote(Hobbies)
        Occupation = Dao.FilterQuote(Occupation)
        Relative = Dao.FilterQuote(Relative)
        Awards = Dao.FilterQuote(Awards)
        Notes = Dao.FilterQuote(Notes)
        ResidenceNo = Dao.FilterQuote(ResidenceNo)
        SponserBy = Dao.FilterQuote(SponserBy)
        SchoolshipFor = Dao.FilterQuote(SchoolshipFor)

        Height = Dao.FilterQuote(Height)
        Weight = Dao.FilterQuote(Weight)
        HospitalName = Dao.FilterQuote(HospitalName)
        DoctorName = Dao.FilterQuote(DoctorName)
        BloodGroup2 = Dao.FilterQuote(BloodGroup2)
        BloodPressure = Dao.FilterQuote(BloodPressure)
        Allergy = Dao.FilterQuote(Allergy)
        Nutritition = Dao.FilterQuote(Nutritition)
        HealthNotes = Dao.FilterQuote(HealthNotes)
        IswearLans = Dao.FilterQuote(IswearLans)

        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")

        If (txtApplicationFormNo.Text = "") Then
            txtApplicationFormNo.Focus()
        Else
            Try
                sql = "exec Students.usp_AdmissionNew @RegistrationNumber='" & registrationNo & "', @RegistrationDate='" & registrationDate & _
                    "', @AcademicYear='" & academicYear & "', @ApplicationFormNo='" & applicationFormNo & "',  @Batch='" & batch & _
                    "', @ProgrammeAppliedFor='" & programAppliedFor & "',@Faculty='" & faculty & "',@Categories='" & categories & _
                    "', @UniversityRegdNo='" & universityRegNo & "', @AdmissionStatus='" & admissionStatus & "', @RollNo='" & rollNo & _
                    "', @FirstName='" & firstName & "' ,@MiddleName='" & middleName & "', @LastName='" & lastName & "', @Gender='" & gender & _
                    "', @DateOfBirth= '" & dateOfBirth & "',@MaritalStatus='" & maritalStatus & "',@BloodGroup='',@Language='" & language & _
                    "',@Religion='" & religion & "', @Nationality='" & nationality & "', @CitizenshipNo='" & citizenshipNo & "',@PassportNo='" & passportNo & "', @Profession='" & profession & "', @Country='" & country & _
                    "',@State='" & state & "',@Zone='" & zone & "',@District='" & district & "',@VDC_MUL='" & vdcMul & "',@Address1='" & address1 & _
                    "', @Address2='" & address2 & "',@City='" & city & "',@PINCode='" & pinCode & "', @RoadNo='" & roadNo & "',@Tole='" & tole & _
                    "', @PhoneNo='" & phoneNo & "',@MobileNo='" & mobileNo & "',@Email='" & email & "',@InstitutionalName='" & institutionalName & _
                    "', @PreAddress='" & preAddress & "',@PreviousInstitutionEmail='" & previousInstitutionEmail & "',@Phone='" & phone & _
                    "', @PostboxNo='" & postboxNo & "',@ContactPerson='" & contactPerson & "',@Position='" & position & "',@Reason='" & reason & _
                    "', @Level='" & level & "',@Course='" & course & "',@Board='" & board & "',@Stream='" & stream & "',@InstitutionName='" & institutionName & _
                    "', @EducationalAddress='" & educationalAddress & "',@Division_or_Merit='" & deivisionorMerit & "',@Grade='" & grade & _
                    "', @CompleteDate='" & completeDate & "',@MarkPercentage='" & markPercentage & _
                    "', @CurrentLevel='" & currentLevel & "', @SemesterID='" & semesterId & "',@Section='" & section & _
                    "', @BranchCode='" & branchCode & "',@BirthMiti='" & Birthmiti & "',@IsEDJ='" & IIf(CheckedIsEDJ.Checked = True, 1, 0) & "',@CreatedBy='" & Session("userID") & "',@shift='" & ddl_shift.SelectedValue & _
                    "',@BranchID='" & branchId & "',@CompanyID='" & companyId & "',@userID='" & UserId & "'"

                'sql += "',@GlobalID='" & hdfGlobalID.Value.ToString() & "'"

                sql += ",@FatherName='" & FatherName & "', @FOccupation='" & FOccupation & "', @FMobile='" & FMobile & "', @FEmail='" & FEmail & "', @MotherName='" & MotherName & "', @MOccupation='" & MOccupation & "' ,@MMobile='" & MMobile & "', @MEmail='" & MEmail & "', @GName='" & GName & "', @GAddress='" & txtGuardianAddress.Text.Trim() & "', @GOccupation='" & GOccupation & "', @GMobileNo='" & GMobileNo & "', @GEmail='" & GEmail & "', @RelationToStudent='" & RelationToStudent & "', @GPhoneNo='" & GPhoneNo & "'"
                sql += ",@FQualification='" & FQualification & "',@MQualification='" & MQualification & "'"

                sql += ",@Hobbies='" & Hobbies & "', @Occupation='" & Occupation & "', @Relative='" & Relative & "', @ResidenceNo='" & ResidenceNo & "', @SponserBy='" & SponserBy & "', @SchoolshipFor='" & SchoolshipFor & "' ,@Awards='" & Awards & "', @Notes='" & Notes & "',@Height='" & Height & "',@Weight='" & Weight & "' ,@MeasurementUnit='" & MeasurementUnit & "',@HospitalName='" & HospitalName & "',@DoctorName='" & DoctorName & "',@BloodGroup2='" & BloodGroup2 & "',@BloodPressure='" & BloodPressure & "',@Allergy='" & Allergy & "',@Nutritition='" & Nutritition & "',@HealthNotes='" & HealthNotes & "',@IswearLans='" & IswearLans & "'"

                If ddlSubjectGroup.SelectedIndex > 0 Then
                    sql += ",@SubjectGroupID='" & ddlSubjectGroup.SelectedValue & "'"
                End If


                ds = Dao.ExecuteDataset(sql)
                If (ds.Tables.Count > 0) Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("StudentID") = ds.Tables(0).Rows(0).Item("id").ToString
                        sb.AppendLine("<div class='note note-success'>")
                        sb.AppendLine("<div class='close-note'>x</div>")
                        sb.AppendLine("<p>")
                        sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                        sb.AppendLine("</p>")
                        sb.AppendLine("</div>")
                        message.InnerHtml = sb.ToString
                        FieldClear()

                        'ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "documentUpload();", True)
                    End If
                Else
                    sb.AppendLine("<div class='note note-danger'>")
                    sb.AppendLine("<div class='close-note'>x</div>")
                    sb.AppendLine("<p>")
                    sb.AppendLine("Error while saving data due to paramters missing or mismatch !!")
                    sb.AppendLine("</p>")
                    sb.AppendLine("</div>")
                    message.InnerHtml = sb.ToString
                End If
            Catch ex As Exception
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ex.Message.ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End Try
        End If
    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Response.Redirect("/Student/Admission.aspx")
    End Sub

    Sub FieldClear()


        txtApplicationFormNo.Text = String.Empty
        txtRegistrationNo.Text = String.Empty
        txtRegistrationDate.Text = String.Empty
        'dropAcademicYearList.SelectedIndex = 0


        If (chkStatic.Checked = False) Then

            ddlLevel.SelectedIndex = 0
            ddlFaculty.SelectedIndex = 0
            ddlCourse.SelectedIndex = 0
            ddlBatch.SelectedIndex = 0
            ddlSemester.SelectedIndex = 0
            ddlSection.SelectedIndex = 0
            dropAdmissionStatusList.SelectedIndex = 0
            ddlSubjectGroup.SelectedIndex = 0
        End If

        dropCategoriesList.SelectedIndex = 0
        CheckedIsEDJ.Checked = False
        txtUniversityRegdNo.Text = String.Empty

        'ddl_shift.SelectedIndex = 0
        txtRollNo.Text = String.Empty

        txtFirstName.Text = String.Empty
        txtMiddleName.Text = String.Empty
        txtLastName.Text = String.Empty
        dropGender.SelectedIndex = 0
        txtDateofBirth.Text = String.Empty
        txtDateofBirth_Nep.Text = String.Empty
        'dropMartialStatus.SelectedIndex = 0
        'dropLanguageList.SelectedIndex = 0
        'DropDownReligion.SelectedIndex = 0
        'dropNationality.SelectedIndex = 0
        txtCitizenshipNo.Text = String.Empty
        txtPassportNo.Text = String.Empty
        txtProfession.Text = String.Empty

        'dropCountry.SelectedIndex = 0
        dropDownState.SelectedIndex = 0
        dropDownZone.SelectedIndex = 0
        dropDownDistrict.SelectedIndex = 0
        dropDownMunicipal.SelectedIndex = 0
        txtAddress1.Text = String.Empty
        txtAddress2.Text = String.Empty
        'txtVDCMUL.Text = String.Empty
        txtCity.Text = String.Empty
        'txtState.Text = String.Empty
        txtPINCode.Text = String.Empty
        txtRoadNo.Text = String.Empty
        txtTole.Text = String.Empty
        txtPhoneNo.Text = String.Empty
        txtmobileNo.Text = String.Empty
        txtEmail.Text = String.Empty

        txtBoard.Text = String.Empty
        txtLevel.Text = String.Empty
        txtStream.Text = String.Empty
        txtCorse.Text = String.Empty
        txtInstitutionName.Text = String.Empty
        txtEducatioalAddress.Text = String.Empty
        txtDivision_Or_Merit.Text = String.Empty
        txtGrade.Text = String.Empty
        txtCompleteDate.Text = String.Empty
        txtMarkPercentage.Text = String.Empty

        txtInstitutionalName.Text = String.Empty
        txtPreAddress.Text = String.Empty
        txtPreviousInstitutionEmail.Text = String.Empty
        txtPhone.Text = String.Empty
        txtPostboxNo.Text = String.Empty
        txtContactPerson.Text = String.Empty
        txtPosition.Text = String.Empty
        txtReason.Text = String.Empty


        txtFatherName.Text = String.Empty
        txtFatherOccupation.Text = String.Empty
        txtFatherMobile.Text = String.Empty
        txtFatherEmail.Text = String.Empty
        ddlFQualification.SelectedIndex = 0
        ddlMQualification.SelectedIndex = 0
        txtMotherName.Text = String.Empty
        txtMotherOccupation.Text = String.Empty
        txtMotherMobile.Text = String.Empty
        txtMotherEmail.Text = String.Empty

        txtGuardianName.Text = String.Empty
        txtGuardianAddress.Text = String.Empty
        txtGuardianOccupation.Text = String.Empty
        txtGuardianMobileNo.Text = String.Empty
        txtGuardianPhoneNo.Text = String.Empty
        txtGuardianEmail.Text = String.Empty
        txtRelationToStudent.Text = String.Empty

        txtHobbies.Text = String.Empty
        txtOccupation.Text = String.Empty
        txtRelative.Text = String.Empty
        txtAwards.Text = String.Empty
        txtNotes.Text = String.Empty
        txtResidenceNo.Text = String.Empty
        txtSponserBy.Text = String.Empty
        txtSchoolshipFor.Text = String.Empty

        txtHeight.Text = String.Empty
        txtWeight.Text = String.Empty
        txtHospitalName.Text = String.Empty
        txtDoctorName.Text = String.Empty
        dropBloodGroup.SelectedIndex = 0
        txtBloodPressure.Text = String.Empty
        txtAllergy.Text = String.Empty
        txtNutritition.Text = String.Empty
        txtHealthNotes.Text = String.Empty
        CheckIswearLens.Checked = False

    End Sub

    Protected Sub OnUploadComplete(ByVal sender As Object, ByVal e As AjaxFileUploadEventArgs)
        Dim fileName As String = Path.GetFileName(e.FileName)

        If Not String.IsNullOrWhiteSpace(fileName) Then
            Dim folderName As String = Session("StudentID").ToString().Trim
            If Not String.IsNullOrWhiteSpace(folderName) Then
                Dim stdFolder As String = Server.MapPath("~/Student/StudentDocuments/")
                Dim stdPath As String = System.IO.Path.Combine(stdFolder, folderName)

                If Not Directory.Exists(stdPath) Then
                    Directory.CreateDirectory(stdPath)
                End If

                If Not File.Exists(Server.MapPath("~/Student/StudentDocuments/" & folderName & "/" & fileName)) Then
                    AjaxFileUpload1.SaveAs(Server.MapPath("~/Student/StudentDocuments/" & folderName & "/" & fileName))
                End If
            Else
                AjaxFileUpload1.SaveAs(Server.MapPath("~/Student/StudentDocuments/" & fileName))
            End If
        End If

    End Sub

    Protected Sub OnClientUploadComplete1(ByVal sender As Object, ByVal e As AjaxFileUploadEventArgs)
        message.InnerHtml = "Update Files Successfully."
    End Sub

    'Protected Sub txtApplicationFormNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtApplicationFormNo.Load
    '    Dim FormNo As String = txtApplicationFormNo.Text.Trim.ToString()
    '    Dim sql As String
    '    sql = "Exec [Students].[usp_ApplicationForm] @flag='adm',@FormNo='" & FormNo & "',@BranchID=" & Session("BranchID")
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        txtFirstName.Text = ds.Tables(0).Rows(0).Item("FirstName").ToString()
    '        txtMiddleName.Text = ds.Tables(0).Rows(0).Item("MiddleName").ToString()
    '        txtLastName.Text = ds.Tables(0).Rows(0).Item("LastName").ToString()
    '        ddlBatch.SelectedValue = ds.Tables(0).Rows(0).Item("BatchID").ToString()
    '        hdfGlobalID.Value = ds.Tables(0).Rows(0).Item("GlobalID").ToString()
    '    Else
    '        txtApplicationFormNo.Text = ""
    '        txtFirstName.Text = ""
    '        txtMiddleName.Text = ""
    '        txtLastName.Text = ""
    '        ddlBatch.SelectedIndex = 1
    '        ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "ApplicationAlert", " alert('Application Form Number not found.')", True)
    '    End If
    'End Sub
End Class