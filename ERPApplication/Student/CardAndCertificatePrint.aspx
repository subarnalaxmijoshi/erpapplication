﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="CardAndCertificatePrint.aspx.vb" Inherits="School.CardAndCertificatePrint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        td.details-control
        {
            background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control
        {
            background: url('http://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
    </style>
    <script type="text/javascript">
         var branchid = <%= getSessionBranchID() %>;
        function format(d) {
            debugger;
            return 'Full name: ' + d.Name + ' Batch: ' + d.Batch + ' Semester: ' + d.Sem + '<br>' +
                   'Salary: ' + d.Sec + '<br>' +
        'The child row can contain any data you wish, including links, images, inner tables etc.';
        }

        $(document).ready(function () {
            var dt = $('#displaytable').DataTable({
                        "ajax": { "url": "/SetupServices.svc/GetStudentsForPrint?branchID="+ branchid +"", "type": "GET"
                                },
                "columns": [
            {
                "class": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "SN"},
            { "data": "id"},
            { "data": "Name" },
            { "data": "Batch" },
            { "data": "Semester" },
            { "data": "RollNo" },
            { "data": "DocumentType" },
            { "data": "DocumentTypeID" },
            { "data": "LastPrintedDate"},
            { "data": "Action"}
        ],
        "columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            }
            
        ],
                "order": [[1, 'asc']]
            });
            var detailRows = [];

            $('#displaytable tbody').on('click', 'tr td.details-control', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = dt.row(tr);
//                var idx = $.inArray(tr.attr('id'), detailRows);


                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');


//                     Remove from the 'open' array
//                    detailRows.splice(idx, 1);
                }
                else {

//                    row.child(format(row.data())).show();
//                    tr.addClass('details');

//                    // Add to the 'open' array
//                    if (idx === -1) {
//                        detailRows.push(tr.attr('id'));
                    //                    }

                    // Open this row
                    $.ajax({
                        url: '/SetupServices.svc/GetStudentPrintingInfo',
                        data: '{"studentID":"' + row.data().id + '","branchID":"' + branchid + '","docTypeID":"' + row.data().DocumentTypeID + '"}',
                        type: 'POST',
                        datatype: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            // row.child(format(row.data())).show();
                            row.child(data).show();
                            tr.addClass('shown');
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });

            // On each draw, loop over the `detailRows` array and show any child rows
//            dt.on('draw', function () {
//                $.each(detailRows, function (i, id) {
//                    $('#' + id + ' td.details-control').trigger('click');
//                });
//            });
        });
    </script>
    <script type="text/javascript">
        var branchid = <%= getSessionBranchID() %>;
        $(document).ready(function () {
            var dt = $('#tblpendinglist').DataTable({
                        
                "columns": [
            {
                "class": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "SN"},
            { "data": "StudentID"},
            { "data": "Name" },
            { "data": "Batch" },
            { "data": "Semester" },
             { "data": "RollNo" },
            { "data": "Document Type" },
            { "data": "DocumentTypeID" },
            {"data": "Action"}
        ],
        "columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            }
            
        ],
                "order": [[1, 'asc']]
            });
            var detailRows = [];

            $('#tblpendinglist tbody').on('click', 'tr td.details-control', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = dt.row(tr);
//                var idx = $.inArray(tr.attr('id'), detailRows);


                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');


//                     Remove from the 'open' array
//                    detailRows.splice(idx, 1);
                }
                else {                
                    $.ajax({
                        url: '/SetupServices.svc/GetStudentPrintingInfo',
                        data: '{"studentID":"' + row.data().StudentID + '","branchID":"' + branchid + '","docTypeID":"' + row.data().DocumentTypeID + '"}',
                        type: 'POST',
                        datatype: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                           
                            row.child(data).show();
                            tr.addClass('shown');
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });

          
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {

                $(document).on('click', '.ModifyField', function () {
                    //                $(".ModifyField").click(function () {
                    //                    alert("click");
                    debugger;
                    var thisItem = $(this);
                    var obtainId = $(this).attr("data-id");
                    var itemToModify = $(this).attr("data-modify");

                    var newAmount = prompt("Enter Value", "0");
                    console.log(newAmount);
                    if (newAmount === null || newAmount === false || newAmount === '' || newAmount === '0') {
                        console.log("Field is empty");
                        alert("Field is empty");
                        return;
                    }
                    else {
                        $.ajax({
                            url: '/SetupServices.svc/UpdatePrintingInfo',
                            data: '{"printdocumentId":"' + obtainId + '","modifyitem":"' + itemToModify + '","val" : "' + newAmount + '"}',
                            type: 'POST',
                            datatype: 'json',
                            contentType: 'application/json',
                            success: function (data) {
                                thisItem.html(newAmount);
                                alert(data);
                            },
                            error: function (data) {
                                alert(data.statusText);
                            },
                            failure: function (data) {
                                alert(data.statusText);
                            }

                        });

                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.reload', function () {
                $('#<%= btnReload.ClientID %>').click();
            });

            $("#toPDF").click(function () {
                $("#<%= btnToPDF.ClientID %>").click();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Form Settings</h4>
                    </div>
                    <div class="modal-body">
                        <asp:CheckBox ID="chkIsStatic" runat="server" Text="Tick if want to keep all data static." />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue hidden">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Card and Certificate <small>Print Card and Certificate </small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <%-- <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>--%>
                            <li id="toPDF"><a href="javascript:;">Download All Pending IDCards</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-user"></i><a href="/Student/Studentmenu.aspx">Student</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/Student/CardAndCertificatePrint.aspx">CardAndCertificate</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
            </div>
        </div>
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#create" aria-controls="create" role="tab"
                    data-toggle="tab">Pending</a></li>
                <li role="presentation"><a href="#display" aria-controls="display" role="tab" data-toggle="tab">
                    History</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="create">
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>Document Printing Information
                            </div>
                            <div class="tools">
                                <a href="" class="collapse"></a><a href="javascript:;" class="reload"></a><a class="config"
                                    href="#portlet-config" data-toggle="modal"></a><a href="" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <div id="listdata" runat="server">
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="btnDwnldAll_Pendings" runat="server" Text="Download All ID-Cards" CssClass="btn purple" />
                </div>
                <div role="tabpanel" class="tab-pane" id="display">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Document Printing History <a href="javascript:;" class="collapse">
                                        </a><a href="#portlet-config" data-toggle="modal" class="config"></a><a href="javascript:;"
                                            class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body flip-scroll" runat="server">
                                    <table id="displaytable" class="display table table-bordered table-striped table-condensed flip-content' "
                                        width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>
                                                </th>
                                                <th>
                                                    SN
                                                </th>
                                                <th>
                                                    id
                                                </th>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Batch
                                                </th>
                                                <th>
                                                    Semester
                                                </th>
                                                 <th>
                                                    RollNo
                                                </th>
                                                <th>
                                                    DocumentType
                                                </th>
                                                <th>
                                                    DocumentTypeID
                                                </th>
                                                <th>
                                                    LastPrintedDate
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>
                                                </th>
                                                <th>
                                                    SN
                                                </th>
                                                <th>
                                                    id
                                                </th>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Batch
                                                </th>
                                                <th>
                                                    Semester
                                                </th>
                                                 <th>
                                                    RollNo
                                                </th>
                                                <th>
                                                    DocumentType
                                                </th>
                                                <th>
                                                    DocumentTypeID
                                                </th>
                                                <th>
                                                    LastPrintedDate
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                             <asp:Button ID="btnDwnldAll_History" runat="server" Text="Re-Download All ID-Cards" CssClass="btn green" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnReload" runat="server" Text="Reload" class="btn purple hidden" />
    <asp:Button ID="btnToPDF" runat="server" Text="Export To PDF" Style="display: none;" />
</asp:Content>
