﻿Public Class CardAndCertificatePrint
    Inherits System.Web.UI.Page
    Dim ds As New DataSet
    Private Dao As New DatabaseDao
    Dim sb As New StringBuilder
    Dim StudentDocs As New GlobalStudentCertificate
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetPrintingList()
        If Not Page.IsPostBack Then
            If Not IsNothing(Request.QueryString("DelPrintID")) Then
                DeletePrintingRecord(Request.QueryString("DelPrintID").ToString.Trim)
            End If

            If (Not String.IsNullOrWhiteSpace(Request.QueryString("StudentID"))) Then
                If (Not IsNothing(Request.QueryString("PrintID"))) Then
                    Dim printId As String = Request.QueryString("PrintID").ToString.Trim
                    Dim studentId As String = Request.QueryString("StudentID").ToString.Trim
                    Dim docType As String = Request.QueryString("DocType").ToString.Trim

                    If (docType = "CC") Then 'Character

                        If Session("DatabaseName").ToString.Trim() = "JanaBhawanaCampus" Then 'JanaBhawana
                            StudentDocs.JanaBhawanaCharacterPDF(studentId, Session("BranchID"), printId, Session("userID"))
                        ElseIf Session("DatabaseName").ToString.Trim() = "KoteshworCampus2075" Then 'Koteshwor
                            StudentDocs.CharacterPDF_Portrait(studentId, Session("BranchID"), printId, Session("userID"))
                        ElseIf Session("DatabaseName").ToString.Trim() = "ButwalMultipleCampus2076" Then 'Butwal
                            StudentDocs.Character_Butwal(studentId, Session("BranchID"), printId, Session("userID"))
                        ElseIf Session("DatabaseName").ToString.Trim() = "PUHealth2076" Then 'SHAS
                            StudentDocs.Character_SHAS(studentId, Session("BranchID"), printId, Session("userID"))
                        ElseIf Session("DatabaseName").ToString.Trim() = "ThapathaliCampus" Then 'thapathali
                            'redirect to crystal report
                            Response.Redirect("/crystalreports/idcard/crcharacter.aspx?StudentID=" & studentId & "&PrintID=" & printId)
                        ElseIf Session("DatabaseName").ToString.Trim() = "BirendraMultipleCampus2076" Then
                            StudentDocs.GenerateCharacterPDF(studentId, Session("BranchID"), printId, Session("userID"))
                        Else
                            StudentDocs.GenerateCharacterPDF(studentId, Session("BranchID"), printId, Session("userID")) 'General
                        End If

                    ElseIf (docType = "ID") Then 'ID Card

                        If Session("DatabaseName").ToString.Trim() = "KoteshworCampus2075" Then
                            StudentDocs.IDCard_Portrait_Koteshwor(studentId, printId, Session("userID")) 'Koteshwor
                        ElseIf Session("DatabaseName").ToString.Trim() = "ButwalMultipleCampus2076" Then
                            StudentDocs.IDCard_Portrait_Butwal(studentId, printId, Session("userID")) 'Butwal
                        ElseIf Session("DatabaseName").ToString.Trim() = "TahacalCampusMRC2076" Then 'Tahachal
                            StudentDocs.IDCard_Tahachal(studentId, printId, Session("userID"))
                        ElseIf Session("DatabaseName").ToString.Trim() = "ThapathaliCampus" Then 'Thapathali
                            StudentDocs.IDCard_Thapathali(studentId, printId, Session("userID"))
                            'ElseIf Session("DatabaseName").ToString.Trim() = "DiktelCampus2076" Then 'Diktel
                            '    Response.Redirect("/crystalreports/idcard/IDCard.aspx?StudentID=" & studentId & "&PrintID=" & printId)
                        ElseIf Session("DatabaseName").ToString.Trim() = "ABCampus2075" Then 'Aadhikavi Campus
                            StudentDocs.IDCard_Aadhikavi(studentId, printId, Session("userID"))
                        ElseIf Session("DatabaseName").ToString.Trim() = "BirendraMultipleCampus2076" Then 'Birendra
                            'StudentDocs.IDCard_Portrait_Butwal(studentId, printId, Session("userID"))
                            Response.Redirect("/crystalreports/idcard/IDCard.aspx?PStatus=single&printedBy=" & Session("userID") & "&StudentID=" & studentId)
                        Else
                            StudentDocs.StudentIDCard_PDF(studentId, printId, Session("userID")) 'General
                        End If

                    ElseIf (docType = "TC") Then 'Transfer

                        StudentDocs.GenerateTransferPDF(studentId, printId, Session("userID"))

                    ElseIf (docType = "RL") Then 'Recommendation

                    End If
                End If
                'BiswoBhasa
                'Else
                '    StudentDocs.SingleStudentTranscript("158", "9", "1")
                '    StudentDocs.Generate_MarkSheetPDF("158")
            End If
        End If
    End Sub

    Protected Function getSessionBranchID() As String
        Return Session("BranchID").ToString
    End Function

    Sub GetPrintingList()
        Dim sql As String
        sql = "exec students.usp_PrintingDocuments @Flag='b' ,@BranchID=" & Session("BranchID")
        ds = Dao.ExecuteDataset(sql)
        Dim sb As StringBuilder = New StringBuilder("")
        sb.AppendLine("<table  class='table table-bordered table-striped table-condensed flip-content' id='tblpendinglist'>")
        sb.AppendLine("<thead class='flip-content'>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<th></th>")
        sb.AppendLine("<th>SN</th>")
        sb.AppendLine("<th>StudentID</th>")
        sb.AppendLine("<th>Name </th>")
        sb.AppendLine("<th>Batch </th>")
        sb.AppendLine("<th>Semester</th>")
        sb.AppendLine("<th>RollNo</th>")
        sb.AppendLine("<th>Document Type</th>")
        sb.AppendLine("<th>DocumentTypeID</th>")
        sb.AppendLine("<th>Action</th>")
        sb.AppendLine("</tr>")
        sb.AppendLine("</thead>")
        sb.AppendLine("<tbody>")
        Dim i As Integer = 0
        For Each row As DataRow In ds.Tables(0).Rows
            Dim printId = ds.Tables(0).Rows(i).Item("PrintingDocumentID").ToString
            Dim StudentID = ds.Tables(0).Rows(i).Item("StudentID").ToString
            Dim Name = ds.Tables(0).Rows(i).Item("Name").ToString
            Dim Batch = ds.Tables(0).Rows(i).Item("Batch").ToString
            Dim Sem = ds.Tables(0).Rows(i).Item("Sem.").ToString
            Dim Type = ds.Tables(0).Rows(i).Item("Type").ToString
            Dim RollNo = ds.Tables(0).Rows(i).Item("RollNo").ToString
            Dim DocumentTypeID = ds.Tables(0).Rows(i).Item("DocumentTypeID").ToString
            'Dim Status = ds.Tables(0).Rows(i).Item("Status").ToString

            sb.AppendLine("<tr><td class='numeric'></td>")
            sb.AppendLine("<td>" & i + 1 & "</td>")
            sb.AppendLine("<td>" & StudentID & "</td>")
            sb.AppendLine("<td><a href='/Student/studentinfodetails.aspx?StudentID=" & StudentID & "&Semester=" & Sem & "'>" & Name & "</a></td>")
            sb.AppendLine("<td>" & Batch & "</td>")
            sb.AppendLine("<td>" & Sem & "</td>")
            sb.AppendLine("<td>" & RollNo & "</td>")
            sb.AppendLine("<td>" & Type & "</td>")
            sb.AppendLine("<td>" & DocumentTypeID & "</td>")
            sb.AppendLine("<td style='text-align:center'><a href='/Student/CardAndCertificatePrint.aspx?StudentID=" & StudentID & "&DocType=" & Type & "&PrintID=" & printId & "'>Download</a> | ")
            sb.AppendLine("<a href='/Student/CardAndCertificatePrint.aspx?DelPrintID=" & printId & "'>Delete</a></td>")

            'sb.AppendLine("<td>" & Status & "</td>")
            sb.AppendLine("</tr>")
            i += 1
        Next
        sb.AppendLine("</tbody>")
        sb.AppendLine("</table>")
        listdata.InnerHtml = sb.ToString()
    End Sub

    Sub DeletePrintingRecord(ByVal PrintId As String)
        Try
            Dim sql As String
            sql = "exec students.usp_PrintingDocuments @flag='d' ,@PrintingDocumentID='" & PrintId & "'"
            ds = Dao.ExecuteDataset(sql)
            Dim sb As StringBuilder = New StringBuilder("")
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
                GetPrintingList()
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub


    Protected Sub btnReload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReload.Click
        GetPrintingList()
    End Sub

    Protected Sub btnToPDF_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToPDF.Click
        Dim sql As String
        sql = "exec students.usp_PrintingDocuments @Flag='id_pendings' ,@BranchID=" & Session("BranchID")
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then          
            'GlobalDs = ds
            If Session("DatabaseName").ToString.Trim() = "DiktelCampus2076" Then 'Diktel
                Response.Redirect("/crystalreports/idcard/IDCard.aspx")

            Else
                StudentDocs.StudentIDCard_Portrait_All(ds, Session("userID"))
            End If

        End If
    End Sub

    Protected Sub btnDwnldAll_Pendings_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDwnldAll_Pendings.Click
        If Session("DatabaseName").ToString.Trim() = "BirendraMultipleCampus2076" Then
            Response.Redirect("/crystalreports/idcard/IDCard.aspx?PStatus=pending&printedBy=" & Session("userID"))
        End If
    End Sub

    Protected Sub btnDwnldAll_History_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDwnldAll_History.Click
        If Session("DatabaseName").ToString.Trim() = "BirendraMultipleCampus2076" Then
            Response.Redirect("/crystalreports/idcard/IDCard.aspx?PStatus=history&printedBy=" & Session("userID"))
        End If
    End Sub
End Class