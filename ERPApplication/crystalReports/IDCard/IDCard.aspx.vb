﻿Imports System
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Public Class WebForm3
    Inherits System.Web.UI.Page
    ReadOnly Dao As New DatabaseDao
    Dim cryRpt As ReportDocument = New ReportDocument()
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim infoDS As DataSet = New DataSet
            Dim ds As DataSet = New DataSet
            Dim stdInfoDS As DataSet = New DataSet
            Dim dsPendings As DataSet = New DataSet
            Dim historyDS As DataSet = New DataSet
            Dim singleDS As DataSet = New DataSet
            Dim pStatus As String = ""
            Dim printedby As String = ""
            Dim studentID As String = ""
          
            If Session("DatabaseName").ToString.Trim() = "TahacalCampusMRC2076" Then 'TahacalCampusMRC2076

                ds = GetDataSet()
                cryRpt.Load(Server.MapPath("~/CrystalReports/IDCARD/RptIDCardTahachal.rpt"))
                cryRpt.Database.Tables("DataTable2").SetDataSource(ds.Tables(0)) 'Institutional Info
                cryRpt.Database.Tables("DataTable1").SetDataSource(ds.Tables(1))

            ElseIf Session("DatabaseName").ToString.Trim() = "DiktelCampus2076" Then 'Diktel

                infoDS = GetInstitutionalInfo()
                stdInfoDS = GetPrintPendings()
                cryRpt.Load(Server.MapPath("~/CrystalReports/IDCARD/RptIDCardDiktel.rpt"))
                cryRpt.Database.Tables("DataTable2").SetDataSource(infoDS.Tables(0)) 'Institutional Info
                cryRpt.Database.Tables("DataTable1").SetDataSource(stdInfoDS.Tables(0))

            ElseIf Session("DatabaseName").ToString.Trim() = "BirendraMultipleCampus2076" Then 'Birendra
                If Not String.IsNullOrWhiteSpace(Request.QueryString("PStatus")) Then
                    pStatus = Request.QueryString("PStatus").ToString.Trim
                    If pStatus = "single" Then
                        If Not String.IsNullOrWhiteSpace(Request.QueryString("StudentID")) Then
                            studentID = Request.QueryString("StudentID").ToString.Trim
                            singleDS = GetSingleStudentCard(studentID)
                        End If
                    End If
                End If
                If Not String.IsNullOrWhiteSpace(Request.QueryString("printedBy")) Then
                    printedby = Request.QueryString("printedBy").ToString.Trim
                End If

                dsPendings = GetPrintingList()
                historyDS = GetHistoryList()


                cryRpt.Load(Server.MapPath("~/CrystalReports/IDCARD/rptidbirendra.rpt"))
                If pStatus = "pending" Then
                    cryRpt.Database.Tables("DataTable1").SetDataSource(dsPendings.Tables(1))
                    cryRpt.Database.Tables("DataTable2").SetDataSource(dsPendings.Tables(0))

                    For Each r As DataRow In dsPendings.Tables(2).Rows
                        UpdatePrintingStatus(r(0).ToString.Trim, printedby)
                    Next
                End If
                If pStatus = "history" Then
                    cryRpt.Database.Tables("DataTable1").SetDataSource(historyDS.Tables(1))
                    cryRpt.Database.Tables("DataTable2").SetDataSource(historyDS.Tables(0))

                    For Each r As DataRow In historyDS.Tables(2).Rows
                        UpdatePrintingStatus(r(0).ToString.Trim, printedby)
                    Next
                End If
                If pStatus = "single" Then
                    cryRpt.Database.Tables("DataTable1").SetDataSource(singleDS.Tables(1))
                    cryRpt.Database.Tables("DataTable2").SetDataSource(singleDS.Tables(0))

                    For Each r As DataRow In singleDS.Tables(2).Rows
                        UpdatePrintingStatus(r(0).ToString.Trim, printedby)
                    Next
                End If
            End If


            CrystalReportViewer1.ReportSource = cryRpt
            CrystalReportViewer1.RefreshReport()

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Function GetInstitutionalInfo() As DataSet
        Dim sql As String
        Dim ds As New DataSet
        Dim infoDS As New DataSet
        sql = "select top 1 upper([Name]) [CampusName] ,[Address] [Address] ,Telephone,Fax,POSTBoxNo,Website,Email,Slogan from Management.InstitutionalInfo "
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("University", GetType(String))
            dt.Columns.Add("FacultyOrInstitue", GetType(String))
            dt.Columns.Add("CampusName", GetType(String))
            dt.Columns.Add("Address", GetType(String))
            dt.Columns.Add("Phone", GetType(String))
            dt.Columns.Add("Email", GetType(String))
            dt.Columns.Add("Website", GetType(String))

            Dim dr As DataRow = dt.NewRow
            dr(0) = "TRIBHUWAN UNIVERSITY"
            dr(1) = "FACULTY OF "
            dr(2) = ds.Tables(0).Rows(0).Item("CampusName").ToString
            dr(3) = ds.Tables(0).Rows(0).Item("Address").ToString
            dr(4) = "Tel: " & ds.Tables(0).Rows(0).Item("Telephone").ToString
            dr(5) = ds.Tables(0).Rows(0).Item("Email").ToString
            dr(6) = ds.Tables(0).Rows(0).Item("Website").ToString

            dt.Rows.Add(dr)
            infoDS.Tables.Add(dt)

        End If
        Return infoDS

    End Function

    Function GetDataSet() As DataSet
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC Management.usp_IDCARD"
        ds = Dao.ExecuteDataset(sql)
        Return ds
    End Function

    Function GetPrintPendings() As DataSet
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC [Students].[usp_IDCARD] @flag='allpendings',@BranchID='" & Session("BranchID") & "',@UserID='" & Session("userID") & "'"
        ds = Dao.ExecuteDataset(sql)
        Return ds
    End Function

    Function GetPrintingList() As DataSet
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC Students.usp_IDCARD @flag='pending',@BranchID='" & Session("BranchID") & "'"
        ds = Dao.ExecuteDataset(sql)
        Return ds
    End Function

    Function GetHistoryList() As DataSet
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC Students.usp_IDCARD @flag='history',@BranchID='" & Session("BranchID") & "'"
        ds = Dao.ExecuteDataset(sql)
        Return ds
    End Function

    Private Function UpdatePrintingStatus(ByVal printId As String, ByVal printedBy As String)
        Dim sql, msg As String
        Dim ds As New DataSet
        Try
            sql = "exec students.usp_PrintingDocuments @flag='p' ,@PrintingDocumentID='" & printId & "',@PrintedBy='" & printedBy & "'"
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                msg = ds.Tables(0).Rows(0).Item("ErrorCode").ToString().Trim
                Return msg
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Function GetSingleStudentCard(ByVal studentID As String) As DataSet
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC Students.usp_IDCARD @flag='single',@BranchID='" & Session("BranchID") & "'"
        sql += ",@StudentID='" & studentID & "'"
        ds = Dao.ExecuteDataset(sql)
        Return ds
    End Function

End Class