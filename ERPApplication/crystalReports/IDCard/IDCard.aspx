﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IDCard.aspx.vb" Inherits="School.WebForm3" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../crystalreportviewers/js/crviewer/crv.js" type="text/javascript"></script>
    <style type="text/css">
        .page
        {
            width: 23cm;
            min-height: 29.7cm;
            padding: 0px;
            margin: 1cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            padding-top: 0px;
        }
    
        
    </style>
    <script type="text/javascript">


        function Print() {
            var dvReport = document.getElementById("dvReport");
            var frame1 = dvReport.getElementsByTagName("iframe")[0];
            if (navigator.appName.indexOf("Internet Explorer") != -1) {
                frame1.name = frame1.id;
                window.frames[frame1.id].focus();
                window.frames[frame1.id].print();
            }
            else {
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
                frameDoc.print();
            }
        }
            </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="page">
        <asp:Button ID="btnPrint" runat="server" Text="Print Directly" OnClientClick="Print()"></asp:Button>
         <div id="dvReport">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False"
            AutoDataBind="true" BestFitPage="True" PrintMode="ActiveX" 
                 ToolPanelView="None" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
