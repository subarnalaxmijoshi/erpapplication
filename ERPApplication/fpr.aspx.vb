﻿Imports System.IO
Imports System.Web.HttpContext

Public Class fpr
    Inherits System.Web.UI.Page
    Private Dao As New DatabaseDao

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not String.IsNullOrWhiteSpace(Request.QueryString("userid")) Then
            If Not String.IsNullOrWhiteSpace(Request.QueryString("ISActive")) Then
                GetSchoolInfo()

            End If
        End If
    End Sub

    Protected Sub btn_changePassword_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_changePassword.Click
        Dim userid As String = Request.QueryString("userid")
        Dim ISActive As String = Request.QueryString("IsActive")
        PasswordReset(txtnewpassword.Text, txtconfirmpassword.Text, userid, ISActive)
    End Sub

    Sub PasswordReset(ByVal newpassword As String, ByVal confirmpassword As String, ByVal userid As String, ByVal IsActive As String)
        Dim sb As StringBuilder = New StringBuilder("")
        Dim sql As String
        Dim mes As String
        sql = "exec Management.usp_PasswordRecovery @flag = 'resetPassword', @newpassword = '" & newpassword & "', @confirmpassword = '" & confirmpassword & "' , @userid = '" & userid & "', @ISActive = '" & IsActive & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        mes = ""

        If ds.Tables(0).Rows(0).Item("ErrorCode").ToString.Trim = "0" Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
            ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "UserMsg", "<script>alert('" & mes & "');if(alert){ window.location='login.aspx';}</script>", False)
        ElseIf ds.Tables(0).Rows(0).Item("ErrorCode").ToString.Trim = "1" Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
            ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "UserMsg", "<script>alert('" & mes & "');if(alert){ window.location='fpr.aspx?userid=" & userid & "&ISActive=" & IsActive & "';}</script>", False)

        Else
            mes = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
            ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "UserMsg", "<script>alert('" & mes & "');if(alert){ window.location='login.aspx';}</script>", False)

        End If




    End Sub

    Protected Sub GetSchoolInfo()
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec  [Management].[usp_InstitutionalInfo] @Flag='s'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                schoolName.InnerHtml = ds.Tables(0).Rows(0).Item("Name").ToString.Trim()
                schoolAddress.InnerHtml = ds.Tables(0).Rows(0).Item("Address").ToString.Trim()
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class