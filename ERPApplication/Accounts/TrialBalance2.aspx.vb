﻿Imports System.Globalization
Imports System.IO
Imports System.Web.HttpContext
Public Class TrialBalance2
    Inherits System.Web.UI.Page
    Private ReadOnly Dao As New DatabaseDao
    Private sql As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetBranch()
            GetFiscalYearDates()
        End If
    End Sub

    Sub GetBranch()
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_BranchSetup] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlBranch.DataSource = ds.Tables(0)
            ddlBranch.DataTextField = "BranchName"
            ddlBranch.DataValueField = "BranchID"
            ddlBranch.DataBind()
            ddlBranch.Items.Insert(0, New ListItem(" ALL ", "0"))
        Else
            ddlBranch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    Sub GetFiscalYearDates()
        Dim ds As New DataSet
        sql = "EXEC [Setup].[usp_FiscalYearMaster] @Flag='s',@BranchID= '" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If (ds.Tables(0).Rows.Count > 0) Then
            txtDateFrom.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DateFrom")).ToString("dd MMM yyyy")
            txtDateTo.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DateTo")).ToString("dd MMM yyyy")
        End If
    End Sub

    Protected Function GetisBSDate() As String
        Return System.Configuration.ConfigurationManager.AppSettings("isBSDate").ToString
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

        Dim fromdate As String = Helper.GetFrontToDbDate(txtDateFrom.Text)
        Dim todate As String = Helper.GetFrontToDbDate(txtDateTo.Text)
        GetTrailBalance(fromdate, todate)

    End Sub

    Protected Sub GetTrailBalance(ByVal dateFrom As String, ByVal dateTo As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        message.InnerHtml = ""
        listData.InnerHtml = ""
        Dim dbName As String = Session("DatabaseName").ToString.Trim()
        Try
            If (ddlBranch.SelectedValue = 0) Then
                sql = "EXEC [Accounts].[usp_TrailBalance] @Type= '" & ddlType.SelectedValue & "',@FiscalYearID='" & Session("FiscalYearID") & "'"
                sql += ", @DateFrom='" & dateFrom & "', @DateTo='" & dateTo & "'"
            Else
                sql = "EXEC [Accounts].[usp_TrailBalance] @BranchID= '" & ddlBranch.SelectedValue & "', @Type= '" & ddlType.SelectedValue & "',@FiscalYearID='" & Session("FiscalYearID") & "'"
                sql += ", @DateFrom='" & dateFrom & "', @DateTo='" & dateTo & "'"
            End If
            
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine("<i class='fa fa-cogs'></i>Trial Balance Information</div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")

                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content' id='tblTrialBalance'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>Particulars Name</th>")
                sb.AppendLine("<th class='text-nowrap'>Opening Balance Dr</th>")
                sb.AppendLine("<th class='text-nowrap'>Opening Balance Cr</th>")
                sb.AppendLine("<th>Net Debit</th>")
                sb.AppendLine("<th>Net Credit</th>")
                sb.AppendLine("<th class='text-nowrap'>Closing Balance Dr</th>")
                sb.AppendLine("<th class='text-nowrap'>Closing Balance Cr</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                Dim prevCat As String = ds.Tables(0).Rows(0).Item("AccountCategoriesName").ToString
                sb.AppendLine("<tr><td colspan='8'><h4>" & prevCat & "</h4></td></tr>")
                For Each row As DataRow In ds.Tables(0).Rows
                    Dim AccountCategoriesName As String = ds.Tables(0).Rows(i).Item("AccountCategoriesName").ToString
                    Dim accountName As String = ds.Tables(0).Rows(i).Item("AccountName").ToString
                    Dim openingdr As String = ds.Tables(0).Rows(i).Item("Opening Balance Dr").ToString
                    Dim openingcr As String = ds.Tables(0).Rows(i).Item("Opening Balance Cr").ToString
                    Dim netdr As String = ds.Tables(0).Rows(i).Item("Net Dr").ToString
                    Dim netcr As String = ds.Tables(0).Rows(i).Item("Net Cr").ToString
                    Dim closingdr As String = ds.Tables(0).Rows(i).Item("Closing Balance Dr").ToString
                    Dim closingcr As String = ds.Tables(0).Rows(i).Item("Closing Balance Cr").ToString


                    If (Not String.IsNullOrEmpty(openingdr)) Then
                        openingdr = Convert.ToDecimal(openingdr).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    End If
                    If (Not String.IsNullOrEmpty(openingcr)) Then
                        openingcr = Convert.ToDecimal(openingcr).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    End If
                    If (Not String.IsNullOrEmpty(netdr)) Then
                        netdr = Convert.ToDecimal(netdr).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    End If
                    If (Not String.IsNullOrEmpty(netcr)) Then
                        netcr = Convert.ToDecimal(netcr).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    End If
                    If (Not String.IsNullOrEmpty(closingdr)) Then
                        closingdr = Convert.ToDecimal(closingdr).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    End If
                    If (Not String.IsNullOrEmpty(closingcr)) Then
                        closingcr = Convert.ToDecimal(closingcr).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    End If


                    If prevCat = AccountCategoriesName Then

                    Else
                        sb.AppendLine("<tr><td colspan='8'><h4>" & AccountCategoriesName & "</h4></td></tr>")
                    End If
                    sb.AppendLine("<tr>")
                    sb.Append("<td class='numeric' >" & (i + 1) & "</td>")
                    'sb.AppendLine("<td>" & accountName & "</td>")
                    sb.AppendLine("<td><a data-toggle='modal' data-id='" & accountName & "' data-db='" & dbName & "'  id='show' class='voucherDetails' href='#portlet-config'>" & accountName & "</a></td>")
                    sb.AppendLine("<td class='TextRight'>" & openingdr & "</td>")
                    sb.AppendLine("<td class='TextRight'>" & openingcr & "</td>")
                    sb.AppendLine("<td class='TextRight' >" & netdr & "</td>")
                    sb.AppendLine("<td class='TextRight' >" & netcr & "</td>")
                    sb.AppendLine("<td class='TextRight' >" & closingdr & "</td>")
                    sb.AppendLine("<td class='TextRight' >" & closingcr & "</td>")
                    sb.AppendLine("</tr>")
                    i += 1

                    prevCat = AccountCategoriesName
                Next

                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                listData.InnerHtml = sb.ToString()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Information Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Error generating trail balance or some values missing <hr>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub
  

    Protected Function getSessionBranchID() As String
        Return Session("BranchID").ToString
    End Function

    Protected Function getSessionFiscalID() As String
        Return Session("FiscalYearID").ToString.Trim()
    End Function

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function


    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Trial Balance Report")
    End Sub
End Class