﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="PayrollPreparation.aspx.vb" Inherits="School.PayrollPreparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="https://cdn.jsdelivr.net/datatables.mark.js/2.0.0/datatables.mark.min.css"
        rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.min.css"
        rel="stylesheet" type="text/css" />
    <script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"
        type="text/javascript"></script>
    <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css"
        rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet"
        type="text/css" />
    <script src="http://cdn.datatables.net/fixedheader/2.1.0/js/dataTables.fixedHeader.min.js"
        type="text/javascript"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"
        rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <style type="text/css">
        td.highlight
        {
            background-color: whitesmoke !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('.ApplyFormulaForMe').change(function () {
                    var a = $(this).prop('checked');
                    var thisItem = $(this);
                    var obtainId = $(this).attr("data-id");
                    var MonthID = $(this).attr("data-monthid");
                    var flag = $(this).attr("data-flag");

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/ApplyFormulaForMe',
                        data: '{"EmployeeID" : "' + obtainId + '","MonthID" : "' + MonthID + '","Flag" : "' + flag + '","value" : "' + a + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            $('#toggle-event').bootstrapToggle(data)
                            //  alert(data);
                            // location.href = '/Exam/EditObtainedMark.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });

                })
            })
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#displaytable').DataTable({

                fixedHeader: true,
                "lengthMenu": [[-1], ["All"]],
                mark: true



            });
            var table = $('#displaytable').DataTable();

            $('#displaytable tbody')
        .on('mouseenter', 'td', function () {
            var colIdx = table.cell(this).index().column;

            $(table.cells().nodes()).removeClass('highlight');
            $(table.column(colIdx).nodes()).addClass('highlight');
        });
            $('a.toggle-vis').on('click', function (e) {
                e.preventDefault();

                // Get the column API object
                var column = table.column($(this).attr('data-column'));

                // Toggle the visibility
                column.visible(!column.visible());
            });
        });


    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%= ddl_fiscalYear.ClientID %>').change(function () {
                $('#<%= ddl_year.ClientID %>').html("<option value=''>Loading....</option>");
            });

            $('#<%= ddl_year.ClientID %>').change(function () {
                $('#<%= ddl_month.ClientID %>').html("<option value=''>Loading....</option>");
            });
            window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {

                $('#<%= ddl_fiscalYear.ClientID %>').change(function () {
                    $('#<%= ddl_year.ClientID %>').html("<option value=''>Loading....</option>");
                });

                $('#<%= ddl_year.ClientID %>').change(function () {
                    $('#<%= ddl_month.ClientID %>').html("<option value=''>Loading....</option>");
                });

            });
        });
    
    </script>
    <script type="text/javascript">
        var checkFromValidation = false;
        $(document).ready(function () {


            $('#form').validate();

            $("#<%=ddl_year.ClientID %>").rules('add', { required: true, messages: { required: 'Year is required.'} });
            $("#<%=ddl_month.ClientID %>").rules('add', { required: true, messages: { required: 'Month is required.'} });



            checkFromValidation = function () {
                var bool = true;
                if ($('#<%=ddl_year.ClientID %>').valid() == false) bool = false;
                if ($('#<%=ddl_month.ClientID %>').valid() == false) bool = false;

                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            }

        });
    
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".ClassAddableAmount").click(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var flag = $(this).attr("flag");

                var newTheory = prompt("Enter Deductive Amount", "0");
                console.log(newTheory);
                if (newTheory === null || newTheory === false) {
                    console.log("Deductive Amont is empty");
                    return;
                } else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/updatePayrollValues',
                        data: '{"id" : "' + obtainId + '","value" : "' + newTheory + '","flag" : "' + flag + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            thisItem.html(newTheory);
                            alert(data);
                            // location.href = '/Exam/EditObtainedMark.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });
        });
    </script>
    <script type="text/javascript">

        function disableBtn() {
            $('#MainContent_btnUpdateSalary').addClass('disabled');
            $('#MainContent_btnUpdan').addClass('disabled');
            $('#MainContent_btnSanchayaKosh').addClass('disabled');
            $('#MainContent_Button2').addClass('disabled');
            $('#MainContent_Button1').addClass('disabled');
            $('#MainContent_btnUpdateIncomeTax').addClass('disabled');
            $('#MainContent_btnUpdateSF').addClass('disabled');
            $('#MainContent_btn_fiftyperAllowance').addClass('disabled');
            $('#MainContent_btn_CIT').addClass('disabled');
            $('#MainContent_btn_formula').addClass('disabled');
            $('#MainContent_btn_UpdateGrade').addClass('disabled');
        }

        function undisableBtn() {

            $('#MainContent_btnUpdateSalary').removeClass('disabled');
            $('#MainContent_btnUpdan').removeClass('disabled');
            $('#MainContent_btnSanchayaKosh').removeClass('disabled');
            $('#MainContent_Button1').removeClass('disabled');
            $('#MainContent_Button2').removeClass('disabled');
            $('#MainContent_btnUpdateIncomeTax').removeClass('disabled');
            $('#MainContent_btnUpdateSF').removeClass('disabled');
            $('#MainContent_btn_fiftyperAllowance').removeClass('disabled');
            $('#MainContent_btn_CIT').removeClass('disabled');
            $('#MainContent_btn_formula').removeClass('disabled');
            $('#MainContent_btn_UpdateGrade').removeClass('disabled');
        }



        $(document).ready(function () {
            $('#ck_tot').click(function () {

                if ($(this).is(":checked")) {

                    undisableBtn();
                } else {
                    disableBtn();
                }
            });
        });


    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".UpdatePayrollSingleEmployee").click(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var MonthID = $(this).attr("data-monthid");
                var flag = $(this).attr("data-flag");

                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/UpdatePayrollSingleEmployee',
                    data: '{"EmployeeID" : "' + obtainId + '","MonthID" : "' + MonthID + '","Flag" : "' + flag + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {

                        alert(data);
                        // location.href = '/Exam/EditObtainedMark.aspx';

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });
            });

        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".DeletePayroll").click(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var MonthID = $(this).attr("data-monthid");
                var flag = $(this).attr("data-flag");

                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/DeleteRecordsOfPayroll',
                    data: '{"EmployeeID" : "' + obtainId + '","MonthID" : "' + MonthID + '","Flag" : "' + flag + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {

                        alert(data);
                        // location.href = '/Exam/EditObtainedMark.aspx';

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });
            });

        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".glyphicon").click(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var flag = $(this).attr("data-flag");
                var oldvalue = $('#' + obtainId)
                                .clone()     //clone the element
                                .children() //select all the children
                                .remove()   //remove all the children
                                .end()
                                .text();

                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/UpdatePayrollSingleEmployeeRefresh',
                    data: '{"ID" : "' + obtainId + '","Flag" : "' + flag + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        debugger;
                        if (data > 0) {
                            //                             $('#' + obtainId).html(data);
                            $('#' + obtainId).html('');
                            $('#' + obtainId).html(oldvalue);
                            $('#' + obtainId).append("<br>");
                            $('#' + obtainId).append("<span style='color:red;'>" + data + "</span>");
                            oldvalue = "";
                        }

                        //                         $('#' + obtainId).css({ "color": "red" });
                        // location.href = '/Exam/EditObtainedMark.aspx';

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });
            });

        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $(".myinfo").click(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var flag = $(this).attr("data-flag");

                var newTheory = prompt("Enter Deductive Amount", "0");
                console.log(newTheory);
                if (newTheory === null || newTheory === false) {
                    console.log("Deductive Amont is empty");
                    return;
                } else {

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/AddRemarksOnPayroll',
                        data: '{"ID" : "' + obtainId + '","Remarks" : "' + newTheory + '","Flag" : "' + flag + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {

                            alert(data);
                            // location.href = '/Exam/EditObtainedMark.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });

        });


    </script>
    <%--    <script type="text/javascript">

        $(function () {
            $('#toggle-event').change(function () {
                var a = $(this).prop('checked');
                alert(a);
            })
        })
</script>--%>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#toggle-trigger").change(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var MonthID = $('#<%= hf_monthID.ClientID %>').val();
                var flag = $('#<%= ddl_type.ClientID %>').val();
                var a = $(this).prop('checked');

                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/ApplyFormulaForAll',
                    data: '{"value" : "' + a + '","MonthID" : "' + MonthID + '","Flag" : "' + flag + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {

                        // alert(data);
                        // location.href = '/Exam/EditObtainedMark.aspx';

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });
            });

        });
    </script>
    <style type="text/css">
        a.toggle-vis
        {
            border: 1px solid #909090;
            padding: 5px;
            background-color: #ECECEC;
            cursor: pointer;
            border-radius: 10px 0px 10px 0px !important;
            margin: 5px;
            display: -moz-grid-group;
            display: grid-group;
        }
        
        
        
        table.fixedHeader-floating
        {
            position: fixed !important;
            background-color: white;
        }
        table.fixedHeader-floating.no-footer
        {
            border-bottom-width: 0;
        }
        table.fixedHeader-locked
        {
            position: absolute !important;
            background-color: white;
        }
        @media print
        {
            table.fixedHeader-floating
            {
                display: none;
            }
        }
        
        changeMe
        {
            color: Red;
            background-color: Green;
        }
        .myinfo
        {
            padding-right: 5px;
        }
        
        .myinfoActive
        {
            padding-right: 5px;
            color: Red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hf_monthID" runat="server" />
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Update Manual Basic Salary-wise update</h4>
                    </div>
                    <div class="modal-body">
                        <label class="col-md-12 control-label">
                            <input id="ch_IsManual" type="checkbox" runat="server" />
                            Tick the check box to enable manual basic salary-wise update.
                        </label>
                    </div>
                    <div class="modal-footer">
                        <%--     <button type="button" >
                            Save changes</button>--%>
                        <asp:Button ID="btn_Save_BasicSalary" runat="server" Text="Save" class="btn blue" />
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Monthly Payroll Preparation <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                          
                           <%-- <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>--%>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    
                    <li><a href="/accounts/newpayroll/PayrollPreparation.aspx">Monthly Payroll Preparation</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Filters
                        </div>
                        <div class="tools">
                            <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                            </a><a href="" class="reload"></a><a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>--%>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Fiscal Year :</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                            <asp:DropDownList ID="ddl_fiscalYear" runat="server" CssClass="form-control" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%= ddl_year.ClientID%>" class="error" style="display: none">
                                        </label>
                                    </div>
                                    <label class="col-md-2 control-label">
                                        Addable Type :</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                            <asp:DropDownList ID="ddl_addablesType" runat="server" CssClass="form-control" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%= ddl_addablesType.ClientID%>" class="error" style="display: none">
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button ID="Button1" class="btn red disabled" runat="server" Text="Reset to Zero" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Year :</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                            <asp:DropDownList ID="ddl_year" runat="server" CssClass="form-control" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%= ddl_year.ClientID%>" class="error" style="display: none">
                                        </label>
                                    </div>
                                    <label class="col-md-2 control-label">
                                        Deductive Type :</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                            <asp:DropDownList ID="ddl_deductiveType" runat="server" CssClass="form-control" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%= ddl_addablesType.ClientID%>" class="error" style="display: none">
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button ID="Button2" class="btn red disabled" runat="server" Text="Reset to Zero" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Month :</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                            <asp:DropDownList ID="ddl_month" runat="server" CssClass="form-control" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%= ddl_month.ClientID%>" class="error" style="display: none">
                                        </label>
                                    </div>
                                    <label class="col-md-7 control-label">
                                        <input id="ck_tot" type="checkbox" />
                                        Tick the check box to enable buttons. Buttons are disable to prevents mistakenly
                                        clicked. One click effects one month values of all employees.
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Select Type:</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                            <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="u">Addables</asp:ListItem>
                                                <asp:ListItem Value="d">Deductives</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%= ddl_type.ClientID%>" class="error" style="display: none">
                                        </label>
                                    </div>
                                    <label class="col-md-6 control-label alert-info bg-fa-flag border pull-chart-tooltip animate ">
                                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                                    </label>
                                </div>
                                <%--  </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-1 col-md-11">
                                        <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkFromValidation();"
                                            runat="server" Text="Submit" />
                                        <asp:Button ID="btnCancel" type="button" class="btn default" runat="server" Text="Cancel" />
                                        <input id="toggle-trigger" type="checkbox" data-toggle="toggle">
                                        <div class="pull-right">
                                            <asp:Button ID="btnUpdateIncomeTax" class="btn yellow disabled" runat="server" Text="Income Tax"
                                                ToolTip="Apply the value to Income tax calculated as rule" />
                                            <asp:Button ID="btnUpdateSF" class="btn purple disabled" runat="server" Text="SSF"
                                                ToolTip="Social Security Fund" />
                                            <asp:Button ID="btnUpdateSalary" class="btn green disabled" runat="server" Text="Salary"
                                                ToolTip="Apply the value calculated from grade to basic salary" />
                                            <asp:Button ID="btnUpdan" class="btn blue disabled" runat="server" Text="Upadan"
                                                ToolTip="Apply the value caculated from basic salary and grade as gratituity" />
                                            <asp:Button ID="btnSanchayaKosh" class="btn red disabled" runat="server" Text="PF"
                                                ToolTip="Apply 10% on addable and 20% on deductive from basic salary and grade" />
                                            <asp:Button ID="btn_fiftyperAllowance" class="btn yellow disabled" runat="server"
                                                Text="50% Allowance" ToolTip="Apply 50% value on allowance of basic salary" />
                                            <asp:Button ID="btn_CIT" class="btn yellow disabled" runat="server" Text="CIT" ToolTip="Citizen Investment Trust" />
                                            <asp:Button ID="btn_formula" class="btn red disabled" runat="server" Text="Formula"
                                                ToolTip="Apply value associated in formula" />
                                            <asp:Button ID="btn_UpdateGrade" class="btn yellow disabled" runat="server" Text="UG"
                                                ToolTip="Upgrade Grade Automatically" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="listData" runat="server">
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnToPdf" runat="server" Text="Export To Pdf" />
    <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" />
    <script type="text/javascript">
        $("#<%= btnToPdf.ClientID %>").hide();
        $("#<%= btnToExcel.ClientID %>").hide();

        $(document).ready(function () {
            $("#<%= btnToPdf.ClientID %>").hide();
            $("#<%= btnToExcel.ClientID %>").hide();

            $("#toPdf").click(function () {
                $("#<%= btnToPdf.ClientID %>").click();
            });

            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });
        });
    </script>
</asp:Content>
