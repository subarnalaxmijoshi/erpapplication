﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SalaryVoucherApproval.aspx.vb" Inherits="School.SalaryVoucherApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            $(".approv").click(function () {
                var thisItem = $(this);
                var monthid = $(this).attr("data-MonthID");
                var userid = $(this).attr("data-UserID");
                var branchid = $(this).attr("data-BranchID");

                var a = confirm("Are you comfirm the this month salary is complete Perfectly? \nIf you post the salary to main accounts, It is unable to reverse and modify.\n\n Verify again before Approve.");
                if (a) {

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/ApprovedPayrollForThisMonth_new',
                        data: '{"MonthID" : "' + monthid + '","UserID" : "' + userid + '","BranchID" : "' + branchid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            thisItem.html('Approved & Locked');
                            alert(data);
                            location.href = '/accounts/newpayroll/SalaryVoucherApproval.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });

                }
            });
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".Fetch").click(function () {
                var thisItem = $(this);
                var monthid = $(this).attr("data-MonthID");
                var userid = $(this).attr("data-UserID");
                var branchid = $(this).attr("data-BranchID");

                var a = confirm("Are you comfirm ?");
                if (a) {

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/FetchFromSetupVoucher_new',
                        data: '{"MonthID" : "' + monthid + '","UserID" : "' + userid + '","BranchID" : "' + branchid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            //  thisItem.html('Approved & Locked');
                            alert(data);
                            location.href = '/accounts/newpayroll/SalaryVoucherApproval.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });

                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <%--   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Payroll Approval and View<small>Salary voucher approval and view voucher month-wise.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                  
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    
                    <li><a href="/accounts/newpayroll/SalaryVoucherApproval.aspx">Payroll Approval and View</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Payroll Information</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                    class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll" id="listData" runat="server">
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <asp:Button ID="btnGeneratePDF" class="btn purple" runat="server" Text="Download PDF"
                            Visible="false" />
                        <asp:Button ID="btnPrint" type="button" class="btn default" runat="server" Text="Print"
                            Visible="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>