﻿Public Class PayrollPreparation
    Inherits System.Web.UI.Page

    Private Dao As New DatabaseDao
    Dim sql As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetFiscalYear()
            'GetPayRoll()
            ddl_fiscalYear_SelectedIndexChanged(sender, e)
            ddl_year_SelectedIndexChanged(sender, e)
            GetDeductiveType()
            GetAddableType()
            IsEnableBasicSalaryFromManual()
        End If

    End Sub

    Protected Sub ddl_fiscalYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_fiscalYear.SelectedIndexChanged
        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        GetYear(fy(0).ToString())
    End Sub

    Private Sub GetFiscalYear()

        sql = "exec [Setup].[usp_FiscalYearMaster] @Flag='a',@BranchID='" & Session("BranchID").ToString & "'"
        Dim ds As New DataSet
        Try
            ddl_fiscalYear.Items.Clear()
            ds = Dao.ExecuteDataset(Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddl_fiscalYear.DataSource = ds.Tables(0)
                ddl_fiscalYear.DataValueField = "FID_StartDate_EndDate"
                ddl_fiscalYear.DataTextField = "FiscalYearName"
                ddl_fiscalYear.DataBind()
                ddl_fiscalYear.Items.Insert(0, New ListItem("-- Select a fiscal year -- ", ""))
            Else
                ddl_fiscalYear.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Finally
            ds.Dispose()
        End Try
    End Sub

    Private Sub GetAddableType()

        sql = "exec [Accounts].[usp_AddableType]  @Flag='s' ,@BranchID='" & Session("BranchID") & "'"
        Dim ds As New DataSet
        Try
            ddl_addablesType.Items.Clear()
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddl_addablesType.DataSource = ds.Tables(0)
                ddl_addablesType.DataValueField = "AddableTypeID"
                ddl_addablesType.DataTextField = "AddableTypeName"
                ddl_addablesType.DataBind()
                ddl_addablesType.Items.Insert(0, New ListItem("-- Select a Addable Type -- ", ""))
            Else
                ddl_addablesType.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Finally
            ds.Dispose()
        End Try
    End Sub

    Private Sub GetDeductiveType()

        sql = "exec [Accounts].[usp_DeductiveType]   @Flag='s',@BranchID='" & Session("BranchID") & "'"
        Dim ds As New DataSet
        Try
            ddl_deductiveType.Items.Clear()
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddl_deductiveType.DataSource = ds.Tables(0)
                ddl_deductiveType.DataValueField = "DeductiveTypeID"
                ddl_deductiveType.DataTextField = "DeductiveTypeName"
                ddl_deductiveType.DataBind()
                ddl_deductiveType.Items.Insert(0, New ListItem("-- Select a Deductive Type -- ", ""))
            Else
                ddl_deductiveType.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Finally
            ds.Dispose()
        End Try
    End Sub

    Private Sub GetYear(ByVal fiscalYearID As String)
        Sql = "exec [Setup].[usp_YearMaster] @Flag='p', @FiscalYearID='" & fiscalYearID & "'"
        Dim ds As New DataSet
        ddl_year.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddl_year.DataSource = ds.Tables(0)
                ddl_year.DataValueField = "YearID"
                ddl_year.DataTextField = "YearName"
                ddl_year.DataBind()
                ddl_year.Items.Insert(0, New ListItem("-- Select a year -- ", ""))
            Else
                ddl_year.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Finally
            ds.Dispose()
        End Try
    End Sub



    Private Sub GetMonthByYear(ByVal yearId As String)
        Sql = "exec [Setup].[usp_MonthMaster] @Flag='f', @YearId='" & yearId & "'"
        Dim ds As New DataSet
        ddl_month.Items.Clear()
        Try

            ds = Dao.ExecuteDataset(Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                ddl_month.DataSource = ds.Tables(0)
                ddl_month.DataValueField = "MonthDetail"
                ddl_month.DataTextField = "MonthName"
                ddl_month.DataBind()
                ddl_month.Items.Insert(0, New ListItem("-- Select A Month -- ", ""))
            Else
                ddl_month.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If

        Finally
            ds.Dispose()
        End Try
    End Sub
    Protected Sub ddl_year_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_year.SelectedIndexChanged
        GetMonthByYear(ddl_year.SelectedValue)
    End Sub


    Sub getReports(ByVal YearID As String, ByVal MonthID As String, ByVal flag As String)
        Try
            message.InnerHtml = ""

            Dim ds As New DataSet
            Dim sb As New StringBuilder
            If flag = "u" Then
                flag = "a"
            Else
                flag = "b"
            End If
            hf_monthID.Value = MonthID

            sql = "exec [Accounts].[usp_Edit_Monthly_Payroll] @flag='" & flag & "',@YearID='" & YearID & "',@MonthID='" & MonthID & "'"


            ds = Dao.ExecuteDataset(sql)
            columnsCount = ds.Tables(0).Columns.Count - 1
            If ds.Tables.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine(" <i class='fa fa-cog'></i>Modification of Addable Amount</div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;' class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
                sb.AppendLine("<table  id='displaytable' class='table table-bordered  table-condensed flip-content row-border hover order-column' style='table-layout: auto; top: 35px; left: 255px; width: 1152px !important;'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>Toggle column:")
                For c As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<a class='toggle-vis' data-column='" & c & "'> " & ds.Tables(0).Columns(c).ColumnName.ToString & "</a>")
                Next
                sb.AppendLine("</tr><hr>")
                sb.AppendLine("<tr>")

                For i As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<th>" & ds.Tables(0).Columns(i).ColumnName.ToString & "</td>")
                Next

                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim tot As Decimal

                For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    sb.AppendLine("<tr>")

                    For c As Integer = 0 To 3
                        sb.AppendLine("<td>" & ds.Tables(0).Rows(r).Item(c).ToString() & "</td>")
                    Next
                    For c As Integer = 4 To ds.Tables(0).Columns.Count - 1
                        If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(r).Item(c).ToString()) Then
                            Dim value As String() = ds.Tables(0).Rows(r).Item(c).ToString().Split("/")
                            '   sb.AppendLine("<td>" & value(0) & "</td>")
                            Dim Actions As String = ds.Tables(0).Rows(r).Item("Actions")
                            Dim Remarks As String = IIf(String.IsNullOrWhiteSpace(value(2).ToString), "", value(2).ToString)
                            If Remarks = "" Then
                                Remarks = "<a><span class='myinfo fa fa-info-circle pull-right' data-toggle='tooltip' data-placement='top' title='" & Remarks & "' data-id=" & value(1) & " data-flag='" & ddl_type.SelectedValue & "'></span></a>"
                            Else
                                Remarks = "<a><span class='myinfo myinfoActive fa fa-info-circle pull-right' data-toggle='tooltip' data-placement='top' title='" & Remarks & "' data-id=" & value(1) & " data-flag='" & ddl_type.SelectedValue & "'></span></a>"
                            End If
                            Dim temp As String = "<a data-toggle='modal' data-id='" & value(1) & "' id='" & value(1) & "' flag='" & ddl_type.SelectedValue & "' class='ClassAddableAmount' href='javascript:;' >" & value(0) & "<a><span class='glyphicon glyphicon-refresh pull-right' data-id=" & value(1) & " data-flag='" & ddl_type.SelectedValue & "'></span></a>" & Remarks
                            ' Dim temp1 As String = "<a data-toggle='modal' data-id='" & value(1) & "' id='" & value(1) & "' flag='" & ddl_type.SelectedValue & "' class='ClassAddableAmount' href='javascript:;' >" & value(0) & "<a><span class='glyphicon glyphicon-refresh pull-right' data-id=" & value(1) & " data-flag='" & ddl_type.SelectedValue & "'></span></a>" & Remarks
                            sb.AppendLine("<td>" & IIf(Actions = "Lock", value(0).ToString & Remarks, temp) & "</td>")

                        Else
                            sb.AppendLine("<td></td>")
                        End If
                    Next
                    sb.AppendLine("</tr>")
                    'If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(r).Item("Total").ToString()) Then
                    '    tot += ds.Tables(0).Rows(r).Item("Total")
                    'End If
                Next
                'sb.AppendLine("<tr>")
                'sb.AppendLine("<td colspan='" & ds.Tables(0).Columns.Count & "' align='right'>Total</td><td>" & tot & "</td>")
                'sb.AppendLine("</tr>")
            End If
            sb.AppendLine("</tbody></table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            listData.InnerHtml = sb.ToString

        Catch ex As Exception
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Some values or Parameters are missings ,<hr>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim fy() As String = ddl_month.SelectedValue.Split(",")
        getReports(ddl_year.SelectedValue, fy(0).ToString(), ddl_type.SelectedValue)
    End Sub

    Protected Sub btnUpdateSalary_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateSalary.Click
        If ddl_month.SelectedValue = "" Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Month is empty")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            Return
        End If
        If ddl_type.SelectedValue = "" Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Year is empty")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            Return
        End If
        Dim fy() As String = ddl_month.SelectedValue.Split(",")
        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""

        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec accounts.usp_udatePayrollItemBulk @Flag='s', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "'"

        Else
            sql = "exec accounts.usp_udatePayrollItemBulk @Flag='s', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "'"

        End If
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If

    End Sub

    Protected Sub btnUpdan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdan.Click
        'Dim vbmessage As String = "No Need to Update Salary right now."
        'Dim script As String = "<script type='text/javascript'> alert(" + vbmessage + ");</script>"
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertBox", script)
        'Return
        If ddl_month.SelectedValue = "" Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Month is empty")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            Return
        End If
        If ddl_type.SelectedValue = "" Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Year is empty")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            Return
        End If
        Dim fy() As String = ddl_month.SelectedValue.Split(",")
        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""
        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec accounts.usp_udatePayrollItemBulkPublicCampus @Flag='u', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "'"
        Else
            sql = "exec accounts.usp_udatePayrollItemBulk @Flag='u', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "'"

        End If

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnSanchayaKosh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSanchayaKosh.Click
        'Dim vbmessage As String = "No Need to Update Salary right now."
        'Dim script As String = "<script type='text/javascript'> alert(" + vbmessage + ");</script>"
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertBox", script)
        'Return
        If ddl_month.SelectedValue = "" Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Month is empty")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            Return
        End If
        If ddl_type.SelectedValue = "" Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Year is empty")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

            Return
        End If
        Dim fy() As String = ddl_month.SelectedValue.Split(",")
        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""

        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec accounts.usp_udatePayrollItemBulkPublicCampus @Flag='k', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "'"

        Else
            sql = "exec [Accounts].[usp_udatePayrollItemBulk] @Flag='k', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "'"

        End If
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim vbmessage As String = "No Need to Update Salary right now."
        Dim script As String = "<script type='text/javascript'> alert('" + vbmessage + "');</script>"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertBox", script)
        Return
        Dim fy() As String = ddl_month.SelectedValue.Split(",")
        sql = "exec accounts.usp_set_to_zero  @Flag='a', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@AddableTypeID='" & ddl_addablesType.SelectedValue & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Dim vbmessage As String = "No Need to Update Salary right now."
        Dim script As String = "<script type='text/javascript'> alert('" + vbmessage + "');</script>"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertBox", script)
        Return
        Dim fy() As String = ddl_month.SelectedValue.Split(",")
        sql = "exec accounts.usp_set_to_zero  @Flag='d', @MonthID='" & fy(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@DeductiveTypeID ='" & ddl_deductiveType.SelectedValue & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnUpdateIncomeTax_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateIncomeTax.Click
        'Dim vbmessage As String = "No Need to Update Salary right now."
        'Dim script As String = "<script type='text/javascript'> alert(" + vbmessage + ");</script>"
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertBox", script)
        'Return
        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        Dim mo() As String = ddl_month.SelectedValue.Split(",")
        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""


        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec accounts.usp_UpdateTaxPublicCampus  @Flag='i', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        Else
            sql = "exec accounts.usp_UpdateTax  @Flag='i', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        End If
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnUpdateSF_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateSF.Click
        'Dim vbmessage As String = "No Need to Update Salary right now."
        'Dim script As String = "<script type='text/javascript'> alert(" + vbmessage + ");</script>"
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "AlertBox", script)
        'Return

        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        Dim mo() As String = ddl_month.SelectedValue.Split(",")
        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""


        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec accounts.usp_UpdateTaxPublicCampus  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        Else
            sql = "exec accounts.usp_UpdateTax  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        End If
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btn_fiftyperAllowance_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_fiftyperAllowance.Click
        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        Dim mo() As String = ddl_month.SelectedValue.Split(",")
        'Dim ds1 As New DataSet
        'ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        'Dim sql As String = ""


        'If ds1.Tables(0).Rows(0).Item(0) = True Then
        '    sql = "exec accounts.usp_UpdateTaxPublicCampus  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        'Else
        '    sql = "exec accounts.usp_UpdateTax  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        'End If

        sql = "exec accounts.usp_UpdateFiftyAllowances  @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub


    Protected Sub btn_CIT_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_CIT.Click
        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        Dim mo() As String = ddl_month.SelectedValue.Split(",")
        'Dim ds1 As New DataSet
        'ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        'Dim sql As String = ""


        'If ds1.Tables(0).Rows(0).Item(0) = True Then
        '    sql = "exec accounts.usp_UpdateTaxPublicCampus  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        'Else
        '    sql = "exec accounts.usp_UpdateTax  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        'End If

        sql = "exec [Accounts].[usp_CIT_Update]  @Flag='s', @MonthID='" & mo(0).ToString() & "',@YearID='" & ddl_year.SelectedValue & "',@FiscalYearID ='" & fy(0).ToString() & "',@BrachID='" & Session("BranchID").ToString & "'"

        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub
    Protected Sub btn_formula_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_formula.Click
        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        Dim mo() As String = ddl_month.SelectedValue.Split(",")
        sql = "exec accounts.usp_FormulaImplement @monthid='" & mo(0).ToString() & "',@FiscalYearID ='" & fy(0).ToString() & "',@BranchID='" & Session("BranchID").ToString & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btn_updategrade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_UpdateGrade.Click
        Dim fy() As String = ddl_fiscalYear.SelectedValue.Split(",")
        Dim mo() As String = ddl_month.SelectedValue.Split(",")
        sql = "exec [Accounts].[usp_GradeUpdateAutomaticForTU] @monthid='" & mo(0).ToString() & "',@FiscalYearID ='" & fy(0).ToString() & "',@BranchID='" & Session("BranchID").ToString & "', @UserID='" & Session("UserID").ToString & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            '  btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub


    Protected Sub btn_Save_BasicSalary_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Save_BasicSalary.Click
        sql = "exec accounts.ups_UpdateFromManualBasicSalary @flag='u', @Value='" & IIf(ch_IsManual.Checked, 1, 0) & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            '  btnSubmit_Click(Nothing, Nothing)
            IsEnableBasicSalaryFromManual()
        End If

    End Sub
    Sub IsEnableBasicSalaryFromManual()
        sql = "exec accounts.ups_UpdateFromManualBasicSalary @flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ch_IsManual.Checked = ds.Tables(0).Rows(0)(0)
            If ch_IsManual.Checked Then
                Label1.Text = "Manual Basic Salary update is enable."
            Else
                Label1.Text = "Manual Basic Salary update is disable."
            End If
        End If
    End Sub

End Class