﻿Public Class SalaryVoucherApproval
    Inherits System.Web.UI.Page

    Private Dao As New DatabaseDao
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getSalaryVoucher()
        End If
    End Sub


    Protected Sub getSalaryVoucher()
        Try
            Message.InnerHtml = ""
            Dim ds As New DataSet
            Dim sb As New StringBuilder
            Dim sql As String
            sql = "exec accounts.usp_MainPayrollVoucher @UserID='" & Session("UserID") & "',@branchID='" & Session("BranchID") & "'"
            sql += ",@FiscalYearID='" & Session("FiscalYearID") & "'"

            ds = Dao.ExecuteDataset(Sql)
            columnsCount = ds.Tables(0).Columns.Count - 1
            If ds.Tables.Count > 0 Then
                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>SN</th>")
                For i As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<th>" & ds.Tables(0).Columns(i).ColumnName.ToString & "</td>")
                Next
                sb.AppendLine("</tr>")

                For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td>" & r + 1 & "</td>")
                    For c As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        sb.AppendLine("<td>" & ds.Tables(0).Rows(r).Item(c).ToString() & "</td>")
                    Next
                    sb.AppendLine("</tr>")

                Next

            End If
            sb.AppendLine("</tbody></table>")
            listData.InnerHtml = sb.ToString

        Catch ex As Exception
            Dim sb As New StringBuilder
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Some values or Parameters are missings ,<hr>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            Message.InnerHtml = sb.ToString
        End Try
    End Sub

End Class