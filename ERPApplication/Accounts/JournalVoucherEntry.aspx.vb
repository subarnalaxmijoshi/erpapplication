﻿Imports System.Globalization

Public Class JournalVoucherEntry
    Inherits System.Web.UI.Page


    Private ReadOnly _dao As New DatabaseDao
    Private ReadOnly _num2Word As New Number2Word

    Private _sql, _jvId, _userId As String
    Dim DC As New DateConverter()
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            Response.Redirect(redirectUrl)
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
        Else
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
        End If


    End Sub


    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    If String.IsNullOrWhiteSpace(Session("userID")) And
    '              String.IsNullOrWhiteSpace(Session("username")) And
    '              String.IsNullOrWhiteSpace(Session("Role")) And
    '              String.IsNullOrWhiteSpace(Session("Branch")) Then
    '        Response.Redirect("/Login.aspx")
    '    End If

    '    _userId = Session("username").ToString()
    '    If Not IsPostBack Then
    '        'GetAccountHead()
    '        GetAccountGroup()
    '        GetVoucherType()
    '        ''ddlGroup_SelectedIndexChanged(e, Nothing)
    '        'ddlSubGroup_SelectedIndexChanged(e, Nothing)
    '        'GetJournalVoucher(Session.SessionID, _userId)
    '        If Not String.IsNullOrWhiteSpace(Request.QueryString("jvid")) Then
    '            _jvId = Request.QueryString("jvid")
    '            DeleteJournalVoucher(Me._jvId)

    '        End If
    '        If GetisBSDate() = "Y" Then
    '            Dim arr() As String = eng_to_nep(Date.Now.Year, Date.Now.Month, Date.Now.Day).Split("/")
    '            Dim yy = arr(0)
    '            Dim mm = arr(1)
    '            Dim dd = arr(2)
    '            txt_VourcherDate.Text = dd & "/" & mm & "/" & yy
    '        Else
    '            txt_VourcherDate.Text = Now.ToShortDateString
    '        End If
    '        ddl_voucherType.Items.FindByValue("1").Selected = True
    '        ddlSubGroup.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
    '        ddlAccountHead.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
    '        getSummary()
    '    End If

    'End Sub
    'Protected Function GetisBSDate() As String
    '    Return System.Configuration.ConfigurationManager.AppSettings("isBSDate").ToString
    'End Function
    'Protected Sub BtnAddClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
    '    Dim dR As Decimal
    '    If (txtDR.Text.Trim() <> String.Empty) Then
    '        dR = Convert.ToDecimal(txtDR.Text.Trim())
    '    End If

    '    Dim cR As Decimal
    '    If (txtCR.Text.Trim() <> String.Empty) Then
    '        cR = Convert.ToDecimal(txtCR.Text)
    '    End If
    '    SaveJournalVourcher(ddlAccountHead.SelectedValue.ToString(), dR, cR, txtNarration.Text.Trim(), Session.SessionID, Me._userId)
    'End Sub

    'Protected Sub BtnOldClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnOld.Click
    '    GetOldJournalVoucher(Me._userId)
    'End Sub

    'Protected Sub BtnCopyClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnCopy.Click
    '    CopyToCurrent(Me._userId, Session.SessionID)
    'End Sub



    'Protected Sub BtnSubmitClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
    '    SubmitJournalVoucher(Session.SessionID, Session("userID").ToString, txtNarration.Text.Trim(), Session("Branch").ToString)
    'End Sub

    'Sub getSummary()
    '    _sql = "exec [Accounts].[usp_TempJournalVourcher] @flag='r',@CreatedBy='" & Session("userID").ToString & "'"
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        Dim text As String = "Today summary (DR :" & ds.Tables(0).Rows(0).Item("DR").ToString & " CR : " & ds.Tables(0).Rows(0).Item("CR").ToString & " Total Vourcher Entry : " & ds.Tables(0).Rows(0).Item("Total").ToString & ")"
    '        summary.InnerText = text
    '    End If
    'End Sub
    'Private Sub GetAccountHead(ByVal GroupID As String, ByVal SubGroupID As String)
    '    ddlAccountHead.ClearSelection()
    '    _sql = "EXEC [Accounts].[usp_Account] @Flag='k',@GroupID='" & GroupID & "', @SubGroupID='" & SubGroupID & "'"
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        ddlAccountHead.DataSource = ds.Tables(0)
    '        ddlAccountHead.DataValueField = "AccountId"
    '        ddlAccountHead.DataTextField = "AccountName"
    '        ddlAccountHead.DataBind()
    '        ddlAccountHead.Items.Insert(0, New WebControls.ListItem("-- Select Account Head -- ", ""))
    '    Else
    '        ddlAccountHead.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub
    'Private Sub GetAccountGroup()
    '    _sql = "EXEC [Accounts].[usp_group] @Flag='s'"
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        ddlGroup.DataSource = ds.Tables(0)
    '        ddlGroup.DataValueField = "GroupID"
    '        ddlGroup.DataTextField = "GroupName"
    '        ddlGroup.DataBind()
    '        ddlGroup.Items.Insert(0, New WebControls.ListItem("-- Select Group Head -- ", ""))
    '    Else
    '        ddlGroup.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub



    'Private Sub GetVoucherType()
    '    _sql = "EXEC [Accounts].[usp_VourcherType]  @Flag='s'"
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        ddl_voucherType.DataSource = ds.Tables(0)
    '        ddl_voucherType.DataValueField = "VourcherTypeID"
    '        ddl_voucherType.DataTextField = "VourcherName"
    '        ddl_voucherType.DataBind()
    '        ddl_voucherType.Items.Insert(0, New WebControls.ListItem("-- Select Voucher Type -- ", ""))
    '    Else
    '        ddl_voucherType.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub
    'Private Sub GetAccountSubgroup(ByVal GroupID As String)
    '    ddlAccountHead.ClearSelection()
    '    _sql = "EXEC [Accounts].[usp_Account] @Flag='j',@GroupID='" & GroupID & "'"
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        ddlSubGroup.DataSource = ds.Tables(0)
    '        ddlSubGroup.DataValueField = "SubGroupID"
    '        ddlSubGroup.DataTextField = "SubGroupName"
    '        ddlSubGroup.DataBind()
    '        ddlSubGroup.Items.Insert(0, New WebControls.ListItem("-- Select Subgroup Head -- ", ""))
    '    Else
    '        ddlSubGroup.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub
    'Private Sub SaveJournalVourcher(ByVal accountHead As String, ByVal dR As String, ByVal cR As String, ByVal narration As String,
    '                                ByVal sessionId As String, ByVal userId As String)
    '    _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='i', @AccountHead='" & accountHead & "', @DR='" & dR & "', @CR='" & cR & "', @SessionID='" & sessionId & "', @UserName='" & userId & "', @Narration='" & narration & "'"
    '    Dim sb As New StringBuilder("")
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        Dim msgClass As String = ""
    '        If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "1" Then
    '            msgClass = "note note-success"
    '        ElseIf ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "0" Then
    '            msgClass = "note note-danger"
    '        End If
    '        sb.AppendLine("<div class='" & msgClass & "'>")
    '        sb.AppendLine("<div class='close-note'>x</div>")
    '        sb.AppendLine("<p>")
    '        sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
    '        sb.AppendLine("</p>")
    '        sb.AppendLine("</div>")
    '        message.InnerHtml = sb.ToString
    '        ClearField()
    '        ClientScript.RegisterStartupScript(Me.GetType(), "hide2", "HideMessage();", True)
    '        GetJournalVoucher(Session.SessionID, userId)
    '    End If

    'End Sub

    'Private Sub DeleteJournalVoucher(ByVal jvId As String)
    '    _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='d', @JournalVourcherID='" & jvId & "'"
    '    Dim sb As New StringBuilder("")
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables.Count > 0 Then
    '        Dim msgClass As String = ""
    '        If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "1" Then
    '            msgClass = "note note-success"
    '        ElseIf ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "0" Then
    '            msgClass = "note note-danger"
    '        End If
    '        sb.AppendLine("<div class='" & msgClass & "'>")
    '        sb.AppendLine("<div class='close-note'>x</div>")
    '        sb.AppendLine("<p>")
    '        sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
    '        sb.AppendLine("</p>")
    '        sb.AppendLine("</div>")
    '        message.InnerHtml = sb.ToString
    '        GetJournalVoucher(Session.SessionID, _userId)
    '    End If
    'End Sub

    'Private Sub GetJournalVoucher(ByVal sessionId As String, ByVal userID As String)
    '    listData.InnerHtml = ""
    '    _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='s', @SessionID='" & sessionId & "', @UserName='" & userID & "'"
    '    Dim ds As New DataSet
    '    Dim sb As New StringBuilder("")
    '    Dim dRSum As Decimal = 0, cRSum As Decimal = 0
    '    Dim i As Integer = 0
    '    ds = _dao.ExecuteDataset(_sql)

    '    sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
    '    sb.AppendLine("<thead class='flip-content'>")
    '    sb.AppendLine("<tr>")
    '    sb.AppendLine("<th>S.N</th>")
    '    sb.AppendLine("<th>Account Name</th>")
    '    sb.AppendLine("<th>DR</th>")
    '    sb.AppendLine("<th>CR</th>")

    '    sb.AppendLine("<th style='text-align:center'>Action</th>")
    '    sb.AppendLine("</tr>")
    '    sb.AppendLine("</thead>")
    '    sb.AppendLine("<tbody>")

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each row As DataRow In ds.Tables(0).Rows
    '            Dim journalVourcherId = ds.Tables(0).Rows(i).Item("JournalVourcherID").ToString
    '            Dim accountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
    '            Dim dr = ds.Tables(0).Rows(i).Item("DR").ToString()
    '            Dim cr = ds.Tables(0).Rows(i).Item("CR").ToString()

    '            sb.AppendLine("<tr>")
    '            sb.AppendLine("<td>" & (i + 1) & "</td>")
    '            sb.AppendLine("<td>" & accountName & "</td>")
    '            sb.AppendLine("<td>" & dr & "</td>")
    '            sb.AppendLine("<td>" & cr & "</td>")
    '            sb.AppendLine("<td style='text-align:center'><a href='/Accounts/JournalVoucherEntry.aspx?jvid=" & journalVourcherId & "'> Delete </a></td>")
    '            sb.AppendLine("</tr>")

    '            dr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("DR").ToString()), 0, ds.Tables(0).Rows(i).Item("DR").ToString)
    '            cr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("CR").ToString()), 0, ds.Tables(0).Rows(i).Item("CR").ToString)
    '            dr = Convert.ToDecimal(dr).ToString("0,0.00", CultureInfo.InvariantCulture)
    '            cr = Convert.ToDecimal(cr).ToString("0,0.00", CultureInfo.InvariantCulture)

    '            dRSum += dr
    '            cRSum += cr

    '            i += 1
    '        Next
    '        sb.AppendLine("<tr><td colspan='2'><b>Total</b></td><td><b>" & dRSum.ToString("0,0.00", CultureInfo.InvariantCulture) &
    '                      "</b></td><td><b>" & cRSum.ToString("0,0.00", CultureInfo.InvariantCulture) & "</b></td><td></td></tr>")


    '        If (dRSum = cRSum) Then
    '            btnSubmit.Enabled = True
    '            sb.AppendLine("<tr><td colspan='5'><b> In Words : " & _num2Word.ConvertNumberToRupees(dRSum.ToString()) & "</b></td></tr>")
    '        Else
    '            btnSubmit.Enabled = False
    '        End If

    '        sb.AppendLine("</tbody>")
    '        sb.AppendLine("</table>")
    '        listData.InnerHtml = sb.ToString()
    '    End If
    'End Sub

    'Private Sub GetOldJournalVoucher(ByVal userID As String)
    '    recentJournalVourcher.InnerHtml = ""
    '    _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='re', @UserName='" & userID & "'"
    '    Dim ds As New DataSet
    '    Dim sb As New StringBuilder("")
    '    Dim dRSum As Decimal = 0, cRSum As Decimal = 0
    '    Dim i As Integer = 0
    '    ds = _dao.ExecuteDataset(_sql)

    '    sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
    '    sb.AppendLine("<thead class='flip-content'>")
    '    sb.AppendLine("<tr>")
    '    sb.AppendLine("<th>S.N</th>")
    '    sb.AppendLine("<th>Account Name</th>")
    '    sb.AppendLine("<th>DR</th>")
    '    sb.AppendLine("<th>CR</th>")

    '    sb.AppendLine("<th style='text-align:center'>Action</th>")
    '    sb.AppendLine("</tr>")
    '    sb.AppendLine("</thead>")
    '    sb.AppendLine("<tbody>")

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each row As DataRow In ds.Tables(0).Rows
    '            Dim journalVourcherId = ds.Tables(0).Rows(i).Item("JournalVourcherID").ToString
    '            Dim accountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
    '            Dim dr = ds.Tables(0).Rows(i).Item("DR").ToString()
    '            Dim cr = ds.Tables(0).Rows(i).Item("CR").ToString()

    '            sb.AppendLine("<tr>")
    '            sb.AppendLine("<td>" & (i + 1) & "</td>")
    '            sb.AppendLine("<td>" & accountName & "</td>")
    '            sb.AppendLine("<td>" & dr & "</td>")
    '            sb.AppendLine("<td>" & cr & "</td>")
    '            sb.AppendLine("<td style='text-align:center'><a href='/Accounts/JournalVoucherEntry.aspx?jvid=" & journalVourcherId & "'> Delete </a></td>")
    '            sb.AppendLine("</tr>")

    '            dr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("DR").ToString()), 0, ds.Tables(0).Rows(i).Item("DR").ToString)
    '            cr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("CR").ToString()), 0, ds.Tables(0).Rows(i).Item("CR").ToString)
    '            dr = Convert.ToDecimal(dr).ToString("0,0.00", CultureInfo.InvariantCulture)
    '            cr = Convert.ToDecimal(cr).ToString("0,0.00", CultureInfo.InvariantCulture)

    '            dRSum += dr
    '            cRSum += cr

    '            i += 1
    '        Next
    '        sb.AppendLine("<tr><td colspan='2'><b>Total</b></td><td><b>" & dRSum.ToString("0,0.00", CultureInfo.InvariantCulture) &
    '                      "</b></td><td><b>" & cRSum.ToString("0,0.00", CultureInfo.InvariantCulture) & "</b></td><td></td></tr>")
    '        sb.AppendLine("</tbody>")
    '        sb.AppendLine("</table>")
    '        recentJournalVourcher.InnerHtml = sb.ToString()

    '        If (dRSum = cRSum) Then
    '            btnSubmit.Enabled = True
    '        Else
    '            btnSubmit.Enabled = False
    '        End If

    '    Else
    '        recentJournalVourcher.InnerHtml = "<h2>There are no Recent Journal Voucher Entries recorded by you.</h2>"
    '    End If
    '    ClientScript.RegisterStartupScript(Me.GetType(), "showmodal", "$('.config').click();", True)
    '    ClientScript.RegisterStartupScript(Me.GetType(), "hide3", "HideMessage();", True)
    'End Sub

    'Private Sub CopyToCurrent(ByVal uid As String, ByVal newsessionId As String)
    '    _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='copy', @UserName='" & uid & "', @SessionID='" & newsessionId & "'"
    '    Dim ds As New DataSet
    '    Dim sb As New StringBuilder("")
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        sb.AppendLine("<div class='note note-success'>")
    '        sb.AppendLine("<div class='close-note'>x</div>")
    '        sb.AppendLine("<p>")
    '        sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
    '        sb.AppendLine("</p>")
    '        sb.AppendLine("</div>")
    '        message.InnerHtml = sb.ToString
    '        ClientScript.RegisterStartupScript(Me.GetType(), "hide5", "HideMessage();", True)
    '    End If
    '    GetJournalVoucher(Session.SessionID, Me._userId)
    'End Sub

    'Private Sub ClearField()
    '    ddlAccountHead.SelectedIndex = 0
    '    txtDR.Text = String.Empty
    '    txtCR.Text = String.Empty
    '    txtNarration.Text = String.Empty
    'End Sub

    'Private Sub SubmitJournalVoucher(ByVal sId As String, ByVal uid As String, ByVal narration As String, branch As String)
    '    _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='js', @UserName='" & uid & "', @SessionID='" & sId & "', @Narration='" & narration & "',@VoucherDate='" & Helper.GetFrontToDbDate(txt_VourcherDate.Text.ToString) & "',@VoucherTypeID='" & ddl_voucherType.SelectedValue.ToString & "',@BranchID='" & branch & "'"
    '    Dim msgClass As String
    '    Dim sb As New StringBuilder("")
    '    Dim ds As New DataSet
    '    ds = _dao.ExecuteDataset(_sql)
    '    If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("errorcode") = "1" Then
    '        txtNarration.Text = String.Empty
    '        msgClass = "note note-success"
    '    ElseIf ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("errorcode") = "0" Then
    '        msgClass = "note note-danger"
    '    End If
    '    sb.AppendLine("<div class='" & msgClass & "'>")
    '    sb.AppendLine("<div class='close-note'>x</div>")
    '    sb.AppendLine("<p>")
    '    sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
    '    sb.AppendLine("</p>")
    '    sb.AppendLine("</div>")
    '    message.InnerHtml = sb.ToString

    '    GetJournalVoucher(Session.SessionID, Me._userId)
    'End Sub

    'Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGroup.SelectedIndexChanged
    '    GetAccountSubgroup(ddlGroup.SelectedValue.ToString)
    'End Sub
    'Protected Sub ddlSubGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSubGroup.SelectedIndexChanged
    '    GetAccountHead(ddlGroup.SelectedValue.ToString, ddlSubGroup.SelectedValue.ToString)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrWhiteSpace(Session("userID")) And
                  String.IsNullOrWhiteSpace(Session("username")) And
                  String.IsNullOrWhiteSpace(Session("Role")) And
                  String.IsNullOrWhiteSpace(Session("Branch")) Then
            Response.Redirect("/Login.aspx")
        End If

        _userId = Session("userID").ToString()
        If Not IsPostBack Then
            'GetAccountHead()
            GetAccountGroup()
            GetVoucherType()
            ''ddlGroup_SelectedIndexChanged(e, Nothing)
            'ddlSubGroup_SelectedIndexChanged(e, Nothing)
            'GetJournalVoucher(Session.SessionID, _userId)
            If Not String.IsNullOrWhiteSpace(Request.QueryString("jvid")) Then
                _jvId = Request.QueryString("jvid")
                'DeleteJournalVoucher(Me._jvId)

            End If
            'If GetisBSDate() = "Y" Then
            '    Dim arr() As String = eng_to_nep(Date.Now.Year, Date.Now.Month, Date.Now.Day).Split("/")
            '    Dim yy = arr(0)
            '    Dim mm = arr(1)
            '    Dim dd = arr(2)
            '    txt_VourcherDate.Text = dd & "/" & mm & "/" & yy
            'Else
            '    txt_VourcherDate.Text = Now.ToShortDateString
            'End If

            txt_VourcherDate.Text = Today.ToString("dd MMM yyyy")
            txt_VourcherDate_Nep.Text = DC.ToBS(Convert.ToDateTime(txt_VourcherDate.Text.Trim), "n")

            '  ddl_voucherType.Items.FindByValue("1").Selected = True
            ddlSubGroup.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
            ddlAccountHead.Items.Insert(0, New WebControls.ListItem("-- No Account Found -- ", ""))
            ddlSubAccount.Items.Insert(0, New WebControls.ListItem("-- No Account Found -- ", ""))
            getSummary()
            ddl_voucherType.Items(1).Selected = True

        End If
        '   ddl_voucherType.Items.FindByValue(1).Selected = True
    End Sub

    Protected Function getSessionUserID() As String
        Return Session("userID").ToString
    End Function


    Protected Function getSessionID() As String
        Return Session.SessionID
    End Function


    Protected Function GetisBSDate() As String
        Return System.Configuration.ConfigurationManager.AppSettings("isBSDate").ToString
    End Function

    Protected Sub BtnAddClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim dR As Decimal
        If (txtDR.Text.Trim() <> String.Empty) Then
            dR = Convert.ToDecimal(txtDR.Text.Trim())
        End If

        Dim cR As Decimal
        If (txtCR.Text.Trim() <> String.Empty) Then
            cR = Convert.ToDecimal(txtCR.Text)
        End If
        Dim voucherDate As String
        voucherDate = txt_VourcherDate.Text.ToString
        Dim voucherAmount As Decimal = dR + cR
        If (CheckIfBudgetExceeds(ddlSubGroup.SelectedValue, voucherAmount.ToString.Trim) = False) Then
            SaveJournalVourcher(ddlAccountHead.SelectedValue.ToString(), dR, cR, txtNarration.Text.Trim(), Session.SessionID, Me._userId, voucherDate, ddlSubAccount.SelectedValue.ToString())
        Else
            btnSubmit.Enabled = False
        End If

    End Sub

    Protected Sub BtnOldClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnOld.Click
        GetOldJournalVoucher(Me._userId)
    End Sub

    Protected Sub BtnCopyClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnCopy.Click
        CopyToCurrent(Me._userId, Session.SessionID)
    End Sub



    Protected Sub BtnSubmitClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

        'Dim arr() As String = txtNarration.Text.Split(Chr(13))
        'Dim narration, narr As String
        'For i As Integer = 0 To arr.Length - 1
        '    If (arr.Length < 2 Or i = (arr.Length - 1)) Then
        '        narr = arr(i)
        '    Else
        '        narr = arr(i) & ","
        '    End If
        '    narration = narration & narr
        'Next

        SubmitJournalVoucher(Session.SessionID, Session("userID").ToString, txtNarration.Text.Trim(), Session("Branch").ToString, Guid.NewGuid().ToString(), txt_cheque.Text, txt_voucherNo.Text)
    End Sub

    Sub getSummary()
        _sql = "exec [Accounts].[usp_TempJournalVourcher] @flag='rn',@CreatedBy='" & Session("userID").ToString & "'"
        Dim ds As New DataSet
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables.Count > 0 Then
            Dim text As String = "Today summary (DR :" & ds.Tables(0).Rows(0).Item("DR").ToString & " CR : " & ds.Tables(0).Rows(0).Item("CR").ToString & " Total Vourcher Entry (Not Verified only) : " & ds.Tables(0).Rows(0).Item("Total").ToString & ")"
            summary.InnerText = text
        End If
    End Sub
    Private Sub GetAccountHead(ByVal GroupID As String, ByVal SubGroupID As String)
        ddlAccountHead.Items.Clear()
        ddlAccountHead.ClearSelection()
        _sql = "EXEC [Accounts].[usp_Account] @Flag='k',@GroupID='" & GroupID & "', @SubGroupID='" & SubGroupID & "'"
        Dim ds As New DataSet
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlAccountHead.DataSource = ds.Tables(0)
            ddlAccountHead.DataValueField = "AccountId"
            ddlAccountHead.DataTextField = "AccountName"
            ddlAccountHead.DataBind()
            ddlAccountHead.Items.Insert(0, New WebControls.ListItem("-- Select Account Head -- ", ""))
        Else
            ddlAccountHead.Items.Insert(0, New WebControls.ListItem("-- No Account Found -- ", ""))
        End If
    End Sub
    Private Sub GetSubAccountHead(ByVal AccountID As String)
        ddlSubAccount.Items.Clear()
        ddlSubAccount.ClearSelection()
        _sql = "EXEC [Accounts].[usp_SubAccount] @flag='s',@AccountID='" & AccountID & "'"
        Dim ds As New DataSet
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlSubAccount.DataSource = ds.Tables(0)
            ddlSubAccount.DataValueField = "SubAccountID"
            ddlSubAccount.DataTextField = "SubAccountHead"
            ddlSubAccount.DataBind()
            ddlSubAccount.Items.Insert(0, New WebControls.ListItem("-- Select Sub-Account Head -- ", ""))
        Else
            ddlSubAccount.Items.Insert(0, New WebControls.ListItem("-- No Account Found -- ", ""))
        End If
    End Sub

    Private Sub GetAccountGroup()
        _sql = "EXEC [Accounts].[usp_group] @Flag='v',@BranchID=" & Session("BranchID")
        Dim ds As New DataSet
        ds = _dao.ExecuteDataset(_sql)
        ddlGroup.Items.Clear()
        If ds.Tables.Count > 0 Then
            ddlGroup.DataSource = ds.Tables(0)
            ddlGroup.DataValueField = "GroupID"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
            ddlGroup.Items.Insert(0, New WebControls.ListItem("-- Select Group Head -- ", ""))
        Else
            ddlGroup.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
        End If
    End Sub



    Private Sub GetVoucherType()
        _sql = "EXEC [Accounts].[usp_VourcherType]  @Flag='s'"
        Dim ds As New DataSet
        ddl_voucherType.Items.Clear()
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables.Count > 0 Then
            ddl_voucherType.DataSource = ds.Tables(0)
            ddl_voucherType.DataValueField = "VourcherTypeID"
            ddl_voucherType.DataTextField = "VourcherName"
            ddl_voucherType.DataBind()
            ddl_voucherType.Items.Insert(0, New WebControls.ListItem("-- Select Voucher Type -- ", ""))
        Else
            ddl_voucherType.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
        End If
    End Sub
    Private Sub GetAccountSubgroup(ByVal GroupID As String)
        '  ddlAccountHead.ClearSelection()
        _sql = "EXEC [Accounts].[usp_Account] @Flag='j',@GroupID='" & GroupID & "'"
        Dim ds As New DataSet
        ddlSubGroup.Items.Clear()
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables.Count > 0 Then
            ddlSubGroup.DataSource = ds.Tables(0)
            ddlSubGroup.DataValueField = "SubGroupID"
            ddlSubGroup.DataTextField = "SubGroupName"
            ddlSubGroup.DataBind()
            ddlSubGroup.Items.Insert(0, New WebControls.ListItem("-- Select Subgroup Head -- ", ""))
        Else
            ddlSubGroup.Items.Insert(0, New WebControls.ListItem("-- No Record Found -- ", ""))
        End If
    End Sub
    Private Sub SaveJournalVourcher(ByVal accountHead As String, ByVal dR As String, ByVal cR As String, ByVal narration As String,
                                    ByVal sessionId As String, ByVal userId As String, ByVal voucherDate As String, ByVal subaccountHead As String)
        _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='i', @AccountHead=N'" & accountHead & "', @DR=N'" & dR & "', @CR=N'" & cR & "', @SessionID='" & sessionId & "', @UserName=N'" & userId & "', @Narration=N'" & narration & "',@VoucherDate=N'" & voucherDate & "'"
        _sql += ",@CompanyID= '" & Session("CompanyID") & "',@BranchID= '" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "',@AcademicYearID='" & Session("AcademicYearID") & "'"
        If Not String.IsNullOrWhiteSpace(subaccountHead) Then
            _sql += ",@SubAccountHead='" & subaccountHead & "'"
        End If

        message.InnerHtml = ""
        Dim sb As New StringBuilder("")
        Dim ds As New DataSet
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables.Count > 0 Then
            Dim msgClass As String = ""
            If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "1" Then
                msgClass = "note note-success"
            ElseIf ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "0" Then
                msgClass = "note note-danger"
            End If
            sb.AppendLine("<div class='" & msgClass & "'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            ClearField()
            ClientScript.RegisterStartupScript(Me.GetType(), "hide2", "HideMessage();", True)
            GetJournalVoucher(Session.SessionID, userId)
        End If

    End Sub

    Private Sub DeleteJournalVoucher(ByVal jvId As String)
        _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='d', @JournalVourcherID='" & jvId & "'"
        Dim sb As New StringBuilder("")
        Dim ds As New DataSet
        message.InnerHtml = ""
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables.Count > 0 Then
            Dim msgClass As String = ""
            If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "1" Then
                msgClass = "note note-success"
            ElseIf ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("Errorcode") = "0" Then
                msgClass = "note note-danger"
            End If
            sb.AppendLine("<div class='" & msgClass & "'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            GetJournalVoucher(Session.SessionID, _userId)
        End If
    End Sub

    Private Sub GetJournalVoucher(ByVal sessionId As String, ByVal userID As String)
        listData.InnerHtml = ""

        _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='s', @SessionID='" & sessionId & "', @UserName='" & userID & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Dim dRSum As Decimal = 0, cRSum As Decimal = 0, balance As Decimal = 0, balanceTag As String
        Dim i As Integer = 0
        ds = _dao.ExecuteDataset(_sql)

        sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
        sb.AppendLine("<thead class='flip-content'>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<th>S.N</th>")
        sb.AppendLine("<th>Account Name</th>")
        sb.AppendLine("<th>DR</th>")
        sb.AppendLine("<th>CR</th>")
        sb.AppendLine("<th>Adv_Exp_Code</th>")
        sb.AppendLine("<th style='text-align:center'>Action</th>")
        sb.AppendLine("<th>Notes</th>")
        sb.AppendLine("</tr>")
        sb.AppendLine("</thead>")
        sb.AppendLine("<tbody>")

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                Dim journalVourcherId = ds.Tables(0).Rows(i).Item("JournalVourcherID").ToString
                Dim accountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
                Dim dr = ds.Tables(0).Rows(i).Item("DR").ToString()
                Dim cr = ds.Tables(0).Rows(i).Item("CR").ToString()
                Dim notes = ds.Tables(0).Rows(i).Item("Notes").ToString().Trim
                Dim subaccountHead = ds.Tables(0).Rows(i).Item("SubAccountHead").ToString().Trim
                Dim ledgerCode = ds.Tables(0).Rows(i).Item("LedgerCode").ToString().Trim

                sb.AppendLine("<tr>")
                sb.AppendLine("<td>" & (i + 1) & "</td>")
                sb.AppendLine("<td>" & accountName & " ")
                If Not String.IsNullOrWhiteSpace(subaccountHead) Then
                    sb.AppendLine("( " & subaccountHead & " )")
                End If
                sb.AppendLine("</td>")
                sb.AppendLine("<td><a data-toggle='modal' data-id='" & journalVourcherId & "' data-modify='DR' id='editDR' class='EditAmount' href='javascript:;'> " & dr & "</a></td>")
                sb.AppendLine("<td><a data-toggle='modal' data-id='" & journalVourcherId & "' data-modify='CR' id='editCR' class='EditAmount' href='javascript:;'> " & cr & "</a></td>")
                sb.AppendLine("<td><a data-toggle='modal' data-id='" & journalVourcherId & "' data-modify='LedgerCode'  id='editLedgerCode' class='EditAmount' href='javascript:;'>" & ledgerCode & "</a> </td>")

                'sb.AppendLine("<td style='text-align:center'><a href='/Accounts/JournalVoucherEntry.aspx?jvid=" & journalVourcherId & "'> Delete </a></td>")
                sb.AppendLine("<td style='text-align:center'><a style='cursor:pointer' data-id='" & journalVourcherId & "' class='DeleteJV'> Delete </a></td>")
                If (String.IsNullOrWhiteSpace(notes)) Then
                    sb.AppendLine("<td><a data-toggle='modal' data-id='" & journalVourcherId & "' data-text='" & notes & "' id='addnotes' class='AddNotes' href='#AddNotes'> Add </a></td>")
                Else
                    sb.AppendLine("<td><a data-toggle='modal' data-id='" & journalVourcherId & "' data-text='" & notes & "' id='addnotes' class='AddNotes' href='#AddNotes'> " & notes & "</a></td>")
                End If

                sb.AppendLine("</tr>")

                dr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("DR").ToString()), 0, ds.Tables(0).Rows(i).Item("DR").ToString)
                cr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("CR").ToString()), 0, ds.Tables(0).Rows(i).Item("CR").ToString)
                dr = Convert.ToDecimal(dr).ToString("0,0.00", CultureInfo.InvariantCulture)
                cr = Convert.ToDecimal(cr).ToString("0,0.00", CultureInfo.InvariantCulture)

                dRSum += dr
                cRSum += cr

                i += 1
            Next

            balance = Math.Abs(dRSum - cRSum)
            If Not (dRSum = cRSum) Then
                If (dRSum > cRSum) Then
                    balanceTag = " DR"
                Else
                    balanceTag = " CR"
                End If
            End If

            sb.AppendLine("<tr><td colspan='2'><b>Total</b></td><td><b>" & dRSum.ToString("0,0.00", CultureInfo.InvariantCulture) &
                          "</b></td><td><b>" & cRSum.ToString("0,0.00", CultureInfo.InvariantCulture) & "</b></td>" &
                          "<td><b>Balance : " & balance.ToString("0,0.00", CultureInfo.InvariantCulture) & " " & balanceTag & "</b></td></tr>")


            If (dRSum = cRSum) Then
                btnSubmit.Enabled = True
                sb.AppendLine("<tr><td colspan='7'><b> In Words : " & _num2Word.ConvertNumberToRupees(dRSum.ToString()) & "</b></td></tr>")
            Else
                btnSubmit.Enabled = False
                sb.AppendLine("<tr><td colspan='7' style='color:red;font-size:14pt;'><b>DR and CR sum is not balanced.</b></td></tr>")
            End If

            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            listData.InnerHtml = sb.ToString()
        End If
    End Sub

    Private Sub GetOldJournalVoucher(ByVal userID As String)
        recentJournalVourcher.InnerHtml = ""
        _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='re', @UserName='" & userID & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Dim dRSum As Decimal = 0, cRSum As Decimal = 0
        Dim i As Integer = 0
        ds = _dao.ExecuteDataset(_sql)

        sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
        sb.AppendLine("<thead class='flip-content'>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<th>S.N</th>")
        sb.AppendLine("<th>Account Name</th>")
        sb.AppendLine("<th>DR</th>")
        sb.AppendLine("<th>CR</th>")

        sb.AppendLine("<th style='text-align:center'>Action</th>")
        sb.AppendLine("</tr>")
        sb.AppendLine("</thead>")
        sb.AppendLine("<tbody>")

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                Dim journalVourcherId = ds.Tables(0).Rows(i).Item("JournalVourcherID").ToString
                Dim accountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
                Dim dr = ds.Tables(0).Rows(i).Item("DR").ToString()
                Dim cr = ds.Tables(0).Rows(i).Item("CR").ToString()
                Dim subaccountHead = ds.Tables(0).Rows(i).Item("SubAccountHead").ToString().Trim

                sb.AppendLine("<tr>")
                sb.AppendLine("<td>" & (i + 1) & "</td>")
                sb.AppendLine("<td>" & accountName & " ")
                If Not String.IsNullOrWhiteSpace(subaccountHead) Then
                    sb.AppendLine("( " & subaccountHead & " )")
                End If
                sb.AppendLine("</td>")
                sb.AppendLine("<td>" & dr & "</td>")
                sb.AppendLine("<td>" & cr & "</td>")
                sb.AppendLine("<td style='text-align:center'><a href='/Accounts/JournalVoucherEntry.aspx?jvid=" & journalVourcherId & "'> Delete </a></td>")
                sb.AppendLine("</tr>")

                dr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("DR").ToString()), 0, ds.Tables(0).Rows(i).Item("DR").ToString)
                cr = IIf(String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("CR").ToString()), 0, ds.Tables(0).Rows(i).Item("CR").ToString)
                dr = Convert.ToDecimal(dr).ToString("0,0.00", CultureInfo.InvariantCulture)
                cr = Convert.ToDecimal(cr).ToString("0,0.00", CultureInfo.InvariantCulture)

                dRSum += dr
                cRSum += cr

                i += 1
            Next
            sb.AppendLine("<tr><td colspan='2'><b>Total</b></td><td><b>" & dRSum.ToString("0,0.00", CultureInfo.InvariantCulture) &
                          "</b></td><td><b>" & cRSum.ToString("0,0.00", CultureInfo.InvariantCulture) & "</b></td><td></td></tr>")
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            recentJournalVourcher.InnerHtml = sb.ToString()

            If (dRSum = cRSum) Then
                btnSubmit.Enabled = True
            Else
                btnSubmit.Enabled = False
            End If

        Else
            recentJournalVourcher.InnerHtml = "<h2>There are no Recent Journal Voucher Entries recorded by you.</h2>"
        End If
        ClientScript.RegisterStartupScript(Me.GetType(), "showmodal", "$('.config').click();", True)
        ClientScript.RegisterStartupScript(Me.GetType(), "hide3", "HideMessage();", True)
    End Sub

    Private Sub CopyToCurrent(ByVal uid As String, ByVal newsessionId As String)
        _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='copy', @UserName='" & uid & "', @SessionID='" & newsessionId & "'"
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        message.InnerHtml = ""
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            ClientScript.RegisterStartupScript(Me.GetType(), "hide5", "HideMessage();", True)
        End If
        GetJournalVoucher(Session.SessionID, Me._userId)
    End Sub

    Private Sub ClearField()
        'ddlAccountHead.SelectedIndex = 0
        ddlSubAccount.SelectedIndex = 0
        txtDR.Text = String.Empty
        txtCR.Text = String.Empty
        txtNarration.Text = String.Empty
        txt_cheque.Text = String.Empty
    End Sub

    Private Sub SubmitJournalVoucher(ByVal sId As String, ByVal uid As String, ByVal narration As String, ByVal branch As String, ByVal BulkGUID As String, ByVal chequeno As String, ByVal voucherNo As String)
        ' _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='js', @UserName='" & uid & "', @SessionID='" & sId & "', @Narration='" & narration & "',@VoucherDate='" & Helper.GetFrontToDbDate(txt_VourcherDate.Text.ToString) & "',@VoucherTypeID='" & ddl_voucherType.SelectedValue.ToString & "',@BranchID='" & branch & "'"

        _sql = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='jsPost', @UserName='" & uid & "', @SessionID='" & sId & "', @Narration=N'" & narration & "',@VoucherDate='" & Helper.GetFrontToDbDate(txt_VourcherDate.Text.ToString) & "',@VoucherTypeID='" & ddl_voucherType.SelectedValue.ToString & "',@BranchID='" & branch & "', @BulkGUID='" & BulkGUID & "',@chequeNo=N'" & chequeno & "',@VoucherNo='" & voucherNo & "'"

        Dim msgClass As String
        Dim sb As New StringBuilder("")
        Dim ds As New DataSet
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("errorcode") = "1" Then
            txtNarration.Text = String.Empty
            msgClass = "note note-success"
        ElseIf ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows(0).Item("errorcode") = "0" Then
            msgClass = "note note-danger"
        End If
        sb.AppendLine("<div class='" & msgClass & "'>")
        sb.AppendLine("<div class='close-note'>x</div>")
        sb.AppendLine("<p>")
        sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
        sb.AppendLine("</p>")
        sb.AppendLine("</div>")
        message.InnerHtml = sb.ToString
        ClearField()
        GetJournalVoucher(Session.SessionID, Me._userId)
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        smallMessage.InnerHtml = ""
        message.InnerHtml = ""
        GetAccountSubgroup(ddlGroup.SelectedValue.ToString)
    End Sub
    Protected Sub ddlSubGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSubGroup.SelectedIndexChanged
        message.InnerHtml = ""
        GetBudgetofSubgroup(ddlSubGroup.SelectedValue)
        GetAccountHead(ddlGroup.SelectedValue.ToString, ddlSubGroup.SelectedValue.ToString)
    End Sub

    Protected Sub GetBudgetofSubgroup(ByVal subgroupId As String)
        Dim ds As New DataSet
        smallMessage.InnerHtml = ""
        _sql = "EXEC [Accounts].[usp_Budgeting] @flag='getbudget',@SubGroupID='" & subgroupId & "',@BranchID=" & Session("BranchID")
        ds = _dao.ExecuteDataset(_sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim totalbudget As Decimal
            totalbudget = ds.Tables(0).Rows(0).Item("TotalBudget")
            smallMessage.InnerHtml = "Budget: " & totalbudget.ToString("0,0.00", CultureInfo.InvariantCulture)
        Else
            smallMessage.InnerHtml = ""
        End If
    End Sub

    Private Function CheckIfBudgetExceeds(ByVal subgroupId As String, ByVal VoucherAmount As String) As Boolean
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        _sql = "EXEC [Accounts].[usp_SubGroup] @Flag='checkbudgetexceed',@VoucherAmount='" & VoucherAmount & "',@SubGroupID='" & subgroupId & "',@BranchID='" & Session("BranchID") & "'"
        ds = _dao.ExecuteDataset(_sql)

        If ds.Tables.Count > 1 Then
            Dim msgType As String = ds.Tables(1).Rows(0).Item("MessageType").ToString.Trim
            Dim msgText As String = ds.Tables(1).Rows(0).Item("MessageText").ToString.Trim
            If msgType = "1" Or msgType = "2" Then
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(msgText)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Message", "alert('" & msgText & "');", True)
                Return True
            Else
                Return False
            End If
        End If
        Return False
    End Function

    Protected Sub btnReload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReload.Click
        message.InnerHtml = ""
        GetJournalVoucher(Session.SessionID, Me._userId)
    End Sub

    Protected Sub ddlAccountHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAccountHead.SelectedIndexChanged
        GetSubAccountHead(ddlAccountHead.SelectedValue.ToString)
    End Sub

    Protected Sub btnDeleteJV_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDeleteJV.Click
        DeleteJournalVoucher(hfTempJVId.Value)
    End Sub
End Class