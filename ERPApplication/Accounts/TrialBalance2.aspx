﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TrialBalance2.aspx.vb" Inherits="School.TrialBalance2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <link rel="stylesheet" href="../../assets/Calendar/jquery.calendars.picker.css">
 
    <script src="../../assets/Calendar/jquery.plugin.js"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../../assets/Calendar/jquery.calendars.nepali.js"></script>
     
    <script type="text/javascript">
        var isBSDate = "<%=GetisBSDate() %>";

        var checkFromValidation = false;
        $(document).ready(function () {
            $("#<%=lblLoading.ClientID %>").hide();
            $('#form').validate();

            $("#<%=txtDateFrom.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Report Filtration Start Date is required.'} });
            $("#<%=txtDateTo.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Report Filtration End Date is required.'} });

            checkFromValidation = function () {
                $("#<%=lblLoading.ClientID %>").show();
                $("#<%=btnSubmit.ClientID %>").hide();
                var bool = true;

                if ($('#<%=txtDateFrom.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtDateTo.ClientID %>').valid() == false) bool = false;
                if (!bool) {
                    $("#<%=lblLoading.ClientID %>").hide();
                    $("#<%=btnSubmit.ClientID %>").show();

                    $('#form').validate().focusInvalid();
                }
                return bool;
            };

          

        });

    </script>
     <script type="text/javascript">
         $(document).ready(function () {
             $(function () {
                 var calendar = $.calendars.instance('nepali');
                 $("#<%= txtDateFrom_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                     ChangeDate(selectedDate, 'e', this);
                 }
                 });
                 $("#<%= txtDateTo_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                     ChangeDate(selectedDate, 'e', this);
                 }
                 });

             });

             $('#<%= txtDateFrom.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
             $('#<%= txtDateTo.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();


             Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
                 $(function () {
                     var calendar = $.calendars.instance('nepali');
                     $("#<%= txtDateFrom_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                         ChangeDate(selectedDate, 'e', this);
                     }
                     });
                     $("#<%= txtDateTo_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                         ChangeDate(selectedDate, 'e', this);
                     }
                     });

                 });

                 $("#<%=txtDateFrom.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                 $("#<%=txtDateTo.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();


             });
         });
     </script>
     <script type="text/javascript">
         function print(divId) {
             var contents = $("#" + divId).html();
             var win = window.open("", "", "height=500,width=900");
             win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
             win.document.write("</head><body >");
             win.document.write(contents);
             win.document.write("</body></html>");
             win.print();
             win.close();
             return true;
         }

         $("#<%= btnToExcel.ClientID %>").hide();
         $(document).ready(function () {
             $("#<%= btnToExcel.ClientID %>").hide();

             $("#toExcel").click(function () {
                 $("#<%= btnToExcel.ClientID %>").click();
             });
         });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".voucherDetails").click(function () {
                debugger;

                    var branchid = <%= getSessionBranchID() %>; 
                var fiscalid = <%= getSessionFiscalID() %>
                var accounthead = $(this).attr("data-id");
                var dbName = $(this).attr("data-db"); ;

                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/GetVoucherDetails_forReport',
                    data: '{"accHead" : "' + accounthead + '","sBranchID" : "' + branchid + '","dbName" : "' + dbName + '","sFiscalID" : "' + fiscalid + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        $('#model_data').html(data);
                        

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });
            });
        });

    </script>
    <script type="text/javascript">
        function formatDate(date, type) {
            console.log("date : " + date + " ,type : " + type);
            var month = '';
            var day = '';
            var year = '';
            var resultdate;
            if (type == "n") {
                var d = new Date(date);
                month = d.getMonth() + 1;
                day = d.getDate();
                year = d.getFullYear();
                resultdate = [day, month, year].join('/');
            }
            else {

                year = date[0]._year;
                month = date[0]._month;
                day = date[0]._day;
                resultdate = [year, month, day].join('/');
            }
            console.log("formatted : " + resultdate);
            return resultdate;

        }



        function ChangeDate(value, obj, element) {
            value = formatDate(value, obj);
            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';
            debugger;
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/DateConverterNew',
                data: dataToSend,
                datatype: 'json',
                contentType: 'application/json',
                success: function (data) {
                    debugger;
                    var dd, mm, yy;
                    var value = [];
                    if (obj == "e") {
                        var fieldId1 = element.id.replace(/_Nep$/i, '');
                        //                        $("#" + fieldId1).val(data);
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId1).val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                    }
                    else if (obj == "n") {
                        var fieldId2 = element.id + '_Nep'
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId2).val(yy + "/" + mm + "/" + dd);
                    }
                }
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Details</h4>
                    </div>
                    <div class="modal-body">
                         <div id="model_data">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Trial Balance <small>Generate Trial Balance</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Account</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/Accounts/TrialBalance2.aspx">Trail Balance</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box green">
				    <div class="portlet-title">
					    <div class="caption"><i class="fa fa-cogs"></i>Filter Trial Balance Information</div>
					    <div class="tools">
						    <a href="javascript:;" class="collapse"></a>
						    <a href="javascript:;" class="remove"></a>
					    </div>
				    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>--%>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Branch/Depart :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <asp:DropDownList ID="ddlBranch" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%=ddlBranch.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                    <label class="col-md-2 control-label">
                                        Type :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="all" Selected="True">All</asp:ListItem>
                                                <asp:ListItem Value="jv">Only JV</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%=ddlType.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Date From :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" placeholder="Enter Date from where report should be generated" onchange="ChangeDate(this.value,'n',this);"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateFrom.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                    <label class="col-md-2 control-label">
                                        Miti From :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateFrom_Nep" runat="server" CssClass="form-control" placeholder="Enter From Miti"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateFrom_Nep.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Date To :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" placeholder="Enter Date Upto which report should be generated" onchange="ChangeDate(this.value,'n',this);"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateTo.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                    <label class="col-md-2 control-label">
                                        Miti To :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateTo_Nep" runat="server" CssClass="form-control" placeholder="Enter To Miti"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateTo_Nep.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfobtaindata" runat="server" />
                                <asp:Button ID="btnGetVoucherDetails" runat="server" Text="Button" Style="display: none;" />
                                <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="btnSubmit" class="btn green" runat="server" Text="Generate" OnClientClick="return checkFromValidation();" />
                                        <asp:Label ID="lblLoading" runat="server" Text="Loading ...." class="btn green" disabled></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
			    </div>
                <div id="listData" runat="server">
                </div>
            </div>
        </div>
    </div>
     <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" />

      
</asp:Content>
