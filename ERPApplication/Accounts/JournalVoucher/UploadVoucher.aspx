﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UploadVoucher.aspx.vb" Inherits="School.UploadVoucher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 
 <style type="text/css">
    td.details-control 
    {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control 
    {
    background: url('http://datatables.net/examples/resources/details_close.png') no-repeat center center;
    
    }
</style>
<style type="text/css">
        .Vheader
        {
            text-align: center;
            padding: 0px;
            margin: 0px auto;
        }
        .Vheader h3
        {
            padding-top: 0px;
            margin: 0px;
        }
        
        .Vheader h4
        {
            padding: 0px;
            padding-right: 0px;
          
        }
        
        .Vheader .dec
        {
             padding-left: 0px;
            margin: 0px;
            font-family: "Harlow Solid Italic", Times, serif;
            border: 1px solid #a79e9e;
            font-size: xx-large;
            border-radius: 30px;
            background: #f0f0f0;
            padding-right: 10px;
            padding-left: 10px;
        }
        
        .Vheader h3
        {
            padding: 0px;
            margin: 0px;
        }
        .imglogo
        {
            width: 60px;
            height: 60px;
            top: 0px;
        }
        .Vheader .lefta
        {
            text-align:left; !important;
        }
            
        .Vheader .rightb
        {
            text-align:right; !important;
        }
       
        .tcenter
        {
        text-align:center;
        }
   
        .lineTable 
        {
            border-bottom:1px solid #000 !important;
            border-top:1px solid #000 !important;
            width:100%;      
        }
          
        .lastDouble
        {
            text-align:right;
            border-bottom: double;      
         }
        
        .lastSingle
        {
            text-align:right;
             border-bottom: 1px solid #000000;
                   
         }
        .chequeline
        {
            text-align:right;
            border-left: 1px solid #000000;
            text-align:center;
        }

    </style>
<script type="text/javascript">
        var branchid = <%= getSessionBranchID() %>;

         $(document).ready(function () {
            var dt = $('#displaytable').DataTable({
                      "columns": [
                    {
                        "class": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    { "data": "S.N"},
                    { "data": "BulkGUID"},
                    { "data": "Date" },
                    { "data": "Amount" },
                    { "data": "Narration" },
                    { "data": "Documents" },
                    {"data": "Action"}
                ],
                "columnDefs": [
                    {
                        "targets": [ 2 ],
                        "visible": false,
                        "searchable": false
                    }
            
                ],          
                "order": [[1, 'asc']]
             });
             var detailRows = [];

            $('#displaytable tbody').on('click', 'tr td.details-control', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = dt.row(tr);

                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {  
//                var bulkGUID = $(this).closest('tr').attr('id');  
//                    alert( row.data().BulkGUID);
                    $.ajax({
                        url: '/SetupServices.svc/GetJournalVoucherDetailByBulkID',
                        data: '{"BranchID" :"'+ branchid + '","bulkGUID": "'+ row.data().BulkGUID + '"}',
                        type: 'POST',
                        datatype: 'json',
                        contentType: 'application/json',
                        success: function (data) {
//                            row.child(data.GetJournalVoucherDetailByBulkIDResult).show();
                            row.child(data).show();
                            tr.addClass('shown');
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });
        });
</script>

<script type="text/javascript">
    $(document).ready(function () {

        window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {

            $(document).on('click', '.EditAmount', function () {
                //        $(".EditAmount").click(function () {

                debugger;
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var itemToModify = $(this).attr("data-modify");

                var newAmount = prompt("Enter Amount", "0.00");
                console.log(newAmount);
                if (newAmount === null || newAmount === false || newAmount === '') {
                    console.log("Amount is empty");
                    return;
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/edit_JournalVoucher',
                        data: '{"jvId" : "' + obtainId + '","val" : "' + newAmount + '","modifyitem" : "' + itemToModify + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            debugger;
                            thisItem.html(newAmount);

                            alert(data);

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });

            $(document).on('click', '.deleteVoucher', function () {
                if (confirm("Are you sure to delete this voucher?") == true) {
                    var thisItem = $(this);
                    var obtainId = $(this).attr("data-id");
                    var bulkGuid = $(this).attr("data-bulkGuid");
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/delete_JournalVoucher',
                        data: '{"bulkGuid" : "' + bulkGuid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            alert(data);
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }

            });


            $(document).on('click', '.previewVoucher', function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var bulkGuid = $(this).attr("data-bulkGuid");

                $('#<%= hdnBulkGuid.ClientID %>').val(bulkGuid);
                $('#<%= hdnBulkRowCount.ClientID %>').val(obtainId);
                $("#<%= btnPreview.ClientID %>").click();
            });
        });
    });
    </script>
     <script type="text/javascript">
         $(document).ready(function () {
             $(document).on('click', '.reload', function () {
                 $('#<%= btnReload.ClientID %>').click();
             });
         });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Voucher Preview</h4>
                    </div>
                    <div class="modal-body">
                        <div class="page">
                            <div id='list' runat="server">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue hidden">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                             <asp:HiddenField ID="hdnBulkGuid" runat="server" />
                            <asp:HiddenField ID="hdnBulkRowCount" runat="server" />
                            <asp:Button ID="btnPreview" runat="server" Text="Preview" class="btn purple hidden"/>
                    </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Upload Voucher <small>Upload and preview voucher</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Accounts</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/Accounts/JournalVoucher/UploadVoucher.aspx">Upload Voucher</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                
                 <div  id="listData" runat="server">
            </div>
        </div>
    </div>
    </div>
   
    <asp:Button ID="btnReload" runat="server" Text="Reload" class="btn purple hidden"/>   
</asp:Content>
