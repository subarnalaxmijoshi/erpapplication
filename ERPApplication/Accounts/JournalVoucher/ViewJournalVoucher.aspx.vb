﻿Imports System.IO
Imports System.Web.HttpContext
Imports System.Globalization
Public Class ViewJournalVoucher
    Inherits System.Web.UI.Page

    Dim Dao As New DatabaseDao
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            Response.Redirect(redirectUrl)
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
        Else
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
        End If


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetJournalVoucher()
        End If
    End Sub

    Protected Function GetisBSDate() As String
        Return System.Configuration.ConfigurationManager.AppSettings("isBSDate").ToString
    End Function

    Protected Function getSessionUserID() As String
        Return Session("userID").ToString
    End Function
    Protected Function getSessionBranchID() As String
        Return Session("BranchID").ToString
    End Function

    Protected Function getSessionFiscalYearID() As String
        Return Session("FiscalYearID").ToString
    End Function
    Sub GetJournalVoucher()
        Dim sql As String
        sql = "exec accounts.usp_voucher @BranchID=" & Session("BranchID")
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then

            sb.AppendLine("<div class='row'>")
            sb.AppendLine("<div class='col-md-12'>")
            sb.AppendLine("<div class='portlet box green'>")
            sb.AppendLine("<div class='portlet-title'>")
            sb.AppendLine("<div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cog'></i>Voucher Information</div>")
            sb.AppendLine("<div class='tools'>")
            sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='javascript:;' class='remove'></a>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")


            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")

            sb.AppendLine("<th>S.N </th>")
            sb.AppendLine("<th>Date</th>")
            sb.AppendLine("<th>Miti</th>")
            sb.AppendLine("<th>Amount</th>")
            sb.AppendLine("<th>Preview</th>")
            sb.AppendLine("<th>Status</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")

            Dim i As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows
                Dim voucherDate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("VoucherDate")).ToString("dd MMM yyyy")
                Dim vouchermiti() = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("VoucherDate")).ToString("dd-MM-yyyy").Split("-")
                Dim v_miti = eng_to_nep(vouchermiti(2), vouchermiti(1), vouchermiti(0))

                Dim amount = ds.Tables(0).Rows(i).Item("Amount").ToString
                Dim sessionID = ds.Tables(0).Rows(i).Item("SessionID").ToString
                Dim narration = ds.Tables(0).Rows(i).Item("Narration").ToString
                Dim bulkGUID = ds.Tables(0).Rows(i).Item("BulkGUID").ToString.Trim
                Dim bulkCount = ds.Tables(0).Rows(i).Item("BulkCount").ToString.Trim

                sb.AppendLine("<tr><td class='numeric'>" & (i + 1) & "</td>")
                sb.AppendLine("<td>" & voucherDate & "</td>")
                sb.AppendLine("<td>" & v_miti & "</td>")
                sb.AppendLine("<td><a href='#portlet-config' data-toggle='modal' class='config amountDetail' data-session='" & sessionID & "' data-bulkGuid='" & bulkGUID & "'>" & amount & "</a></td>")
                '  sb.AppendLine("<td>" & narration & "</td>")
                sb.AppendLine("<td style='text-align:center'><a data-toggle='modal' href='#voucherPreview' class='previewVoucher' data-id='" & bulkCount & "' data-bulkGuid='" & bulkGUID & "' title='Preview'>View</a></td>")
                sb.AppendLine("<td style='text-align:center'><a class='voucherVerify' href='javascript:;' class='verify' data-session='" & sessionID & "' data-bulkGuid='" & bulkGUID & "' > Verify </a></td>")
                sb.AppendLine("</tr>")

                i += 1
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")

            listData.InnerHtml = sb.ToString()
        Else
            sb.AppendLine("<div class='note note-success'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("There are no Journal Voucher to display")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPreview.Click
        Dim dbName As String = Session("DatabaseName").ToString.Trim()
        If (Session("IsPublicCampus") = False) Then
            GetVoucherPreviewInNepali(hdnBulkGuid.Value, hdnBulkRowCount.Value)
        Else
            GetVoucherPreview(hdnBulkGuid.Value, hdnBulkRowCount.Value)
        End If

    End Sub

    Sub GetVoucherPreview(ByVal bulkGUID As String, ByVal bulkCount As String)
        Dim sql1 As String
        Dim ds1 As New DataSet
        sql1 = "EXEC [Management].[usp_InstitutionalInfo] @flag='s'"
        ds1 = Dao.ExecuteDataset(sql1)


        Dim institutionName As String = ds1.Tables(0).Rows(0).Item("Name").ToString
        Dim InstitutionAddress As String = ds1.Tables(0).Rows(0).Item("Address").ToString

        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC [dbo].[usp_previewJournalVoucher] @flag='v1',@BulkGUID='" & bulkGUID & "',@BranchID='" & Session("BranchID") & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim sb As New StringBuilder("")


        Dim k As Integer = 6
        Dim crsum As Decimal
        Dim drsum As Decimal

        Dim Summary = ds.Tables(0).Rows(0).Item("Summary").ToString()
        Dim CreatedBy = ds.Tables(0).Rows(0).Item("CreatedBy").ToString()
        Dim CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString()
        'Dim ReferenceNo = ds.Tables(0).Rows(0).Item("ReferenceNo").ToString()
        Dim VoucherDate = ds.Tables(0).Rows(0).Item("VoucherDate").ToString()
        Dim VoucherTypeID = ds.Tables(0).Rows(0).Item("VoucherTypeID").ToString()
        Dim Branch = ds.Tables(0).Rows(0).Item("Branch").ToString()
        Dim ChequeNo = ds.Tables(0).Rows(0).Item("chequeNo").ToString


        'If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(0).Item("VoucherDate").ToString) Then
        '    VoucherDate = Helper.GetDbToFrontDate(Convert.ToDateTime(VoucherDate).ToString("MM/dd/yyyy"))
        '    Dim arr() = VoucherDate.Split("/")
        '    VoucherDate = arr(0) & "/" & arr(1) & "/" & arr(2)
        'End If

        sb.AppendLine("<table class='Vheader' width='100%'>")
        sb.AppendLine("<tr><td rowspan='4'>")
        If File.Exists(HttpContext.Current.Server.MapPath("../../assets/InstitutionLogo/CampusLogo.jpg")) Then
            sb.AppendLine("<img src='../../assets/InstitutionLogo/CampusLogo.jpg' class='imglogo'/></td>")
        Else
            sb.AppendLine("<img src='../../assets/InstitutionLogo/tulogo1.png' class='imglogo'/></td>")
        End If
        sb.AppendLine("<td>")
        sb.AppendLine("</td></tr><td><h3>" & institutionName.Replace(" ", "&nbsp;") & "</h3>")
        sb.AppendLine("</td><td></td> </tr><tr><td>")
        sb.AppendLine("<h4>" & InstitutionAddress.Replace(" ", "&nbsp;") & "</h4>")
        sb.AppendLine("</td><td></td></tr> <tr><td>")
        sb.AppendLine("<span class='dec'>Voucher</span>")
        sb.AppendLine("</td><td></td></tr><tr><td class='lefta'>SN :" & bulkCount & "</td><td class='rightb'>DATE:</td><td class='lefta'>" & Convert.ToDateTime(VoucherDate).ToString("MM-dd-yyyy") & " </td></tr></table>")
        sb.AppendLine("   <table border='1' width='100%' cellpadding='0' cellspacing='0' class='Vtbl'>")
        sb.AppendLine("<tr><th style='background-color:#efefef;' rowspan='2'>Particulars</th><th style='background-color:#efefef;' rowspan='2'>Amount Details</th><th style='background-color:#efefef;'>Debit</th><th style='background-color:#efefef;'>Credit</th></tr>")
        sb.AppendLine("<tr><th style='background-color:#efefef;'>Rs.</th><th style='background-color:#efefef;'>Rs.</th></tr>")
        Dim oldGroupName As String = ""
        Dim IsPreviousDr As Boolean = False
        Dim IsPreviousCr As Boolean = False
        Dim c As Integer = 1
        Dim g As Integer = 1
        Dim SumOnlyDR As Decimal
        Dim SumOnlyCR As Decimal
        Dim i As Integer
        Dim DR As Decimal
        Dim CR As Decimal
        Dim JNotes As String
        Dim arrNotes() As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            sb.AppendLine("<tr>")
            Dim GroupName = ds.Tables(0).Rows(i).Item("GroupName")
            Dim code = ds.Tables(0).Rows(i).Item("ledgerCode")

            Dim groupSumDr As Decimal
            Dim groupSumCr As Decimal



            Dim count As Integer = 0


            If Not oldGroupName = GroupName Then
                Dim sql2 = "EXEC [dbo].[usp_previewJournalVoucher] @flag='v2',@BulkGUID='" & bulkGUID & "', @GroupName=N'" & GroupName & "',@BranchID='" & Session("BranchID") & "'"
                Dim ds2 As New DataSet
                ds2 = Dao.ExecuteDataset(sql2)
                groupSumDr = ds2.Tables(0).Rows(0).Item("DR")
                groupSumCr = ds2.Tables(0).Rows(0).Item("CR")

                sb.AppendLine("<td><b>" & code & "/" & GroupName & "<b></td><td></td>")
                sb.AppendLine("<td>" & IIf(groupSumDr > 0, (groupSumDr), "") & "</td>")
                sb.AppendLine("<td>" & IIf(groupSumCr > 0, (groupSumCr), "") & "</td>")
                oldGroupName = GroupName
            End If


            Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString().Replace("A/C", "")
            DR = ds.Tables(0).Rows(i).Item("DR")
            CR = ds.Tables(0).Rows(i).Item("CR")
            Dim SubAccountName = ds.Tables(0).Rows(i).Item("SubAccountHead").ToString().Replace("A/C", "")

            JNotes = ds.Tables(0).Rows(i).Item("Notes").ToString()
            arrNotes = JNotes.ToString().Split(Chr(10))

            '  sb.AppendLine("<td style='text-align:center'>" & i + 1 & ".</td>")
            sb.AppendLine("</tr>")
            sb.AppendLine("<tr>")

            sb.AppendLine("<td style='padding-left:25px'>" & AccountName)

            If Not String.IsNullOrWhiteSpace(SubAccountName) Then
                sb.AppendLine("</br><span style='padding-left:40px'>" & SubAccountName & "</span>")
            End If
            sb.AppendLine("</td>")

            If ds.Tables(0).Rows(i).Item("DR") > 0 Then
                If ds.Tables(0).Select("Dr>0").Count > c Then
                    If Not ds.Tables(0).Select("Dr>0").Count = c Then
                        c = c + 1

                        SumOnlyDR += ds.Tables(0).Rows(i).Item("DR")
                        If ds.Tables(0).Select("ledgerCode='" & code & "'").Count() = g Then
                            sb.AppendLine("<td class='lastSingle'>" & (DR + CR) & "</td>")
                            g = 1
                        Else
                            sb.AppendLine("<td  style='text-align:right'>" & (DR + CR) & "</td>")
                            g = g + 1

                        End If

                    End If
                Else
                    SumOnlyDR += ds.Tables(0).Rows(i).Item("DR")


                    If ds.Tables(0).Select("ledgerCode='" & code & "'").Count() = g Then
                        sb.AppendLine("<td class='lastSingle'>" & (DR + CR) & "</td>")
                        g = 1
                    Else
                        sb.AppendLine("<td style='text-align:right'>" & (DR + CR) & "</td>")
                        g = g + 1

                    End If
                    sb.AppendLine("<td></td><td></td></tr><tr><td>")
                    For j As Integer = 0 To arrNotes.Length - 1
                        sb.AppendLine("" & arrNotes(j).Trim() & "<br/>")
                    Next
                    sb.AppendLine("</td><td class='lastDouble'><b> " & (SumOnlyDR) & "</b></td>")
                    c = 0

                End If




            Else

                If ds.Tables(0).Select("Cr>0").Count > c Then
                    If Not ds.Tables(0).Select("Cr>0").Count = c Then
                        c = c + 1

                        SumOnlyCR += ds.Tables(0).Rows(i).Item("CR")
                        ' sb.AppendLine("<td  style='text-align:right'>" & NumberConvertor(DR + CR) & "</td>")

                        If ds.Tables(0).Select("ledgerCode='" & code & "'").Count() = g Then
                            sb.AppendLine("<td class='lastSingle'>" & (DR + CR) & "</td>")
                            g = 1
                        Else
                            sb.AppendLine("<td style='text-align:right' >" & (DR + CR) & "</td>")
                            g = g + 1
                        End If
                    End If
                Else
                    SumOnlyCR += ds.Tables(0).Rows(i).Item("CR")
                    sb.AppendLine("<td class='lastSingle'>" & (DR + CR) & "</td>")
                    sb.AppendLine("<td></td><td></td></tr><tr><td>")


                    For j As Integer = 0 To arrNotes.Length - 1
                        sb.AppendLine("" & arrNotes(j).Trim() & "<br/>")
                    Next
                    sb.AppendLine("</td><td class='lastDouble'><b> " & (SumOnlyCR) & "</b></td>")
                    c = 0

                End If

            End If

            sb.AppendLine("<td style='text-align:right'></td>")
            sb.AppendLine("<td style='text-align:right'></td>")
            sb.AppendLine("</tr>")

            If Not String.IsNullOrWhiteSpace(DR) Then
                drsum += DR

            End If

            If Not String.IsNullOrWhiteSpace(CR) Then
                crsum += CR
            End If

            k = k + 1
        Next


        'SumOnlyCR += ds.Tables(0).Rows(i).Item("CR")
        'sb.AppendLine("<td class='lastSingle'>" & NumberConvertor(DR + CR) & "</td>")
        sb.AppendLine("<tr><td>")
        For j As Integer = 0 To arrNotes.Length - 1
            sb.AppendLine("" & arrNotes(j).Trim() & "<br/>")
        Next
        sb.AppendLine("</td><td class='lastDouble'><b> " & (SumOnlyCR) & "</b></td><td></td><td></td></tr>")
        c = 0
        'sb.AppendLine("<tr><td></td><td></td><td></td><td></td></tr>")

        'For j As Integer = k To 14
        '    sb.AppendLine("<tr><td></td><td></td><td></td><td></td></tr>")
        'Next


        sb.AppendLine("<tr><th></th><th style='text-align:right' >Total</th><th style='text-align:right padding-right=1px;'>" & (Convert.ToDecimal(crsum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))) & "</th><th style='text-align:right  padding-right=1px;'>" & (Convert.ToDecimal(drsum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))) & " </th></tr>")
        sb.AppendLine("<table width='100%'>")
        '  sb.AppendLine("<tr><td colspan='4'> Inwords:" & Helper.ConvertNumberToRupees(crsum) & "</td></tr>")
        sb.AppendLine("<tr><td colspan='4'>Narration:</td></tr>")

        Dim arrSummary() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arrSummary.Length - 1
            ' sb.AppendLine("".PadRight(5) & "" & arr(j).Trim().PadRight(32))
            sb.AppendLine("<tr><td colspan='4'>" & arrSummary(j).Trim() & "</td></tr>")
        Next

        'sb.AppendLine("<tr><td colspan='4'>" & Summary & "</td></tr>")
        sb.AppendLine("<tr><td colspan='4'></td></tr></table><table class='lineTable'>")
        sb.AppendLine("<tr><td>Receiver's Signature </td><td class='chequeline'>Cheque No.: " & ChequeNo & "</td></tr></table>")
        sb.AppendLine("<table width='100%'>")

        sb.AppendLine("<tr><td colspan='4'></td></tr>")
        sb.AppendLine("<tr><td colspan='4'></td></tr>")
        sb.AppendLine("<tr><td colspan='4'></td></tr>")
        sb.AppendLine("<tr class='tcenter'><td></td><td>Prepared by </td><td> Checked by</td><td>Approved by</td></tr>")
        sb.AppendLine("</table>")
        list.InnerHtml = sb.ToString
    End Sub

    Sub GetVoucherPreviewInNepali(ByVal bulkGUID As String, ByVal bulkCount As String)
        list.InnerHtml = ""
        Dim sql1, sql3 As String
        Dim ds1, ds3 As New DataSet
        sql1 = "EXEC [Management].[usp_InstitutionalInfo] @flag='s'"
        ds1 = Dao.ExecuteDataset(sql1)


        Dim institutionName As String = ds1.Tables(0).Rows(0).Item("Name").ToString
        Dim InstitutionAddress As String = ds1.Tables(0).Rows(0).Item("Address").ToString
        Dim institutionNameNep As String = ds1.Tables(0).Rows(0).Item("NameInNepali").ToString
        Dim InstitutionAddressNep As String = ds1.Tables(0).Rows(0).Item("AddressInNepali").ToString

        sql3 = "EXEC [Setup].[usp_BranchSetup] @flag='n',@BranchID='" & Session("BranchID") & "'"
        ds3 = Dao.ExecuteDataset(sql3)
        Dim UniversityNep, DepartNep, BranchNameNep As String
        If ds3.Tables(0).Rows.Count > 0 Then
            UniversityNep = ds3.Tables(0).Rows(0).Item("UniversityInNepali").ToString
            DepartNep = ds3.Tables(0).Rows(0).Item("InstitutionInNepali").ToString
            BranchNameNep = ds3.Tables(0).Rows(0).Item("BranchNameInNepali").ToString
        End If

        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC [dbo].[usp_previewJournalVoucher] @flag='v1',@BulkGUID='" & bulkGUID & "',@BranchID='" & Session("BranchID") & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim sb As New StringBuilder("")


        Dim k As Integer = 6
        Dim crsum As Decimal
        Dim drsum As Decimal

        Dim Summary = ds.Tables(0).Rows(0).Item("Summary").ToString()
        Dim CreatedBy = ds.Tables(0).Rows(0).Item("CreatedBy").ToString()
        Dim CreatedDate = ds.Tables(0).Rows(0).Item("CreatedDate").ToString()
        'Dim ReferenceNo = ds.Tables(0).Rows(0).Item("ReferenceNo").ToString()
        Dim VoucherDate = ds.Tables(0).Rows(0).Item("VoucherDate").ToString()
        Dim VoucherTypeID = ds.Tables(0).Rows(0).Item("VoucherTypeID").ToString()
        Dim Branch = ds.Tables(0).Rows(0).Item("Branch").ToString()
        Dim ChequeNo = ds.Tables(0).Rows(0).Item("chequeNo").ToString

        'If GetisBSDate() = "Y" Then
        If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(0).Item("VoucherDate").ToString) Then
            VoucherDate = Helper.GetDbToFrontDate(Convert.ToDateTime(VoucherDate).ToString("MM/dd/yyyy"))
            Dim arr() = VoucherDate.Split("/")
            VoucherDate = eng_to_nep(arr(0), arr(2), arr(1))
        End If
        'End If
        sb.AppendLine("<table class='Vheader' width='100%'>")
        sb.AppendLine("<tr><td rowspan='5'>")
        If File.Exists(HttpContext.Current.Server.MapPath("../../assets/InstitutionLogo/CampusLogo.jpg")) Then
            sb.AppendLine("<img src='../../assets/InstitutionLogo/CampusLogo.jpg' class='imglogo'/></td>")
        Else
            sb.AppendLine("<img src='../../assets/InstitutionLogo/tulogo1.png' class='imglogo'/></td>")
        End If
        sb.AppendLine("<td>")

        sb.AppendLine("<h5>" & UniversityNep & "</h5></td><td align='right'><h5>त्रि.वि.फा.नं.६</h5></td></tr>")
        sb.AppendLine("<tr><td><h5>" & DepartNep & "</h5></td><td></td></tr>")
        sb.AppendLine("<tr><td><h3>" & institutionNameNep & "</h3></td><td></td></tr>")
        sb.AppendLine("<tr><td><h4>" & InstitutionAddressNep & "</h4></td><td></td></tr>")
        sb.AppendLine("<tr><td><h5>" & BranchNameNep & "</h5></td><td></td></tr>")
        sb.AppendLine("<tr><td></td><td><span class='dec'>भाउचर</span></td><td></td></td></tr>")

        sb.AppendLine("</tr><tr><td class='lefta'>क्रम संख्या : " & NumberConvertor(bulkCount) & "</td><td class='rightb'>मिति:</td><td class='lefta'>" & NumberConvertor(VoucherDate) & " </td></tr></table>")

        sb.AppendLine("   <table border='1' width='100%' cellpadding='0' cellspacing='0' >")
        sb.AppendLine("<tr><th style='background-color:#efefef' rowspan='2'>हिसाव विवरण</th><th style='background-color:#efefef;' rowspan='2'>रकम विवरण</th><th style='background-color:#efefef;'>डेविट</th><th style='background-color:#efefef;'>क्रेडिट</th></tr>")
        sb.AppendLine("<tr><th style='background-color:#efefef;'>रू</th><th style='background-color:#efefef;'>रू</th></tr>")



        Dim oldGroupName As String = ""
        Dim IsPreviousDr As Boolean = False
        Dim IsPreviousCr As Boolean = False
        Dim c As Integer = 1
        Dim g As Integer = 1
        Dim SumOnlyDR As Decimal
        Dim SumOnlyCR As Decimal
        Dim i As Integer
        Dim DR As Decimal
        Dim CR As Decimal
        Dim JNotes As String
        Dim arrNotes() As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            sb.AppendLine("<tr>")
            Dim GroupName = ds.Tables(0).Rows(i).Item("GroupName")
            Dim code = ds.Tables(0).Rows(i).Item("ledgerCode")

            Dim groupSumDr As Decimal
            Dim groupSumCr As Decimal



            Dim count As Integer = 0


            If Not oldGroupName = GroupName Then
                Dim sql2 = "EXEC [dbo].[usp_previewJournalVoucher] @flag='v2',@BulkGUID='" & bulkGUID & "', @GroupName=N'" & GroupName & "',@BranchID='" & Session("BranchID") & "'"
                Dim ds2 As New DataSet
                ds2 = Dao.ExecuteDataset(sql2)
                groupSumDr = ds2.Tables(0).Rows(0).Item("DR")
                groupSumCr = ds2.Tables(0).Rows(0).Item("CR")

                sb.AppendLine("<td><b>" & code & "/" & GroupName & "<b></td><td></td>")
                sb.AppendLine("<td>" & IIf(groupSumDr > 0, NumberConvertor(groupSumDr), "") & "</td>")
                sb.AppendLine("<td>" & IIf(groupSumCr > 0, NumberConvertor(groupSumCr), "") & "</td>")
                oldGroupName = GroupName
            End If


            Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString().Replace("A/C", "")
            DR = ds.Tables(0).Rows(i).Item("DR")
            CR = ds.Tables(0).Rows(i).Item("CR")
            Dim SubAccountName = ds.Tables(0).Rows(i).Item("SubAccountHead").ToString().Replace("A/C", "")

            JNotes = ds.Tables(0).Rows(i).Item("Notes").ToString()
            arrNotes = JNotes.ToString().Split(Chr(10))

            '  sb.AppendLine("<td style='text-align:center'>" & i + 1 & ".</td>")
            sb.AppendLine("</tr>")
            sb.AppendLine("<tr>")

            sb.AppendLine("<td style='padding-left:25px'>" & AccountName)

            If Not String.IsNullOrWhiteSpace(SubAccountName) Then
                sb.AppendLine("</br><span style='padding-left:40px'>" & SubAccountName & "</span>")
            End If
            sb.AppendLine("</td>")

            If ds.Tables(0).Rows(i).Item("DR") > 0 Then
                If ds.Tables(0).Select("Dr>0").Count > c Then
                    If Not ds.Tables(0).Select("Dr>0").Count = c Then
                        c = c + 1

                        SumOnlyDR += ds.Tables(0).Rows(i).Item("DR")
                        If ds.Tables(0).Select("ledgerCode='" & code & "'").Count() = g Then
                            sb.AppendLine("<td class='lastSingle'>" & NumberConvertor(DR + CR) & "</td>")
                            g = 1
                        Else
                            sb.AppendLine("<td  style='text-align:right'>" & NumberConvertor(DR + CR) & "</td>")
                            g = g + 1

                        End If

                    End If
                Else
                    SumOnlyDR += ds.Tables(0).Rows(i).Item("DR")


                    If ds.Tables(0).Select("ledgerCode='" & code & "'").Count() = g Then
                        sb.AppendLine("<td class='lastSingle'>" & NumberConvertor(DR + CR) & "</td>")
                        g = 1
                    Else
                        sb.AppendLine("<td style='text-align:right'>" & NumberConvertor(DR + CR) & "</td>")
                        g = g + 1

                    End If
                    sb.AppendLine("<td></td><td></td></tr><tr><td>")
                    For j As Integer = 0 To arrNotes.Length - 1
                        sb.AppendLine("" & arrNotes(j).Trim() & "<br/>")
                    Next
                    sb.AppendLine("</td><td class='lastDouble'><b> " & NumberConvertor(SumOnlyDR) & "</b></td>")
                    c = 0

                End If




            Else

                If ds.Tables(0).Select("Cr>0").Count > c Then
                    If Not ds.Tables(0).Select("Cr>0").Count = c Then
                        c = c + 1

                        SumOnlyCR += ds.Tables(0).Rows(i).Item("CR")
                        ' sb.AppendLine("<td  style='text-align:right'>" & NumberConvertor(DR + CR) & "</td>")

                        If ds.Tables(0).Select("ledgerCode='" & code & "'").Count() = g Then
                            sb.AppendLine("<td class='lastSingle'>" & NumberConvertor(DR + CR) & "</td>")
                            g = 1
                        Else
                            sb.AppendLine("<td style='text-align:right' >" & NumberConvertor(DR + CR) & "</td>")
                            g = g + 1
                        End If
                    End If
                Else
                    SumOnlyCR += ds.Tables(0).Rows(i).Item("CR")
                    sb.AppendLine("<td class='lastSingle'>" & NumberConvertor(DR + CR) & "</td>")
                    sb.AppendLine("<td></td><td></td></tr><tr><td>")


                    For j As Integer = 0 To arrNotes.Length - 1
                        sb.AppendLine("" & arrNotes(j).Trim() & "<br/>")
                    Next
                    sb.AppendLine("</td><td class='lastDouble'><b> " & NumberConvertor(SumOnlyCR) & "</b></td>")
                    c = 0

                End If

            End If

            sb.AppendLine("<td style='text-align:right'></td>")
            sb.AppendLine("<td style='text-align:right'></td>")
            sb.AppendLine("</tr>")

            If Not String.IsNullOrWhiteSpace(DR) Then
                drsum += DR

            End If

            If Not String.IsNullOrWhiteSpace(CR) Then
                crsum += CR
            End If

            k = k + 1
        Next



        'SumOnlyCR += ds.Tables(0).Rows(i).Item("CR")
        'sb.AppendLine("<td class='lastSingle'>" & NumberConvertor(DR + CR) & "</td>")
        sb.AppendLine("<tr><td>")
        For j As Integer = 0 To arrNotes.Length - 1
            sb.AppendLine("" & arrNotes(j).Trim() & "<br/>")
        Next
        sb.AppendLine("</td><td class='lastDouble'><b> " & NumberConvertor(SumOnlyCR) & "</b></td><td></td><td></td></tr>")
        c = 0
        'sb.AppendLine("<tr><td></td><td></td><td></td><td></td></tr>")

        'For j As Integer = k To 14
        '    sb.AppendLine("<tr><td></td><td></td><td></td><td></td></tr>")
        'Next


        'sb.AppendLine("<tr><th></th><th style='text-align:right' >Total</th><th style='text-align:right padding-right=1px;'>" & (Convert.ToDecimal(crsum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))) & "</th><th style='text-align:right  padding-right=1px;'>" & (Convert.ToDecimal(drsum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))) & " </th></tr>")
        sb.AppendLine("<tr><th></th><th style='text-align:right' >जम्मा</th><th style='text-align:right padding-right=1px;'>" & NumberConvertor(Convert.ToDecimal(crsum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))) & "</th><th style='text-align:right  padding-right=1px;'>" & NumberConvertor(Convert.ToDecimal(drsum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))) & " </th></tr>")
        sb.AppendLine("<table width='100%'>")
        '  sb.AppendLine("<tr><td colspan='4'> Inwords:" & Helper.ConvertNumberToRupees(crsum) & "</td></tr>")
        sb.AppendLine("<tr><td colspan='4'>कारोवारको विवरण:</td></tr>")

        Dim arrSummary() As String = ds.Tables(0).Rows(0)("summary").ToString().Split(Chr(13))
        For j As Integer = 0 To arrSummary.Length - 1
            ' sb.AppendLine("".PadRight(5) & "" & arr(j).Trim().PadRight(32))
            sb.AppendLine("<tr><td colspan='4'>" & arrSummary(j).Trim() & "</td></tr>")
        Next

        'sb.AppendLine("<tr><td colspan='4'>" & Summary & "</td></tr>")
        sb.AppendLine("<tr><td colspan='4'></td></tr></table><table class='lineTable'>")
        sb.AppendLine("<tr><td>भुक्तानी प्राप्त गर्नेको हस्ताक्षर </td><td class='chequeline'>चेक न‌ं: " & ChequeNo & "</td></tr></table>")
        sb.AppendLine("<table width='100%'>")

        sb.AppendLine("<tr><td colspan='4'></td></tr>")
        sb.AppendLine("<tr><td colspan='4'></td></tr>")
        sb.AppendLine("<tr><td colspan='4'></td></tr>")
        'sb.AppendLine("<tr class='tcenter'><td></td><td>Prepared by </td><td> Checked by</td><td>Approved by</td></tr>")
        sb.AppendLine("<tr class='tcenter'><td></td><td>तयार गर्ने: </td><td>  जाँच गर्ने:</td><td>स्वीकृत दिने अधिकृत:</td></tr>")
        sb.AppendLine("</table>")
        list.InnerHtml = sb.ToString
    End Sub

    Function NumberConvertor(ByVal num As String) As String
        If num = "" Then
            Return ""
        End If
        Dim i As Integer = 0
        Dim j As Char() = num.ToCharArray
        Dim converted As String = ""
        While num.ToCharArray.Length > i
            Dim value = j(i)
            Select Case (value)
                Case "0"
                    converted += "०"
                Case "1"
                    converted += "१"
                Case "2"
                    converted += "२"
                Case "3"
                    converted += "३"
                Case "4"
                    converted += "४"
                Case "5"
                    converted += "५"
                Case "6"
                    converted += "६"
                Case "7"
                    converted += "७"
                Case "8"
                    converted += "८"
                Case "9"
                    converted += "९"
                Case Else
                    converted += value

            End Select
            i = i + 1

        End While

        Return converted
    End Function

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Journal_Voucher_Details")
    End Sub
End Class