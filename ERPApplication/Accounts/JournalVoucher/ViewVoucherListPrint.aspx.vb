﻿Imports System.IO
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Globalization
Imports System.Collections.Generic
Imports System.Web.HttpContext
Public Class ViewVoucherListPrint
    Inherits System.Web.UI.Page
    Dim Dao As New DatabaseDao
    Dim DC As New DateConverter()
    Dim sql As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RenameVoucherDocument()
        End If
        GetFiscalYearDates()
        GetInstitutionsList()
        GetResultAsHTML_ALL()

    End Sub

    Sub GetFiscalYearDates()
        Dim ds As New DataSet
        sql = "EXEC [Setup].[usp_FiscalYearMaster] @Flag='s',@BranchID= '" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If (ds.Tables(0).Rows.Count > 0) Then
            txtDateFrom.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DateFrom")).ToString("dd MMM yyyy")
            txtDateTo.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DateTo")).ToString("dd MMM yyyy")
        End If
    End Sub

    Protected Function GetisBSDate() As String
        Return System.Configuration.ConfigurationManager.AppSettings("isBSDate").ToString
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

        Dim fromdate As String = Helper.GetFrontToDbDate(txtDateFrom.Text)
        Dim todate As String = Helper.GetFrontToDbDate(txtDateTo.Text)

        GetResultAsHTML(fromdate, todate)

    End Sub

    Private Sub GetResultAsHTML(ByVal dateFrom As String, ByVal dateTo As String)
        listData.InnerHtml = ""
        message.InnerHtml = ""
        Dim ds As New DataSet
        Dim i As Integer
        Dim sb As New StringBuilder("")
        listData.InnerHtml = String.Empty
        Dim stringSplite As String()
        Dim dbName As String = Session("DatabaseName").ToString.Trim()

        sql = "EXEC usp_viewJournalVoucher @flag='v', @DateFrom='" & dateFrom & "', @dateto='" & dateTo & "',@BranchID=" & Session("BranchID")
        sql += ",@FiscalYearID='" & Session("FiscalYearID") & "'"
        If (Session("DatabaseName") = "PulchowkCampusNew2075") Then
            sql += ",@UserID='" & Session("userID") & "'"
        End If


        ds = Dao.ExecuteDataset(sql)

        If (ds.Tables(0).Rows.Count > 0) Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers/vouchers</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")
            'ReferenceNo,VoucherDate, AccountName
            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content' id='displaytable' >")
            sb.AppendLine("<thead class='flip-content' style='background-color:#CCC'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            sb.AppendLine("<th>ReferenceNo </th>")
            sb.AppendLine("<th>VoucherDate </th>")
            sb.AppendLine("<th>Total</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            Dim sum As Decimal
            'If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Columns(0).ColumnName.ToLower().Trim <> "errornumber" Then

            For Each row As DataRow In ds.Tables(0).Rows
                Dim SN = ds.Tables(0).Rows(i).Item("SN").ToString
                'Dim CreatedDate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("CreatedDate")).ToString("dd MMM yyyy")
                Dim CreatedDate = ""
                If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("VoucherDate").ToString) Then
                    CreatedDate = Helper.GetDbToFrontDate(Convert.ToDateTime(ds.Tables(0).Rows(i).Item("VoucherDate")).ToString("MM/dd/yyyy"))
                End If
                Dim ReferenceNo = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString

                ' Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString



                'stringSplite = AccountName.Split(New Char() {"-"c})
                'AccountName = stringSplite(0)

                Dim DR As Decimal = ds.Tables(0).Rows(i).Item("Total")
                sum += DR

                If (Not String.IsNullOrEmpty(DR)) Then
                    DR = Convert.ToDecimal(DR).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                End If

                'Dim CR = ds.Tables(0).Rows(i).Item("CR").ToString
                'If (Not String.IsNullOrEmpty(CR)) Then
                '    CR = Convert.ToDecimal(CR).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                'End If

                '  Dim Summary = ds.Tables(0).Rows(i).Item("Summary").ToString
                sb.AppendLine("<tr>")
                sb.AppendLine("<td>" & i + 1 & "</td>")
                'sb.AppendLine("<td><a href='#portlet-config' data-toggle='modal' class='config amountDetail' data-session='" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                'If (dbName = "PokharaNurshingCampus" Or dbName = "PulchowkCampusNew2075" Or dbName = "ForestryDean2075" Or dbName = "MechiCampusNew2075") Then
                '    sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucherInNepali2.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                'Else
                '    sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucher.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                'End If

                If (Session("IsPublicCampus") = True) Then
                    sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucher.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                Else
                    If (Session("IsSimpleVoucher") = True) Then
                        sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/SimpleVoucher.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                    Else
                        sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucherInNepali2.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                    End If
                End If

                'sb.AppendLine("<td>" & ReferenceNo & "</td>")
                sb.AppendLine("<td>" & CreatedDate & "</td>")
                '  sb.AppendLine("<td>" & AccountName & "</td>")
                sb.AppendLine("<td style='text-align: right; padding-right:15px;'>" & DR & "</td>")
                sb.AppendLine("</tr>")

                i += 1
            Next
            'sb.AppendLine("<tr>")
            'sb.AppendLine("<td></td>")

            'sb.AppendLine("<td></td>")
            'sb.AppendLine("<td style='text-align: left; padding-right:15px;'><b>Total</b></td>")
            'sb.AppendLine("<td style='text-align: right; padding-right:15px font-face:bold;'>" & Convert.ToDecimal(sum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP")) & "</td>")
            ''sb.AppendLine("<td style='text-align: right; padding-right:15px;'><b>" & Convert.ToDecimal(ds.Tables(2).Rows(0).Item("CR").ToString).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP")) & "</b></td>")
            'sb.AppendLine("</tr>")
            'Dim Balance = ds.Tables(1).Rows(0).Item("Balance").ToString
            'sb.AppendLine("<tr><td colspan='6' style='text-align: center;'><b>Balance <span style='width:100px; text-decoration: underline;display: inline-block;'> " & Balance & " </span>CR/DR</b></td>")
            'sb.AppendLine("</tr>")
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            listData.InnerHtml = sb.ToString()
            'ElseIf ds.Tables(0).Columns(0).ColumnName.ToLower().Trim = "errornumber" Then
            '    ''sb.AppendLine("<div class='note note-danger'>")
            '    ''sb.AppendLine("<div class='close-note'>x</div>")
            '    ''sb.AppendLine("<p>")
            '    ''sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            '    ''sb.AppendLine("</p>")
            '    ''sb.AppendLine("</div>")
        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Record Found for the search text")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")

            message.InnerHtml = sb.ToString

        End If

    End Sub

    Sub GetInstitutionsList()
        Dim sql As String
        Dim ds As New DataSet

        sql = "exec [Management].[usp_InstitutionalInfo] @flag='s',@ID=" & Session("CompanyID")
        ds = Dao.ExecuteDataset(sql)


        Dim i As Integer = 0
        For Each row As DataRow In ds.Tables(0).Rows
            Dim ID = ds.Tables(0).Rows(i).Item("ID").ToString
            Dim Name = ds.Tables(0).Rows(i).Item("Name").ToString
            Dim Slogan = ds.Tables(0).Rows(i).Item("Slogan").ToString
            Dim Address = ds.Tables(0).Rows(i).Item("Address").ToString
            Dim Telephone = ds.Tables(0).Rows(i).Item("Telephone").ToString
            Dim Email = ds.Tables(0).Rows(i).Item("Email").ToString
            Dim Fax = ds.Tables(0).Rows(i).Item("Fax").ToString
            Dim Website = ds.Tables(0).Rows(i).Item("Website").ToString
            Dim Branch = ds.Tables(0).Rows(i).Item("BranchID").ToString

            InstitutionaName.InnerText = Name
            campusAddress.InnerText = Address
        Next


    End Sub


    Private Sub GetResultAsHTML_ALL()
        listData.InnerHtml = ""
        message.InnerHtml = ""
        Dim ds As New DataSet
        Dim i As Integer
        Dim sb As New StringBuilder("")
        listData.InnerHtml = String.Empty
        Dim stringSplite As String()
        Dim dbName As String = Session("DatabaseName").ToString.Trim()

        sql = "EXEC usp_viewJournalVoucher @flag='a',@BranchID=" & Session("BranchID")
        sql += ",@FiscalYearID='" & Session("FiscalYearID") & "'"
        If (Session("DatabaseName") = "PulchowkCampusNew2075") Then
            sql += ",@UserID='" & Session("userID") & "'"
        End If

        ds = Dao.ExecuteDataset(sql)

        If (ds.Tables(0).Rows.Count > 0) Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers/Vouchers</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")
            'ReferenceNo,VoucherDate, AccountName
            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content' id='displaytable1'>")
            sb.AppendLine("<thead class='flip-content' style='background-color:#CCC'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            sb.AppendLine("<th>ReferenceNo </th>")
            sb.AppendLine("<th>VoucherDate </th>")
            sb.AppendLine("<th>Miti</th>")
            sb.AppendLine("<th>Total</th>")
            sb.AppendLine("<th>View Documents</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            Dim sum As Decimal
            'If ds.Tables(0).Rows.Count > 0 And ds.Tables(0).Columns(0).ColumnName.ToLower().Trim <> "errornumber" Then
            For Each row As DataRow In ds.Tables(0).Rows
                Dim SN = ds.Tables(0).Rows(i).Item("SN").ToString
                'Dim CreatedDate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("CreatedDate")).ToString("dd MMM yyyy")
                Dim CreatedDate = ""
                If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item("VoucherDate").ToString) Then
                    CreatedDate = ds.Tables(0).Rows(i).Item("VoucherDate").ToString()
                    'Helper.GetDbToFrontDate(Convert.ToDateTime(ds.Tables(0).Rows(i).Item("VoucherDate")).ToString("MM/dd/yyyy"))
                End If
                Dim ReferenceNo = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString

                ' Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString



                'stringSplite = AccountName.Split(New Char() {"-"c})
                'AccountName = stringSplite(0)
                'Dim vouchermiti() = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("VoucherDate")).ToString("dd-MM-yyyy").Split("-")
                'Dim v_miti = eng_to_nep(vouchermiti(2), vouchermiti(1), vouchermiti(0))

                'Dim VoucherDate = Helper.GetDbToFrontDate(Convert.ToDateTime(ds.Tables(0).Rows(i).Item("VoucherDate")).ToString("dd/MM/yyyy"))


                'Dim voucherdt() = ds.Tables(0).Rows(i).Item("v_date").ToString().Split("/")
                'Dim nepDate As String = DC.ToBS(voucherdt(0), voucherdt(1), voucherdt(2))
                Dim nepDate As String = ds.Tables(0).Rows(i).Item("v_date").ToString().Trim


                Dim DR As Decimal = ds.Tables(0).Rows(i).Item("Total")
                sum += DR

                If (Not String.IsNullOrEmpty(DR)) Then
                    DR = Convert.ToDecimal(DR).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                End If
                sb.AppendLine("<tr>")
                sb.AppendLine("<td>" & i + 1 & "</td>")
                'sb.AppendLine("<td><a href='#portlet-config' data-toggle='modal' class='config amountDetail' data-session='" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")

                If (Session("IsPublicCampus") = False) Then
                    If (Session("IsSimpleVoucher") = True) Then
                        sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/SimpleVoucher.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                    Else
                        sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucherInNepali2.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                    End If
                Else
                    sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucher.aspx?vid=" & ReferenceNo & "'>" & ReferenceNo & "</a></td>")
                End If



                'sb.AppendLine("<td>" & ReferenceNo & "</td>")
                sb.AppendLine("<td>" & CreatedDate & "</td>")
                sb.AppendLine("<td>" & nepDate & "</td>")
                '  sb.AppendLine("<td>" & AccountName & "</td>")
                sb.AppendLine("<td style='text-align: right; padding-right:15px;'>" & DR & "</td>")
                sb.AppendLine("<td style='text-align: center;'><a href='/Accounts/JournalVoucher/VoucherDocumentViewer.aspx?ReferenceNo=" & ReferenceNo & "'>View</a></td>")
                sb.AppendLine("</tr>")

                i += 1
            Next
            sb.AppendLine("<tr>")
            sb.AppendLine("<td></td>")
            sb.AppendLine("<td></td>")
            sb.AppendLine("<td></td>")
            sb.AppendLine("<td style='text-align: left; padding-right:15px;'><b>Total</b></td>")
            sb.AppendLine("<td style='text-align: right; padding-right:15px font-face:bold;'>" & Convert.ToDecimal(sum).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP")) & "</td>")
            sb.AppendLine("<td></td>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            listAllData.InnerHtml = sb.ToString()

        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Record Found for the search text")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")

            message.InnerHtml = sb.ToString

        End If

    End Sub


    Protected Sub RenameVoucherDocument()
        Try
            Dim ds2 As New DataSet
            Dim sql2 As String
            sql2 = "EXEC [Accounts].[usp_TempJournalVourcher] @Flag='bulkhistory', @BranchID='" & Session("BranchID") & "'"
            ds2 = Dao.ExecuteDataset(sql2)
            If ds2.Tables(0).Rows.Count > 0 Then

                Dim vFolder As String = Server.MapPath("~/Accounts/JournalVoucher/Documents")
                Dim branchName As String = Session("BranchName").ToString
                Dim vPath As String = System.IO.Path.Combine(vFolder, branchName)


                If Directory.Exists(vPath) Then
                    Dim di As New System.IO.DirectoryInfo(vPath)
                    Dim arrDI As System.IO.DirectoryInfo() = di.GetDirectories()

                    For i As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                        Dim bulkID As String = ds2.Tables(0).Rows(i).Item("BulkGUID").ToString().Trim
                        Dim refNo As String = ds2.Tables(0).Rows(i).Item("ReferenceNo").ToString().Trim
                        Dim sourcedir As String = System.IO.Path.Combine(vPath, bulkID)
                        Dim destinationdir As String = System.IO.Path.Combine(vPath, refNo)


                        For Each d In arrDI
                            If (d.Name.Trim = bulkID) Then
                                Directory.Move(sourcedir, destinationdir)
                            End If
                        Next
                        sourcedir = String.Empty
                        destinationdir = String.Empty
                    Next
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listAllData.InnerHtml, "Voucher List")
    End Sub

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function
End Class