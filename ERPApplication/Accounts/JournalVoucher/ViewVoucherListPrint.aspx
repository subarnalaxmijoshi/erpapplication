﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="ViewVoucherListPrint.aspx.vb" Inherits="School.ViewVoucherListPrint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../../assets/Calendar/jquery.calendars.picker.css">
    <script src="../../assets/Calendar/jquery.plugin.js"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../../assets/Calendar/jquery.calendars.nepali.js"></script>
    <script type="text/javascript">
        var isBSDate = "<%=GetisBSDate() %>";

        var checkFromValidation = false;
        $(document).ready(function () {

            $('#form').validate();

            $("#<%=txtDateFrom.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Report Filtration Start Date is required.'} });
            $("#<%=txtDateTo.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Report Filtration End Date is required.'} });

            checkFromValidation = function () {
                var bool = true;

                if ($('#<%=txtDateFrom.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtDateTo.ClientID %>').valid() == false) bool = false;
                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };

        });

    </script>
    <script type="text/javascript">

        $("#<%= btnToExcel.ClientID %>").hide();


        $(document).ready(function () {
            $("#<%= btnToExcel.ClientID %>").hide();



            if (isBSDate == "Y") {
                $(function () {
                    var calendar = $.calendars.instance('nepali');
                    $('#<%= txtDateFrom.ClientID %>').calendarsPicker({ calendar: calendar });
                    $('#<%= txtDateTo.ClientID %>').calendarsPicker({ calendar: calendar });

                });

            }
            else {
                $('#<%= txtDateFrom.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                $('#<%= txtDateTo.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            }

            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
                if (isBSDate == "Y") {
                    $(function () {
                        var calendar = $.calendars.instance('nepali');
                        $('#<%= txtDateFrom.ClientID %>').calendarsPicker({ calendar: calendar });
                        $('#<%= txtDateTo.ClientID %>').calendarsPicker({ calendar: calendar });

                    });
                } else {
                    $("#<%=txtDateFrom.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                    $("#<%=txtDateTo.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                }

            });
            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });

            function print(divId) {
                var contents = $("#" + divId).html();
                var win = window.open("", "", "height=500,width=900");
                win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
                win.document.write("</head><body >");
                win.document.write(contents);
                win.document.write("</body></html>");
                win.print();
                win.close();
                return true;
            }

        });
    </script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#displaytable').DataTable({
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
            });
            $('#displaytable1').DataTable({
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h1>
                            <span id="InstitutionaName" runat="server" style="text-align: center;"></span>
                        </h1>
                        <h2>
                            <span id="campusAddress" runat="server" style="text-align: center;"></span>
                        </h2>
                        <h4>
                            <span id="v" runat="server" style="text-align: center;">Voucher</span></h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-panel hidden-xs hidden-sm">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Voucher List<small>List of vouchers, view and print</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li id="toExcel"><a href="javascript:;">Export To Excel</a></li>
                            <%--<li id="toPrinter"><a onclick="return print('<%=listAllData.ClientID %>')" href="#">Print</a></li>--%>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Account</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/accounts/journalvoucher/ViewVoucherListPrint.aspx">vourcher list</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#create" aria-controls="create" role="tab"
                            data-toggle="tab">Range Filter</a></li>
                        <li role="presentation"><a href="#display" aria-controls="display" role="tab" data-toggle="tab">
                            All Ledgers/Vouchers</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="create">
                            <div class="portlet box purple ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>Vouchers/Ledgers
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                                        </a><a href="" class="reload"></a><a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-horizontal">
                                        <div class="form-body">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Date From :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                                <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" placeholder="Enter Date from where report should be generated"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtDateFrom.ClientID%>" class="error" style="display: none;">
                                                        </div>
                                                        <label class="col-md-2 control-label">
                                                            Date To :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                                                <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" placeholder="Enter Date Upto which report should be generated"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtDateTo.ClientID%>" class="error" style="display: none;">
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="form-actions fluid">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <asp:Button ID="btnSubmit" class="btn purple" runat="server" Text="Search" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="listData" runat="server">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="display">
                            <div id="listAllData" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnToExcel" class="btn green" runat="server" Text="Export To Excel"
                   UseSubmitBehavior="false"/>
                <%--<asp:Button ID="btnPDF" class="btn red" runat="server" Text="Export To PDF" OnClientClick="return checkFromValidation();" />--%>
                <%--<asp:Button ID="Print" class="btn blue" runat="server" Text="Print" UseSubmitBehavior="false" />--%>
            </div>
        </div>
    </div>
</asp:Content>
