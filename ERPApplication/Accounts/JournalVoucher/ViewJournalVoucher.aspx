﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ViewJournalVoucher.aspx.vb" Inherits="School.ViewJournalVoucher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .Vheader
        {
            text-align: center;
            padding: 0px;
            margin: 0px auto;
        }
        .Vheader h3
        {
            padding-top: 0px;
            margin: 0px;
        }
        
        .Vheader h4
        {
            padding: 0px;
            padding-right: 0px;
          
        }
        
        .Vheader .dec
        {
             padding-left: 0px;
            margin: 0px;
            font-family: "Harlow Solid Italic", Times, serif;
            border: 1px solid #a79e9e;
            font-size: xx-large;
            border-radius: 30px;
            background: #f0f0f0;
            padding-right: 10px;
            padding-left: 10px;
        }
        
        .Vheader h5
        {
            padding: 0px;
            padding-right: 0px;
        }
        .imglogo
        {
            width: 60px;
            height: 60px;
            top: 0px;
        }
        .Vheader .lefta
        {
            text-align:left; !important;
        }
            
        .Vheader .rightb
        {
            text-align:right; !important;
        }
       
        .tcenter
        {
        text-align:center;
        }
   
        .lineTable 
        {
            border-bottom:1px solid #000 !important;
            border-top:1px solid #000 !important;
            width:100%;      
        }
          
        .lastDouble
        {
            text-align:right;
            border-bottom: double;      
         }
        
        .lastSingle
        {
            text-align:right;
             border-bottom: 1px solid #000000;
                   
         }
        .chequeline
        {
            text-align:right;
            border-left: 1px solid #000000;
            text-align:center;
        }

    </style>
    <script type="text/javascript">
        function print(divId) {
            var contents = $("#" + divId).html();
            var win = window.open("", "", "height=500,width=900");
            win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
            win.document.write("</head><body >");
            win.document.write(contents);
            win.document.write("</body></html>");
            win.print();
            win.close();
            return true;
        }
        $(document).ready(function () {
            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
         var sessionUserid = <%= getSessionUserID() %>;
         var sessionBranchID =<%=getSessionBranchID() %>;
            $(".amountDetail").click(function () {
                var session = $(this).attr("data-session");
                var bulkGUID = $(this).attr("data-bulkGuid");

                $(".modal-body").html("<img src='/assets/img/loading.gif' />");
                $.ajax({
                    type: "POST",
                    url: '/SetupServices.svc/JournalVoucherDetail',
                    data: '{"sessionId": "' + session +'","BranchID" :"'+ sessionBranchID + '","bulkGUID": "'+ bulkGUID + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        console.log(data);
                        var html = "<table class='table table-bordered table-striped table-condensed flip-content'>";
                        html += "<thead><tr>";
                        html += "<td>SN</td><td>Account Head</td><td>DR</td><td>CR</td><td colspan=2>Option</td></tr></thead><tbody>";
                        for (var i = 0; i < data.length; i++) {
                            html += "<tr><td>" + (i + 1) + "</td><td>" + data[i].AccountHead + " ";
                            
                            if(!(data[i].SubAccountHead ==='')){
                                html += "( " + data[i].SubAccountHead + " )";
                            }

                            html +="</td><td>" + data[i].DR + "</td><td>" + data[i].CR + "</td>";
                            html += "<td><a class='voucherEdit' href='/Accounts/JournalVoucher/EditVoucher.aspx?EditId=" + data[i].JournalVoucherId +
                                        "&EditBulkGUID=" + bulkGUID + "' data-id='" + data[i].JournalVoucherId + "'>Edit</a></td>";
                            html += "<td><a class='voucherDelete' href='javascript:;'  data-id='" + data[i].JournalVoucherId + "'>Delete</a></td></tr>";
                        }
                        html += "</tbody></table>";
                        $(".modal-body").html(html);
                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }
                });
            });

            $(document).on("click", ".voucherDelete", function () {
            if (confirm("Are you sure to delete this vourcher?") == true) {
                var thisItem = $(this);
                thisItem.parent("td").parent("tr").remove();
                var voucherId = $(this).attr("data-id");
                console.log(voucherId);
                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/DeleteJournalVoucher',
                    data: '{"voucherId": "' + voucherId + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        console.log(data);
                        alert(data);
                        thisItem.parent("td").parent("tr").remove();
                    }, error: function (data) {
                        console.log(data);
                        alert(data.statusText);
                    }, failure: function (data) {
                        console.log(data);
                        alert(data.statusText);
                    }
                });
                }  
            });


            $(document).on("click", ".voucherVerify", function () {
                 var sessionUserid = <%= getSessionUserID() %>;
                 var sessionBranchID =<%=getSessionBranchID() %>;
                 var sessionFiscalyear = <%=getSessionFiscalYearID() %>;
                  if (confirm("Are you sure to verify this vourcher?") == true) {
                    var session = $(this).attr("data-session");
                    var bulkGUID = $(this).attr("data-bulkGuid");
                    var thisItem = $(this);
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/VerifyVoucher',
                        data: '{"sessionId" : "' + session + '","sUserId" : "' + sessionUserid + '","BranchID" :"'+ sessionBranchID +'","bulkGUID": "'+ bulkGUID + '","sFiscalID" :"'+ sessionFiscalyear +'"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            console.log(data);
                            alert(data);
//                            var str = data.slice(0, data.indexOf('/'));
//                            var refNo = data.slice(data.lastIndexOf('/')+1);
//                            console.log(str);
//                            console.log(refNo);
//                            alert(str);
                            thisItem.parent("td").parent("tr").remove();
                        }, error: function (data) {
                            console.log(data);
                            alert(data.statusText);
                        }, failure: function (data) {
                            console.log(data);
                            alert(data.statusText);
                        }
                    });
                }
            });


            $(document).on('click', '.previewVoucher', function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var bulkGuid = $(this).attr("data-bulkGuid");

                $('#<%= hdnBulkGuid.ClientID %>').val(bulkGuid);
                $('#<%= hdnBulkRowCount.ClientID %>').val(obtainId);
                $("#<%= btnPreview.ClientID %>").click();
            });


        });

       
    </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#displaytable').DataTable({
                    "lengthMenu": [[10,25, 50, -1], [10,25, 50, "All"]]
                });
            });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Voucher Detail</h4>
                    </div>
                    <div class="modal-body">
                        <img src="/assets/img/loading.gif"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue hidden">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="voucherPreview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Voucher Preview</h4>
                    </div>
                    <div class="modal-body">
                        <div class="page">
                            <div id='list' runat="server">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue hidden">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                             <asp:HiddenField ID="hdnBulkGuid" runat="server" />
                            <asp:HiddenField ID="hdnBulkRowCount" runat="server" />
                            <asp:Button ID="btnPreview" runat="server" Text="Preview" class="btn purple hidden"/>
                    </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Journal Voucher <small>View Journal Voucher</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-gear"></i><a href="/Setup/Setupmenu.aspx">Setup</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/Accounts/JournalVoucher/ViewJournalVoucher.aspx">View Journal Voucher</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                
                 <div  id="listData" runat="server">
            </div>
        </div>
    </div>
    </div>
<asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" style="display:none;" />
</asp:Content>
