﻿<%@ Page Title="Journal Vourcher Entry" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="JournalVoucherEntry.aspx.vb" Inherits="School.JournalVoucherEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="../assets/Calendar/jquery.calendars.picker.css">
    <script src="../assets/Calendar/jquery.plugin.js"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../assets/Calendar/jquery.calendars.js"></script>
    <script src="../assets/Calendar/jquery.calendars.plus.js"></script>
    <script src="../assets/Calendar/jquery.calendars.picker.js"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../assets/Calendar/jquery.calendars.nepali.js"></script>
    <style type="text/css">
        .pull-center
        {
            text-align: center !important;
            font-weight: bold;
        }
        
        .pull-left
        {
            text-align: left !important;
        }
    </style>
    <script type="text/javascript">
        var isBSDate = "N";
        var checkFromValidation = false;
        var checkNarration = false;

        $(document).ready(function () {

            $('#form').validate();



            checkFromValidation = function () {
                $("#MainContent_txtDR").rules('add', { number: true, minlength: 1, messages: { required: 'Debit Amount is required.', minlength: jQuery.format("At least {0} characters are required.")} });
                $("#MainContent_txtCR").rules('add', { number: true, minlength: 1, messages: { required: 'Credit Amount is required.', minlength: jQuery.format("At least {0} characters are required.")} });
                $("#MainContent_ddlAccountHead").rules('add', { required: true, minlength: 1, messages: { required: 'Account Head is required.', minlength: jQuery.format("At least {0} characters are required.")} });

                var bool = true;

                if ($('#MainContent_txtDR').valid() == false) bool = false;
                if ($('#MainContent_txtCR').valid() == false) bool = false;
                if ($('#MainContent_ddlAccountHead').valid() == false) bool = false;
                if ($('#MainContent_ddl_voucherType').valid() == false) bool = false;

                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };

            checkNarration = function () {
                $("#MainContent_txtNarration").rules('add', { required: true, minlength: 10, messages: { required: 'Narration is required.', minlength: jQuery.format("At least {0} characters are required.")} });
                var bool = true;
                if ($('#MainContent_txtNarration').valid() == false) bool = false;
                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };

        });
    
    </script>
    <script type="text/javascript">
        var isBSDate = "<%=GetisBSDate() %>";
        $(document).ready(function () {

                $(function () {
                    var calendar = $.calendars.instance('nepali');
//                    $('#<%= txt_VourcherDate.ClientID %>').calendarsPicker({ calendar: calendar });
                    $("#<%= txt_VourcherDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                        ChangeDate(selectedDate, 'e', this);
                    }
                    });

                });

                $('#<%= txt_VourcherDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
//                $('#<%= txt_VourcherDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true });

           

            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
               
                    $(function () {
                        var calendar = $.calendars.instance('nepali');
                        $("#<%= txt_VourcherDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                            ChangeDate(selectedDate, 'e', this);
                        }
                        });

                    });

                $('#<%= txt_VourcherDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();

                
            });

            window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
                $('#<%= ddlGroup.ClientID %>').change(function () {
                    $('#<%= ddlsubGroup.ClientID %>').html("<option value=''>Loading....</option>");
                });

                $('#<%= ddlsubGroup.ClientID %>').change(function () {
                    $('#<%= ddlAccountHead.ClientID %>').html("<option value=''>Loading....</option>");
                });

                $('#<%= ddlAccountHead.ClientID %>').change(function () {
                    $('#<%= ddlSubAccount.ClientID %>').html("<option value=''>Loading....</option>");
                });


            });

        });
    </script>
    <script type="text/javascript">
        function formatDate(date, type) {
            console.log("date : " + date + " ,type : " + type);
            var month = '';
            var day = '';
            var year = '';
            var resultdate;
            if (type == "n") {
                var d = new Date(date);
                month = d.getMonth() + 1;
                day = d.getDate();
                year = d.getFullYear();
                resultdate = [day, month, year].join('/');
            }
            else {

                year = date[0]._year;
                month = date[0]._month;
                day = date[0]._day;
                resultdate = [year, month, day].join('/');
            }
            console.log("formatted : " + resultdate);
            return resultdate;

        }



        function ChangeDate(value, obj, element) {
            value = formatDate(value, obj);
            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';
            debugger;
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/DateConverterNew',
                data: dataToSend,
                datatype: 'json',
                contentType: 'application/json',
                success: function (data) {
                    debugger;
                    var dd, mm, yy;
                    var value = [];
                    if (obj == "e") {
                        var fieldId1 = element.id.replace(/_Nep$/i, '');
                        //                        $("#" + fieldId1).val(data);
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId1).val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                    }
                    else if (obj == "n") {
                        var fieldId2 = element.id + '_Nep'
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId2).val(yy + "/" + mm + "/" + dd);
                    }
                }
            });

        }
    </script>
    <script type="text/javascript">
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = '' + days[day] + ', ' + months[month] + ' ' + d + ', ' + year + ', ' + h + ':' + m + ':' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
        
    </script>
    <script type="text/javascript">
        $('#<%= btnOld.ClientID %>').hide();

        $(document).ready(function () {
            $('#recentJV').click(function () {
                $('#<%= btnOld.ClientID %>').click();
                $('.config').click();
            });

        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".EditAmount").click(function () {

                debugger;
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var itemToModify = $(this).attr("data-modify");

                var newAmount = prompt("Enter Amount/Value", "0.00");
                console.log(newAmount);
                if (newAmount === null || newAmount === false || newAmount === '') {
                    console.log("Amount or value is empty");
                    alert("Amount or value is empty");
                    return;
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/edit_TempJournalVourcher',
                        data: '{"jvId" : "' + obtainId + '","val" : "' + newAmount + '","modifyitem" : "' + itemToModify + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            thisItem.html(newAmount);

                            alert(data);

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });
        });
    
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            /*
            *  Escape newline chars for being transmitted with JSON over the wire
            */
            function escapeNewLineChars(valueToEscape) {
                if (valueToEscape != null && valueToEscape != "") {
                    return valueToEscape.replace(/\n/g, "\\n");
                } else {
                    return valueToEscape;
                }
            }

            $(".AddNotes").click(function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var notes = $(this).attr("data-text");
                $('#txt_notes').val(notes);
                $('#<%=hfTempJVId.ClientID %>').val(obtainId);
            });

            $("#btnSave_Notes").click(function () {
                debugger;
                var jvId = $('#<%=hfTempJVId.ClientID %>').val();
                var Notes = $('#txt_notes').attr("value");

                Notes = escapeNewLineChars(Notes);

                if (Notes === '' || Notes === null) {
                    alert('notes is empty');
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/TempJournalVoucher_AddNotes',
                        data: '{"jvId" : "' + jvId + '","Notes" : "' + Notes + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            alert(data);

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });

                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.reload', function () {
                $('#<%= btnReload.ClientID %>').click();
            });
        });
    </script>
    <script type="text/javascript">


        function DisableDR() {

            var len = $('#<%= txtCR.ClientID %>').val().length;

            if (len > 0) {
                $('#<%= txtDR.ClientID %>').val("");
                $('#<%= txtDR.ClientID %>').html("");
                $('#<%= txtDR.ClientID %>').attr("disabled", "disabled");
            }
            else {
                $('#<%= txtDR.ClientID %>').removeAttr("disabled");
            }
        };

        function DisableCR() {

            var len = $('#<%= txtDR.ClientID %>').val().length;

            if (len > 0) {
                $('#<%= txtCR.ClientID %>').html("");
                $('#<%= txtCR.ClientID %>').val("");
                $('#<%= txtCR.ClientID %>').attr("disabled", "disabled");
            }
            else {
                $('#<%= txtCR.ClientID %>').removeAttr("disabled");
            }

        };
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.DeleteJV', function () {
                if (confirm("Are you sure to delete this vourcher item ?") == true) {
                    var obtainId = $(this).attr("data-id");
                    $('#<%=hfTempJVId.ClientID %>').val(obtainId);
                    $('#<%= btnDeleteJV.ClientID %>').click();

                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            window.Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {

                $(document).on('click', '.chequeentry', function () {
                    debugger;
                    var Userid = "<%= getSessionUserID() %>";
                    var sessionid = "<%= getSessionID() %>";
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/GetChequePayee',
                        data: '{"SessionID":"' + sessionid + '","UserID":"' + Userid + '"}',
                        dataType: "json",
                        contentType: "application/json",
                        success: function (data) {
                            $("#chqlst").html(data);
                        },
                        error: function (data) {
                            alert(data);
                        },
                        failure: function () {
                            alert('Data sending failed');
                        }
                    });

                });

                $(document).on('click', '.editchqno', function () {
                    var thisItem = $(this);
                    var obtainId = $(this).attr("data-id");
                    var newAmount = prompt("Enter ChequeNo", "");
                    console.log(newAmount);
                    if (newAmount === null || newAmount === false || newAmount === '') {
                        console.log("Cheque No is empty");
                        alert("Cheque No is empty");
                        return;
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            url: '/SetupServices.svc/UpdateChequeNoInTempJV',
                            data: '{"jvId":"' + obtainId + '","ChequeNo":"' + newAmount + '"}',
                            dataType: "json",
                            contentType: "application/json",
                            success: function (data) {
                                thisItem.html(newAmount);
                                alert(data);
                            },
                            error: function (data) {
                                alert(data);
                            },
                            failure: function () {
                                alert('Data sending failed');
                            }
                        });
                    }
                });

                $("#btn_clearcheques").click(function () {
                    $("#<%=txtChequeNo.ClientID %>").val('');
                    $("#<%=txt_cheque.ClientID %>").val('');
                });

                $("#btn_savecheques").click(function () {

                    var chequestoReconciliate = $("#<%=txtChequeNo.ClientID %>").val();

                    if (chequestoReconciliate.indexOf(',') != -1) {
                        var arrcheques = chequestoReconciliate.split(",");
                        var newarrchq = [];
                        $.each(arrcheques, function (key, value) {
                            //                                alert(value);
                            if (isNaN(value) || value == "") {
                                alert('Invalid data');
                            }
                            else {
                                newarrchq.push(value);
                            }
                        });

                        var validchqs = newarrchq.join(",");
                        //                            alert(validchqs);
                        $("#<%=txt_cheque.ClientID %>").val(validchqs);

                    }
                });

            });
        });
    </script>
    <style type="text/css">
        .input-group-addon
        {
    padding: 0px !important;
font-size: 14px;
font-weight: normal;
line-height: 1;
text-align: center;
background-color: #eee;
border: 1px solid #ccc;
}

.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child > .btn, .input-group-btn:first-child > .dropdown-toggle, .input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle) {
     height: 30px !important;
}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Code').keyup(function () {
                if ($(this).val() == 1) {
                    $('#MainContent_ddlGroup').val(48);
                } else if ($(this).val() > 0) {
                    $('#MainContent_ddlGroup').val(49);
                } else {
                    $('#MainContent_ddlGroup').val(50);
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Recent Entries</h4>
                    </div>
                    <div class="modal-body">
                        <div id="recentJournalVourcher" runat="server">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnCopy" runat="server" Text="Copy To" CssClass="btn blue" />
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="AddNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Add Notes
                        </h4>
                    </div>
                    <div class="modal-body">
                        <textarea id="txt_notes" cols="20" rows="2" class="form-control" placeholder="Write notes for this Account Head."></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue" id="btnSave_Notes">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ChequeEntry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Provide Cheque Numbers</h4>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="modal-body">
                                <div class="portlet-body form">
                                    <div class="form-horizontal">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3 control-label pull-center">
                                                            Pay To</label>
                                                        <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                            Amount</label>
                                                        <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                            Cheque No.</label>
                                                        <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3 pull-center">
                                                                <asp:TextBox ID="txtpayto" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3 pull-center">
                                                                <asp:TextBox ID="txtchqAmt" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3 pull-center">
                                                                <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox></div>
                                                            <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3 pull-right">
                                                                <button type="button" class="btn blue" id="btn_AddCheque">
                                                                    Add
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="chqlst">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button type="button" class="btn blue" id="btn_savecheques">
                            Save
                        </button>
                        <button type="button" class="btn default" id="btn_clearcheques">
                            Clear</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    POST VOUCHER <span class="pull-right"><span id="date_time">
                        <script type="text/javascript">                            window.onload = date_time('date_time');</script>
                    </span></span>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li id="recentJV"><a href="#">Recent Entries</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Accounts</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/Accounts/JournalVoucherEntry.aspx">Voucher</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Voucher Entry</div>
                        <marquee width="75%" behavior="scroll" scrollamount="5" direction="left" onmouseover="this.setAttribute('scrollamount',0);"
                            onmouseout="this.setAttribute('scrollamount',5);">
<span id="summary" class="pull-right" runat="server"></span></marquee>
                        <div class="tools">
                            <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                            </a><a href="" class="reload"></a><a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="col-md-6 control-label">
                                                Voucher No.
                                            </label>
                                            <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox runat="server" ID="txt_voucherNo" CssClass="form-control" placeholder="Enter Voucher Number">
                                                    </asp:TextBox>
                                                </div>
                                                <label for="<%= txt_voucherNo.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                              <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txt_VourcherDate_Nep" runat="server" CssClass="form-control" placeholder="Enter From Miti" ></asp:TextBox>
                                                </div>
                                                <label for="<%=txt_VourcherDate_Nep.ClientID%>" class="error" style="display: none;">
                                                </label>
                                              </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3 control-label pull-center">
                                                Group</label>
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                Sub-Group</label>
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                Voucher Type</label>
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                Voucher Date</label>
                                        </div>
                                        <div class="form-group">
                                            <%--   <label class="col-md-2 control-label">
                                                   Group :</label>--%>

                                                   
                                            <div class="col-md-3  col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                               <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control col-md-1" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%= ddlGroup.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <%--    <label class="col-md-2 control-label">
                                                  Sub Group :</label>--%>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlSubGroup" runat="server" CssClass="form-control" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                                <label class="error">
                                                    <small style="color: Red;" id="smallMessage" runat="server"></small>
                                                </label>
                                                <label for="<%= ddlSubGroup.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <%--    <label class="col-md-2 control-label">
                                                  Sub Group :</label>--%>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddl_voucherType" runat="server" CssClass="form-control" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%= ddl_voucherType.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <%--   <label class="col-md-2 control-label">
                                                  Sub Group :</label>--%>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txt_VourcherDate" runat="server" CssClass="form-control" placeholder="mm ddd yyyy (02 feb 2017)" onchange="ChangeDate(this.value,'n',this);">
                                                    </asp:TextBox>
                                                </div>
                                                <label for="<%= txt_VourcherDate.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                        </div>
                                      
                                        <div class="form-group">
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3 control-label pull-center">
                                                Account Head</label>
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3 control-label pull-center">
                                                Sub-Account Head</label>
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                DR</label>
                                            <label class="col-md-3 col-sm-6 col-lg-3 col-xs-3  control-label pull-center">
                                                CR</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlAccountHead" runat="server" CssClass="form-control" placeholder="Enter text"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%= ddlAccountHead.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlSubAccount" runat="server" CssClass="form-control" placeholder="Enter text">
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%= ddlSubAccount.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                                    <asp:TextBox ID="txtDR" runat="server" CssClass="form-control" placeholder="Enter DR Amount"
                                                        onkeyup="DisableCR()"></asp:TextBox>
                                                </div>
                                                <label for="<%= txtDR.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-lg-3 col-xs-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                                    <asp:TextBox ID="txtCR" runat="server" CssClass="form-control" placeholder="Enter CR Amount"
                                                        onkeyup="DisableDR()"></asp:TextBox>
                                                </div>
                                                <label for="<%= txtCR.ClientID%>" class="error" style="display: none">
                                                </label>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="btnAdd" class="btn purple" OnClientClick="return checkFromValidation();"
                                            runat="server" Text="Add Item" />
                                        <asp:Button ID="btnOld" class="btn purple" runat="server" Text="Old Journal Vourcher"
                                            Style="display: none" />
                                        <asp:Button ID="btnReload" runat="server" Text="Reload" class="btn purple hidden" />
                                        <asp:Button ID="btnDeleteJV" runat="server" Text="Delete" class="btn default hidden" />
                                        <asp:HiddenField ID="hfTempJVId" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Voucher Narration</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                        class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                            class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="portlet-body flip-scroll" id="listData" runat="server">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-sm-12 col-lg-12 col-xs-12 control-label pull-left bold">
                                                Narration</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                                <asp:TextBox ID="txtNarration" runat="server" CssClass="form-control" placeholder="Enter Narration"
                                                    TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-sm-12 col-lg-12 col-xs-12 control-label pull-left bold">
                                                Cheque No</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10 col-sm-8 col-lg-10 col-xs-10">
                                                <asp:TextBox ID="txt_cheque" runat="server" CssClass="form-control" placeholder="Enter Cheque No"
                                                    TextMode="SingleLine" Rows="3" Enabled="true"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-lg-2 col-xs-2 hidden">
                                                <a href='#ChequeEntry' data-toggle='modal' class='chequeentry btn purple btn-md'><span
                                                    class='glyphicon glyphicon-pencil'>Cheque Entry</span></a>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkNarration();"
                                                    runat="server" Text="Post Voucher" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
