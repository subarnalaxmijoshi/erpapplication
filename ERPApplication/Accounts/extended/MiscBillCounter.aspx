﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="MiscBillCounter.aspx.vb" Inherits="School.MiscBillCounter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
         <%= Neodynamic.SDK.Web.WebClientPrint.CreateScript(MyUtils.GetWebsiteRoot() + "Reports/ReceiptPrint.ashx?id=" + getSessionUserID())%>
    <link href="../../assets/Calendar/jquery.calendars.picker.css" rel="stylesheet" type="text/css" />
    <script src="../../assets/Calendar/jquery.plugin.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js" type="text/javascript"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js" type="text/javascript"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js"type="text/javascript"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../../assets/Calendar/jquery.calendars.nepali.js" type="text/javascript"></script>
    <script src="../../assets/scripts/shortcut.js" type="text/javascript"></script>

<style type="text/css">
     .topspacing 
     {  
         margin-top:15px;
     }
     .paymentlist
     {       
        margin-left: -10px;
        margin-right: 40px;
     }
    .particulars
    {  margin-left: -46px;
         margin-right: 200px;
    }
    .qty
    {
         margin-left: -50px;
         margin-right: 133px;
    }
    .rate
    {
         margin-left: -5px;
         margin-right: 40px;
    }
    
    
</style>

<script type="text/javascript">
    var checkFromValidation = false;
    var SaveFromValidation = false;
    $(document).ready(function () {
        $('#form').validate();

        checkFromValidation = function () {
            $("#<%=txtParticular.ClientID %>").rules('add', { required: true, messages: { required: 'Particular Name is required.'} });
            $("#<%=txtQuantity.ClientID %>").rules('add', { required: true,number:true, messages: { required: 'Quantity is required.'} });
            $("#<%=txtRate.ClientID %>").rules('add', { required: true,number:true, messages: { required: 'Amount is required.'} });
            var bool = true;

            if ($('#<%=txtQuantity.ClientID %>').valid() == false) bool = false;
            if ($('#<%=txtRate.ClientID %>').valid() == false) bool = false;
            if ($('#<%=txtParticular.ClientID %>').valid() == false) bool = false;
            if (!bool) $('#form').validate().focusInvalid();
            return bool;
        };

        SaveFromValidation = function () {

            $("#<%=txtpartyName.ClientID %>").rules('add', { required: true, messages: { required: 'Name is required.'} });
            $("#<%=txnDate.ClientID %>").rules('add', { required: true, messages: { required: 'Transaction Date is required.'} });
            var bool = true;
            if ($('#<%=txtpartyName.ClientID %>').valid() == false) bool = false;
            if ($('#<%=txnDate.ClientID %>').valid() == false) bool = false;
            if (!bool) $('#form').validate().focusInvalid();
            return bool;
        };
    });

    
    </script>

<script type="text/javascript">
    $(document).ready(function () {
        $(function () {
            var calendar = $.calendars.instance('nepali');
            $("#<%= txnDate.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });

        });

        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
            $(function () {
                var calendar = $.calendars.instance('nepali');
                $("#<%= txnDate.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });
            });

            getMiscParticularAmt();

            $(document).on('click', '.editMiscAmt', function () {
                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var sessionUserid = <%= getSessionUserID() %>;
                var newTheory = prompt("Enter Amount", "0");
                console.log(newTheory);
                if (newTheory === null || newTheory === false || newTheory === '') {
                    console.log("Amount is empty");
                    alert("Amount is empty");
                    return;
                } else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/edit_MiscAmount',
                        data: '{"assignId" : "' + obtainId + '","val" : "' + newTheory + '","sUserId" : "' + sessionUserid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            thisItem.html(newTheory);
                            alert(data);
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }

            });

              $(document).on('click', '.deleteMisAmt', function () {
                if (confirm("Are you sure to delete this item ?") == true) {
                    var obtainId = $(this).attr("data-id");

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/delete_MisAmount',
                        data: '{"assignId" : "' + obtainId + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            alert(data);
                            getMiscParticularAmt();

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }

            });
        });
    });
     </script>

<script type="text/javascript">
    function Particular_Populated(sender, e) {
        var particulars = sender.get_completionList().childNodes;
    }

    function OnParticular_Selected(source, eventArgs) {
        debugger;
        var idx = source._selectIndex;
        var particulars = source.get_completionList().childNodes;
        var particularid = particulars[idx]._value;
        var text = particulars[idx].firstChild.nodeValue;
        var particularname = text.split('~')[0]
        var particularamt = text.split('~')[1];


        source.get_element().value = particularname;
        $('#txtQuantity').val(1);
        $('#txtRate').val(particularamt);
        $('#<%= hdnParticularAccID.ClientID %>').val(particularid);
        $('#txtQuantity').focus();

    }

    function Students_Populated(sender, e) {
        var students = sender.get_completionList().childNodes;
    }

    function OnStudent_Selected(source, eventArgs) {
        debugger;
        var idx = source._selectIndex;
        var students = source.get_completionList().childNodes;
        var studentid = students[idx]._value;
        var text = students[idx].firstChild.nodeValue;
        var studentname = text.split('~')[0]
        var regno = text.split('~')[1];
        source.get_element().value = studentname;
        if (studentid !== null) {
            $('#<%= hdnStudentID.ClientID %>').val(studentid);
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/GetStudentInfoDetails',
                data: '{"studentID" : "' + studentid + '"}',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
//                    $('#model_data').html(data);
                    $("#stdDetails").text(data);
                },
                error: function (data) {
                    alert(data.statusText);
                },
                failure: function (data) {
                    alert(data.statusText);
                }


            });
        }
       
    }
        
    </script>

<script type="text/javascript">
//         function onEnterkeyPress(event,obj) {
//             debugger;
//             var id = obj.id;
//             if (event.keyCode == 13) {

//                 if (id = $('#txtQuantity')) {
//                     $('#txtRate').focus(5); 
//                 }
// 
//             }
//         }

//         $(document).ready(function () {
////             $("#txtQuantity").live('keypress', function (event) {
////                 if (event.keyCode == 13) {
////                     $('#txtRate').focus(); 
////                 }
////             });
//                
//             $("#txtRate").live('keypress', function (event) {
//                 if (event.keyCode == 13) {
//                     $("#<%=btnAdd.ClientID %>").click();
//                 }
//             });
//         });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".form-control").on("keypress", function (e) {

            if (e.keyCode == 13) {
                debugger;
                var next = $('[tabindex="' + (this.tabIndex + 1) + '"]');
                if (next.length > 0) {
                    if (next[0].type == "submit") {
                        $("#<%=btnAdd.ClientID %>").click();
                    } else {
                        next.focus();
                    }
                } else {
                    $('[tabindex="1"]').focus();
                }
                return false;
            }
        });

       
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        Sys.Extended.UI.AutoCompleteBehavior.prototype.set_enabled = function (value) {
            try {
                $removeHandler(this.get_element(), "keydown", this._keyDownHandler);
            } catch (error) { } //just escape error if handler already removed
            this._timer.set_enabled(!!value);
            if (value) {
                this._keyDownHandler = Function.createDelegate(this, this._onKeyDown);
            } else {
                this._keyDownHandler = Function.createDelegate(this, function () { });
            }
            $addHandler(this.get_element(), "keydown", this._keyDownHandler);
        };

        $("#chkIsExistingStudent").click(function () {
            if ($(this).is(":checked")) {
                $find("AutoCompleteEx5").set_enabled(1);
                $("#<%=txtpartyName.ClientID %>").attr("autocomplete", "on");
                $("#<%=txtpartyName.ClientID %>").focus();
            }
            else {
                $find("AutoCompleteEx5").set_enabled();
                $("#<%=txtpartyName.ClientID %>").attr("autocomplete", "off");
                $("#stdDetails").text("");
                $("#<%=txtpartyName.ClientID %>").focus();
            }

        });
    });
</script>



<script type="text/javascript">
function date_time(id) {
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    d = date.getDate();
    day = date.getDay();
    days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    h = date.getHours();
    if (h < 10) {
        h = "0" + h;
    }
    m = date.getMinutes();
    if (m < 10) {
        m = "0" + m;
    }
    s = date.getSeconds();
    if (s < 10) {
        s = "0" + s;
    }
    result = '' + days[day] + ', ' + months[month] + ' ' + d + ', ' + year + ', ' + h + ':' + m + ':' + s;
    document.getElementById(id).innerHTML = result;
    setTimeout('date_time("' + id + '");', '1000');
    return true;
}
        
</script>

<script type="text/javascript">
$(document).ready(function () {

    $(document).on('click', "#btnSave", function () {
        if (confirm("Are you Sure to Issue Bill?") == true) {
            $("#<%=hdnDirectPrint.ClientID %>").val("0");
            $("#<%=btnSubmit.ClientID %>").click();
            
        }
    });

    $(document).on('click', "#btnSavenPrint", function () {
        if (confirm("Are you Sure to Issue Bill?") == true) {
            $("#<%=hdnDirectPrint.ClientID %>").val("1");
            $("#<%=btnSubmit.ClientID %>").click();
                  
        }
    });

});
</script>


<script type="text/javascript">
    shortcut.add("alt+p", function () {
        $("#btnSavenPrint").click();
    });
    shortcut.add("alt+j", function () {
        $("#btnSave").click();
    });
    shortcut.add("alt+n", function () {
        $("#<%=txtNotes.ClientID %>").focus();
    }); 
</script>

  <script type="text/javascript">
      $(document).ready(function () {
          $("#btn_assignAmount").click(function () {
              var accountId = $('#<%= ddlMiscParticular.ClientID %>').val();
              var assignAmt = $('#<%= txtAssignAmount.ClientID %>').val();
              var branchid = <%= getSessionBranchID() %>;
              var sessionUserid = <%= getSessionUserID() %>;

              if (assignAmt === null || assignAmt === '' || assignAmt === 0) {
                  alert('Amount is required!');
              }
              else {
              $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/AssignMiscAmount',
                    data: '{"AccountID":"' + accountId + '","Amount":"' + assignAmt + '","BranchID" : "' + branchid + '","UserID" : "' + sessionUserid + '"}',
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                        alert(data);                       
                    },
                    error: function (data) {
                        alert(data);
                    },
                    failure: function () {
                        alert('Data sending failed');
                    }
                });

              }
          });

          $("#btn_addNewAccount").click(function () {
              var accountName = $('#<%= txtAccountName.ClientID %>').val();
              var amountToAssign = $('#<%= txtAmountToAssign.ClientID %>').val();
              var branchid = <%= getSessionBranchID() %>;
              var sessionUserid = <%= getSessionUserID() %>;

              if(confirm("Are you sure to create new account?") == true)
              {
               if (amountToAssign === null || amountToAssign === '' || amountToAssign === 0 || isNaN(amountToAssign)) {
                  alert('Invalid Amount');
              }
              else if(accountName === null || accountName === '' || accountName === 0){
                 alert('Invalid Account Name');
              }
              else {
              $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/CreateMiscAccount',
                    data: '{"AccountName":"' + accountName + '","Amount":"' + amountToAssign + '","BranchID" : "' + branchid + '","UserID" : "' + sessionUserid + '"}',
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                        alert(data);                       
                    },
                    error: function (data) {
                        alert(data);
                    },
                    failure: function () {
                        alert('Data sending failed');
                    }
                });

              }

              }

          });
      });
  </script>
   <script type="text/javascript">
       function getMiscParticularAmt() {
         var branchid = <%= getSessionBranchID() %>;
          debugger;
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/GetMiscParticularAmount',
                data: '{"BranchID" : "' + branchid + '"}',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    $('#model_data').html(data);
                   
                },
                error: function (data) {
                    alert(data.statusText);
                },
                failure: function (data) {
                    alert(data.statusText);
                }


            });

       }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Assign Amount In Misc. Bill Particulars</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
                            <div id="msg" runat="server">
                            </div>
                            <div class="form-group">
                                <label for="ddlMiscParticular" class="col-md-4 control-label">
                                    Particular :</label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlMiscParticular" runat="server" class="form-control" >
                                    </asp:DropDownList>
                                </div>
                                 </div>
                            <div class="form-group">
                                <label for="txtAssignAmount" class="col-md-4 control-label">
                                    Amount :</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtAssignAmount" runat="server" CssClass="form-control" placeholder="Enter Amount To Assign" MaxLength="7"
                                        Text="0"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue" id="btn_assignAmount">
                            Save </button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="viewAmtInMiscIncome" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Amounts Assigned In Misc. Bill Particulars</h4>
                    </div>
                    <div class="modal-body">
                        <div id="model_data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue hidden">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addNewAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                           Add New Account for Misc. Bill</h4>
                    </div>
                   <div class="modal-body">
                        <div class="form-body">
                            <div id="msg2" runat="server">
                            </div>
                            <div class="form-group">
                                <label for="txtAccountName" class="col-md-4 control-label">
                                    AccountName :</label>
                                <div class="col-md-8">
                                     <asp:TextBox ID="txtAccountName" runat="server" CssClass="form-control" placeholder="Enter AccountName"
                                        Text="" MaxLength="50"></asp:TextBox>
                                </div>
                                 </div>
                            <div class="form-group">
                                <label for="txtAmountToAssign" class="col-md-4 control-label">
                                    Amount :</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtAmountToAssign" runat="server" CssClass="form-control" placeholder="Enter Amount To Assign"
                                        Text="0" MaxLength="7"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue" id="btn_addNewAccount">
                            Save </button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Misc. Bill Counter<small></small>
                    <span class="pull-right"><span id="date_time">
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                         <ul class="dropdown-menu pull-right" role="menu">                       
                            <li><a data-toggle='modal'  href='#portlet-config' class='assignMiscAmount'>Assign Amount</a></li>
                             <li><a data-toggle='modal'  href='#viewAmtInMiscIncome'>Edit Assigned Amount</a></li>
                             <li><a data-toggle='modal' href='#addNewAccount'>Add New Account</a></li>
                        </ul>
                        
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Account</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/Accounts/extended/MiscBillCounter.aspx">Misc. Bill Counter</a><i class="fa fa-angle-right"></i></li>
                    <li><span id="billingSummary" runat="server" class='bold'></span></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <asp:HiddenField ID="hdnParticularAccID" runat="server" />
            <asp:HiddenField ID="hdnStudentID" runat="server" />
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Misc. Billing
                        </div>
                        <marquee width="85%" behavior="scroll" scrollamount="5" direction="left" onmouseover="this.setAttribute('scrollamount',0);"
                            onmouseout="this.setAttribute('scrollamount',5);">
                       <span id="stdDetails" class="pull-right" ></span>
                      </marquee>
                        <div class="tools">
                            <a href="" class="collapse"></a><a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                 
                                    <label class="col-md-1 control-label">
                                        Name :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><%--<i class="fa fa-user"></i>--%>
                                             <input id="chkIsExistingStudent" type="checkbox" title="Search Student In Database When Tick Marked" checked/>
                                            </span>
                                            <asp:TextBox ID="txtpartyName" runat="server" CssClass="form-control" placeholder="Enter Name of Student or party"
                                                TabIndex="1"></asp:TextBox>
                                            <asp:AutoCompleteExtender ServiceMethod="SearchStudentDetails" MinimumPrefixLength="3"
                                            ServicePath="../../Autocomplete.asmx" CompletionInterval="100" EnableCaching="true" UseContextKey="true"
                                            CompletionSetCount="10" TargetControlID="txtpartyName" ID="AutoCompleteExtender5" 
                                            runat="server" FirstRowSelected="true" CompletionListCssClass="autocomplete_completionListElement"
                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            BehaviorID="AutoCompleteEx5" ShowOnlyCurrentWordInCompletionListItem="true" OnClientPopulated="Students_Populated" OnClientItemSelected="OnStudent_Selected"> 
                                                </asp:AutoCompleteExtender>
                                        </div>
                                        <label for="<%=txtpartyName.ClientID%>" class="error" style="display: none;"></label>
                                    </div>
                                     <div class="col-md-2">
                                         <asp:DropDownList ID="ddlProgram" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="">Select Program</asp:ListItem>
                                          <asp:ListItem Value="1">Regular</asp:ListItem>
                                           <asp:ListItem Value="2">Internal</asp:ListItem>
                                         </asp:DropDownList>
                                     </div>
                                    <label class="col-md-1 control-label">
                                        Date :</label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txnDate" runat="server" CssClass="form-control" placeholder="Enter Date"
                                                ></asp:TextBox>
                                        </div>
                                        <label for="<%=txnDate.ClientID%>" class="error" style="display: none;"></label>
                                    </div>
                                   
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkSetPersonalize" runat="server" Text=" Static Entries" AutoPostBack="false" ToolTip="Static Misc. Particulars From Previous Entries"/>
                                </div>
                            </div>
                             

                                <div class="col-md-12 clearfix">
                                    <fieldset class="scheduler-border form-control">
                                      
                                         
                                           
                                        <div class="form-group topspacing">
                                           <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                            <label class="col-md-2 control-label" style="margin-left: -20px;">
                                                Payment Mode :</label>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <asp:DropDownList ID="ddlPaymentMode" runat="server" CssClass="form-control paymentlist"
                                                       >
                                                    </asp:DropDownList>
                                                </div>
                                                <label for="<%=ddlPaymentMode.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtParticular" runat="server" CssClass="form-control particulars"
                                                        TabIndex="2" placeholder="Enter particular" autocomplete="on"></asp:TextBox>
                                                    <asp:AutoCompleteExtender ServiceMethod="SearchMiscParticular" MinimumPrefixLength="1"
                                                        ServicePath="../../Autocomplete.asmx" CompletionInterval="100" EnableCaching="true"
                                                        UseContextKey="true" CompletionSetCount="10" TargetControlID="txtParticular"
                                                        ID="AutoCompleteExtender4" runat="server" FirstRowSelected="true" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        BehaviorID="AutoCompleteEx4" ShowOnlyCurrentWordInCompletionListItem="true" OnClientPopulated="Particular_Populated"
                                                        OnClientItemSelected="OnParticular_Selected">
                                                    </asp:AutoCompleteExtender>
                                                </div>
                                                <label for="<%=txtParticular.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control qty" placeholder="quantity"
                                                        ClientIDMode="Static" TabIndex="3"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtQuantity.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtRate" runat="server" CssClass="form-control rate" placeholder="Enter rate"
                                                        ClientIDMode="Static" TabIndex="4"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtRate.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                             </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn purple" 
                                                TabIndex="5" />
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-12 topspacing">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="grdviewMiscIncome" runat="server" CssClass="table table-bordered  table-condensed flip-content"
                                                AutoGenerateColumns="False" DataKeyNames="RowID" OnRowDeleting="grdviewMiscIncome_RowDeleting"
                                                ShowFooter="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SN" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RowID" HeaderText="RowID" Visible="false"></asp:BoundField>
                                                    <asp:BoundField DataField="Particular" HeaderText="Particulars"></asp:BoundField>
                                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity"></asp:BoundField>
                                                    <asp:BoundField DataField="Rate" HeaderText="Rate"></asp:BoundField>
                                                    <asp:BoundField DataField="Amount" HeaderText="Amount"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="15%">
                                                        <ItemTemplate>
                                                            <span onclick="return confirm('Are you sure want to delete?')">
                                                                <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CommandName="Delete"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                                <HeaderStyle BackColor="LightGray" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <RowStyle HorizontalAlign="Center" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                         <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="chkSetPersonalize" EventName="CheckedChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group topspacing">
                                 <label class="col-md-2 control-label" style="margin-left: -62px;" accesskey="N">
                                        Notes :</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                            <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" placeholder="Write Notes"
                                                ></asp:TextBox>
                                        </div>
                                        <label for="<%=txtNotes.ClientID%>" class="error" style="display: none;"></label>
                                    </div>
                                    <div class="col-md-2">
                                        <input id="chkIsNotesPrint" type="checkbox" class='form-control' runat="server"/> Notes Print On Bill <br />
                                        <input id="chkNoBillPrint" type="checkbox" class='form-control  hidden'/> <%--No Bill Print--%>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <input id="btnSave" type="button" value="Preview and Print" class="btn blue"/>
                                        <input id="btnSavenPrint" type="button" value="Pay Now"  class="btn blue" />
                                        <asp:Button ID="btnSubmit" class="btn blue hidden" OnClientClick="return SaveFromValidation();"
                                            runat="server" Text="Print Bill" />
                                        <asp:HiddenField ID="hdnDirectPrint" runat="server" />
                                          <input type="button" id="prntRcpt" style="font-size: 18px; display: none;" value="Print Receipt"
                                            onclick="return prntRcpt_onclick()" />
                                         <span class="pull-right">Shortcuts: Alt+P >> Pay Now <br />
                                          &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Alt+N >> Notes <br />
                                          &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Alt+J >> Preview and Print
                                          </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="listData" runat="server">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function prntRcpt_onclick() {        
            javascript: jsWebClientPrint.print();
            window.location.href ="/Accounts/extended/MiscBillCounter.aspx";
        }
</script>
</asp:Content>
