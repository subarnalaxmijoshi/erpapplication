﻿Imports System.IO
Imports System.Web.HttpContext
Imports System.Globalization
Public Class MiscBillCounter
    Inherits System.Web.UI.Page
    Dim Dao As New DatabaseDao
    Dim DC As New DateConverter()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim today As String = Date.Now.ToString("dd MMM yyyy")
            txnDate.Text = DC.ToBS(Convert.ToDateTime(today), "n")
            txtpartyName.Focus()
            GetPaymentMode()
            chkSetPersonalize.Checked = True
            chkSetPersonalize_OnCheckChanged(sender, e)
            LoadAccParticular()
            GetBillingSummary()
        End If
    End Sub

    Protected Sub GetBillingSummary()
        Dim ds As New DataSet
        Dim sql As String = ""
        sql = "exec [Accounts].[usp_MiscIncome] @flag='summary',@BranchID='" & Session("BranchID") & "',@UserId='" & Session("UserID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            billingSummary.InnerHtml = "Today's Total Bills: "
            billingSummary.InnerHtml += ds.Tables(0).Rows(0).Item("totalentries").ToString.Trim
            billingSummary.InnerHtml += " | Today's Total Collection:(Rs.) "
            billingSummary.InnerHtml += ds.Tables(0).Rows(0).Item("totalcollection").ToString.Trim
        End If
    End Sub

    Protected Sub LoadAccParticular()
        Dim ds As New DataSet
        Dim sql As String = ""
        sql = "exec [Accounts].[usp_MiscIncome] @flag='getaccounts',@BranchID='" & Session("BranchID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlMiscParticular.DataSource = ds.Tables(0)
            ddlMiscParticular.DataValueField = "AccountID"
            ddlMiscParticular.DataTextField = "AccountName"
            ddlMiscParticular.DataBind()
            ddlMiscParticular.Items.Insert(0, New ListItem("-- Select Misc Particular Account -- ", ""))
        Else
            ddlMiscParticular.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If

    End Sub


    Protected Function getSessionUserID() As String
        Return Session("userID").ToString
    End Function

    Protected Function getSessionBranchID() As String
        Return Session("BranchID").ToString
    End Function

    Protected Sub chkSetPersonalize_OnCheckChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkSetPersonalize.CheckedChanged
        If chkSetPersonalize.Checked = True Then
            LoadMiscBillParticular()
        End If

    End Sub

    Private Sub GetPaymentMode()
        Dim ds As New DataSet
        Dim sql As String = ""
        ddlPaymentMode.Items.Clear()
        sql = "EXEC Accounts.usp_CounterBill @FLAG='PM',@BranchID='" & Session("BranchID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlPaymentMode.DataSource = ds.Tables(0)
            ddlPaymentMode.DataValueField = "AccountID"
            ddlPaymentMode.DataTextField = "AccountName"
            ddlPaymentMode.DataBind()

        End If
    End Sub

    Private Sub SetInitialRow()

        Dim dt As New DataTable()
        Dim dr As DataRow = Nothing

        dt.Columns.Add(New DataColumn("SN", GetType(String)))
        dt.Columns.Add(New DataColumn("RowID", GetType(String)))
        dt.Columns.Add(New DataColumn("Particular", GetType(String)))
        dt.Columns.Add(New DataColumn("Quantity", GetType(String)))
        dt.Columns.Add(New DataColumn("Amount", GetType(String)))

        dr = dt.NewRow()

        dr("SN") = 1
        dr("RowID") = String.Empty
        dr("Particular") = String.Empty
        dr("Quantity") = String.Empty
        dr("Amount") = String.Empty
        dt.Rows.Add(dr)

        'Store the DataTable in ViewState
        ViewState("MiscParticulars") = dt


        grdviewMiscIncome.DataSource = dt
        grdviewMiscIncome.DataBind()


    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim ds As New DataSet
        Dim sql As String = ""
        sql = "exec [Accounts].[usp_MiscIncome] @flag='i',@particularAccId='" & hdnParticularAccID.Value & "',@quantity='" & Dao.FilterQuote(txtQuantity.Text) & "',@amount='" & Dao.FilterQuote(txtRate.Text) & "'"
        sql += ",@BranchID='" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "',@SESSIONID='" & Session.SessionID & "',@UserId='" & Session("UserID") & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            'message.InnerHtml = ds.Tables(0).Rows(0)("msg")
            LoadMiscBillParticular()
            ClearFields()
        End If

        'If ViewState("MiscParticulars") IsNot Nothing Then
        '    Dim dtMisc As DataTable = DirectCast(ViewState("MiscParticulars"), DataTable)
        '    Dim drMisc As DataRow = Nothing

        '    Dim i As Integer = dtMisc.Rows.Count

        '    drMisc("SN") = i + 1
        '    drMisc("RowID") = ++1
        '    drMisc("Particular") = txtParticular.Text.Trim
        '    drMisc("Quantity") = txtQuantity.Text.Trim
        '    drMisc("Amount") = txtRate.Text.Trim

        '    dtMisc.Rows.Add(drMisc)
        '    ViewState("MiscParticulars") = dtMisc

        '    grdviewMiscIncome.DataSource = dtMisc
        '    grdviewMiscIncome.DataBind()

        'End If
    End Sub


    Private Sub LoadMiscBillParticular()
        Dim ds As New DataSet
        Dim sql As String = ""

        sql = "exec [Accounts].[usp_MiscIncome] @flag='s',@SESSIONID='" & Session.SessionID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            grdviewMiscIncome.Visible = True
            grdviewMiscIncome.DataSource = ds.Tables(0)
            grdviewMiscIncome.DataBind()
            'txtParticular.Focus()
        Else
            grdviewMiscIncome.Visible = False
        End If

    End Sub

    Protected Sub grdviewMiscIncome_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim ds As New DataSet
        Dim sql As String = ""

        Dim rowId As String
        rowId = e.RowIndex

        Dim tempid = grdviewMiscIncome.DataKeys(rowId).Value.ToString

        sql = "exec [Accounts].[usp_MiscIncome] @flag='d',@SESSIONID='" & Session.SessionID & "',@RowID='" & tempid & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows(0).Item("code").ToString = "0" Then
            'message.InnerHtml = ds.Tables(0).Rows(0)("msg")
            LoadMiscBillParticular()
        End If
    End Sub

    Protected Sub grdviewMiscIncome_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdviewMiscIncome.DataBound
        Dim total As Decimal = 0
        Dim qtotal As Integer = 0

        For Each r As GridViewRow In grdviewMiscIncome.Rows
            If r.RowType = DataControlRowType.DataRow Then
                total += Convert.ToDecimal(r.Cells(5).Text)
                qtotal += Convert.ToInt32(r.Cells(3).Text)
            End If
        Next
        grdviewMiscIncome.FooterRow.Cells(2).Text = "Total"
        grdviewMiscIncome.FooterRow.Cells(3).Text = qtotal.ToString()
        grdviewMiscIncome.FooterRow.Cells(5).Text = Math.Round(total, 0).ToString("N2")
    End Sub

    Private Sub ClearFields()
        txtParticular.Text = String.Empty
        txtQuantity.Text = String.Empty
        txtRate.Text = String.Empty
        txtParticular.Focus()
    End Sub

    Private Sub ClearAll()
        txtpartyName.Text = String.Empty
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim ds As New DataSet
        Dim sql As String = ""
        message.InnerHtml = ""
        Dim nepdt() = txnDate.Text.ToString().Split("/")
        Dim transDate As String = DC.ToAD(nepdt(0), nepdt(1), nepdt(2))
        'Dim transDate As String = DC.ToAD(Convert.ToDateTime(txnDate.Text), "e")

        sql = "EXEC [Accounts].[usp_MiscIncome] @flag='FinalSave',@PARTY='" & Dao.FilterQuote(txtpartyName.Text) & "',@USERID = '" & Session("userId") & "'"
        sql += ",@SESSIONID='" & Session.SessionID & "',@TXNDATE='" & Helper.GetFrontToDbDate(transDate) & "'"
        sql += ",@CompanyID='" & Session("CompanyID").ToString & "',@BranchID='" & Session("BranchID").ToString & "', @FiscalYearID='" & Session("FiscalYearID").ToString & "',@AcademicYearID='" & Session("AcademicYearID").ToString & "'"
        sql += ",@paymentmode='" & ddlPaymentMode.SelectedItem.Text & "',@paymentaccountid='" & ddlPaymentMode.SelectedValue & "'"
        sql += ",@Notes=N'" & Dao.FilterQuote(txtNotes.Text.Trim) & "'"
        sql += ",@IsPersonalized='" & chkSetPersonalize.Checked & "'"

        If Not String.IsNullOrWhiteSpace(hdnStudentID.Value) Then
            sql += ",@StudentID='" & hdnStudentID.Value & "'"
            sql += ",@ProgramID='" & ddlProgram.SelectedValue & "'"
        End If

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            If (ds.Tables(0).Rows(0).Item("CODE").ToString.Trim = "0") Then
                If hdnDirectPrint.Value = "1" Then
                    'Direct Bill Print
                    BillPrint(ds.Tables(0).Rows(0)("ref").ToString.Trim, Convert.ToString(chkIsNotesPrint.Checked).ToLower)
                    'Response.Redirect("/Accounts/extended/MiscBillCounter.aspx")
                Else
                    'Bill Preview and Print
                    Dim IsNotesPrint As String = "&IsNotesPrint=" + Convert.ToString(chkIsNotesPrint.Checked).ToLower
                    Response.Redirect(ds.Tables(0).Rows(0)("msg") + IsNotesPrint)                  
                End If
            Else
                message.InnerHtml = ds.Tables(0).Rows(0).Item("msg").ToString.Trim
            End If
        End If
       
        

    End Sub


    Private Sub BillPrint(ByVal refNo As String, ByVal IsNotesPrint As String)
        Dim Ref As String = refNo
        Dim BranchID As String = Session("BranchID").ToString
        Dim CompanyID As String = Session("CompanyID").ToString
        Dim FiscalYearID As String = Session("FiscalYearID").ToString
        Dim dbName As String = Session("DatabaseName").ToString.Trim()

        Dim obj As New BilllToPrint
        If (IsCumulative() = "True") Then
            Select Case dbName
                Case "JanaBhawanaCampus"
                    obj.MiscIncomeBILL_JanaBhawanaCampus(Ref, FiscalYearID, BranchID, CompanyID, Session("userID").ToString, Session("username").ToString)
                Case "PokharaNurshingCampus"
                    obj.MiscIncomeBILL_PNC(Ref, FiscalYearID, BranchID, CompanyID, Session("userID").ToString, Session("username").ToString)
                Case "KoteshworCampus2075"
                    obj.MiscIncomeBILL_KoteshworCampus(Ref, FiscalYearID, BranchID, CompanyID, Session("userID").ToString, Session("username").ToString)
                Case Else
                    obj.MiscIncomeBILL_new(Ref, FiscalYearID, BranchID, CompanyID, Session("userID").ToString, Session("username").ToString, IsNotesPrint)
            End Select
        Else
            obj.MiscIncomeBILL_PulchowkCampus(Ref, FiscalYearID, BranchID, CompanyID, Session("userID").ToString, Session("username").ToString)
        End If
        ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "PrintNewBillScript", " $('#prntRcpt').click();", True)
    End Sub

    Protected Function IsCumulative() As String
        Try
            Dim sql = "exec [Accounts].[usp_preference] @flag='a'"
            Dim ds As New DataSet
            ds.Clear()
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds.Tables(0).Rows(0).Item("IsCumulativeDueEnable").ToString
            Else
                Return ""
            End If
        Catch ex As Exception

        End Try
    End Function
End Class