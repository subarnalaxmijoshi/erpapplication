﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="IncomeReportNew.aspx.vb" Inherits="School.IncomeReportNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

 <link rel="stylesheet" href="../../assets/Calendar/jquery.calendars.picker.css" type="text/css"/>
   <link href="../../assets/datatableplugins/css/buttons.dataTables.min.css" rel="stylesheet"
        type="text/css" />
  
    <script src="../../assets/Calendar/jquery.plugin.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js" type="text/javascript"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js" type="text/javascript"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../../assets/Calendar/jquery.calendars.nepali.js" type="text/javascript"></script>
    

    <script src="../../assets/datatableplugins/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/ajax/jszip.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/ajax/pdfmake.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/ajax/vfs_fonts.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.print.min.js" type="text/javascript"></script>
     

    <script type="text/javascript">


        var checkFromValidation = false;
        $(document).ready(function () {



            $(function () {
                var calendar = $.calendars.instance('nepali');

                $("#<%= txtDateFrom_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", defaultDate: '0w', onSelect: function (selectedDate) {
                    ChangeDate(selectedDate, 'e', this);
                }
                });
                $("#<%= txtDateTo_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", defaultDate: '0w', onSelect: function (selectedDate) {
                    ChangeDate(selectedDate, 'e', this);
                }
                });
            });


            $("#<%=txtDateFrom.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            $("#<%=txtDateTo.ClientID %>").datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();



            $("#<%=lblLoading.ClientID %>").hide();
            $('#form').validate();
            $("#<%=txtDateFrom.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Report Filtration Start Date is required.', date: 'Enter valid date'} });
            $("#<%=txtDateTo.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Report Filtration End Date is required.', date: 'Enter valid date'} });

            checkFromValidation = function () {
                $("#<%=lblLoading.ClientID %>").show();
                $("#<%=btnSearch.ClientID %>").hide();
                var bool = true;
                if ($('#<%=txtDateFrom.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtDateTo.ClientID %>').valid() == false) bool = false;
                if (!bool) {
                    $("#<%=lblLoading.ClientID %>").hide();
                    $("#<%=btnSearch.ClientID %>").show();

                    $('#form').validate().focusInvalid();
                }
                return bool;
            };

        });

    </script>

    <script type="text/javascript">
        function formatDate(date, type) {

            console.log("date : " + date + " ,type : " + type);
            var month = '';
            var day = '';
            var year = '';
            var resultdate;
            if (type == "n") {
                var d = new Date(date);
                month = d.getMonth() + 1;
                day = d.getDate();
                year = d.getFullYear();
                resultdate = [day, month, year].join('/');
            }
            else {

                year = date[0]._year;
                month = date[0]._month;
                day = date[0]._day;
                resultdate = [year, month, day].join('/');
            }
            console.log("formatted : " + resultdate);
            return resultdate;

        }



        function ChangeDate(value, obj, element) {
            debugger;
            value = formatDate(value, obj);
            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';

            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/DateConverterNew',
                data: dataToSend,
                datatype: 'json',
                contentType: 'application/json',
                success: function (data) {
                    debugger;
                    var dd, mm, yy;
                    var value = [];
                    if (obj == "e") {
                        var fieldId1 = element.id.replace(/_Nep$/i, '');
                        //                        $("#" + fieldId1).val(data);
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId1).val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                    }
                    else if (obj == "n") {
                        var fieldId2 = element.id + '_Nep'
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId2).val(yy + "/" + mm + "/" + dd);
                    }
                }
            });

        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#displaytable1').DataTable({
                dom: 'Bfrtip',
                sorting: false,
                lengthMenu: [[10, 25, 50, 1000], ['10 rows', '25 rows', '50 rows', 'Show All']],
                buttons: [
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore'],
                    collectionLayout: 'fixed four-column',
                    columns: ':not(.noVis)'
                },

                { extend: 'excelHtml5',
                    title: 'Regular Income Report',
                    autoFilter: false,
                    sheetName: 'Regular Income',
                    messageTop: 'Regular income report from ' + $('#<%= txtDateFrom_Nep.ClientID%>').val() + ' to ' + $('#<%= txtDateTo_Nep.ClientID%>').val() + ' .',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                   extend: 'print',
                   title: 'Regular Income Report',
                   orientation: 'landscape',
                    messageTop: 'Regular income report from ' + $('#<%= txtDateFrom_Nep.ClientID%>').val() + ' to ' + $('#<%= txtDateTo_Nep.ClientID%>').val() + ' .',
                    exportOptions: {
                        columns: ':visible'
                    }
               }
               , 'pageLength'
            ]

            });

            var table2 = $('#displaytable2').DataTable({
                dom: 'Bfrtip',
                lengthMenu: [[10, 25, 50, 1000], ['10 rows', '25 rows', '50 rows', 'Show All']],
                buttons: [
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore'],
                    collectionLayout: 'fixed four-column',
                    columns: ':not(.noVis)'
                },
                { extend: 'excelHtml5',
                    title: 'Misc Income Report',
                    autoFilter: false,
                    sheetName: 'Misc Income',
                    messageTop: 'Misc income report from ' + $('#<%= txtDateFrom_Nep.ClientID%>').val() + ' to ' + $('#<%= txtDateTo_Nep.ClientID%>').val() + ' .',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                extend: 'print',
                title: 'Misc Income Report',
                orientation: 'landscape',
                messageTop: 'Misc income report from ' + $('#<%= txtDateFrom_Nep.ClientID%>').val() + ' to ' + $('#<%= txtDateTo_Nep.ClientID%>').val() + ' .',
                    exportOptions: {
                    columns: ':visible'
                    }
                }
                , 'pageLength'
            ]

            });

            var table3 = $('#displaytable3').DataTable({
                dom: 'Bfrtip',
                lengthMenu: [[10, 25, 50, 1000], ['10 rows', '25 rows', '50 rows', 'Show All']],
                buttons: [
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore'],
                    collectionLayout: 'fixed four-column',
                    columns: ':not(.noVis)'
                },

                { extend: 'excelHtml5',
                    title: 'Merge Income Report',
                    sheetName: 'Merge Income',
                    messageTop: 'Merge income report from ' + $('#<%= txtDateFrom_Nep.ClientID%>').val() + ' to ' + $('#<%= txtDateTo_Nep.ClientID%>').val() + ' .',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    title: 'Merge Income Report',
                    orientation: 'landscape',
                    messageTop: 'Merge income report from ' + $('#<%= txtDateFrom_Nep.ClientID%>').val() + ' to ' + $('#<%= txtDateTo_Nep.ClientID%>').val() + ' .',
                    exportOptions: {
                        columns: ':visible'
                    }
                }
               , 'pageLength'
            ]

            });


        });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Income Reports<small>View reports for income vouchers.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                   
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Account</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/accounts/extended/IncomeReportNew.aspx">Income Reports</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Income Reports
                        </div>
                        <div class="tools">
                            <a href="" class="collapse"></a><a href="" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Date From :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" placeholder="Enter Date from where report should be generated"
                                                onchange="ChangeDate(this.value,'n',this);"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateFrom.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateFrom_Nep" runat="server" CssClass="form-control" placeholder="Enter Nepali Date(Miti)"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateFrom_Nep.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Date To :</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" placeholder="Enter Date Upto which report should be generated"
                                                onchange="ChangeDate(this.value,'n',this);"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateTo.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <asp:TextBox ID="txtDateTo_Nep" runat="server" CssClass="form-control" placeholder="Enter Nepali Date(Miti)"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtDateTo_Nep.ClientID%>" class="error" style="display: none;">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="btnSearch" class="btn purple" OnClientClick="return checkFromValidation();"
                                            runat="server" Text="Search" />
                                        <asp:Label ID="lblLoading" runat="server" Text="Loading ...." class="btn purple"
                                            disabled></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="message" runat="server">
                </div>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab"
                            data-toggle="tab">Regular</a></li>
                        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">
                            Misc</a></li>
                        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">
                            Mixed/Merge</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Regular Billing</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                        class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                            class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll" id="listData" runat="server">
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab2">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Misc Billing</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                        class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                            class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll" id="listDataMisc" runat="server">
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab3">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Merge Billing</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a><a href="#portlet-config" data-toggle="modal"
                                        class="config"></a><a href="javascript:;" class="reload"></a><a href="javascript:;"
                                            class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll" id="listDataMerge" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
