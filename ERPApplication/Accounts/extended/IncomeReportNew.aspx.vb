﻿Imports System.IO
Imports System.Web.HttpContext
Imports System.Globalization
Public Class IncomeReportNew
    Inherits System.Web.UI.Page
    Dim Dao As New DatabaseDao

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Dim fDate = Helper.GetFrontToDbDate(txtDateFrom.Text)
        Dim tDate = Helper.GetFrontToDbDate(txtDateTo.Text)
        GetIncomeReportRegular(fDate, tDate)
        GetIncomeReportMisc(fDate, tDate)
        GetIncomeReportMerge(fDate, tDate)
    End Sub

    Private Sub GetIncomeReportRegular(ByVal dateFrom As String, ByVal dateTo As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Dim sql As String = ""
        listData.InnerHtml = ""
        message.InnerHtml = ""

        sql = "exec [Accounts].[usp_IncomeReportRegular] @DateFrom='" & dateFrom & "',@DateTo='" & dateTo & "'"
        sql += ",@BranchID= '" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "'"

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Columns(0).ColumnName.ToLower() = "errorcode" Then
                    sb.AppendLine("<div class='note note-danger'>")
                    sb.AppendLine("<div class='close-note'>x</div>")
                    sb.AppendLine("<p>")
                    sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString().Trim)
                    sb.AppendLine("</p>")
                    sb.AppendLine("</div>")
                    listData.InnerHtml = sb.ToString
                    Return
                End If
                listData.InnerHtml = Generatereport(ds, dateFrom, dateTo, "Regular", "displaytable1")

            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No information found for given date range.")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Error : " + ex.Message)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

        End Try
    End Sub

    Private Sub GetIncomeReportMisc(ByVal dateFrom As String, ByVal dateTo As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Dim sql As String = ""
        listDataMisc.InnerHtml = ""
        message.InnerHtml = ""

        sql = "exec [Accounts].[usp_IncomeReportMisc] @DateFrom='" & dateFrom & "',@DateTo='" & dateTo & "'"
        sql += ",@BranchID= '" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "'"

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Columns(0).ColumnName.ToLower() = "errorcode" Then
                    sb.AppendLine("<div class='note note-danger'>")
                    sb.AppendLine("<div class='close-note'>x</div>")
                    sb.AppendLine("<p>")
                    sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString().Trim)
                    sb.AppendLine("</p>")
                    sb.AppendLine("</div>")
                    listDataMisc.InnerHtml = sb.ToString
                    Return
                End If
                listDataMisc.InnerHtml = Generatereport(ds, dateFrom, dateTo, "Misc", "displaytable2")

            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No information found for given date range.")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Error : " + ex.Message)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

        End Try
    End Sub

    Private Sub GetIncomeReportMerge(ByVal dateFrom As String, ByVal dateTo As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Dim sql As String = ""
        listDataMerge.InnerHtml = ""
        message.InnerHtml = ""

        sql = "exec [Accounts].[usp_IncomeReport] @DateFrom='" & dateFrom & "',@DateTo='" & dateTo & "'"
        sql += ",@BranchID= '" & Session("BranchID") & "',@FiscalYearID='" & Session("FiscalYearID") & "'"

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Columns(0).ColumnName.ToLower() = "errorcode" Then
                    sb.AppendLine("<div class='note note-danger'>")
                    sb.AppendLine("<div class='close-note'>x</div>")
                    sb.AppendLine("<p>")
                    sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString().Trim)
                    sb.AppendLine("</p>")
                    sb.AppendLine("</div>")
                    listDataMerge.InnerHtml = sb.ToString
                    Return
                End If
                listDataMerge.InnerHtml = Generatereport(ds, dateFrom, dateTo, "Merge", "displaytable3")

            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No information found for given date range.")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("Error : " + ex.Message)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

        End Try
    End Sub

    Protected Function Generatereport(ByVal ds As DataSet, ByVal dateFrom As String, ByVal dateTo As String, ByVal type As String, ByVal datatableId As String) As String
        Dim sb As New StringBuilder("")
        If ds.Tables(0).Rows.Count > 0 Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>" & type & " Income report from " & dateFrom & "(" & txtDateFrom_Nep.Text & ") to " & dateTo & "(" & txtDateTo_Nep.Text & ") </div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")
            sb.AppendLine("<table id='" & datatableId & "' class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.Append("<TH>SN</TH>")

            For c As Integer = 0 To ds.Tables(0).Columns.Count - 1
                sb.AppendLine("<th>" & ds.Tables(0).Columns(c).ColumnName.ToString & "</th>")
            Next

            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            Dim colTotal() As Decimal
            ReDim colTotal(ds.Tables(0).Columns.Count - 1)
            For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                sb.AppendLine("<tr>")
                sb.AppendLine("<td class='text-center'>" & r + 1 & "</td>")
                For c As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    If c = 1 Then

                        sb.AppendLine("<td class='text-center'>" & ds.Tables(0).Rows(r).Item(c).ToString().Trim & "</td>")

                    ElseIf c > 4 Then
                        Dim Amount As String
                        If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(r).Item(c).ToString().Trim) Then
                            Amount = ds.Tables(0).Rows(r).Item(c).ToString().Trim
                            colTotal(c) += Convert.ToDecimal(Amount)
                        Else
                            Amount = "0.00"
                        End If
                        sb.AppendLine("<td class='text-right'>" & Convert.ToDecimal(Amount).ToString("N2") & "</td>")
                    Else
                        sb.AppendLine("<td class='text-left'>" & ds.Tables(0).Rows(r).Item(c).ToString.Trim & "</td>")
                    End If

                Next

                sb.AppendLine("</tr>")
            Next

            sb.AppendLine("<tr>")
            sb.AppendLine("<td>" & (ds.Tables(0).Rows.Count + 1).ToString.Trim & " </td><td></td><td></td><td></td><td></td><td class='text-right bold'>Total : </td>")
            For j As Integer = 5 To ds.Tables(0).Columns.Count - 1
                sb.AppendLine("<td class='text-right bold'>" & colTotal(j).ToString("N2") & "</td>")
            Next

            sb.AppendLine("</tr>")

            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine(" </div>")
            sb.AppendLine("</div>")
            sb.AppendLine(" </div>")
            sb.AppendLine(" </div>")

        End If
        Return sb.ToString.Trim
    End Function
End Class