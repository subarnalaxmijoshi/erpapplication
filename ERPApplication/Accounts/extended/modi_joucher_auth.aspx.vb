﻿Imports System.IO
Imports System.Text
Imports System.Globalization
Imports System.Web.HttpContext
Public Class modi_joucher_auth
    Inherits System.Web.UI.Page
    Private ReadOnly Dao As New DatabaseDao
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            Response.Redirect(redirectUrl)
        End If


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'GetAllLeadger()
        If (CheckIfSupervisor() = False) Then
            checkPermission()
        Else
            Session("hasPermissionForLedgerRepair") = True
        End If
        GetLeadger_BasicAccounting()
    End Sub

    Protected Function CheckIfSupervisor() As Boolean
        Dim ds As New DataSet
        Dim sql As String
        sql = "EXEC Accounts.[usp_PermissionForLedgerRepair]  @Flag='isSupervisor',@UserID='" & Session("userID") & "' ,@BranchID=" & Session("BranchID")
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            hfisSupervisor.Value = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("isSupervisor"))
            Return Convert.ToBoolean(ds.Tables(0).Rows(0).Item("isSupervisor"))
        Else
            hfisSupervisor.Value = False
            Return False
        End If

    End Function

    Protected Function getSessionUserID() As String
        Return Session("userID").ToString
    End Function
    Protected Function getSessionBranchID() As String
        Return Session("BranchID").ToString
    End Function

    Protected Function getSessionFiscalID() As String
        Return Session("FiscalYearID").ToString
    End Function

    Protected Function checkPermission() As Boolean
        Dim ds As New DataSet
        Dim sql As String
        Dim haspermission As Boolean
        Try
            sql = "EXEC [Accounts].[usp_PermissionForLedgerRepair] @Flag='haspermission',@UserID='" & Session("UserID") & "',@BranchID='" & Session("BranchID") & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                haspermission = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("hasPermission").ToString)
                Session("hasPermissionForLedgerRepair") = haspermission
                hfcountertimer.Value = Convert.ToDateTime(ds.Tables(1).Rows(0).Item("countdownDate")).ToString("MMM dd yyyy HH:mm:ss")
                Return haspermission
            Else
                Session("hasPermissionForLedgerRepair") = False
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Submit.Click

        If ddlFilterCategory.SelectedValue = "1" Then
            GetLeadger_BasicAccounting()
        ElseIf ddlFilterCategory.SelectedValue = "2" Then
            GetLeadger_StudentIncome()
        ElseIf ddlFilterCategory.SelectedValue = "3" Then
            GetLeadger_MiscIncome()
        End If

    End Sub

    Private Sub GetLeadger_StudentIncome()
        Dim Sql = "EXEC accounts.usp_modi_journalEntry @flag='t_dis',@BranchID=" & Session("BranchID") & ", @UserID='" & Session("UserID") & "'"
        Sql += ",@FiscalYearID='" & Session("FiscalYearID") & "'"
        Dim ds As New DataSet
        listData.InnerHtml = ""
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine(" <div class='col-md-12'>")
                sb.AppendLine("  <div class='portlet box green'>")
                sb.AppendLine(" <div class='portlet-title'>")
                sb.AppendLine("  <div class='caption'>")
                sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers Information</div>")
                sb.AppendLine(" <div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
                sb.AppendLine("  </div>")
                sb.AppendLine("  </div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")
                sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>Voucher/bill No. </th>")
                sb.AppendLine("<th>Ledger Head </th>")
                sb.AppendLine("<th>Date </th>")
                sb.AppendLine("<th>Amount</th>")
                sb.AppendLine("<th>Actions</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                Dim balance As Decimal = 0
                Dim dr As Decimal = 0
                Dim calcDr As Decimal = 0
                Dim dr1 As String

                For Each row As DataRow In ds.Tables(0).Rows
                    Dim refno As String = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString
                    Dim posteddate = ds.Tables(0).Rows(i).Item("CreatedDate").ToString
                    Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
                    posteddate = Convert.ToDateTime(posteddate).ToString("d MMM yyyy")
                    dr = ds.Tables(0).Rows(i).Item("DR").ToString
                    '  Dim cr As Decimal = ds.Tables(0).Rows(i).Item("CR").ToString
                    dr1 = dr.ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    ' cr1 = cr.ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    sb.AppendLine("<tr  class='details-control'><td>" & (i + 1) & ".</td>")
                    sb.AppendLine("<td>" & refno & "</td>")
                    sb.AppendLine("<td>" & AccountName & "</td>")
                    sb.AppendLine("<td>" & posteddate & "</td>")
                    sb.AppendLine("<td><span class='pull-right' style='padding-right : 8px'>" & dr1 & "</span></td>")
                    If Session("hasPermissionForLedgerRepair").ToString.ToLower = "true" Then
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='ClassAdd btn btn-primary btn-xs' href='#portlet-config'><span class='btn-label icon fa fa-edit'></span></a> | ")
                        sb.AppendLine("<a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='delete_by_ref_no btn btn-danger btn-xs' href='#'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                    Else
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='ClassAdd btn btn-primary btn-xs hidden' href='#portlet-config'><span class='btn-label icon fa fa-edit'></span></a> | ")
                        sb.AppendLine("<a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='delete_by_ref_no btn btn-danger btn-xs hidden' href='#'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                    End If
                    sb.AppendLine("</tr>")

                    i += 1
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")

                listData.InnerHtml = sb.ToString()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                listData.InnerHtml = sb.ToString
            End If
       
        End If
    End Sub

    Private Sub GetLeadger_BasicAccounting()
        Dim dbName As String = Session("DatabaseName").ToString.Trim()
        Dim Sql = "EXEC accounts.usp_modi_journalEntry @flag='dis_voucher',@BranchID='" & Session("BranchID") & "', @UserID='" & Session("UserID") & "'"
        Sql += ",@FiscalYearID='" & Session("FiscalYearID") & "'"
        Dim ds As New DataSet
        listData.InnerHtml = ""
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine(" <div class='col-md-12'>")
                sb.AppendLine("  <div class='portlet box green'>")
                sb.AppendLine(" <div class='portlet-title'>")
                sb.AppendLine("  <div class='caption'>")
                sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers Information</div>")
                sb.AppendLine(" <div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
                sb.AppendLine("  </div>")
                sb.AppendLine("  </div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")
                sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>Voucher/bill No. </th>")
                sb.AppendLine("<th>Ledger Head </th>")
                sb.AppendLine("<th>Date </th>")
                sb.AppendLine("<th>Amount</th>")
                sb.AppendLine("<th>Actions</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                Dim balance As Decimal = 0
                Dim dr As Decimal = 0
                Dim calcDr As Decimal = 0
                Dim dr1 As String

                For Each row As DataRow In ds.Tables(0).Rows
                    Dim refno As String = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString
                    Dim posteddate = ds.Tables(0).Rows(i).Item("CreatedDate").ToString
                    Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
                    posteddate = Convert.ToDateTime(posteddate).ToString("d MMM yyyy")
                    dr = ds.Tables(0).Rows(i).Item("DR").ToString
                    '  Dim cr As Decimal = ds.Tables(0).Rows(i).Item("CR").ToString
                    dr1 = dr.ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    ' cr1 = cr.ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    sb.AppendLine("<tr  class='details-control'><td>" & (i + 1) & ".</td>")
                    'sb.AppendLine("<td>" & refno & "</td>")
                    If (Session("IsPublicCampus") = False) Then
                        If (Session("IsSimpleVoucher") = True) Then
                            sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/SimpleVoucher.aspx?vid=" & refno & "'>" & refno & "</a></td>")
                        Else
                            sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucherInNepali2.aspx?vid=" & refno & "'>" & refno & "</a></td>")
                        End If
                    Else
                        sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucher.aspx?vid=" & refno & "'>" & refno & "</a></td>")
                    End If
                    sb.AppendLine("<td>" & AccountName & "</td>")
                    sb.AppendLine("<td>" & posteddate & "</td>")
                    sb.AppendLine("<td><span class='pull-right' style='padding-right : 8px'>" & dr1 & "</span></td>")
                    If Session("hasPermissionForLedgerRepair").ToString.ToLower = "true" Then
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='ClassAdd btn btn-primary btn-xs' href='#portlet-config'><span class='btn-label icon fa fa-edit'></span></a> | ")
                        sb.AppendLine("<a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='delete_by_ref_no btn btn-danger btn-xs' href='#'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                    Else
                        sb.AppendLine("<td style='color:red;'>Expired </td>")

                    End If
                   
                    sb.AppendLine("</tr>")

                    i += 1
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")

                listData.InnerHtml = sb.ToString()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                listData.InnerHtml = sb.ToString
            End If
      
        End If
    End Sub

    Private Sub GetLeadger_MiscIncome()
        Dim Sql = "EXEC accounts.usp_modi_journalEntry @flag='misc_bill',@BranchID=" & Session("BranchID") & ", @UserID='" & Session("UserID") & "'"
        Sql += ",@FiscalYearID='" & Session("FiscalYearID") & "'"
        Dim ds As New DataSet
        listData.InnerHtml = ""
        Dim sb As New StringBuilder
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine(" <div class='col-md-12'>")
                sb.AppendLine("  <div class='portlet box green'>")
                sb.AppendLine(" <div class='portlet-title'>")
                sb.AppendLine("  <div class='caption'>")
                sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers Information</div>")
                sb.AppendLine(" <div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
                sb.AppendLine("  </div>")
                sb.AppendLine("  </div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")
                sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>Voucher/bill No. </th>")
                sb.AppendLine("<th>Ledger Head </th>")
                sb.AppendLine("<th>Date </th>")
                sb.AppendLine("<th>Amount</th>")
                sb.AppendLine("<th>Actions</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                Dim balance As Decimal = 0
                Dim dr As Decimal = 0
                Dim calcDr As Decimal = 0
                Dim dr1 As String

                For Each row As DataRow In ds.Tables(0).Rows
                    Dim refno As String = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString
                    Dim posteddate = ds.Tables(0).Rows(i).Item("CreatedDate").ToString
                    Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
                    posteddate = Convert.ToDateTime(posteddate).ToString("d MMM yyyy")
                    dr = ds.Tables(0).Rows(i).Item("DR").ToString

                    dr1 = dr.ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))

                    sb.AppendLine("<tr  class='details-control'><td>" & (i + 1) & ".</td>")
                    sb.AppendLine("<td>" & refno & "</td>")
                    sb.AppendLine("<td>" & AccountName & "</td>")
                    sb.AppendLine("<td>" & posteddate & "</td>")
                    sb.AppendLine("<td><span class='pull-right' style='padding-right : 8px'>" & dr1 & "</span></td>")
                    If Session("hasPermissionForLedgerRepair").ToString.ToLower = "true" Then
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='ClassAdd btn btn-primary btn-xs' href='#portlet-config'><span class='btn-label icon fa fa-edit'></span></a> | ")
                        sb.AppendLine("<a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='delete_by_ref_no btn btn-danger btn-xs' href='#'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                    Else
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='ClassAdd btn btn-primary btn-xs hidden' href='#portlet-config'><span class='btn-label icon fa fa-edit'></span></a> | ")
                        sb.AppendLine("<a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("ReferenceNo").ToString & "'  id='add' class='delete_by_ref_no btn btn-danger btn-xs hidden' href='#'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                    End If
                    sb.AppendLine("</tr>")

                    i += 1
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")

                listData.InnerHtml = sb.ToString()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                listData.InnerHtml = sb.ToString
            End If

        End If
    End Sub


    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefresh.Click
        If ddlFilterCategory.SelectedValue = "1" Then
            GetLeadger_BasicAccounting()
        Else
            GetLeadger_StudentIncome()
        End If
    End Sub

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Voucher List")
    End Sub

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function
End Class