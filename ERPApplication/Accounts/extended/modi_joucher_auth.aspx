﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="modi_joucher_auth.aspx.vb" Inherits="School.modi_joucher_auth" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">


.Expire-animation{

   -webkit-animation-name: moveInButtonToRightCorner;
    -webkit-animation-duration: 5s;
    

}

@-webkit-keyframes moveInButtonToRightCorner {
  0% {opacity: 0; transform: translateX(40rem) translateY(40rem) scale(4.1); }   
 90% {opacity: 1; transform: translateX(20rem)  scale(.1);  } 
 100% {opacity: 1; transform: translateX(20); }
}

    
    
    
</style>
    <script type="text/javascript">

        $(document).ready(function () {

            var isSupervisor = $('#<%= hfisSupervisor.ClientID %>').val();
            // Set the date we're counting down to
            //    var countDownDate = new Date("Feb 10, 2019 15:37:25").getTime();
            if (isSupervisor == 'False') {
                debugger;
                var gettimer = $('#<%= hfcountertimer.ClientID %>').val();
                console.log(gettimer);
                var countDownDate = new Date(gettimer).getTime();


                // Update the count down every 1 second
                var x = setInterval(function () {

                    // Get todays date and time
                    var now = new Date().getTime();

                    // Find the distance between now and the count down date
                    var distance = countDownDate - now;
                    //            console.log(distance);
                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    // Display the result in the element with id="demo"
                    document.getElementById("countdownTimer").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

                    // If the count down is finished, write some text
                    if (distance < 0) {
                        clearInterval(x);


                        //                if (document.getElementById("countdownTimer").innerHTML != "EXPIRED") {
                        //                 
                        //                    $('#<%= btnRefresh.ClientID %>').click();
                        //                }
                        document.getElementById("countdownTimer").innerHTML = "EXPIRED";
                        alert('Your time has expired!');
                        hideActionbuttons();


                    }
                }, 1000);

            }
            else {
                document.getElementById("countdownTimer").innerHTML = ""
            }

        });
            function hideActionbuttons() {
                if (document.getElementById("countdownTimer").innerHTML == "EXPIRED") {
                    $(".ClassAdd").addClass("hidden");
                    $(".delete_by_ref_no").addClass("hidden");
//                    $('table.dataTable td:contains(|)').replace("|", " ");

                }
              
            }
  
</script>

  <script type="text/javascript">
      var checkFromValidation = false;
      $(document).ready(function () {
          $('#form').validate();
          $("#<%=ddlFilterCategory.ClientID %>").rules('add', { required: true, messages: { required: 'Filter By is required.'} });

          checkFromValidation = function () {
              var bool = true;
              if ($('#<%=ddlFilterCategory.ClientID %>').valid() == false) bool = false;
              if (!bool) $('#form').validate().focusInvalid();
              return bool;
          };

      });    
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
         var sessionUserid = <%= getSessionUserID() %>;
           var branchid = <%= getSessionBranchID() %>;
            $(document).on('click', '.ClassFees', function () {

                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var modi = $(this).attr("data-mo");


                var newTheory = prompt("Enter Value", "0");
                console.log(newTheory);
                if (newTheory === null || newTheory === false || newTheory === '') {
                    console.log("Value is empty");
                    alert("Value is empty");
                    return;
                } else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/edit_vourcher',
                        data: '{"jid" : "' + obtainId + '","val" : "' + newTheory + '","modi" : "' + modi + '","sUserId" : "' + sessionUserid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            thisItem.html(newTheory);

                            alert(data);

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
         var sessionUserid = <%= getSessionUserID() %>;
           var branchid = <%= getSessionBranchID() %>;
            var fiscalid = <%= getSessionFiscalID() %>;
            $(document).on('click', '.multi_rows_edit', function () {

                var thisItem = $(this);
                var obtainId = $(this).attr("data-id");
                var modi = $(this).attr("data-mo");


                var newTheory = prompt("Enter Value", "0");
                console.log(newTheory);
                if (newTheory === null || newTheory === false || newTheory === '') {
                    console.log("Value is empty");
                    alert("Value is empty");
                    return;
                } else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/edit_multi_rows_vourcher',
                        data: '{"ref" : "' + obtainId + '","val" : "' + newTheory + '","modi" : "' + modi + '","sUserId" : "' + sessionUserid + '","BranchID":"'+ branchid +'","sFiscalyearID" : "' + fiscalid+ '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            thisItem.html(newTheory);

                            alert(data);

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
         var sessionUserid = <%= getSessionUserID() %>;

            $(document).on('click', '.deletesinglerow', function () {
                if (confirm("Are you sure to delete this vourcher item ?") == true) {
                    var obtainId = $(this).attr("data-id");

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/delete_v_single_row',
                        data: '{"jid" : "' + obtainId + '","sUserId" : "' + sessionUserid + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            alert(data);
                            getvdetails();

                            //                         location.href = '/Exam/EditObtainedMark.aspx';

                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }

            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
         var sessionUserid = <%= getSessionUserID() %>;
          var branchid = <%= getSessionBranchID() %>;
            var fiscalid = <%= getSessionFiscalID() %>;
            $(document).on('click', '.delete_by_ref_no', function () {

                if (confirm("Are you sure to delete this vourcher?") == true) {



                    var obtainId = $(this).attr("data-id");

                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/delete_multi_rows_vourcher_no',
                        data: '{"ref" : "' + obtainId + '","sUserId" : "' + sessionUserid + '","BranchID":"'+ branchid +'","sFiscalyearID" : "' + fiscalid+ '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            alert(data);
                            location.href = '/Accounts/extended/modi_joucher_auth.aspx';
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }

            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".ClassAdd").click(function () {
                var ref = $(this).attr("data-id");
                var branchid = <%= getSessionBranchID() %>;
                var fiscalid = <%= getSessionFiscalID() %>;
                $('#<%= hf_ref.ClientID %>').val(ref);
                $.ajax({
                    type: 'POST',
                    url: '/SetupServices.svc/GetVoucherDetails',
                    data: '{"ref" : "' + ref + '","sBranchID" : "' + branchid + '","sFiscalyearID" : "' + fiscalid + '"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        $('#model_data').html(data);

                    },
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }

                });
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.reload', function () {
                getvdetails();
            });
        });
        function getvdetails() {
            
            var ref = $('#<%= hf_ref.ClientID %>').val();
             var branchid = <%= getSessionBranchID() %>;
               var fiscalid = <%= getSessionFiscalID() %>;

            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/GetVoucherDetails',
                data: '{"ref" : "' + ref + '","sBranchID" : "' + branchid + '","sFiscalyearID" : "' + fiscalid + '"}',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    $('#model_data').html(data);
                   
                },
                error: function (data) {
                    alert(data.statusText);
                },
                failure: function (data) {
                    alert(data.statusText);
                }


            });

        }
           

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#displaytable').DataTable({
                "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
                "fnDrawCallback": function (oSettings) {
//                    alert('DataTables has redrawn the table');
                    hideActionbuttons();
                }
            });

            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hf_ref" runat="server" />
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            EDIT</h4>
                    </div>
                    <div class="modal-body">
                        <div id="model_data">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--  <button type="button" class="btn blue">
                            Save changes</button>--%>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Ledger Repair and Maintenace <small>Managing the value to account/ledger.</small><span id="countdownTimer" style="margin: 160px;" class="Expire-animation"></span>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                           
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-money"></i><a href="/Accounts/Accountsmenu.aspx">Account</a> <i
                        class="fa fa-angle-right"></i></li>
                    <li><a href="/Accounts/extended/modi_joucher_auth.aspx">Ledger Repair and Maintenace</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="message" runat="server">
                </div>
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Filter Ledger Information</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Filter By :</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                        <asp:DropDownList ID="ddlFilterCategory" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1">Basic Accounting</asp:ListItem>
                                            <asp:ListItem Value="2">Student Income</asp:ListItem>
                                            <asp:ListItem Value="3">Misc. Income</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label for="<%= ddlFilterCategory.ClientID%>" class="error" style="display: none">
                                    </label>
                                </div>
                                 <asp:Button ID="btn_Submit" Class="btn purple" runat="server" Text="Show" OnClientClick="return  checkFromValidation();" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="listData" runat="server" class="">
                </div>
                <asp:HiddenField ID="hfcountertimer" runat="server" />
                <asp:HiddenField ID="hfisSupervisor" runat="server" />
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" class="btn purple hidden"/>
                 <asp:Button ID="btnToExcel" class="btn green" runat="server" Text="Export To Excel"
                   UseSubmitBehavior="false"/>
            </div>
        </div>
    </div>
</asp:Content>
