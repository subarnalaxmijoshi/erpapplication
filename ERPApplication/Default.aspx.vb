﻿Public Class _Default
    Inherits System.Web.UI.Page

    Private Dao As New DatabaseDao
    Dim ds As New DataSet
    Dim sql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Lock") = "lock" Then
            Response.Redirect("~/Login.aspx")
        End If



        If String.IsNullOrWhiteSpace(Session("userID")) And
           String.IsNullOrWhiteSpace(Session("username")) And
           String.IsNullOrWhiteSpace(Session("Role")) And
           String.IsNullOrWhiteSpace(Session("Branch")) Then
            Response.Redirect("/Login.aspx")
        End If
        If Not IsPostBack Then
            ' GetReport()
            'ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "OSVersionInfo", "alert(sessionOSVer);", True)
        End If


    End Sub

    Protected Function getSessionUserID() As String
        Return Session("userID").ToString
    End Function

    'Protected Function getOSVersion() As String
    '    If Not String.IsNullOrWhiteSpace(Session("OSVersion")) Then
    '        Return Session("OSVersion").ToString()
    '    Else
    '        Return ""
    '    End If
    'End Function
    Private Sub GetReport()
        Dim sql As String = "exec [Management].[usp_DashboardReport]"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then

            totalstudent.InnerText = ds.Tables(0).Rows(0).Item("TotalStudent").ToString
            male.InnerText = ds.Tables(0).Rows(0).Item("Male").ToString
            female.InnerText = ds.Tables(0).Rows(0).Item("Female").ToString
            If ds.Tables(1).Rows.Count > 0 Then
                LastMonth.InnerText = ds.Tables(1).Rows(0).Item("LastMonth").ToString
            End If

            totalemployee.InnerText = ds.Tables(2).Rows(0).Item("TotalEmployee").ToString
            emale.InnerText = ds.Tables(2).Rows(0).Item("Male").ToString
            efemale.InnerText = ds.Tables(2).Rows(0).Item("Female").ToString
            holiday.InnerText = ds.Tables(3).Rows(0).Item("HolidayName").ToString

            If ds.Tables(4).Rows.Count > 0 Then
                paymentmonth.InnerText = ds.Tables(4).Rows(0).Item("MonthName").ToString
            End If

            If ds.Tables(5).Rows.Count > 0 Then
                examname.InnerText = ds.Tables(5).Rows(0).Item("ExamName").ToString
                start.InnerText = ds.Tables(5).Rows(0).Item("starting").ToString
                ending.InnerText = ds.Tables(5).Rows(0).Item("ending").ToString
            End If

            books.InnerText = ds.Tables(6).Rows(0).Item("Total").ToString
            assignstudent.InnerText = ds.Tables(7).Rows(0).Item("Total").ToString



        End If





    End Sub

    
End Class