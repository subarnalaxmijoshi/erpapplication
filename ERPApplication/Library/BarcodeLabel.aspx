﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="BarcodeLabel.aspx.vb" Inherits="School.BarcodeLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="/assets/plugins/jquery-datetimepicker/jquery.datetimepicker.css" rel="stylesheet"
        type="text/css" />
    <script src="/assets/plugins/jquery-datetimepicker/jquery.datetimepicker.js" type="text/javascript"></script>
     <script type="text/javascript">

         var checkFromValidation = false;
         $(document).ready(function () {
             jQuery(".datetimepicker").datetimepicker({ step: 10, defaultDate: new Date(), defaultTime: '10:00' });
           
             var dNow = new Date();
             //var localdate = (dNow.getMonth() + 1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
             var localdate = dNow.getFullYear() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getDate() + ' ' + '10:00';
             $(".datetimepicker").val(localdate);
             //            $(".attendancetimeAll").val(localdate);

         });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#date1").show();
            $("#date2").show();
            $("#barcodeno").hide();
            $("#random").hide();
            $("#<%=dropdowntype.ClientID %>").val("Date");

            $("#<%=dropdowntype.ClientID %>").live("change", function () {
                var value = $('#<%=dropdowntype.ClientID %> option:selected').val();
                if (value == "Date") {
                    $("#date1").show();
                    $("#date2").show();
                    $("#barcodeno").hide();
                    $("#random").hide();
                }
                else if (value == "BarcodeNo") {
                    $("#date1").hide();
                    $("#date2").hide();
                    $("#barcodeno").show();
                    $("#random").hide();
                }
                else if (value == "Random") {
                    $("#date1").hide();
                    $("#date2").hide();
                    $("#barcodeno").hide();
                    $("#random").show();
                }
                else {
                    $("#date1").hide();
                    $("#date2").hide();
                    $("#barcodeno").hide();
                    $("#random").hide();
                }
            });

            $('#displaytable').DataTable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Barcode Label <small>create barcode label for book</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li id="toExcel"><a href="javascript:;">Action</a></li>
                            <li id="toPdf"><a href="javascript:;">Action Link</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-list"></i>Catalogue<i class="fa fa-angle-right"></i></li>
                    <li><a href="/Library/BarcodeLabel.aspx">Barcode</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div class="portlet box purple ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Search Barcode
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Search By:</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <asp:DropDownList ID="dropdowntype" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="Date" Selected="True">Date</asp:ListItem>
                                                        <asp:ListItem Value="BarcodeNo">Barcode No.</asp:ListItem>
                                                        <asp:ListItem Value="Random">Random Entry</asp:ListItem>
                                                        <asp:ListItem Value="NotPrinted">Not Printed</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                Blank Label:</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                                    <asp:TextBox ID="txtBlankLabel" TextMode="Number" CssClass="form-control" runat="server"
                                                        Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                            </label>
                                            <div class="col-md-5" id="date1">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txtStartDate" CssClass="form-control datetimepicker" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-5" id="date2">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txtEndDate" CssClass="form-control datetimepicker" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-10" id="barcodeno">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                                    <asp:TextBox ID="txtBarcode"  CssClass="form-control" runat="server"
                                                        placeholder="Enter barcode"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-10" id="random">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
                                                    <asp:TextBox ID="txtRandom" TextMode="MultiLine" CssClass="form-control" runat="server"
                                                        placeholder="Enter barcode no separating by comma"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="btnMark" class="btn purple" data-toggle="modal" data-target="#MarkModal">
                                            Search</button>
                                        <asp:Button ID="btnSubmit" class="btn purple" Style="display: none;" runat="server"
                                            Text="Search" />
                                        <asp:Button ID="btnDonotMark" class="btn purple" Style="display: none;" runat="server"
                                            Text="Search" />
                                        <asp:Button ID="btnCancel" type="button" class="btn default" runat="server" Text="Cancel" />
                                        <asp:Button ID="btnGenerate" runat="server" Text="Generate" Style="display: none;"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="listData" runat="server">
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnMarkPrinted").click(function () {
                $("#<%=btnSubmit.ClientID %>").click();
            });
            $("#btnno").click(function () {
                $("#<%=btnDonotMark.ClientID %>").click();
            });

            $(".generate").click(function () {
                var thisItem = $(this);
                $("#<%=btnGenerate.ClientID %>").click();
                
            });
        });
    </script>
    <!-- Start Delete Modal -->
    <div class="modal fade" id="MarkModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>
                        You want to mark these barcodes as printed?</h4>
                </div>
                <div class="modal-footer">
                    <div class="col-md-offset-9 col-md-3">
                        <button type="button" id="btnMarkPrinted" class="btn btn-primary btn-xs" data-dismiss="modal">
                            <i class="glyphicon glyphicon-check"></i>&nbsp;Yes</button>
                        <button type="button" id="btnno" class="btn btn-default btn-xs" data-dismiss="modal">
                            <i class="glyphicon glyphicon-remove"></i>&nbsp;No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete Modal -->
</asp:Content>
