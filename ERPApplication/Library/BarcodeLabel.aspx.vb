﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Web.HttpContext
Imports System.IO

Public Class BarcodeLabel
    Inherits System.Web.UI.Page
    Private dao As New DatabaseDao
    Private sb As New StringBuilder("")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        End If
    End Sub


    Sub MakePDF(ByVal SearchBy As String, ByVal BlankLabel As String, ByVal Value As String, ByVal Value2 As String, ByVal Mark As String)
        Dim sb As StringBuilder = New StringBuilder("")
        message.InnerHtml = ""
        Try
            Dim sql As String = "EXEC Library.usp_PrintBarcode @Flag='Barcode',@SearchBy='" & SearchBy & "',@Blank='" & BlankLabel & "',@Value1='" & Replace(Value, "T", " ") & "',@Value2='" & Replace(Value2, "T", " ") & "',@Mark='" & Mark & "'"
            Dim ds As New DataSet
            ds = dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then

                Dim FontSize As Double = ds.Tables(1).Rows(0)("BarcodeFontSize")
                Dim CellHeight As Double = ds.Tables(1).Rows(0)("BarcodeCellHeight")
                Dim PaddingLeft As Integer = ds.Tables(1).Rows(0)("BarcodePaddingLeft")
                Dim PaddingRight As Integer = ds.Tables(1).Rows(0)("BarcodePaddingRight")
                Dim PaddingTop As Integer = ds.Tables(1).Rows(0)("BarcodePaddingTop")
                Dim PaddingBottom As Integer = ds.Tables(1).Rows(0)("BarcodePaddingBottom")
                Dim TableWidth As Integer = ds.Tables(1).Rows(0)("BarcodeTableWidth")
                Dim PageLeft As Double = ds.Tables(1).Rows(0)("BarcodePageLeft")
                Dim PageRight As Double = ds.Tables(1).Rows(0)("BarcodePageRight")
                Dim PageTop As Double = ds.Tables(1).Rows(0)("BarcodePageTop")
                Dim PageBottom As Double = ds.Tables(1).Rows(0)("BarcodePageBottom")
                Dim Border As Boolean = ds.Tables(1).Rows(0)("BarcodeBorder")
                Dim BHeight As Integer = ds.Tables(1).Rows(0)("BarcodeHeight")
                Dim BWidth As Integer = ds.Tables(1).Rows(0)("BarcodeWidth")
                Dim label As String = ds.Tables(1).Rows(0)("DisplayLabel")

                Dim fname As String = Server.MapPath("/Library/Barcode/BarcodeLabel.pdf")

                Dim pdfDoc As Document = New Document(PageSize.A4, PageLeft, PageRight, PageTop, PageBottom)
                Dim pdfWriter As PdfWriter
                pdfWriter = pdfWriter.GetInstance(pdfDoc, New FileStream(fname, FileMode.Create))
                pdfDoc.Open()

                Dim reportHeader As PdfPTable = New PdfPTable(5)
                reportHeader.WidthPercentage = TableWidth

                Dim contentByte As PdfContentByte
                contentByte = pdfWriter.DirectContent

                Dim normalFont As New Font(Font.FontFamily.TIMES_ROMAN, FontSize, 0)
                Dim spacer = ""

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                    Dim code = ds.Tables(0).Rows(i)("AccessionNo").ToString()

                    Dim code128 As Barcode128 = New Barcode128()
                    code128.Code = code
                    code128.CodeType = Barcode128.CODE128
                    code128.Baseline = 10
                    code128.Size = 11

                    Dim codeLength = code.Length
                    Dim check = 0
                    For c As Integer = 1 To codeLength
                        If codeLength > 2 Then
                            If codeLength Mod 2 = 0 Then
                                spacer = spacer & " "
                            Else
                                If check = 0 Then

                                    spacer = spacer & " " & " "
                                    check = 1
                                Else
                                    spacer = spacer & " "
                                End If

                            End If
                        Else
                            spacer = spacer & " "
                        End If

                    Next
                    check = 0
                        Dim img As iTextSharp.text.Image = code128.CreateImageWithBarcode(contentByte, BaseColor.BLACK, BaseColor.BLACK)
                        img.ScaleToFit(BWidth, BHeight)
                        img.ScaleToFitLineWhenOverflow = True
                        img.ScaleToFitHeight = True

                        Dim cell As PdfPCell = New PdfPCell()

                        If String.IsNullOrWhiteSpace(code) Then
                            cell.AddElement(New Phrase(""))
                        Else

                            cell.AddElement(New Phrase(spacer & label, normalFont))
                            cell.AddElement(img)
                        End If
                    spacer = ""
                        cell.FixedHeight = CellHeight
                        cell.PaddingLeft = PaddingLeft
                        cell.PaddingRight = PaddingRight
                        cell.PaddingTop = PaddingTop
                        cell.PaddingBottom = PaddingBottom

                        If Not Border Then
                            cell.Border = Rectangle.NO_BORDER
                            cell.BorderColorTop = BaseColor.WHITE
                        End If

                        reportHeader.AddCell(cell)
                    Next
                reportHeader.DefaultCell.Border = Rectangle.NO_BORDER
                reportHeader.CompleteRow()
                pdfDoc.Add(reportHeader)

                pdfDoc.Close()

                'Response.Redirect("/Library/Barcode/BarcodeLabel.pdf")

                'Current.Response.ContentType = "application/pdf"
                'Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
                'Current.Response.Write(pdfDoc)
                'Current.Response.End()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Records Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    'Sub CreateBarcode()
    '    Try
    '        bLabel.InnerHtml = ""
    '        Dim j As Integer
    '        Dim count As Integer
    '        Dim sql As String = "EXEC Library.usp_PrintBarcode"
    '        Dim dt As New DataTable
    '        dt = dao.ExecuteDataTable(sql)
    '        If (dt.Rows.Count > 0) Then
    '            count = dt.Rows.Count - 1
    '            Dim sb As New StringBuilder("")
    '            sb.AppendLine("<table class='displaytable'>")
    '            For i As Integer = 0 To count
    '                sb.AppendLine("<tr>")
    '                If (count - i >= 5) Then
    '                    For j = i To i + 4
    '                        sb.AppendLine("<td><p>" & dt.Rows(i)("Label").ToString() & "</p>")
    '                        sb.AppendLine("<span class='barcodedisplay'>*" & dt.Rows(i)("AccessionNo").ToString() & "*</span>")
    '                        sb.AppendLine("<p>" & dt.Rows(i)("AccessionNo").ToString() & "</p></td>")
    '                        i += 1
    '                    Next
    '                Else
    '                    For j = i To count
    '                        sb.AppendLine("<td><p>" & dt.Rows(i)("Label").ToString() & "</p>")
    '                        sb.AppendLine("<span class='barcodedisplay'>*" & dt.Rows(i)("AccessionNo").ToString() & "*</span>")
    '                        sb.AppendLine("<p>" & dt.Rows(i)("AccessionNo").ToString() & "</p></td>")
    '                        i += 1
    '                    Next
    '                End If
    '                i = j - 1
    '                sb.AppendLine("</tr>")
    '            Next
    '            sb.AppendLine("</table>")
    '            bLabel.InnerHtml = sb.ToString()

    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Sub BarcodeDataList(ByVal SearchBy As String, ByVal BlankLabel As String, ByVal Value As String, ByVal Value2 As String, ByVal Mark As String)
        message.InnerHtml = ""
        Dim sql As String
        sql = "EXEC Library.usp_PrintBarcode @Flag='BarcodeList',@SearchBy='" & SearchBy & "',@Blank='" & BlankLabel & "',@Value1='" & Replace(Value, "T", " ") & "',@Value2='" & Replace(Value2, "T", " ") & "'"
        Dim ds As New DataSet
        ds = dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine("<div class='col-md-12'>")
            sb.AppendLine("<div class='portlet box green'>")
            sb.AppendLine("<div class='portlet-title'>")
            sb.AppendLine("<div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cog'></i>Barcode Label Information</div>")
            sb.AppendLine("<div class='tools'>")
            sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class=''></a><a href='javascript:;' class=''></a><a href='javascript:;' class='remove'></a>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
            'sb.AppendLine("<a href='/Library/Barcode/BarcodeLabel.pdf' class='btn btn-primary btn-xs pull-right' target='_blank' class='generate'>Generate</a>")

            sb.AppendLine("<a data-search='" & SearchBy & "' data-label='" & BlankLabel & "' data-val1='" & Value & "' data-val2='" & Value2 & "' data-mark='" & Mark & "' class='generate btn btn-primary btn-xs pull-right'>Generate</a>")
            sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            sb.AppendLine("<th>Accession No</th>")
            sb.AppendLine("<th>Book Title</th>")
            sb.AppendLine("<th>Author</th>")
            sb.AppendLine("<th>Publisher</th>")
            sb.AppendLine("<th>Published Year</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")

            Dim i As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows
                Dim SN = i + 1
                Dim AccNo = ds.Tables(0).Rows(i).Item("AccessionNo").ToString
                Dim Title = ds.Tables(0).Rows(i).Item("Title").ToString
                Dim Author = ds.Tables(0).Rows(i).Item("Author").ToString
                Dim Publisher = ds.Tables(0).Rows(i).Item("Publishser").ToString
                Dim Year = ds.Tables(0).Rows(i).Item("PublishedYear").ToString

                sb.AppendLine("<tr>")
                sb.AppendLine("<td>" & SN & "</td>")
                sb.AppendLine("<td>" & AccNo & "</td>")
                sb.AppendLine("<td>" & Title & "</td>")
                sb.AppendLine("<td>" & Author & "</td>")
                sb.AppendLine("<td>" & Publisher & "</td>")
                sb.AppendLine("<td>" & Year & "</td>")
                sb.AppendLine("</tr>")

                i += 1
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")

            listData.InnerHtml = sb.ToString()

            MakePDF(SearchBy, BlankLabel, Value, Value2, Mark)
        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Records Found")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            listData.InnerHtml = ""
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        message.InnerHtml = ""
        Try
            Dim searchby = dropdowntype.SelectedValue.ToString()
            Dim blank = txtBlankLabel.Text.Trim()
            Dim value1 As String
            Dim value2 As String
            If searchby = "Date" Then
                value1 = txtStartDate.Text.Trim()
                value2 = txtEndDate.Text.Trim()
            ElseIf searchby = "BarcodeNo" Then
                value1 = txtBarcode.Text.Trim()
                value2 = Nothing
            ElseIf searchby = "Random" Then
                value1 = txtRandom.Text.Trim()
                value2 = Nothing
            Else
                value1 = Nothing
                value2 = Nothing
            End If
            BarcodeDataList(searchby, blank, value1, value2, "1")
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try

    End Sub

    Protected Sub btnDonotMark_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDonotMark.Click
        message.InnerHtml = ""
        Try
            Dim searchby = dropdowntype.SelectedValue.ToString()
            Dim blank = txtBlankLabel.Text.Trim()
            Dim value1 As String
            Dim value2 As String
            If searchby = "Date" Then
                value1 = txtStartDate.Text.Trim()
                value2 = txtEndDate.Text.Trim()
            ElseIf searchby = "BarcodeNo" Then
                value1 = txtBarcode.Text.Trim()
                value2 = Nothing
            ElseIf searchby = "Random" Then
                value1 = txtRandom.Text.Trim()
                value2 = Nothing
            Else
                value1 = Nothing
                value2 = Nothing
            End If
            BarcodeDataList(searchby, blank, value1, value2, "0")
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerate.Click
        'Response.Redirect("/Library/Barcode/BarcodeLabel.pdf")
        Current.Response.ContentType = "application/pdf"
        Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Current.Response.AppendHeader("Content-Disposition", "attachment; filename=BarcodeLabel.pdf")
        Current.Response.TransmitFile(Server.MapPath("~/Library/Barcode/BarcodeLabel.pdf"))
        Current.Response.End()
    End Sub
End Class