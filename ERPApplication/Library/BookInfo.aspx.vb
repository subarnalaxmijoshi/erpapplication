﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml
Imports System.Web.HttpContext
Public Class LibraryInfo
    Inherits System.Web.UI.Page
    Private Dao As New DatabaseDao
    Dim ds As New DataSet
    Dim EditID As String = ""
    Dim Sql As String = ""
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            Response.Redirect("/Logout.aspx")
        End If
        Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        If Not hasRightToView Then
            Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
            Response.Redirect(redirectUrl)
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
        Else
            UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
        End If


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            CascadResponsibility()
            CascadeMediaType()
            CascadeLanguage()
            If Not String.IsNullOrWhiteSpace(Request.QueryString("EditID")) Then
                EditID = Request.QueryString("EditID")
                GetBookInfoByID(EditID)
            ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("DeleteID")) Then
                EditID = Request.QueryString("DeleteID")
                DeleteBookInfo(EditID)
            Else
                ddlResposibility.SelectedIndex = 1
                ddllanguage.SelectedIndex = 1
                ddlmediatype.SelectedIndex = 1
                ddlResposibility.SelectedIndex = 1
                btnCancel.Visible = True
            End If


            'CascadeCategory()
            'CascadeCondition()
            'CascadeLocation()
            'CascadeCollectionType()
            'CascadeSection()

            TotalBookInfo()
            BooksDistinct()
            TodayTotals()
            CurrentUsers()

            
        End If
    End Sub

    Sub InsertBookInfo()

            Dim sql As String
            Dim sb As StringBuilder = New StringBuilder("")
        Try
            Dim title As String = Dao.FilterQuote(txttitle.Text.ToString().Trim)
            If Not (String.IsNullOrWhiteSpace(title)) Then

                sql = "exec [Library].[usp_BookInfo] @Flag='i',@Title =N'" & Dao.FilterQuote(txttitle.Text.ToString().Trim) &
                    "',@CharLength=0, @SubTitle =N'" & Dao.FilterQuote(txtsubtitle.Text.ToString.Trim) &
                    "',@Class =N'" & txtclass.Text.Trim &
                    "',@BookNo =N'" & txtbookno.Text.Trim &
                    "',@PhysicalDescription=N'" & txtphyiscaldes.Text.Trim &
                    "',@MediaType=N'" & ddlmediatype.SelectedValue.ToString.Trim() &
                    "',@Subject=N'" & Dao.FilterQuote(txtsubject.Text.Trim()) &
                    "',@Responsibility=N'" & ddlResposibility.SelectedValue.ToString.Trim() &
                    "',@ResTitle=N'" & Dao.FilterQuote(txtrestitle.Text.Trim()) &
                    "',@ISBN=N'" & txtisbn.Text.Trim() &
                    "',@Vol=N'" & txtvolume.Text.Trim() &
                    "',@Edition=N'" & txtedition.Text.Trim() &
                    "',@Language=N'" & ddllanguage.SelectedValue.ToString.Trim() &
                    "',@Keyword=N'" & Dao.FilterQuote(txtkeyword.Text.Trim()) &
                    "',@Notes=N'" & Dao.FilterQuote(txtnotes.Text.Trim()) &
                    "',@Publisher=N'" & Dao.FilterQuote(txtpublisher.Text.Trim()) &
                    "',@Place=N'" & Dao.FilterQuote(txtplace.Text.Trim()) &
                    "',@Year=N'" & txtyear.Text.Trim() &
                    "',@Price=N'" & IIf(String.IsNullOrWhiteSpace(txtprice.Text.Trim()), 0, txtprice.Text.Trim()) &
                    "',@BillNo=N'" & txtbillno.Text.Trim() &
                    "',@URL='" & txturl.Text.Trim() &
                    "',@BranchID='" & Session("BranchID") &
                    "',@EntryBy='" & Session("UserID") & "'"
                Dim ds As New DataSet

                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not (String.IsNullOrWhiteSpace(ds.Tables(0).Rows(0).Item("BookID").ToString)) Then
                        Dim BookId As String = ds.Tables(0).Rows(0).Item("BookID").ToString
                        Response.Redirect("/Library/BookAccessioning.aspx?BookID=" & BookId & "")

                    End If
                    'sb.AppendLine("<div class='note note-success'>")
                    'sb.AppendLine("<div class='close-note'>x</div>")
                    'sb.AppendLine("<p>")
                    'sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                    'sb.AppendLine("</p>")
                    'sb.AppendLine("</div>")
                    'message.InnerHtml = sb.ToString

                    'GetBookInfo()
                    'ClearFields()
                End If
            End If

        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
        
    End Sub

    Sub GetBookInfo()
        Dim sql As String
        Dim sb As StringBuilder = New StringBuilder("")
        sql = "exec [Library].[usp_BookInfo] @flag='s'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then


            sb.AppendLine("<div class='row'>")
            sb.AppendLine("<div class='col-md-12'>")
            sb.AppendLine("<div class='portlet box green'>")
            sb.AppendLine("<div class='portlet-title'>")
            sb.AppendLine("<div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cog'></i>Book Information</div>")
            sb.AppendLine("<div class='tools'>")
            sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class=''></a><a href='javascript:;' class=''></a><a href='javascript:;' class='remove'></a>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")

            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            sb.AppendLine("<th>Title</th>")
            sb.AppendLine("<th>Subtitle</th>")
            sb.AppendLine("<th>Class</th>")
            sb.AppendLine("<th>Book No</th>")
            sb.AppendLine("<th>Physical Description</th>")
            sb.AppendLine("<th>Category</th>")
            sb.AppendLine("<th>Media Type</th>")
            sb.AppendLine("<th>Subject </th>")
            sb.AppendLine("<th>Collection Type </th>")
            sb.AppendLine("<th>Location </th>")
            sb.AppendLine("<th>Section </th>")
            sb.AppendLine("<th>Condition</th>")
            sb.AppendLine("<th>Responsibility</th>")
            sb.AppendLine("<th>Res-Title</th>")
            sb.AppendLine("<th>ISBN</th>")
            sb.AppendLine("<th>Volume</th>")
            sb.AppendLine("<th>Edition</th>")
            sb.AppendLine("<th>Language</th>")
            sb.AppendLine("<th>Keyword</th>")
            sb.AppendLine("<th>Notes </th>")
            sb.AppendLine("<th>Publisher </th>")
            sb.AppendLine("<th>Place </th>")
            sb.AppendLine("<th>Year </th>")
            sb.AppendLine("<th>Price</th>")
            sb.AppendLine("<th>BillNo</th>")
            sb.AppendLine("<th>URL</th>")





            sb.AppendLine("<th Colspan=2 style='text-align:center'>Actions</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            Dim i As Integer = 0
            For Each row As DataRow In ds.Tables(0).Rows
                EditID = ds.Tables(0).Rows(i).Item("BookID").ToString


                Dim Title = ds.Tables(0).Rows(i).Item("Title").ToString
                Dim subtilte = ds.Tables(0).Rows(i).Item("SubTitle").ToString
                Dim class1 = ds.Tables(0).Rows(i).Item("Class").ToString
                Dim BookNo = ds.Tables(0).Rows(i).Item("BookNo").ToString
                Dim PhysicalDecription = ds.Tables(0).Rows(i).Item("PhysicalDescription").ToString
                Dim Category = ds.Tables(0).Rows(i).Item("CategoryName").ToString
                Dim MediaType = ds.Tables(0).Rows(i).Item("MediaTypeName").ToString
                Dim Subject = ds.Tables(0).Rows(i).Item("Subject").ToString
                Dim CollectionType = ds.Tables(0).Rows(i).Item("CollectionTypeName").ToString
                Dim Location = ds.Tables(0).Rows(i).Item("LocationName").ToString
                Dim Section = ds.Tables(0).Rows(i).Item("SectionName").ToString
                Dim Condition = ds.Tables(0).Rows(i).Item("ConditionName").ToString
                Dim Responsibility = ds.Tables(0).Rows(i).Item("ResponsibilityTitle").ToString
                Dim ResTitle = ds.Tables(0).Rows(i).Item("ResTitle").ToString
                Dim ISBN = ds.Tables(0).Rows(i).Item("ISBN").ToString
                Dim volume = ds.Tables(0).Rows(i).Item("Vol").ToString
                Dim Edition = ds.Tables(0).Rows(i).Item("Edition").ToString
                Dim Language = ds.Tables(0).Rows(i).Item("LanguageName").ToString
                Dim Keyword = ds.Tables(0).Rows(i).Item("Keyword").ToString
                Dim Notes = ds.Tables(0).Rows(i).Item("Notes").ToString
                Dim Publisher = ds.Tables(0).Rows(i).Item("Publisher").ToString
                Dim Place = ds.Tables(0).Rows(i).Item("Place").ToString
                Dim Year = ds.Tables(0).Rows(i).Item("Year").ToString
                Dim Price = ds.Tables(0).Rows(i).Item("Price").ToString
                Dim BillNo = ds.Tables(0).Rows(i).Item("BillNo").ToString
                Dim URL = ds.Tables(0).Rows(i).Item("URL").ToString




                sb.AppendLine("<tr><td class='numeric'>" & (i + 1) & "</td>")
                sb.AppendLine("<td>" & Title & "</td>")
                sb.AppendLine("<td>" & subtilte & "</td>")
                sb.AppendLine("<td>" & class1 & "</td>")
                sb.AppendLine("<td>" & BookNo & "</td>")
                sb.AppendLine("<td>" & PhysicalDecription & "</td>")
                sb.AppendLine("<td>" & Category & "</td>")
                sb.AppendLine("<td>" & MediaType & "</td>")
                sb.AppendLine("<td>" & Subject & "</td>")
                sb.AppendLine("<td>" & CollectionType & "</td>")
                sb.AppendLine("<td>" & Location & "</td>")
                sb.AppendLine("<td>" & Section & "</td>")
                sb.AppendLine("<td>" & Condition & "</td>")
                sb.AppendLine("<td>" & Responsibility & "</td>")
                sb.AppendLine("<td>" & ResTitle & "</td>")
                sb.AppendLine("<td>" & ISBN & "</td>")
                sb.AppendLine("<td>" & volume & "</td>")
                sb.AppendLine("<td>" & Edition & "</td>")
                sb.AppendLine("<td>" & Language & "</td>")
                sb.AppendLine("<td>" & Keyword & "</td>")
                sb.AppendLine("<td>" & Notes & "</td>")
                sb.AppendLine("<td>" & Publisher & "</td>")
                sb.AppendLine("<td>" & Place & "</td>")
                sb.AppendLine("<td>" & Year & "</td>")
                sb.AppendLine("<td>" & Price & "</td>")
                sb.AppendLine("<td>" & BillNo & "</td>")
                sb.AppendLine("<td>" & URL & "</td>")

                sb.AppendLine("<td style='text-align:center'><a href='/Library/BookInfo.aspx?EditID=" & EditID & "'> Edit </a></td>")
                sb.AppendLine("<td style='text-align:center'><a href='/Library/BookInfo.aspx?DeleteID=" & EditID & "'> Delete </a></td>")

                sb.AppendLine("</tr>")

                i += 1
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            listData.InnerHtml = sb.ToString()
        End If
    End Sub

    Sub GetBookInfoByID(ByVal BookID As String)
        Dim sql As String
        Dim sb As StringBuilder = New StringBuilder("")
        Try

            sql = "exec [Library].[usp_BookInfo]  @flag='s', @BookID ='" & BookID & "'"
            ds = Dao.ExecuteDataset(sql)

            txttitle.Text = ds.Tables(0).Rows(0).Item("Title").ToString
            txtsubtitle.Text = ds.Tables(0).Rows(0).Item("SubTitle").ToString
            txtclass.Text = ds.Tables(0).Rows(0).Item("Class").ToString
            txtbookno.Text = ds.Tables(0).Rows(0).Item("BookNo").ToString
            txtphyiscaldes.Text = ds.Tables(0).Rows(0).Item("PhysicalDescription").ToString

            ddlmediatype.SelectedIndex = ddlmediatype.Items.IndexOf(ddlmediatype.Items.FindByValue(ds.Tables(0).Rows(0).Item("MediaType").ToString))
            'ddlmediatype.SelectedValue = ds.Tables(0).Rows(0).Item("MediaType").ToString
            txtsubject.Text = ds.Tables(0).Rows(0).Item("Subject").ToString
            
            ddlResposibility.SelectedValue = ds.Tables(0).Rows(0).Item("Responsibility").ToString
            txtrestitle.Text = ds.Tables(0).Rows(0).Item("ResTitle").ToString
            txtisbn.Text = ds.Tables(0).Rows(0).Item("ISBN").ToString
            txtvolume.Text = ds.Tables(0).Rows(0).Item("Vol").ToString
            txtedition.Text = ds.Tables(0).Rows(0).Item("Edition").ToString
            ddllanguage.SelectedValue = ds.Tables(0).Rows(0).Item("Language").ToString
            txtkeyword.Text = ds.Tables(0).Rows(0).Item("Keyword").ToString
            txtnotes.Text = ds.Tables(0).Rows(0).Item("Notes").ToString
            txtpublisher.Text = ds.Tables(0).Rows(0).Item("Publisher").ToString
            txtplace.Text = ds.Tables(0).Rows(0).Item("Place").ToString
            txtyear.Text = ds.Tables(0).Rows(0).Item("Year").ToString
            txtprice.Text = ds.Tables(0).Rows(0).Item("Price").ToString
            txtbillno.Text = ds.Tables(0).Rows(0).Item("BillNo").ToString
            txturl.Text = ds.Tables(0).Rows(0).Item("URL").ToString

            btnSubmit.Text = "Update"
            btnBacktoSearchDetail.Visible = True
            btnCancel.Visible = False
            divBookTitle.Style.Add("display", "inline")
            lbBookTitle.Text = ds.Tables(0).Rows(0).Item("Title").ToString

        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    Sub DeleteBookInfo(ByVal BookID As String)

        Dim sb As StringBuilder = New StringBuilder("")

        Try
            Dim sql As String
            sql = "exec [Library].[usp_BookInfo] @flag='d' ,@BookID ='" & BookID & "'"

            ds = Dao.ExecuteDataset(sql)

            If (ds.Tables(0).Rows.Count > 0) Then
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
            'GetBookInfo()

        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString



        End Try

    End Sub

    Sub UpdateBookInfo(ByVal BookID As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Library].[usp_BookInfo] @flag='u',@BookID='" & BookID &
                "',@Title =N'" & Dao.FilterQuote(txttitle.Text.ToString().Trim) &
                "', @SubTitle =N'" & Dao.FilterQuote(txtsubtitle.Text.ToString.Trim) &
                "',@Class =N'" & txtclass.Text.Trim &
                "',@BookNo =N'" & txtbookno.Text.Trim &
                "',@PhysicalDescription=N'" & txtphyiscaldes.Text.Trim &
                "',@MediaType=N'" & ddlmediatype.SelectedValue.ToString.Trim() &
                "',@Subject=N'" & Dao.FilterQuote(txtsubject.Text.Trim()) &
                "',@Responsibility=N'" & ddlResposibility.SelectedValue.ToString.Trim() &
                "',@ResTitle=N'" & Dao.FilterQuote(txtrestitle.Text.Trim()) &
                "',@ISBN=N'" & txtisbn.Text.Trim() &
                "',@Vol=N'" & txtvolume.Text.Trim() &
                "',@Edition=N'" & txtedition.Text.Trim() &
                "',@Language=N'" & ddllanguage.SelectedValue.ToString.Trim() &
                "',@Keyword=N'" & Dao.FilterQuote(txtkeyword.Text.Trim()) &
                "',@Notes=N'" & Dao.FilterQuote(txtnotes.Text.Trim()) &
                "',@Publisher=N'" & Dao.FilterQuote(txtpublisher.Text.Trim()) &
                "',@Place=N'" & Dao.FilterQuote(txtplace.Text.Trim()) &
                "',@Year=N'" & txtyear.Text.Trim() &
                "',@Price=N'" & txtprice.Text.Trim() &
                "',@BillNo=N'" & txtbillno.Text.Trim() &
                "',@URL='" & txturl.Text.Trim() & "'"


            ds = Dao.ExecuteDataset(sql)

            If ds.Tables(0).Rows.Count > 0 Then
                If Not (String.IsNullOrWhiteSpace(ds.Tables(0).Rows(0).Item("id").ToString)) Then
                    hfBookId.Value = ds.Tables(0).Rows(0).Item("id").ToString
                End If
                sb.AppendLine("<div class='note note-success'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
                'ClearFields()
                'btnSubmit.Text = "Submit"
            End If
        Catch ex As Exception
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub
    Sub ClearFields()
        txttitle.Text = ""
        txtsubtitle.Text = ""
        txtbookno.Text = ""
       
        If (chkIsStatic.Checked = False) Then
            txtclass.Text = ""
            txtphyiscaldes.Text = ""
            txtsubject.Text = ""
            ddlmediatype.SelectedIndex = 0          
            ddlResposibility.SelectedIndex = 0
            ddllanguage.SelectedIndex = 0
            txtkeyword.Text = ""
            txtplace.Text = ""
            txtyear.Text = ""
            txtpublisher.Text = ""
        End If
        txtrestitle.Text = ""
        txtisbn.Text = ""
        txtvolume.Text = ""
        txtedition.Text = ""
        txtnotes.Text = ""
        txtprice.Text = ""
        txtbillno.Text = ""
        txturl.Text = ""
        txtaccessionno.Text = ""
        txttitle.Focus()

    End Sub


    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text.ToLower = "update" Then
            EditID = Request.QueryString("EditID")
            UpdateBookInfo(EditID)

        Else
            InsertBookInfo()
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        ClearFields()
        message.InnerHtml = ""
    End Sub


    Private Sub CascadResponsibility()
        ddlResposibility.Items.Clear()
        Sql = "EXEC [Setup].[usp_Responsibility] @flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlResposibility.DataSource = ds.Tables(0)
            ddlResposibility.DataValueField = "ResponsibilityID"
            ddlResposibility.DataTextField = "ResponsibilityTitle"
            ddlResposibility.DataBind()
            ddlResposibility.Items.Insert(0, New ListItem("-- Select Responsibility -- ", ""))
        Else
            ddlResposibility.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    'Private Sub CascadeCategory()
    '    ddlcategory.Items.Clear()
    '    Sql = "EXEC [Library].[usp_Transaction] @flag='getDdlCategory'"
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(Sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddlcategory.DataSource = ds.Tables(0)
    '        ddlcategory.DataValueField = "CategoryID"
    '        ddlcategory.DataTextField = "Category"
    '        ddlcategory.DataBind()
    '        ddlcategory.Items.Insert(0, New ListItem("-- Select Category -- ", ""))
    '    Else
    '        ddlcategory.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub

    'Private Sub CascadeCondition()
    '    ddlcondition.Items.Clear()
    '    Sql = "EXEC [Setup].[usp_Condition] @flag='s'"
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(Sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddlcondition.DataSource = ds.Tables(0)
    '        ddlcondition.DataValueField = "ConditionID"
    '        ddlcondition.DataTextField = "Condition"
    '        ddlcondition.DataBind()
    '        ddlcondition.Items.Insert(0, New ListItem("-- Select Condition -- ", ""))
    '    Else
    '        ddlcondition.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub

    'Private Sub CascadeLocation()
    '    ddllocation.Items.Clear()
    '    Sql = "EXEC [Setup].[usp_Location] @flag='s'"
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(Sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddllocation.DataSource = ds.Tables(0)
    '        ddllocation.DataValueField = "LocationID"
    '        ddllocation.DataTextField = "LocationName"
    '        ddllocation.DataBind()
    '        ddllocation.Items.Insert(0, New ListItem("-- Select Location -- ", ""))
    '    Else
    '        ddllocation.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub

    Private Sub CascadeMediaType()
        ddlmediatype.Items.Clear()
        Sql = "EXEC [Setup].[usp_MediaType] @flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlmediatype.DataSource = ds.Tables(0)
            ddlmediatype.DataValueField = "MediaTypeID"
            ddlmediatype.DataTextField = "MediaType"
            ddlmediatype.DataBind()
            ddlmediatype.Items.Insert(0, New ListItem("-- Select Media Type -- ", ""))
        Else
            ddlmediatype.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    'Private Sub CascadeCollectionType()
    '    ddlcollectiontype.Items.Clear()
    '    Sql = "EXEC [Setup].[usp_CollectionType] @flag='s'"
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(Sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddlcollectiontype.DataSource = ds.Tables(0)
    '        ddlcollectiontype.DataValueField = "CollectionTypeID"
    '        ddlcollectiontype.DataTextField = "CollectionType"
    '        ddlcollectiontype.DataBind()
    '        ddlcollectiontype.Items.Insert(0, New ListItem("-- Select Collection Type -- ", ""))
    '    Else
    '        ddlcollectiontype.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub

    Private Sub CascadeLanguage()
        ddllanguage.Items.Clear()
        Sql = "EXEC [Setup].[usp_LibraryLanguage] @flag='s'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddllanguage.DataSource = ds.Tables(0)
            ddllanguage.DataValueField = "LanguageID"
            ddllanguage.DataTextField = "Language"
            ddllanguage.DataBind()
            ddllanguage.Items.Insert(0, New ListItem("-- Select Language -- ", ""))
        Else
            ddllanguage.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    'Private Sub CascadeSection()
    '    ddlsection.Items.Clear()
    '    Sql = "EXEC [Setup].[usp_LibrarySection] @flag='s'"
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(Sql)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddlsection.DataSource = ds.Tables(0)
    '        ddlsection.DataValueField = "SectionID"
    '        ddlsection.DataTextField = "SectionName"
    '        ddlsection.DataBind()
    '        ddlsection.Items.Insert(0, New ListItem("-- Select Section -- ", ""))
    '    Else
    '        ddlsection.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
    '    End If
    'End Sub

    Private Sub TotalBookInfo()

        Sql = "EXEC [Library].[usp_TopStatus] @flag='TotalBooks'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            TotalBooks.InnerHtml = ds.Tables(0).Rows(0).Item("TotalBooks").ToString


        End If
    End Sub

    Private Sub BooksDistinct()

        Sql = "EXEC [Library].[usp_TopStatus] @flag='distinctBooks'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            DistinctBooks.InnerHtml = ds.Tables(0).Rows(0).Item("DistinctBooks").ToString


        End If
    End Sub

    Private Sub TodayTotals()

        Sql = "EXEC [Library].[usp_TopStatus] @flag='TodayTotal'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            TodayTotal.InnerHtml = ds.Tables(0).Rows(0).Item("TotayTotal").ToString


        End If
    End Sub

    Private Sub CurrentUsers()

        Sql = "EXEC [Library].[usp_TopStatus] @flag='CurrentUsers'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            TotalByCurrentUser.InnerHtml = ds.Tables(0).Rows(0).Item("CurrentUsers").ToString


        End If
    End Sub
    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function


    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Student_Details_list")
    End Sub

    Protected Sub btnBacktoSearchDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBacktoSearchDetail.Click
        Response.Redirect("/Library/BookSearchDetails.aspx?BookID=" & hfBookId.Value)
    End Sub
End Class