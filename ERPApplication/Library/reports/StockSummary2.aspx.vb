﻿Imports System.IO
Imports System.Web.HttpContext
Public Class StockSummary2
    Inherits System.Web.UI.Page


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim currentUri = Request.Url.AbsolutePath
        'Dim pa As New PageAuthority
        'Dim uid As String
        'If (Session("userId") <> Nothing) Then
        '    uid = Session("userID").ToString()
        'Else
        '    Response.Redirect("/Logout.aspx")
        'End If
        'Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        'If Not hasRightToView Then
        '    Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
        '    Response.Redirect(redirectUrl)
        '    UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
        'Else
        '    UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
        'End If


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CurrentStatus()
    End Sub

    Private Sub CurrentStatus()
        Dim sb = New StringBuilder()
        Dim ds = (New DatabaseDao().ExecuteDataset("[Library].[usp_LibaryStockSumamry]"))
        Dim ReportTitle = "Test"
        For t As Integer = 0 To ds.Tables.Count - 1

            If ds.Tables(t).Rows.Count > 0 Then
                ' sb.Append("<div class='row'>")
                sb.Append("<div class='col-md-6'>")
                sb.Append("<div class='portlet box green'>")
                sb.Append("<div class='portlet-title'>")
                sb.Append("<div class='caption'>")
                sb.Append("<i class='fa fa-cogs'></i>" & ds.Tables(t).Columns(1).ColumnName.ToString & "</div>")
                sb.Append("<div class='tools'>")
                sb.Append("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
                sb.Append("</div>")
                sb.Append("</div>")
                sb.Append("<div class='portlet-body flip-scroll'>")


                sb.Append("<table  class='display table table-bordered table-striped table-condensed flip-content summarytable' id='Summary'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<th>SN</th>")
                For i As Integer = 1 To ds.Tables(t).Columns.Count - 1
                    sb.AppendLine("<th>" & ds.Tables(t).Columns(i).ColumnName.ToString & "</th>")
                Next

                sb.Append("</thead>")
                sb.Append("<tbody>")

                For r As Integer = 0 To ds.Tables(t).Rows.Count - 1
                    sb.Append("<tr>")
                    sb.Append("<td class='text-center'>" & r + 1 & "</td>")

                    Dim keyVal As String = ds.Tables(t).Rows(r)(0).ToString.Trim
                    Dim searchVal As String = ds.Tables(t).Rows(r)(1).ToString.Trim

                    For c As Integer = 1 To ds.Tables(t).Columns.Count - 1
                        If c = 1 Then
                            sb.Append("<td class='text-center'><a data-toggle='modal' data-keyVal='" & keyVal & "' data-searchVal='" & searchVal & "' data-title='" & ds.Tables(t).Columns(1).ColumnName.ToString.Trim & "' id='link' class='viewList' href='#portlet-config'>" & ds.Tables(t).Rows(r)(c).ToString & "</a></td>")
                        Else
                            sb.Append("<td class='text-center'>" & ds.Tables(t).Rows(r)(c).ToString & "</td>")
                        End If

                    Next
                    sb.Append("</tr>")
                Next

                sb.Append("</tbody>")
                sb.Append("<tfoot>")
                sb.Append("<tr>")
                sb.Append("<th colspan='2' style='text-align:right'>Total:</th>")
                sb.Append("<th></th>")
                sb.Append("</tr>")
                sb.Append("</tfoot>")

                sb.Append("</table>")
                sb.Append("</div>")
                sb.Append("</div>")
                sb.Append("</div>")
                '  sb.Append("</div>")
                listData.InnerHtml = sb.ToString
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "InitializeDatatable", "initDataTable();", True)
            Else
                Throw New ArgumentNullException("Dataset is empty")
            End If
        Next

    End Sub

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function


    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Student_Details_list")
    End Sub

End Class