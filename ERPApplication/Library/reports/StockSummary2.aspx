﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="StockSummary2.aspx.vb" Inherits="School.StockSummary2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

 <link href="../../assets/datatableplugins/css/buttons.dataTables.min.css" rel="stylesheet"
        type="text/css" /> 
    <script src="../../assets/datatableplugins/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/ajax/jszip.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/ajax/pdfmake.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/ajax/vfs_fonts.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/datatableplugins/js/buttons.print.min.js" type="text/javascript"></script>
     <style type="text/css">
        .portlet-body {
    background-color: #fff;
    padding: 10px;
    height: 354.2px !important;
}
    </style>
    <script type="text/javascript">
        function print(divId) {
            var contents = $("#" + divId).html();
            var win = window.open("", "", "height=500,width=900");
            win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
            win.document.write("</head><body >");
            win.document.write(contents);
            win.document.write("</body></html>");
            win.print();
            win.close();
            return true;
        }
        $(document).ready(function () {
            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });


        });

//        $(document).ready(function () {

        function initDataTable(){
            return $('table.summarytable').DataTable({
                "retrieve": true,
                "dom": 'Bfrtip',
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                 buttons: [
                        
                        {   extend: 'excel',
                            title: 'Stock Summary',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }, 'pageLength'
                        ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;
                    debugger;
                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                             typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(2)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                            .column(2, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    // Update footer
                    $(api.column(2).footer()).html(
                        total
                );
                }
            });
//        });
        }
    </script>
     <script type="text/javascript">
         $(document).ready(function () {

             $(document).on('click', '.viewList', function () {

                 var keyVal = $(this).attr("data-keyVal");
                 var searchVal = $(this).attr("data-searchVal");
                 var lstHeader = $(this).attr("data-title");
                 $('#model_data').html("Loading <img src='../../assets/img/loading.gif' />");
                 $.ajax({
                     type: 'POST',
                     url: '/SetupServices.svc/GetStockDetails',
                     data: '{"keyValue" : "' + keyVal + '","searchValue" : "' + searchVal + '","header" : "' + lstHeader + '"}',
                     dataType: 'json',
                     contentType: 'application/json',
                     success: function (data) {
                         $('#model_data').html('');
                         $('#model_data').html(data);
                         var table1 = initListDataTable(lstHeader);
                     },
                     error: function (data) {
                         alert(data.statusText);
                     },
                     failure: function (data) {
                         alert(data.statusText);
                     }
                 });
             });

             function initListDataTable(tbltitle) {
                 return $("table.detailstable").DataTable({
                     "retrieve": true,
                     dom: 'Bfrtip',
                     "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                     "ordering": false,
                     buttons: [
                        {
                            extend: 'colvis',
                            postfixButtons: ['colvisRestore'],
                            collectionLayout: 'fixed two-column',
                            columns: ':not(.noVis)'
                        },
                        { extend: 'excel',
                            title: 'Stock Summary Details By ' + tbltitle,
                            exportOptions: {
                                columns: ':visible'
                            }
                        }, 'pageLength'
                        ]
                 });
             }

         });
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            List of Items in Stock.</h4>
                    </div>
                    <div class="modal-body">
                        <div id="model_data">
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                  Stock Summaries<small>Library Book Stocks Summaries as per book properties.</small></h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-gear"></i><a href="">Reports</a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><i class="fa fa-list"></i><a href="/library/reports/stocksummary2.aspx">Stock Summaries</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div id="listData" runat="server">
                </div>
            </div>
            <div class="col-md-12 ">
                <div id="Div2" runat="server">
                </div>
                <div id="Div1" runat="server">
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" Style="display: none;" />
</asp:Content>
