﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="BookInfo.aspx.vb" Inherits="School.LibraryInfo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus
        {
            color: #fff;
            cursor: default;
            background-color: #35AA47 !important;
            border: 1px solid #ddd;
            border-bottom-color: rgb(221, 221, 221);
            border-bottom-color: transparent;
        }
        .nav-tabs
        {
            background-color: #f5f5f5;
        }
        a, a:active, a:focus
        {
            outline: none; /* Works in Firefox, Chrome, IE8 and above */
        }
        
       
    </style>
    <script type="text/javascript">

        var checkFromValidation = false;

        $(document).ready(function () {
            $('#form').validate();
            $("#<%=txttitle.ClientID %>").rules('add', { required: true, messages: { required: 'Title is required.'} });
           
            $("#<%=txtclass.ClientID %>").rules('add', { required: true, messages: { required: 'Class is required.'} });
            $("#<%=txtbookno.ClientID %>").rules('add', { required: true, messages: { required: 'Book No is required.'} });
            $("#<%=ddlResposibility.ClientID %>").rules('add', { required: true, messages: { required: 'Selection is required.'} });
            
            $("#<%=ddlmediatype.ClientID %>").rules('add', { required: true, messages: { required: 'Selection is required.'} });
            
            $("#<%=txtsubject.ClientID %>").rules('add', { required: true, maxlength: 500, messages: { required: 'Required field', maxlength: jQuery.format("Not more than {0}")} });
            $("#<%=txtkeyword.ClientID %>").rules('add', { required: true, maxlenght: 500, messages: { required: 'Required field', maxlenght: jQuery.format("Not more than {0}")} });
            $("#<%=txtnotes.ClientID %>").rules('add', { required: true, maxlength: 500, messages: { required: 'Required field', maxlength: jQuery.format("Not more than {0}")} });

            checkFromValidation = function () {
                var bool = true;
                if ($('#<%=txttitle.ClientID %>').valid() == false) bool = false;

               
                if ($('#<%=txtclass.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtbookno.ClientID %>').valid() == false) bool = false;
                if ($('#<%=ddlResposibility.ClientID %>').valid() == false) bool = false;
                
                if ($('#<%=ddlmediatype.ClientID %>').valid() == false) bool = false;
               

                if ($('#<%=txtsubject.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtkeyword.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtnotes.ClientID %>').valid() == false) bool = false;


                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };

        });

    </script>
    <script type="text/javascript">
        function print(divId) {
            var contents = $("#" + divId).html();
            var win = window.open("", "", "height=500,width=900");
            win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
            win.document.write("</head><body >");
            win.document.write(contents);
            win.document.write("</body></html>");
            win.print();
            win.close();
            return true;
        }
        $(document).ready(function () {
            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });
        });
    </script>
    <script type="text/javascript">
        function googleISBN() {
            debugger;
            var isbn = $("#<%= txt_isbn_search.ClientID %>").val(); //get isbn direct from input, no need for php
            if (isbn === null || isbn === '') {
                alert("ISBN is empty");
            }
            else {
                var url = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn;
                $("#btn_Isbn").val("Loading...");
                $.getJSON(url, function (data) {

                })
              .done(function (data) {
                  console.log("success");
                  if (data.totalItems > 0) {
                      $.each(data.items, function (entryIndex, entry) {


                          $("#<%= txttitle.ClientID %>").val(entry.volumeInfo.title)
                          $("#<%= txtsubtitle.ClientID %>").val(entry.volumeInfo.subtitle)
                          $("#<%= txtrestitle.ClientID %>").val(entry.volumeInfo.authors)
                          $("#<%= txtnotes.ClientID %>").val(entry.volumeInfo.description)
                          $("#<%= txtyear.ClientID %>").val(entry.volumeInfo.publishedDate)
                          $("#<%= txtisbn.ClientID %>").val(entry.volumeInfo.identifier)
                          $("#<%= txtsubject.ClientID %>").val(entry.volumeInfo.categories)
                          $("#<%= txtpublisher.ClientID %>").val(entry.volumeInfo.publisher)
                          $("#<%= txtphyiscaldes.ClientID %>").val(entry.volumeInfo.pageCount)
                          $("#<%= txtkeyword.ClientID %>").val(entry.volumeInfo.textSnippet)
                          $("#<%= txturl.ClientID %>").val(entry.volumeInfo.infoLink)
                          $("#<%= txtisbn.ClientID %>").val(isbn)


                      });
                      getclassification();
                  }
                  else {
                      alert("No information found");
                      $("#btn_Isbn").val("Find");
                  }
              })
              .fail(function (jqxhr, textStatus, error) {
                  var err = textStatus + ", " + error;
                  console.log("Request Failed: " + err);
              });
            }
        }
        function getclassification() {
             var isbn = $("#<%= txt_isbn_search.ClientID %>").val();
                $.ajax({
                    url: "/SetupServices.svc/GetBookClassification",
                    dataType: "json",
                    type: 'POST',
                    data: '{"isbn":' + isbn + '}',
                    contentType: 'application/json',
                    success: function (data) {
                        var s = data.split(":");
                        for (var k = 0; k < s.length; k++) {
                            $("#<%= txtclass.ClientID %>").val(s[0]);
                            $("#<%= txtbookno.ClientID %>").val(s[1]);

                        }
                        $("#btn_Isbn").val("Find");
                    }
                    ,
                    error: function (data) {
                        alert(data.statusText);
                    },
                    failure: function (data) {
                        alert(data.statusText);
                    }
                });

            
        }

        $(document).ready(function () {

            $('#btn_Isbn').click(function (ev) {
                ev.preventDefault();
                var isbn = $("#<%= txt_isbn_search.ClientID %>").val();
                if (isbn === null || isbn === '') {
                    alert("ISBN is empty");
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/SetupServices.svc/CheckIfExistsISBN',
                        data: '{"ISBN" : "' + isbn + '"}',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            //                        alert(data);
                            if (data == "0") {
                                googleISBN();
                            }
                            else {
                                window.location.href = "/Library/BookAccessioning.aspx?BookID=" + data;
                            }
                        },
                        error: function (data) {
                            alert(data.statusText);
                        },
                        failure: function (data) {
                            alert(data.statusText);
                        }

                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
//        $(document).ready(function () {
//            $('#btn_Isbn').click(function () {
//                


//        });
    </script>
     <script type="text/javascript">
        

             function validateAccessionNo() {
                 debugger;
                 var accessionNo = $("#<%= txtaccessionno.ClientID%>").val();

                 $.ajax({
                     type: 'POST',
                     url: '/SetupServices.svc/CheckIfExistsBookAccessionNo',
                     data: '{"Accession" : "' + accessionNo + '"}',
                     dataType: 'json',
                     contentType: 'application/json',
                     success: function (data) {
                         //                        alert(data);
                         if (!(data == "NotDuplicate")) {
                             $('.accessioncheck').prop("style", "block");
                             $('.accessioncheck').html(data);
                             $("#<%= txtaccessionno.ClientID%>").val('');
//                             $("#<%= txtaccessionno.ClientID%>").focus(function () {
//                                 $(this).val('');
//                             });
                               
                         }
                     },
                     error: function (data) {
                         alert(data.statusText);
                     },
                     failure: function (data) {
                         alert(data.statusText);
                     }

                 });

             }
        
      </script>
      <script type="text/javascript">
          
          function imageExists(url, callback) {
              var img = new Image();
              img.onload = function () { callback(true); };
              img.onerror = function () { callback(false); };
              img.src = url;
          }

          function Books_Populated(sender, e) {
              debugger;
              var books = sender.get_completionList().childNodes;
//              for (var i = 0; i < books.length; i++) {
//                  var div = document.createElement("DIV");
//                  var text = books[i].firstChild.nodeValue;
//                  var name = text.split('~')[0]
//                  var isbn = $.trim(text.split('~')[1]);
//                  if (isbn == "") {
//                      isbn = "NoCover";
//                  }

//                  var url = "/Library/Images/BookCovers/" + isbn + ".jpg";


////                  var imgurl = imageExists(url, function (exists) {
////                      if (exists == true) {
////                          return "/Library/Images/BookCovers/" + isbn + ".jpg";
////                      }
////                      else if (exists == false) {
////                          return "/Library/Images/BookCovers/NoCover.jpg";
////                      }
////                      else {
////                          return "";
////                      }
////                  });

//                  div.innerHTML = "<img style = 'height:100px;width:90px' src = '" + url + "' class='img-responsive' /><br />";

////                  div.innerHTML = "<img style = 'height:100px;width:90px' src = '/Library/Images/BookCovers/" + isbn + ".jpg' class='img-responsive' /><br />";
////                  div.innerHTML = "<a href = '/Library/BookAccessioning.aspx?BookID=" + books[i]._value + "' target='_blank' />" + books[i].firstChild.nodeValue + "<a/>";
//                  books[i].appendChild(div);

//              }
          }

          function OnBook_Selected(source, eventArgs) {
              debugger;
              var idx = source._selectIndex;
              var books = source.get_completionList().childNodes;
              var value = books[idx]._value;
              var text = books[idx].firstChild.nodeValue;
              var name = text.split('~')[0]
              var isbn = text.split('~')[1];
              source.get_element().value = name;
              window.location.href = "/Library/BookAccessioning.aspx?BookID=" + value;
          }
      </script>
      <script type="text/javascript">
//          $(document).ready(function () {

//              $('#btn_Isbn').click(function (ev) {
//                  ev.preventDefault();
//              });
//          });


          function onEnterkeyPress(event) {
              debugger;
              if (event.keyCode == 13) {
                  $("#btn_Isbn").click();
                  return false;
              }
          }
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                           Form Settings</h4>
                    </div>
                    <div class="modal-body">
                         <asp:CheckBox ID="chkIsStatic" runat="server" Text="Tick if want to keep all list data static." />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue hidden">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Book Info <small>Book Information for Application.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li>Total Books: <span id="TotalBooks" runat="server"></span>Distinct Books: <span
                        id="DistinctBooks" runat="server"></span>Total Current Users: <span id="TotalByCurrentUser"
                            runat="server"></span>Today Total: <span id="TodayTotal" runat="server"></span>
                        <div id="divBookTitle" runat="server" style="display:none;"><span class="fa fa-book"></span>
                        <asp:Label ID="lbBookTitle" runat="server" Text="" Style="color:Green"></asp:Label></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1_1" data-toggle="tab">Basic Book info</a></li>
                    <%--<li class=""><a href="#tab_1_2" data-toggle="tab">Bliographic</a></li>--%>
                    <li class=""><a href="#tab_1_3" data-toggle="tab">Notes</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_1_1">
                        <div class="portlet box green ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Book Info 1
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a>
                                   <a class="config" href="#portlet-config" data-toggle="modal"></a>
							<%--<a href="" class="reload"></a>--%>
                                    <a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                ISBN :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txt_isbn_search" runat="server" CssClass="form-control" placeholder="Enter ISBN or ISSN" onkeypress="return onEnterkeyPress(event);"></asp:TextBox>
                                                </div>
                                                <label for="<%=txt_isbn_search.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <%--</div>
                                          <div class="form-group">--%>
                                            <div class="col-md-6">
                                                <input id="btn_Isbn" type="button" class="btn green" value="Find" />
                                              
                                          

                                            </div>
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="col-md-2 control-label">
                                                Accession No:</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtaccessionno" runat="server" CssClass="form-control" placeholder="eg.1,5,07-15,090-120" onblur="return validateAccessionNo()"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtaccessionno.ClientID%>" class="error accessioncheck" style="display: none;">
                                                </label>
                                            </div>
                                           
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Title:</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txttitle" runat="server" CssClass="form-control" placeholder="Enter Title" autocomplete="off"></asp:TextBox>

                                                    <asp:AutoCompleteExtender ServiceMethod="SearchBooks" MinimumPrefixLength="3"
                                                    ServicePath="../Autocomplete.asmx" CompletionInterval="100" EnableCaching="true"
                                                    CompletionSetCount="10" TargetControlID="txttitle" ID="AutoCompleteExtender2"
                                                    runat="server" FirstRowSelected="false" CompletionListCssClass="autocomplete_completionListElement"
                                                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                    BehaviorID="AutoCompleteEx2" ShowOnlyCurrentWordInCompletionListItem="true" OnClientPopulated="Books_Populated" OnClientItemSelected="OnBook_Selected"> 
                                                     </asp:AutoCompleteExtender>
                                                </div>
                                                <label for="<%=txttitle.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Sub-Title:</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtsubtitle" runat="server" CssClass="form-control" placeholder="Enter Sub Title"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtsubtitle.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Class :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtclass" runat="server" CssClass="form-control" placeholder="Enter Class"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtclass.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                Book No :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtbookno" runat="server" CssClass="form-control" placeholder="Enter Book No"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtbookno.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Resposibility :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlResposibility" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                Resposibility Title :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtrestitle" runat="server" CssClass="form-control" placeholder="Enter Responsibility Title"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtrestitle.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           
                                            <label class="col-md-2 control-label">
                                                Media Type :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddlmediatype" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                             <label class="col-md-2 control-label">
                                                Physical Description :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtphyiscaldes" runat="server" CssClass="form-control" placeholder="Enter Physical Description"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtphyiscaldes.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Edition :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtedition" runat="server" CssClass="form-control" placeholder="Enter Edition"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtedition.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                Volume :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtvolume" runat="server" CssClass="form-control" placeholder="Enter Volume"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtvolume.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Publisher :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtpublisher" runat="server" CssClass="form-control" placeholder="Enter Publisher"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtpublisher.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                ISBN :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtisbn" runat="server" CssClass="form-control" placeholder="Enter ISBN"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtisbn.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                Language :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:DropDownList ID="ddllanguage" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">                                           
                                            <label class="col-md-2 control-label">
                                                Place :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtplace" runat="server" CssClass="form-control" placeholder="Enter Place"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtplace.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                Year :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtyear" runat="server" CssClass="form-control" placeholder="Enter Year"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtyear.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<div class="tab-pane fade" id="tab_1_2">
                        <div class="portlet box green ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Book Info 2
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>							
                                    <a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">                                            
                                        </div>
                                        
                                        <div class="form-group">
                                                                                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <div class="tab-pane fade" id="tab_1_3">
                        <div class="portlet box green ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Book Info 2
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
							<%--<a href="" class="reload"></a>--%>
                                    <a href="" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Subject :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtsubject" TextMode="multiline" runat="server" CssClass="form-control"
                                                        placeholder="Enter Subject"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtsubject.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Keyword :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtkeyword" TextMode="multiline" runat="server" CssClass="form-control"
                                                        placeholder="Enter Keyword"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtkeyword.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Notes :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtnotes" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                        placeholder="Enter Notes"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtnotes.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                URL :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txturl" runat="server" CssClass="form-control" placeholder="Enter URL"></asp:TextBox>
                                                </div>
                                                <label for="<%=txturl.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Price :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtprice" runat="server" CssClass="form-control" placeholder="Enter Price"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtprice.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Bill No :</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                    <asp:TextBox ID="txtbillno" runat="server" CssClass="form-control" placeholder="Enter Bill No"></asp:TextBox>
                                                </div>
                                                <label for="<%=txtbillno.ClientID%>" class="error" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            <asp:Button ID="btnSubmit" class="btn green" OnClientClick="return checkFromValidation();"
                                runat="server" Text="Submit"/>
                            <asp:Button ID="btnCancel" type="button" class="btn default" runat="server" Text="Cancel" />
                            <asp:Button ID="btnBacktoSearchDetail" runat="server" Text="Go Back" Visible="false" class="btn blue"/>
                            <asp:HiddenField ID="hfBookId" runat="server" />
                        </div>
                    </div>
                    <div id="listData" runat="server">
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" Style="display: none;" />
    </div>
</asp:Content>
