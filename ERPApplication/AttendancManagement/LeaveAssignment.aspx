﻿<%@ Page Title="Leave Assignment" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="LeaveAssignment.aspx.vb" Inherits="School.LeaveAssignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link rel="stylesheet" href="../assets/Calendar/jquery.calendars.picker.css" type="text/css">
    <script src="../assets/Calendar/jquery.plugin.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../assets/Calendar/jquery.calendars.js" type="text/javascript"></script>
    <script src="../assets/Calendar/jquery.calendars.plus.js" type="text/javascript"></script>
    <script src="../assets/Calendar/jquery.calendars.picker.js" type="text/javascript"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../assets/Calendar/jquery.calendars.nepali.js" type="text/javascript"></script>
    <script type="text/javascript">
        var checkFromValidation = false;
        $(document).ready(function () {
            $('#form').validate();
            $("#<%=ddlBranch.ClientID %>").rules('add', { required: true, messages: { required: 'Branch is required.'} });
            $("#<%=ddlLeaveName.ClientID %>").rules('add', { required: true, messages: { required: 'Leave Name is required.'} });
            $("#<%=txtLeaveStartDate.ClientID %>").rules('add', { required: true, messages: { required: 'Leave Start Date is required.'} });
            $("#<%=txtLeaveEndDate.ClientID %>").rules('add', { required: true, messages: { required: 'Leave End Date required.'} });            
            $("#<%=ddlApprovedBy.ClientID %>").rules('add', { required: true, messages: { required: 'Approved By is required.'} });
            $("#<%=txtReason.ClientID %>").rules('add', { required: true, messages: { required: 'Reason is required.'} });

            checkFromValidation = function () {
                var bool = true;
                if ($('#<%=ddlBranch.ClientID %>').valid() == false) bool = false;
                if ($('#<%=ddlLeaveName.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtLeaveStartDate.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtLeaveEndDate.ClientID %>').valid() == false) bool = false;              
                if ($('#<%=ddlApprovedBy.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtReason.ClientID %>').valid() == false) bool = false;
                if (!bool) $('#form').validate().focusInvalid();
                return bool;
            };
        });
    </script>
    <script type="text/javascript">
        function print(divId) {
            var contents = $("#" + divId).html();
            var win = window.open("", "", "height=500,width=900");
            win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
            win.document.write("</head><body >");
            win.document.write(contents);
            win.document.write("</body></html>");
            win.print();
            win.close();
            return true;
        }
        $(document).ready(function () {
            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(function () {
                var calendar = $.calendars.instance('nepali');
                $("#<%= txtLeaveStartDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                    ChangeDate(selectedDate, 'e', this);
                }
                });
                $("#<%= txtLeaveEndDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                    ChangeDate(selectedDate, 'e', this);
                }
                });

            });

            $('#<%= txtLeaveStartDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            $('#<%= txtLeaveEndDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            $('#<%= txtApprovedDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();

            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
                $(function () {
                    var calendar = $.calendars.instance('nepali');
                    $("#<%= txtLeaveStartDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });
                    $("#<%= txtLeaveEndDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });

                });

                $('#<%= txtLeaveStartDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                $('#<%= txtLeaveEndDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                $('#<%= txtApprovedDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            });
        });
     </script>
      <script type="text/javascript">
          function formatDate(date, type) {
              console.log("date : " + date + " ,type : " + type);
              var month = '';
              var day = '';
              var year = '';
              var resultdate;
              if (type == "n") {
                  var d = new Date(date);
                  month = d.getMonth() + 1;
                  day = d.getDate();
                  year = d.getFullYear();
                  resultdate = [day, month, year].join('/');
              }
              else {

                  year = date[0]._year;
                  month = date[0]._month;
                  day = date[0]._day;
                  resultdate = [year, month, day].join('/');
              }
              console.log("formatted : " + resultdate);
              return resultdate;

          }



          function ChangeDate(value, obj, element) {
              value = formatDate(value, obj);
              var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';
              debugger;
              $.ajax({
                  type: 'POST',
                  url: '/SetupServices.svc/DateConverterNew',
                  data: dataToSend,
                  datatype: 'json',
                  contentType: 'application/json',
                  success: function (data) {
                      debugger;
                      var dd, mm, yy;
                      var value = [];
                      if (obj == "e") {
                          var fieldId1 = element.id.replace(/_Nep$/i, '');
                          //                        $("#" + fieldId1).val(data);
                          value = data.split("/");
                          dd = value[0];
                          mm = value[1];
                          yy = value[2];
                          $("#" + fieldId1).val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                      }
                      else if (obj == "n") {
                          var fieldId2 = element.id + '_Nep'
                          value = data.split("/");
                          dd = value[0];
                          mm = value[1];
                          yy = value[2];
                          $("#" + fieldId2).val(yy + "/" + mm + "/" + dd);
                      }
                  }
              });

          }
    </script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('#displaytable').DataTable();
         });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Leave Assignment <small>Assign Leave for Employee.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    
                    <li><a href="/AttendanceManagement/LeaveAssignment.aspx">Leave Assignment</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#create" aria-controls="create" role="tab"
                            data-toggle="tab">Add</a></li>
                        <li role="presentation"><a href="#display" aria-controls="display" role="tab" data-toggle="tab">
                            View</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="create">
                            <div class="portlet box purple ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>Assign Leave
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a><a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-horizontal">
                                        <div class="form-body">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Branch/Depart :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                                <asp:DropDownList ID="ddlBranch" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label for="<%=ddlBranch.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div runat="server" id="divEmpChkList" visible="false">
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">
                                                            </label>
                                                            <div class="col-md-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                                                    <asp:CheckBox ID="ck_select" runat="server" AutoPostBack="True" Text="Select All/Deselect All" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">
                                                                Employees :</label>
                                                            <div class="col-md-10">
                                                                <div class="portlet box" style="overflow-y: auto; max-height: 200px;">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                                                        <asp:CheckBoxList ID="chkEmployeeList" runat="server" CssClass="form-control" placeholder="Enter text"
                                                                            RepeatColumns="3" CellPadding="3" CellSpacing="2" RepeatLayout="table" RepeatDirection="Horizontal"
                                                                            Font-Size="Smaller" Height="10">
                                                                        </asp:CheckBoxList>
                                                                    </div>
                                                                </div>
                                                                <label for="<%=chkEmployeeList.ClientID%>" class="error" style="display: none;">
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" runat="server" id="divEmpList" visible="false">
                                                        <label class="col-md-2 control-label">
                                                            Employees :</label>
                                                        <div class="col-md-10">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                                <asp:DropDownList ID="ddlEmployeeName" runat="server" CssClass="form-control" placeholder="Enter Branch Name">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label for="<%=ddlEmployeeName.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Leave Name :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                                                <asp:DropDownList ID="ddlLeaveName" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label for="<%=ddlLeaveName.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                        <label class="col-md-2 control-label">
                                                            Leave Type :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-caret-down"></i></span>
                                                                <asp:DropDownList ID="ddlLeaveType" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label for="<%= ddlLeaveType.ClientID%>" class="error" style="display: none">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Approved By :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                                                <asp:DropDownList ID="ddlApprovedBy" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label for="<%=ddlApprovedBy.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                        <label class="col-md-2 control-label">
                                                            Approved Date :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                                <asp:TextBox ID="txtApprovedDate" runat="server" CssClass="form-control" placeholder="Enter Approved Date"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtApprovedDate.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Leave Start Date :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                                <asp:TextBox ID="txtLeaveStartDate" runat="server" CssClass="form-control" placeholder="Enter Start Date"
                                                                    onchange="ChangeDate(this.value,'n',this);"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtLeaveStartDate.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                        <label class="col-md-2 control-label">
                                                            Start Miti :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <asp:TextBox ID="txtLeaveStartDate_Nep" runat="server" CssClass="form-control" placeholder="Enter Start Miti"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtLeaveStartDate_Nep.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Leave End Date :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                                <asp:TextBox ID="txtLeaveEndDate" runat="server" CssClass="form-control" placeholder="Select Leave End Date"
                                                                    onchange="ChangeDate(this.value,'n',this);"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtLeaveEndDate.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                        <label class="col-md-2 control-label">
                                                            End Miti :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <asp:TextBox ID="txtLeaveEndDate_Nep" runat="server" CssClass="form-control" placeholder="Enter End Miti"></asp:TextBox>
                                                            </div>
                                                            <label for="<%=txtLeaveEndDate_Nep.ClientID%>" class="error" style="display: none;">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Has Half Day :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-check"></i></span>
                                                                <div class="form-control">
                                                                    <asp:CheckBox type="checkbox" ID="chkHasHalfDay" runat="server" Text=" Yes/No"></asp:CheckBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2 control-label">
                                                            Half Type :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                                <asp:DropDownList ID="ddlHalfType" runat="server" CssClass="form-control" placeholder="Description About The Branch">
                                                                    <asp:ListItem Value="">-- Select a half type --</asp:ListItem>
                                                                    <asp:ListItem Value="FirstHalf">First Half</asp:ListItem>
                                                                    <asp:ListItem Value="SecondHalf">Second Half</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Reason :</label>
                                                        <div class="col-md-10">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                                <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" placeholder="Write Reason For Leave"
                                                                    TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="form-actions fluid">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkFromValidation();"
                                                        runat="server" Text="Submit" />
                                                    <asp:Button ID="btnCancel" type="button" class="btn default" runat="server" Text="Cancel" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="display">
                            <div id="listData" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" style="display:none;" />   
</asp:Content>
