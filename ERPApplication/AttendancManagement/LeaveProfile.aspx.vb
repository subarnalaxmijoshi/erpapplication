﻿Imports System.IO
Public Class LeaveProfile
    Inherits System.Web.UI.Page
    Private Dao As New DatabaseDao
    Dim pdfDoc As New EmployeePdfReport
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrWhiteSpace(Request.QueryString("EmployeeID")) Then
                GetEmployeeInfo(Request.QueryString("EmployeeID").ToString.Trim)
            End If
        End If
    End Sub

    Public Sub GetEmployeeInfo(ByVal employeeId As String)
        Dim sb As StringBuilder = New StringBuilder("")
        Dim sql, sql2 As String
        Dim ds As New DataSet
        listData.InnerHtml = ""
        Try
            sql = "select EmployeeNo,FullName,JobTitleName [Designation] from hr.View_Employee where EmployeeID ='" & employeeId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim PhotoEmp As String = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString()
                If Not String.IsNullOrWhiteSpace(PhotoEmp) Then
                    If File.Exists(Server.MapPath("~/HR/Images/" & PhotoEmp & ".png")) Then
                        employeeImage.InnerHtml = "<img class='img-responsive' src='/HR/Images/" & PhotoEmp & ".png' style='width:110px; height:125px;'/>"
                    ElseIf File.Exists(Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpeg")) Then
                        employeeImage.InnerHtml = "<img class='img-responsive' src='/HR/Images/" & PhotoEmp & ".jpeg' style='width:110px; height:125px;'/>"
                    ElseIf File.Exists(Server.MapPath("~/HR/Images/" & PhotoEmp & ".jpg")) Then
                        employeeImage.InnerHtml = "<img class='img-responsive' src='/HR/Images/" & PhotoEmp & ".jpg' style='width:110px; height:125px;'/>"
                    ElseIf File.Exists(Server.MapPath("~/Student/Images/photos/avatar.png")) Then
                        employeeImage.InnerHtml = "<img class='img-responsive' src='/Student/Images/photos/avatar.png' style='width:110px; height:125px;'/>"
                    End If
                End If

                Name.InnerHtml = ds.Tables(0).Rows(0).Item("FullName").ToString().Trim
                Designation.InnerHtml = ds.Tables(0).Rows(0).Item("Designation").ToString().Trim
                EmployeeNo.InnerHtml = ds.Tables(0).Rows(0).Item("EmployeeNo").ToString().Trim
            End If

            sql2 = "exec [Attendance].[usp_LeaveProfile] @EmployeeID='" & employeeId & "',@BranchID='" & Session("BranchID") & "'"
            ds.Reset()
            ds = Dao.ExecuteDataset(sql2)
            If ds.Tables.Count > 0 Then
                For t As Integer = 0 To ds.Tables.Count - 1
                    If (ds.Tables(t).Rows.Count > 0) Then
                        'sb.AppendLine("<div class='row'>")
                        'sb.AppendLine("<div class='col-md-12'>")
                        sb.AppendLine("<div class='portlet box green'>")
                        sb.AppendLine("<div class='portlet-title'>")
                        sb.AppendLine("<div class='caption'>")
                        sb.AppendLine(" <i class='fa fa-cog'></i>Table " & t + 1 & " : " & ds.Tables(t).Rows(0).Item("TableName").ToString & "</div>")
                        sb.AppendLine("<div class='tools'>")
                        sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class=''></a><a href='javascript:;' class=''></a><a href='javascript:;' class='remove'></a>")
                        sb.AppendLine("</div>")
                        sb.AppendLine("</div>")
                        sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
                        sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed' cellspacing='0' width='100%'>")
                        sb.AppendLine("<thead class='flip-content'>")
                        sb.AppendLine("<tr>")
                        sb.AppendLine("<th>SN</th>")
                        For c As Integer = 0 To ds.Tables(t).Columns.Count - 2
                            sb.AppendLine("<th class='text-center'>" & ds.Tables(t).Columns(c).ColumnName.ToString & "</th>")
                        Next
                        sb.AppendLine("</tr>")
                        sb.AppendLine("</thead>")
                        sb.AppendLine("<tbody>")

                        For i As Integer = 0 To ds.Tables(t).Rows.Count - 1
                            sb.AppendLine("<tr>")
                            sb.Append("<td class='text-center'>" & i + 1 & "</td>")
                            For j As Integer = 0 To ds.Tables(t).Columns.Count - 2
                                sb.AppendLine("<td class='text-center'>" & ds.Tables(t).Rows(i).Item(j).ToString().Trim & "</td>")
                            Next
                            sb.AppendLine("</tr>")
                        Next
                        sb.AppendLine("</tbody>")
                        sb.AppendLine("</table>")
                        sb.AppendLine("</div>")
                        sb.AppendLine("</div>")
                        'sb.AppendLine("</div>")

                    End If
                Next
                listData.InnerHtml = sb.ToString()
            Else
                sb.Clear()
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnToPDF_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToPDF.Click
        pdfDoc.SingleHRLeaveProfile(Request.QueryString("EmployeeID").ToString.Trim, Session("BranchID").ToString.Trim)
    End Sub

End Class