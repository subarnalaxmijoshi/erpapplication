﻿Imports System.IO
Imports System.Web.HttpContext
Public Class CurrentLeaveSummary
    Inherits System.Web.UI.Page
    Private ReadOnly Dao As New DatabaseDao
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetBranch()
        End If
    End Sub

    Sub GetBranch()
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_BranchSetup] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlBranch.DataSource = ds.Tables(0)
            ddlBranch.DataTextField = "BranchName"
            ddlBranch.DataValueField = "BranchID"
            ddlBranch.DataBind()
            ddlBranch.Items.Insert(0, New ListItem("-- Select Branch --", ""))
        Else
            ddlBranch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        GetLeaveDetails(ddlBranch.SelectedValue)
    End Sub

    Protected Sub GetLeaveDetails(ByVal branchId As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Try
            sql = "EXEC [Attendance].[usp_EditLeave] @Flag='s',@BranchID='" & Session("BranchID") & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine(" <i class='fa fa-cog'></i>Overall Leave Summary</div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class=''></a><a href='javascript:;' class=''></a><a href='javascript:;' class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
                sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed' cellspacing='0' width='100%'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>SN</th>")
                For c As Integer = 1 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<th class='text-center'>" & ds.Tables(0).Columns(c).ColumnName.ToString & "</th>")
                Next
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                
                For Each row As DataRow In ds.Tables(0).Rows
                    sb.AppendLine("<tr>")
                    sb.Append("<td class='text-center'>" & i + 1 & "</td>")
                    For j As Integer = 1 To ds.Tables(0).Columns.Count - 1
                        If j > 1 Then
                            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item(j).ToString().Trim) Then

                                sb.AppendLine("<td class='text-center'>" & ds.Tables(0).Rows(i).Item(j).ToString() & "</td>")

                            Else
                                sb.AppendLine("<td class='text-center'>0</td>")
                            End If
                        Else
                            sb.AppendLine("<td>" & ds.Tables(0).Rows(i).Item(j).ToString() & "</td>")
                        End If
                    Next

                    sb.AppendLine("</tr>")
                    i += 1
                Next

                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                message.InnerHtml = ""
                listData.InnerHtml = sb.ToString()
            Else
                sb.Clear()
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.Clear()
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        GetDatewiseLeaveDetails(txtStartDate.Text, txtEndDate.Text)
    End Sub

    Protected Sub GetDatewiseLeaveDetails(ByVal startdate As String, ByVal enddate As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder("")
        Dim rptTitle As String = ""
        listDataDatewise.InnerHtml = ""
        Try
            sql = "EXEC [Attendance].[usp_EditLeave] @Flag='a',@BranchID='" & Session("BranchID") & "',@StartDate='" & startdate & "',@EndDate='" & enddate & "'"
            If Not String.IsNullOrWhiteSpace(ddlWorkingType.SelectedValue) Then
                sql += ",@WorkingType='" & ddlWorkingType.SelectedValue & "'"
            End If
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Columns.Remove("EmployeeID1")

                rptTitle = "Attendace record of "
                If ddlWorkingType.SelectedValue = "Teaching" Then
                    rptTitle = rptTitle & "all academic "
                ElseIf ddlWorkingType.SelectedValue = "Non-Teaching" Then
                    rptTitle = rptTitle & "all administrative "
                Else
                    rptTitle = rptTitle & "all academic and administrative "
                End If
                rptTitle = rptTitle & "employees of this campus from " & startdate & " (" & txtStartDate_Nep.Text & ") to " & enddate & " (" & txtEndDate_Nep.Text & ")"

                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine(" <i class='fa fa-reorder'></i>" & rptTitle & "</div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class=''></a><a href='javascript:;' class=''></a><a href='javascript:;' class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
                sb.AppendLine("<table id='displaylist' class='table table-bordered table-striped table-condensed' cellspacing='0' width='100%'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>SN</th>")
                For c As Integer = 1 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<th class='text-center'>" & ds.Tables(0).Columns(c).ColumnName.ToString & "</th>")
                Next
                sb.AppendLine("<th>Total Days</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                Dim rowTotal() As Decimal
                ReDim rowTotal(ds.Tables(0).Rows.Count - 1)
                For Each row As DataRow In ds.Tables(0).Rows
                    sb.AppendLine("<tr>")
                    sb.Append("<td class='text-center'>" & i + 1 & "</td>")
                    For j As Integer = 1 To ds.Tables(0).Columns.Count - 1
                        If j > 4 Then
                            If Not String.IsNullOrWhiteSpace(ds.Tables(0).Rows(i).Item(j).ToString().Trim) Then
                                'Dim arr As String() = ds.Tables(0).Rows(i).Item(j).ToString().Split("/")

                                sb.AppendLine("<td class='text-center'>" & ds.Tables(0).Rows(i).Item(j).ToString() & "</td>")
                                rowTotal(i) += Convert.ToDecimal(ds.Tables(0).Rows(i).Item(j).ToString())
                            Else
                                sb.AppendLine("<td class='text-center'>0</td>")
                            End If
                        Else
                            sb.AppendLine("<td>" & ds.Tables(0).Rows(i).Item(j).ToString() & "</td>")
                        End If
                    Next
                    sb.AppendLine("<td class='text-center bold'>" & rowTotal(i).ToString() & "</td>")
                    sb.AppendLine("</tr>")
                    i += 1
                Next

                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                message.InnerHtml = ""
                listDataDatewise.InnerHtml = sb.ToString()
            Else
                sb.Clear()
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        Catch ex As Exception
            sb.Clear()
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ex.Message.ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End Try
    End Sub

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Current_Leave_Details")
    End Sub

    Protected Sub btnToExcel2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel2.Click
        Excel(listDataDatewise.InnerHtml, "Attendance_Leave_Details")
    End Sub
End Class