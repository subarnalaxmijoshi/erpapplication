﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LeaveSummary.aspx.vb" Inherits="School.LeaveSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function print(divId) {
        var contents = $("#" + divId).html();
        var win = window.open("", "", "height=500,width=900");
        win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
        win.document.write("</head><body >");
        win.document.write(contents);
        win.document.write("</body></html>");
        win.print();
        win.close();
        return true;
    }
    $(document).ready(function () {
        $("#toExcel").click(function () {
            $("#<%= btnToExcel.ClientID %>").click();
        });
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Leave Summary <small>Display leave summary of current year.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print</a></li>
                            <li id="toExcel"><a href="javascript:;">Export To Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><a href="/AttendanceManagement/LeaveSummary.aspx">Leave Summary</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div id="listData" runat="server">
                </div>
            </div>
        </div>
    </div>
        <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" style="display:none;" />   
</asp:Content>
