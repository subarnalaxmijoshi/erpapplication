﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CurrentLeaveSummary.aspx.vb" Inherits="School.CurrentLeaveSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link rel="stylesheet" href="../../assets/Calendar/jquery.calendars.picker.css">
    <script src="../../assets/Calendar/jquery.plugin.js"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../../assets/Calendar/jquery.calendars.nepali.js"></script>

    <link href="../assets/datatableplugins/css/fixedHeader.dataTables.min.css" rel="stylesheet"
        type="text/css" />
     <link href="../assets/datatableplugins/css/fixedColumns.dataTables.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/datatableplugins/css/buttons.dataTables.min.css" rel="stylesheet"
    type="text/css" />
    <script src="../assets/datatableplugins/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
    <script src="../assets/datatableplugins/js/dataTables.fixedColumns.min.js" type="text/javascript"></script>
     <script src="../assets/datatableplugins/js/dataTables.buttons.min.js" type="text/javascript"></script>
       <script src="../assets/datatableplugins/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../assets/datatableplugins/js/buttons.flash.min.js" type="text/javascript"></script>
     <script src="../assets/datatableplugins/ajax/jszip.min.js" type="text/javascript"></script>
      <script src="../assets/datatableplugins/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../assets/datatableplugins/ajax/pdfmake.min.js" type="text/javascript"></script>
    <script src="../assets/datatableplugins/ajax/vfs_fonts.js" type="text/javascript"></script>
    <script src="../assets/datatableplugins/js/buttons.html5.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        var checkFromValidation = false;
        $(document).ready(function () {
            $("#<%=lblLoading.ClientID %>").hide();
            $('#form').validate();
            $("#<%=txtStartDate.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Start Date is required.', date: jQuery.format("Enter a date (DD MM YYYY) format.")} });
            $("#<%=txtEndDate.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'End Date is required.', date: jQuery.format("Enter a date (DD MM YYYY) format.")} });
           
            checkFromValidation = function () {
                $("#<%=lblLoading.ClientID %>").show();
                $("#<%=btnSubmit.ClientID %>").hide();
                var bool = true;
                if ($('#<%=txtStartDate.ClientID %>').valid() == false) bool = false;
                if ($('#<%=txtEndDate.ClientID %>').valid() == false) bool = false;
               
                if (!bool) {
                    $("#<%=lblLoading.ClientID %>").hide();
                    $("#<%=btnSubmit.ClientID %>").show();

                    $('#form').validate().focusInvalid();
                }
                return bool;
            };
        });
    </script>

<script type="text/javascript">
    function print(divId) {
        var contents = $("#" + divId).html();
        var win = window.open("", "", "height=500,width=900");
        win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
//        win.document.write("<style>table th, table td {border:0.8px solid #000;padding:0.1em;}</style>");
        win.document.write("<title>Current Year Leave Summary</title>")
        win.document.write("</head><body >");
        win.document.write(contents);
        win.document.write("</body></html>");
        win.print();
        win.close();
        return true;
    }
    $(document).ready(function () {
        $("#toExcel").click(function () {
            $("#<%= btnToExcel.ClientID %>").click();
        });

        $("#toExcel2").click(function () {
            $("#<%= btnToExcel2.ClientID %>").click();
        });
    });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            $(function () {
                var calendar = $.calendars.instance('nepali');
                $("#<%= txtStartDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                    ChangeDate(selectedDate, 'e', this);
                }
                });
                $("#<%= txtEndDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                    ChangeDate(selectedDate, 'e', this);
                }
                });

            });

            $('#<%= txtStartDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
            $('#<%= txtEndDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();

            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
                $(function () {
                    var calendar = $.calendars.instance('nepali');
                    $("#<%= txtStartDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });
                    $("#<%= txtEndDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });

                });

                $('#<%= txtStartDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();
                $('#<%= txtEndDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();

            });
        });
     </script>

    <script type="text/javascript">
        function formatDate(date, type) {
            console.log("date : " + date + " ,type : " + type);
            var month = '';
            var day = '';
            var year = '';
            var resultdate;
            if (type == "n") {
                var d = new Date(date);
                month = d.getMonth() + 1;
                day = d.getDate();
                year = d.getFullYear();
                resultdate = [day, month, year].join('/');
            }
            else {

                year = date[0]._year;
                month = date[0]._month;
                day = date[0]._day;
                resultdate = [year, month, day].join('/');
            }
            console.log("formatted : " + resultdate);
            return resultdate;

        }
        function ChangeDate(value, obj, element) {
            value = formatDate(value, obj);
            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';
            debugger;
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/DateConverterNew',
                data: dataToSend,
                datatype: 'json',
                contentType: 'application/json',
                success: function (data) {
                    debugger;
                    var dd, mm, yy;
                    var value = [];
                    if (obj == "e") {
                        var fieldId1 = element.id.replace(/_Nep$/i, '');
                        //                        $("#" + fieldId1).val(data);
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId1).val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                    }
                    else if (obj == "n") {
                        var fieldId2 = element.id + '_Nep'
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId2).val(yy + "/" + mm + "/" + dd);
                    }
                }
            });

        }
    </script>

      <script type="text/javascript">
          $(document).ready(function () {
              var table = $('#displaytable').DataTable({
                  paging: false,
                  fixedHeader: { header: true, headerOffset: 55 },
                  scrollY: "300px",
                  scrollX: true,
                  scrollCollapse: true,
                  sorting: false


              });

              var table2 = $('#displaylist').DataTable({
                  paging: false,
                  fixedHeader: { header: true, headerOffset: 55 },
                  scrollY: "300px",
                  scrollX: true,
                  scrollCollapse: true,
                  sorting: false,
                  //                  "columnDefs": [
                  //                        {
                  //                            "targets": [ 5 ],
                  //                            "visible": false,
                  //                            "searchable": false
                  //                        }

                  //                    ],
                  dom: 'Bfrtip',
                  buttons: [
                   {
                       extend: 'colvis',
                       postfixButtons: ['colvisRestore'],
                       collectionLayout: 'fixed four-column',
                       columns: ':not(.noVis)'
                   },
                    {
                        extend: 'print',
                        title: 'Current Leave Taken Report',
                        messageTop: 'Attendace record of ' + $('#<%= ddlWorkingType.ClientID%> option:selected').text() + ' employees of this campus from ' + $('#<%= txtStartDate_Nep.ClientID%>').val() + ' to ' + $('#<%= txtEndDate_Nep.ClientID%>').val() + ' .',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        title: 'Current Leave Taken Report',
                        messageTop: 'Attendace record of ' + $('#<%= ddlWorkingType.ClientID%> option:selected').text() + ' employees of this campus from ' + $('#<%= txtStartDate_Nep.ClientID%>').val() + ' to ' + $('#<%= txtEndDate_Nep.ClientID%>').val() + ' .',
                        messageBottom: 'Copyright Mitra ERP',
                        exportOptions: {
                            columns: ':visible'
                        }
                    }
                   ]

                  //                ,fixedColumns: {
                  //                    leftColumns: 2            
                  //                }

              });
          });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                 Current Year Leave Taken  <small>Get list leave taken by employee in current year.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a onclick="return print('<%=listData.ClientID %>')" href="#">Print Overall Leave</a></li>
                            <li id="toExcel"><a href="javascript:;">Overall Leave In Excel</a></li>
                             <li class="divider"></li>                                                   
                             <li id="toExcel2"><a href="javascript:;">Export To Excel Date Rangewise Leave</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    
                    <li><a href="/AttendanceManagement/CurrentLeaveSummary.aspx">Current Leave Taken</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#create" aria-controls="create" role="tab"
                            data-toggle="tab">Range Filter</a></li>
                        <li role="presentation"><a href="#display" aria-controls="display" role="tab" data-toggle="tab">
                            Overall Leave Summary</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="create">
                            <div class="portlet box purple ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>Filter
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a></a><a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">
                                                    Start Date :</label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <asp:TextBox ID="txtStartDate" runat="server" class="form-control" placeholder="Enter Start Date"
                                                            onchange="ChangeDate(this.value,'n',this);" />
                                                    </div>
                                                    <label for="<%=txtStartDate.ClientID%>" class="error" style="display: none;">
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <asp:TextBox ID="txtStartDate_Nep" runat="server" CssClass="form-control" placeholder="Enter Start Miti"></asp:TextBox>
                                                    </div>
                                                    <label for="<%=txtStartDate_Nep.ClientID%>" class="error" style="display: none;">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">
                                                    End Date :</label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <asp:TextBox ID="txtEndDate" runat="server" class="form-control" placeholder="Enter End Date"
                                                            onchange="ChangeDate(this.value,'n',this);" />
                                                    </div>
                                                    <label for="<%=txtEndDate.ClientID%>" class="error" style="display: none;">
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <asp:TextBox ID="txtEndDate_Nep" runat="server" CssClass="form-control" placeholder="Enter End Miti"></asp:TextBox>
                                                    </div>
                                                    <label for="<%=txtEndDate_Nep.ClientID%>" class="error" style="display: none;">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            Working Type :</label>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                                                <asp:DropDownList ID="ddlWorkingType" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="" Selected="False">--All Working Type--</asp:ListItem>
                                                                    <asp:ListItem Value="Teaching">Teaching</asp:ListItem>
                                                                    <asp:ListItem Value="Non-Teaching">Non Teaching</asp:ListItem>   
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label for="<%= ddlWorkingType.ClientID%>" class="error" style="display: none">
                                                            </label>
                                                        </div>
                                                    </div>
                                            <div class="form-actions fluid">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkFromValidation();"
                                                        runat="server" Text="Search" />
                                                    <asp:Label ID="lblLoading" runat="server" Text="Loading ...." class="btn purple"
                                                        disabled></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="listDataDatewise" runat="server">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="display">
                            <div class="portlet box purple ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i>Filter Leave
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a><a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">
                                                    Branch/Depart :</label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                                        <asp:DropDownList ID="ddlBranch" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <label for="<%=ddlBranch.ClientID%>" class="error" style="display: none;">
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn purple" UseSubmitBehavior="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="listData" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel Overall" style="display:none;" />
    <asp:Button ID="btnToExcel2" runat="server" Text="Export To Excel Datewise" style="display:none;"/>
</asp:Content>
