﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="CincoutDetails.aspx.vb" Inherits="School.CincoutDetails"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Check-in/Check-Out Reports </title>
    <link rel="stylesheet" href="../../assets/Calendar/jquery.calendars.picker.css">

    <script src="../../assets/Calendar/jquery.plugin.js"></script>
    <!--<script src="jquery.calendars.all.js"></script><!-- Use instead of calendars, plus, and picker below -->
    <script src="../../assets/Calendar/jquery.calendars.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.plus.js"></script>
    <script src="../../assets/Calendar/jquery.calendars.picker.js"></script>
    <!--<script src="jquery.calendars.picker.ext.js"></script> <!-- Include for ThemeRoller styling -->
    <script src="../../assets/Calendar/jquery.calendars.nepali.js"></script>

    <!--For multi-select list-->
   
    <link href="../assets/bootstrap-3.3.2/css/bootstrap-3.3.2.min.css" rel="stylesheet" type="text/css" />
    <%-- <script src="../assets/jquery-2.1.3/js/jquery-2.1.3.min.js" type="text/javascript"></script>--%>
   <%-- <script src="../assets/bootstrap-3.3.2/js/bootstrap-3.3.2.min.js" type="text/javascript"></script>--%>

  
    <link href="../assets/plugins/bootstrap-multi-select/css/bootstrap-multiselect.css"
        rel="stylesheet" type="text/css" />
    <script src="../assets/jquery-2.1.3/js/require-2.3.5.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-multi-select/js/bootstrap-multiselect.js"
        type="text/javascript"></script>

     <style type="text/css">
            
            .fixed_header{
                width: 400px;
                table-layout: fixed;
                border-collapse: collapse;
            }

            .fixed_header tbody{
              display:block;
              width: 100%;
              overflow: auto;
              height: 450px;
            }

            .fixed_header thead tr {
               display: block;
            }

            .fixed_header thead {
              background: #35aa47;
              color:#fff;
            }

            .fixed_header th, .fixed_header td {
              padding: 0px;
              width: 98px;
            }

        </style>

   <script type="text/javascript">
        var checkFromValidation = false;
        $(document).ready(function () {
            $("#<%=lblLoading.ClientID %>").hide();
            $('#form').validate();
            $("#<%=txtStartDate.ClientID %>").rules('add', { required: true, date: true, messages: { required: 'Date is required.', date: jQuery.format("Enter a date (DD MM YYYY) format.")} });

            checkFromValidation = function () {
                $("#<%=lblLoading.ClientID %>").show();
                $("#<%=btnSubmit.ClientID %>").hide();
                var bool = true;
                if ($('#<%=txtStartDate.ClientID %>').valid() == false) bool = false;

                if (!bool) {
                    $("#<%=lblLoading.ClientID %>").hide();
                    $("#<%=btnSubmit.ClientID %>").show();

                    $('#form').validate().focusInvalid();
                }
                return bool;
            };
        });
    </script>
   <script type="text/javascript">
       $(document).ready(function () {

           $(function () {
               var calendar = $.calendars.instance('nepali');
               $("#<%= txtStartDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd", onSelect: function (selectedDate) {
                   ChangeDate(selectedDate, 'e', this);
               }
               });
           });

           $('#<%= txtStartDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();


           Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (evt, args) {
               $(function () {
                   var calendar = $.calendars.instance('nepali');
                   $("#<%= txtStartDate_Nep.ClientID %>").calendarsPicker({ calendar: calendar, dateFormat: "yyyy/mm/dd" });
               });

               $('#<%= txtStartDate.ClientID %>').datepicker({ dateFormat: "dd M yy", changeMonth: true, changeYear: true }).val();


           });
       });
     </script>

    <script type="text/javascript">
        function formatDate(date, type) {
            console.log("date : " + date + " ,type : " + type);
            var month = '';
            var day = '';
            var year = '';
            var resultdate;
            if (type == "n") {
                var d = new Date(date);
                month = d.getMonth() + 1;
                day = d.getDate();
                year = d.getFullYear();
                resultdate = [day, month, year].join('/');
            }
            else {

                year = date[0]._year;
                month = date[0]._month;
                day = date[0]._day;
                resultdate = [year, month, day].join('/');
            }
            console.log("formatted : " + resultdate);
            return resultdate;

        }



        function ChangeDate(value, obj, element) {
            value = formatDate(value, obj);
            var dataToSend = '{"Convertdate":"' + value + '","type": "' + obj + '"}';
            debugger;
            $.ajax({
                type: 'POST',
                url: '/SetupServices.svc/DateConverterNew',
                data: dataToSend,
                datatype: 'json',
                contentType: 'application/json',
                success: function (data) {
                    debugger;
                    var dd, mm, yy;
                    var value = [];
                    if (obj == "e") {
                        var fieldId1 = element.id.replace(/_Nep$/i, '');
                        //                        $("#" + fieldId1).val(data);
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId1).val(new Date(yy, mm - 1, dd).format("dd MMM yyyy"));
                    }
                    else if (obj == "n") {
                        var fieldId2 = element.id + '_Nep'
                        value = data.split("/");
                        dd = value[0];
                        mm = value[1];
                        yy = value[2];
                        $("#" + fieldId2).val(yy + "/" + mm + "/" + dd);
                    }
                }
            });

        }
    </script>

    <script type="text/javascript">
        function print(divId) {
            var contents = $("#" + divId).html();
            var win = window.open("", "", "height=500,width=900");
            win.document.write("<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
            win.document.write("</head><body >");
            win.document.write(contents);
            win.document.write("</body></html>");
            win.print();
            win.close();
            return true;
        }
        $(document).ready(function () {
            $("#toExcel").click(function () {
                $("#<%= btnToExcel.ClientID %>").click();
            });
        });
    </script>

    <script type="text/javascript">
////        $(document).ready(function () {

////            $('[id*=listboxDesignation]').multiselect({
////                maxHeight: 200,
////                buttonWidth: '200px',
////                enableCaseInsensitiveFiltering: true,
////                enableFiltering: true,
////                includeSelectAllOption: true,
////                nonSelectedText: 'Select Designation'
//////                onDropdownHidden: function (event) {
//////                    debugger;
//////                    var selectedJobTitleIds = [];
//////                    $('[id*=listboxDesignation] option:selected').each(function () {
//////                        selectedJobTitleIds.push($(this).val());
//////                    });
//////                    var selectedDesgnIds = selectedJobTitleIds.join(',');
//////                    LoadEmpByJobTitle(selectedDesgnIds);
//////                }

////            });

////            $('#btn_ResetAll').on('click', function () {
////                //                $('[id*=listboxDesignation]').multiselect('selectAll', false);
////                //                $('[id*=listboxDesignation]').multiselect('updateButtonText');
//////                var selectedJobTitleIds = [];
////                $('[id*=listboxDesignation] option:selected').each(function () {
////                    debugger;
////                    $(this).prop('selected', false);
////                })
//////                var selectedDesgnIds = selectedJobTitleIds.join(',');
//////                LoadEmpByJobTitle(selectedDesgnIds);
////                $('[id*=listboxDesignation]').multiselect('refresh');
////            });

////            $('[id*=lstEmployees]').multiselect({
////                enableCaseInsensitiveFiltering: true,
////                enableFiltering: true,
////                maxHeight: '300',
////                buttonWidth: '290px',
////                numberDisplayed: 2,
////                nonSelectedText: 'Select Employees',
////                includeSelectAllOption: true
////            });

////            $('#btn_ResetEmp').on('click', function () {
////                debugger;
////                $('[id*=lstEmployees]  option:selected').each(function () {
////                    $(this).prop('selected', false);
////                })
////                $('[id*=lstEmployees]').multiselect('refresh');
////            });
////        });
////        

////        function LoadEmpByJobTitle(selectedDesgnIds) {
////            var branchId = $("[id*=ddlBranch]  option:selected").val();
////            $.ajax({
////                type: "POST",
////                url: "/SetupServices.svc/GetEmployeeByJobTitle",
////                data: '{"jobTitleIds" : "' + selectedDesgnIds + '","branchId" : "' + branchId + '"}',
////                dataType: "json",
////                contentType: "application/json",
////                success: function (data) {
////                    $('[id*=lstEmployees]').empty();
////                    $('[id*=lstEmployees]').multiselect('destroy');
////                    $.each(data, function (i, item) {
//////                        $('[id*=lstEmployees]').append($("<option'></option>").val(item.ApplicationID).html(item.EmployeeName));
////                        $("[id*=lstEmployees]").append('<option value="' + item.ApplicationID + '">' + item.EmployeeName + '</option>');
////                    });
////                    $('[id*=lstEmployees]').multiselect({
////                        enableCaseInsensitiveFiltering: true,
////                        enableFiltering: true,
////                        maxHeight: '300',
////                        buttonWidth: '290px',
////                        numberDisplayed: 2,
////                        nonSelectedText: 'Select Employees',
////                        includeSelectAllOption: true
////                    });

////                },
////                error: function (data) {
////                    alert("Sorry we could not load . Please make sure you have provided at least one item.");
////                }
////            });
////        }
////  

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        </button>
                        <h4 class="modal-title">
                            Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">
                            Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="theme-panel hidden-xs hidden-sm hidden">
            <div class="toggler">
            </div>
            <div class="toggler-close">
            </div>
            <div class="theme-options">
                <div class="theme-option theme-colors clearfix">
                    <span>THEME COLOR</span>
                    <ul>
                        <li class="color-black current color-default" data-style="default"></li>
                        <li class="color-blue" data-style="blue"></li>
                        <li class="color-brown" data-style="brown"></li>
                        <li class="color-purple" data-style="purple"></li>
                        <li class="color-grey" data-style="grey"></li>
                        <li class="color-white color-light" data-style="light"></li>
                    </ul>
                </div>
                <div class="theme-option">
                    <span>Layout</span>
                    <select class="layout-option form-control input-small">
                        <option value="fluid" selected="selected">Fluid</option>
                        <option value="boxed">Boxed</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Header</span>
                    <select class="header-option form-control input-small">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="default">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Sidebar</span>
                    <select class="sidebar-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
                <div class="theme-option">
                    <span>Footer</span>
                    <select class="footer-option form-control input-small">
                        <option value="fixed">Fixed</option>
                        <option value="default" selected="selected">Default</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title">
                    Single Day Attendance Details  <small>Details of Check-in/Check-out of employees in a single day.</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                     <li id="toExcel"><a href="javascript:;">Generate Excel</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                  
                    <li><a href="/AttendanceManagement/CincoutDetails.aspx">Attendance Details</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div id="message" runat="server">
                </div>
            </div>
        </div>
        <div class="portlet box purple ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-reorder"></i>Filter
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a><a href="#portlet-config" data-toggle="modal" class="config">
                    </a><a href="" class="reload"></a><a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                       
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Branch :</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span>
                                    <asp:DropDownList ID="ddlBranch" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <label class="col-md-2 control-label">
                                Department :</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Working Status :</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <asp:DropDownList ID="ddlWorkingStatus" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <label class="col-md-2 control-label">
                                Working Time :</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                    <asp:DropDownList ID="ddlWorkingTime" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">-- All --</asp:ListItem>
                                        <asp:ListItem Value="1">Full Time</asp:ListItem>
                                        <asp:ListItem Value="2">Part Time</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Shift :</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    <asp:DropDownList ID="ddlShift" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                            </label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                    <asp:CheckBox ID="chk_allJobTitle" runat="server" AutoPostBack="True" Text="Select All/Deselect All" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Designation :</label>
                            <div class="col-md-10">
                                <div class="portlet box" style="overflow-y: auto; max-height: 200px;">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                        <asp:CheckBoxList ID="chkDesignation" runat="server" CssClass="form-control" RepeatColumns="4"
                                            CellPadding="3" CellSpacing="2" RepeatLayout="table" RepeatDirection="Horizontal"
                                            Font-Size="Smaller" Height="200" AutoPostBack="true">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                            <%-- <input id="btn_ResetAll" type="button" value="Reset"  class="col-md-1 btn btn-primary"/>--%>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                            </label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                    <asp:CheckBox ID="ck_select" runat="server" AutoPostBack="True" Text="Select All/Deselect All" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Employees :</label>
                            <div class="col-md-10">
                                <%--<div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                    <asp:ListBox ID="lstEmployees" runat="server"  multiple="multiple" CssClass="form-control">
                                    </asp:ListBox>
                                </div>--%>
                                <div class="portlet box" style="overflow-y: auto; max-height: 200px;">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                        <asp:CheckBoxList ID="chkEmployeeList" runat="server" CssClass="form-control" placeholder="Enter text"
                                            RepeatColumns="3" CellPadding="3" CellSpacing="2" RepeatLayout="table" RepeatDirection="Horizontal"
                                            Font-Size="Smaller" Height="200">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                                <label for="<%=chkEmployeeList.ClientID%>" class="error" style="display: none;">
                                </label>
                            </div>
                            <%-- <input id="btn_ResetEmp" type="button" value="Reset All"  class="col-md-1 btn btn-primary"/>--%>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Select Date :</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" placeholder="Enter a Date"
                                        onchange="ChangeDate(this.value,'n',this);" />
                                </div>
                                <label for="<%=txtStartDate.ClientID%>" class="error" style="display: none;">
                                </label>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <asp:TextBox ID="txtStartDate_Nep" runat="server" CssClass="form-control" placeholder="Enter a Miti"></asp:TextBox>
                                </div>
                                <label for="<%=txtStartDate_Nep.ClientID%>" class="error" style="display: none;">
                                </label>
                            </div>
                        </div>
                         </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-9">
                                <asp:Button ID="btnSubmit" class="btn purple" OnClientClick="return checkFromValidation();"
                                    runat="server" Text="Submit" />
                                <asp:Label ID="lblLoading" runat="server" Text="Loading ...." class="btn purple"
                                    disabled></asp:Label>
                                <asp:Button ID="btnCancel" type="button" class="btn default" runat="server" Text="Cancel" />
                                <asp:Button ID="btnToExcel" runat="server" Text="Export To Excel" Style="display: none;" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="listData" runat="server">
        </div>
    </div>
</asp:Content>
