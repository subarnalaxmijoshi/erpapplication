﻿Imports System.IO
Imports System.Web.HttpContext
Public Class LeaveAssignment
    Inherits System.Web.UI.Page


    Private ReadOnly Dao As New DatabaseDao

    Dim DC As New DateConverter()

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim currentUri = Request.Url.AbsolutePath
        Dim pa As New PageAuthority
        Dim uid As String
        If (Session("userId") <> Nothing) Then
            uid = Session("userID").ToString()
        Else
            Response.Redirect("/Logout.aspx")
        End If
        '    Dim hasRightToView = PageAuthority.IsAuthorized(uid, currentUri)
        '    If Not hasRightToView Then
        '        Dim redirectUrl = Request.Cookies("Redirect").Value.ToString()
        '        Response.Redirect(redirectUrl)
        '        UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "0")
        '    Else
        '        UserFormActivity.SaveLog(Request.Url.AbsoluteUri, "Type = " & Request.Browser.Type & ", Name = " & Request.Browser.Browser & ", Version = " & Request.Browser.Version & ", Major Version = " & Request.Browser.MajorVersion & ", Minor Version = " & Request.Browser.MinorVersion, uid, System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList(0).ToString(), System.Net.Dns.GetHostName(), Request.UserHostAddress, "1")
        '    End If


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            divEmpChkList.Visible = True
            divEmpList.Visible = False
            GetBranch()
            GetEmployeeByBranch(ddlBranch.SelectedValue)
            GetApprovalBy(ddlBranch.SelectedValue)
            GetLeave()
            GetLeaveType()

            GetLeaveAssignment()
            If Not String.IsNullOrWhiteSpace(Request.QueryString("LeaveAsgnId")) Then
                GetLeaveById(Request.QueryString("LeaveAsgnId").ToString.Trim)
            ElseIf Not String.IsNullOrWhiteSpace(Request.QueryString("DeleteLeaveAsgnId")) Then
                DeleteLeave(Request.QueryString("DeleteLeaveAsgnId").ToString.Trim)
            End If
        End If
    End Sub

    Sub GetBranch()
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_BranchSetup] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlBranch.DataSource = ds.Tables(0)
            ddlBranch.DataTextField = "BranchName"
            ddlBranch.DataValueField = "BranchID"
            ddlBranch.DataBind()
            ddlBranch.Items.Insert(0, New ListItem("-- Select Branch --", ""))
        Else
            ddlBranch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
    End Sub

    Private Sub GetApprovalBy(ByVal branchId As String)
        Dim ds As New DataSet
        Dim sql As String
        sql = "exec [HR].[usp_EmployeeInfo] @Flag='s',@BranchID='" & branchId & "'"
        ddlApprovedBy.Items.Clear()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlApprovedBy.DataSource = ds.Tables(0)
                ddlApprovedBy.DataValueField = "EmployeeId"
                ddlApprovedBy.DataTextField = "EmployeeName"
                ddlApprovedBy.DataBind()
                ddlApprovedBy.Items.Insert(0, New ListItem("-- Select -- ", ""))
            Else
                ddlApprovedBy.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        End If
    End Sub

    Private Sub GetLeaveType()
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC Attendance.usp_LeaveType @Flag='s'"
        ds = Dao.ExecuteDataset(sql)
        ddlLeaveType.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlLeaveType.DataSource = ds.Tables(0)
            ddlLeaveType.DataValueField = "LeaveTypeID"
            ddlLeaveType.DataTextField = "LeaveType"
            ddlLeaveType.DataBind()
        End If
    End Sub


    Protected Sub ddlBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBranch.SelectedIndexChanged
        GetEmployeeByBranch(ddlBranch.SelectedValue)
        GetApprovalBy(ddlBranch.SelectedValue)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        If ddlBranch.SelectedIndex > 0 Then
            If btnSubmit.Text.ToLower = "update" Then
                EditLeave(Request.QueryString("LeaveAsgnId"))
            Else
                AssignLeave()
            End If
        End If

        'If btnSubmit.Text.ToLower = "update" Then
        '    leaveAssignementId = Request.QueryString("LeaveAsgnId")
        '    UpdateLeave(leaveAssignementId, ddlEmployeeName.SelectedValue, ddlLeaveName.SelectedValue, txtLeaveStartDate.Text, txtLeaveEndDate.Text, chkHasHalfDay.Checked, ddlHalfType.SelectedValue, txtReason.Text, "", DateTime.Now.ToString())
        'Else
        '    SaveLeave(ddlEmployeeName.SelectedValue, ddlLeaveName.SelectedValue, txtLeaveStartDate.Text, txtLeaveEndDate.Text, chkHasHalfDay.Checked, ddlHalfType.SelectedValue, txtReason.Text, "")
        'End If
    End Sub


    Private Sub GetEmployeeByBranch(ByVal branchId As String)
        Dim sql As String
        sql = "exec [HR].[usp_EmployeeInfo] @Flag='s',@BranchID='" & branchId & "'"
        Dim ds As New DataSet
        chkEmployeeList.Items.Clear()
        ddlEmployeeName.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                chkEmployeeList.DataSource = ds.Tables(0)
                chkEmployeeList.DataValueField = "EmployeeId"
                chkEmployeeList.DataTextField = "EmployeeName"
                chkEmployeeList.DataBind()

                ddlEmployeeName.DataSource = ds.Tables(0)
                ddlEmployeeName.DataValueField = "EmployeeId"
                ddlEmployeeName.DataTextField = "EmployeeName"
                ddlEmployeeName.DataBind()
            End If
        Finally

        End Try
    End Sub

    Protected Sub ck_select_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ck_select.CheckedChanged
        If ck_select.Checked = True Then
            For Each listItem As ListItem In chkEmployeeList.Items
                listItem.Selected = True
            Next


            ck_select.Text = "Deselect All"
        Else
            For Each listItem As ListItem In chkEmployeeList.Items
                listItem.Selected = False
            Next
            ck_select.Text = "Select All"
        End If

    End Sub

    Private Sub GetLeave()
        Dim ds As New DataSet
        Dim sql As String
        sql = "EXEC Attendance.usp_Leave @Flag='s'"
        ddlLeaveName.Items.Clear()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlLeaveName.DataSource = ds.Tables(0)
                ddlLeaveName.DataValueField = "LeaveID"
                ddlLeaveName.DataTextField = "LeaveName"
                ddlLeaveName.DataBind()
                ddlLeaveName.Items.Insert(0, New ListItem("-- Select Leave Name -- ", ""))
            Else
                ddlLeaveName.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        End If
    End Sub

    Private Sub AssignLeave()
        Dim mes As String = ""
        Dim sb As StringBuilder = New StringBuilder("")
        message.InnerHtml = ""
        If Not chkEmployeeList.Items.Count > 0 Then
            mes = "No Employee selected."
        End If
        If mes <> "" Then
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(mes)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            Return
        End If
        Try
            Dim count As Integer = 0
            Dim count2 As Integer = 0
            Dim EmployeeIDs As String = ""
            count = 0
            count2 = 0
            For Each listItem As ListItem In chkEmployeeList.Items
                count = count + 1
                If listItem.Selected = True Then
                    count2 = count2 + 1
                    If count2 = 1 Then
                        EmployeeIDs = chkEmployeeList.Items(count - 1).Value
                    Else
                        EmployeeIDs = EmployeeIDs & "," & chkEmployeeList.Items(count - 1).Value
                    End If
                Else

                End If
            Next
            If String.IsNullOrWhiteSpace(EmployeeIDs) Then
                mes = "No Employee selected."
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(mes)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
                Return
            End If

            Dim ds As New DataSet
            Dim sql As String
            Dim msgClass As String

            sql = "EXEC [Attendance].[usp_AssignLeave] @Flag='i', @EmployeeIDs='" & EmployeeIDs & "', @LeaveID='" & ddlLeaveName.SelectedValue & "', @DateFrom='" & txtLeaveStartDate.Text & "', @DateTo='" & txtLeaveEndDate.Text & "', @HasHalfDay='" & chkHasHalfDay.Checked & "', @FirstOrSecondHalf=N'" & ddlHalfType.SelectedValue & "', @Reason=N'" & txtReason.Text.Trim & "'"
            sql += ",@ApprovedBy='" & ddlApprovedBy.SelectedValue & "',@ApprovedDate='" & txtApprovedDate.Text & "',@CreatedBy='" & Session("userID") & "'"
            sql += ",@BranchID='" & ddlBranch.SelectedValue & "',@LeaveTypeID='" & ddlLeaveType.SelectedValue & "'"

            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows(0).Item("errorCode") = "0" Then
                msgClass = "note note-success"
                ClearField()
                GetLeaveAssignment()
            ElseIf ds.Tables(0).Rows(0).Item("errorCode") = "1" Then
                msgClass = "note note-danger"
            End If
            message.InnerHtml = ""
            sb.AppendLine("<div class='" & msgClass & "'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString

        Catch ex As Exception

        End Try
    End Sub

    Private Sub EditLeave(ByVal leaveassignId As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim sql As String
        Dim msgClass As String

        sql = "EXEC [Attendance].[usp_LeaveAssignment] @Flag='u', @EmployeeID='" & ddlEmployeeName.SelectedValue & "', @LeaveID='" & ddlLeaveName.SelectedValue & "', @DateFrom='" & txtLeaveStartDate.Text & "', @DateTo='" & txtLeaveEndDate.Text & "', @HasHalfDay='" & chkHasHalfDay.Checked & "', @FirstOrSecondHalf='" & ddlHalfType.SelectedValue & "', @Reason='" & txtReason.Text & "', @LeaveAssignmentID='" & leaveassignId & "'"
        sql += ",@LeaveTypeID='" & ddlLeaveType.SelectedValue & "',@ModifiedBy='" & Session("userID") & "'"

        ds = Dao.ExecuteDataset(sql)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("errorCode") = "0" Then
                    msgClass = "note note-success"
                    btnSubmit.Text = "Submit"
                    ClearField()
                    divEmpChkList.Visible = True
                    divEmpList.Visible = False
                    GetLeaveAssignment()
                ElseIf ds.Tables(0).Rows(0).Item("errorCode") = "1" Then
                    msgClass = "note note-danger"
                End If
                message.InnerHtml = ""
                sb.AppendLine("<div class='" & msgClass & "'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        End If
    End Sub

    Private Sub ClearField()
        'ddlEmployeeName.SelectedIndex = 0
        ddlBranch.SelectedIndex = 0
        chkEmployeeList.Items.Clear()
        ddlLeaveName.SelectedIndex = 0
        ddlLeaveType.SelectedIndex = 0
        ddlApprovedBy.SelectedIndex = 0
        txtApprovedDate.Text = String.Empty
        txtLeaveStartDate.Text = String.Empty
        txtLeaveEndDate.Text = String.Empty
        txtLeaveStartDate_Nep.Text = String.Empty
        txtLeaveEndDate_Nep.Text = String.Empty
        chkHasHalfDay.Checked = False
        ddlHalfType.SelectedIndex = 0
        'txtRemainingLeave.Text = String.Empty
        txtReason.Text = String.Empty
    End Sub


    'Private Sub SaveLeave(ByVal employeeId As String, ByVal leaveId As String, ByVal dateFrom As String, ByVal dateTo As String,
    '                      ByVal isHalfDay As Boolean, ByVal firstOrSecond As String, ByVal reason As String, ByVal createdBy As String)
    '    Dim ds As New DataSet
    '    Dim sb As New StringBuilder
    '    Dim msgClass As String
    '    sql = "EXEC [Attendance].[usp_LeaveAssignment] @Flag='i', @EmployeeID='" & employeeId & "', @LeaveID='" & leaveId & "', @DateFrom='" & dateFrom & "', @DateTo='" & dateTo & "', @HasHalfDay='" & isHalfDay & "', @FirstOrSecondHalf='" & firstOrSecond & "', @Reason='" & reason & "'"
    '    sql += ",@RemainingLeave='" & txtRemainingLeave.Text.Trim & "',@CreatedBy='" & Session("userID") & "'"

    '    ds = Dao.ExecuteDataset(sql)
    '    If ds.Tables.Count > 0 Then
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            If ds.Tables(0).Rows(0).Item("errorCode") = "1" Then
    '                msgClass = "note note-success"
    '                ClearField()
    '                GetLeaveAssignment()
    '            ElseIf ds.Tables(0).Rows(0).Item("errorCode") = "0" Then
    '                msgClass = "note note-danger"
    '            End If
    '            message.InnerHtml = ""
    '            sb.AppendLine("<div class='" & msgClass & "'>")
    '            sb.AppendLine("<div class='close-note'>x</div>")
    '            sb.AppendLine("<p>")
    '            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
    '            sb.AppendLine("</p>")
    '            sb.AppendLine("</div>")
    '            message.InnerHtml = sb.ToString
    '        End If
    '    End If
    'End Sub

    'Private Sub UpdateLeave(ByVal leaveassignmentId As String, ByVal employeeId As String, ByVal leaveId As String, ByVal dateFrom As String, ByVal dateTo As String,
    '                      ByVal isHalfDay As Boolean, ByVal firstOrSecond As String, ByVal reason As String, ByVal modifiedBy As String, ByVal modifiedDate As String)
    '    Dim ds As New DataSet
    '    Dim sb As New StringBuilder
    '    Dim msgClass As String
    '    sql = "EXEC [Attendance].[usp_LeaveAssignment] @Flag='u', @EmployeeID='" & employeeId & "', @LeaveID='" & leaveId & "', @DateFrom='" & dateFrom & "', @DateTo='" & dateTo & "', @HasHalfDay='" & isHalfDay & "', @FirstOrSecondHalf='" & firstOrSecond & "', @Reason='" & reason & "', @ModifiedBy='" & modifiedBy & "', @LeaveAssignmentID='" & leaveassignmentId & "'"
    '    sql += ",@RemainingLeave='" & txtRemainingLeave.Text.Trim & "',@ModifiedBy='" & Session("userID") & "'"

    '    ds = Dao.ExecuteDataset(sql)

    '    If ds.Tables.Count > 0 Then
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            If ds.Tables(0).Rows(0).Item("errorCode") = "1" Then
    '                msgClass = "note note-success"
    '                btnSubmit.Text = "Submit"
    '                ClearField()
    '                GetLeaveAssignment()
    '            ElseIf ds.Tables(0).Rows(0).Item("errorCode") = "0" Then
    '                msgClass = "note note-danger"
    '            End If
    '            message.InnerHtml = ""
    '            sb.AppendLine("<div class='" & msgClass & "'>")
    '            sb.AppendLine("<div class='close-note'>x</div>")
    '            sb.AppendLine("<p>")
    '            sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
    '            sb.AppendLine("</p>")
    '            sb.AppendLine("</div>")
    '            message.InnerHtml = sb.ToString
    '        End If
    '    End If

    'End Sub


    Private Sub DeleteLeave(ByVal leaveId As String)
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim sql As String
        Dim msgClass As String
        sql = "EXEC [Attendance].[usp_LeaveAssignment] @Flag='d', @LeaveAssignmentID='" & leaveId & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("errorCode") = "0" Then
                    msgClass = "note note-success"
                    ClearField()
                    GetLeaveAssignment()
                ElseIf ds.Tables(0).Rows(0).Item("errorCode") = "1" Then
                    msgClass = "note note-danger"
                End If
                message.InnerHtml = ""
                sb.AppendLine("<div class='" & msgClass & "'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(ds.Tables(0).Rows(0).Item("mes").ToString)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
            End If
        End If
    End Sub

    Private Sub GetLeaveById(ByVal leaveId As String)
        Dim ds As New DataSet
        Dim sql As String
        sql = "EXEC [Attendance].[usp_LeaveAssignment] @Flag='a', @LeaveAssignmentID='" & leaveId & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim branchId = ds.Tables(0).Rows(0).Item("BranchID").ToString()
                Dim employeeId = ds.Tables(0).Rows(0).Item("EmployeeID").ToString()
                Dim leave = ds.Tables(0).Rows(0).Item("LeaveID").ToString()
                Dim leavetype = ds.Tables(0).Rows(0).Item("LeaveTypeID").ToString()
                Dim approvedby = ds.Tables(0).Rows(0).Item("ApprovedBy").ToString()
                Dim approveddate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ApprovedDate").ToString()).ToString("dd MMM yyyy")
                Dim lstartdate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DateFrom").ToString()).ToString("dd MMM yyyy")
                Dim lenddate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DateTo").ToString()).ToString("dd MMM yyyy")
                Dim hashalf = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("HasHalfDay").ToString())
                Dim halftype = ds.Tables(0).Rows(0).Item("FirstOrSecondHalf").ToString()
                Dim reason = ds.Tables(0).Rows(0).Item("Reason").ToString()


                divEmpChkList.Visible = False
                divEmpList.Visible = True

                ddlBranch.SelectedValue = branchId
                GetEmployeeByBranch(ddlBranch.SelectedValue)
                GetApprovalBy(ddlBranch.SelectedValue)
                ddlEmployeeName.SelectedValue = employeeId
                ddlLeaveName.SelectedValue = leave
                ddlLeaveType.SelectedValue = leavetype
                ddlApprovedBy.SelectedValue = approvedby
                txtApprovedDate.Text = approveddate
                txtLeaveStartDate.Text = lstartdate
                txtLeaveEndDate.Text = lenddate
                chkHasHalfDay.Checked = hashalf
                ddlHalfType.SelectedValue = halftype
                txtReason.Text = reason
                btnSubmit.Text = "Update"
            End If
        End If

    End Sub

    Private Sub GetLeaveAssignment()
        Dim sql As String
        sql = "EXEC [Attendance].[usp_LeaveAssignment] @Flag='s'"
        listData.InnerHtml = ""
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim sb As New StringBuilder("")
                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine(" <i class='fa fa-list'></i>Assigned Leave List</div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class=''></a><a href='javascript:;' class=''></a><a href='javascript:;' class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
                sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed flip-content'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>Employee</th>")
                sb.AppendLine("<th>Leave</th>")
                sb.AppendLine("<th>Leave Type</th>")
                sb.AppendLine("<th>From(A.D)</th>")
                sb.AppendLine("<th>To(A.D)</th>")
                sb.AppendLine("<th>From(B.S)</th>")
                sb.AppendLine("<th>To(A.D)</th>")
                sb.AppendLine("<th>Reason</th>")
                sb.AppendLine("<th>Has Half Day</th>")
                sb.AppendLine("<th>ApprovedBy</th>")
                sb.AppendLine("<th class='text-center'>Actions</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    Dim leaveAssignmentId = ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString
                    Dim employeeName = ds.Tables(0).Rows(i).Item("EmployeeName").ToString
                    Dim leaveName = ds.Tables(0).Rows(i).Item("LeaveName").ToString
                    Dim leaveType = ds.Tables(0).Rows(i).Item("LeaveType").ToString
                    Dim dateFrom = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("DateFrom")).ToString("dd MMM yyyy")
                    Dim dateTo = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("DateTo")).ToString("dd MMM yyyy")


                    'Dim nepfromDate As String = DC.ToBS(Convert.ToDateTime(dateFrom), "n")
                    'Dim neptoDate As String = DC.ToBS(Convert.ToDateTime(dateTo), "n")

                    Dim reason = ds.Tables(0).Rows(i).Item("Reason").ToString
                    Dim hasHalfDay = IIf(Convert.ToBoolean(ds.Tables(0).Rows(i).Item("HasHalfDay").ToString) = True, "Yes", "No")
                    Dim approvedBy = ds.Tables(0).Rows(i).Item("ApprovedBy").ToString

                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td>" & (i + 1) & "</td>")
                    sb.AppendLine("<td>" & employeeName & "</td>")
                    sb.AppendLine("<td>" & leaveName & "</td>")
                    sb.AppendLine("<td>" & leaveType & "</td>")
                    sb.AppendLine("<td class='text-center'>" & dateFrom & "</td>")
                    sb.AppendLine("<td class='text-center'>" & dateTo & "</td>")
                    'sb.AppendLine("<td class='text-center'>" & nepfromDate & "</td>")'' Need to change it make show system
                    'sb.AppendLine("<td class='text-center'>" & neptoDate & "</td>")

                    '------------------- remove below two line-----------
                    sb.AppendLine("<td class='text-center'>" & dateFrom & "</td>")
                    sb.AppendLine("<td class='text-center'>" & dateTo & "</td>")

                    sb.AppendLine("<td>" & reason & "</td>")
                    sb.AppendLine("<td class='text-center'>" & hasHalfDay & "</td>")
                    sb.AppendLine("<td>" & approvedBy & "</td>")
                    sb.AppendLine("<td class='text-center'><a href='?LeaveAsgnId=" & leaveAssignmentId & "' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-pencil'></span></a> | ")
                    sb.AppendLine("<a href='?DeleteLeaveAsgnId=" & leaveAssignmentId & "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></a></td>")
                    sb.AppendLine("</tr>")
                    i += 1
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                listData.InnerHtml = sb.ToString()
            End If
        End If
    End Sub




    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Leave_Details")
    End Sub
End Class