﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LeaveProfile.aspx.vb" Inherits="School.LeaveProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <style type="text/css">
      
        .BoxName1
        {
            border: 2px solid #b5b5b5;
            background-color: #f5f5f5;
            height: 125px;
            margin-right: 6px;
        }
          .Fontsize
        {
            font-size: 1.0em;
            line-height: 0px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#toPDF").click(function () {
                $("#<%= btnToPDF.ClientID %>").click();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div id="message" runat="server">
                </div>
                <div class="portlet">
                    <div class="portlet-title">
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                    <li class="btn-group">
                        <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                            data-delay="1000" data-close-others="true">
                            <span>Actions</span> <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">                           
                            <li id="toPDF"><a href="javascript:;">Export To PDF</a></li>
                        </ul>
                    </li>
                    <li><i class="fa fa-home"></i><a href="/Default.aspx">Home</a> <i class="fa fa-angle-right">
                    </i></li>
                    <li><i class="fa fa-file"></i><a href="/AttendanceManagement/LeaveSummary.aspx">Leave Summary</a><i class="fa fa-angle-right"></i></li>
                    <li><i class="fa fa-user"></i>Leave Profile</li>
                    </ul>
                    <div class="portlet-body form">
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 BoxImage">
                                        <span class="totalpay" id="Span1" runat="server">
                                            <span id ="employeeImage" runat="server"></span>
                                        </span>
                                    </div>
                                    <div class="col-lg-9 col-md-9  col-sm-9 col-xs-12 BoxName1 ">
                                        <label class=" control-label ">
                                            Name : <span class="Fontsize" id="Name" runat="server"></span></label><br />
                                        <label class=" control-label">
                                            Designation : <span class="Fontsize" id="Designation" runat="server"></span></label><br />
                                        <label class=" control-label">
                                            Employee No. : <span class="Fontsize" id="EmployeeNo" runat="server"></span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="listData" runat="server">
                </div>
            </div>
        </div>
    </div>
     <asp:Button ID="btnToPDF" runat="server" Text="Export To PDF" style="display:none;" />   
</asp:Content>
