﻿Imports System.IO
Imports System.Web.HttpContext
Public Class CincoutDetails
    Inherits System.Web.UI.Page
    Private Dao As New DatabaseDao

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then           
            GetBranch()
        End If
    End Sub

    Sub GetBranch()
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_BranchSetup] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlBranch.DataSource = ds.Tables(0)
            ddlBranch.DataTextField = "BranchName"
            ddlBranch.DataValueField = "BranchID"
            ddlBranch.DataBind()
            ddlBranch.Items.Insert(0, New ListItem("-- Select Branch --", "0"))
        Else
            ddlBranch.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
        'ds.Dispose()
    End Sub

    Protected Sub ddlBranch_SelectedIndexChange(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBranch.SelectedIndexChanged

        GetDepartment(ddlBranch.SelectedValue)
        GetDesignation(ddlBranch.SelectedValue)
        GetShift(ddlBranch.SelectedValue)
        GetWorkingStatus(ddlBranch.SelectedValue)
        chkDesignation_SelectedIndexChanged(sender, e)
        If (ddlBranch.SelectedValue = 0) Then
            ck_select.Checked = False
            chk_allJobTitle.Checked = False
            ck_select_CheckedChanged(sender, e)
            chk_allJobTitle__CheckedChanged(sender, e)
        End If
    End Sub

    Private Sub GetDepartment(ByVal branchId As String)
        Dim sql As String
        Dim ds As New DataSet
        ddlDepartment.Items.Clear()
        sql = "EXEC HR.usp_Department @flag='s',@BranchID='" & branchId & "'"
        ds.Reset()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlDepartment.DataSource = ds.Tables(0)
            ddlDepartment.DataValueField = "DepartmentID"
            ddlDepartment.DataTextField = "DepartmentName"
            ddlDepartment.DataBind()
            ddlDepartment.Items.Insert(0, New ListItem("-- All Department -- ", "0"))
        Else
            ddlDepartment.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
        'ds.Dispose()
    End Sub

    Private Sub GetDesignation(ByVal branchId As String)
        Dim sql As String
        chkDesignation.Items.Clear()
        sql = "EXEC HR.usp_JobTitle @flag='j',@BranchID='" & branchId & "'"
        Dim ds As New DataTable
        ds = Dao.ExecuteDataTable(sql)
        If ds.Rows.Count > 0 Then
            chkDesignation.DataSource = ds
            chkDesignation.DataValueField = "JobTitleID"
            chkDesignation.DataTextField = "JobTitleName"
            chkDesignation.DataBind()
        Else
            chkDesignation.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
        'ds.Dispose()
    End Sub

    Protected Sub chk_allJobTitle__CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chk_allJobTitle.CheckedChanged
        If chk_allJobTitle.Checked = True Then

            For Each listItem As ListItem In chkDesignation.Items
                listItem.Selected = True
            Next
            chk_allJobTitle.Text = "Deselect All"
        Else
            For Each listItem As ListItem In chkDesignation.Items
                listItem.Selected = False
            Next
            chk_allJobTitle.Text = "Select All"
        End If
        chkDesignation_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub chkDesignation_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkDesignation.SelectedIndexChanged
        Dim jobTitleIds As String
        Dim count As Integer = 0
        Dim count2 As Integer = 0
        'Get selected job titles
        For Each listItem As ListItem In chkDesignation.Items
            count = count + 1
            If listItem.Selected = True Then
                count2 = count2 + 1
                If count2 = 1 Then
                    jobTitleIds = chkDesignation.Items(count - 1).Value
                Else
                    jobTitleIds = jobTitleIds & "," & chkDesignation.Items(count - 1).Value
                End If
            Else

            End If
        Next
        GetEmployeeName(ddlBranch.SelectedValue, jobTitleIds)
    End Sub

    Private Sub GetShift(ByVal branchId As String)
        Dim sql As String
        ddlShift.Items.Clear()
        sql = "EXEC HR.usp_Shift @Flag='s',@BranchID='" & branchId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlShift.DataSource = ds.Tables(0)
            ddlShift.DataValueField = "ShiftID"
            ddlShift.DataTextField = "ShiftName"
            ddlShift.DataBind()
            ddlShift.Items.Insert(0, New ListItem("-- All Shift -- ", "0"))
        Else
            ddlShift.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
        'ds.Dispose()
    End Sub

    Private Sub GetWorkingStatus(ByVal branchId As String)
        Dim sql As String
        ddlWorkingStatus.Items.Clear()
        sql = "EXEC HR.usp_EmployeeCategories @flag='s',@BranchID='" & branchId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlWorkingStatus.DataSource = ds.Tables(0)
            ddlWorkingStatus.DataValueField = "EmployeeCategoriesID"
            ddlWorkingStatus.DataTextField = "EmployeeCategoriesName"
            ddlWorkingStatus.DataBind()
            ddlWorkingStatus.Items.Insert(0, New ListItem("-- All -- ", "0"))
        Else
            ddlWorkingStatus.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        End If
        'ds.Dispose()
    End Sub

    Private Sub GetEmployeeName(ByVal branchId As String, ByVal jobTitleIds As String)
        Dim sql As String

        'sql = "exec [Attendance].[usp_Employee] @flag='a',@BranchID='" & branchId & "'"
        sql = "exec [Attendance].[usp_Employee] @flag='b',@BranchID='" & branchId & "',@JobTitleIDs='" & jobTitleIds & "'"
        Dim ds As New DataSet
        chkEmployeeList.Items.Clear()
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                chkEmployeeList.DataSource = ds.Tables(0)
                chkEmployeeList.DataValueField = "ApplicationID"
                chkEmployeeList.DataTextField = "EmployeeName"
                chkEmployeeList.DataBind()

            Else
                chkEmployeeList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
            End If
        Finally

        End Try
    End Sub

   

    Private Sub GetEmployee(ByVal branchId As String)
        'Dim sql As String
        'sql = "exec [Attendance].[usp_Employee] @flag='a',@BranchID='" & branchId & "'"
        'Dim ds As New DataSet
        'chkEmployeeList.Items.Clear()
        'Try
        '    ds = Dao.ExecuteDataset(sql)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        chkEmployeeList.DataSource = ds.Tables(0)
        '        chkEmployeeList.DataValueField = "ApplicationID"
        '        chkEmployeeList.DataTextField = "EmployeeName"
        '        chkEmployeeList.DataBind()

        '    Else
        '        chkEmployeeList.Items.Insert(0, New ListItem("-- No Record Found -- ", ""))
        '    End If
        'Finally

        'End Try
    End Sub

    Protected Sub ck_select_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ck_select.CheckedChanged
        If ck_select.Checked = True Then
            For Each listItem As ListItem In chkEmployeeList.Items
                listItem.Selected = True
            Next


            ck_select.Text = "Deselect All"
        Else
            For Each listItem As ListItem In chkEmployeeList.Items
                listItem.Selected = False
            Next
            ck_select.Text = "Select All"
        End If

    End Sub

    Sub GetAttendaceList(ByVal StartDate As Date, ByVal EmpIds As String, ByVal JobTitleIds As String)
        Dim sql As String
        Dim sb As StringBuilder = New StringBuilder("")
        Dim ds As New DataSet
        listData.InnerHtml = ""
        sql = "exec [Attendance].[Summary_New] @StartDate='" & StartDate & "', @EndDate='" & StartDate & "', @EmpIds='" & EmpIds & "'"
        If ddlDepartment.SelectedValue > 0 Then
            sql += ",@DeptId='" & ddlDepartment.SelectedValue & "'"
        End If
        If Not String.IsNullOrWhiteSpace(JobTitleIds) Then
            sql += ",@JobTitleIds='" & JobTitleIds & "'"
        End If
        If ddlShift.SelectedValue > 0 Then
            sql += ",@ShiftId='" & ddlShift.SelectedValue & "'"
        End If
        If ddlWorkingStatus.SelectedValue > 0 Then
            sql += ",@WorkingStatus='" & ddlWorkingStatus.SelectedValue & "'"
        End If
        If ddlWorkingTime.SelectedValue > 0 Then
            sql += ",@WorkingTime='" & ddlWorkingTime.SelectedValue & "'"
        End If

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine("<div class='col-md-12'>")
            sb.AppendLine("<div class='portlet box purple'>")
            sb.AppendLine("<div class='portlet-title'>")
            sb.AppendLine("<div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cog'></i>Attendace Summary: " & txtStartDate.Text & "(" & txtStartDate_Nep.Text & ") -" & ds.Tables(0).Rows(0).Item("weekDayName").ToString & " </div>")
            sb.AppendLine("<div class='tools'>")
            sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;' class='remove'></a>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("<div class='portlet-body flip-scroll' id='listData' runat='server'>")
            sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content fixed_header'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>S.N</th>")
            'sb.AppendLine("<th>Date</th>")
            sb.AppendLine("<th>Name</th>")
            ' sb.AppendLine("<th>Designation</th>")
            sb.AppendLine("<th>Check-In</th>")
            sb.AppendLine("<th>Check-Out</th>")
            sb.AppendLine("<th>Duration</th>")
            sb.AppendLine("<th>Actual Time</th>")
            sb.AppendLine("<th>OT</th>")
            sb.AppendLine("<th>Late(MIN)</th>")
            sb.AppendLine("<th>Early(MIN)</th>")
            sb.AppendLine("<th>Holiday</th>")
            sb.AppendLine("<th>Leave</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")
            Dim i As Integer = 0
            '   EmpId	EmpName	JobTitleName	ATTENDEEID	CheckIn	checkOut	DT	stime	etime	workingHour	workMin	Late(MIN)	Early(MIN)	Duration	Holiday	Leave	OT

            'EmpId	EmpName	JobTitleName	ATTENDEEID	CheckIn	checkOut	DT	stime	etime	workingHour	workMin	Late(MIN)	Early(MIN)	Duration	Holiday	Leave	OT

            For Each row As DataRow In ds.Tables(0).Rows
                Dim SN = i + 1 'ds.Tables(0).Rows(i).Item("SN").ToString
                Dim DateTime As String = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dt").ToString()).ToString("dd-MMM-yyyy")
                Dim Name = ds.Tables(0).Rows(i).Item("EmpName").ToString
                Dim Job = ds.Tables(0).Rows(i).Item("JobtitleName").ToString
                Dim CheckIn = ds.Tables(0).Rows(i).Item("checkin").ToString
                Dim CheckOut = ds.Tables(0).Rows(i).Item("checkout").ToString
                Dim Duration = ds.Tables(0).Rows(i).Item("duration").ToString
                Dim ActualTime = ds.Tables(0).Rows(i).Item("ActualTime").ToString
                Dim OT = ds.Tables(0).Rows(i).Item("ot").ToString
                Dim Late = ds.Tables(0).Rows(i).Item("late(min)").ToString
                Dim Early = ds.Tables(0).Rows(i).Item("early(min)").ToString
                Dim Holiday = ds.Tables(0).Rows(i).Item("Holiday").ToString
                Dim Leave = ds.Tables(0).Rows(i).Item("Leave").ToString
                Dim Satuarday = ds.Tables(0).Rows(i).Item("weekDayName").ToString
                Dim Cssclass = ds.Tables(0).Rows(i).Item("class").ToString
                Dim AttStatus = ds.Tables(0).Rows(i).Item("AttStatus").ToString
                If Not String.IsNullOrWhiteSpace(Cssclass) Then
                    sb.AppendLine("<tr><td class='text-center' style='" & Cssclass & "'>" & i + 1 & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & Name & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & CheckIn & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & CheckOut & "</td>")
                Else
                    sb.AppendLine("<tr><td class='text-center'>" & i + 1 & "</td>")
                    sb.AppendLine("<td class='text-center'>" & Name & "</td>")
                    sb.AppendLine("<td class='text-center'>" & CheckIn & "</td>")
                    sb.AppendLine("<td class='text-center'>" & CheckOut & "</td>")
                End If
                '  sb.AppendLine("<td>" & DateTime & "</td>")
                ' sb.AppendLine("<td>" & Job & "</td>")
                If String.IsNullOrWhiteSpace(Cssclass) Then
                    
                    sb.AppendLine("<td class='text-center'>" & Duration & "</td>")
                    sb.AppendLine("<td class='text-center'>" & ActualTime & "</td>")
                    sb.AppendLine("<td class='text-center'>" & OT & "</td>")
                    sb.AppendLine("<td class='text-center'>" & Late & "</td>")
                    sb.AppendLine("<td class='text-center'>" & Early & "</td>")
                    If ds.Tables(0).Rows(0).Item("weekDayName") = "Saturday" Then
                        sb.AppendLine("<td  style='background:red; color:white;'>Saturday</td>")
                    Else
                        If Not String.IsNullOrWhiteSpace(Holiday) Then
                            sb.AppendLine("<td style='background:red; color:white;'>" & Holiday & "</td>")
                        Else
                            sb.AppendLine("<td>" & Holiday & "</td>")
                        End If
                    End If
                    sb.AppendLine("<td>" & Leave & "</td>")
                Else
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & Duration & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & ActualTime & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & OT & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & Late & "</td>")
                    sb.AppendLine("<td class='text-center' style='" & Cssclass & "'>" & Early & "</td>")
                    'If String.IsNullOrWhiteSpace(Leave) Then
                    '    Leave = "Absent"
                    'End If
                    sb.AppendLine("<td style='" & Cssclass & "'>" & AttStatus & "</td>")
                    sb.AppendLine("<td style='" & Cssclass & "'>" & Leave & "</td>")
                End If


                sb.AppendLine("</tr>")
                i += 1
            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            listData.InnerHtml = sb.ToString()
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "msg", "alert('" + ds.Tables(0).Rows.Count.ToString + " records found.');", True)

        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Record Found")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim mes As String = ""
        Dim sb As StringBuilder = New StringBuilder("")
        message.InnerHtml = ""
        If Not chkEmployeeList.Items.Count > 0 Then
            mes = "No Employee selected."
        End If
        If mes <> "" Then
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine(mes)
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
            message.InnerHtml = sb.ToString
            Return
        End If
        Try
            Dim count As Integer = 0
            Dim count2 As Integer = 0
            Dim EmployeeIDs As String = ""
            Dim jobTitleIds As String
            count = 0
            count2 = 0
            For Each listItem As ListItem In chkEmployeeList.Items
                count = count + 1
                If listItem.Selected = True Then
                    count2 = count2 + 1
                    If count2 = 1 Then
                        EmployeeIDs = chkEmployeeList.Items(count - 1).Value
                    Else
                        EmployeeIDs = EmployeeIDs & "," & chkEmployeeList.Items(count - 1).Value
                    End If
                Else

                End If
            Next

            'Get selected Employees
            'For Each listItem As ListItem In lstEmployees.Items
            '    count = count + 1
            '    If listItem.Selected = True Then
            '        count2 = count2 + 1
            '        If count2 = 1 Then
            '            EmployeeIDs = lstEmployees.Items(count - 1).Value
            '        Else
            '            EmployeeIDs = EmployeeIDs & "," & lstEmployees.Items(count - 1).Value
            '        End If
            '    Else

            '    End If
            'Next

            'Get selected job titles
            count = 0
            count2 = 0
            For Each listItem As ListItem In chkDesignation.Items
                count = count + 1
                If listItem.Selected = True Then
                    count2 = count2 + 1
                    If count2 = 1 Then
                        jobTitleIds = chkDesignation.Items(count - 1).Value
                    Else
                        jobTitleIds = jobTitleIds & "," & chkDesignation.Items(count - 1).Value
                    End If
                Else

                End If
            Next


            If String.IsNullOrWhiteSpace(EmployeeIDs) Then
                mes = "No Employee selected."
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine(mes)
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                message.InnerHtml = sb.ToString
                Return
            End If
            GetAttendaceList(txtStartDate.Text, EmployeeIDs, jobTitleIds)
        Catch ex As Exception

        End Try

    End Sub

    Public Function Excel(ByVal html As String, ByVal fileName As String)

        Current.Response.Clear()
        Current.Response.Buffer = True
        Current.Response.AddHeader("content-disposition", "attachment;filename=" & fileName.Replace(" ", "_") & ".xls")
        Current.Response.Charset = ""
        Current.Response.ContentType = "application/vnd.ms-excel"

        Dim sw As New StringWriter()
        sw.Write("<html>")
        sw.Write("<head><meta http-equiv=""Content-Type"" content=""text/html"" charset=""UTF-8"" />")
        'sw.Write("<link href='" & ConfigurationSettings.AppSettings("rootUrl") & "/assets/plugins/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css' /></head>")

        sw.Write("<style>")
        sw.Write("table{font-size:12pt; font-family:'Times New Roman'; border:1px; width:700px; border-collapse: collapse;}th{ border:1px solid black;}td{border:1px solid black;} .caption{ text-align:center; font-weight:bold; margin-top:50px important!; font-size: 18px; }")
        sw.Write("</style>")

        sw.Write("<body>")

        Dim row As DataRow = (New DatabaseDao().ExecuteDataTable("SELECT top 1 Name,[Address] FROM Management.InstitutionalInfo").Rows(0))

        sw.Write("<div class='row' style='clear:both'>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 20px;'>" & row("Name").ToString() & "</div>")
        sw.Write("<div style='text-align:center; font-weight:bold; font-size: 15px;'>" & row("Address").ToString() & "</div>")
        sw.Write("</div><br>")

        sw.Write(html)
        sw.Write("</body>")
        sw.Write("</html>")
        Dim fullHtml As String = sw.ToString()
        Current.Response.Output.Write(fullHtml)
        Current.Response.Flush()
        Current.Response.[End]()
    End Function


    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToExcel.Click
        Excel(listData.InnerHtml, "Attendance_summary_All")
    End Sub
End Class