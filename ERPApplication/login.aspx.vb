﻿
Public Class login1
    Inherits System.Web.UI.Page
    Private Dao As New DatabaseDao
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '    'If Not String.IsNullOrWhiteSpace(Request.QueryString("userID")) And
        '    '    Not String.IsNullOrWhiteSpace(Request.QueryString("username")) Or
        '    '    Not String.IsNullOrWhiteSpace(Request.QueryString("Role")) Or
        '    '    Not String.IsNullOrWhiteSpace(Request.QueryString("Branch")) Or
        '    '    Not String.IsNullOrWhiteSpace(Request.QueryString("Type")) Or
        '    '    Not String.IsNullOrWhiteSpace(Request.QueryString("CompanyID")) Then
        '    '    Session("userID") = Request("userID")
        '    '    Session("username") = Request("username")
        '    '    Session("Role") = Request("Role")
        '    '    Session("Branch") = Request("Branch")
        '    '    Session("Type") = Request("Type")
        '    '    Session("YearID") = Request("YearID")
        '    '    'GloblaBranchID = Request("Branch").ToString
        '    '    'GlobalUserID = Request("userID")
        '    '    Session("FY") = "1"
        '    '    Session("DatabaseName") = Request("DatabaseName")
        '    '    Session("CompanyID") = Request("CompanyID")
        '    '    Session("BranchID") = Request("Branch")


        '    '    'If (Session("BranchID") <> Nothing) Then
        '    '    '    globalBranchID = Session("BranchID")
        '    '    'End If
        '    '    'If (Session("CompanyID") <> Nothing) Then
        '    '    '    globalCompanyID = Session("CompanyID")
        '    '    'End If
        '    '    'If (Session("username") <> Nothing) Then
        '    '    '    globalUserName = Session("username")
        '    '    'End If

        '    '    Session("FiscalYearID") = CommonFunctionsModule.GetCurrentFiscalYearId(Convert.ToInt32(Session("BranchID")))   'Active current fiscal year

        '    '    Session("AcademicYearID") = CommonFunctionsModule.GetCurrentAcademicYearId(Convert.ToInt32(Session("BranchID"))) 'get active academic year branchwise

        '    '    Session("IsPublicCampus") = CommonFunctionsModule.IsPublicCampus()
        '    '    GetOSVersionInfo()
        '    'End If

        If Session("Lock") = "lock" Then
            If Not String.IsNullOrWhiteSpace(Request.QueryString("x")) And
                     Not String.IsNullOrWhiteSpace(Request.QueryString("err")) Then
                Dim UserID As String = Request.QueryString("x")
                Dim ErrorCode As String = Request.QueryString("err")
                If UserID = Session("UserID").ToString And ErrorCode = 0 Then
                    Response.Cookies("Redirect").Value = "/NoPermission/NoPermission.aspx"
                    Session("Lock") = ""
                    DefaultPage(Session("Type"))

                Else
                    Response.Redirect("~/Login.aspx")
                End If

            End If
        Else
            DefaultPage(Session("Type"))
        End If


       

    End Sub


    Private Sub DefaultPage(ByVal userType As String)
        Select Case userType
            Case "User"
                Response.Redirect("/Default.aspx")

            Case "Student"
                Response.Redirect("~/StudentPanel/Default.aspx")

            Case "Employee"
                Response.Redirect("~/TeacherPanel/Default.aspx")
            Case "Parents"
                Response.Redirect("")
        End Select
    End Sub


    Private Sub GetOSVersionInfo()
        Dim os As OperatingSystem = Environment.OSVersion
        Dim ver As String = os.Version.ToString()
        Dim verstring As String = os.VersionString.ToString()

        Dim majorVer As Integer = os.Version.Major
        Dim minorVer As Integer = os.Version.Minor

        Session("OSVersion") = ""
        If majorVer = 5 And minorVer = 1 Then
            'Console.WriteLine("Windows XP")
            Session("OSVersion") = "WinXP"
        ElseIf majorVer = 6 And minorVer = 0 Then
            'Console.WriteLine("Windows Vista")
            Session("OSVersion") = "WinVista"
        ElseIf majorVer = 6 And minorVer = 1 Then
            'Console.WriteLine("Windows 7")
            Session("OSVersion") = "Win7"
        ElseIf majorVer = 6 And minorVer = 2 Then
            'Console.WriteLine("Windows 8")
            Session("OSVersion") = "Win8"
        ElseIf majorVer = 6 And minorVer = 3 Then
            'Console.WriteLine("Windows 8.1")
            Session("OSVersion") = "Win8"
        ElseIf majorVer = 10 And minorVer = 0 Then
            'Console.WriteLine("Windows 10")
            Session("OSVersion") = "Win10"
        Else
            Session("OSVersion") = "Not Found"
        End If

        'Console.ReadLine()
    End Sub


    Protected Sub btn_login2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_login2.Click
        UserLogin(username.Text, password.Text)
    End Sub

    Sub UserLogin(ByVal username As String, ByVal password As String, Optional ByVal db_connectionString As String = "")
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_UserInfo]  @flag='l', @UserName='" & username & "', @Password='" & password & "'" ',@FiscalYearID='" & academicyear & "'"
        Dim returnMsg As LoginMessage()
        Try
            Global_Active_db_fy = db_connectionString
            ds = Dao.ExecuteDataset(sql)

            If ds.Tables.Count > 0 Then

                If ds.Tables(0).Rows(0).Item("errorCode").ToString = "0" Then

                    Session("userID") = ds.Tables(0).Rows(0).Item("userID")
                    Session("username") = ds.Tables(0).Rows(0).Item("mes")
                    Session("Role") = ds.Tables(0).Rows(0).Item("Role")
                    Session("Branch") = ds.Tables(0).Rows(0).Item("Branch")
                    Session("Type") = ds.Tables(0).Rows(0).Item("Category")
                    ' Session("YearID") = ds.Tables(0).Rows(0).Item("YearID")
                    'GloblaBranchID = Request("Branch").ToString
                    'GlobalUserID = Request("userID")
                    Session("FY") = "1"
                    Session("DatabaseName") = ds.Tables(0).Rows(0).Item("DatabaseName")
                    Session("CompanyID") = ds.Tables(0).Rows(0).Item("CompanyID")
                    Session("BranchID") = ds.Tables(0).Rows(0).Item("Branch")

                    Session("FiscalYearID") = CommonFunctionsModule.GetCurrentFiscalYearId(Convert.ToInt32(Session("BranchID")))   'Active current fiscal year

                    Session("AcademicYearID") = CommonFunctionsModule.GetCurrentAcademicYearId(Convert.ToInt32(Session("BranchID"))) 'get active academic year branchwise

                    Session("IsPublicCampus") = CommonFunctionsModule.IsPublicCampus()

                    If (Session("IsPublicCampus") = False) Then
                        Session("IsSimpleVoucher") = CommonFunctionsModule.IsSimpleVoucher()
                    End If
                    GetOSVersionInfo()

                    'Check User Account Lock
                    If ds.Tables.Count > 1 Then
                        If (ds.Tables(1).Rows.Count > 0) Then
                            If (String.IsNullOrWhiteSpace(ds.Tables(1).Rows(0).Item("IsActive").ToString.Trim())) Then
                                Session("IsActiveLock") = False
                            Else
                                Session("IsActiveLock") = ds.Tables(1).Rows(0).Item("IsActive").ToString.Trim()
                            End If
                        Else
                            Session("IsActiveLock") = False
                        End If
                        If Session("IsActiveLock") = True Then
                            Session("Lock") = "lock"
                            ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "Error", " $(document).ready(function () { window.location.href = 'login.aspx?x=" & Session("UserID").ToString & "';});", True)
                        End If
                    End If
                    If Session("IsActiveLock") Is Nothing Or Session("IsActiveLock") = False Then
                        DefaultPage(ds.Tables(0).Rows(0).Item("Category"))
                    End If

                End If
                ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "Error", " alert('Password or Username Does Not Match');", True)
            End If


        Catch ex As Exception

            ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "Error", " alert('" & ex.Message & "');", True)
        End Try


    End Sub

    Protected Function IsActiveLock() As String
        Return Session("IsActiveLock").ToString.ToLower.Trim
    End Function

    'Protected Sub btnUnLockAccount_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUnLockAccount.Click
    '    UnLockUserAccount(Session("userID"), txtlockcode.Text.Trim)
    'End Sub

    Protected Sub UnLockUserAccount(ByVal UserID, ByVal LockCode)
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Dim returnMsg As LoginMessage()
        Try
            sql = "exec [Setup].[usp_ACLOCK] @Flag = 'unlock',@UserID='" & UserID & "',@LockCode='" & LockCode & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("errorcode").ToString = "0" Then
                    'returnMsg = New LoginMessage(1) {}
                    'returnMsg(0) = New LoginMessage()
                    'returnMsg(0).message = ds.Tables(0).Rows(0).Item("mes").ToString.Trim()
                    'returnMsg(0).role = ds.Tables(0).Rows(0).Item("Role").ToString.Trim()
                    'returnMsg(0).branch = ds.Tables(0).Rows(0).Item("Branch").ToString.Trim()
                    'returnMsg(0).errorcode = ds.Tables(0).Rows(0).Item("errorcode").ToString()
                    'returnMsg(0).userid = ds.Tables(0).Rows(0).Item("UserID").ToString()
                    'returnMsg(0).Type = ds.Tables(0).Rows(0).Item("Category").ToString()
                    '' returnMsg(i).YearID = rows("YearID").ToString
                    'returnMsg(0).DatabaseName = ds.Tables(0).Rows(0).Item("DatabaseName").ToString
                    '' returnMsg(i).FiscalYearID = rows("FiscalYearID").ToString
                    'returnMsg(0).CompanyID = ds.Tables(0).Rows(0).Item("CompanyID").ToString.Trim()
                    DefaultPage(Session("Type"))
                Else
                    'returnMsg = New LoginMessage(1) {}
                    'returnMsg(0) = New LoginMessage()
                    'returnMsg(0).message = ds.Tables(0).Rows(0).Item("mes").ToString()
                    'returnMsg(0).errorcode = "2"
                    ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "Error", " alert('" & ds.Tables(0).Rows(0).Item("mes").ToString() & "');", True)
                End If
            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, MetaDescription.GetType(), "Error", " alert('" & ex.Message & "');", True)
        End Try

    End Sub

End Class

