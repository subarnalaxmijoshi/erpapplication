﻿<%@ Page Language="vb" AutoEventWireup="false" EnableSessionState="True" CodeBehind="login.aspx.vb"
    Inherits="School.login1" %>

<%--<%@ Import Namespace="System.Web.Optimization" %>--%>
<html>
<head runat="server">
    <meta charset="utf-8" />
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="MobileOptimized" content="320">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/pages/login.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <%--<script src="assets/scripts/login.js" type="text/javascript"></script>--%>
    <%-- <script src="/assets/scripts/main.js" type="text/javascript"></script>--%>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">
        .up
        {
            position: relative;
        }
    </style>
    <script src="/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.lock-form').hide();
            if (location.search.substring(1).split('?') != "") {
                $('.login-form').hide();
                $('.lock-form').show();
            }
        });

     
    </script>
</head>
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo" style="color: #FFFFFF; font-size: 24px;">
        <%--<img src="assets/img/schoollogo.png" alt="Logo"/> Mitra erp--%>
        <img src="assets/InstitutionLogo/tulogo1.png" alt="Logo" />
        <div>
            Mitra ERP 2076</div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form id="Form1" class="login-form" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <%--     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>--%>
        <%--    <img src="assets/img/logo.png" alt="Logo" width="50px" height="65px" display="Absolute" class="up"/>--%>
        <h4 class="form-title " style='text-align: center'>
            Welcome To Mitra&#8482;ERP
        </h4>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert">
            </button>
            <span>Enter any username and password.</span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">
                AcademicYear</label>
            <select name="AcademicYear" id="AcademicYear" class="select2 form-control">
                <option value="connectionString">2077</option>
              
            </select>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">
                Username</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <asp:TextBox class="form-control placeholder-no-fix" type="text" autocomplete="off"
                    placeholder="Username" runat="server" name="username" ID="username">
                </asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">
                Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <asp:TextBox class="form-control placeholder-no-fix" type="password" autocomplete="off"
                    runat="server" placeholder="Password" name="password" ID="password">
                </asp:TextBox>
                    <asp:HiddenField ID="hdn_userID" runat="server" />
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox">
                <input type="checkbox" name="remember" value="1" />
                Remember me
            </label>
            <%--      <button type="submit" class="btn green pull-right" id="btn_login" onclick="return btn_login_onclick()">
                Login <i class="m-icon-swapright m-icon-white"></i>
            </button>--%>
            <asp:Button ID="btn_login2" runat="server" class="btn green pull-right" Text="LOGIN"
                UseSubmitBehavior="true" />
            <input type="hidden" name="id" id="id" /><br />
            <span id="popup" style="display: none; text-align: center">
                <img src="assets/img/loading.gif" /></span>
        </div>
        <div class="forget-password">
            <h4>
                Forgot your password ?</h4>
            <p>
                No worries, click <a href="/fp.aspx" id="forget-password">here</a> to reset
                your password.
            </p>
        </div>
        <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
        </form>
        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form">
        <h3>
            Forget Password ?</h3>
        <p>
            Enter your e-mail address below to reset your password.</p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email"
                    name="email" />
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="m-icon-swapleft"></i>Back
            </button>
            <button type="submit" class="btn green pull-right">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        </form>
        <form class="lock-form">
        <h3>
            Lock Code ?</h3>
        <p>
            Enter your lock code below to access your account.</p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Lock Code"
                    name="lockcode" id="lockcode" maxlength="4" />
                <%--   <asp:TextBox ID="txtlockcode" runat="server" CssClass="form-control placeholder-no-fix" placeholder="Lock Code"
                MaxLength="4"></asp:TextBox>--%>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="Button1" class="btn hidden">
                <i class="m-icon-swapleft"></i>Back
            </button>
            <button type="button" class="btn green pull-right" id="btn_lock">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
            <%--<asp:Button ID="btnUnLockAccount" runat="server" Text="Submit" class="btn green pull-right"/>--%>
        </div>
   
        <script type="text/javascript">

            function ParseURL(name, url) {
                if (!url) url = location.href;
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regexS = "[\\?&]" + name + "=([^&#]*)";
                var regex = new RegExp(regexS);
                var results = regex.exec(url);
                return results == null ? null : results[1];
            }
            $("#btn_lock").click(function () {
                var url = window.location.href;
                var userid = ParseURL('x', url);
                var lockcode_ = $('#lockcode').val();
                $.ajax({
                    type: "POST",
                    url: "/SetupServices.svc/UnLockAccount",
                    data: '{"UserID":"' + userid + '","LockCode":"' + lockcode_ + '"}',
                    dataType: "json",
                    contentType: "application/json",
                    beforeSend: function () {
                        $('.alert-danger,.alert-success').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                        $('.alert-danger').show();
                        $('.alert-danger span').html(errorThrown);
                        $('#popup').hide();
                    },
                    success: function (data) {
                        debugger;
                        console.log(data);

                        if (data[0].errorcode == "0") {

                            userid_ = data[0].userid;
                            username_ = data[0].message;
                            role = data[0].role;
                            branch = data[0].branch;
                            userType = data[0].Type;
                            yearid = data[0].YearID;
                            dbname = data[0].DatabaseName;
                            FiscalYearID = data[0].FiscalYearID;
                            err = data[0].errorcode;

                            window.location.href = "login.aspx?x=" + userid_ + "&err=" + err;

                        } else {
                            $('#popup').hide();
                            alert(data[0].message);

                        }


                        $('.alert-success').show().css({ display: 'block' });

                        $('.alert-success span').html(data);
                    }
                });
            });
        </script>
        </form>
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        Copyright © 2019 Mitra™ ERP.
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
	<script src="/assets/plugins/respond.min.js"></script>
	<script src="/assets/plugins/excanvas.min.js"></script> 
	<![endif]-->
    <%-- <script src="/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>--%>
    <%--   <script src="/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
   <script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>--%>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <%--  <script src="/assets/plugins/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>--%>
    <%--<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>--%>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <%--<script src="/assets/scripts/app.js" type="text/javascript"></script>--%>
    <%--<script src="/assets/scripts/login.js" type="text/javascript"></script>--%>
    <!-- END PAGE LEVEL SCRIPTS -->
    <%--     <script type="text/javascript">
//         jQuery(document).ready(function () {
//             App.init();
//             Login.init();

//         });

//         function btn_login_onclick() {

//                  }
</script>--%>
    <script type="text/javascript">

        $('.alert-danger').hide();


        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            }

            //             submitHandler: function (form) {
            ////                 ajaxLogin(form); // form validation success, call ajax form submit
            //             }
        });

//        $('.login-form input').keypress(function (e) {
//            if (e.which == 13) {
//                if ($('.login-form').validate().form()) {
//                    return true;
//                    //                     ajaxLogin($('.login-form')); //form validation success, call ajax form submit
//                }
//                return false;
//            }
//        });

        $('.lock-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                lockcode: {
                    required: true,
                    number: true,
                    minlength: 4
                }
            },

            messages: {
                lockcode: {
                    required: "Lock Code is required.",
                    number: "Code must be numbers.",
                    minlength: jQuery.format("At least {0} character are required.")

                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            }

//            submitHandler: function (form) {
//                ajaxLock(form); // form validation success, call ajax form submit
//            }
        });



//        $('.lock-form input').keypress(function (e) {

//            if (e.which == 13) {
//                if ($('.lock-form').validate().form()) {
//                    return true;
//                    // alert('jsdkf');
//                    //ajaxLock($('.lock-form')); //form validation success, call ajax form submit
//                }
//                return false;
//            }
//        });
    </script>
</body>
</html>
