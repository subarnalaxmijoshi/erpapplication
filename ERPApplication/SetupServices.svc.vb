﻿Imports System.Web
Imports System.Web.HttpContext
Imports System.Globalization
Imports System.Runtime.Serialization
Imports System.Data.SqlClient
Imports System.Web.Script.Services
Imports System.IO
Imports System.Net


' NOTE: You can use the "Rename" command on the context menu to change the class name "SetupServices" in code, svc and config file together.
Public Class SetupServices
    Implements ISetupServices

    Private Dao As New DatabaseDao

    'Interface to Connect to Finger Print Device

    'Private ReadOnly axCZKEM1 As New zkemkeeper.CZKEM
    Public Function ChangeDate(ByVal Convertdate As String, ByVal type As String) As String Implements ISetupServices.ChangeDate
        Dim dt As String = String.Empty
        Dim dd, mm, yy As String
        Dim val As Array
        Try
            If type.ToLower = "e" Then
                val = Convertdate.Split("-")

                dd = val(2)
                mm = val(1)
                yy = val(0)
                dt = nep_to_eng(yy, mm, dd)

            ElseIf type.ToLower = "n" Then

                val = Convertdate.Split("/")

                dd = val(0)
                mm = val(1)
                yy = val(2)
                dt = eng_to_nep(yy, mm, dd)
            End If
        Catch ex As Exception
            dt = String.Empty
        End Try

        If String.IsNullOrWhiteSpace(dt) Then
            Return dt
        End If

        val = dt.Split("/")
        dd = val(2)
        mm = val(1)
        yy = val(0)
        dt = dd & "/" & mm & "/" & yy
        Return dt
    End Function

    Public Function ChangeDate2(ByVal Convertdate As String, ByVal type As String) As String Implements ISetupServices.ChangeDate2
        Dim dt As String = String.Empty
        Dim dd, mm, yy As String
        Dim val As Array
        Try
            If type.ToLower = "e" Then
                val = Convertdate.Split("-")

                dd = val(2)
                mm = val(1)
                yy = val(0)
                dt = nep_to_eng(yy, mm, dd)

            ElseIf type.ToLower = "n" Then

                val = Convertdate.Split("/")

                dd = val(1)
                mm = val(0)
                yy = val(2)
                dt = eng_to_nep(yy, mm, dd)
            End If
        Catch ex As Exception
            dt = String.Empty
        End Try

        If String.IsNullOrWhiteSpace(dt) Then
            Return dt
        End If

        val = dt.Split("/")
        dd = val(2)
        mm = val(1)
        yy = val(0)
        dt = dd & "/" & mm & "/" & yy
        Return dt
    End Function

    Public Function DateConverterNew(ByVal Convertdate As String, ByVal type As String) As String Implements ISetupServices.DateConverterNew
        Dim dt As String = String.Empty
        Dim dd, mm, yy As String
        Dim val As Array
        Dim DC As New DateConverter()
        Try
            If type.ToLower = "e" Then
                val = Convertdate.Split("/")

                dd = val(2)
                mm = val(1)
                yy = val(0)
                'dt = nep_to_eng(yy, mm, dd)
                'dt = DC.ToAD(New Date(yy, mm, dd), "e")
                dt = DC.ToAD(yy, mm, dd)

            ElseIf type.ToLower = "n" Then

                val = Convertdate.Split("/")

                dd = val(0)
                mm = val(1)
                yy = val(2)
                'dt = eng_to_nep(yy, mm, dd)
                'dt = DC.ToBS(New Date(yy, mm, dd), "n")
                dt = DC.ToBS(dd, mm, yy)
            End If
        Catch ex As Exception
            dt = String.Empty
        End Try

        If String.IsNullOrWhiteSpace(dt) Then
            Return dt
        End If

        val = dt.Split("/")
        dd = val(2)
        mm = val(1)
        yy = val(0)
        dt = dd & "/" & mm & "/" & yy
        Return dt
    End Function
    Public Function DeactivateStudent(ByVal studentId As String, ByVal opt As String) As String Implements ISetupServices.DeactivateStudent
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Students].[usp_PersonalInfo]  @Flag='Activate', @IsActive='" & opt & "', @StudentID='" & studentId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim stat As String = ds.Tables(0).Rows(0).Item("IsActive").ToString()
                    If stat = "False" Then
                        stat = "0"
                    Else
                        stat = "1"
                    End If

                    Return "{" & """DeactivateStatus"" : " & """" & stat & """}"
                End If
            End If
        Catch ex As Exception
            Return "{" & """DeactivateStatus"" : " & """" & "0" & """}"
        End Try
        Return "{" & """DeactivateStatus"" : " & """" & "0" & """}"
    End Function


    Public Function FatherDetail(ByVal id As String) As String Implements ISetupServices.FatherDetail
        Dim sql As String
        sql = "EXEC  [Students].[usp_ParentsInfo] @Flag='a', @ParentsID='" & id & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = ""
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                returnMsg += "{" & """fatherName"" : " & """" & ds.Tables(0).Rows(0).Item("FathersName").ToString() & ""","
                returnMsg += """fatherOccupation"" : " & """(" & ds.Tables(0).Rows(0).Item("FOccupation").ToString() & ")"","
                returnMsg += """fatherMobile"" : " & """" & ds.Tables(0).Rows(0).Item("FMobile").ToString() & ""","
                returnMsg += """fatherEmail"" : " & """" & ds.Tables(0).Rows(0).Item("FEmail").ToString().Trim() & """},"
            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += ""
        Return returnMsg
    End Function

    Public Function MotherDetail(ByVal id As String) As String Implements ISetupServices.MotherDetail
        Dim sql As String
        sql = "EXEC  [Students].[usp_ParentsInfo] @Flag='a', @ParentsID='" & id & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = ""
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                returnMsg += "{" & """motherName"" : " & """" & ds.Tables(0).Rows(0).Item("MothersName").ToString() & ""","
                returnMsg += """motherOccupation"" : " & """(" & ds.Tables(0).Rows(0).Item("MOccupation").ToString() & ")"","
                returnMsg += """motherMobile"" : " & """" & ds.Tables(0).Rows(0).Item("MMobile").ToString() & ""","
                returnMsg += """motherEmail"" : " & """" & ds.Tables(0).Rows(0).Item("MEmail").ToString().Trim() & """},"
            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += ""
        Return returnMsg
    End Function

    Public Function GuardianDetail(ByVal id As String) As String Implements ISetupServices.GuardianDetail
        Dim sql As String
        sql = "EXEC  [Students].[usp_ParentsInfo] @Flag='g', @GuardianID='" & id & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = ""
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                returnMsg += "{" & """guardianName"" : " & """" & ds.Tables(0).Rows(0).Item("Name").ToString() & ""","
                returnMsg += """guardianOccupation"" : " & """(" & ds.Tables(0).Rows(0).Item("Occupation").ToString() & ")"","
                returnMsg += """guardianMobile"" : " & """" & ds.Tables(0).Rows(0).Item("MobileNo").ToString() & ""","
                returnMsg += """guardianEmail"" : " & """" & ds.Tables(0).Rows(0).Item("Email").ToString() & ""","
                returnMsg += """guardianAddress"" : " & """" & ds.Tables(0).Rows(0).Item("GAddress").ToString() & ""","
                returnMsg += """guardianRelation"" : " & """" & ds.Tables(0).Rows(0).Item("RelationToStudent").ToString().Trim() & """},"
            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += ""
        Return returnMsg
    End Function


    Public Function StudentExamTimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String,
                                         ByVal batchId As String) As String Implements ISetupServices.StudentExamTimeTable
        Dim sql As String = "EXEC  [Setup].[usp_StudentCalendar] @Flag='exam', @YearID='" & YearId & "', @MonthID='" & MonthId &
                            "', @DayNumber='" & DayNumber & "', @BatchID= '" & batchId & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    returnMsg += "{" & """batchTitle"" : " & """" & ds.Tables(0).Rows(i).Item("BatchTitle").ToString() & ""","
                    returnMsg += """subjectTitle"" : " & """" & ds.Tables(0).Rows(i).Item("SubjectTitle").ToString() & ""","
                    returnMsg += """date"" : " & """" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("Date")).ToString("dd MMM yyyy") & ""","
                    returnMsg += """timeFrom"" : " & """" & ds.Tables(0).Rows(i).Item("TimeFrom").ToString() & ""","
                    returnMsg += """timeTo"" : " & """" & ds.Tables(0).Rows(i).Item("TimeTo").ToString().Trim() & """},"
                    i += 1
                Next
            End If
            returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        End If
        returnMsg += "]"
        Return returnMsg
    End Function

    Public Function Notice(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String Implements ISetupServices.Notice
        Dim sql As String = "EXEC  [Setup].[usp_Calendar] @Flag='Notice', @YearID='" & YearId & "', @MonthID='" & MonthId & "', @DayNumber='" & DayNumber & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows

                    returnMsg += "{" & """noticeTitle"" : " & """" & ds.Tables(0).Rows(i).Item("NoticeTitle").ToString() & ""","
                    returnMsg += """noticeDescription"" : " & """" & ds.Tables(0).Rows(i).Item("NoticeDescription").ToString().Trim() & """},"
                    i += 1
                Next


            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += "]"
        Return returnMsg

    End Function


    Public Function StudentTimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String, ByVal searchOption As String, ByVal searchValue As String,
                                     ByVal batchId As String, ByVal semesterId As String) As String Implements ISetupServices.StudentTimeTable
        Dim sql As String = "EXEC  [Setup].[usp_StudentCalendar] @Flag = 't', @YearID = '" & YearId & "', @MonthID = '" & MonthId & "', @DayNumber = '" & DayNumber & "', @SearchValue='" & searchValue & "', @searchOption = '" & searchOption & "', @BatchID = '" & batchId & "', @SemesterID='" & semesterId & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                For Each row As DataRow In ds.Tables(0).Rows

                    returnMsg += "{" & """name"" : " & """" & ds.Tables(0).Rows(i).Item("EmployeeName").ToString() & ""","
                    returnMsg += """weekDay"" : " & """" & ds.Tables(0).Rows(i).Item("WeekDayName").ToString() & ""","
                    returnMsg += """shift"" : " & """" & ds.Tables(0).Rows(i).Item("ShiftName").ToString() & ""","
                    returnMsg += """period"" : " & """" & ds.Tables(0).Rows(i).Item("PeriodName").ToString() & ""","
                    returnMsg += """timeFrom"" : " & """" & ds.Tables(0).Rows(i).Item("TimeFrom").ToString() & ""","
                    returnMsg += """timeTo"" : " & """" & ds.Tables(0).Rows(i).Item("TimeTo").ToString() & ""","
                    returnMsg += """subject"" : " & """" & ds.Tables(0).Rows(i).Item("SubjectTitle").ToString() & ""","
                    returnMsg += """batch"" : " & """" & ds.Tables(0).Rows(i).Item("BatchTitle").ToString() & ""","
                    returnMsg += """semester"" : " & """" & ds.Tables(0).Rows(i).Item("SemesterName").ToString() & ""","
                    returnMsg += """section"" : " & """" & ds.Tables(0).Rows(i).Item("Section").ToString().Trim() & """},"
                    i += 1
                Next

                returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
            End If
        End If

        returnMsg += "]"
        Return returnMsg
    End Function

    Public Function StudentCalendarDays(ByVal currentYear As String, ByVal currentMonth As String, ByVal batchId As String,
                                        ByVal semesterId As String) As String Implements ISetupServices.StudentCalendarDays
        Dim returnDays As String = ""
        Dim sql As String = "EXEC Setup.usp_StudentCalendar @Flag='d', @YearID= '" & currentYear & "', @MonthID='" & currentMonth & "', @BatchID = '" & batchId & "', @SemesterID = '" & semesterId & "'"
        Dim ds As New DataSet
        Try
            Dim i As Integer
            Dim eventSeperator As String = ""
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    returnDays += "{""monthStartDayId"" : """ & ds.Tables(0).Rows(0).Item("monthStartDayId").ToString() & """," &
                        """currentDay"" : """ & ds.Tables(0).Rows(0).Item("currentDay").ToString() & """," & """monthDays"" : ["

                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        eventSeperator = ""
                        returnDays += "{""name"" : """ & ds.Tables(0).Rows(i).Item("DayNumber").ToString() & """, ""events"" : {"

                        Dim eventId As String = ds.Tables(0).Rows(i).Item("EventID").ToString()
                        If eventId <> 0 Or eventId <> "0" Then
                            returnDays += """event"" : """ & ds.Tables(0).Rows(i).Item("EventDescription").ToString() & """"
                            eventSeperator = ","
                        End If

                        'If ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> "" Then
                        '    returnDays += ", ""timeTable"" : """ & ds.Tables(0).Rows(i).Item("").ToString() & ""
                        'End If

                        If ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> "" Then
                            returnDays += eventSeperator & """holiday"" : """ & ds.Tables(0).Rows(i).Item("HolidayName").ToString() & """"
                            eventSeperator = ","
                        End If

                        If ds.Tables(0).Rows(i).Item("ExamTimeTableID").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("ExamTimeTableID").ToString() <> "" Then
                            returnDays += eventSeperator & """examTimeTable"" : """ & ds.Tables(0).Rows(i).Item("ExamTimeTableID").ToString() & """"
                            eventSeperator = ","
                        End If
                        If ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString() <> "" Then
                            returnDays += eventSeperator & """leave"" : """ & ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString() & """"
                            eventSeperator = ","
                        End If
                        If ds.Tables(0).Rows(i).Item("NoticeID").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("NoticeID").ToString() <> "" Then
                            returnDays += eventSeperator & """Notice"" : """ & ds.Tables(0).Rows(i).Item("NoticeID").ToString() & """"
                            eventSeperator = ","
                        End If


                        If ds.Tables(0).Rows(i).Item("TimeTable").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("TimeTable").ToString() <> "" Then
                            returnDays += eventSeperator & """timeTable"" : """ & ds.Tables(0).Rows(i).Item("TimeTable").ToString() & """"
                        End If


                        returnDays += "}},"
                    Next
                    returnDays = returnDays.Substring(0, returnDays.Length - 1) & "]}"
                End If
            End If
        Catch ex As Exception

        End Try


        'If (ds.Tables(0).Rows.Count > 0) Then
        '    returnDays += "{""monthStartDayId"" : """ & ds.Tables(0).Rows(0).Item("monthStartDayId").ToString() & """," &
        '        """currentDay"" : """ & ds.Tables(0).Rows(0).Item("currentDay").ToString() & """," & """monthDays"" :["

        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        returnDays += " """ & ds.Tables(0).Rows(i).Item("DayNumber").ToString() & ""","
        '    Next
        '    returnDays = returnDays.Substring(0, returnDays.Length - 1)
        '    returnDays += "]}"
        'End If
        Return returnDays
    End Function



    Function GetIncomeDetails(ByVal groupId As String, ByVal subgroupId As String, ByVal grouptype As String) As IncomeDetails() Implements ISetupServices.GetIncomeDetails
        Dim sql As String
        Dim ds As New DataSet
        If grouptype.ToLower = "i" Then
            sql = "EXEC Accounts.usp_Reports @Flag='incomeDetail', @GroupID='" & groupId & "', @SubGroupID='" & subgroupId & "'"
        Else
            sql = "EXEC Accounts.usp_Reports @Flag='expenseDetail', @GroupID='" & groupId & "', @SubGroupID='" & subgroupId & "'"
        End If

        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim jsonArray As IncomeDetails() = New IncomeDetails(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            jsonArray(i) = New IncomeDetails()
            jsonArray(i).AccountName = rs("AccountName").ToString
            Dim amt = rs("Amount").ToString()
            If (Not String.IsNullOrWhiteSpace(amt)) Then
                amt = Convert.ToDecimal(amt).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
            End If
            jsonArray(i).Amount = amt
            i = i + 1
        Next
        Return jsonArray
    End Function


    Public Function SaveDeviceUsers(ByVal deviceUser As DeviceUser()) As DeviceUser Implements ISetupServices.SaveDeviceUsers
        'Dim deviceUsr As New DeviceUser
        'For i As Integer = 0 To deviceUser.Count() - 1
        '    Dim sql As String = "EXEC [Attendance].[usp_UserInfo] @Flag='i', @ApplicationId='" & deviceUser(i).ApplicationId & "', @CardNo='" & deviceUser(i).CardNo & "', @FingerPrint='" & deviceUser(i).FingerPrint & "'"
        '    Dim ds As New DataSet
        '    ds = Dao.ExecuteDataset(sql)
        '    If ds.Tables.Count > 0 Then
        '        If ds.Tables(0).Rows.Count > 0 Then
        '            deviceUsr.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        '        End If
        '    End If
        'Next
        'Return deviceUsr
    End Function

    Public Function GetDevice() As String Implements ISetupServices.GetDevice
        'Dim sql As String = "EXEC [Attendance].[usp_Device] @Flag='s'"
        'Dim returnMsg As String = "["
        'Dim ds As New DataSet
        'ds = Dao.ExecuteDataset(sql)
        'If ds.Tables.Count > 0 Then
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
        '            returnMsg += "{" & """deviceId"" : " & """" & ds.Tables(0).Rows(i).Item("DeviceID").ToString() & ""","
        '            returnMsg += """deviceName"" : " & """" & ds.Tables(0).Rows(i).Item("DeviceName").ToString() & ""","
        '            returnMsg += """machineID"" : " & """" & ds.Tables(0).Rows(i).Item("MachineID").ToString() & ""","
        '            returnMsg += """iP"" : " & """" & ds.Tables(0).Rows(i).Item("IP").ToString() & ""","
        '            returnMsg += """port"" : " & """" & ds.Tables(0).Rows(i).Item("Port").ToString() & """},"
        '        Next
        '        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        '    End If
        'End If
        'returnMsg += "]"
        'Return returnMsg
    End Function

    Public Function ConnectDevice(ByVal ip As String, ByVal port As Integer) As Boolean Implements ISetupServices.ConnectDevice
        'Try
        '    isConnected = axCZKEM1.Connect_Net(ip, port)
        'Catch ex As Exception
        '    Return False
        'End Try
        'Return isConnected
    End Function

    Public Function DisconnectDevice(ByVal ip As String, ByVal port As Integer) As Boolean Implements ISetupServices.DisconnectDevice
        'Try
        '    axCZKEM1.Disconnect()
        '    Return True
        'Catch ex As Exception
        '    Return False
        'End Try

    End Function

    Public Function GetDeviceAttendanceRecord(ByVal deviceId As String) As String Implements ISetupServices.GetDeviceAttendanceRecord
        'Dim recCount As Integer = 0
        'Dim sdwEnrollNumber As String = ""
        'Dim idwVerifyMode As Integer
        'Dim idwInOutMode As Integer
        'Dim idwYear As Integer
        'Dim idwMonth As Integer
        'Dim idwDay As Integer
        'Dim idwHour As Integer
        'Dim idwMinute As Integer
        'Dim idwSecond As Integer
        'Dim idwWorkcode As Integer
        'Dim returnMsg As String = ""
        'Try
        '    Dim sql As String = "EXEC [Attendance].[usp_Device] @Flag='s', @DeviceID='" & deviceId & "'"
        '    Dim ds As New DataSet
        '    ds = Dao.ExecuteDataset(sql)
        '    If ds.Tables.Count > 0 Then
        '        If ds.Tables(0).Rows.Count > 0 Then
        '            Dim ip As String = ds.Tables(0).Rows(0).Item("IP").ToString()
        '            Dim port As Integer = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Port").ToString())
        '            If axCZKEM1.Connect_Net(ip, port) Then
        '                axCZKEM1.EnableDevice(deviceId, False)
        '                If axCZKEM1.ReadGeneralLogData(deviceId) Then
        '                    axCZKEM1.ReadAllUserID(deviceId)
        '                    axCZKEM1.ReadAllTemplate(deviceId)
        '                    returnMsg = "["
        '                    While axCZKEM1.SSR_GetGeneralLogData(deviceId, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
        '                        Dim attDateStr As String = idwYear & "/" & idwMonth & "/" & idwDay & " " & idwHour & ":" & idwMinute & ":" & idwSecond
        '                        Dim attDate As DateTime = Convert.ToDateTime(attDateStr)
        '                        returnMsg += "{" & """machineNo"" : " & """" & 1.ToString() & ""","
        '                        returnMsg += """userId"" : " & """" & sdwEnrollNumber.ToString() & ""","
        '                        returnMsg += """Attendance"" : " & """" & attDate.ToString() & ""","

        '                        returnMsg += """workCode"" : " & """" & idwWorkcode.ToString() & """},"
        '                        recCount += 1
        '                    End While
        '                    returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        '                    returnMsg += "]"
        '                End If
        '            End If
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try
        'axCZKEM1.EnableDevice(deviceId, True)

        'Return returnMsg
    End Function

    Public Function GetDeviceUsers(ByVal deviceId As String) As String Implements ISetupServices.GetDeviceUsers
        Dim returnMsg As String = "["
        Dim sdwEnrollNumber As String = ""
        Dim sCardNumber As String = ""
        Dim sName As String = ""
        Dim sPassword As String = ""
        Dim iPrivilege As Integer
        Dim bEnabled As Boolean = False
        Dim idwFingerIndex As Integer
        Dim sTempData As String = ""
        Dim iTmpLenght As Integer
        Dim iFlag As Integer
        Dim sql As String = "EXEC [Attendance].[usp_Device] @Flag='s', @DeviceID='" & deviceId & "'"
        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then

                Dim ip As String = ds.Tables(0).Rows(0).Item("IP").ToString()
                Dim port As Integer = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Port").ToString())

                '    If ConnectDevice(ip, port) Then
                '        axCZKEM1.EnableDevice(deviceId, False)
                '        axCZKEM1.ReadAllUserID(deviceId)
                '        axCZKEM1.ReadAllTemplate(deviceId)


                '        While axCZKEM1.SSR_GetAllUserInfo(deviceId, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) = True
                '            For idwFingerIndex = 0 To 9
                '                If axCZKEM1.GetUserTmpExStr(deviceId, sdwEnrollNumber, idwFingerIndex, iFlag, sTempData, iTmpLenght) Then
                '                    returnMsg += "{" & """deviceId"" : " & """" & deviceId.ToString() & ""","
                '                    returnMsg += """userId"" : " & """" & sdwEnrollNumber.ToString() & ""","
                '                    returnMsg += """userName"" : " & """" & sName.ToString() & ""","
                '                    returnMsg += """passWord"" : " & """" & sPassword.ToString() & ""","
                '                    returnMsg += """privilege"" : " & """" & iPrivilege.ToString() & ""","
                '                    returnMsg += """enabled"" : " & """" & bEnabled.ToString() & ""","
                '                    returnMsg += """fingerIndex"" : " & """" & idwFingerIndex.ToString() & ""","
                '                    returnMsg += """flag"" : " & """" & iFlag.ToString() & ""","
                '                    returnMsg += """tempData"" : " & """" & sTempData.ToString() & ""","
                '                    returnMsg += """tempLength"" : " & """" & iTmpLenght.ToString() & """"
                '                    If axCZKEM1.GetStrCardNumber(sCardNumber) = True Then
                '                        returnMsg += ",""cardNumber"" : " & """" & sCardNumber.ToString() & """"
                '                    Else
                '                        returnMsg += ",""cardNumber"" : " & """" & sCardNumber.ToString() & """"
                '                    End If
                '                    returnMsg += "},"
                '                End If
                '            Next
                '        End While
                '        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
                '        axCZKEM1.EnableDevice(deviceId, True)
                '        axCZKEM1.Disconnect()
                '    End If
            End If
        End If


        returnMsg += "]"

        Return returnMsg
    End Function

    Public Function LiveAttendance() As String

        Return "hello"
    End Function



    Public Function EmployeeAttendance(ByVal empAttendance As EmployeeAttendances()) As EmployeeAttendances Implements ISetupServices.EmployeeAttendance
        'Dim sql As String
        'Dim empAtdev As New EmployeeAttendances
        ''sql = "EXEC [Attendance].[usp_StudentAttendance] @Flag='', @StudentId='" & stdAtt & "', @AttendanceTime='" & attendanceTime & "', @AttendanceModeId='" & attendanceMode & "', @IsPresent='" & isPresent & "'"
        'Dim ds As New DataSet

        'For i As Integer = 0 To empAttendance.Count - 1

        '    Dim value As Date = empAttendance(i).AttendanceTime.ToString
        '    value.ToString("dd/MM/yyyy")
        '    Dim miti As String = eng_to_nep(value.Year, value.Month, value.Day)
        '    sql = "EXEC [Attendance].[usp_AttendanceMain]  @Flag='device',@AttendeeID='" & empAttendance(i).AttendeeId & "', @AttendanceTime='" & empAttendance(i).AttendanceTime & "',@AttendanceModeID='1',@Miti='" & miti & "',@CreatedBy='" & empAttendance(i).sUserId & "'"
        '    ds = Dao.ExecuteDataset(sql)
        'Next
        'If ds.Tables.Count > 0 Then
        '    empAtdev.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        'End If
        'ds.Dispose()
        'Return empAtdev
    End Function


    Public Function StudentAttendance(ByVal stdAtt As StudentAttendances()) As StudentAttendances Implements ISetupServices.StudentAttendance
        Dim s As New StudentAttendances
        Dim sql As String
        'sql = "EXEC [Attendance].[usp_StudentAttendance] @Flag='', @StudentId='" & stdAtt & "', @AttendanceTime='" & attendanceTime & "', @AttendanceModeId='" & attendanceMode & "', @IsPresent='" & isPresent & "'"
        Dim ds As New DataSet
        For i As Integer = 0 To stdAtt.Count - 1
            sql = "EXEC [Attendance].[usp_StudentAttendance] @Flag='i', @StudentId='" & stdAtt(i).StudentId & "', @AttendanceTime='" & stdAtt(i).AttendanceTime & "', @AttendanceModeId='" & stdAtt(i).AttendanceMode & "', @IsPresent='" & stdAtt(i).IsPresent & "'"
            sql += ",@CreatedBy='" & stdAtt(i).sUserId & "',@BranchID='" & stdAtt(i).sBranchId & "'"
            sql += ",@SubjectID='" & stdAtt(i).SubjectID & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        If ds.Tables.Count > 0 Then
            s.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        End If
        ds.Dispose()
        Return s
    End Function

    Public Function StudentAttendanceSubjectwise(ByVal stdAtt As StudentAttendances()) As StudentAttendances Implements ISetupServices.StudentAttendanceSubjectwise
        Dim s As New StudentAttendances
        Dim sql As String
        'sql = "EXEC [Attendance].[usp_StudentAttendance] @Flag='', @StudentId='" & stdAtt & "', @AttendanceTime='" & attendanceTime & "', @AttendanceModeId='" & attendanceMode & "', @IsPresent='" & isPresent & "'"
        Dim ds As New DataSet
        For i As Integer = 0 To stdAtt.Count - 1
            sql = "EXEC [Attendance].[usp_StudentAttendance] @Flag='i', @StudentId='" & stdAtt(i).StudentId & "', @AttendanceTime='" & stdAtt(i).AttendanceTime & "', @AttendanceModeId='" & stdAtt(i).AttendanceMode & "', @IsPresent='" & stdAtt(i).IsPresent & "'"
            sql += ",@CreatedBy='" & stdAtt(i).sUserId & "',@BranchID='" & stdAtt(i).sBranchId & "'"
            sql += ",@SubjectID='" & stdAtt(i).SubjectID & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        If ds.Tables.Count > 0 Then
            s.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        End If
        ds.Dispose()
        Return s
    End Function

    Public Function SaveEmployee(ByVal employeeNo As String,
                          ByVal firstName As String,
                          ByVal lastName As String,
                          ByVal gender As String,
                          ByVal doB As String,
                          ByVal nationality As String,
                          ByVal religion As String,
                          ByVal maritalStatus As String,
                          ByVal spouseName As String,
                          ByVal numberOfChild As String,
                          ByVal fatherName As String,
                          ByVal motherName As String,
                          ByVal email As String,
                          ByVal phone As String,
                          ByVal bloodGroup As String,
                          ByVal webSite As String,
                          ByVal permanentAddress As String,
                          ByVal contactAddress As String,
                          ByVal pan As String,
                          ByVal joiningDate As String,
                          ByVal employeeCategories As String,
                          ByVal department As String,
                          ByVal jobTitleId As String,
                          ByVal workingStatus As String,
                          ByVal shift As String,
                          ByVal isActive As String,
                          ByVal basicSalary As String,
                          ByVal photo As String,
                          ByVal modifiedBy As String,
                          ByVal employeeRole As String,
                          ByVal branchId As String,
                          ByVal jobTitle As String,
                          ByVal experienceDateFrom As String,
                          ByVal experienceDateTo As String,
                          ByVal experienceInstitution As String,
                          ByVal experienceRemarks As String,
                          ByVal awardName As String,
                          ByVal awardDate As String,
                          ByVal awardInstitution As String,
                          ByVal awardRemarks As String,
                          ByVal qualificationName As String,
                          ByVal passedYear As String,
                          ByVal percentage As String,
                          ByVal qualificationInstitution As String,
                          ByVal qualificationRemarks As String,
                          ByVal trainingName As String,
                          ByVal trainingFrom As String,
                          ByVal trainingTo As String,
                          ByVal trainingInstitution As String,
                          ByVal trainingRemarks As String) As String Implements ISetupServices.SaveEmployee
        Dim sql As String
        sql = "EXEC [HR].[usp_CreateEmployee] @Flag='i', @EmployeeNo='" & employeeNo & "', @FirstName='" & firstName & "', @LastName='" & lastName & "', @Gender='" & gender & "', @DOB='" & doB & "', @Nationality='" & nationality & "', @Religion='" & religion & "', @MaritalStatus='" & maritalStatus & "', @SpouseName='" & spouseName & "', @NumberOfChild='" & numberOfChild & "', @FatherName='" & fatherName & "', @MotherName='" & motherName & "', @Email='" & email & "', @Phone='" & phone & "', @BloodGroup='" & bloodGroup & "', @Website='" & webSite & "', @PermanentAddress='" & permanentAddress & "', @ContactAddress='" & contactAddress & "', @PAN='" & pan & "', @JoiningDate='" & joiningDate & "', @EmployeeCategories='" & employeeCategories & "', @Department='" & department & "', @JobTitleID='" & jobTitleId & "', @WorkingStatus='" & workingStatus & "', @Shift='" & shift & "', @IsActive='" & isActive & "', @BasicSalary='" & basicSalary & "', @Photo='" & photo & "', @ModifiedBy='" & modifiedBy & "', @EmployeeRole='" & employeeRole & "', @BranchID='" & branchId & "', @JobTitle='" & jobTitle & "', @ExperienceDateFrom='" & experienceDateFrom & "', @ExperienceDateTo='" & experienceDateTo & "', @ExperienceInstitution='" & experienceInstitution & "', @ExperienceRemarks='" & experienceRemarks & "', @AwardsDate='" & awardDate & "', @AwardsName='" & awardName & "', @AwardInstitution='" & awardInstitution & "', @AwardRemarks='" & awardRemarks & "', @QualificationName='" & qualificationName & "', @PassedYear='" & passedYear & "', @Percentage='" & percentage & "', @QualificationInstitution='" & qualificationInstitution & "', @QualificationRemarks='" & qualificationRemarks & "',  @TrainingName='" & trainingName & "', @TrainingFrom='" & trainingFrom & "', @TrainingTo='" & trainingTo & "', @TrainingInstitution='" & trainingInstitution & "', @TrainingRemarks ='" & trainingRemarks & "'"
        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        End If

        Return "This message is returned from web server"
    End Function

    Public Function CashPayment(ByVal obj As CashPayments()) As CashPayments Implements ISetupServices.CashPayment
        Dim sum As Decimal = 0
        Dim sql As String
        Dim stringQuery As String = "<cashpayment>"
        Dim cp As CashPayments() = New CashPayments(obj.Count()) {}
        Dim cp1 As CashPayments = Nothing
        Dim paymentAccountId, paymentGlobal, paymentName, studentGlobalId, remarks, MonthList, sUserId, sBranchid As String
        Dim ds As New DataSet
        Dim objCount As Integer = 0
        For i As Integer = 0 To obj.Count - 1
            Dim particularAccountId As String = obj(i).ParticularAccountId
            Dim particularAccountName As String = obj(i).ParticularAccountName
            Dim particularAmount As Decimal = obj(i).ParticularAmount
            Dim taxableAmt As Decimal = obj(i).taxableAmt
            Dim particularGlobalId As String = obj(i).ParticularGlobalId
            Dim particularReferenceId As String = obj(i).ParticularReferenceId
            remarks = obj(i).Remarks
            MonthList = obj(i).MonthList
            Dim dr As Decimal = 0
            paymentAccountId = obj(i).PaymentAccountId
            paymentGlobal = obj(i).PaymentGlobalId
            paymentName = obj(i).PaymentName
            studentGlobalId = obj(i).StudentGlobalId
            sUserId = obj(i).sUserId
            sBranchid = obj(i).sBranchid

            If (particularAmount > 0) Then
                If particularAccountName.Contains("Discount") Then
                    dr = particularAmount
                    particularAmount = 0
                End If


                stringQuery += "<row "
                stringQuery += " AccountID =""" & particularAccountId & """"
                stringQuery += " AccountName =""" & particularAccountName & """"
                stringQuery += " CR =""" & particularAmount & """"
                stringQuery += " DR =""" & dr & """"
                stringQuery += " ReferenceID =""" & studentGlobalId & """"
                stringQuery += " GlobalID =""" & particularGlobalId & """"
                stringQuery += " taxableAmt =""" & taxableAmt & """"
                stringQuery += " />"
                sum = sum + particularAmount + taxableAmt - dr
            End If

            ''OLD LOGIC

            ''If (particularAmount > 0) Then
            ''    cp(objCount) = New CashPayments()
            ''    cp(objCount).ParticularAccountName = particularAccountName
            ''    cp(objCount).ParticularAmount = particularAmount
            ''    objCount = objCount + 1
            ''    sum = sum + particularAmount
            ''    ''''''Sql insertion items
            ''    sql = "EXEC Accounts.usp_CashPayment @Flag = 'BillItems', @AccountID = '" & particularAccountId & "', @AccountName = '" & particularAccountName & "', @CR = '" & particularAmount & "', @ReferenceID = '" & studentGlobalId & "', @GlobalID = '" & particularGlobalId & "',@CreatedBy='" & sUserId & "',@BranchID='" & GlobalBranch & "'"
            ''    sql += " ,@taxableAmt=" + Dao.FilterQuote(taxableAmt)
            ''    ds = Dao.ExecuteDataset(sql)
            ''End If
        Next

        stringQuery += "</cashpayment>"

        sql = "EXEC Accounts.usp_CashPayment @Flag = 'BillItems',@CreatedBy='" & sUserId & "',@BranchID='" & sBranchid & "'"
        sql += " ,@AccountID='" & paymentAccountId & "'"
        sql += " ,@AccountName='" & paymentName & " A/C" & "'"
        sql += " ,@xml='" & stringQuery & "'"
        sql += " ,@DR='" & sum & "'"
        sql += " ,@cr='" & sum & "'"
        sql += " ,@ReferenceID='" & studentGlobalId & "'"
        sql += " ,@GlobalID='" & paymentGlobal & "'"
        sql += " ,@StudentGlobalId='" & studentGlobalId & "'"
        sql += " ,@Remarks='" & remarks & "'"
        sql += " ,@MonthList='" & MonthList & "'"

        ds = Dao.ExecuteDataset(sql)

        ''OLD LOGIC
        ''sql = "EXEC Accounts.usp_CashPayment @Flag = 'Payment', @AccountID = '" & paymentAccountId & "', @AccountName = '" & paymentName & " A/C', @DR = '" & sum & "', @ReferenceID = '" & studentGlobalId & "', @GlobalID = '" & paymentGlobal & "', @StudentGlobalId = '" & studentGlobalId & "', @CR = '" & sum & "',@CreatedBy='" & sUserId & "',@BranchID='" & GlobalBranch & "'"
        ''ds = Dao.ExecuteDataset(sql)
        objCount = 0
        cp1 = New CashPayments

        If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
            cp1.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        Else
            'Name, Batch, Sem, Sec, RollNo
            cp1.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
            cp1.Name = ds.Tables(0).Rows(0).Item("Name").ToString()
            cp1.Batch = ds.Tables(0).Rows(0).Item("BatchTitle").ToString()
            cp1.Sem = ds.Tables(0).Rows(0).Item("SemesterName").ToString()
            cp1.Sec = ds.Tables(0).Rows(0).Item("Section").ToString()
            cp1.RollNo = ds.Tables(0).Rows(0).Item("RollNo").ToString()
            cp1.ParticularGlobalId = ds.Tables(0).Rows(0).Item("ID").ToString()
            cp1.PaymentAccountId = ds.Tables(0).Rows(0).Item("RecptNo").ToString()
            '     cp(objCount).Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        End If

        Return cp1
    End Function


    Public Function CashPaymentNew(ByVal sCompanyID As String, ByVal sBranchID As String, ByVal sUserid As String, ByVal obj As CashPaymentsNew(), ByVal ReceiveAmount As String, Optional ByVal latefee As String = "0", Optional ByVal due As String = "0", Optional ByVal schemediscount As String = "0", Optional ByVal ManualBillDate As String = "") As CashPaymentsNew Implements ISetupServices.CashPaymentNew
        Dim sum As Decimal = 0
        Dim sql As String
        Dim stringQuery As String = "<cashpayment>"
        Dim cp As CashPaymentsNew() = New CashPaymentsNew(obj.Count()) {}
        Dim cp1 As CashPaymentsNew = Nothing
        Dim paymentAccountId, paymentGlobal, paymentName, studentGlobalId, remarks, MonthList, MonthIds, SemesterId As String
        Dim ds As New DataSet
        Dim objCount As Integer = 0
        For i As Integer = 0 To obj.Count - 1
            Dim particularAccountId As String = obj(i).ParticularAccountId
            Dim particularAccountName As String = obj(i).ParticularAccountName
            Dim particularAmount As Decimal = obj(i).ParticularAmount
            Dim taxableAmt As Decimal = obj(i).taxableAmt
            Dim particularGlobalId As String = obj(i).ParticularGlobalId
            Dim particularReferenceId As String = obj(i).ParticularReferenceId
            MonthIds = obj(i).MonthIds
            remarks = obj(i).Remarks
            MonthList = obj(i).MonthList
            Dim dr As Decimal = 0
            paymentAccountId = obj(i).PaymentAccountId
            paymentGlobal = obj(i).PaymentGlobalId
            paymentName = obj(i).PaymentName
            studentGlobalId = obj(i).StudentGlobalId
            SemesterId = obj(i).SemesterId

            If (particularAmount > 0) Then
                If particularAccountName.Contains("Discount") Then
                    dr = particularAmount
                    particularAmount = 0
                End If


                stringQuery += "<row "
                stringQuery += " AccountID =""" & particularAccountId & """"
                stringQuery += " AccountName =""" & particularAccountName & """"
                stringQuery += " CR =""" & particularAmount & """"
                stringQuery += " DR =""" & dr & """"
                stringQuery += " ReferenceID =""" & studentGlobalId & """"
                stringQuery += " GlobalID =""" & particularGlobalId & """"
                stringQuery += " taxableAmt =""" & taxableAmt & """"
                stringQuery += " />"
                sum = sum + particularAmount + taxableAmt - dr
            End If

            ''OLD LOGIC

            ''If (particularAmount > 0) Then
            ''    cp(objCount) = New CashPayments()
            ''    cp(objCount).ParticularAccountName = particularAccountName
            ''    cp(objCount).ParticularAmount = particularAmount
            ''    objCount = objCount + 1
            ''    sum = sum + particularAmount
            ''    ''''''Sql insertion items
            ''    sql = "EXEC Accounts.usp_CashPayment @Flag = 'BillItems', @AccountID = '" & particularAccountId & "', @AccountName = '" & particularAccountName & "', @CR = '" & particularAmount & "', @ReferenceID = '" & studentGlobalId & "', @GlobalID = '" & particularGlobalId & "',@CreatedBy='" & sUserId & "',@BranchID='" & GlobalBranch & "'"
            ''    sql += " ,@taxableAmt=" + Dao.FilterQuote(taxableAmt)
            ''    ds = Dao.ExecuteDataset(sql)
            ''End If
        Next

        stringQuery += "</cashpayment>"

        sql = "EXEC Accounts.usp_CashPaymentNew @Flag = 'BillItems',@CreatedBy='" & sUserid & "',@BranchID='" & sBranchID & "'"
        sql += " ,@AccountID='" & paymentAccountId & "'"
        sql += " ,@AccountName='" & paymentName & " A/C" & "'"
        sql += " ,@xml='" & stringQuery & "'"
        sql += " ,@DR='" & ReceiveAmount & "'"
        sql += " ,@cr='" & sum & "'"
        sql += " ,@ReferenceID='" & studentGlobalId & "'"
        sql += " ,@GlobalID='" & paymentGlobal & "'"
        sql += " ,@StudentGlobalId='" & studentGlobalId & "'"
        sql += " ,@Remarks='" & remarks & "'"
        sql += " ,@MonthList='" & MonthList & "'"
        sql += " ,@MonthIds='" & MonthIds & "'"
        sql += " ,@lateFee='" & latefee & "'"
        sql += " ,@due='" & due & "'"
        sql += " ,@schemediscount='" & schemediscount & "'"
        sql += " ,@ManualBillDate='" & ManualBillDate & "'"
        sql += " ,@CompanyID='" & sCompanyID & "'"
        sql += " ,@SemesterID='" & SemesterId & "'"
        ds = Dao.ExecuteDataset(sql)

        ''OLD LOGIC
        ''sql = "EXEC Accounts.usp_CashPayment @Flag = 'Payment', @AccountID = '" & paymentAccountId & "', @AccountName = '" & paymentName & " A/C', @DR = '" & sum & "', @ReferenceID = '" & studentGlobalId & "', @GlobalID = '" & paymentGlobal & "', @StudentGlobalId = '" & studentGlobalId & "', @CR = '" & sum & "',@CreatedBy='" & sUserId & "',@BranchID='" & GlobalBranch & "'"
        ''ds = Dao.ExecuteDataset(sql)
        objCount = 0
        cp1 = New CashPaymentsNew

        If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
            cp1.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        Else
            'Name, Batch, Sem, Sec, RollNo
            cp1.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
            cp1.Name = ds.Tables(0).Rows(0).Item("Name").ToString()
            cp1.Batch = ds.Tables(0).Rows(0).Item("BatchTitle").ToString()
            cp1.Sem = ds.Tables(0).Rows(0).Item("SemesterName").ToString()
            cp1.Sec = ds.Tables(0).Rows(0).Item("Section").ToString()
            cp1.RollNo = ds.Tables(0).Rows(0).Item("RollNo").ToString()
            cp1.ParticularGlobalId = ds.Tables(0).Rows(0).Item("ID").ToString()
            cp1.PaymentAccountId = ds.Tables(0).Rows(0).Item("RecptNo").ToString()
            '     cp(objCount).Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        End If

        Return cp1
    End Function

    Public Function GetIndividualMarksCompareAcrossTerms(ByVal batchId As String, ByVal StudentID As String) As Array Implements ISetupServices.GetIndividualMarksCompareAcrossTerms
        Dim sql As String = "EXEC [Exam].[usp_IndividualMarksCompareAcrossTerms]  @Flag='loadMarks', @BatchId='" & batchId & "', @StudentID='" & StudentID & "'"
        Dim ds As New DataSet

        Dim jsonArray As Array = {Nothing, Nothing, Nothing}

        Dim chartData As String = Nothing
        Dim xAxis As String = Nothing

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count = 0 Then
            sql = "Record not found"
        Else

            sql = "<thead class='flip-content'><tr><th>S.N</th>"

            For index = 1 To ds.Tables(0).Columns.Count - 1
                sql += "<th>" + ds.Tables(0).Columns.Item(index).ToString + "</th>"

            Next
            sql += "</tr> </thead><tbody>"

            For index = 0 To ds.Tables(0).Rows.Count - 1
                sql += "<tr>"
                sql += "<td align='center'>" + index.ToString + "</td>"

                For j = 1 To ds.Tables(0).Columns.Count - 1
                    If j = 1 Then
                        xAxis += """" + ds.Tables(0).Rows(index).Item(j).ToString + ""","
                        sql += "<td>" + ds.Tables(0).Rows(index).Item(j).ToString + "</td>"
                    Else
                        sql += "<td align='center'>" + ds.Tables(0).Rows(index).Item(j).ToString + "</td>"
                    End If

                Next

                sql += "</tr>"
            Next
            sql += "</tbody>"

            For j = 2 To ds.Tables(0).Columns.Count - 1

                chartData += "{""name"" : """ + ds.Tables(0).Columns(j).ToString + ""","

                chartData += """data"":["
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i = ds.Tables(0).Rows.Count - 1 Then
                        chartData += ds.Tables(0).Rows(i).Item(j).ToString
                    Else
                        chartData += ds.Tables(0).Rows(i).Item(j).ToString + ","
                    End If
                Next
                chartData += "]},"
            Next
            chartData = chartData.Substring(0, chartData.Length - 1)
            xAxis = xAxis.Substring(0, xAxis.Length - 1)
        End If

        jsonArray(0) = sql
        Dim result As String = Nothing
        result = "{""xaxis"" : [" + xAxis + "]"
        result += ",""yaxis"" : [" + chartData + "]}"
        jsonArray(1) = result
        'jsonArray(2) = "[" + xAxis + "]"

        Return jsonArray
    End Function

    Public Function GetIndividualMarksCompareAcrossTermsST(ByVal StudentID As String) As Array Implements ISetupServices.GetIndividualMarksCompareAcrossTermsST
        Dim sql As String = "EXEC [Exam].[usp_IndividualMarksCompareAcrossTerms]  @Flag='loadMarksST', @StudentID='" & StudentID & "'"
        Dim ds As New DataSet

        Dim jsonArray As Array = {Nothing, Nothing, Nothing}

        Dim chartData As String = Nothing
        Dim xAxis As String = Nothing

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count = 0 Then
            sql = "Record not found"
        Else

            sql = "<thead class='flip-content'><tr><th>S.N</th>"

            For index = 1 To ds.Tables(0).Columns.Count - 1
                sql += "<th>" + ds.Tables(0).Columns.Item(index).ToString + "</th>"

            Next
            sql += "</tr> </thead><tbody>"

            For index = 0 To ds.Tables(0).Rows.Count - 1
                sql += "<tr>"
                sql += "<td align='center'>" + (index + 1).ToString + "</td>"

                For j = 1 To ds.Tables(0).Columns.Count - 1
                    If j = 1 Then
                        xAxis += """" + ds.Tables(0).Rows(index).Item(j).ToString + ""","
                        sql += "<td>" + ds.Tables(0).Rows(index).Item(j).ToString + "</td>"
                    Else
                        sql += "<td align='center'>" + ds.Tables(0).Rows(index).Item(j).ToString + "</td>"
                    End If

                Next

                sql += "</tr>"
            Next
            sql += "</tbody>"

            For j = 2 To ds.Tables(0).Columns.Count - 1

                chartData += "{""name"" : """ + ds.Tables(0).Columns(j).ToString + ""","

                chartData += """data"":["
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i = ds.Tables(0).Rows.Count - 1 Then
                        chartData += ds.Tables(0).Rows(i).Item(j).ToString
                    Else
                        chartData += ds.Tables(0).Rows(i).Item(j).ToString + ","
                    End If
                Next
                chartData += "]},"
            Next
            chartData = chartData.Substring(0, chartData.Length - 1)
            xAxis = xAxis.Substring(0, xAxis.Length - 1)
        End If

        jsonArray(0) = sql
        Dim result As String = Nothing
        result = "{""xaxis"" : [" + xAxis + "]"
        result += ",""yaxis"" : [" + chartData + "]}"
        jsonArray(1) = result
        'jsonArray(2) = "[" + xAxis + "]"

        Return jsonArray
    End Function
    Public Function GetDivisionComparisonReport(ByVal batchId As String) As Array Implements ISetupServices.GetDivisionComparisonReport
        Dim sql As String = "EXEC [Exam].[usp_DivisionComparison] @BatchId='" & batchId & "'"
        Dim ds As New DataSet

        Dim jsonArray As Array = {Nothing, Nothing}

        Dim html As String = Nothing
        Dim chartData As String = Nothing

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
            html = "Record not found"
        Else

            html = "<thead class='flip-content'><tr><th>S.N</th>"

            For index = 0 To ds.Tables(0).Columns.Count - 2
                html += "<th>" + ds.Tables(0).Columns.Item(index).ToString + "</th>"

            Next
            html += "</tr> </thead><tbody>"

            For index = 0 To ds.Tables(0).Rows.Count - 1
                html += "<tr>"
                html += "<td align='center'>" + (index + 1).ToString + "</td>"

                For j = 0 To ds.Tables(0).Columns.Count - 2
                    html += "<td align='center'>" + ds.Tables(0).Rows(index).Item(j).ToString + "</td>"
                Next
                html += "</tr>"
            Next
            html += "</tbody>"


            chartData += "{""name"" : ""No of Student"",""colorByPoint"":""true"","
            chartData += """data"":["
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = ds.Tables(0).Rows.Count - 1 Then
                    chartData += "{""name"":""" + ds.Tables(0).Rows(i).Item("Division").ToString + """"
                    chartData += ",""y"":" + ds.Tables(0).Rows(i).Item("No of Students").ToString + "}"

                Else
                    chartData += "{""name"":""" + ds.Tables(0).Rows(i).Item("Division").ToString + """"
                    chartData += ",""y"":" + ds.Tables(0).Rows(i).Item("No of Students").ToString + "},"
                End If
            Next
            chartData += "]}"
        End If

        jsonArray(0) = html
        jsonArray(1) = "{""chartData"" : [" + chartData + "]}"

        Return jsonArray
    End Function

    Public Function ComparisonMarksPercentVsStudentNumber(ByVal slabPerentage As String) As Array Implements ISetupServices.ComparisonMarksPercentVsStudentNumber
        Dim sql As String = "EXEC [Exam].[usp_ComparisonMarksPercentVsStudentNumber] @slabPerentage=" & Dao.FilterQuote(slabPerentage)
        Dim ds As New DataSet

        Dim jsonArray As Array = {Nothing, Nothing}

        Dim html As String = Nothing
        Dim chartData As String = Nothing

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count = 0 Then
            html = "Record not found"
        Else

            html = "<thead class='flip-content'><tr><th>S.N</th>"

            For index = 0 To ds.Tables(0).Columns.Count - 1
                If (index = 0) Then
                    html += "<th nowrap='nowrap'>" + ds.Tables(0).Columns.Item(index).ToString + "</th>"
                Else
                    html += "<th>" + ds.Tables(0).Columns.Item(index).ToString + "</th>"
                End If

            Next
            html += "</tr> </thead><tbody>"

            'Dim totalArray() As Integer

            For index = 0 To ds.Tables(0).Rows.Count - 1
                html += "<tr>"
                html += "<td align='center'>" + (index + 1).ToString + "</td>"

                For j = 0 To ds.Tables(0).Columns.Count - 1
                    Dim val As String = "0"
                    If Not IsDBNull(ds.Tables(0).Rows(index).Item(j)) Then
                        val = ds.Tables(0).Rows(index).Item(j)
                    End If
                    html += "<td align='center'>" + val + "</td>"

                    'If j <> 0 Then
                    '    Dim tval As Integer = Convert.ToInt32(val)
                    '    If (Not String.IsNullOrWhiteSpace(totalArray(j - 1))) Then
                    '        tval += totalArray(j - 1)
                    '    End If
                    '    totalArray(j - 1) = tval
                    'End If
                Next
                html += "</tr>"
            Next
            'html += "<tr>"
            'html += "<td align='right'><strong>Total Student</strong></td>"
            'For index = 1 To totalArray.Length
            '    html += "<td align='center'>" + totalArray(index) + "</td>"
            'Next
            'html += "</tr>"


            html += "</tbody>"


            'chartData += "{""name"" : ""Percentage"",""colorByPoint"":""true"","
            'chartData += """data"":["
            'For i = 0 To ds.Tables(0).Rows.Count - 1
            '    If i = ds.Tables(0).Rows.Count - 1 Then
            '        chartData += "{""name"":""" + ds.Tables(0).Rows(i).Item("GradeTitle").ToString + """"
            '        chartData += ",""y"":" + ds.Tables(0).Rows(i).Item("precentage").ToString + "}"

            '    Else
            '        chartData += "{""name"":""" + ds.Tables(0).Rows(i).Item("GradeTitle").ToString + """"
            '        chartData += ",""y"":" + ds.Tables(0).Rows(i).Item("precentage").ToString + "},"
            '    End If
            'Next
            'chartData += "]}"
        End If

        jsonArray(0) = html
        'jsonArray(1) = "{""chartData"" : [" + chartData + "]}"

        Return jsonArray
    End Function


    Public Function GetCategories() As Categories() Implements ISetupServices.GetCategories
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Categories]  @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As Categories() = New Categories(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New Categories()
            JsonArray(i).CategoriesID = rs("CategoriesID").ToString
            JsonArray(i).CategoriesTitle = rs("CategoriesTitle").ToString
            JsonArray(i).Code = rs("Code").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function Calendar() As String Implements ISetupServices.Calendar

        Dim returnMsg As String = "{""weekDays"" : ["
        Dim sql As String
        Dim i As Integer = 0
        sql = "EXEC Setup.usp_Calendar @Flag='c'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If (ds.Tables(0).Rows.Count > 0) Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                returnMsg += "{""id"" : """ & ds.Tables(0).Rows(i).Item("WeekDayID").ToString() & """," &
                        """name"" : """ & ds.Tables(0).Rows(i).Item("WeekDayName").ToString() & """},"
            Next
            returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
            returnMsg += "],"
        End If

        If (ds.Tables(1).Rows.Count > 0) Then
            returnMsg += """months"" : ["
            For i = 0 To ds.Tables(1).Rows.Count - 1
                returnMsg += "{""id"" : """ & ds.Tables(1).Rows(i).Item("MonthID").ToString() & """," &
                        """name"" : """ & ds.Tables(1).Rows(i).Item("MonthName").ToString() & """},"
            Next
            returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
            returnMsg += "],"
        End If

        If (ds.Tables(2).Rows.Count > 0) Then
            returnMsg += """years"" : ["
            For i = 0 To ds.Tables(2).Rows.Count - 1
                returnMsg += "{""id"" : """ & ds.Tables(2).Rows(i).Item("YearID").ToString() & """," &
                        """name"" : """ & ds.Tables(2).Rows(i).Item("YearName").ToString() & """},"
            Next
            returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
            returnMsg += "],"
        End If
        If (ds.Tables(3).Rows.Count > 0) Then
            returnMsg += """currentYearId"" : """ & ds.Tables(3).Rows(0).Item("YearID").ToString() & """,""currentMonthId"" : """ &
                ds.Tables(3).Rows(0).Item("MonthID").ToString() & "" & """}"
        End If


        Return returnMsg
    End Function
    Public Function Months(ByVal yearId As String) As MonthResponse() Implements ISetupServices.Months
        ' Dim yearMonths As MonthResponse() = Nothing
        Dim yearMonths As MonthResponse() = {}
        Dim sql As String = "SELECT MonthId, MonthName from	 Setup.MonthMaster where YearID = '" & yearId & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            yearMonths = New MonthResponse(ds.Tables(0).Rows.Count - 1) {}

            For index = 0 To ds.Tables(0).Rows.Count - 1
                yearMonths(index) = New MonthResponse()
                yearMonths(index).MonthId = ds.Tables(0).Rows(index).Item("MonthId").ToString
                yearMonths(index).MonthName = ds.Tables(0).Rows(index).Item("MonthName").ToString
            Next
            Return yearMonths
        End If
        Return Nothing
    End Function
    Public Function CalendarDays(ByVal currentYear As String, ByVal currentMonth As String) As String Implements ISetupServices.CalendarDays
        Dim returnDays As String = ""
        Dim sql As String = "EXEC Setup.usp_Calendar @Flag='d', @YearID= '" & currentYear & "', @MonthID='" & currentMonth & "'"
        Dim ds As New DataSet
        Try
            Dim i As Integer
            Dim eventSeperator As String = ""
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    returnDays += "{""monthStartDayId"" : """ & ds.Tables(0).Rows(0).Item("monthStartDayId").ToString() & """," &
                        """currentDay"" : """ & ds.Tables(0).Rows(0).Item("currentDay").ToString() & """," & """monthDays"" : ["

                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        eventSeperator = ""
                        returnDays += "{""name"" : """ & ds.Tables(0).Rows(i).Item("DayNumber").ToString() & """, ""events"" : {"

                        If ds.Tables(0).Rows(i).Item("EventID").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("EventID").ToString() <> "" Then
                            returnDays += """event"" : """ & ds.Tables(0).Rows(i).Item("EventDescription").ToString() & """"
                            eventSeperator = ","
                        End If

                        'If ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> "" Then
                        '    returnDays += ", ""timeTable"" : """ & ds.Tables(0).Rows(i).Item("").ToString() & ""
                        'End If

                        If ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("HolidayId").ToString() <> "" Then
                            returnDays += eventSeperator & """holiday"" : """ & ds.Tables(0).Rows(i).Item("HolidayName").ToString() & """"
                            eventSeperator = ","
                        End If

                        If ds.Tables(0).Rows(i).Item("ExamTimeTableID").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("ExamTimeTableID").ToString() <> "" Then
                            returnDays += eventSeperator & """examTimeTable"" : """ & ds.Tables(0).Rows(i).Item("ExamTimeTableID").ToString() & """"
                            eventSeperator = ","
                        End If
                        If ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString() <> "" Then
                            returnDays += eventSeperator & """leave"" : """ & ds.Tables(0).Rows(i).Item("LeaveAssignmentID").ToString() & """"
                            eventSeperator = ","
                        End If

                        If ds.Tables(0).Rows(i).Item("TimeTable").ToString() <> Nothing Or ds.Tables(0).Rows(i).Item("TimeTable").ToString() <> "" Then
                            returnDays += eventSeperator & """timeTable"" : """ & ds.Tables(0).Rows(i).Item("TimeTable").ToString() & """"
                        End If


                        returnDays += "}},"
                    Next
                    returnDays = returnDays.Substring(0, returnDays.Length - 1) & "]}"
                End If
            End If
        Catch ex As Exception

        End Try


        'If (ds.Tables(0).Rows.Count > 0) Then
        '    returnDays += "{""monthStartDayId"" : """ & ds.Tables(0).Rows(0).Item("monthStartDayId").ToString() & """," &
        '        """currentDay"" : """ & ds.Tables(0).Rows(0).Item("currentDay").ToString() & """," & """monthDays"" :["

        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        returnDays += " """ & ds.Tables(0).Rows(i).Item("DayNumber").ToString() & ""","
        '    Next
        '    returnDays = returnDays.Substring(0, returnDays.Length - 1)
        '    returnDays += "]}"
        'End If
        Return returnDays
    End Function

    Public Function CalendarEvent(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String Implements ISetupServices.CalendarEvent
        Dim sql As String = "EXEC  [Setup].[usp_Calendar] @Flag='e', @YearID='" & YearId & "', @MonthID='" & MonthId & "', @DayNumber='" & DayNumber & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    returnMsg += "{" & """dayEvent"" : " & """" & ds.Tables(0).Rows(i).Item("EventDescription").ToString() & ""","
                    returnMsg += """dateFrom"" : " & """" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("DateFrom")).ToString("dd MMM yyyy") & ""","
                    returnMsg += """dateTo"" : " & """" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("DateTo")).ToString("dd MMM yyyy") & ""","
                    returnMsg += """timeFrom"" : " & """" & ds.Tables(0).Rows(i).Item("TimeFrom").ToString() & ""","
                    returnMsg += """TimeTo"" : " & """" & ds.Tables(0).Rows(i).Item("TimeTo").ToString().Trim() & ""","
                    returnMsg += """batchTitle"" : " & """" & ds.Tables(0).Rows(i).Item("BatchTitle").ToString() & ""","
                    returnMsg += """semesterName"" : " & """" & ds.Tables(0).Rows(i).Item("SemesterName").ToString() & ""","
                    returnMsg += """section"" : " & """" & ds.Tables(0).Rows(i).Item("Section").ToString().Trim() & """},"
                    i += 1
                Next


            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += "]"
        Return returnMsg
    End Function

    Public Function TimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String, ByVal searchOption As String, ByVal searchValue As String) As String Implements ISetupServices.TimeTable
        Dim sql As String = "EXEC  [Setup].[usp_Calendar] @Flag='t', @YearID='" & YearId & "', @MonthID='" & MonthId & "', @DayNumber='" & DayNumber & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows

                    returnMsg += "{" & """name"" : " & """" & ds.Tables(0).Rows(i).Item("EmployeeName").ToString() & ""","
                    returnMsg += """weekDay"" : " & """" & ds.Tables(0).Rows(i).Item("WeekDayName").ToString() & ""","
                    returnMsg += """shift"" : " & """" & ds.Tables(0).Rows(i).Item("ShiftName").ToString() & ""","
                    returnMsg += """period"" : " & """" & ds.Tables(0).Rows(i).Item("PeriodName").ToString() & ""","
                    returnMsg += """timeFrom"" : " & """" & ds.Tables(0).Rows(i).Item("TimeFrom").ToString() & ""","
                    returnMsg += """timeTo"" : " & """" & ds.Tables(0).Rows(i).Item("TimeTo").ToString() & ""","
                    returnMsg += """subject"" : " & """" & ds.Tables(0).Rows(i).Item("SubjectTitle").ToString() & ""","
                    returnMsg += """batch"" : " & """" & ds.Tables(0).Rows(i).Item("BatchTitle").ToString() & ""","
                    returnMsg += """semester"" : " & """" & ds.Tables(0).Rows(i).Item("SemesterName").ToString() & ""","
                    returnMsg += """section"" : " & """" & ds.Tables(0).Rows(i).Item("Section").ToString().Trim() & """},"
                    i += 1
                Next


            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += "]"
        Return returnMsg
    End Function

    Public Function Holiday(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String Implements ISetupServices.Holiday
        Dim sql As String = "EXEC  [Setup].[usp_Calendar] @Flag='h', @YearID='" & YearId & "', @MonthID='" & MonthId & "', @DayNumber='" & DayNumber & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = ""
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                returnMsg += "[{" & """holiday"" : " & """" & ds.Tables(0).Rows(0).Item("HolidayName").ToString() & """}]"
            End If
        End If
        Return returnMsg
    End Function

    Public Function Leave(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String Implements ISetupServices.Leave
        Dim sql As String = "EXEC  [Setup].[usp_Calendar] @Flag='l', @YearID='" & YearId & "', @MonthID='" & MonthId & "', @DayNumber='" & DayNumber & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    returnMsg += "{" & """employeeName"" : " & """" & ds.Tables(0).Rows(i).Item("EmployeeName").ToString() & ""","
                    returnMsg += """leaveType"" : " & """" & ds.Tables(0).Rows(i).Item("LeaveType").ToString() & ""","
                    returnMsg += """leaveName"" : " & """" & ds.Tables(0).Rows(i).Item("LeaveName").ToString() & ""","
                    returnMsg += """dateFrom"" : " & """" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("DateFrom")).ToString("dd MMM yyyy") & ""","
                    returnMsg += """dateTo"" : " & """" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("DateTo")).ToString("dd MMM yyyy") & ""","
                    returnMsg += """totalDays"" : " & """" & ds.Tables(0).Rows(i).Item("TotalDays").ToString() & ""","
                    returnMsg += """reason"" : " & """" & ds.Tables(0).Rows(i).Item("Reason").ToString().Trim() & """},"
                    i += 1
                Next
            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1)
        returnMsg += "]"
        Return returnMsg
    End Function

    Public Function ExamTimeTable(ByVal DayNumber As String, ByVal YearId As String, ByVal MonthId As String) As String Implements ISetupServices.ExamTimeTable
        Dim sql As String = "EXEC  [Setup].[usp_Calendar] @Flag='exam', @YearID='" & YearId & "', @MonthID='" & MonthId & "', @DayNumber='" & DayNumber & "'"
        Dim ds As New DataSet
        Dim returnMsg As String = "["
        ds = Dao.ExecuteDataset(sql)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    returnMsg += "{" & """batchTitle"" : " & """" & ds.Tables(0).Rows(i).Item("BatchTitle").ToString() & ""","
                    returnMsg += """subjectTitle"" : " & """" & ds.Tables(0).Rows(i).Item("SubjectTitle").ToString() & ""","
                    returnMsg += """date"" : " & """" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("Date")).ToString("dd MMM yyyy") & ""","
                    returnMsg += """timeFrom"" : " & """" & ds.Tables(0).Rows(i).Item("TimeFrom").ToString() & ""","
                    returnMsg += """timeTo"" : " & """" & ds.Tables(0).Rows(i).Item("TimeTo").ToString().Trim() & """},"
                    i += 1
                Next
            End If
        End If
        returnMsg = returnMsg.Substring(0, returnMsg.Length - 1) & "]"
        Return returnMsg


    End Function

    Public Function TimeTableFilterOptions(ByVal filterOption As String) As String Implements ISetupServices.TimeTableFilterOptions
        Dim sql As String
        Dim returnMsg As String = ""
        sql = "EXEC [Setup].[usp_Calendar] @Flag = '" & filterOption & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                returnMsg &= "["
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    returnMsg &= "{""id"":" & """" & ds.Tables(0).Rows(i).Item(0).ToString() & ""","
                    returnMsg &= """name"":" & """" & ds.Tables(0).Rows(i).Item(1).ToString() & """},"
                Next
                returnMsg = returnMsg.Substring(0, returnMsg.Length - 1) + "]"
            End If
        End If
        Return returnMsg
    End Function


    Function GetFaculty(ByVal sBranchID As String) As FacultyParameter() Implements ISetupServices.GetFaculty
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Faculty @flag='p' ,@BranchID='" & sBranchID & "'"

        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As FacultyParameter() = New FacultyParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New FacultyParameter()
            JsonArray(i).FacultyID = rs("FacultyID").ToString
            JsonArray(i).FacultyTitle = rs("FacultyTitle").ToString
            JsonArray(i).FacultyCode = rs("FacultyCode").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetAccountUnder() As AccountUnder() Implements ISetupServices.GetAccountUnder
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Accounts.usp_SubGroup @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As AccountUnder() = New AccountUnder(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New AccountUnder()
            JsonArray(i).SubGroupID = rs("SubGroupID").ToString
            JsonArray(i).GroupID = rs("GroupID").ToString
            JsonArray(i).PID = rs("PID").ToString
            JsonArray(i).SubGroupName = rs("SubGroupName").ToString
            JsonArray(i).Description = rs("Description").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function



    Function GetProgrammeAppliedFor() As ProgrammeAppliedFor() Implements ISetupServices.GetProgrammeAppliedFor
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Course @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ProgrammeAppliedFor() = New ProgrammeAppliedFor(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ProgrammeAppliedFor()
            JsonArray(i).CourseID = rs("CourseID").ToString
            JsonArray(i).CourseTitle = rs("CourseTitle").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetAcademicYear() As AcademicYearParameter() Implements ISetupServices.GetAcademicYear
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_AcademicYear @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As AcademicYearParameter() = New AcademicYearParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New AcademicYearParameter()
            JsonArray(i).AcademicYearID = rs("AcademicYearID").ToString
            JsonArray(i).AcademicTitle = rs("AcademicTitle").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function GetBatch(ByVal sBranchID As String) As BatchParameter() Implements ISetupServices.GetBatch
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Batch @flag='p',@BranchID='" & sBranchID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As BatchParameter() = New BatchParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New BatchParameter()
            JsonArray(i).BatchID = rs("BatchID").ToString
            JsonArray(i).BatchTitle = rs("BatchTitle").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function GetParticulars_Batch_SemWise(ByVal BatchID As String, ByVal SemesterID As String) As ParticularParameter() Implements ISetupServices.GetParticulars_Batch_SemWise
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_CreditBillParticulars] @flag='c',@BatchID='" & BatchID & "',@SemesterID='" & SemesterID & "' "
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ParticularParameter() = New ParticularParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ParticularParameter()
            JsonArray(i).CreditBillParticularID = rs("CreditBillParticularID").ToString
            JsonArray(i).ParticularName = rs("ParticularName").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function


    Function GetAdmissionStatus() As AdmissionStatusParameter() Implements ISetupServices.GetAdmissionStatus
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_AdmissionStatus @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As AdmissionStatusParameter() = New AdmissionStatusParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New AdmissionStatusParameter()
            JsonArray(i).AdmissionStatusID = rs("AdmissionStatusID").ToString
            JsonArray(i).AdmissionEligibleStatusTitle = rs("AdmissionEligibleStatusTitle").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function


    Function GetBoard() As BoardParameter() Implements ISetupServices.GetBoard
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Board @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As BoardParameter() = New BoardParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New BoardParameter()
            JsonArray(i).BoardID = rs("BoardID").ToString
            JsonArray(i).BoardTitle = rs("BoardTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function


    Function GetGrade() As GradeParameter() Implements ISetupServices.GetGrade
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Grade @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As GradeParameter() = New GradeParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New GradeParameter()
            JsonArray(i).GradeID = rs("GradeID").ToString
            JsonArray(i).GradeTitle = rs("GradeTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetLanguage() As LanguageParameter() Implements ISetupServices.GetLanguage
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Language @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As LanguageParameter() = New LanguageParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New LanguageParameter()
            JsonArray(i).LanguageID = rs("LanguageID").ToString
            JsonArray(i).Language = rs("Language").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetLevel() As LevelParameter() Implements ISetupServices.GetLevel
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Level @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As LevelParameter() = New LevelParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New LevelParameter()
            JsonArray(i).LevelID = rs("LevelID").ToString
            JsonArray(i).LevelTitle = rs("LevelTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetReligious() As ReligiousParameter() Implements ISetupServices.GetReligious
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Religious @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ReligiousParameter() = New ReligiousParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ReligiousParameter()
            JsonArray(i).ReligiousID = rs("ReligiousID").ToString
            JsonArray(i).ReligiousName = rs("ReligiousName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetStream() As StreamParameter() Implements ISetupServices.GetStream
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_Stream @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As StreamParameter() = New StreamParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New StreamParameter()
            JsonArray(i).StreamID = rs("StreamID").ToString
            JsonArray(i).StreamTitle = rs("StreamTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetFeesCategories() As FeesCategoriesParameter() Implements ISetupServices.GetFeesCategories
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_FeesCategoriesAppliedToBatch] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As FeesCategoriesParameter() = New FeesCategoriesParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New FeesCategoriesParameter()
            JsonArray(i).FeesCategoriesID = rs("FeesCategoriesID").ToString
            JsonArray(i).FeesCategoriesName = rs("FeesCategoriesName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function SubjectDelete(ByVal SubjectID As String) As Message() Implements ISetupServices.SubjectDelete
        Dim sql As String
        sql = "exec Setup.usp_Subject @flag='d' ,@SubjectID=" & SubjectID
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim sb As StringBuilder = New StringBuilder("")
        Dim i As Integer = 0
        Dim JsonArray As Message() = New Message(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New Message()
            JsonArray(i).messages = ds.Tables(0).Rows(0).Item("mes").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function


    Function AddLocalResource(ByVal counterID As String, ByVal resourceType As String, ByVal resourceFor As String, ByVal resourceName As String) As LocalResource() Implements ISetupServices.LocalResource
        Dim sql As String
        sql = "EXEC [HR].[usp_LocalResources] @Flag='i', @CounterId='" & counterID & "',@ResourceType ='" & resourceType & "', @ResourceUsedFor='" & resourceFor & "',@ResourceName ='" & resourceName & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim sb As StringBuilder = New StringBuilder("")
        Dim i As Integer = 0
        Dim JsonArray As LocalResource() = New LocalResource(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New LocalResource()
            JsonArray(i).mes = ds.Tables(0).Rows(0).Item("mes").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetEmployeeCategories() As EmployeeCategoriesParameter() Implements ISetupServices.GetEmployeeCategories
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [HR].[usp_EmployeeCategories] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As EmployeeCategoriesParameter() = New EmployeeCategoriesParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New EmployeeCategoriesParameter()
            JsonArray(i).EmployeeCategoriesID = rs("EmployeeCategoriesID").ToString
            JsonArray(i).EmployeeCategoriesName = rs("EmployeeCategoriesName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetDepartment() As DepartmentParameter() Implements ISetupServices.GetDepartment
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [HR].[usp_Department] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As DepartmentParameter() = New DepartmentParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New DepartmentParameter()
            JsonArray(i).DepartmentID = rs("DepartmentID").ToString
            JsonArray(i).DepartmentName = rs("DepartmentName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetJobTitle(ByVal sBranchID As String) As JobTitleParameter() Implements ISetupServices.GetJobTitle
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [HR].[usp_JobTitle] @flag='s',@BranchID='" & sBranchID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As JobTitleParameter() = New JobTitleParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New JobTitleParameter()
            JsonArray(i).JobTitleCode = rs("JobTitleCode").ToString
            JsonArray(i).JobTitleName = rs("JobTitleName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetShift() As ShiftParameter() Implements ISetupServices.GetShift
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [HR].[usp_Shift] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ShiftParameter() = New ShiftParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ShiftParameter()
            JsonArray(i).ShiftID = rs("ShiftID").ToString
            JsonArray(i).ShiftName = rs("ShiftName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetSubject() As SubjectParameter() Implements ISetupServices.GetSubject
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Subject] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As SubjectParameter() = New SubjectParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New SubjectParameter()
            JsonArray(i).SubjectID = rs("SubjectID").ToString
            JsonArray(i).SubjectTitle = rs("SubjectTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function ExamAttendance(ByVal obj As StudentExamAttendance()) As StudentExamAttendance Implements ISetupServices.ExamAttendance
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            ds.Clear()
            Dim studentId As String = obj(i).StudentId
            Dim attendanceDays As String = obj(i).AttendanceDays
            Dim totalSchoolDays As String = obj(i).TotalSchoolDays
            Dim examId As String = obj(i).ExamId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim sectionId As String = obj(i).SectionId
            Dim sql As String = "EXEC Exam.usp_Attendance @Flag='i', @ExamID = '" & examId & "', @StudentId = '" & studentId & "', @TotalSchoolDays = '" & totalSchoolDays & "', @AttendedDays = '" & attendanceDays & "', @BatchId = '" & batchId & "', @SemesterId = '" & semesterId & "', @SectionId = '" & sectionId & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        Dim JsonArray As New StudentExamAttendance
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function
    Public Function ExamSubjectAttendance(ByVal obj As StudentExamSubjectAttendance()) As StudentExamSubjectAttendance Implements ISetupServices.ExamSubjectAttendance
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            ds.Clear()
            Dim studentId As String = obj(i).StudentId
            Dim Ispresent As String = obj(i).isPresent
            Dim isPresentP As String = obj(i).isPresentP
            Dim SubjectID As String = obj(i).SubjectID
            Dim examId As String = obj(i).ExamId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim sectionId As String = obj(i).SectionId
            Dim sUserId As String = obj(i).sUserID
            Dim branchId As String = obj(i).BranchID

            Dim sql As String = "EXEC [Exam].[usp_ExamSubjectAttendance]  @Flag='i', @ExamID = '" & examId & "', @StudentId = '" & studentId & "', @Ispresent = '" & Ispresent & "', @IspresentP = '" & isPresentP & "',@SubjectID='" & SubjectID & "', @UserID = '" & sUserId & "', @BatchId = '" & batchId & "', @SemesterId = '" & semesterId & "', @SectionId = '" & sectionId & "'"
            sql += ",@CreatedBy='" & sUserId & "',@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        Dim JsonArray As New StudentExamSubjectAttendance
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function ExamSeminar(ByVal obj As PUSeminarMarks()) As PUSeminarMarks Implements ISetupServices.ExamSeminar
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            ds.Clear()
            Dim studentId As String = obj(i).StudentId
            Dim Ispresent As String = obj(i).marks
            Dim SubjectID As String = obj(i).SubjectID
            Dim examId As String = obj(i).ExamId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim sectionId As String = obj(i).SectionId
            Dim sUserId As String = obj(i).sUserID
            Dim branchId As String = obj(i).BranchID

            Dim sql As String = "EXEC [Exam].[usp_SeminarPU]  @Flag='i', @ExamID = '" & examId & "', @StudentId = '" & studentId & "', @marks = '" & Ispresent & "',@SubjectID='" & SubjectID & "', @BatchId = '" & batchId & "', @SemesterId = '" & semesterId & "', @SectionId = '" & sectionId & "'"
            sql += ",@UserID='" & sUserId & "',@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        Dim JsonArray As New PUSeminarMarks
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function


    Public Function TeacerEvaluation(ByVal obj As PUSeminarMarks()) As PUSeminarMarks Implements ISetupServices.TeacerEvaluation
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            ds.Clear()
            Dim studentId As String = obj(i).StudentId
            Dim Ispresent As String = obj(i).marks
            Dim SubjectID As String = obj(i).SubjectID
            Dim examId As String = obj(i).ExamId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim sectionId As String = obj(i).SectionId
            Dim sUserId As String = obj(i).sUserID
            Dim branchId As String = obj(i).BranchID

            Dim sql As String = "EXEC exam.usp_TeacherEvaluation  @Flag='i', @ExamID = '" & examId & "', @StudentId = '" & studentId & "', @marks = '" & Ispresent & "',@SubjectID='" & SubjectID & "', @BatchId = '" & batchId & "', @SemesterId = '" & semesterId & "', @SectionId = '" & sectionId & "'"
            sql += ",@UserID='" & sUserId & "',@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        Dim JsonArray As New PUSeminarMarks
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function PractialEvaluation(ByVal obj As PUSeminarMarks()) As PUSeminarMarks Implements ISetupServices.PractialEvaluation
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            ds.Clear()
            Dim studentId As String = obj(i).StudentId
            Dim Ispresent As String = obj(i).marks
            Dim SubjectID As String = obj(i).SubjectID
            Dim examId As String = obj(i).ExamId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim sectionId As String = obj(i).SectionId
            Dim sUserId As String = obj(i).sUserID
            Dim branchId As String = obj(i).BranchID
            Dim Pra_Evl_ID As String = obj(i).Pra_Evl_ID

            Dim sql As String = "EXEC exam.usp_PracticalEvaluation  @Flag='i',@Pra_Evl_ID='" & Pra_Evl_ID & "', @ExamID = '" & examId & "', @StudentId = '" & studentId & "', @marks = '" & Ispresent & "',@SubjectID='" & SubjectID & "', @BatchId = '" & batchId & "', @SemesterId = '" & semesterId & "'"
            sql += ",@UserID='" & sUserId & "',@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        Dim JsonArray As New PUSeminarMarks
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function ExamWeight(ByVal obj As StudentExamWeight()) As StudentExamWeight Implements ISetupServices.ExamWeight
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            ds.Clear()
            Dim studentId As String = obj(i).StudentId
            Dim weight As String = obj(i).Weight
            Dim examId As String = obj(i).ExamId
            Dim batchId As String = obj(i).BatchId
            Dim sectionId As String = obj(i).SectionId
            Dim sql As String = "EXEC Exam.ups_Weight @Flag='i', @ExamID = '" & examId & "', @StudentId = '" & studentId & "', @Weight = '" & weight & "', @BatchId = '" & batchId & "', @SectionId = '" & sectionId & "'"
            ds = Dao.ExecuteDataset(sql)
        Next
        Dim JsonArray As New StudentExamWeight
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function GetExamAttendance(ByVal examId As String, ByVal batchId As String, ByVal sortBy As String,
                                      ByVal semesterId As String, ByVal sectionId As String) As StudentExamAttendance() Implements ISetupServices.GetExamAttendance
        Dim sql As String
        sql = "EXEC Exam.usp_Attendance @Flag = '" & sortBy & "', @ExamID = '" & examId & "', @BatchId = '" & batchId & "', @SemesterId = '" & semesterId & "', @SectionId = '" & sectionId & "'"
        Dim ds As New DataSet
        Dim jsonArray As StudentExamAttendance()
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            jsonArray = New StudentExamAttendance(ds.Tables(0).Rows.Count - 1) {}
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                jsonArray(i) = New StudentExamAttendance()
                jsonArray(i).StudentId = ds.Tables(0).Rows(i).Item("StudentId").ToString()
                jsonArray(i).StudentName = ds.Tables(0).Rows(i).Item("StudentName").ToString()
                jsonArray(i).TotalSchoolDays = ds.Tables(0).Rows(i).Item("TotalSchoolDays").ToString()
                jsonArray(i).AttendanceDays = ds.Tables(0).Rows(i).Item("AttendedDays").ToString()
                jsonArray(i).AttendanceId = ds.Tables(0).Rows(i).Item("AttendanceId").ToString()
                jsonArray(i).RollNo = ds.Tables(0).Rows(i).Item("RollNo").ToString()
            Next
            Return jsonArray
        End If
        Return Nothing
    End Function

    Public Function UpdateAttendance(ByVal attendanceId As String, ByVal studentId As String, ByVal attendance As String) As String Implements ISetupServices.UpdateAttendance
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_Attendance] @Flag='u', @AttendanceId = '" & attendanceId & "', @StudentId = '" & studentId &
                            "', @AttendedDays = '" & attendance & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateExamSubjectAttendanceTh(ByVal EditID As String, ByVal Ispresent As String, ByVal UserID As String) As String Implements ISetupServices.UpdateExamSubjectAttendanceTh
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_editExamSubjectAttendance] @Flag='UpdateTheory', @Ispresent = '" & Ispresent & "', @EditID = '" & EditID & "'"
        sql += ",@ModifiedBy='" & UserID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateExamSubjectAttendancePr(ByVal EditID As String, ByVal Ispresent As String, ByVal UserID As String) As String Implements ISetupServices.UpdateExamSubjectAttendancePr
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_editExamSubjectAttendance] @Flag='UpdatePratical', @Ispresent = '" & Ispresent & "', @EditID = '" & EditID & "'"
        sql += ",@ModifiedBy='" & UserID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Function GetSubjectAllotment() As SubjectAllotmentParameter() Implements ISetupServices.GetSubjectAllotment
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_SubjectAllotment]  @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As SubjectAllotmentParameter() = New SubjectAllotmentParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New SubjectAllotmentParameter()
            JsonArray(i).SubjectAllotmentID = rs("SubjectAllotmentID").ToString
            JsonArray(i).SubjectName = rs("SubjectName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetTeacherAllotment() As TeacherAllotmentParameter() Implements ISetupServices.GetTeacherAllotment
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_TeacherAllotment] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As TeacherAllotmentParameter() = New TeacherAllotmentParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New TeacherAllotmentParameter()
            JsonArray(i).TeacherAllotmentID = rs("TeacherAllotmentID").ToString
            JsonArray(i).EmployeeName = rs("EmployeeName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetWeekDaysAllotment() As WeekDaysAllotmentParameter() Implements ISetupServices.GetWeekDaysAllotment
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_WeekDaysAllotment] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As WeekDaysAllotmentParameter() = New WeekDaysAllotmentParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New WeekDaysAllotmentParameter()
            JsonArray(i).WeekDaysAllotmentID = rs("WeekDaysAllotmentID").ToString
            JsonArray(i).NameOfWeek = rs("NameOfWeek").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetEmployee() As EmployeeParameter() Implements ISetupServices.GetEmployee
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [HR].[usp_EmployeeInfo] @flag='b'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As EmployeeParameter() = New EmployeeParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New EmployeeParameter()
            JsonArray(i).EmployeeID = rs("EmployeeID").ToString
            JsonArray(i).FullName = rs("FirstName").ToString & " " & rs("LastName").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetStudentWithoutAccount() As StudentsWithouAccount() Implements ISetupServices.GetStudentsWithoutAccount

        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Students.usp_StudentsWithoutAccount"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As StudentsWithouAccount() = New StudentsWithouAccount(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New StudentsWithouAccount()
            JsonArray(i).GlobalId = rs("GlobalID").ToString()
            JsonArray(i).StudentName = rs("StudentName").ToString()
            JsonArray(i).StudentAccounts = rs("StudentAccouts").ToString()
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetLeave() As LeavePatameter() Implements ISetupServices.GetLeave
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_LeaveType] @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As LeavePatameter() = New LeavePatameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New LeavePatameter()
            JsonArray(i).LeaveTypeID = rs("LeaveTypeID").ToString
            JsonArray(i).LeaveType = rs("LeaveType").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetComplaintType() As ComplainTypeParameter() Implements ISetupServices.GetComplaintType
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_ComplaintType]  @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ComplainTypeParameter() = New ComplainTypeParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ComplainTypeParameter()
            JsonArray(i).ComplaintTypeID = rs("ComplaintTypeID").ToString
            JsonArray(i).ComplaintTypeName = rs("ComplaintTypeName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetDisciplineCategories() As DisciplineCategoriesParameter() Implements ISetupServices.GetDisciplineCategories
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_DisciplineCategories]  @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As DisciplineCategoriesParameter() = New DisciplineCategoriesParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New DisciplineCategoriesParameter()
            JsonArray(i).DisciplineCategoriesID = rs("DisciplineCategoriesID").ToString
            JsonArray(i).DisciplineCategoriesName = rs("DisciplineCategoriesName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetStudent() As StudentParameter() Implements ISetupServices.GetStudent
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Students].[usp_GetStudentsDetails]  @flag='b'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As StudentParameter() = New StudentParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New StudentParameter()
            JsonArray(i).StudentID = rs("StudentID").ToString
            JsonArray(i).StudentName = rs("FirstName").ToString & " " & rs("MiddleName").ToString & " " & rs("LastName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetYear(ByVal sBranchID As String) As YearParameter() Implements ISetupServices.GetYear
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_YearMaster]  @flag='s',@BranchID='" & sBranchID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As YearParameter() = New YearParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New YearParameter()
            JsonArray(i).YearID = rs("YearID").ToString
            JsonArray(i).YearName = rs("YearName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetFiscalYear(ByVal sBranchID As String) As FiscalYearParameter() Implements ISetupServices.GetFiscalYear
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_FiscalYearMaster]  @flag='s',@BranchID='" & sBranchID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As FiscalYearParameter() = New FiscalYearParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New FiscalYearParameter()
            JsonArray(i).FiscalYearID = rs("FiscalYearID").ToString
            JsonArray(i).FiscalYearName = rs("FiscalYearName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetBusStation() As BusStationParameter() Implements ISetupServices.GetBusStation
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_BusStation]   @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As BusStationParameter() = New BusStationParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New BusStationParameter()
            JsonArray(i).BusStationID = rs("BusStationID").ToString
            JsonArray(i).BusStationName = rs("BusStationName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function StudentLogin(ByVal username As String, ByVal password As String) As LoginMessage() Implements ISetupServices.StudentLogin
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Students].[usp_StudentLogin] @flag='s', @username='" & username & "', @password='" & password & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim returnMsg As LoginMessage() = New LoginMessage(ds.Tables(0).Rows.Count) {}
        Dim i As Integer = ds.Tables(0).Rows.Count
        If i > 0 Then
            i = 0

            For Each rows As DataRow In ds.Tables(0).Rows
                returnMsg(i) = New LoginMessage()
                returnMsg(i).message = rows("mes").ToString.Trim()
                returnMsg(i).errorcode = rows("errorcode").ToString()
            Next

        End If
        Return returnMsg
    End Function

    Function UserLogin(ByVal username As String, ByVal password As String, Optional ByVal db_connectionString As String = "") As LoginMessage() Implements ISetupServices.UserLogin
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_UserInfo]  @flag='l', @UserName='" & username & "', @Password='" & password & "'" ',@FiscalYearID='" & academicyear & "'"
        Dim returnMsg As LoginMessage()
        Try
            Global_Active_db_fy = db_connectionString
            ds = Dao.ExecuteDataset(sql)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("errorCode").ToString = "0" Then


                    Dim i As Integer = ds.Tables(0).Rows.Count
                    If i > 0 Then
                        returnMsg = New LoginMessage(ds.Tables(0).Rows.Count) {}
                        i = 0

                        For Each rows As DataRow In ds.Tables(0).Rows
                            returnMsg(i) = New LoginMessage()
                            returnMsg(i).message = rows("mes").ToString.Trim()
                            returnMsg(i).role = rows("Role").ToString.Trim()
                            returnMsg(i).branch = rows("Branch").ToString.Trim()
                            returnMsg(i).errorcode = rows("errorcode").ToString()
                            returnMsg(i).userid = rows("UserID").ToString()
                            returnMsg(i).Type = rows("Category").ToString()
                            ' returnMsg(i).YearID = rows("YearID").ToString
                            returnMsg(i).DatabaseName = rows("DatabaseName").ToString
                            ' returnMsg(i).FiscalYearID = rows("FiscalYearID").ToString
                            returnMsg(i).CompanyID = rows("CompanyID").ToString.Trim()
                            If ds.Tables.Count > 1 Then


                                If (ds.Tables(1).Rows.Count > 0) Then
                                    If (String.IsNullOrWhiteSpace(ds.Tables(1).Rows(0).Item("IsActive").ToString.Trim())) Then
                                        returnMsg(i).IsActiveLock = False
                                    Else
                                        returnMsg(i).IsActiveLock = ds.Tables(1).Rows(0).Item("IsActive").ToString.Trim()
                                    End If
                                Else
                                    returnMsg(i).IsActiveLock = False
                                End If
                            End If
                        Next

                    End If
                Else
                    returnMsg = New LoginMessage(1) {}
                    returnMsg(0) = New LoginMessage()
                    returnMsg(0).message = ds.Tables(0).Rows(0).Item("mes").ToString()
                    returnMsg(0).errorcode = "2"
                End If
            End If
        Catch ex As Exception
            returnMsg = New LoginMessage(1) {}
            returnMsg(0) = New LoginMessage()
            returnMsg(0).message = ex.Message.ToString() & " INNER EXCEPTION " & ex.InnerException.ToString()
            returnMsg(0).errorcode = "2"

        End Try
        Return returnMsg

    End Function


    Function Test() As String
        Return "Test Success"
    End Function

    Function GetSubGroup() As SubGroupParameter() Implements ISetupServices.GetSubGroup
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_SubGroup]   @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As SubGroupParameter() = New SubGroupParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New SubGroupParameter()
            JsonArray(i).SubGroupID = rs("SubGroupID").ToString
            JsonArray(i).SubGroupName = rs("SubGroupName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetGroup() As GroupParameter() Implements ISetupServices.GetGroup
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_Group]   @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As GroupParameter() = New GroupParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New GroupParameter()
            JsonArray(i).GroupID = rs("GroupID").ToString
            JsonArray(i).GroupName = rs("GroupName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetAccount() As AccountParameter() Implements ISetupServices.Getaccount
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_Account]   @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As AccountParameter() = New AccountParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New AccountParameter()
            JsonArray(i).AccountID = rs("AccountID").ToString
            JsonArray(i).AccountName = rs("AccountName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetSemester() As SemesterParameter() Implements ISetupServices.GetSemester
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Semester]   @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As SemesterParameter() = New SemesterParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New SemesterParameter()
            JsonArray(i).SemesterID = rs("SemesterID").ToString
            JsonArray(i).SemesterName = rs("SemesterName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function DeleteVoucher(ByVal voucherId As String) As String Implements ISetupServices.DeleteVoucher
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag = 'd', @JournalVourcherID = '" & voucherId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function DeleteJournalVoucher(ByVal voucherId As String) As String Implements ISetupServices.DeleteJournalVoucher
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag = 'd_jv', @JournalVourcherID = '" & voucherId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function VerifyVoucher(ByVal sessionId As String, ByVal sUserId As String, ByVal BranchID As String, ByVal bulkGUID As String, ByVal sFiscalID As String) As String Implements ISetupServices.VerifyVoucher
        Dim sql As String
        Dim ds As New DataSet
        Dim referenceNo As String
        sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag = 'veri', @SessionID = '" & sessionId & "',@UserName='" & sUserId & "',@BranchID='" & BranchID & "',@BulkGUID='" & bulkGUID & "'"
        sql += ",@FiscalYearID='" & sFiscalID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'referenceNo = ds.Tables(0).Rows(0).Item("refNo").ToString()
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function SetAttendance(ByVal EmployeeID As String, ByVal sUserId As String, ByVal MonthID As String, ByVal sFiscalID As String) As String Implements ISetupServices.SetAttendance
        Dim sql As String
        Dim ds As New DataSet
        Dim referenceNo As String
        sql = "EXEC  [Accounts].[usp_PayrollAttendace]  @Flag = 't', @EmployeeID= '" & EmployeeID & "', @MonthID = '" & MonthID & "',@CreatedBy='" & sUserId & "',@FiscalYearID='" & sFiscalID & "'"

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'referenceNo = ds.Tables(0).Rows(0).Item("refNo").ToString()
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function


    Function AsignSal(ByVal EmployeeID As String, ByVal MonthID As String) As String Implements ISetupServices.AsignSal
        Dim sql As String
        Dim ds As New DataSet
        Dim referenceNo As String
        sql = "EXEC Accounts.ups_SingleInsertToPayroll  @Flag = 'SetPayroll', @EmployeeID= '" & EmployeeID & "', @MonthID = '" & MonthID & "'"

        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'referenceNo = ds.Tables(0).Rows(0).Item("refNo").ToString()
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function approveVoucher(ByVal sessionId As String, ByVal sUserId As String) As String Implements ISetupServices.approveVoucher
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag = 'approv', @SessionID = '" & sessionId & "',@UserName='" & sUserId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function JournalVoucherDetail(ByVal sessionId As String, ByVal BranchID As String, ByVal bulkGUID As String) As JournalVoucherDetail() Implements ISetupServices.JournalVoucherDetail
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag='jd', @SessionID='" & sessionId & "',@BranchID='" & BranchID & "',@BulkGUID='" & bulkGUID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim jsonArray As JournalVoucherDetail() = New JournalVoucherDetail(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            jsonArray(i) = New JournalVoucherDetail()
            jsonArray(i).JournalVoucherId = rs("JournalVourcherID").ToString()
            jsonArray(i).AccountHead = rs("AccountName").ToString()
            jsonArray(i).DR = rs("DR").ToString()
            jsonArray(i).CR = rs("CR").ToString()
            jsonArray(i).Narration = rs("Narration").ToString()
            jsonArray(i).SubAccountHead = rs("SubAccountHead").ToString()
            i = i + 1
        Next
        Return jsonArray
    End Function

    Function GetJournalVoucherDetailByBulkID(ByVal BranchID As String, ByVal bulkGUID As String) As String Implements ISetupServices.GetJournalVoucherDetailByBulkID
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag='jd',@BranchID='" & BranchID & "',@BulkGUID='" & bulkGUID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim tab As String
            Dim i As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then

                tab = "<table cellpadding='5' cellspacing='0' border='0' style='padding-left:50px;'>"
                tab += "<tr><th style='width:20px;text-align:center;'>SN</th><th>Account Head</th><th>Debit</th><th>Credit</th></tr>"

                For Each rs As DataRow In ds.Tables(0).Rows
                    Dim jvID As String = rs("JournalVourcherID").ToString.Trim

                    tab += "<tr><td style='width:20px;text-align:center;'>" & i + 1 & ".</td><td style='width:60px;text-align:left;'>" & rs("AccountName").ToString
                    If Not String.IsNullOrWhiteSpace(rs("SubAccountHead").ToString) Then
                        tab += "( " & rs("SubAccountHead").ToString & " )"
                    End If

                    tab += "</td>"
                    tab += "<td style='width:50px;text-align:center;'><a data-toggle='modal' data-id='" & jvID & "' data-modify='DR' id='edit1' class='EditAmount' href='javascript:;'>" & rs("DR").ToString & "</a></td>"
                    tab += "<td style='width:50px;text-align:center;'><a data-toggle='modal' data-id='" & jvID & "' data-modify='CR' id='edit2' class='EditAmount' href='javascript:;'>" & rs("CR").ToString & "</a></td></tr>"
                    i = i + 1
                Next
                tab += "</table>"
            End If
            Return tab
        Catch ex As Exception

        End Try
    End Function

    Function UpdateParticular(ByVal Amount As String, ByVal FeesDetailsID As String) As String Implements ISetupServices.UpdateParticular
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_RevisedFeeDetails @Flag = 'l', @Amount = '" & Amount & "',@FeesDetailsID='" & FeesDetailsID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function UpdateAcademicStatus(ByVal AcademicInfoID As String, ByVal StatusVal As String, ByVal StatusName As String) As String Implements ISetupServices.UpdateAcademicStatus
        Dim sql As String
        Dim ds As New DataSet
        Dim status As Boolean

        status = IIf(StatusVal = "1", True, False)

        sql = "EXEC [Students].[usp_UpdateAcademicStatus] @flag='u',@AcademicInfoID='" & AcademicInfoID & "',@StatusName='" & StatusName & "',@StatusVal='" & status & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
    End Function

    Function UpdateAcademicYear(ByVal AcademicInfoID As String, ByVal AcademicYear As String, ByVal BranchID As String) As String Implements ISetupServices.UpdateAcademicYear
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC [Students].[usp_UpdateAcademicStatus] @flag='a',@AcademicInfoID='" & AcademicInfoID & "',@AcademicYear='" & AcademicYear & "',@BranchID='" & BranchID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
    End Function

    Function DeleteFees(ByVal FeesDetailsID As String) As String Implements ISetupServices.DeleteFees
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_modification @Flag = 'd',@FeesDetailsID='" & FeesDetailsID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function


    Function RegisterUser(ByVal SymbolNo As String, ByVal MobileNo As String, ByVal Name As String) As String Implements ISetupServices.RegisterUser
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  [Mobile].[usp_Registration]  @Flag = 'i',@EmployeeSymbolNo='" & SymbolNo & "',@Mobile='" & MobileNo & "',@Name='" & Name & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
        Return ""
    End Function

    Function GetUser(ByVal RegistrationID As String) As Profile() Implements ISetupServices.GetUser
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  [Mobile].[usp_Registration]  @Flag = 'a',@RegistrationID='" & RegistrationID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim JsonArray As Profile() = New Profile(ds.Tables(0).Rows.Count - 1) {}
            For Each rs As DataRow In ds.Tables(0).Rows
                JsonArray(i) = New Profile()
                JsonArray(i).Name = rs("Name").ToString
                JsonArray(i).MobileNo = rs("Mobile").ToString
                JsonArray(i).SymbolNo = rs("EmployeeSymbolNo").ToString
                i = i + 1
            Next
            Return JsonArray
        Catch ex As Exception

        End Try

    End Function


    Function GetVoucherDetails(ByVal ref As String, ByVal sBranchID As String, ByVal sFiscalyearID As String) As String Implements ISetupServices.GetVoucherDetails
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        sql = "EXEC accounts.usp_modi_journalEntry @flag='t_modal', @voucherNo='" & ref & "',@BranchID='" & sBranchID & "',@FiscalYearID='" & sFiscalyearID & "'"

        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        sb.AppendLine("<div class='row'>")
        sb.AppendLine(" <div class='col-md-12'>")
        sb.AppendLine("  <div class='portlet box green'>")
        sb.AppendLine(" <div class='portlet-title'>")
        sb.AppendLine("  <div class='caption'>")
        sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers Information</div>")
        sb.AppendLine(" <div class='tools'>")
        sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
        sb.AppendLine("  </div>")
        sb.AppendLine("  </div>")
        sb.AppendLine("<div class='portlet-body flip-scroll'>")
        sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
        sb.AppendLine("<thead class='flip-content'>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<th>S.N</th>")
        sb.AppendLine("<th>Ledger Head </th>")
        sb.AppendLine("<th>DR </th>")
        sb.AppendLine("<th>CR</th>")
        sb.AppendLine("<th>Adv_Exp_Code</th>")
        sb.AppendLine("<th colspan='2'>Actions</th>")
        sb.AppendLine("</tr>")
        sb.AppendLine("</thead>")
        sb.AppendLine("<tbody>")

        Dim refno As String = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString
        Dim v_date As String = ds.Tables(0).Rows(i).Item("VoucherDate").ToString
        Dim chequeNo As String = ds.Tables(0).Rows(i).Item("ChequeNo").ToString

        For Each row As DataRow In ds.Tables(0).Rows
            Dim jid = ds.Tables(0).Rows(i).Item("JournalEntryID").ToString
            Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
            Dim subAccountName = ds.Tables(0).Rows(i).Item("SubAccountName").ToString.Trim
            Dim cr = ds.Tables(0).Rows(i).Item("cr").ToString
            Dim dr = ds.Tables(0).Rows(i).Item("DR").ToString
            Dim ledgerCode = ds.Tables(0).Rows(i).Item("LedgerCode").ToString

            sb.AppendLine("<tr><td>" & (i + 1) & ".</td>")
            sb.AppendLine("<td>" & AccountName & " ")
            If Not String.IsNullOrWhiteSpace(subAccountName) Then
                sb.AppendLine("( " & subAccountName & " )")
            End If
            sb.AppendLine("</td>")
            sb.AppendLine("<td><a  data-id='" & jid & "' data-mo='dr' data='" & dr & "' id='link1' class='ClassFees' href='javascript:;'>" & dr & "</a> </td>")
            sb.AppendLine("<td><a  data-id='" & jid & "' data-mo='cr' data='" & cr & "' id='link2' class='ClassFees' href='javascript:;'>" & cr & "</a> </td>")
            sb.AppendLine("<td><a  data-id='" & jid & "' data-mo='LedgerCode' data='" & ledgerCode & "' id='link3' class='ClassFees' href='javascript:;'>" & ledgerCode & "</a> </td>")
            sb.AppendLine("<td colspan='2'><a  id='add' class='modifyLedger btn btn-primary btn-xs' href='/Accounts/extended/modi_ledger.aspx?ReferenceNo=" & refno & "'><span class='btn-label icon fa fa-edit'></span></a> | ")
            sb.AppendLine("<a  data-id='" & jid & "' id='deleteid' class='deletesinglerow btn btn-danger btn-xs' href='javascript:;'><span class='glyphicon glyphicon-trash'></span></a> </td>")
            sb.AppendLine("</tr>")

            i += 1
        Next

        Dim dv As DataView = New DataView(ds.Tables(0))
        dv.RowFilter = "summary<>''"
        If dv.Count > 0 Then
            sb.AppendLine("<tr><td colspan=7>Narration : <a  data-id='" & refno & "' data-mo='Summary' data='" & dv.Item(0).Item("summary").ToString & "' id='link4' class='multi_rows_edit' href='javascript:;'>" & dv.Item(0).Item("summary").ToString & "</a> </td></tr>")

        End If
        sb.AppendLine("<tr><td colspan=7>Voucher Date : <a  data-id='" & refno & "' data-mo='VoucherDate' data='" & v_date & "' id='link5' class='multi_rows_edit' href='javascript:;'>" & v_date & "</a> </td></tr>")
        sb.AppendLine("<tr><td colspan=7>Cheque No : <a  data-id='" & refno & "' data-mo='ChequeNo' data='" & chequeNo & "' id='link6' class='multi_rows_edit' href='javascript:;'>" & chequeNo & "</a> </td></tr>")
        sb.AppendLine("</tbody>")
        sb.AppendLine("</table>")
        sb.AppendLine("</div>")
        sb.AppendLine(" </div>")
        sb.AppendLine("</div>")
        sb.AppendLine(" </div>")

        Return sb.ToString
    End Function


    Function GetVoucherDetails_forReport(ByVal accHead As String, ByVal sBranchID As String, ByVal sFiscalID As String, ByVal dbName As String) As String Implements ISetupServices.GetVoucherDetails_forReport
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        sql = "EXEC [Accounts].[usp_Reports] @flag='TB_details', @filterby=N'" & accHead & "',@BranchID='" & sBranchID & "',@FiscalYearID='" & sFiscalID & "'"

        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        sb.AppendLine("<div class='row'>")
        sb.AppendLine(" <div class='col-md-12'>")
        sb.AppendLine("  <div class='portlet box purple'>")
        sb.AppendLine(" <div class='portlet-title'>")
        sb.AppendLine("  <div class='caption'>")
        sb.AppendLine(" <i class='fa fa-cogs'></i>Ledgers Information</div>")
        sb.AppendLine(" <div class='tools'>")
        sb.AppendLine("<a href='javascript:;' class='collapse'></a>")
        sb.AppendLine("  </div>")
        sb.AppendLine("  </div>")
        sb.AppendLine("<div class='portlet-body flip-scroll'>")
        sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
        sb.AppendLine("<thead class='flip-content'>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<th>S.N</th>")
        sb.AppendLine("<th>Voucher No.</th>")
        sb.AppendLine("<th>Ledger Head </th>")
        sb.AppendLine("<th>DR </th>")
        sb.AppendLine("<th>CR</th>")
        sb.AppendLine("<th>Date</th>")
        sb.AppendLine("</tr>")
        sb.AppendLine("</thead>")
        sb.AppendLine("<tbody>")


        For Each row As DataRow In ds.Tables(0).Rows
            Dim refno As String = ds.Tables(0).Rows(i).Item("ReferenceNo").ToString

            Dim AccountName = ds.Tables(0).Rows(i).Item("AccountName").ToString
            Dim subAccountName = ds.Tables(0).Rows(i).Item("SubAccountName").ToString.Trim
            Dim cr = ds.Tables(0).Rows(i).Item("CR").ToString
            Dim dr = ds.Tables(0).Rows(i).Item("DR").ToString
            Dim v_date As String = ds.Tables(0).Rows(i).Item("VoucherDate").ToString
            'Dim chequeNo As String = ds.Tables(0).Rows(i).Item("ChequeNo").ToString

            sb.AppendLine("<tr><td>" & (i + 1) & ".</td>")
            If (dbName = "PokharaNurshingCampus" Or dbName = "PulchowkCampusNew2075" Or dbName = "ForestryDean2075") Then
                sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucherInNepali2.aspx?vid=" & refno & "'>" & refno & "</a></td>")
            Else
                sb.AppendLine("<td><a target='_blank' href='/Accounts/JournalVoucher/ViewSingleVoucher.aspx?vid=" & refno & "'>" & refno & "</a></td>")
            End If
            sb.AppendLine("<td>" & AccountName & " ")
            If Not String.IsNullOrWhiteSpace(subAccountName) Then
                sb.AppendLine("( " & subAccountName & " )")
            End If
            sb.AppendLine("</td>")
            sb.AppendLine("<td>" & dr & "</td>")
            sb.AppendLine("<td>" & cr & "</td>")
            sb.AppendLine("<td>" & v_date & "</td>")
            sb.AppendLine("</tr>")

            i += 1
        Next

        'Dim dv As DataView = New DataView(ds.Tables(0))
        'dv.RowFilter = "summary<>''"
        'If dv.Count > 0 Then
        '    sb.AppendLine("<tr><td colspan=7>Narration : " & dv.Item(0).Item("summary").ToString & "</td></tr>")
        'End If
        'sb.AppendLine("<tr><td colspan=7>Voucher Date : " & v_date & "</td></tr>")
        'sb.AppendLine("<tr><td colspan=7>Cheque No : " & chequeNo & "</td></tr>")
        'sb.AppendLine("<tr><td colspan=7>Voucher No : " & refno & "</td></tr>")

        sb.AppendLine("</tbody>")
        sb.AppendLine("</table>")
        sb.AppendLine("</div>")
        sb.AppendLine(" </div>")
        sb.AppendLine("</div>")
        sb.AppendLine(" </div>")

        Return sb.ToString
    End Function
    Function delete_v_single_row(ByVal jid As String, ByVal sUserId As String) As String Implements ISetupServices.delete_v_single_row
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_modi_journalEntry  @Flag = 'd_row', @JournalEntryID='" & jid & "',@userID='" & sUserId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)


            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception

        End Try

    End Function

    Function edit_vourcher(ByVal jid As String, ByVal modi As String, ByVal val As String, ByVal sUserId As String) As String Implements ISetupServices.edit_vourcher
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_modi_journalEntry  @Flag = 'u_row', @modi_item='" & modi & "',@JournalEntryID='" & jid & "', @value=N'" & val & "',@userID='" & sUserId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)


            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception

        End Try

    End Function

    Function edit_multi_rows_vourcher(ByVal ref As String, ByVal modi As String, ByVal val As String, ByVal sUserId As String, ByVal BranchID As String, ByVal sFiscalyearID As String) As String Implements ISetupServices.edit_multi_rows_vourcher
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_modi_journalEntry  @Flag = 'muliple_rows_edit', @modi_item='" & modi & "',@voucherNo='" & ref & "', @value=N'" & val & "',@userID='" & sUserId & "',@BranchID='" & BranchID & "'"
        sql += ",@FiscalYearID='" & sFiscalyearID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)


            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception

        End Try

    End Function

    Function delete_multi_rows_vourcher_no(ByVal ref As String, ByVal sUserId As String, ByVal BranchID As String, ByVal sFiscalyearID As String) As String Implements ISetupServices.delete_multi_rows_vourcher_no
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_modi_journalEntry  @Flag = 'row_del', @voucherNo='" & ref & "',@userID='" & sUserId & "',@BranchID='" & BranchID & "'"
        sql += ",@FiscalYearID='" & sFiscalyearID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception

        End Try

    End Function

    Function edit_vourcher_no(ByVal refNo As String, ByVal modifyVal As String, ByVal refID As String, ByVal jeID1 As String, ByVal jeID2 As String, ByVal sUserId As String, ByVal BranchID As String, ByVal sFiscalyearID As String) As String Implements ISetupServices.edit_vourcher_no
        Dim sql As String
        Dim ds As New DataSet
        Try
            If (modifyVal <> "0" And modifyVal.StartsWith("V")) Then
                sql = "EXEC  accounts.usp_modi_journalEntry  @Flag = 'jv_reorder', @RefID='" & refID & "',@voucherNo='" & refNo & "',@value='" & modifyVal & "', @topJEId='" & jeID1 & "', @lastJEId='" & jeID2 & "',@userID='" & sUserId & "',@BranchID='" & BranchID & "'"
                sql += ",@FiscalYearID='" & sFiscalyearID & "'"
                ds = Dao.ExecuteDataset(sql)
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            Else
                Return "Please provide valid Voucher No."
            End If

        Catch ex As Exception

        End Try
    End Function


    Function delete_counter_bill(ByVal ref As String, ByVal sUserId As String) As String Implements ISetupServices.delete_counter_bill
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_OtherIncomeDayBook  @Flag = 'del_bill', @voucherNo='" & ref & "',@userID='" & sUserId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception

        End Try
    End Function
    Function vouchersList(ByVal branchid As String, ByVal fyid As String) As vouchersList() Implements ISetupServices.vouchersList
        Dim sql As String

        Dim ds As New DataSet
        sql = "EXEC  accounts.usp_VoucherlistAdv @BranchID='" & branchid & "',@FiscalYearID='" & fyid & "'"
        Try
            'ReferenceNo	VoucherDate	VoucherTime	AccountName	DR	Summary	VourcherName	BranchName	FullName
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim JsonArray As vouchersList() = New vouchersList(ds.Tables(0).Rows.Count - 1) {}

            For Each rs As DataRow In ds.Tables(0).Rows
                JsonArray(i) = New vouchersList()
                JsonArray(i).id = rs("ReferenceNo").ToString
                JsonArray(i).VoucherDate = rs("VoucherDate").ToString
                JsonArray(i).VoucherTime = rs("VoucherTime").ToString
                JsonArray(i).DR = rs("DR").ToString
                JsonArray(i).FullName = rs("FullName").ToString
                JsonArray(i).VourcherName = rs("VourcherName").ToString
                JsonArray(i).Summary = rs("Summary").ToString

                JsonArray(i).BranchName = rs("BranchName").ToString


                i = i + 1
            Next
            Return JsonArray
        Catch ex As Exception

        End Try

    End Function

    Function CancelvouchersList(ByVal branchid As String, ByVal fyid As String) As vouchersList() Implements ISetupServices.CancelvouchersList
        Dim sql As String

        Dim ds As New DataSet
        sql = "exec accounts.usp_CancelledVoucherlist @BranchID='" & branchid & "',@FiscalYearID='" & fyid & "'"
        Try
            'ReferenceNo	VoucherDate	VoucherTime	AccountName	DR	Summary	VourcherName	BranchName	FullName
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim JsonArray As vouchersList() = New vouchersList(ds.Tables(0).Rows.Count - 1) {}

            For Each rs As DataRow In ds.Tables(0).Rows
                JsonArray(i) = New vouchersList()
                JsonArray(i).id = rs("ReferenceNo").ToString
                JsonArray(i).VoucherDate = rs("VoucherDate").ToString
                JsonArray(i).VoucherTime = rs("VoucherTime").ToString
                JsonArray(i).DR = rs("DR").ToString
                JsonArray(i).Summary = rs("Summary").ToString
                JsonArray(i).VourcherName = rs("VourcherName").ToString
                JsonArray(i).BranchName = rs("BranchName").ToString
                JsonArray(i).FullName = rs("FullName").ToString

                i = i + 1
            Next
            Return JsonArray
        Catch ex As Exception

        End Try

    End Function



    Function vouchersListByID(ByVal ref As String) As String Implements ISetupServices.vouchersListByID
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec accounts.usp_rowDetails @ReferenceID='" & ref & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim tab As String = "<table cellpadding='5' cellspacing='0' border='0' style='padding-left:50px;'>"
            tab += "<tr><th>SN</th><th>Items</th><th>Debit</th><th>Credit</th></tr>"
            For Each rs As DataRow In ds.Tables(0).Rows
                tab += "<tr><td>" & i + 1 & ".</td><td>" & rs("AccountName").ToString & " </td><td>" & rs("DR").ToString & "</td><td>" & rs("CR").ToString & "</td></tr>"
                i = i + 1
            Next
            tab += "</table>"
            Return tab
        Catch ex As Exception

        End Try

    End Function


    Function CancelvouchersListByID(ByVal ref As String, ByVal sBranchID As String) As String Implements ISetupServices.CancelvouchersListByID
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec accounts.usp_rowDetailsCancellVouchers @ReferenceID='" & ref & "',@BranchID='" & sBranchID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim tab As String = "<table cellpadding='5' cellspacing='0' border='0' style='padding-left:50px;'>"
            tab += "<tr><th>SN</th><th>Items</th><th>Debit</th><th>Credit</th></tr>"
            For Each rs As DataRow In ds.Tables(0).Rows
                tab += "<tr><td>" & i + 1 & ".</td><td>" & rs("AccountName").ToString & " </td><td>" & rs("DR").ToString & "</td><td>" & rs("CR").ToString & "</td></tr>"
                i = i + 1
            Next
            tab += "</table>"
            Return tab
        Catch ex As Exception

        End Try

    End Function

    Function GetDueSummaryList(ByVal studentID As String, ByVal branchID As String) As String Implements ISetupServices.GetDueSummaryList
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_FillDueSummary] @flag='b',@StudentID='" & studentID & "',@BranchID='" & branchID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim tab As String

            For t As Integer = 0 To ds.Tables.Count - 1
                If ds.Tables(t).Rows.Count > 0 Then
                    Dim semtitle As String = ds.Tables(t).Rows(0).Item("SemesterName").ToString
                    Dim i As Integer = 0
                    tab = "<div style='padding-left:15px;font-weight:bolder;'>Table: " & (t + 1) & " >>" & semtitle & "</div><div>"
                    tab += "<table cellpadding='5' cellspacing='0' border='0' style='padding-left:20px;width:800px;'>"
                    tab += "<tr><th style='width:40px;text-align:center;'>SN</th><th>Bill No</th><th>Particulars</th><th>DR</th><th>CR</th></tr>"

                    For Each rs As DataRow In ds.Tables(t).Rows
                        tab += "<tr><td style='width:40px;text-align:center;'>" & i + 1 & ".</td><td style='width:45px;text-align:center;'>" & rs("ReferenceNo").ToString & " </td><td style='width:60px;text-align:center;'>" & rs("AccountName").ToString & "</td><td style='width:50px;text-align:center;'>" & rs("DR").ToString & "</td><td style='width:50px;text-align:center;'>" & rs("CR").ToString & "</td></tr>"
                        i = i + 1
                    Next
                    tab += "</table>"
                    tab += "</div>"
                Else
                    tab += "<div style='padding-left:15px;font-weight:bolder;'>Table: " & (t + 1) & " >> No Records Found.</div>"
                End If
            Next
            Return tab
        Catch ex As Exception

        End Try
    End Function


    Function GetStudentsForPrint(ByVal branchID As String) As printList() Implements ISetupServices.GetStudentsForPrint
        Dim sql As String

        Dim ds As New DataSet
        sql = "exec [Students].[usp_PrintingDocuments] @Flag='a',@BranchID='" & branchID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim JsonArray As printList() = New printList(ds.Tables(0).Rows.Count - 1) {}

            For Each rs As DataRow In ds.Tables(0).Rows
                Dim printId As String = rs("PrintingDocumentID").ToString.Trim

                JsonArray(i) = New printList()
                JsonArray(i).SN = i + 1
                JsonArray(i).id = rs("StudentID").ToString
                JsonArray(i).Name = rs("Name").ToString.Trim
                JsonArray(i).Batch = rs("Batch").ToString.Trim
                JsonArray(i).Semester = rs("Sem.").ToString.Trim
                JsonArray(i).RollNo = rs("RollNo").ToString
                JsonArray(i).DocumentType = rs("Type").ToString.Trim
                JsonArray(i).DocumentTypeID = rs("DocumentTypeID").ToString.Trim
                JsonArray(i).LastPrintedDate = rs("PrintedDate").ToString()
                JsonArray(i).Action = "<a href='/Student/CardAndCertificatePrint.aspx?StudentID=" & rs("StudentID").ToString.Trim & "&DocType=" & rs("Type").ToString.Trim & "&PrintID=" & printId & "'>Re-Print</a>"
                JsonArray(i).Action += " | <a href='/Student/CardAndCertificatePrint.aspx?DelPrintID=" & printId & "'>Delete</a>"
                i = i + 1
            Next
            Return JsonArray
        Catch ex As Exception

        End Try
    End Function


    Function GetStudentPrintingInfo(ByVal studentID As String, ByVal branchID As String, ByVal docTypeID As String) As String Implements ISetupServices.GetStudentPrintingInfo
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Students].[usp_PrintingDocuments] @Flag='s',@StudentID='" & studentID & "',@BranchID='" & branchID & "',@DocumentTypeID='" & docTypeID.Trim & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim tab As String
            If ds.Tables(0).Rows.Count > 1 Then
                Dim i As Integer = 0
                tab = "<table cellpadding='5' cellspacing='0' border='0' style='padding-left:50px;'>"
                tab += "<tr><th>SN</th><th>Document Type</th><th>Status</th><th>Bill No.</th><th>Approved Document No.</th><th>Template No.</th><th>Symbol No.</th><th>Division/CGPA</th><th>Passed Year</th><th>BarCode</th><th>Valid From</th><th>Valid To</th></tr>"

                For Each rs As DataRow In ds.Tables(0).Rows
                    tab += "<tr><td>" & i + 1 & ".</td><td>" & rs("DocType").ToString & " </td><td>" & rs("Status").ToString & "</td><td>" & rs("BillNumber").ToString & "</td><td>" & rs("ApprovedDocNo").ToString & "</td><td>" & rs("TemplateNumber").ToString & "</td><td>" & rs("SymbolNo").ToString & "</td><td>" & rs("Division").ToString & "</td><td>" & rs("PassedYear").ToString & "</td><td>" & rs("Barcode").ToString & "</td><td>" & rs("ValidFrom").ToString & "</td><td>" & rs("ValidTo").ToString & "</td></tr>"
                    i = i + 1
                Next
                tab += "</table>"
            Else
                Dim j As Integer = 0
                tab = "<table cellpadding='5' cellspacing='0' border='0' style='padding-left:50px;'>"
                For Each rs As DataRow In ds.Tables(0).Rows
                    tab += "<tr><th style='width:45px'>SN</th><td style='width:45px'>" & j + 1 & ".</td></tr>"
                    tab += "<tr><th>Document Type</th><td>" & rs("DocType").ToString & "</td></tr>"
                    tab += "<tr><th>Status</th><td>" & rs("Status").ToString & "<tr></tr>"
                    tab += "<tr><th>Bill No.</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='BillNumber' id='edit1' class='ModifyField' href='javascript:;'>" & rs("BillNumber").ToString & "</a></td></tr>"
                    tab += "<tr><th>Approved Document No.</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='ApprovedDocumentAndNumber' id='edit2' class='ModifyField' href='javascript:;'>" & rs("ApprovedDocNo").ToString & "</a></td></tr>"
                    tab += "<tr><th>Template No.</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='TemplateNumber' id='edit3' class='ModifyField' href='javascript:;'>" & rs("TemplateNumber").ToString & "</a></td></tr>"
                    tab += "<tr><th>Symbol No.</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='SymbolNo' id='edit7' class='ModifyField' href='javascript:;'>" & rs("SymbolNo").ToString & "</a></td></tr>"
                    tab += "<tr><th>Division/CGPA</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='Division' id='edit8' class='ModifyField' href='javascript:;'>" & rs("Division").ToString & "</a></td></tr>"

                    tab += "<tr><th>Passed Year</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='PassedYear' id='edit9' class='ModifyField' href='javascript:;'>" & rs("PassedYear").ToString & "</a></td></tr>"
                    tab += "<tr><th>Completion Month</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='CompletionMonth' id='edit10' class='ModifyField' href='javascript:;'>" & rs("CompletionMonth").ToString & "</a></td></tr>"
                    tab += "<tr><th>Serial No</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='SerialNo' id='edit11' class='ModifyField' href='javascript:;'>" & rs("SerialNo").ToString & "</a></td></tr>"

                    tab += "<tr><th>BarCode</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='Barcode' id='edit4' class='ModifyField' href='javascript:;'>" & rs("Barcode").ToString & "</a></td></tr>"
                    tab += "<tr><th>Valid From</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='ValidFrom' id='edit5' class='ModifyField' href='javascript:;'>" & rs("ValidFrom").ToString & "</a></td></tr>"
                    tab += "<tr><th>Valid To</th><td><a data-toggle='modal' data-id='" & rs("PrintingDocumentID").ToString & "' data-modify='validTo' id='edit6' class='ModifyField' href='javascript:;'>" & rs("ValidTo").ToString & "</a></td></tr>"
                    j = j + 1
                Next
                tab += "</table>"
            End If
            Return tab

        Catch ex As Exception

        End Try
    End Function

    Function UpdatePrintingInfo(ByVal printdocumentId As String, ByVal modifyitem As String, ByVal val As String) As String Implements ISetupServices.UpdatePrintingInfo
        Dim sql As String

        Dim ds As New DataSet

        Try
            sql = "exec [Students].[usp_PrintingDocuments] @Flag='u',@PrintingDocumentID='" & printdocumentId & "',@item_to_modify='" & modifyitem & "', @modifyvalue=N'" & val & "'"
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Item("ErrorCode") = "0" Then
                    Return ds.Tables(0).Rows(0).Item("mes").ToString()
                Else
                    Return "Unable to update!!"
                End If
            End If
            Return ""
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function edit_TempJournalVourcher(ByVal jvId As String, ByVal modifyitem As String, ByVal val As String) As String Implements ISetupServices.edit_TempJournalVourcher
        Dim sql As String
        Dim ds As New DataSet
        Try
            If Not String.IsNullOrWhiteSpace(val) Then
                sql = "EXEC  [Accounts].[usp_TempJournalVourcher]  @Flag = 'edit_colmn', @item_to_modify='" & modifyitem & "',@JournalVourcherID='" & jvId & "', @modifyvalue=N'" & val & "'"

                ds = Dao.ExecuteDataset(sql)


                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function

    Function edit_JournalVoucher(ByVal jvId As String, ByVal modifyitem As String, ByVal val As String) As String Implements ISetupServices.edit_JournalVoucher
        Dim sql As String
        Dim ds As New DataSet
        Try
            If Not String.IsNullOrWhiteSpace(val) Then
                sql = "EXEC  [Accounts].[usp_TempJournalVourcher]  @Flag = 'edit_JV', @item_to_modify='" & modifyitem & "',@JournalVourcherID='" & jvId & "', @modifyvalue=N'" & val & "'"

                ds = Dao.ExecuteDataset(sql)
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function delete_JournalVoucher(ByVal bulkGuid As String) As String Implements ISetupServices.delete_JournalVoucher
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag='delete_JV',@BulkGUID='" & bulkGuid & "'"
            ds = Dao.ExecuteDataset(sql)
            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function TempJournalVoucher_AddNotes(ByVal jvId As String, ByVal Notes As String) As String Implements ISetupServices.TempJournalVoucher_AddNotes
        Dim sql As String
        Dim ds As New DataSet
        Dim jvNotes As String
        Try
            'jvNotes = Notes.Replace("\\n", Chr(13))
            sql = "EXEC  [Accounts].[usp_TempJournalVourcher] @Flag='add_notes',@JournalVourcherID='" & jvId & "',@Notes=N'" & Notes & "'"
            ds = Dao.ExecuteDataset(sql)


            Return ds.Tables(0).Rows(0).Item("mes").ToString()
        Catch ex As Exception

        End Try
    End Function

    Function Allocate_BudgetAmount(ByVal BudgetId As String, ByVal AllocatedAmount As String, ByVal GroupID As String, ByVal SubGroupID As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.Allocate_BudgetAmount
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_Budgeting] @flag='i',@BudgetId='" & BudgetId & "',@AllotedAmount=N'" & AllocatedAmount & "',@GroupID='" & GroupID & "',@SubGroupID='" & SubGroupID & "'"
            sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@UserId='" & sUserID & "',@FiscalYearID='" & FiscalYearID & "'"
            ds = Dao.ExecuteDataset(sql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function UpdateBudgeting(ByVal BudgetId As String, ByVal modifyitem As String, ByVal val As String, ByVal GroupID As String, ByVal SubGroupID As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.UpdateBudgeting
        Dim sql As String
        Dim ds As New DataSet
        Try
            If Not String.IsNullOrWhiteSpace(val) Then
                sql = "EXEC  [Accounts].[usp_Budgeting]  @Flag = 'a',@BudgetId='" & BudgetId & "', @item_to_modify='" & modifyitem & "', @modifyvalue=N'" & val & "'"
                sql += ",@GroupID='" & GroupID & "',@SubGroupID='" & SubGroupID & "'"
                sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@UserId='" & sUserID & "',@FiscalYearID='" & FiscalYearID & "'"

                ds = Dao.ExecuteDataset(sql)
                If (ds.Tables(0).Rows.Count > 0) Then
                    Return ds.Tables(0).Rows(0).Item("mes").ToString()
                End If
            End If
            Return ""
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function VoucherDetails_DR_CR_Add(ByVal DetailId As String, ByVal GroupID As String, ByVal SubGroupID As String, ByVal AccountID As String, ByVal DR As String, ByVal CR As String, ByVal Summary As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String, ByVal sFiscalID As String) As String Implements ISetupServices.VoucherDetails_DR_CR_Add
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_VoucherDetails] @flag='addDRCR',@DetailId='" & DetailId & "',@GroupID='" & GroupID & "',@SubGroupID='" & SubGroupID & "',@AccountID='" & AccountID & "'"
            sql += ",@DR=N'" & DR & "',@CR=N'" & CR & "',@Summary=N'" & Summary & "'"
            sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@CreatedBy='" & sUserID & "',@FiscalYearID='" & sFiscalID & "'"

            ds = Dao.ExecuteDataset(sql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""

        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function UpdateVoucherDetails_Summary(ByVal DetailId As String, ByVal Summary As String) As String Implements ISetupServices.UpdateVoucherDetails_Summary
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_VoucherDetails] @flag='summary',@DetailId='" & DetailId & "',@Summary=N'" & Summary & "'"
            ds = Dao.ExecuteDataset(sql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function GetVoucherDetails_ByAccount(ByVal AccountID As String, ByVal sFiscalID As String) As String Implements ISetupServices.GetVoucherDetails_ByAccount
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Accounts].[usp_VoucherDetails] @flag='a',@AccountID='" & AccountID & "',@FiscalYearID='" & sFiscalID & "'"
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine(" <div class='col-md-12'>")
                sb.AppendLine("  <div class='portlet box green'>")
                sb.AppendLine(" <div class='portlet-title'>")
                sb.AppendLine("  <div class='caption'>")
                sb.AppendLine(" <i class='fa fa-user'></i>" & ds.Tables(0).Rows(0)("AccountName").ToString().Trim() & "</div>")
                sb.AppendLine(" <div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;' class='reload'></a>")
                sb.AppendLine("  </div>")
                sb.AppendLine("  </div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")
                sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.Append("<th>SN</th>")
                sb.Append("<th>DR</th>")
                sb.Append("<th>CR</th>")
                sb.Append("<th>Summary</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")

                sb.AppendLine("<tbody>")
                Dim count As Integer = ds.Tables(0).Rows.Count
                For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim DetailId = ds.Tables(0).Rows(r).Item("DetailID").ToString
                    Dim drAmt = ds.Tables(0).Rows(r).Item("DR").ToString
                    Dim crAmt = ds.Tables(0).Rows(r).Item("CR").ToString
                    Dim Summary = ds.Tables(0).Rows(r).Item("Summary").ToString.Trim

                    Dim DR As Decimal = Convert.ToDecimal(drAmt).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    Dim CR As Decimal = Convert.ToDecimal(crAmt).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td>" & r + 1 & "</td>")
                    sb.AppendLine("<td style='text-align: right; padding-right:15px;'><a  data-id='" & DetailId & "' data-accId='" & AccountID & "' data-item='DR' id='addNewDR' class='AddDRCR' href='javascript:;'>" & DR & "</a></td>")
                    sb.AppendLine("<td style='text-align: right; padding-right:15px;'><a  data-id='" & DetailId & "' data-accId='" & AccountID & "' data-item='CR' id='addNewCR' class='AddDRCR' href='javascript:;'>" & CR & "</a></td>")

                    If (String.IsNullOrWhiteSpace(Summary)) Then
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & DetailId & "' href='javascript:;' id='addVSummary' class='AddRemarks'> Add Summary </a></td>")
                    Else
                        sb.AppendLine("<td><a data-toggle='modal' data-id='" & DetailId & "' data-text='" & Summary & "' href='javascript:;' id='editVSummary' class='AddRemarks'>" & Summary & "</a></td>")
                    End If
                    sb.AppendLine("</tr>")
                Next

                'For adding new rows
                For i As Integer = (count + 1) To (count + 5)
                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td>" & i & "</td>")
                    sb.AppendLine("<td style='text-align: right; padding-right:15px;'><a  data-id='0' data-accId='" & AccountID & "' data-item='DR' id='addNewDR' class='AddDRCR' href='javascript:;'>0.00</a></td>")
                    sb.AppendLine("<td style='text-align: right; padding-right:15px;'><a  data-id='0' data-accId='" & AccountID & "' data-item='CR' id='addNewCR' class='AddDRCR' href='javascript:;'>0.00</a></td>")
                    sb.AppendLine("<td><a data-id='0' href='javascript:;' id='add' class='AddRemarks'> Add Summary </a></td>")
                    sb.AppendLine("</tr>")
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")

                Return sb.ToString()
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function



    'Function GetIncomeDetails(ByVal groupId As String, ByVal subgroupId As String, ByVal grouptype As String) As IncomeDetails() Implements ISetupServices.GetIncomeDetails
    '    Dim sql As String
    '    Dim ds As New DataSet
    '    sql = "EXEC Accounts.usp_Reports @Flag='incomeDetail', @GroupID='" & groupId & "', @SubGroupID='" & subgroupId & "'"
    '    ds = Dao.ExecuteDataset(sql)
    '    Dim i As Integer = 0
    '    Dim jsonArray As IncomeDetails() = New IncomeDetails(ds.Tables(0).Rows.Count - 1) {}
    '    For Each rs As DataRow In ds.Tables(0).Rows
    '        jsonArray(i) = New IncomeDetails()
    '        jsonArray(i).AccountName = rs("AccountName").ToString
    '        Dim amt = rs("Amount").ToString()
    '        If (Not String.IsNullOrWhiteSpace(amt)) Then
    '            amt = Convert.ToDecimal(amt).ToString("0,0.00", CultureInfo.CreateSpecificCulture("ne-NP"))
    '        End If
    '        jsonArray(i).Amount = amt
    '        i = i + 1
    '    Next
    '    Return jsonArray
    'End Function


#Region "Methods for Exam module || developer : Pralhad "

#Region "Direct Assessment module || developer : Pralhad "

    Public Function GetAssessmentStudents(ByVal batchId As String, ByVal examID As String, ByVal StudentName As String, ByVal SectionID As String) As ExamStudents() Implements ISetupServices.GetAssessmentStudents
        Dim sql As String = "EXEC [Exam].[usp_AssessmentProgressReport]  @Flag='stdList'"
        sql += ", @examID=" + Dao.FilterQuote(examID)
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        sql += ", @SectionID=" + Dao.FilterQuote(SectionID)
        sql += ", @StudentName=" + Dao.FilterQuote(StudentName)

        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As ExamStudents() = New ExamStudents(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New ExamStudents()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                i = i + 1
            Next
        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function

    Public Function GetFinalStudents(ByVal batchId As String, ByVal StudentName As String, ByVal SectionID As String) As ExamStudents() Implements ISetupServices.GetFinalStudents
        Dim sql As String = "EXEC [Exam].[usp_FinalProgressReport]  @Flag='stdList'"
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        sql += ", @SectionID=" + Dao.FilterQuote(SectionID)
        sql += ", @StudentName=" + Dao.FilterQuote(StudentName)

        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As ExamStudents() = New ExamStudents(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New ExamStudents()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                i = i + 1
            Next
        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function

    Public Function GetDirectAssessmentStudents(ByVal batchId As String, ByVal sortyBy As String, ByVal SubjectID As String, ByVal SectionID As String, ByVal studentId As String) As ExamStudents() Implements ISetupServices.GetDirectAssessmentStudents
        Dim sql As String = "EXEC [Exam].[usp_DirectAssessment]  @Flag='s'"
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        sql += ", @sortyBy=" + Dao.FilterQuote(sortyBy)
        sql += ", @SubjectID=" + Dao.FilterQuote(SubjectID)
        sql += ", @SectionID=" + Dao.FilterQuote(SectionID)
        sql += ", @studentId=" + Dao.FilterQuote(studentId)

        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As ExamStudents() = New ExamStudents(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New ExamStudents()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                i = i + 1
            Next
        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function
    Public Function GetStudentsForSheetPlan(ByVal ExamID As String, ByVal batchId As String, ByVal sortyBy As String, ByVal SectionIDs As String) As StudentsForSheetPlan() Implements ISetupServices.GetStudentsForSheetPlan
        Dim sql As String = "EXEC [Exam].[usp_SheetPlanStudentGrouping]  @Flag='stdList'"
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        sql += ", @sortyBy=" + Dao.FilterQuote(sortyBy)
        sql += ", @SectionIDs=" + Dao.FilterQuote(SectionIDs)
        sql += ", @ExamID=" + Dao.FilterQuote(ExamID)

        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As StudentsForSheetPlan() = New StudentsForSheetPlan(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New StudentsForSheetPlan()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                jsonArray(i).Section = rs("Section").ToString()
                jsonArray(i).SectionId = rs("sectionID").ToString()
                i = i + 1
            Next
        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function
    Public Function SaveDirectAssessment(ByVal obj As SaveDirectAssessment()) As ErrorMessage Implements ISetupServices.SaveDirectAssessment
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim subjectId As String = obj(i).SubjectId
            Dim examId As String = obj(i).ExamId
            Dim assementCategoryID As String = obj(i).AssessmentCategoryID
            Dim fullMarks As String = obj(i).FullMarks
            Dim user As String = obj(i).CreatedBy

            If (obj(i).ObtainedMarks <> "" Or obj(i).ObtainedMarks <> "") Then
                Dim ObtainMarks As String = obj(i).ObtainedMarks

                Dim sql As String = "EXEC [Exam].[usp_DirectAssessment]  @Flag='i'"
                sql += ", @BatchId=" + Dao.FilterQuote(batchId)
                sql += ", @StudentID=" + Dao.FilterQuote(studentId)
                sql += ", @SubjectID=" + Dao.FilterQuote(subjectId)
                sql += ", @ExamID=" + Dao.FilterQuote(examId)
                sql += ", @AssessmentCategoryID=" + Dao.FilterQuote(assementCategoryID)
                sql += ", @ObtainedMarks=" + Dao.FilterQuote(ObtainMarks)
                sql += ", @user=" + Dao.FilterQuote(user)
                sql += ", @FullMarks=" + Dao.FilterQuote(fullMarks)

                ds = Dao.ExecuteDataset(sql)
            End If
        Next
        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function SaveExamAttendanceSheet(ByVal obj As SaveExamAttendanceSheet()) As ErrorMessage Implements ISetupServices.SaveExamAttendanceSheet
        Dim ds As New DataSet
        Dim stringQuery As String = "<attendance>"
        For i As Integer = 0 To obj.Count - 1

            stringQuery += "<row "
            stringQuery += " SheetPlaningID =""" & obj(i).SheetPlaningID & """"
            stringQuery += " SymbolNumber =""" & obj(i).SheetPlaningID & """"
            stringQuery += " IsPresent =""" & obj(i).IsPresent & """"
            stringQuery += " SubjectID =""" & obj(i).SubjectID & """"
            stringQuery += " UserID =""" & obj(i).UserID & """"
            stringQuery += " />"
        Next

        stringQuery += "</attendance>"

        Dim sql As String = "EXEC [Exam].[usp_ExamAttendanceSheet]  @Flag='i'"
        sql += ", @insertQuery='" & stringQuery & "'"
        ds = Dao.ExecuteDataset(sql)

        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function SaveStudentsForSheetPlan(ByVal obj As SaveStudentsForSheetPlan()) As ErrorMessage Implements ISetupServices.SaveStudentsForSheetPlan
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim SectionID As String = obj(i).SectionID
            Dim examId As String = obj(i).ExamId
            Dim SheetPlanGroupSetupID As String = obj(i).SheetPlanGroupSetupID
            Dim SubjectId As String = obj(i).SubjectId
            Dim user As String = obj(i).CreatedBy

            If (obj(i).StudentId <> "") Then

                Dim sql As String = "EXEC [Exam].[usp_SheetPlanStudentGrouping]  @Flag='i'"
                sql += ", @BatchId=" + Dao.FilterQuote(batchId)
                sql += ", @StudentID=" + Dao.FilterQuote(studentId)
                sql += ", @SectionIDs=" + Dao.FilterQuote(SectionID)
                sql += ", @ExamID=" + Dao.FilterQuote(examId)
                sql += ", @SheetPlanGroupSetupID=" + Dao.FilterQuote(SheetPlanGroupSetupID)
                sql += ", @SubjectId=" + Dao.FilterQuote(SubjectId)
                sql += ", @ExamDate=" + Dao.FilterQuote(obj(i).ExamDate)
                sql += ", @user=" + Dao.FilterQuote(user)

                ds = Dao.ExecuteDataset(sql)
            End If
        Next
        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    'GROUP DIRECT ASSESSMENT STUDENT LIST
    Public Function GetGroupCASEntryStudents(ByVal batchId As String, ByVal sortyBy As String, ByVal SubjectID As String, ByVal SectionID As String, ByVal studentId As String) As GroupCASEntryStudents() Implements ISetupServices.GetGroupCASEntryStudents
        Dim sql As String = "EXEC [Exam].[usp_GroupCASEntry]  @Flag='s'"
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        sql += ", @sortyBy=" + Dao.FilterQuote(sortyBy)
        sql += ", @SubjectID=" + Dao.FilterQuote(SubjectID)
        sql += ", @SectionID=" + Dao.FilterQuote(SectionID)
        sql += ", @studentId=" + Dao.FilterQuote(studentId)

        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As GroupCASEntryStudents() = New GroupCASEntryStudents(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New GroupCASEntryStudents()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                jsonArray(i).AssessmentCategoryID = rs("assCatIDs").ToString()
                jsonArray(i).AssessmentCategoryName = rs("assCatNames").ToString()
                i = i + 1
            Next
        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function

    Public Function SaveGroupCASEntry(ByVal obj As SaveDirectAssessment()) As ErrorMessage Implements ISetupServices.SaveGroupCASEntry
        Dim ds As New DataSet
        Dim message As String = ""
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim subjectId As String = obj(i).SubjectId
            Dim examId As String = obj(i).ExamId
            Dim assementCategoryID As String = obj(i).AssessmentCategoryID
            Dim fullMarks As String = obj(i).FullMarks
            Dim user As String = obj(i).CreatedBy

            If (obj(i).ObtainedMarks <> "" Or obj(i).ObtainedMarks <> "") Then
                Dim ObtainMarks As String = obj(i).ObtainedMarks

                Dim sql As String = "EXEC [Exam].[usp_GroupCASEntry]  @Flag='i'"
                sql += ", @BatchId=" + Dao.FilterQuote(batchId)
                sql += ", @StudentID=" + Dao.FilterQuote(studentId)
                sql += ", @SubjectID=" + Dao.FilterQuote(subjectId)
                sql += ", @ExamID=" + Dao.FilterQuote(examId)
                sql += ", @AssessmentCategoryID=" + Dao.FilterQuote(assementCategoryID)
                sql += ", @ObtainedMarks=" + Dao.FilterQuote(ObtainMarks)
                sql += ", @user=" + Dao.FilterQuote(user)
                sql += ", @FullMarks=" + Dao.FilterQuote(fullMarks)

                ds = Dao.ExecuteDataset(sql)

                If ds.Tables(0).Rows(0).Item("errorCode").ToString() = "11" Then
                    message += ds.Tables(0).Rows(0).Item("mes").ToString() & " " & Environment.NewLine
                End If

            End If
        Next
        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ErrorMessage
        'JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        JsonArray.Message = "Record Successfully Inserted. " & Environment.NewLine & IIf(String.IsNullOrWhiteSpace(message), "", "Partial Assessment Category found for student(s) :" & Environment.NewLine & message)

        Return JsonArray
    End Function


    Public Function SaveContinuousAssessment(ByVal obj As SaveContinuousAssessment()) As ErrorMessage Implements ISetupServices.SaveContinuousAssessment
        Dim ds As New DataSet

        Dim stringQuery As String = Nothing
        Dim sql As String = Nothing

        Dim studentId As String = Nothing
        Dim batchId As String = Nothing
        Dim subjectId As String = Nothing
        Dim assementCategoryID As String = Nothing
        Dim DateFrom As String = Nothing
        Dim DateTo As String = Nothing
        Dim user As String = Nothing

        stringQuery = "<assessment>"

        For i As Integer = 0 To obj.Count - 1
            studentId = obj(i).StudentId
            batchId = obj(i).BatchId
            subjectId = obj(i).SubjectId
            assementCategoryID = obj(i).AssessmentCategoryID
            DateFrom = obj(i).DateFrom
            DateTo = obj(i).DateTo
            user = obj(i).CreatedBy

            stringQuery += "<row "

            If (obj(i).ObtainedMarks <> "" Or obj(i).ObtainedMarks <> "") Then
                Dim ObtainMarks As String = obj(i).ObtainedMarks

                stringQuery += " SID =""" + studentId + """"
                stringQuery += " marks =""" + ObtainMarks + """"
            End If

            stringQuery += " />"
        Next
        stringQuery += "</assessment>"

        sql = "EXEC [Exam].[usp_ContinuousAssessment]  @Flag='i'"
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        'sql += ", @StudentID=" + Dao.FilterQuote(studentId)
        sql += ", @SubjectID=" + Dao.FilterQuote(subjectId)
        sql += ", @AssessmentCategoryID=" + Dao.FilterQuote(assementCategoryID)
        'sql += ", @ObtainedMarks=" + Dao.FilterQuote(ObtainMarks)
        sql += ", @user=" + Dao.FilterQuote(user)
        sql += ", @DateFrom=" + Dao.FilterQuote(DateFrom)
        sql += ", @DateTo=" + Dao.FilterQuote(DateTo)
        sql += ", @sql=" + Dao.FilterQuote(stringQuery)

        ds = Dao.ExecuteDataset(sql)
        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

#End Region

    Public Function GetExamStudents(ByVal examId As String, ByVal batchId As String, ByVal semester As String, ByVal section As String,
                                    ByVal sortyBy As String, ByVal subject As String, ByVal studentId As String) As ExamStudents() Implements ISetupServices.GetExamStudents
        Dim sql As String = "EXEC [Exam].[usp_ExamTimeTable]  @Flag='" & sortyBy & "', @BatchId='" & batchId & "', @SemesterId='" & semester & "', @SectionId='" & section & "', @SubjectId='" + subject + "'"
        sql += ",@ExamID = '" + examId + "'"
        sql += ",@StudentID = '" + studentId + "'"
        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As ExamStudents() = New ExamStudents(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0
            'If ds.Tables(0).Rows(0).Item("ErrorCode") = 1 Then
            '    jsonArray(i) = New ExamStudents()
            '    jsonArray(0).StudentId = 0
            '    jsonArray(0).StudentName = ds.Tables(0).Rows(0).Item("mes").ToString
            'Else
            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New ExamStudents()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                jsonArray(i).FullMarksTheory = rs("FullMarksTheory").ToString()
                jsonArray(i).FullMarksPractical = rs("FullMarksPractical").ToString()
                jsonArray(i).PassMarksTheory = rs("PassMarksTheory").ToString()
                jsonArray(i).PassMarksPractical = rs("PassMarksPractical").ToString()
                i = i + 1
            Next
            ' End If

        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function



    Public Function GetExamStudentsw(ByVal examId As String, ByVal batchId As String, ByVal semester As String, ByVal section As String,
                                    ByVal sortyBy As String, ByVal subject As String, ByVal studentId As String) As ExamStudents() Implements ISetupServices.GetExamStudentsW
        Dim sql As String = "EXEC [Exam].[usp_ExamTimeTable]  @Flag='" & sortyBy & "', @BatchId='" & batchId & "', @SemesterId='" & semester & "', @SectionId='" & section & "', @SubjectId='" + subject + "'"
        sql += ",@ExamID = '" + examId + "'"
        sql += ",@StudentID = '" + studentId + "'"
        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As ExamStudents() = New ExamStudents(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New ExamStudents()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                jsonArray(i).FullMarksTheory = rs("FullMarksTheory").ToString()
                jsonArray(i).FullMarksPractical = rs("FullMarksPractical").ToString()
                jsonArray(i).PassMarksTheory = rs("PassMarksTheory").ToString()
                jsonArray(i).PassMarksPractical = rs("PassMarksPractical").ToString()
                i = i + 1
            Next

        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function
    Public Function ObtainMarkEntry(ByVal obj As ObtainMarkEntry()) As ObtainMarkEntry Implements ISetupServices.ObtainMarkEntry
        Dim ds As New DataSet
        Dim JsonArray As New ObtainMarkEntry
        Try


            For i As Integer = 0 To obj.Count - 1
                Dim studentId As String = obj(i).StudentId
                Dim batchId As String = obj(i).BatchId
                Dim subjectId As String = obj(i).SubjectId
                Dim examId As String = obj(i).ExamId
                Dim branchId As String = obj(i).BranchID
                Dim SemesterId As String = obj(i).semeterid
                Dim userId As String = obj(i).UserID
                If (obj(i).ObtainedMarksTheory <> "" Or obj(i).ObtainedPracticalMarks <> "") Then
                    Dim theory As String = obj(i).ObtainedMarksTheory
                    Dim practical As String = obj(i).ObtainedPracticalMarks
                    Dim sql As String = "EXEC [Exam].[usp_ObtainedMarkEntry] @Flag='i', @BatchID='" & batchId & "',@SemesterId='" & SemesterId & "', @StudentID='" & studentId & "', @SubjectID='" & subjectId & "', @ExamTypeID='" & examId & "', @ObtainedMarksTheory='" & theory & "', @ObtainedPracticalMarks='" & practical & "'"
                    sql += ",@CreatedBy='" & userId & "',@BranchID='" & branchId & "'"
                    ds = Dao.ExecuteDataset(sql)
                End If
            Next
            'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}

            JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()


        Catch ex As Exception
            JsonArray.Message = "error"
        End Try
        Return JsonArray
    End Function

    Public Function ObtainMarkEntryForTrainee(ByVal obj As ObtainMarkEntry()) As ObtainMarkEntry Implements ISetupServices.ObtainMarkEntryForTrainee
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim subjectId As String = obj(i).SubjectId
            Dim examId As String = obj(i).ExamId
            If (obj(i).ObtainedMarksTheory <> "" Or obj(i).ObtainedPracticalMarks <> "") Then
                Dim theory As String = obj(i).ObtainedMarksTheory
                Dim practical As String = obj(i).ObtainedPracticalMarks
                Dim sql As String = "EXEC [Students].[usp_PTAMarksheet]  @Flag='i',  @Traineeid='" & studentId & "', @EvaluationID='" & subjectId & "', @PublishedTrainingID='" & examId & "', @Marks='" & theory & "'"
                ds = Dao.ExecuteDataset(sql)
            End If
        Next
        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ObtainMarkEntry
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function SaveSubjectRegistration(ByVal obj As SaveSubjectRegistration()) As ErrorMessage Implements ISetupServices.SaveSubjectRegistration
        Dim ds As New DataSet
        Dim JsonArray As New ErrorMessage
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim subjectIds As String = obj(i).AssignSub
            Dim userId As String = obj(i).UserId
            Dim branchId As String = obj(i).BranchID

            Dim sql As String = "EXEC exam.usp_SubjectRegistration @Flag='i', @BatchID='" & batchId & "', @StudentID='" & studentId & "', @SubjectIDs='" & subjectIds & "', @semesterId='" & semesterId & "',@user='" & userId & "'"
            sql += ",@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString().Trim
                JsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorCode").ToString().Trim
            Else
                JsonArray.Message = "Error in subject registration."
                JsonArray.ErrorCode = "1"
            End If
        Next

        'JsonArray.Message = "Subject assigned successfully"
        'JsonArray.ErrorCode = "0"
        Return JsonArray
    End Function

    Public Function GetEvaluationTrainee(ByVal examId As String, ByVal sortyBy As String, ByVal subject As String, ByVal studentId As String) As ExamTrainee() Implements ISetupServices.GetEvaluationTrainee
        Dim sql As String = "EXEC [Students].[usp_PublishTraining] @Flag = 'TraineeList'"
        sql += ",@TrainingTitleID = '" + examId + "'"
        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As ExamTrainee() = New ExamTrainee(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0
            'If ds.Tables(0).Rows(0).Item("ErrorCode") = 1 Then
            '    jsonArray(i) = New ExamStudents()
            '    jsonArray(0).StudentId = 0
            '    jsonArray(0).StudentName = ds.Tables(0).Rows(0).Item("mes").ToString
            'Else
            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New ExamTrainee()
                jsonArray(i).TraineeID = rs("TraineeID").ToString
                jsonArray(i).TraineeName = rs("Name").ToString
                jsonArray(i).symbolNo = rs("SymbolNo").ToString()
                jsonArray(i).FullMarksTheory = 100 'rs("FullMarksTheory").ToString()
                jsonArray(i).FullMarksPractical = 100 'rs("FullMarksPractical").ToString()
                jsonArray(i).PassMarksTheory = 40 'rs("PassMarksTheory").ToString()
                jsonArray(i).PassMarksPractical = 40 'rs("PassMarksPractical").ToString()
                i = i + 1
            Next
            ' End If

        Else
            jsonArray(0).TraineeID = 0.ToString()
            jsonArray(0).TraineeName = "Error Loading Content"
        End If
        Return jsonArray
    End Function

    Public Function SaveExamSubjectRegistration(ByVal obj As SaveExamSubjectRegistration()) As ErrorMessage Implements ISetupServices.SaveExamSubjectRegistration
        Dim ds As New DataSet
        Dim JsonArray As New ErrorMessage
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim subjectIds As String = obj(i).AssignSub
            Dim userId As String = obj(i).UserId
            Dim examId As String = obj(i).ExamId
            Dim branchId As String = obj(i).BranchID

            Dim sql As String = "EXEC exam.usp_ExamSubjectRegistration @Flag='i', @BatchID='" & batchId & "', @StudentID='" & studentId & "', @SubjectIDs='" & subjectIds & "', @semesterId='" & semesterId & "',@user='" & userId & "'"
            sql += ", @ExamId=" & examId
            sql += ",@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString().Trim
                JsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorCode").ToString().Trim
            Else
                JsonArray.Message = "Error in subject registration."
                JsonArray.ErrorCode = "1"
            End If
        Next
        'Dim JsonArray As New ErrorMessage
        'JsonArray.Message = "Subject assigned successfully"
        'JsonArray.ErrorCode = "0"
        Return JsonArray
    End Function

    Public Function ObtainGradeEntry(ByVal obj As ObtainGradeEntry()) As ObtainGradeEntry Implements ISetupServices.ObtainGradeEntry
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim subjectId As String = obj(i).SubjectId
            Dim examId As String = obj(i).ExamId
            Dim user As String = obj(i).User

            If (obj(i).ObtainedGrade <> "") Then
                Dim grade As String = obj(i).ObtainedGrade
                Dim sql As String = "EXEC [Exam].[usp_SubjectGradeEntry] @Flag='i', @BatchID='" & batchId & "', @StudentID='" & studentId & "', @SubjectID='" & subjectId & "'"
                sql += ",@ExamID=" + Dao.FilterQuote(examId)
                sql += ",@ObtainedGrade=" + Dao.FilterQuote(grade)
                sql += ",@User=" + Dao.FilterQuote(user)
                ds = Dao.ExecuteDataset(sql)
            End If
        Next
        'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
        Dim JsonArray As New ObtainGradeEntry
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function
    Public Function UpdateTag(ByVal StudentID As String, ByVal TagID As String) As String Implements ISetupServices.UpdateTag
        Dim ds As New DataSet
        Dim sql As String = "exec students.usp_DeviceTag  @Flag='us' , @ApplicationID='" & TagID & "', @studentID='" & StudentID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function UpdateTheoryMarks(ByVal obtainMarkId As String, ByVal ObtainedTheory As String, ByVal UserID As String) As String Implements ISetupServices.UpdateTheoryMarks
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_ObtainedMarkEntry] @Flag='UpdateTheory', @ObtainedMarkEntryID = '" & obtainMarkId & "', @ObtainedMarksTheory = '" & ObtainedTheory & "'"
        sql += ",@ModifiedBy='" & UserID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePraticalMarks(ByVal obtainMarkId As String, ByVal ObtainedPratical As String, ByVal UserID As String) As String Implements ISetupServices.UpdatePraticalMarks
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_ObtainedMarkEntry] @Flag='UpdatePratical', @ObtainedMarkEntryID = '" & obtainMarkId & "', @ObtainedPracticalMarks = '" & ObtainedPratical & "'"
        sql += ",@ModifiedBy='" & UserID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateTheoryMarksSub(ByVal sn As String, ByVal FullTheory As String) As String Implements ISetupServices.UpdateTheoryMarksSub
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_SubjectMarkList] @Flag='FMT', @SN = '" & sn & "', @FullMarksTheory = '" & FullTheory & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePracticalMarksSub(ByVal sn As String, ByVal FullPractical As String) As String Implements ISetupServices.UpdatePracticalMarksSub
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_SubjectMarkList] @Flag='FMP', @SN = '" & sn & "', @FullMarksPractical = '" & FullPractical & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePassMarksSub(ByVal sn As String, ByVal PassTheory As String) As String Implements ISetupServices.UpdatePassMarksSub
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_SubjectMarkList] @Flag='PMT', @SN = '" & sn & "', @PassMarksTheory = '" & PassTheory & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePracticalPassMarksSub(ByVal sn As String, ByVal PassPractical As String) As String Implements ISetupServices.UpdatePracticalPassMarksSub
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_SubjectMarkList] @Flag='PMP', @SN = '" & sn & "', @PassMarksPractical = '" & PassPractical & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateCAS(ByVal DirectAssementID As String, ByVal ObtainedMarks As String) As String Implements ISetupServices.UpdateCAS
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_DirectAssessment] @Flag='u', @DirectAssementID = '" & DirectAssementID & "', @ObtainedMarks = '" & ObtainedMarks & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    'Public Function ObtainGradeEntry(ByVal obj As UpdateObtainGradeEntry()) As ErrorMessage Implements ISetupServices.UpdateObtainGradeEntry
    '    Dim ds As New DataSet
    '    For i As Integer = 0 To obj.Count - 1
    '        Dim SubjectGradeEntryID As String = obj(i).SubjectGradeEntryID
    '        Dim ObtainedGrade As String = obj(i).ObtainedGrade
    '        Dim user As String = obj(i).User

    '        If (obj(i).ObtainedGrade <> "") Then
    '            Dim grade As String = obj(i).ObtainedGrade
    '            Dim sql As String = "EXEC [Exam].[usp_SubjectGradeEntry] @Flag='u'"
    '            sql += ",@ObtainedGrade=" + Dao.FilterQuote(grade)
    '            sql += ",@User=" + Dao.FilterQuote(user)
    '            sql += ",@SubjectGradeEntryID=" + Dao.FilterQuote(SubjectGradeEntryID)
    '            ds = Dao.ExecuteDataset(sql)
    '        End If
    '    Next
    '    'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
    '    Dim JsonArray As New ErrorMessage
    '    JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

    '    Return JsonArray
    'End Function
    Public Function GetSubjectGradeFroEdit(ByVal batchId As String, ByVal semester As String, ByVal section As String,
                                    ByVal sortyBy As String, ByVal subject As String) As GetSubjectGradeFroEdit() Implements ISetupServices.GetSubjectGradeFroEdit
        Dim sql As String = "EXEC [Exam].[usp_SubjectGradeEntry]  @Flag='" & sortyBy & "'"
        sql += ", @BatchId=" + Dao.FilterQuote(batchId)
        sql += ", @SemesterId=" + Dao.FilterQuote(semester)
        sql += ", @SectionId=" + Dao.FilterQuote(section)
        sql += ", @SubjectId=" + Dao.FilterQuote(subject)

        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        Dim jsonArray As GetSubjectGradeFroEdit() = New GetSubjectGradeFroEdit(ds.Tables(0).Rows.Count - 1) {}
        If ds.Tables.Count > 0 Then
            Dim i As Integer = 0

            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New GetSubjectGradeFroEdit()
                jsonArray(i).StudentId = rs("StudentID").ToString
                jsonArray(i).StudentName = rs("StudentName").ToString
                jsonArray(i).RollNo = rs("RollNo").ToString()
                jsonArray(i).SubjectGradeEntryID = rs("SubjectGradeEntryID").ToString()
                jsonArray(i).ObtainedGrade = rs("ObtainedGrade").ToString()
                i = i + 1
            Next
        Else
            jsonArray(0).StudentId = 0.ToString()
            jsonArray(0).StudentName = "Error Loading Content"
        End If
        Return jsonArray
    End Function

    Public Function GetExamGroupingList(ByVal fromYearID As String, ByVal toYearID As String, ByVal flag As String, ByVal HaveWeightage As String) As GetExamGroupingList() Implements ISetupServices.GetExamGroupingList
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [exam].[usp_Grouping]  @flag=" + Dao.FilterQuote(flag)
        sql += ",@fromYearID = " + Dao.FilterQuote(fromYearID)
        sql += ",@toYearID = " + Dao.FilterQuote(toYearID)
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As GetExamGroupingList() = New GetExamGroupingList(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New GetExamGroupingList()
            JsonArray(i).ExamID = rs("ExamID").ToString
            JsonArray(i).ExamName = rs("ExamName").ToString
            JsonArray(i).AcademicYearID = rs("AcademicYearID").ToString
            JsonArray(i).AcademicTitle = rs("AcademicTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function
    'Public Function UpdateGroupingExam(ByVal obj As UpdateGroupingExam()) As ErrorMessage Implements ISetupServices.UpdateGroupingExam
    '    Dim ds As New DataSet

    '    Dim ExamGroupID As String = ""
    '    Dim ExamIDChk As String = ""
    '    Dim AcademicYearIDChk As String = ""
    '    Dim msg As String = ""

    '    For i As Integer = 0 To obj.Count - 1
    '        ExamIDChk += obj(i).ExamID + ","
    '        AcademicYearIDChk += obj(i).AcademicYearID + ","
    '    Next

    '    For i As Integer = 0 To obj.Count - 1
    '        Dim ExamID As String = obj(i).ExamID
    '        Dim Weightage As String = obj(i).Weightage
    '        Dim AcademicYearID As String = obj(i).AcademicYearID
    '        Dim MergeMarks As String = obj(i).MergeMarks
    '        Dim GroupPurpose As String = obj(i).GroupPurpose
    '        Dim HaveWeightage As String = obj(i).HaveWeightage
    '        Dim GroupName As String = obj(i).GroupName
    '        Dim user As String = obj(i).User

    '        Dim sql As String = "EXEC [Exam].[usp_Grouping] @Flag='i'"
    '        sql += ",@GroupName=" + Dao.FilterQuote(GroupName)
    '        sql += ",@GroupPurpose=" + Dao.FilterQuote(GroupPurpose)
    '        sql += ",@AcademicYearID=" + Dao.FilterQuote(AcademicYearID)
    '        sql += ",@ExamID=" + Dao.FilterQuote(ExamID)
    '        sql += ",@HaveWeightage=" + Dao.FilterQuote(HaveWeightage)
    '        sql += ",@Weightage=" + Dao.FilterQuote(Weightage)
    '        sql += ",@MergeMarks=" + Dao.FilterQuote(MergeMarks)
    '        sql += ",@User=" + Dao.FilterQuote(user)
    '        sql += ",@ExamGroupID=" + Dao.FilterQuote(ExamGroupID)
    '        sql += ",@ExamIDChk=" + Dao.FilterQuote(ExamIDChk)
    '        sql += ",@AcademicYearIDChk=" + Dao.FilterQuote(AcademicYearIDChk)

    '        ds = Dao.ExecuteDataset(sql)

    '        If ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "0" Then
    '            ExamIDChk = ""
    '            AcademicYearIDChk = ""
    '            ExamGroupID = ds.Tables(0).Rows(0).Item("id").ToString()
    '            msg = ds.Tables(0).Rows(0).Item("mes").ToString()
    '        ElseIf ds.Tables(0).Rows(0).Item("ErrorCode").ToString() = "1" Then
    '            msg = ds.Tables(0).Rows(0).Item("mes").ToString()
    '            Exit For
    '        Else
    '            msg = ds.Tables(0).Rows(0).Item("mes").ToString()
    '        End If

    '    Next
    '    'Dim JsonArray As ObtainMarkEntry() = New ObtainMarkEntry(ds.Tables(0).Rows.Count - 1) {}
    '    Dim JsonArray As New ErrorMessage
    '    JsonArray.Message = msg

    '    Return JsonArray
    'End Function

    Public Function ViewExamGroupingList() As ViewExamGroupingList() Implements ISetupServices.ViewExamGroupingList
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [exam].[usp_Grouping]  @flag='groupList'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ViewExamGroupingList() = New ViewExamGroupingList(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ViewExamGroupingList()
            JsonArray(i).ExamGroupID = rs("ExamGroupID").ToString
            JsonArray(i).GroupName = rs("GroupName").ToString
            JsonArray(i).UserName = rs("UserName").ToString
            JsonArray(i).CreatedDate = rs("CreatedDate").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function
    Public Function ViewExamGroupingByID(ByVal ExamGroupID As String) As ViewExamGroupingByID() Implements ISetupServices.ViewExamGroupingByID
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [exam].[usp_Grouping]  @flag='groupDetail'"
        sql += ",@ExamGroupID=" + Dao.FilterQuote(ExamGroupID)
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ViewExamGroupingByID() = New ViewExamGroupingByID(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ViewExamGroupingByID()
            JsonArray(i).GroupingID = rs("GroupingID").ToString
            JsonArray(i).GroupName = rs("GroupName").ToString
            JsonArray(i).GroupPurpose = rs("GroupPurpose").ToString
            JsonArray(i).AcademicYearID = rs("AcademicYearID").ToString
            JsonArray(i).ExamName = rs("ExamName").ToString
            JsonArray(i).Weightage = rs("Weightage").ToString
            JsonArray(i).HaveWeightage = rs("HaveWeightage").ToString
            JsonArray(i).MergeMarks = rs("MergeMarks").ToString
            JsonArray(i).UserName = rs("UserName").ToString
            JsonArray(i).CreatedDate = rs("CreatedDate").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function DeleteExamGroup(ByVal ExamGroupID As String, ByVal DeletedBy As String) As ErrorMessage Implements ISetupServices.DeleteExamGroup
        Dim ds As New DataSet

        Dim sql As String = "EXEC [Exam].[usp_Grouping] @Flag='d'"
        sql += ",@ExamGroupID=" + Dao.FilterQuote(ExamGroupID)
        sql += ",@User=" + Dao.FilterQuote(DeletedBy)
        ds = Dao.ExecuteDataset(sql)

        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function
    Public Function SaveStudentsGroupMarks(ByVal ExamGroupID As String, ByVal BatchID As String, ByVal UserID As String) As ErrorMessage Implements ISetupServices.SaveStudentsGroupMarks
        Dim ds As New DataSet

        Dim sql As String = "EXEC [Exam].[usp_GroupMarks] @Flag='i'"
        sql += ",@ExamGroupID=" + Dao.FilterQuote(ExamGroupID)
        sql += ",@BatchID=" + Dao.FilterQuote(BatchID)
        sql += ",@User=" + Dao.FilterQuote(UserID)
        ds = Dao.ExecuteDataset(sql)

        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

        Return JsonArray
    End Function

    Public Function GetStudentsGroupMarks(ByVal ExamGroupID As String, ByVal BatchID As String) As String Implements ISetupServices.GetStudentsGroupMarks
        Dim ds As New DataSet

        Dim sql As String = "EXEC [Exam].[usp_GroupMarks] @Flag='loadMarks'"
        sql += ",@ExamGroupID=" + Dao.FilterQuote(ExamGroupID)
        sql += ",@BatchID=" + Dao.FilterQuote(BatchID)
        ds = Dao.ExecuteDataset(sql)

        Dim result As String

        If ds.Tables.Count = 0 Then
            Return ""
        End If

        result = GetJson(ds.Tables(0))
        Return result
    End Function

    '' PARSE DATATABLE TO JSON 
    Public Function GetJson(ByVal dt As DataTable) As String
        Dim NumberText As String

        Dim sb As StringBuilder = New StringBuilder("")
        Dim sbSecHeader As StringBuilder = New StringBuilder("")

        '' FOR PRACTICAL AND THEORY
        If dt.Columns.Contains("pStudentID") Then
            sb.AppendLine("<thead class='flip-content'><tr>")
            sb.AppendLine("<th rowspan='2'>S.N </th>")
            sb.AppendLine("<th rowspan='2'>Student Name</th>")
            sb.AppendLine("<th rowspan='2'>Exam Name</th>")

            sbSecHeader.AppendLine("<th >&nbsp;</th>")
            sbSecHeader.AppendLine("<th >&nbsp;</th>")
            sbSecHeader.AppendLine("<th >&nbsp;</th>")

            For index = 2 To dt.Columns.Count - 1 Step 1
                If dt.Columns(index).ToString().ToLower() <> "studentid" Then
                    If dt.Columns(index).ToString().ToLower() <> "pstudentid" Then
                        Dim colume As String
                        colume = dt.Columns(index).ToString() + "_TH"

                        If dt.Columns.Contains(colume) Then
                            sb.AppendLine("<th colspan='3'>" + dt.Columns(index).ToString() + "</th>")
                            sbSecHeader.AppendLine("<th >Theory</th>")
                            sbSecHeader.AppendLine("<th >Practical</th>")
                            sbSecHeader.AppendLine("<th >Total</th>")

                        ElseIf Not colume.Contains("_TH_TH") Then
                            sb.AppendLine("<th>" + dt.Columns(index).ToString() + "</th>")
                            sbSecHeader.AppendLine("<th >&nbsp;</th>")

                        End If
                    End If
                End If
            Next

            sb.AppendLine("</tr></thead>")
            sb.AppendLine("<tr>")
            sb.AppendLine(sbSecHeader.ToString())
            sb.AppendLine("</tr>")


            '''' for generating body
            For index = 0 To dt.Rows.Count - 1 Step 1
                sb.AppendLine("<tbody><tr>")
                sb.AppendLine("<td>" + (index + 1).ToString() + "</td>")

                For j = 0 To dt.Columns.Count - 1 Step 1
                    NumberText = ""
                    If j > 1 Then
                        NumberText = "class='NumberText'"
                    End If

                    Dim total As Decimal
                    If dt.Columns(j).ToString().ToLower() <> "studentid" Then
                        If dt.Columns(j).ToString().ToLower() <> "pstudentid" Then
                            Dim colume As String
                            colume = dt.Columns(j).ToString() + "_TH"

                            If dt.Columns.Contains(colume) Then
                                total += Convert.ToDecimal(dt.Rows(index)(j).ToString()) + Convert.ToDecimal(dt.Rows(index)(colume).ToString())

                                sb.AppendLine("<td " + NumberText + ">" + dt.Rows(index)(j).ToString() + "</td>")
                                sb.AppendLine("<td " + NumberText + ">" + dt.Rows(index)(colume).ToString() + "</td>")
                                sb.AppendLine("<td class='TotalText'>" + total.ToString() + "</td>")
                                total = 0
                            ElseIf Not colume.Contains("_TH_TH") Then
                                sb.AppendLine("<td " + NumberText + ">" + dt.Rows(index)(j).ToString() + "</td>")
                            End If
                        End If
                    End If
                Next

                sb.AppendLine("</tr></tbody>")
            Next

        Else
            '' FOR NON PRACTICAL 
            ''''  generating header
            sb.AppendLine("<thead class='flip-content'><tr>")
            sb.AppendLine("<th >S.N </th>")
            For index = 0 To dt.Columns.Count - 1 Step 1
                If dt.Columns(index).ToString().ToLower() <> "studentid" Then
                    sb.AppendLine("<th>" + dt.Columns(index).ToString() + "</th>")
                End If
            Next
            sb.AppendLine("</tr></thead>")
            '''' for generating body
            For index = 0 To dt.Rows.Count - 1 Step 1
                sb.AppendLine("<tbody><tr>")
                sb.AppendLine("<td>" + (index + 1).ToString() + "</td>")

                For j = 0 To dt.Columns.Count - 1 Step 1
                    NumberText = ""
                    If j > 1 Then
                        NumberText = "class='NumberText'"
                    End If
                    If dt.Columns(j).ToString().ToLower() <> "studentid" Then
                        sb.AppendLine("<td " + NumberText + ">" + dt.Rows(index)(j).ToString() + "</td>")
                    End If
                Next
                sb.AppendLine("</tr></tbody>")
            Next
        End If

        Return sb.ToString()
    End Function


#End Region


#Region "MediaGallery"
    Function PhotoGallery(ByVal ImageName As String) As String Implements ISetupServices.PhotoGallery

        Dim myPath As String = System.Web.Hosting.HostingEnvironment.MapPath(ImageName)
        File.Delete(myPath)

        Try

        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function
#End Region


#Region "New Functions || Developer : Bhuban Shrestha || Date : 7/25/2014"
    'Bhuban Shrestha

    Function GetStudentInfo() As StudentParameter() Implements ISetupServices.GetStudentInfo
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Students].[usp_GetStudentInfo] @flag='p'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As StudentParameter() = New StudentParameter(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New StudentParameter()
            JsonArray(i).StudentID = rs("StudentID").ToString
            JsonArray(i).StudentName = rs("StudentName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function



    Function GetStudentBatch(ByVal studentId As Integer) As StudentBatch() Implements ISetupServices.GetStudentBatch
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Students].[usp_GetStudentInfo] @flag='b' @StudentId='" & studentId & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As StudentBatch() = New StudentBatch(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New StudentBatch()
            JsonArray(i).BatchId = rs("BatchId").ToString
            JsonArray(i).BatchTitle = rs("BatchTitle").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

#End Region


    'Public Function UserLogin1(ByVal username As String, ByVal pp As String) As LoginUser() Implements ISetupServices.UserLogin

    'End Function


    Function GetSection() As SectionService() Implements ISetupServices.GetSection
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_section @flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As SectionService() = New SectionService(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New SectionService()
            JsonArray(i).SectionID = rs("SectionID").ToString
            JsonArray(i).Section = rs("Section").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function GetCustomers(ByVal BatchID As String) As List(Of ListItem) Implements ISetupServices.GetCustomers
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Section] @flag='b', @BatchID='" & BatchID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim customers As New List(Of ListItem)()
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            customers.Add(New ListItem() With { _
                         .Value = ds.Tables(0).Rows(i).Item("SectionID").ToString, _
                         .Text = ds.Tables(0).Rows(i).Item("Section").ToString _
                       })
        Next
        Return customers
    End Function

    Public Function GetClass() As List(Of ListItem) Implements ISetupServices.GetClass
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Setup].[usp_Batch] @Flag='s'"
        ds = Dao.ExecuteDataset(sql)
        Dim customers As New List(Of ListItem)()
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            customers.Add(New ListItem() With { _
                         .Value = ds.Tables(0).Rows(i).Item("BatchID").ToString, _
                         .Text = ds.Tables(0).Rows(i).Item("BatchTitle").ToString _
                       })
        Next
        Return customers
    End Function

    Function SaveMiniSchoolStudent(ByVal FirstName As String,
                              ByVal MiddleName As String,
                              ByVal LastName As String,
                              ByVal Gender As String,
                              ByVal DateOfBirth As String,
                              ByVal Address As String,
                              ByVal Status As String,
                              ByVal UserID As String,
                              ByVal FatherName As String,
                              ByVal FatherMobile As String,
                              ByVal RollNo As String,
                              ByVal S_Class As String,
                              ByVal Section As String,
                              ByVal RegistrationNumber As String) As String Implements ISetupServices.SaveMiniSchoolStudent

        FirstName = FirstName.Replace("'", "''")
        MiddleName = MiddleName.Replace("'", "''")
        LastName = LastName.Replace("'", "''")
        Gender = Gender.Replace("'", "''")
        UserID = UserID.Replace("'", "''")
        DateOfBirth = DateOfBirth.Replace("'", "''")
        Address = Address.Replace("'", "''")
        Status = Status.Replace("'", "''")
        FatherName = FatherName.Replace("'", "''")
        FatherMobile = FatherMobile.Replace("'", "''")
        RollNo = RollNo.Replace("'", "''")
        S_Class = S_Class.Replace("'", "''")
        Section = Section.Replace("'", "''")
        RegistrationNumber = RegistrationNumber.Replace("'", "''")

        '  Dim Dob = Helper.GetFrontToDbDate(txt_Dob.Text)
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Students].[minischoolInsert]  @FirstName='" & FirstName & "',@MiddleName='" & MiddleName & "' ,@LastName='" & LastName & "' ,@BatchID='" & S_Class & "' ,@DOB='" & DateOfBirth & "' ,@RegistrationNumber='" & RegistrationNumber & "', @userID= '" & UserID & "',@Gender='" & Gender & "',@RollNo='" & RollNo & "',@FatherMobile='" & FatherMobile & "',@FatherName='" & FatherName & "',@Section='" & Section & "',@Address='" & Address & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function
    ''' <summary>
    ''' om khadka
    ''' </summary>
    ''' <remarks></remarks>
    ''' 

    Public Function UpdatePreference(ByVal obj As String) As String Implements ISetupServices.UpdatePreference
        'StartRegistrationNo, IsIncrementRegdNo, DropOutDays, IsOneTimeIncomeTaxApplied, isautovoucherno
        If Not String.IsNullOrWhiteSpace(obj) Then
            Dim sql As String
            obj = obj.Remove(obj.Length - 1, 1)
            Dim ds As New DataSet
            sql = "exec accounts.usp_preference @Flag='u',@sql='" & obj & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If
        End If
        Return "--"
    End Function

    Public Function SaveBankVoucher(ByVal StudentID As String, ByVal VoucherNo As String, ByVal BankName As String, ByVal BankAddress As String, ByVal VoucherAmount As String, ByVal VoucherDate As String, ByVal DepositBy As String, ByVal Remarks As String, ByVal sCompanyID As String, ByVal sBranchID As String, ByVal sFiscalYearID As String, ByVal sAcademicYearID As String, ByVal sUserId As String) As String Implements ISetupServices.SaveBankVoucher
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_BankVouchers]  @Flag='i',@StudentID='" & StudentID & "',	@VoucherNo='" & VoucherNo & "',	@BankName='" & BankName & "',	@BankAddress='" & BankAddress & "',	@VoucherAmount='" & VoucherAmount & "',	@VoucherDate='" & VoucherDate & "',	@DepositBy='" & DepositBy & "',	@CreatedByID='" & sUserId & "',	@Remarks ='" & Remarks & "'"
        sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@FiscalYearID='" & sFiscalYearID & "',@AcademicYearID='" & sAcademicYearID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function


    Public Function SaveNote(ByVal StudentID As String, ByVal Note As String, ByVal Isdisplay As String, ByVal sCompanyID As String, ByVal sBranchID As String, ByVal sFiscalYearID As String, ByVal sAcademicYearID As String, ByVal sUserId As String) As String Implements ISetupServices.SaveNote
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_notes]   @Flag='i',@StudentID='" & StudentID & "',	@Notes='" & Note & "',	@IsDisplay='" & Isdisplay & "',	@CreatedByID='" & sUserId & "'"
        sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@FiscalYearID='" & sFiscalYearID & "',@AcademicYearID='" & sAcademicYearID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function

    Public Function SaveScholarshipOrDiscount(ByVal StudentID As String, ByVal SchemeID As String, ByVal discountPercent As String, ByVal discountAmt As String, ByVal ParticularID As String, ByVal sBranchID As String, ByVal sCompanyID As String, ByVal sUserID As String) As String Implements ISetupServices.SaveScholarshipOrDiscount
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_ScholarshipScheme] @flag='e',@StudentID='" & StudentID & "',@SchemeName='" & SchemeID & "', @DiscountPercentage='" & discountPercent & "', @DiscountAmount='" & discountAmt & "',@DiscountOnParticularID='" & ParticularID & "'"
            sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@CreatedBy='" & sUserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function DeleteNote(ByVal NoteID As String) As String Implements ISetupServices.DeleteNote
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_notes]   @Flag='d',@noteID='" & NoteID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function

    Public Function SymbolNumber(ByVal SymbolNumberID As String) As String Implements ISetupServices.SymbolNumber
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Exam].[usp_TuSymbolNumber]   @Flag='up',@SymbolNumberID='" & SymbolNumberID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function
    Public Function SymbolNumberInsert(ByVal BatchID As String, ByVal SemesterID As String, ByVal SectionID As String, ByVal ExamID As String, ByVal StudentID As String, ByVal SymberNumber As String, ByVal sUserId As String, ByVal BranchID As String) As String Implements ISetupServices.SymbolNumberInsert
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Exam].[usp_TuSymbolNumber]   @Flag='up',@BatchID ='" & BatchID & "', @SemesterID ='" & SemesterID & "', @SectionID ='" & SectionID & "', @ExamID ='" & ExamID & "', @StudentID ='" & StudentID & "', @SymberNumber ='" & SymberNumber & "', @UserID ='" & sUserId & "'"
        sql += ",@BranchID='" & BranchID & "',@CreatedBy='" & sUserId & "',@ModifiedBy='" & sUserId & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function

    Public Function UpdateRollNo(ByVal RollNo As String, ByVal StudentID As String) As String Implements ISetupServices.UpdateRollNo
        Dim sql As String
        Dim ds As New DataSet
        If RollNo = "null" Or RollNo = "" Then
            Return "--"
        End If
        sql = "exec [Students].[usp_GetStudentsDetails]    @Flag='r',@RollNo='" & RollNo & "',@StudentID='" & StudentID & "'"
        ds = Dao.ExecuteDataset(sql)


        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function

    Public Function UpdateUniversityReg(ByVal reg As String, ByVal studentID As String) As String Implements ISetupServices.UpdateUniversityReg
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec students.UpdateRegistration  @univercityReg='" & reg & "',@studentID='" & studentID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function

    Public Function AssignStudentFeeGroup(ByVal FeeGroupID As String, ByVal StudentID As String, ByVal BatchID As String, ByVal SemesterID As String) As String Implements ISetupServices.AssignStudentFeeGroup
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Students].[usp_AssignGroup] @StudentGroupID='" & FeeGroupID & "',@StudentID='" & StudentID & "',@BatchID='" & BatchID & "',@SemesterID='" & SemesterID & "'"
            ds = Dao.ExecuteDataset(sql)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If
        Catch ex As Exception
            Return "error"
        End Try
    End Function


    Function GetStudentInfoDetails(ByVal studentID As String) As String Implements ISetupServices.GetStudentInfoDetails
        Dim sql As String
        Dim ds As New DataSet
        Dim StdInfo As String = String.Empty
        sql = "select FullName,RegistrationNumber,BatchTitle,SemesterName,Section,RollNo,Address1,MobileNo from Students.StudentDetails where StudentID='" & studentID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                StdInfo += ds.Tables(0).Rows(0)(0).ToString.Trim
                StdInfo += " / Regd#:" & ds.Tables(0).Rows(0)(1).ToString.Trim
                StdInfo += " / " & ds.Tables(0).Rows(0)(2).ToString.Trim
                StdInfo += " / " & ds.Tables(0).Rows(0)(3).ToString.Trim
                StdInfo += " / " & ds.Tables(0).Rows(0)(4).ToString.Trim
                StdInfo += " / Roll#:" & ds.Tables(0).Rows(0)(5).ToString.Trim
                StdInfo += " / Address:" & ds.Tables(0).Rows(0)(6).ToString.Trim
                StdInfo += " / Mobile:" & ds.Tables(0).Rows(0)(7).ToString.Trim

                Return StdInfo
            End If
        Catch ex As Exception
            Return "Error"
        End Try


    End Function

    Public Function UpdateMenuID(ByVal MenuID As String, ByVal UserID As String, ByVal isActive As String) As String Implements ISetupServices.UpdateMenuID
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Users].[usp_AssignMenusToUser] @flag='update',  @Menuid='" & MenuID & "',@UserID='" & UserID & "',@isActive='" & isActive & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "0"
        End If

        Return "0"
    End Function


    Public Function UpdateMenuByModuleID(ByVal UserName As String, ByVal isActive As String, ByVal ModuleID As String) As String Implements ISetupServices.UpdateMenuByModuleID
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Users].[usp_AssignMenusToUser] @flag='updateModule',  @ModuleId='" & ModuleID & "',@UserName='" & UserName & "',@isActive='" & isActive & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "0"
        End If

        Return "0"
    End Function


    Private Function PostSendSMS(ByVal [to] As String, ByVal text As String) As String Implements ISetupServices.PostSendSMS
        Dim responseString As String
        Try

            'Return "You have not registerd as sms user."
            Dim token As String = "DkQpbUwUjIqH0MbK6ERY"
            Dim from As String = "Onver"
            Using client = New WebClient()
                Dim values = New NameValueCollection()
                values("from") = from
                values("token") = token
                values("to") = [to]
                values("text") = text
                Dim response = client.UploadValues("http://api.sparrowsms.com/v2/sms/", "Post", values)
                responseString = Encoding.[Default].GetString(response)
                Return responseString
            End Using

        Catch ex As Exception
            Return responseString
        End Try
    End Function


    Public Function GetChartData(ByVal BranchID As String, ByVal Flag As String, ByVal FiscalYearID As String) As Array Implements ISetupServices.GetChartData
        Dim sql As String
        Dim ds As New DataSet
        Dim jsonArray As Array = {Nothing, Nothing, Nothing}
        Dim chartData As String = Nothing


        sql = " exec [Students].[usp_HighDashboard] @flag='" & Flag & "',@BranchID='" & BranchID & "',@FiscalyearID='" & FiscalYearID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                chartData += "{""name"" : """ + ds.Tables(0).Rows(0).Item("xname").ToString + """,""colorByPoint"":""true"","
                chartData += """data"":["
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i = ds.Tables(0).Rows.Count - 1 Then
                        chartData += "{""name"":""" + ds.Tables(0).Rows(i).Item("Name").ToString + """"
                        chartData += ",""y"":" + ds.Tables(0).Rows(i).Item("Y").ToString + "}"

                    Else
                        chartData += "{""name"":""" + ds.Tables(0).Rows(i).Item("Name").ToString + """"
                        chartData += ",""y"":" + ds.Tables(0).Rows(i).Item("Y").ToString + "},"
                    End If
                Next
                chartData += "]}"



                jsonArray(0) = "{""chartData"" : [" + chartData + "]}"
                jsonArray(1) = ds.Tables(0).Rows(0).Item("ChartType").ToString.Trim
                jsonArray(2) = ds.Tables(0).Rows(0).Item("Title").ToString.Trim

                Return jsonArray
            End If
        Catch ex As Exception

        End Try
    End Function


    Public Function UpdateCampusOtherInfo(ByVal ColumnToModify As String, ByVal ValueToSet As String) As String Implements ISetupServices.UpdateCampusOtherInfo
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_InstitutionalInfo] @Flag='e',@columnname='" & ColumnToModify & "',@valuetoset='" & ValueToSet & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return ""
        End If

        Return ""
    End Function

    Public Function SaveSubjectAttendance(ByVal StudentID As String, ByVal SubjectID As String, ByVal UserID As String, ByVal subAtt As String) As String Implements ISetupServices.SaveSubjectAttendance
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Exam].[usp_SubjectAttendanceUGC]  @flag='i',@StudentID='" & StudentID & "',@SubjectID='" & SubjectID & "',@UserID='" & UserID & "',@SubAtt='" & subAtt & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return ""
        End If

        Return ""
    End Function

    Public Function AddHostelStudent(ByVal StudentIDs As String) As String Implements ISetupServices.AddHostelStudent
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Hostel].[usp_AddStudent]  @flag='i',@StudentID='" & StudentIDs & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return ""
        End If

        Return ""
    End Function

    Public Function AssignBedToHostelStudent(ByVal HostelStudentIDs As String, ByVal RoomIDs As String, ByVal BedIDs As String) As String Implements ISetupServices.AssignBedToHostelStudent
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Hostel].[usp_AddStudent]  @flag='AssignBed',@HostelStudentID='" & HostelStudentIDs & "',@RoomID='" & RoomIDs & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return ""
        End If

        Return ""
    End Function

    Public Function RemoveBedToStudent(ByVal BedIds As String) As String Implements ISetupServices.RemoveBedToStudent
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Hostel].[usp_AddStudent]  @flag='RemoveBed',@BedID='" & BedIds & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return ""
        End If

        Return ""
    End Function

    Public Function IsActiveHostelStudent(ByVal ISHostelIDs As String, ByVal ISHostelStudentIDs As String) As String Implements ISetupServices.IsActiveHostelStudent
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Hostel].[usp_AddStudent]  @flag='Activehstl',@isActivehostel='" & ISHostelIDs & "',@HostelStudentID='" & ISHostelStudentIDs & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return ""
        End If

        Return ""
    End Function

#Region "Internal Exam Methods"
    Public Function UpdateExamReg(ByVal value As String, ByVal SubjectID As String, ByVal StudentID As String, ByVal BatchID As String, ByVal SemesterID As String, ByVal ExamID As String, ByVal UserID As String, ByVal BranchID As String, ByVal AcademicYearID As String) As String Implements ISetupServices.UpdateExamReg
        Dim sql As String
        Dim ds As New DataSet
        If value = "null" Or value = "" Then
            Return "--"
        End If
        If IsNumeric(value) And value.Length = 1 Then


            sql = "exec exam.usp_listExamRegistration @Flag='u',@subjectID='" & SubjectID & "', @BatchID='" & BatchID & "',@SemesterID='" & SemesterID & "',@userID='" & UserID & "',@ExamID='" & ExamID & "',@AcademicYearID='" & AcademicYearID & "', @BranchID='" & BranchID & "', @value='" & value & "',@StudentID='" & StudentID & "'"
            ds = Dao.ExecuteDataset(sql)


            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If
        Else
            Return "Invalid Input"
        End If
        Return "--"
    End Function

#End Region

#Region "Cheque Methods"
    Public Function ReconciliateCheques(ByVal ChequeNos As String, ByVal UserID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.ReconciliateCheques
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_ChequeReconciliation] @flag='reconciliate',@ChequesNos='" & ChequeNos & "',@BranchID='" & BranchID & "',@UserId='" & UserID & "',@FiscalYearID='" & FiscalYearID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If

            Return " "
        Catch ex As Exception

        End Try
    End Function


    Public Function GetChequePayee(ByVal SessionID As String, ByVal UserID As String) As String Implements ISetupServices.GetChequePayee
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Try
            sql = "exec [Accounts].[usp_ChequeEntry] @Flag='s',@SessionID='" & SessionID & "',@UserID='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine(" <div class='col-md-12'>")
                sb.AppendLine("  <div class='portlet box green'>")
                sb.AppendLine(" <div class='portlet-title'>")
                sb.AppendLine("  <div class='caption'>")
                sb.AppendLine(" <i class='fa fa-user'></i>List of Cheque Payees</div>")
                sb.AppendLine(" <div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;' class='reload'></a>")
                sb.AppendLine("  </div>")
                sb.AppendLine("  </div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")
                sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.Append("<th>SN</th>")
                sb.Append("<th>Pay To</th>")
                sb.Append("<th>Amount</th>")
                sb.Append("<th>Cheque No.</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")

                sb.AppendLine("<tbody>")
                For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim jvID = ds.Tables(0).Rows(r).Item("JournalVourcherID").ToString
                    Dim subaccountid = ds.Tables(0).Rows(r).Item("SubaccountId").ToString
                    Dim payto = ds.Tables(0).Rows(r).Item("SubAccountHead").ToString
                    Dim Amt = ds.Tables(0).Rows(r).Item("amount").ToString
                    Dim chqno = ds.Tables(0).Rows(r).Item("chequeno").ToString

                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td>" & r + 1 & "</td>")
                    sb.AppendLine("<td>" & payto & "</td>")
                    sb.AppendLine("<td>" & Amt & "</td>")
                    sb.AppendLine("<td class='text-center'><a  id='edit' class='editchqno' href='#' data-id='" & jvID & "'>" & chqno & "</a></td>")
                    sb.AppendLine("</tr>")
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")

                Return sb.ToString()
            End If
        Catch ex As Exception

        End Try
    End Function


    Public Function UpdateChequeNoInTempJV(ByVal jvId As String, ByVal ChequeNo As String) As String Implements ISetupServices.UpdateChequeNoInTempJV
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_ChequeEntry] @flag='u',@ChequeNo='" & ChequeNo & "',@JournalVourcherID='" & jvId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If

            Return " "
        Catch ex As Exception

        End Try
    End Function
#End Region

#Region "Student Methods"
    Public Function ValidateApplicationFormNo(ByVal ApplicationFormNo As String, ByVal BranchID As String) As String Implements ISetupServices.ValidateApplicationFormNo
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Students].[usp_ValidateStudentEntry] @flag='validateAppNo',@ApplicationFormNo=N'" & ApplicationFormNo & "',@BranchID='" & BranchID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0).Item("ErrorCode").ToString.Contains("1")) Then
                    Return ds.Tables(0).Rows(0).Item("mes").ToString.Trim
                Else
                    Return ""
                End If
            End If

        Catch ex As Exception
            Return "error: " & ex.Message
        End Try
    End Function

    Public Function ValidateMobileNo(ByVal MobileNo As String) As String Implements ISetupServices.ValidateMobileNo
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Students].[usp_ValidateStudentEntry] @flag='validateMobileNo',@MobileNo=N'" & MobileNo & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0).Item("ErrorCode").ToString.Contains("1")) Then
                    Return ds.Tables(0).Rows(0).Item("mes").ToString.Trim
                Else
                    Return ""
                End If
            End If

        Catch ex As Exception
            Return "error: " & ex.Message
        End Try
    End Function

#End Region

#Region "Accounts >> Billing Methods"
    Function UpdateParticularAmount(ByVal CreditBillParticularID As String, ByVal Amount As String) As String Implements ISetupServices.UpdateParticularAmount
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Accounts].[usp_CreditBillParticulars]  @Flag='a',@CreditBillParticularID='" & CreditBillParticularID & "',@Amount='" & Amount & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to complete operation."
        End If

        Return " "
    End Function

    Function UpdateBillProgram(ByVal ReferenceNo As String, ByVal ProgramID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.UpdateBillProgram
        Dim sql As String
        Dim ds As New DataSet
        Try
            If IsNumeric(ProgramID) And ProgramID.Length = 1 And ProgramID <= 2 Then
                sql = "exec [Accounts].[usp_modifyBillProgram] @flag='edit',@ProgramID='" & ProgramID & "',@ReferenceNo='" & ReferenceNo & "',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "'"
                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                    Return mes
                Else
                    Return "Uable to save records"
                End If
            Else
                Return "Invalid Data.ProgramID can take only two values 1 or 2."
            End If
            Return " "
        Catch ex As Exception
            Return "error"
        End Try
        
    End Function
#End Region

#Region "Account>> Vouchers Methods By Subarna"
    Function PostMergeIncomeVoucher(ByVal ReferenceNo As String, ByVal UserID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.PostMergeIncomeVoucher
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_MergeIncomeVoucher] @Flag ='p', @reference='" & ReferenceNo & "',@UserID='" & UserID & "',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "Unable to post vocher due to error.Error Detail : " & ex.Message
        End Try
    End Function

    Function PostIncomeVoucher(ByVal ReferenceNo As String, ByVal MonthID As String, ByVal VType As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.PostIncomeVoucher
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_postApprove_IncomeVoucher] @Flag ='p', @reference='" & ReferenceNo & "',@monthId='" & MonthID & "',@VType='" & VType & "',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "Unable to post vocher due to error.Error Detail : " & ex.Message
        End Try
    End Function

    Function PostIncomeVoucher_ByProgram(ByVal ReferenceNo As String, ByVal MonthID As String, ByVal VType As String, ByVal ProgramID As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.PostIncomeVoucher_ByProgram
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_post_IncomeVoucher_ByProgram] @Flag ='p', @reference='" & ReferenceNo & "',@monthId='" & MonthID & "',@VType='" & VType & "',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "'"
            sql += ",@ProgramID='" & ProgramID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "Unable to post vocher due to error.Error Detail : " & ex.Message
        End Try
    End Function

    Function PostDaywiseIncomeVoucher(ByVal ReferenceNo As String, ByVal MonthID As String, ByVal DayID As String, ByVal VType As String, ByVal BranchID As String, ByVal FiscalYearID As String) As String Implements ISetupServices.PostDaywiseIncomeVoucher
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_postApprove_DayIncomeVoucher] @Flag ='p', @reference='" & ReferenceNo & "',@monthId='" & MonthID & "',@VType='" & VType & "',@BranchID='" & BranchID & "',@FiscalYearID='" & FiscalYearID & "'"
            sql += ",@dayID='" & DayID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "Unable to post vocher due to error.Error Detail : " & ex.Message
        End Try
    End Function
#End Region


#Region "User Management and Security By Subarna"
    Public Function ChangePassword(ByVal userId As String, ByVal oldpassword As String, ByVal newpassword As String) As ErrorMessage Implements ISetupServices.ChangePassword
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Dim jsonArray As New ErrorMessage
        Try
            If String.IsNullOrWhiteSpace(oldpassword) Or String.IsNullOrWhiteSpace(newpassword) Then
                mes = "Password can be empty."
                jsonArray.ErrorCode = 1
                jsonArray.Message = mes
            Else
                sql = "exec [Management].[usp_UserInfo] @Flag='changePassword',@UserID='" & userId & "',@Password='" & oldpassword & "',@NewPassword='" & newpassword & "'"
                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    mes = ds.Tables(0).Rows(0).Item("mes").ToString
                    jsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorCode").ToString.Trim
                    jsonArray.Message = mes
                Else
                    mes = "Uable to save"
                    jsonArray.ErrorCode = 1
                    jsonArray.Message = mes
                End If
            End If
            Return jsonArray
        Catch ex As Exception
            jsonArray.ErrorCode = 1
            jsonArray.Message = "Error"
            Return jsonArray
        End Try
    End Function

    Function SetAccountLock(ByVal UserID As String) As LockMessage Implements ISetupServices.SetAccountLock
        Dim sql As String
        Dim ds As New DataSet
        Dim jsonArray As New LockMessage
        Try
            sql = "exec [Setup].[usp_ACLOCK] @Flag = 'i',@UserID='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                jsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorcode").ToString.Trim
                jsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString
                jsonArray.LockCode = ds.Tables(0).Rows(0).Item("Lockcode").ToString.Trim
            End If
            Return jsonArray
        Catch ex As Exception
            jsonArray.ErrorCode = 1
            jsonArray.Message = "Error"
            jsonArray.LockCode = ""
            Return jsonArray
        End Try
    End Function

    Function UnLockAccount(ByVal UserID As String, ByVal LockCode As String) As LoginMessage() Implements ISetupServices.UnLockAccount
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Dim returnMsg As LoginMessage()
        Try
            sql = "exec [Setup].[usp_ACLOCK] @Flag = 'unlock',@UserID='" & UserID & "',@LockCode='" & LockCode & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("errorcode").ToString = "0" Then
                    returnMsg = New LoginMessage(1) {}
                    returnMsg(0) = New LoginMessage()
                    returnMsg(0).message = ds.Tables(0).Rows(0).Item("mes").ToString.Trim()
                    returnMsg(0).role = ds.Tables(0).Rows(0).Item("Role").ToString.Trim()
                    returnMsg(0).branch = ds.Tables(0).Rows(0).Item("Branch").ToString.Trim()
                    returnMsg(0).errorcode = ds.Tables(0).Rows(0).Item("errorcode").ToString()
                    returnMsg(0).userid = ds.Tables(0).Rows(0).Item("UserID").ToString()
                    returnMsg(0).Type = ds.Tables(0).Rows(0).Item("Category").ToString()
                    ' returnMsg(i).YearID = rows("YearID").ToString
                    returnMsg(0).DatabaseName = ds.Tables(0).Rows(0).Item("DatabaseName").ToString
                    ' returnMsg(i).FiscalYearID = rows("FiscalYearID").ToString
                    returnMsg(0).CompanyID = ds.Tables(0).Rows(0).Item("CompanyID").ToString.Trim()
                Else
                    returnMsg = New LoginMessage(1) {}
                    returnMsg(0) = New LoginMessage()
                    returnMsg(0).message = ds.Tables(0).Rows(0).Item("mes").ToString()
                    returnMsg(0).errorcode = "2"
                End If
            End If
        Catch ex As Exception
            returnMsg = New LoginMessage(1) {}
            returnMsg(0) = New LoginMessage()
            returnMsg(0).message = ex.Message.ToString() & " INNER EXCEPTION " & ex.InnerException.ToString()
            returnMsg(0).errorcode = "2"
        End Try
        Return returnMsg
    End Function

    Function CheckLockStatus(ByVal UserID As String) As Boolean Implements ISetupServices.CheckLockStatus
        Dim sql As String
        Dim ds As New DataSet
        Dim status As Boolean
        Try
            sql = "exec [Setup].[usp_ACLOCK] @Flag = 'ini',@UserID='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                status = Convert.ToBoolean(ds.Tables(0).Rows(0).Item("IsActive"))
            End If
            Return status
        Catch ex As Exception
            Return False
        End Try
    End Function

    Function DeactivateLock(ByVal UserID As String) As String Implements ISetupServices.DeactivateLock
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Setup].[usp_ACLOCK] @Flag = 'button_update',@UserID='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    'Function SendMail_ACLOCK(ByVal UserID As String) As String Implements ISetupServices.SendMail_ACLOCK
    '    Dim sql = "exec [Management].[usp_SendMail_ACLOCK] @UserID ='" & UserID & "'"
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(sql)
    '    Dim m As String = ""
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        Dim bodyMessage As String = ds.Tables(0).Rows(0).Item("mes").ToString
    '        Dim subject As String = "Mitra EMIS Lock Code"
    '        Dim emailto As String = ds.Tables(0).Rows(0).Item("Email").ToString
    '        m = Email.SendByCDO(emailto, subject, bodyMessage)
    '        If m = "OK" Then
    '            m = "Mail Successfully sent."
    '        End If
    '    End If
    '    Return m
    'End Function

    Function GetMenusByModuleID(ByVal ModuleID As String) As String Implements ISetupServices.GetMenusByModuleID
        Dim sql2 As String
        Dim ds2 As New DataSet
        Dim sb As New StringBuilder
        Try

            sql2 = "exec [Users].[usp_AssignMenusToUser] @flag='userpermission',@ModuleId='" & ModuleID & "'"
            ds2 = Dao.ExecuteDataset(sql2)
            If ds2.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed menutable' cellspacing='0' width='80%'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>SN</th>")
                For c As Integer = 0 To ds2.Tables(0).Columns.Count - 1
                    If c = 0 Then
                        sb.AppendLine("<th class='text-center'>" & ds2.Tables(0).Columns(c).ColumnName.ToString & "</th>")
                    Else
                        sb.AppendLine("<th class='text-center'><button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='top' data-html='true' title='Click the User Name to grant all permissions of this module.'><a  href='javascript:;' module-id='" & ModuleID & "' class='menutitle' >" & ds2.Tables(0).Columns(c).ColumnName.ToString & "</a></button></th>")
                    End If

                Next
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")

                For x As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                    sb.AppendLine("<tr>")
                    sb.Append("<td class='text-center'>" & x + 1 & "</td>")
                    For y As Integer = 0 To ds2.Tables(0).Columns.Count - 1
                        If y > 0 Then

                            If Not String.IsNullOrWhiteSpace(ds2.Tables(0).Rows(x).Item(y).ToString()) Then
                                Dim arrIsActive As String() = ds2.Tables(0).Rows(x).Item(y).ToString().Split("/")
                                If arrIsActive(2) = "1" Then
                                    sb.AppendLine("<td class='text-center'><input type='checkbox' class='assign' checked  data-toggle='toggle' data-size='mini' user-id='" & arrIsActive(0).ToString & "' menu-id='" & arrIsActive(1).ToString & "'></td>")
                                Else
                                    sb.AppendLine("<td class='text-center'><input type='checkbox' class='assign' data-toggle='toggle' data-size='mini'  user-id='" & arrIsActive(0).ToString & "' menu-id='" & arrIsActive(1).ToString & "'></td>")
                                End If

                            Else
                                ' sb.AppendLine("<td class='text-center'><input type='checkbox' id='toggle-trigger'  data-toggle='toggle' data-size='mini'  user-id='" & arrIsActive(0).ToString & "' menu-id='" & arrIsActive(1).ToString & "'></td>")
                            End If
                        Else
                            sb.AppendLine("<td class='text-left'>" & ds2.Tables(0).Rows(x).Item(y).ToString().Trim & "</td>")
                        End If

                    Next
                    sb.AppendLine("</tr>")
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                Return sb.ToString()

            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region


#Region "Misc. Income Billing By Subarna"

    Function CreateMiscAccount(ByVal AccountName As String, ByVal Amount As String, ByVal BranchID As String, ByVal UserID As String) As String Implements ISetupServices.CreateMiscAccount
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_MiscIncome] @flag='createaccount',@AccountName=N'" & AccountName & "',@amount='" & Amount & "',@BranchID='" & BranchID & "',@UserId='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If

            Return " "
        Catch ex As Exception
            Return "Error while saving data"
        End Try
    End Function


    Function AssignMiscAmount(ByVal AccountID As String, ByVal Amount As String, ByVal BranchID As String, ByVal UserID As String) As String Implements ISetupServices.AssignMiscAmount
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Accounts].[usp_MiscIncome] @flag='assignAmount',@particularAccId='" & AccountID & "',@amount='" & Amount & "',@BranchID='" & BranchID & "',@UserId='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                Return mes
            Else
                Return "Uable to save records"
            End If

            Return " "
        Catch ex As Exception

        End Try
    End Function

    Function GetMiscParticularAmount(ByVal BranchID As String) As String Implements ISetupServices.GetMiscParticularAmount
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder

        sql = "exec [Accounts].[usp_MiscIncome] @flag='v',@BranchID='" & BranchID & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine(" <div class='col-md-12'>")
                sb.AppendLine("  <div class='portlet box green'>")
                sb.AppendLine(" <div class='portlet-title'>")
                sb.AppendLine("  <div class='caption'>")
                sb.AppendLine(" <i class='fa fa-cogs'></i>Misc. Particulars Assgined Info</div>")
                sb.AppendLine(" <div class='tools'>")
                sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
                sb.AppendLine("  </div>")
                sb.AppendLine("  </div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'>")
                sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.AppendLine("<th>S.N</th>")
                sb.AppendLine("<th>Particular</th>")
                sb.AppendLine("<th>Amount</th>")
                sb.AppendLine("<th>Action</th>")
                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")
                sb.AppendLine("<tbody>")
                Dim i As Integer = 0
                For Each row As DataRow In ds.Tables(0).Rows
                    Dim assignId = ds.Tables(0).Rows(i).Item("AssignId").ToString.Trim
                    Dim particularName = ds.Tables(0).Rows(i).Item("AccountName").ToString.Trim
                    Dim particularAmt = ds.Tables(0).Rows(i).Item("Amount").ToString.Trim

                    sb.AppendLine("<tr><td class='text-center'>" & (i + 1) & ".</td>")
                    sb.AppendLine("<td>" & particularName & "</td>")
                    sb.AppendLine("<td class='text-right'><a  data-id='" & assignId & "'  id='link1' class='editMiscAmt' href='javascript:;'>" & particularAmt & "</a></td>")
                    sb.AppendLine("<td class='text-center'><a  data-id='" & assignId & "' id='deleteid' class='deleteMisAmt btn btn-danger btn-xs' href='javascript:;'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                    sb.AppendLine("</tr>")

                    i = i + 1
                Next
                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")
                sb.AppendLine("</div>")
                sb.AppendLine(" </div>")

                Return sb.ToString
            End If

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Function edit_MiscAmount(ByVal assignId As String, ByVal val As String, ByVal sUserId As String) As String Implements ISetupServices.edit_MiscAmount
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_MiscIncome] @flag='editAmount',@AssignId='" & assignId & "', @amount=N'" & val & "',@userID='" & sUserId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Function delete_MisAmount(ByVal assignId As String) As String Implements ISetupServices.delete_MisAmount
        Dim sql As String
        Dim ds As New DataSet
        sql = "EXEC  [Accounts].[usp_MiscIncome] @flag='delMisc',@AssignId='" & assignId & "'"
        Try
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If

        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region


#Region "Library Methods By Subarna"

    Public Function GetBookClassification(ByVal isbn As String) As String Implements ISetupServices.GetBookClassification

        ' Dim isbn = "9781890774691"
        Dim ds As New DataSet
        ds.ReadXml("http://classify.oclc.org/classify2/Classify?isbn=" & isbn & "&summary=true")
        Dim ddc As String = ""
        Dim bookno As String
        If ds.Tables().Count > 6 Then
            Dim Title As String = ds.Tables("work").Rows(0).Item("title").ToString
            Dim Author As String = ds.Tables("work").Rows(0).Item("author").ToString
            ddc = ds.Tables("mostPopular").Rows(0).Item("nsfa").ToString
            Dim isbn1 As String = ds.Tables("input").Rows(0).Item("input_text").ToString
            'txtclass.Text = ddc
            'txtisbn.Text = isbn1
            'txtbookno.Text = Author.Substring(3, Author.Length) & "-" & Title.Substring(1, Title.Length())
            bookno = Author.Substring(0, Author.Length - (Author.Length - 3)).ToUpper & "-" & Title.Substring(0, Title.Length() - (Title.Length() - 1)).ToLower
        End If
        Return ddc & ":" & bookno
    End Function

    Public Function CheckIfExistsBookAccessionNo(ByVal Accession As String) As String Implements ISetupServices.CheckIfExistsBookAccessionNo
        Try
            Dim sql As String
            Dim ds As New DataSet
            sql = "exec [Library].[usp_BookInfo] @Flag='check',@AccessionNo='" & Accession & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0).Item("ErrorCode").ToString.Contains("1")) Then
                    Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                    Return mes
                Else
                    Return "Two segments of accession numbers must be equal."
                End If
            End If
        Catch ex As Exception
            Return "Two segments of accession numbers must be equal."
        End Try
    End Function

    Public Function CheckIfExistsISBN(ByVal ISBN As String) As String Implements ISetupServices.CheckIfExistsISBN
        Try
            Dim sql As String
            Dim ds As New DataSet
            sql = "exec [Library].[usp_BookInfo] @Flag='isbncheck',@ISBN='" & ISBN & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0).Item("ErrorCode").ToString = "0") Then
                    Return ds.Tables(0).Rows(0).Item("id").ToString
                Else
                    Return ds.Tables(0).Rows(0).Item("id").ToString
                End If
            Else
                Return "0"
            End If

        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Public Function GetFineCollectReport() As LibraryFineCollect() Implements ISetupServices.GetFineCollectReport
        Try
            Dim sql As String
            Dim ds As New DataSet
            sql = "exec [Library].[usp_FinePivot] @flag='monthly'"
            ds = Dao.ExecuteDataset(sql)
            Dim i As Integer = 0
            Dim JsonArray As LibraryFineCollect() = New LibraryFineCollect(ds.Tables(0).Rows.Count - 1) {}

            For Each row As DataRow In ds.Tables(0).Rows
                JsonArray(i) = New LibraryFineCollect()
                JsonArray(i).SN = i + 1
                JsonArray(i).LibraryMemberID = row("LibraryMemberID").ToString
                JsonArray(i).MemberCode = row("MemberCode").ToString
                JsonArray(i).MemberName = row("MemberName").ToString
                JsonArray(i).PaidAmount = row("paid").ToString
                JsonArray(i).PendingAmount = row("pending").ToString
                JsonArray(i).DuesAmount = row("Dues").ToString
                JsonArray(i).MonthName = row("MonthName").ToString

                i = i + 1
            Next
            Return JsonArray
        Catch ex As Exception

        End Try
    End Function

    Public Function SendDuesToAccount(ByVal MemberCode As String, ByVal AccessionNo As String, ByVal Amount As Decimal, ByVal UserID As String) As String Implements ISetupServices.SendDuesToAccount
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Library].[usp_sendDuestoAccount] @Flag='send',@Barcode='" & MemberCode & "',@AccessionNo='" & AccessionNo & "',@Amount='" & Amount & "',@userId='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return ""
        Catch ex As Exception
            Return "Error :" & ex.Message
        End Try
    End Function

    Public Function ReviseDueAmount(ByVal FineDetailID As String, ByVal Amount As String) As String Implements ISetupServices.ReviseDueAmount
        Dim sql As String
        Dim ds As New DataSet
        Try
            If Not String.IsNullOrWhiteSpace(Amount) Then
                sql = "exec [Library].[usp_Maintenance] @flag='revisefine',@libraryfinedetail='" & FineDetailID & "',@Amount='" & Amount & "'"
                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds.Tables(0).Rows(0).Item("mes").ToString
                End If
            End If
            Return ""
        Catch ex As Exception
            Return "Error :" & ex.Message
        End Try
    End Function

    Public Function GetStockDetails(ByVal keyValue As String, ByVal searchValue As String, ByVal header As String) As String Implements ISetupServices.GetStockDetails
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Try
            sql = "exec [Library].[usp_StockDetails] @keyvalue='" & keyValue & "',@searchvalue='" & searchValue & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine(" <i class='fa fa-list'></i>Stock Details By " & header & " </div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='javascript:;' class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'  runat='server'>")
                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content detailstable' id='displaytable1'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>")
                sb.Append("<TH>SN</TH>")

                For c As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<th>" & ds.Tables(0).Columns(c).ColumnName.ToString.Trim & "</th>")
                Next

                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")

                sb.AppendLine("<tbody>")

                For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td class='text-center'>" & r + 1 & "</td>")
                    For c As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        sb.AppendLine("<td>" & ds.Tables(0).Rows(r).Item(c).ToString.Trim & "</td>")
                    Next
                    sb.AppendLine("</tr>")
                Next

                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                Return sb.ToString()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                Return sb.ToString()
            End If
        Catch ex As Exception
            Return "Error " + ex.Message.ToString.Trim
        End Try
    End Function
#End Region

#Region "HR Methods"
    Public Function UpdateEmployeeHierarchy(ByVal EmployeeID As String, ByVal Value As String) As String Implements ISetupServices.UpdateEmployeeHierarchy
        Dim ds As New DataSet
        Dim sql As String
        Try
            sql = "exec [HR].[usp_EmployeeHierarchy] @flag='u',@EmployeeId='" & EmployeeID & "',@EmployeeOrder='" & Value & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item(1).ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try

    End Function
#End Region

#Region "Payroll Methods"

    Function ApprovedPayrollForThisMonth(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String Implements ISetupServices.ApprovedPayrollForThisMonth
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_postPayrollToVoucher] @Flag ='PostVoucherApprov', @monthID='" & MonthID & "',@UserID='" & UserID & "',@branchID='" & BranchID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function ApprovedPayrollForThisMonth_new(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String Implements ISetupServices.ApprovedPayrollForThisMonth_new
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_postPayrollToVoucher_new] @Flag ='PostVoucherApprov', @monthID='" & MonthID & "',@UserID='" & UserID & "',@branchID='" & BranchID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "error"
        End Try
    End Function


    Function FetchFromSetupVoucher(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String Implements ISetupServices.FetchFromSetupVoucher
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_postPayrollToVoucher] @Flag ='v', @monthID='" & MonthID & "',@UserID='" & UserID & "',@branchID='" & BranchID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Function FetchFromSetupVoucher_new(ByVal MonthID As String, ByVal UserID As String, ByVal BranchID As String) As String Implements ISetupServices.FetchFromSetupVoucher_new
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_postPayrollToVoucher_new] @Flag ='v', @monthID='" & MonthID & "',@UserID='" & UserID & "',@branchID='" & BranchID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString
            End If
            Return mes
        Catch ex As Exception
            Return "error"
        End Try
    End Function
    Public Function UpdateDeductiveAmount(ByVal DeductiblesToPayrollID As String, ByVal DeductiveAmount As String) As String Implements ISetupServices.UpdateDeductiveAmount
        Dim ds As New DataSet
        Dim sql As String = "exec [Accounts].[usp_DeductiblesToPayroll] @Flag='r', @DeductiblesToPayrollID = '" & DeductiblesToPayrollID & "', @DeductibleAmount = '" & DeductiveAmount & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateAddableAmount(ByVal AddablesToPayrollID As String, ByVal AddableAmount As String) As String Implements ISetupServices.UpdateAddableAmount
        Dim ds As New DataSet
        Dim sql As String = "exec [Accounts].[usp_AddablestoPayroll] @Flag='r', @AddablesToPayrollID = '" & AddablesToPayrollID & "', @AddableAmount = '" & AddableAmount & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function GetEmployeeInfo(ByVal YearID As String, ByVal MonthID As String, ByVal AddableID As String, ByVal JobID As String, ByVal IsZero As Boolean, ByVal sBranchID As String, Optional ByVal type As String = "") As EmployeeInfos() Implements ISetupServices.GetEmployeeInfo
        Dim sql As String = ""
        If type = "d" Then
            sql = "exec [Accounts].[usp_DeductiblesToPayroll]  @Flag='v',@MonthID='" & MonthID & "',@YearID='" & YearID & "',@JobTitleID='" & JobID & "',@DeductibleID='" & AddableID & "',@BranchID='" & sBranchID & "'"
        Else
            sql = "exec [Accounts].[usp_AddablestoPayroll]  @Flag='v',@MonthID='" & MonthID & "',@YearID='" & YearID & "',@JobTitleID='" & JobID & "',@AddableID='" & AddableID & "',@BranchID='" & sBranchID & "'"
        End If

        Dim ds As New DataSet
        Try
            ds = Dao.ExecuteDataset(sql)
            Dim jsonArray As EmployeeInfos() = New EmployeeInfos(ds.Tables(0).Rows.Count - 1) {}
            Dim i As Integer = 0
            For Each rs As DataRow In ds.Tables(0).Rows
                jsonArray(i) = New EmployeeInfos()
                jsonArray(i).EmployeeID = rs("EmployeeID").ToString
                jsonArray(i).EmployeeName = rs("EmployeeName").ToString
                jsonArray(i).EmployeeNo = rs("EmployeeNo").ToString
                If IsZero Then
                    jsonArray(i).Amount = 0
                Else
                    jsonArray(i).Amount = rs("Amount").ToString
                End If

                i = i + 1
            Next
            Return jsonArray
        Catch ex As Exception
            Dim jsonArray As EmployeeInfos() = New EmployeeInfos(ds.Tables(0).Rows.Count - 1) {}
            jsonArray(0).EmployeeID = ex.Message.ToString()
            Return jsonArray
        End Try
    End Function

    Public Function AddAddableToPayRoll(ByVal obj As AddableToPayroll()) As AddableToPayroll Implements ISetupServices.AddAddableToPayRoll
        Try


            Dim ds As New DataSet
            For i As Integer = 0 To obj.Count - 1
                Dim EmployeeID As String = obj(i).EmployeeID
                Dim YearID As String = obj(i).YearID
                Dim MonthID As String = obj(i).MonthID
                Dim AddableID As String = obj(i).AddableID
                Dim Amount As String = obj(i).Amount
                Dim FiscalYearID As String = obj(i).FiscalYearID
                Dim sCompanyID As String = obj(i).sCompanyID
                Dim sBranchID As String = obj(i).sBranchID
                Dim Sql = "exec [Accounts].[usp_AddablestoPayroll]  @Flag='n',@MonthID='" & MonthID & "',@YearID='" & YearID & "',@EmployeeID='" & EmployeeID & "',@AddableID='" & AddableID & "',@AddableAmount='" & Amount & "'"
                Sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@FiscalYearID='" & FiscalYearID & "' "

                ds = Dao.ExecuteDataset(Sql)

            Next
            Dim JsonArray As New AddableToPayroll
            JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

            Return JsonArray
        Catch ex As Exception

        End Try
    End Function

    Public Function AddDeductiveToPayRoll(ByVal obj As AddableToPayroll()) As AddableToPayroll Implements ISetupServices.AddDeductiveToPayRoll
        Try


            Dim ds As New DataSet
            For i As Integer = 0 To obj.Count - 1
                Dim EmployeeID As String = obj(i).EmployeeID
                Dim YearID As String = obj(i).YearID
                Dim MonthID As String = obj(i).MonthID
                Dim DeductiveID As String = obj(i).AddableID
                Dim Amount As String = obj(i).Amount
                Dim FiscalYearID As String = obj(i).FiscalYearID
                Dim sCompanyID As String = obj(i).sCompanyID
                Dim sBranchID As String = obj(i).sBranchID

                Dim Sql = "exec [Accounts].[usp_DeductiblesToPayroll]  @Flag='n',@MonthID='" & MonthID & "',@YearID='" & YearID & "',@EmployeeID='" & EmployeeID & "',@DeductibleID='" & DeductiveID & "',@DeductibleAmount='" & Amount & "'"
                Sql += ",@CompanyID= '" & sCompanyID & "',@BranchID= '" & sBranchID & "',@FiscalYearID='" & FiscalYearID & "' "

                ds = Dao.ExecuteDataset(Sql)

            Next
            Dim JsonArray As New AddableToPayroll
            JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()

            Return JsonArray
        Catch ex As Exception

        End Try
    End Function

    Public Function UpdateBasicSalary(ByVal EmployeeID As String, ByVal Salary As String) As String Implements ISetupServices.UpdateBasicSalary
        Dim ds As New DataSet
        Dim sql As String = "exec hr.usp_UpdateBasicSalary @Flag='u',  @BasicSalary = '" & Salary & "', @EmployeeID = '" & EmployeeID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateKoshInfo(ByVal EmployeeID As String, ByVal ChangeInfo As String, ByVal Flag As String) As String Implements ISetupServices.UpdateKoshInfo
        Dim ds As New DataSet
        Dim sql As String = "exec hr.usp_updateKoshInfo @Flag='" & Flag & "',  " & ChangeInfo.Replace("'@", "@").Replace("= ", "='").Trim & ", @EmployeeID = '" & EmployeeID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePayrollProfile(ByVal ProfileID As String, ByVal ColumnTitle As String, ByVal values As String) As String Implements ISetupServices.UpdatePayrollProfile
        Dim ds As New DataSet
        Dim sql As String = "exec accounts.usp_UpdatePayrollProfile @flag='s', @ColumnTitle='" & ColumnTitle & "', @value='''" & values & "''', @ProfileID = '" & ProfileID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePayrollProfileAllEmp(ByVal ColumnTitle As String, ByVal values As String) As String Implements ISetupServices.UpdatePayrollProfileAllEmp
        Dim ds As New DataSet
        Dim sql As String = "exec accounts.usp_UpdatePayrollProfile @flag='m', @ColumnTitle='" & ColumnTitle & "', @value='''" & values & "'''"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function empTransferOut(ByVal EmployeeID As String, ByVal FiscalYearID As String, ByVal BranchID As String, ByVal UserID As String) As String Implements ISetupServices.empTransferOut
        Dim ds As New DataSet

        Dim sql = "exec accounts.usp_autoSalaryGenerate  @fiscalYearID='" & FiscalYearID & "',@BranchID='" & BranchID & "',@UserID='" & UserID & "', @EmployeeID='" & EmployeeID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function updatePayrollValues(ByVal id As String, ByVal value As String, ByVal flag As String) As String Implements ISetupServices.updatePayrollValues
        Try


            Dim ds As New DataSet
            Dim sql As String = ""
            If flag = "u" Then
                sql = "exec accounts.usp_Edit_deductive_addable @Flag='" & flag & "',  @AddablesToPayrollID = '" & id & "', @NewValue = '" & value & "'"
            ElseIf flag = "d" Then
                sql = "exec accounts.usp_Edit_deductive_addable @Flag='" & flag & "',  @DeductiblesToPayrollID = '" & id & "', @NewValue = '" & value & "'"
            End If

            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item(1).ToString()
            End If
            Return ""
        Catch ex As Exception
            Dim a As String = ex.Message
        End Try
    End Function

    Public Function UpdatePayrollAttendance(ByVal EditID As String, ByVal Ispresent As String) As String Implements ISetupServices.UpdatePayrollAttendance
        Dim ds As New DataSet
        Dim sql As String = "exec accounts.usp_PayrollAttendace @Flag='u', @Ispresent = '" & Ispresent & "', @ID = '" & EditID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePayrollExcludedEmployee(ByVal EditID As String, ByVal IsExcluded As String, ByVal MonthID As String) As String Implements ISetupServices.UpdatePayrollExcludedEmployee
        Dim ds As New DataSet
        Dim sql As String = "exec accounts.usp_PayrollAttendace @Flag='e', @IsExcluded = '" & IsExcluded & "', @ID = '" & EditID & "',@MonthID='" & MonthID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePayrollPeriod(ByVal EditID As String, ByVal Period As String, ByVal MonthID As String) As String Implements ISetupServices.UpdatePayrollPeriod
        Dim ds As New DataSet
        Dim sql As String = "exec accounts.usp_PayrollAttendace @Flag='p', @Period = '" & Period & "', @ID = '" & EditID & "',@MonthID='" & MonthID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePayrollRate(ByVal EditID As String, ByVal Rate As String, ByVal MonthID As String) As String Implements ISetupServices.UpdatePayrollRate
        Dim ds As New DataSet
        Dim sql As String = "exec accounts.usp_PayrollAttendace @Flag='r', @rate = '" & Rate & "', @ID = '" & EditID & "',@MonthID='" & MonthID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function



    Public Function DeleteRecordsOfPayroll(ByVal EmployeeID As String, ByVal MonthID As String, ByVal Flag As String) As String Implements ISetupServices.DeleteRecordsOfPayroll
        Dim ds As New DataSet
        Dim sql As String = "exec [Accounts].[usp_Edit_deductive_addable] @Flag='" & Flag & "', @EmployeeID = '" & EmployeeID & "',@MonthID='" & MonthID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function UpdatePayrollSingleEmployee(ByVal EmployeeID As String, ByVal MonthID As String, ByVal Flag As String) As String Implements ISetupServices.UpdatePayrollSingleEmployee

        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""
        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec [Accounts].[usp_updateSingleEmployeePublicCampus] @Flag='" & Flag & "', @EmployeeID = '" & EmployeeID & "',@MonthID='" & MonthID & "'"
        Else
            sql = "exec usp_updateSingleEmployee @Flag='" & Flag & "', @EmployeeID = '" & EmployeeID & "',@MonthID='" & MonthID & "'"

        End If
        Dim ds As New DataSet


        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePayrollSingleEmployeeRefresh(ByVal ID As String, ByVal Flag As String) As String Implements ISetupServices.UpdatePayrollSingleEmployeeRefresh

        Dim ds1 As New DataSet
        ds1 = Dao.ExecuteDataset("select ispublicCampus from setup.Preference")
        Dim sql As String = ""
        If ds1.Tables(0).Rows(0).Item(0) = True Then
            sql = "exec accounts.usp_RefressCalcPublicCampus @Flag='" & Flag & "', @ID = '" & ID & "'"
        Else
            sql = "exec accounts.[usp_New_RefressCalc] @Flag='" & Flag & "', @ID = '" & ID & "'"
            'sql = "exec accounts.[usp_RefressCalc] @Flag='" & Flag & "', @ID = '" & ID & "'"
        End If
        Dim ds As New DataSet

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function DeletePayrollByAddableType(ByVal AddableTypeID As String, ByVal MonthID As String) As String Implements ISetupServices.DeletePayrollByAddableType
        Dim ds As New DataSet
        Dim sql As String
        Try
            sql = "exec [Accounts].[usp_ListForMerge] @flag='a_delete',@TargetID='" & AddableTypeID & "',@monthID='" & MonthID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
    End Function

    Public Function DeletePayrollByDeductibleType(ByVal DeductibleTypeID As String, ByVal MonthID As String) As String Implements ISetupServices.DeletePayrollByDeductibleType
        Dim ds As New DataSet
        Dim sql As String
        Try
            sql = "exec [Accounts].[usp_ListForMerge] @flag='d_delete',@TargetID='" & DeductibleTypeID & "',@monthID='" & MonthID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
    End Function

    Public Function AddRemarksOnPayroll(ByVal ID As String, ByVal Remarks As String, ByVal Flag As String) As String Implements ISetupServices.AddRemarksOnPayroll
        Dim ds As New DataSet
        Dim sql As String = "exec  accounts.usp_Add_Note_payroll @Flag='" & Flag & "', @notes = '" & Remarks & "',@Id='" & ID & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function ApplyFormulaForMe(ByVal MonthID As String, ByVal EmployeeID As String, ByVal Flag As String, ByVal value As String) As Boolean Implements ISetupServices.ApplyFormulaForMe
        Dim ds As New DataSet
        Dim sql As String = "exec  accounts.usp_ApplyFormulaForMe @Flag='" & Flag & "', @MonthID = '" & MonthID & "',@Employeeid='" & EmployeeID & "',@Value='" & value & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return False
    End Function


    Public Function ApplyFormulaForAll(ByVal MonthID As String, ByVal Flag As String, ByVal value As String) As Boolean Implements ISetupServices.ApplyFormulaForAll
        Dim ds As New DataSet
        Dim sql As String = "exec  accounts.usp_ApplyFormulaForAll @Flag='" & Flag & "', @MonthID = '" & MonthID & "',@Value='" & value & "'"

        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return False
    End Function

    'Public Function SendMail(ByVal EmployeeID As String, ByVal MonthID As String) As String Implements ISetupServices.SendMail
    '    Dim sql = "exec [Accounts].[usp_DbMail_SalarySingleToCDO] @MonthID =" & MonthID & " ,@EmployeeId=" & EmployeeID & ""
    '    Dim ds As New DataSet
    '    ds = Dao.ExecuteDataset(sql)
    '    Dim m As String = ""
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        Dim bodyMessage As String = ds.Tables(0).Rows(0).Item("mes").ToString
    '        Dim subject As String = ds.Tables(0).Rows(0).Item("subject").ToString
    '        Dim emailto As String = ds.Tables(0).Rows(0).Item("email").ToString
    '        m = Email.SendByCDO(emailto, subject, bodyMessage)
    '        '   m = Email.SendByCDO("okhadka@gmail.com", subject, bodyMessage)
    '        If m = "OK" Then
    '            m = "Mail Successfully sent."
    '        End If
    '    End If
    '    Return m
    'End Function


    Public Function AdjustAddablesDeductibles(ByVal EmployeeId As String, ByVal Amount As String, ByVal MonthId As String, ByVal Type As String, ByVal TypeId As String, ByVal BranchId As String, ByVal UserId As String) As String Implements ISetupServices.AdjustAddablesDeductibles
        Dim ds As New DataSet
        Dim sql As String = ""
        Dim mes As String = ""
        Try
            sql = "exec [Accounts].[usp_AdjustPayroll] @Flag='u',@MonthID='" & MonthId & "',@EmployeeID='" & EmployeeId & "',@TypeId='" & TypeId & "'"
            sql += ",@Amount='" & Amount & "',@Type='" & Type & "',@BranchID='" & BranchId & "',@UserID='" & UserId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("ErrorCode").ToString = "0" Then
                    mes = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
                End If

            End If
            Return mes
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function AdjustSinglePayrollInMonth(ByVal EmployeeId As String, ByVal MonthId As String, ByVal Type As String, ByVal BranchId As String, ByVal FiscalyearId As String) As String Implements ISetupServices.AdjustSinglePayrollInMonth
        Dim ds As New DataSet
        Dim sql As String = ""
        Dim mes As String = ""

        Try
            sql = "exec [Accounts].[usp_AdjustPayrollInMonth] @flag='" & Type & "'"
            sql += ",@MonthID='" & MonthId & "',@employeeID='" & EmployeeId & "'"
            sql += ",@branchID='" & BranchId & "',@FiscalYearID='" & FiscalyearId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
            End If
            Return mes
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
#End Region

#Region "PCExam Methods"
    Function GetAssignedStudentToUniversityExam(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList() Implements ISetupServices.GetAssignedStudentToUniversityExam
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_student  @flag='l',@AcademicYearID='" & AcademicYearID & "',@SemesterID='" & SemesterID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As UniversityAssignStudentList() = New UniversityAssignStudentList(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New UniversityAssignStudentList()
            JsonArray(i).Name = rs("FullName").ToString
            JsonArray(i).AsignDate = rs("Date").ToString
            JsonArray(i).IsBackPaper = rs("IsBackPaper").ToString
            JsonArray(i).AssignStudentToSubject = rs("AssignStudentToSubject").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetAssignedStudentToUniversityExamBackPaper(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList() Implements ISetupServices.GetAssignedStudentToUniversityExamBackPaper
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_studentBackPaper  @flag='l',@AcademicYearID='" & AcademicYearID & "',@SemesterID='" & SemesterID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As UniversityAssignStudentList() = New UniversityAssignStudentList(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New UniversityAssignStudentList()
            JsonArray(i).Name = rs("FullName").ToString
            JsonArray(i).AsignDate = rs("Date").ToString
            JsonArray(i).IsBackPaper = rs("IsBackPaper").ToString
            JsonArray(i).AssignStudentToSubject = rs("AssignStudentToSubject").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function DeleteAssignedStudentToUniversityExam(ByVal AssignStudentToSubjectID As String) As ErrorMessage Implements ISetupServices.DeleteAssignedStudentToUniversityExam
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_student  @flag='d',@AssignStudentToSubjectID='" & AssignStudentToSubjectID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function

    Function DeleteAssignedStudentToUniversityExamBackPaper(ByVal AssignStudentToSubjectID As String) As ErrorMessage Implements ISetupServices.DeleteAssignedStudentToUniversityExamBackPaper
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_studentBackPaper  @flag='d',@AssignStudentToSubjectID='" & AssignStudentToSubjectID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function

    Function UpdateAssignedStudentToUniversityExamIsBackPaper(ByVal AssignStudentToSubjectID As String, ByVal IsBackPaper As String) As ErrorMessage Implements ISetupServices.UpdateAssignedStudentToUniversityExamIsBackPaper
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_student  @flag='u',@AssignStudentToSubjectID='" & AssignStudentToSubjectID & "', @IsBackPaper='" & IsBackPaper & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function

    Function ListSubjectAssignToStudent(ByVal AcademicYearID As String, ByVal SemesterID As String) As String Implements ISetupServices.ListSubjectAssignToStudent
        Dim sql As String
        Dim mes As String = ""
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_subject_to_student  @flag='l',@AcademicYearID='" & AcademicYearID & "', @SemesterID='" & SemesterID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim i As Integer = 0
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>List</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")
            sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>S.N</th>")
            For c As Integer = 1 To ds.Tables(0).Columns.Count - 1
                sb.AppendLine("<th>" & ds.Tables(0).Columns(c).ColumnName.ToString().ToUpper & "</th>")
            Next
            sb.AppendLine("<th>Actions</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")



            For Each row As DataRow In ds.Tables(0).Rows
                sb.AppendLine("<tr'><td>" & (i + 1) & ".</td>")
                For j As Integer = 1 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<td>" & ds.Tables(0).Rows(i)(j).ToString & "</td>")
                Next
                sb.AppendLine("<td align='center'><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("StudentID") & "'  data-SemesterID='0' id='add' class='ClassSingleStudentSubject btn blue btn-xs' href='#SingleStudentSubjectList'><span class='glyphicon glyphicon-edit'></a>")
                sb.AppendLine("<a  data-id='" & ds.Tables(0).Rows(i).Item("StudentID") & "' id='AddableID' class='ClassDeleteAllSubject btn btn-danger btn-xs' href='javascript:;'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                sb.AppendLine("</tr>")

                i += 1
            Next


            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
        End If

        Return sb.ToString

    End Function

    Function ListSubjectOfSingleStudent(ByVal StudentID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As String Implements ISetupServices.ListSubjectOfSingleStudent
        Dim sql As String
        Dim mes As String = ""
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_subject_to_student  @flag='a',@StudentID='" & StudentID & "', @SemesterID='" & SemesterID & "',@AcademicYearID='" & AcademicYearID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim i As Integer = 0
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>List</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='#portlet-config' data-toggle='modal' class='config'></a><a href='javascript:;' class='reload'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")
            sb.AppendLine("<table class='display table table-bordered table-striped table-condensed flip-content' id='displaytable'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>S.N</th>")
            For c As Integer = 3 To ds.Tables(0).Columns.Count - 1
                sb.AppendLine("<th>" & ds.Tables(0).Columns(c).ColumnName.ToString().ToUpper & "</th>")
            Next
            sb.AppendLine("<th>Actions</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")



            For Each row As DataRow In ds.Tables(0).Rows
                sb.AppendLine("<tr'><td>" & (i + 1) & ".</td>")
                For j As Integer = 3 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<td>" & ds.Tables(0).Rows(i)(j).ToString & "</td>")
                Next

                'sb.AppendLine("<td><a  data-id='" & jid & "' data-mo='dr' data='" & dr & "' id='AddableID' class='ClassFees' href='javascript:;'>" & dr & "</a> </td>")
                'sb.AppendLine("<td><a  data-id='" & jid & "' data-mo='cr' data='" & cr & "' id='AddableID' class='ClassFees' href='javascript:;'>" & cr & "</a> </td>")
                sb.AppendLine("<td align='center'><a data-toggle='modal' data-id='" & ds.Tables(0).Rows(i).Item("StudentID") & "'  data-SubjectID='" & ds.Tables(0).Rows(i).Item("SubjectID") & "' id='add' class='ClassAddSubject btn blue btn-xs' href='#AddSubjectToOneStudent'><span class='glyphicon glyphicon-edit'></a>")

                'sb.AppendLine("<td><a  data-id='" & i & "' id='AddableID' class='ClassViewStd btn blue btn-xs' href='javascript:;'><span class='glyphicon glyphicon-edit'></span></a> &nbsp;")
                sb.AppendLine("<a  data-id='" & ds.Tables(0).Rows(i).Item("ExamSubjectRegistrationID") & "' id='AddableID' class='ClassSingleSubjectDelete btn btn-danger btn-xs' href='javascript:;'><span class='glyphicon glyphicon-trash'></span></a> </td>")
                sb.AppendLine("</tr>")

                i += 1
            Next


            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
        End If

        Return sb.ToString

    End Function

    Function AddNewSubject(ByVal StudentID As String, ByVal SubjectID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As ErrorMessage Implements ISetupServices.AddNewSubject
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_subject_to_student  @flag='i',@StudentID='" & StudentID & "',@SubjectID='" & SubjectID & "',@SemesterID='" & SemesterID & "',@AcademicYearID='" & AcademicYearID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function

    Function GetSubjectDropDownList(ByVal SemesterID As String) As Subjectlist() Implements ISetupServices.GetSubjectDropDownList
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_subject_to_student  @flag='sublist',@SemesterID='" & SemesterID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As Subjectlist() = New Subjectlist(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New Subjectlist()
            JsonArray(i).SubjectID = rs("SubjectID").ToString
            JsonArray(i).SubjectTitle = rs("SubjectTitle").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Function DeleteOneStudentAllSubjectInOneSemesterOneAcademicYear(ByVal StudentID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As ErrorMessage Implements ISetupServices.DeleteOneStudentAllSubjectInOneSemesterOneAcademicYear
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_subject_to_student  @flag='AllSub',@StudentID='" & StudentID & "',@SemesterID='" & SemesterID & "',@AcademicYearID='" & AcademicYearID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function


    Function DeleteOneStudentOneSubjectInOneSemesterOneAcademicYear(ByVal ExamSubjectRegistrationID As String) As ErrorMessage Implements ISetupServices.DeleteOneStudentOneSubjectInOneSemesterOneAcademicYear
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_assign_subject_to_student  @flag='d',@ExamSubjectRegistrationID='" & ExamSubjectRegistrationID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function

    Function GetMarksEntryStudent(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList() Implements ISetupServices.GetMarksEntryStudent
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_MarksEntry  @flag='l',@AcademicYearID='" & AcademicYearID & "',@SemesterID='" & SemesterID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As UniversityAssignStudentList() = New UniversityAssignStudentList(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New UniversityAssignStudentList()
            JsonArray(i).Name = rs("FullName").ToString
            JsonArray(i).AsignDate = rs("Date").ToString
            '   JsonArray(i).AssignStudentToSubject = rs("ObtainedMarksEntryID").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function


    Function GetMarksEntryStudentBackPaper(ByVal AcademicYearID As String, ByVal SemesterID As String) As UniversityAssignStudentList() Implements ISetupServices.GetMarksEntryStudentBackPaper
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_MarksEntry  @flag='l',@AcademicYearID='" & AcademicYearID & "',@SemesterID='" & SemesterID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As UniversityAssignStudentList() = New UniversityAssignStudentList(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New UniversityAssignStudentList()
            JsonArray(i).Name = rs("FullName").ToString
            JsonArray(i).AsignDate = rs("Date").ToString
            '   JsonArray(i).AssignStudentToSubject = rs("ObtainedMarksEntryID").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function DeleteAllSubjectBySingleStudent(ByVal StudentID As String, ByVal SemeseterID As String, ByVal AcademicYearID As String) As ErrorMessage Implements ISetupServices.DeleteAllSubjectBySingleStudent
        Dim sql As String
        Dim mes As String = ""
        Dim ds As New DataSet
        sql = "exec pcexam.usp_list_of_MarksEntryBackPaper  @flag='d',@AcademicYearID='" & AcademicYearID & "',@SemesterID='" & SemeseterID & "',@StudentID='" & StudentID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            mes = ds.Tables(0).Rows(0).Item("mes").ToString
        End If

        Dim JsonArray As New ErrorMessage

        JsonArray.Message = mes
        JsonArray.ErrorCode = "0"
        Return JsonArray

    End Function

    Public Function BulkMarksEntry(ByVal obj As BulkMarks(), ByVal AcademicYearID As String, ByVal CourseID As String, ByVal SemesterID As String, ByVal UserID As String, ByVal isOverwirte As Boolean, ByVal isPartial As Boolean, ByVal isBackPaper As String, ByVal BranchID As String) As ErrorMessage Implements ISetupServices.BulkMarksEntry
        Dim ds As New DataSet
        Dim sql As String
        Dim mes As String = String.Empty
        Dim errcode As String = String.Empty
        Dim JsonArray As New ErrorMessage
        Try
            Dim stringQuery As String = "<cashpayment>"

            For i As Integer = 0 To obj.Count - 1
                Dim StudentID As String = obj(i).StudentID
                Dim MarksBulk As String() = obj(i).Marks.Split(",")

                For j As Integer = 0 To MarksBulk.Length - 1
                    Dim Marks As String() = MarksBulk(j).Split(":")
                    If Marks.Length > 4 Then
                        stringQuery += "<row "
                        stringQuery += " StudentID =""" & StudentID & """"
                        stringQuery += " SubjectID =""" & Marks(0).ToString & """"
                        stringQuery += " MarksTheory =""" & Marks(1).ToString & """"
                        stringQuery += " MarksPractical =""" & Marks(2).ToString & """"
                        stringQuery += " AttendanceTheorySub =""" & Marks(3).ToString & """"
                        stringQuery += " AttendancePracticalSub =""" & Marks(4).ToString & """"
                        stringQuery += " />"
                    End If
                Next
            Next

            stringQuery += "</cashpayment>"

            sql = "EXEC PCExam.usp_BulkMarksEentry @Flag = '',@EntryBy='" & UserID & "',@BranchID='" & BranchID & "'"
            sql += " ,@CourseID='" & CourseID & "'"
            sql += " ,@SemesterID='" & SemesterID & "'"
            sql += " ,@AcademicYearID='" & AcademicYearID & "'"
            sql += " ,@xml='" & stringQuery & "'"
            sql += " ,@overwirte='" & isOverwirte & "'"
            sql += " ,@Partial='" & isPartial & "'"
            sql += " ,@CompanayID='1'"
            sql += ",@isBackPaper='" & isBackPaper & "'"

            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                mes = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
                errcode = ds.Tables(0).Rows(0).Item("ErrorCode").ToString.Trim
            End If

            JsonArray.Message = mes
            JsonArray.ErrorCode = errcode

        Catch ex As Exception
            JsonArray.Message = "Error in Saving Marks"
            JsonArray.ErrorCode = "1"
        End Try
        Return JsonArray
    End Function

    Public Function IL_BulkMarksEntry(ByVal obj As BulkMarks(), ByVal AcademicYearID As String, ByVal LanguageID As String, ByVal SemesterID As String, ByVal LevelID As String, ByVal CourseDurationID As String, ByVal UserID As String, ByVal isOverwirte As Boolean, ByVal isPartial As Boolean) As ErrorMessage Implements ISetupServices.IL_BulkMarksEntry
        Dim ds As New DataSet
        Dim sql As String
        Dim stringQuery As String = "<cashpayment>"

        For i As Integer = 0 To obj.Count - 1
            Dim StudentID As String = obj(i).StudentID
            Dim MarksBulk As String() = obj(i).Marks.Split(",")
            Dim SymbolNo As String = obj(i).SymbolNo
            For j As Integer = 0 To MarksBulk.Length - 1
                Dim Marks As String() = MarksBulk(j).Split(":")
                If Marks.Length > 4 Then
                    stringQuery += "<row "
                    stringQuery += " StudentID =""" & StudentID & """"
                    stringQuery += " SymbolNo =""" & SymbolNo & """"
                    stringQuery += " SubjectID =""" & Marks(0).ToString & """"
                    stringQuery += " MarksInternal =""" & Marks(1).ToString & """"
                    stringQuery += " MarksFinal =""" & Marks(2).ToString & """"
                    stringQuery += " AttendanceInternalSub =""" & Marks(3).ToString & """"
                    stringQuery += " AttendanceFinalSub =""" & Marks(4).ToString & """"
                    stringQuery += " />"
                End If
            Next
        Next

        stringQuery += "</cashpayment>"

        sql = "EXEC [IL].[usp_BulkMarksEntry] @flag = 'i',@EntryBy='" & UserID & "'"
        sql += " ,@LanguageID='" & LanguageID & "'"
        sql += " ,@SemesterID='" & SemesterID & "'"
        sql += " ,@AcademicYearID='" & AcademicYearID & "'"
        sql += " ,@LevelID='" & LevelID & "'"
        sql += " ,@CourseDurationID='" & CourseDurationID & "'"
        sql += " ,@xml='" & stringQuery & "'"
        sql += " ,@overwirte='" & isOverwirte & "'"
        sql += " ,@Partial='" & isPartial & "'"

        ds = Dao.ExecuteDataset(sql)
        Dim JsonArray As New ErrorMessage
        If ds.Tables(0).Rows.Count > 0 Then
            Dim recordsAffected As String = ds.Tables(0).Rows(0).Item("mes").ToString()
            JsonArray.Message = "Marks saved successfully. " & recordsAffected
            JsonArray.ErrorCode = "0"
        Else
            JsonArray.Message = "Error"
        End If

        Return JsonArray
    End Function


    Public Function IL_BulkMarksEdit(ByVal ObtainedMarkId As String, ByVal modifyitem As String, ByVal val As String) As String Implements ISetupServices.IL_BulkMarksEdit
        Dim sql As String
        Dim ds As New DataSet
        Try
            If Not String.IsNullOrWhiteSpace(val) Then
                sql = "EXEC  [IL].[usp_BulkMarksEdit]  @Flag='u', @item_to_modify='" & modifyitem & "',@ObtainedMarksID='" & ObtainedMarkId & "', @modifyvalue=N'" & val & "'"

                ds = Dao.ExecuteDataset(sql)
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function

    Public Function IL_EditSymbolNo(ByVal StudentId As String, ByVal SymbolNo As String) As String Implements ISetupServices.IL_EditSymbolNo
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "EXEC  [IL].[usp_BulkMarksEdit]  @Flag='edit_Symbol',@StudentID='" & StudentId & "',@SymbolNo=N'" & SymbolNo & "'"
            ds = Dao.ExecuteDataset(sql)
            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString()
            End If

        Catch ex As Exception
            Return "error"
        End Try
    End Function

    Public Function UpdatePCExamMarks(ByVal ID As String, ByVal Marks As String, ByVal flag As String) As String Implements ISetupServices.UpdatePCExamMarks
        Dim ds As New DataSet
        Dim sql As String = "exec pcexam.usp_SingleEdit @Flag='" & flag & "', @Marks = '" & Marks & "', @ID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateSeminarObtainMarks(ByVal ID As String, ByVal Marks As String) As String Implements ISetupServices.UpdateSeminarObtainMarks
        Dim ds As New DataSet
        Dim sql As String = "exec [Exam].[usp_SeminarPU] @Flag='r', @Marks = '" & Marks & "', @ID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function UpdateTeacherEvaluation(ByVal ID As String, ByVal Marks As String) As String Implements ISetupServices.UpdateTeacherEvaluation
        Dim ds As New DataSet
        Dim sql As String = "exec exam.usp_TeacherEvaluation @Flag='r', @Marks = '" & Marks & "', @ID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdatePracticalEvaluation(ByVal ID As String, ByVal Marks As String) As String Implements ISetupServices.UpdatePracticalEvaluation
        Dim ds As New DataSet
        Dim sql As String = "exec exam.usp_PracticalEvaluation @Flag='u', @Marks = '" & Marks & "', @ID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function UpdatePCExamSymbol(ByVal ID As String, ByVal Symbol As String) As String Implements ISetupServices.UpdatePCExamSymbol
        Dim ds As New DataSet
        Dim sql As String = "exec pcExam.usp_GetSymbolNo @Flag='u', @input = '" & Symbol & "', @AssignStudentToSubjectID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function SaveSubjectRegistrationNew(ByVal obj As SaveSubjectRegistration()) As ErrorMessage Implements ISetupServices.SaveSubjectRegistrationNew
        Dim ds As New DataSet
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentId
            Dim batchId As String = obj(i).BatchId
            Dim semesterId As String = obj(i).SemesterId
            Dim subjectIds As String = obj(i).AssignSub
            Dim userId As String = obj(i).UserId
            Dim academicyearid As String = obj(i).AcademicYearID

            Dim sql As String = "EXEC PCexam.usp_SubjectRegistration @Flag='i', @CourseID='" & batchId & "', @StudentID='" & studentId & "', @SubjectIDs='" & subjectIds & "', @semesterId='" & semesterId & "',@user='" & userId & "',@AcademicYearID='" & academicyearid & "'"
            ds = Dao.ExecuteDataset(sql)

        Next
        Dim JsonArray As New ErrorMessage
        JsonArray.Message = "Subject assigned successfully."
        JsonArray.ErrorCode = "0"
        Return JsonArray
    End Function

    Public Function SubjectwiseMarksEntry(ByVal obj As SubjectwiseMarks()) As ErrorMessage Implements ISetupServices.SubjectwiseMarksEntry
        Dim ds As New DataSet
        Dim sql As String
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentID
            Dim courseId As String = obj(i).CourseId
            Dim subjectId As String = obj(i).SubjectId
            Dim semesterId As String = obj(i).SemeterId
            Dim academicyearId As String = obj(i).AcademicYearId
            Dim marksTheory As String = obj(i).MarksTheory
            Dim marksPr As String = obj(i).MarksPractical
            Dim AttTheory As String = obj(i).AttendanceTheory
            Dim attPr As String = obj(i).AttendancePractical
            Dim user As String = obj(i).UserID
            Dim brachId As String = obj(i).BranchID
            Dim type As String = obj(i).Type

            If (obj(i).MarksTheory <> "" Or obj(i).AttendanceTheory <> "") Then

                sql = "EXEC [PCExam].[usp_MarksEntrySubjectwise]  @Flag='save'"
                sql += ", @CourseID=" + Dao.FilterQuote(courseId)
                sql += ", @StudentID=" + Dao.FilterQuote(studentId)
                sql += ", @SubjectID=" + Dao.FilterQuote(subjectId)
                sql += ", @AcademicYearID=" + Dao.FilterQuote(academicyearId)
                sql += ", @SemesterID=" + Dao.FilterQuote(semesterId)
                sql += ", @type=" + Dao.FilterQuote(type)
                sql += ", @MarksTheory=" + Dao.FilterQuote(marksTheory)
                sql += ", @MarksPractical=" + Dao.FilterQuote(marksPr)
                sql += ", @AttenTheory=" + Dao.FilterQuote(AttTheory)
                sql += ", @AttenPractical=" + Dao.FilterQuote(attPr)
                sql += ", @EntryBy=" + Dao.FilterQuote(user)
                sql += ", @BranchID=" + Dao.FilterQuote(brachId)

                ds = Dao.ExecuteDataset(sql)
            End If
        Next

        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        Return JsonArray
    End Function

    Public Function UpdatePCExamMarks_New(ByVal ID As String, ByVal Marks As String, ByVal columnsName As String) As String Implements ISetupServices.UpdatePCExamMarks_New
        Dim ds As New DataSet
        Dim sql As String = "exec pcexam.usp_CourseWiseEdit @Flag='u', @Valu = '" & Marks & "', @ObtainedMarksEntryID = '" & ID & "',@columnName='" & columnsName & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function delPCExamMarks_New(ByVal ID As String) As String Implements ISetupServices.delPCExamMarks_New
        Dim ds As New DataSet
        Dim sql As String = "exec pcexam.usp_CourseWiseEdit @Flag='d', @ObtainedMarksEntryID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function UpdateAnyTable(ByVal SchemaName As String, ByVal tableName As String, ByVal ColumnName As String, ByVal ColumnID As String, ByVal ID As String, ByVal Value As String) As String Implements ISetupServices.UpdateAnyTable
        Dim ds As New DataSet
        'exec usp_GlobalSP @Flag='u',@SchemaName='setup',@tableName='section',@ColumnName='section', @ColumnID='SectionID',@ID='425',@Value='E'
        Dim sql As String = "exec usp_GlobalSP @Flag='u',@SchemaName='" & SchemaName & "',@tableName='" & tableName & "',@ColumnName='" & ColumnName & "', @ColumnID='" & ColumnID & "',@ID='" & ID & "',@Value='" & Value & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function

    Public Function delTableRow(ByVal SchemaName As String, ByVal tableName As String, ByVal ColumnID As String, ByVal ID As String) As String Implements ISetupServices.delTableRow
        Dim ds As New DataSet
        Dim sql As String = "exec usp_GlobalSP @Flag='d',@SchemaName='" & SchemaName & "',@tableName='" & tableName & "',@ColumnID='" & ColumnID & "',@ID='" & ID & "'"
        ' Dim sql As String = "exec pcexam.usp_CourseWiseEdit @Flag='d', @ObtainedMarksEntryID = '" & ID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(1).ToString()
        End If
        Return ""
    End Function


    Public Function EditSubjectRegd(ByVal value As String, ByVal StudentID As String, ByVal SubjectID As String, ByVal CourseID As String, ByVal SemesterID As String, ByVal AcademicYearID As String) As String Implements ISetupServices.EditSubjectRegd
        Dim ds As New DataSet
        Dim sql As String
        Try
            sql = "exec [PCExam].[usp_EditSubjectRegistration] @Flag='u',@CourseID='" & CourseID & "',@SemesterID='" & SemesterID & "',@AcademicYearID ='" & AcademicYearID & "'"
            sql += ",@value='" & value & "',@studentID='" & StudentID & "',@subjectID='" & SubjectID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item(1).ToString().Trim
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function

    Public Function GetStudentBoardResults(ByVal StudentID As String) As String Implements ISetupServices.GetStudentBoardResults
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Try
            sql = "exec [PCExam].[usp_GetPassFailSummary] @flag='i',@StudentID='" & StudentID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim studentname As String = ds.Tables(0).Rows(0).Item("StudentName").ToString.Trim
                sb.AppendLine("<div class='row'>")
                sb.AppendLine("<div class='col-md-12'>")
                sb.AppendLine("<div class='portlet box green'>")
                sb.AppendLine("<div class='portlet-title'>")
                sb.AppendLine("<div class='caption'>")
                sb.AppendLine(" <i class='fa fa-list'></i>Result of " & studentname & " </div>")
                sb.AppendLine("<div class='tools'>")
                sb.AppendLine(" <a href='javascript:;' class='collapse'></a><a href='javascript:;' class='remove'></a>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("<div class='portlet-body flip-scroll'  runat='server'>")
                sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content detailstable' id='resulttable'>")
                sb.AppendLine("<thead class='flip-content'>")
                sb.AppendLine("<tr>University Exam Results In Semester/Year of " & studentname & "</tr>")
                sb.AppendLine("<tr>")


                For c As Integer = 1 To ds.Tables(0).Columns.Count - 1
                    sb.AppendLine("<th>" & ds.Tables(0).Columns(c).ColumnName.ToString.Trim & "</th>")
                Next

                sb.AppendLine("</tr>")
                sb.AppendLine("</thead>")

                sb.AppendLine("<tbody>")

                For r As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    sb.AppendLine("<tr>")

                    For c As Integer = 1 To ds.Tables(0).Columns.Count - 1
                        sb.AppendLine("<td>" & ds.Tables(0).Rows(r).Item(c).ToString.Trim & "</td>")
                    Next
                    sb.AppendLine("</tr>")
                Next

                sb.AppendLine("</tbody>")
                sb.AppendLine("</table>")
                sb.AppendLine("<br/>")
                sb.AppendLine("<div>Exam Attempt Results In Semester/Year of " & studentname & "</div>")
                If ds.Tables(1).Rows.Count > 0 Then
                    sb.AppendLine("<table class='table table-bordered table-striped table-condensed flip-content resultdisplay'>")
                    sb.AppendLine("<thead class='flip-content'>")
                    sb.AppendLine("<tr>")
                    For c As Integer = 0 To ds.Tables(1).Columns.Count - 1
                        sb.AppendLine("<th>" & ds.Tables(1).Columns(c).ColumnName.ToString.Trim & "</th>")
                    Next
                    sb.AppendLine("</tr>")
                    sb.AppendLine("</thead>")
                    sb.AppendLine("<tbody>")
                    For r As Integer = 0 To ds.Tables(1).Rows.Count - 1
                        sb.AppendLine("<tr>")
                        For c As Integer = 0 To ds.Tables(1).Columns.Count - 1
                            sb.AppendLine("<td>" & ds.Tables(1).Rows(r).Item(c).ToString.Trim & "</td>")
                        Next
                        sb.AppendLine("</tr>")
                    Next
                    sb.AppendLine("</tbody>")
                    sb.AppendLine("</table>")
                End If
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                sb.AppendLine("</div>")
                Return sb.ToString()
            Else
                sb.AppendLine("<div class='note note-danger'>")
                sb.AppendLine("<div class='close-note'>x</div>")
                sb.AppendLine("<p>")
                sb.AppendLine("No Record Found")
                sb.AppendLine("</p>")
                sb.AppendLine("</div>")
                Return sb.ToString()

            End If
        Catch ex As Exception
            Return "Error " + ex.Message.ToString.Trim
        End Try
    End Function

    Public Function SingleMarksEntryAssessment(ByVal obj As SingleMarksAssessment()) As ErrorMessage Implements ISetupServices.SingleMarksEntryAssessment
        Dim ds As New DataSet
        Dim sql As String
        For i As Integer = 0 To obj.Count - 1
            Dim studentId As String = obj(i).StudentID
            Dim courseId As String = obj(i).CourseId
            Dim subjectId As String = obj(i).SubjectId
            Dim semesterId As String = obj(i).SemeterId
            Dim academicyearId As String = obj(i).AcademicYearId
            Dim marksAssessment As String = obj(i).Assessment
            Dim marksTheory As String = obj(i).MarksTheory
            Dim marksPr As String = obj(i).MarksPractical
            Dim AttTheory As String = obj(i).AttendanceTheory
            Dim attPr As String = obj(i).AttendancePractical
            Dim user As String = obj(i).UserID
            Dim brachId As String = obj(i).BranchID
            Dim type As String = obj(i).Type

            If (obj(i).MarksTheory <> "" Or obj(i).AttendanceTheory <> "") Then

                sql = "EXEC [PCExam].[usp_MarksEntrySubjectwise]  @Flag='insert'"
                sql += ", @CourseID=" + Dao.FilterQuote(courseId)
                sql += ", @StudentID=" + Dao.FilterQuote(studentId)
                sql += ", @SubjectID=" + Dao.FilterQuote(subjectId)
                sql += ", @AcademicYearID=" + Dao.FilterQuote(academicyearId)
                sql += ", @SemesterID=" + Dao.FilterQuote(semesterId)
                sql += ", @type=" + Dao.FilterQuote(type)
                sql += ", @MarksTheory=" + Dao.FilterQuote(marksTheory)
                sql += ", @MarksPractical=" + Dao.FilterQuote(marksPr)
                sql += ", @AttenTheory=" + Dao.FilterQuote(AttTheory)
                sql += ", @AttenPractical=" + Dao.FilterQuote(attPr)
                sql += ", @EntryBy=" + Dao.FilterQuote(user)
                sql += ", @BranchID=" + Dao.FilterQuote(brachId)
                sql += ", @MarksAssessment=" + Dao.FilterQuote(marksAssessment)
                ds = Dao.ExecuteDataset(sql)
            End If
        Next

        Dim JsonArray As New ErrorMessage
        JsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
        Return JsonArray
    End Function
#End Region


#Region "Attendance Methods"

    Function GetEmployeeByJobTitle(ByVal jobTitleIds As String, ByVal branchId As String) As EmployeeByJobTitle() Implements ISetupServices.GetEmployeeByJobTitle
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Attendance].[usp_Employee] @flag='b',@BranchID='" & branchId & "',@JobTitleIDs='" & jobTitleIds & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As EmployeeByJobTitle() = New EmployeeByJobTitle(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New EmployeeByJobTitle()
            JsonArray(i).EmployeeName = rs("EmployeeName").ToString
            JsonArray(i).ApplicationID = rs("ApplicationID").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function

    Function UpdateApplicationId(ByVal EmpId As String, ByVal ApplicationId As String, ByVal sUserId As String) As String Implements ISetupServices.UpdateApplicationId
        Dim sql As String
        Dim ds As New DataSet
        Try
            If Not String.IsNullOrWhiteSpace(ApplicationId) Then
                If IsNumeric(ApplicationId) And ApplicationId.Length >= 0 Then
                    sql = "EXEC [Attendance].[usp_Employee] @Flag='u',@EmployeeId='" & EmpId & "',@ApplicationID='" & ApplicationId & "',@ModifiedBy='" & sUserId & "'"
                    ds = Dao.ExecuteDataset(sql)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
                        Return mes
                    Else
                        Return "Uable to save records."
                    End If
                End If
            End If
            Return "Provide valid number."
        Catch ex As Exception
            Return ex.Message.ToString()
        End Try
    End Function
#End Region


#Region "Inventry Methods"
    Public Function InventryReturn(ByVal ReferenceNo As String, ByVal ItemID As String, ByVal AssigneeID As String, ByVal UserID As String) As String Implements ISetupServices.InventryReturn
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Inventry].[usp_Transaction] @Flag='return',@ReferenceNo='" & ReferenceNo & "',@ItemID='" & ItemID & "',@AssigneID='" & AssigneeID & "',@CreatedBy='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return "Error : " & ex.Message
        End Try
    End Function

    Public Function ValidateInventoryItemCode(ByVal ItemCode As String) As String Implements ISetupServices.ValidateInventoryItemCode
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Inventry].[usp_AssignItem_New] @Flag='validate',@ItemCode='" & ItemCode & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            Return "Error : " & ex.Message
        End Try
    End Function
#End Region


#Region "Proposal and Project Methods"


    Function SaveNewRegisteredProposal(ByVal ProposalTitle As String, ByVal StudentId As Integer, ByVal ProposalRegistrationNumber As String, ByVal LogInUser As String, ByVal RegistrationDate As String) As String Implements ISetupServices.SaveNewRegisteredProposal
        Dim sql As String
        Dim ds As New DataSet
        Try
            ProposalTitle = Dao.FilterQuote(ProposalTitle)
            ProposalRegistrationNumber = Dao.FilterQuote(ProposalRegistrationNumber)
            RegistrationDate = Dao.FilterQuote(RegistrationDate)

            sql = "exec [Proposal].[usp_ProposalsRegistration] @flag='i',@ProposalTitle=N'" & ProposalTitle & "',@StudentId='" & StudentId & "' ,@ProposalRegistrationNumber=N'" & ProposalRegistrationNumber & "',@LogInUser='" & LogInUser & "',@RegistrationDate ='" & RegistrationDate & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function GetProposalInfo(ByVal ProposalID As String) As ProposalInfo() Implements ISetupServices.GetProposalInfo
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_ProposalsRegistration] @flag='s',@ProposalID='" & ProposalID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim JsonArray As ProposalInfo() = New ProposalInfo(ds.Tables(0).Rows.Count - 1) {}
                For Each row As DataRow In ds.Tables(0).Rows
                    JsonArray(i) = New ProposalInfo()
                    JsonArray(i).ProposalID = row("ProposalId").ToString
                    JsonArray(i).Title = row("ProposalTitle").ToString
                    JsonArray(i).RegdNo = row("ProposalRegdNo").ToString
                    JsonArray(i).RegdDate = row("ProposalRegdDate").ToString
                    i = i + 1
                Next
                Return JsonArray
            End If
        Catch ex As Exception
            Dim jsonArray As ProposalInfo() = New ProposalInfo(ds.Tables(0).Rows.Count - 1) {}
            jsonArray(0).ProposalID = ex.Message.ToString()
            Return jsonArray
        End Try
    End Function

    Function UpdateProposalInfo(ByVal ProposalID As String, ByVal Title As String, ByVal RegdNo As String, ByVal RegdDate As String, ByVal LogInUser As String) As ErrorMessage Implements ISetupServices.UpdateProposalInfo
        Dim sql As String
        Dim ds As New DataSet
        Dim jsonArray As New ErrorMessage()
        Try
            Title = Dao.FilterQuote(Title)
            RegdNo = Dao.FilterQuote(RegdNo)
            RegdDate = Dao.FilterQuote(RegdDate)

            sql = "exec [Proposal].[usp_ProposalsRegistration] @flag='u',@ProposalID='" & ProposalID & "',@ProposalTitle=N'" & Title & "',@ProposalRegistrationNumber=N'" & RegdNo & "',@RegistrationDate ='" & RegdDate & "',@LogInUser='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                jsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorCode").ToString
                jsonArray.Message = ds.Tables(0).Rows(0).Item("mes").ToString
                Return jsonArray
            End If
        Catch ex As Exception
            jsonArray.ErrorCode = 1
            jsonArray.Message = ex.Message.ToString
            Return jsonArray
        End Try
    End Function

    Function SaveNewRoaster(ByVal RoasterName As String, ByVal Expertise As String, ByVal MobileNumber As String, ByVal Email As String, ByVal Remarks As String, ByVal LogInUser As String) As String Implements ISetupServices.SaveNewRoaster
        Dim sql As String
        Dim ds As New DataSet

        Try
            RoasterName = Dao.FilterQuote(RoasterName)
            Expertise = Dao.FilterQuote(Expertise)
            MobileNumber = Dao.FilterQuote(MobileNumber)
            Email = Dao.FilterQuote(Email)
            Remarks = Dao.FilterQuote(Remarks)

            sql = "exec [Proposal].[usp_RegisterSupervisior] @Flag ='i' ,@ActionFlag='roaster',  @SupervisiorName='" & RoasterName & "', @Expertise ='" & Expertise & "', @Mobile ='" & MobileNumber & "',@Email ='" & Email & "',@Remark ='" & Remarks & "'"
            sql += ",@LogInUserName='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function AssignRoaster(ByVal ResearchHead As Integer, ByVal RoasterId As Integer, ByVal ProposalId As Integer, ByVal InterviewDate As String, ByVal LogInUser As String) As String Implements ISetupServices.AssignRoaster

        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")

        Try
            sql = "exec [Proposal].[usp_AssignSupervisiors]  @ResearchHead='" & ResearchHead & "' , @RoasterId='" & RoasterId & "',@InterviewDate='" & InterviewDate & "',@ProposalId='" & ProposalId & "',@LogInUserName='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function UpdateVivaResult(ByVal MarksAwarded As String, ByVal CotForEdit As String, ByVal RorForEdit As String, ByVal LanguageForEdit As String, ByVal VivaExamRecordId As Integer, ByVal LogInUser As String) As String Implements ISetupServices.UpdateVivaResult
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Proposal].[usp_VivaExamsResultEntry] @Flag ='u',@LogInUserName ='" & LogInUser & "',@VivaExamRecordId ='" & VivaExamRecordId & "'"
            sql += ",@MarksAwarded='" & MarksAwarded & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function getVivaResultEntryById(ByVal VivaResultEntryId As Integer) As VivaResultSet() Implements ISetupServices.getVivaResultEntryById

        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_VivaExamsResultEntry] @Flag='s',@VivaExamRecordId='" & VivaResultEntryId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then


                Dim i As Integer = 0
                Dim JsonArray As VivaResultSet() = New VivaResultSet(ds.Tables(0).Rows.Count - 1) {}
                For Each rs As DataRow In ds.Tables(0).Rows
                    JsonArray(i) = New VivaResultSet()
                    JsonArray(i).MarksObtained = rs("MarksAwarded").ToString
                    'JsonArray(i).ConceptualAndTheoreticalReview = rs("ConceptualAndTheoreticalReview").ToString
                    'JsonArray(i).ReviewOfRelatedStudies = rs("ReviewOfRelatedStudies").ToString
                    'JsonArray(i).Languages = rs("Languages").ToString
                    'JsonArray(i).Total = rs("Total").ToString
                    'JsonArray(i).VivaDate = rs("VivaDate").ToString

                    i = i + 1
                Next
                Return JsonArray
            End If
        Catch ex As Exception


        End Try
    End Function



    Function SaveVivaResult(ByVal MarksAwarded As String, ByVal CotReview As String, ByVal ReviewOfRelatedStudies As String, ByVal Language As String, ByVal ProposalID As String, ByVal LogInUser As String) As String Implements ISetupServices.SaveVivaResult
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Proposal].[usp_VivaExamsResultEntry] @Flag ='i',@ProposalId ='" & ProposalID & "',@LogInUserName='" & LogInUser & "'"
            sql += ",@MarksAwarded='" & MarksAwarded & "'"

            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function DeleteVivaMarks(ByVal VivaExamRecordId As String) As String Implements ISetupServices.DeleteVivaMarks
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_VivaExamsResultEntry] @Flag='d' ,@VivaExamRecordId='" & VivaExamRecordId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("Mes").ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function

    Function UpdateInterviewResult(ByVal ConceptualAndTheoreticalReview As String,
                                   ByVal ReviewOfRelatedStudies As String,
                                   ByVal Language As String,
                                   ByVal UserID As String,
                                   ByVal InterviewResultEntryId As Integer) As String Implements ISetupServices.UpdateInterviewResult


        ConceptualAndTheoreticalReview = ConceptualAndTheoreticalReview.Replace("'", "''")
        ReviewOfRelatedStudies = ReviewOfRelatedStudies.Replace("'", "''")
        Language = Language.Replace("'", "''")


        '  Dim Dob = Helper.GetFrontToDbDate(txt_Dob.Text)
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Proposal].[usp_InterviewsResultEntry] @Flag ='u' ,  @ConceptualAndTheoreticalReview='" & ConceptualAndTheoreticalReview & "', @ReviewOfRelatedStudies ='" & ReviewOfRelatedStudies & "', @Language ='" & Language & "',@LogInUserName ='" & UserID & "',@InterviewResultEntryId ='" & InterviewResultEntryId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function getInterviewResultById(ByVal InterviewResultEntryId As Integer) As ResultSet() Implements ISetupServices.getInterviewResultById



        '  Dim Dob = Helper.GetFrontToDbDate(txt_Dob.Text)
        Dim sql As String
        Dim ds As New DataSet


        Try
            sql = "exec [Proposal].[usp_InterviewsResultEntry] @Flag='s' ,@InterviewResultEntryId='" & InterviewResultEntryId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then


                Dim i As Integer = 0
                Dim JsonArray As ResultSet() = New ResultSet(ds.Tables(0).Rows.Count - 1) {}
                For Each rs As DataRow In ds.Tables(0).Rows
                    JsonArray(i) = New ResultSet()
                    'JsonArray(i).SupervisiorName = rs("SupervisiorName").ToString
                    JsonArray(i).ConceptualOrTheoreticalReview = rs("ConceptualOrTheoreticalReview").ToString
                    JsonArray(i).ReviewOfRelatedStudies = rs("ReviewOfRelatedStudies").ToString
                    JsonArray(i).Language = rs("Language").ToString
                    'JsonArray(i).Total = rs("Total").ToString
                    'JsonArray(i).InterviewDate = rs("InterviewDate").ToString
                    'JsonArray(i).txtPassOrFail = rs("Result").ToString
                    i = i + 1
                Next
                Return JsonArray


                'SupervisiorName,ConceptualOrTheoreticalReview,ReviewOfRelatedStudies,Language,Total,InterviewDate
            End If
        Catch ex As Exception


        End Try
    End Function

    Function DeleteVivaInfo(ByVal AssignedVivaListId As String) As String Implements ISetupServices.DeleteVivaInfo
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_VivaDateAssignList] @Flag='d'  ,@AssignedVivaListId='" & AssignedVivaListId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("Mes").ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function

    Function SaveInterviewResult(ByVal ConceptualAndTheoreticalReview As String,
                              ByVal ReviewOfRelatedStudies As String,
                              ByVal Language As String,
                              ByVal UserID As String,
                              ByVal Result As String,
                              ByVal ProposalRegNumber As String) As String Implements ISetupServices.SaveInterviewResult



        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try

            ConceptualAndTheoreticalReview = Dao.FilterQuote(ConceptualAndTheoreticalReview)
            ReviewOfRelatedStudies = Dao.FilterQuote(ReviewOfRelatedStudies)
            Language = Dao.FilterQuote(Language)

            sql = "exec [Proposal].[usp_InterviewsResultEntry] @Flag ='i' ,  @ConceptualAndTheoreticalReview='" & ConceptualAndTheoreticalReview & "', @ReviewOfRelatedStudies ='" & ReviewOfRelatedStudies & "', @Language ='" & Language & "',@LogInUserName ='" & UserID & "',@ProposalID ='" & ProposalRegNumber & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function


    Function getVivaResultById(ByVal VivaResultEntryId As Integer) As VivaResultSet() Implements ISetupServices.getVivaResultById
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_VivaDateAssignList] @Flag='s',@AssignedVivaListId='" & VivaResultEntryId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then


                Dim i As Integer = 0
                Dim JsonArray As VivaResultSet() = New VivaResultSet(ds.Tables(0).Rows.Count - 1) {}
                For Each rs As DataRow In ds.Tables(0).Rows
                    JsonArray(i) = New VivaResultSet()

                    JsonArray(i).HeadOfResearchDepartment = rs("HeadOfResearchDepartment").ToString
                    JsonArray(i).ThesisSupervisiorFirst = rs("ThesisSupervisiorFirst").ToString
                    JsonArray(i).ThesisSupervisiorSecond = rs("ThesisSupervisiorSecond").ToString
                    JsonArray(i).ExternalExpert = rs("ExternalExpert").ToString
                    JsonArray(i).VivaDate = rs("VivaDate").ToString

                    i = i + 1
                Next
                Return JsonArray

            End If
        Catch ex As Exception


        End Try
    End Function


    Function AssignVivaDate(ByVal headofresearchdepartment As String, ByVal thesisSupervisior As String, ByVal secondthesisSupervisior As String, ByVal externalExpert As String, ByVal vivaDate As String, ByVal ProposalId As Integer, ByVal LogInUser As String) As String Implements ISetupServices.AssignVivaDate
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_VivaDateAssignList] @Flag = 'i', @headofresearchdepartment='" & headofresearchdepartment & "',@thesisSupervisior='" & thesisSupervisior & "',@secondthesisSupervisior='" & secondthesisSupervisior & "',@externalExpert='" & externalExpert & "', @vivaDate='" & vivaDate & "',@ProposalId='" & ProposalId & "',@LogInUserName='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function


    Function UpdateVivaDate(ByVal HOR As String, ByVal FirstThSupervisior As String, ByVal SecondThSupervisior As String, ByVal ExternalExpert As String, ByVal VivaDate As String, ByVal VivaListId As Integer, ByVal LogInUser As String) As String Implements ISetupServices.UpdateVivaDate
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_VivaDateAssignList] @Flag ='u', @HeadOfResearchDepartment='" & HOR & "',@thesisSupervisior='" & FirstThSupervisior & "',@secondthesisSupervisior='" & SecondThSupervisior & "',@ExternalExpert='" & ExternalExpert & "', @VivaDate='" & VivaDate & "',@AssignedVivaListId='" & VivaListId & "',@LogInUserName='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function SaveNewRoasterForProject(ByVal RoasterName As String,
                              ByVal Expertise As String,
                              ByVal MobileNumber As String,
                              ByVal Email As String,
                              ByVal Remarks As String) As String Implements ISetupServices.SaveNewRoasterForProject

        RoasterName = RoasterName.Replace("'", "''")
        Expertise = Expertise.Replace("'", "''")
        MobileNumber = MobileNumber.Replace("'", "''")
        Email = Email.Replace("'", "''")
        Remarks = Remarks.Replace("'", "''")

        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Project].[usp_RegisterSupervisorForProject] @Flag ='i' ,  @SupervisiorName='" & RoasterName & "', @Expertise ='" & Expertise & "', @Mobile ='" & MobileNumber & "',@Email ='" & Email & "',@Remark ='" & Remarks & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function



    Function AssignRoasterForProject(ByVal txtHORName As Integer, ByVal RoasterId As Integer, ByVal ProposalId As Integer, ByVal InterviewDate As String, ByVal LogInUser As String) As String Implements ISetupServices.AssignRoasterForProject
        Dim sql As String
        Dim ds As New DataSet

        Try
            sql = "exec [Project].[usp_AssignSupervisors] @HeadOfResearchId='" & txtHORName & "', @RoasterId='" & RoasterId & "',@InterviewDate='" & InterviewDate & "',@ProjectId='" & ProposalId & "',@LogInUserName='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function



    Function UpdateInterviewResultForProject(
                                   ByVal ConceptualAndTheoreticalReview As String,
                                   ByVal ReviewOfRelatedStudies As String,
                                   ByVal Language As String,
                                   ByVal UserID As String,
                                   ByVal InterviewResultEntryId As Integer) As String Implements ISetupServices.UpdateInterviewResultForProject


        ConceptualAndTheoreticalReview = ConceptualAndTheoreticalReview.Replace("'", "''")
        ReviewOfRelatedStudies = ReviewOfRelatedStudies.Replace("'", "''")
        Language = Language.Replace("'", "''")


        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Project].[usp_InterviewsResultEntry] @Flag ='u' ,@ConceptualAndTheoreticalReview='" & ConceptualAndTheoreticalReview & "', @ReviewOfRelatedStudies ='" & ReviewOfRelatedStudies & "', @Language ='" & Language & "',@LogInUserName ='" & UserID & "',@InterviewResultEntryId ='" & InterviewResultEntryId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function


    Function getInterviewResultByIdForProject(ByVal InterviewResultEntryId As Integer) As ResultSet() Implements ISetupServices.getInterviewResultByIdForProject



        '  Dim Dob = Helper.GetFrontToDbDate(txt_Dob.Text)
        Dim sql As String
        Dim ds As New DataSet


        Try
            sql = "exec [Project].[usp_InterviewsResultEntry] @Flag='s' ,@InterviewResultEntryId='" & InterviewResultEntryId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then


                Dim i As Integer = 0
                Dim JsonArray As ResultSet() = New ResultSet(ds.Tables(0).Rows.Count - 1) {}
                For Each rs As DataRow In ds.Tables(0).Rows
                    JsonArray(i) = New ResultSet()
                    'JsonArray(i).SupervisiorName = rs("SupervisiorName").ToString
                    JsonArray(i).ConceptualOrTheoreticalReview = rs("ConceptualOrTheoreticalReview").ToString
                    JsonArray(i).ReviewOfRelatedStudies = rs("ReviewOfRelatedStudies").ToString
                    JsonArray(i).Language = rs("Language").ToString
                    'JsonArray(i).Total = rs("Total").ToString
                    'JsonArray(i).InterviewDate = rs("InterviewDate").ToString
                    'JsonArray(i).txtPassOrFail = rs("Result").ToString
                    i = i + 1
                Next
                Return JsonArray


                'SupervisiorName,ConceptualOrTheoreticalReview,ReviewOfRelatedStudies,Language,Total,InterviewDate
            End If
        Catch ex As Exception


        End Try
    End Function


    Function SaveInterviewResultForProject(ByVal ConceptualAndTheoreticalReview As String,
                              ByVal ReviewOfRelatedStudies As String,
                              ByVal Language As String,
                              ByVal UserID As String,
                              ByVal ProjectID As String) As String Implements ISetupServices.SaveInterviewResultForProject

        ConceptualAndTheoreticalReview = ConceptualAndTheoreticalReview.Replace("'", "''")
        ReviewOfRelatedStudies = ReviewOfRelatedStudies.Replace("'", "''")
        Language = Language.Replace("'", "''")

        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        Try
            sql = "exec [Project].[usp_InterviewsResultEntry] @Flag ='i' ,@ConceptualAndTheoreticalReview='" & ConceptualAndTheoreticalReview & "', @ReviewOfRelatedStudies ='" & ReviewOfRelatedStudies & "', @Language ='" & Language & "',@ProjectID ='" & ProjectID & "'"
            sql += ",@LogInUserName='" & UserID & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Function SaveNewRegisteredProject(ByVal ProjectTitle As String, ByVal StudentId As Integer, ByVal ProjRegNo As String, ByVal RegistrationDate As String, ByVal LogInUser As String) As String Implements ISetupServices.SaveNewRegisteredProject

        Dim sql As String
        Dim ds As New DataSet

        ProjectTitle = Dao.FilterQuote(ProjectTitle)
        ProjRegNo = Dao.FilterQuote(ProjRegNo)
        RegistrationDate = Dao.FilterQuote(RegistrationDate)
        Try
            sql = "exec [Project].[usp_ProjectsRegistration] @flag='i', @ProjectTitle='" & ProjectTitle & "',@StudentId='" & StudentId & "' ,@ProjectRegistrationNumber='" & ProjRegNo & "',@RegistrationDate='" & RegistrationDate & "',@LogInUserName ='" & LogInUser & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("mes").ToString
            End If
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function


    Function DeleteInterviewMarks(ByVal InterviewMarksId As String) As String Implements ISetupServices.DeleteInterviewMarks
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Proposal].[usp_InterviewsResultEntry]  @Flag='d' ,@InterviewResultEntryId='" & InterviewMarksId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("Mes").ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function

    Function DeleteInterviewMarks_Project(ByVal InterviewMarksId As String) As String Implements ISetupServices.DeleteInterviewMarks_Project
        Dim sql As String
        Dim ds As New DataSet
        Try
            sql = "exec [Project].[usp_InterviewsResultEntry]  @Flag='d' ,@InterviewResultEntryId='" & InterviewMarksId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0).Item("Mes").ToString()
            End If
            Return ""
        Catch ex As Exception

        End Try
    End Function
#End Region

#Region "StudentPanel Methods"
    Public Function ChangeStudentLoginPassword(ByVal StudentId As String, ByVal oldpassword As String, ByVal newpassword As String) As ErrorMessage Implements ISetupServices.ChangeStudentLoginPassword
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Dim jsonArray As New ErrorMessage
        Try
            If String.IsNullOrWhiteSpace(oldpassword) Or String.IsNullOrWhiteSpace(newpassword) Then
                mes = "Password can be empty."
                jsonArray.ErrorCode = 1
                jsonArray.Message = mes
            Else
                sql = "exec [Students].[usp_StudentLogin] @Flag='changePwd',@StdId='" & StudentId & "',@password=N'" & oldpassword & "',@NewPassword=N'" & newpassword & "'"
                sql += ",@UserId='" & StudentId & "'"

                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    mes = ds.Tables(0).Rows(0).Item("mes").ToString
                    jsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorCode").ToString.Trim
                    jsonArray.Message = mes
                Else
                    mes = "Uable to save"
                    jsonArray.ErrorCode = 1
                    jsonArray.Message = mes
                End If
            End If
            Return jsonArray
        Catch ex As Exception
            jsonArray.ErrorCode = 1
            jsonArray.Message = "Error"
            Return jsonArray
        End Try
    End Function
#End Region


#Region "TeacherPanel Methods"
    Public Function ChangeStaffLoginPassword(ByVal EmployeeId As String, ByVal oldpassword As String, ByVal newpassword As String) As ErrorMessage Implements ISetupServices.ChangeStaffLoginPassword
        Dim sql As String
        Dim ds As New DataSet
        Dim mes As String = ""
        Dim jsonArray As New ErrorMessage
        Try
            If String.IsNullOrWhiteSpace(oldpassword) Or String.IsNullOrWhiteSpace(newpassword) Then
                mes = "Password can be empty."
                jsonArray.ErrorCode = 1
                jsonArray.Message = mes
            Else
                sql = "exec [HR].[usp_EmployeeLogin] @Flag='changepassword',@EmployeeId='" & EmployeeId & "',@Password=N'" & oldpassword & "',@NewPassword=N'" & newpassword & "'"
                sql += ",@UserId='" & EmployeeId & "'"

                ds = Dao.ExecuteDataset(sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    mes = ds.Tables(0).Rows(0).Item("mes").ToString
                    jsonArray.ErrorCode = ds.Tables(0).Rows(0).Item("errorCode").ToString.Trim
                    jsonArray.Message = mes
                Else
                    mes = "Uable to save"
                    jsonArray.ErrorCode = 1
                    jsonArray.Message = mes
                End If
            End If
            Return jsonArray
        Catch ex As Exception
            jsonArray.ErrorCode = 1
            jsonArray.Message = "Error"
            Return jsonArray
        End Try
    End Function

    'Public Function PostDataFromAPI(ByVal AttendeeID As String, ByVal AttDateTime As String) As String Implements ISetupServices.PostDataFromAPI
    '    'Dim empAtdev As New EmployeeAttendances
    '    Dim resultArrary As New ResultMessage
    '    Try
    '        Dim sql As String = "EXEC Attendance.usp_PostDataFromAPI  @AttendeeID='" & AttendeeID & "', @AttendanceTime='" & AttDateTime & "'"
    '        Dim ds As New DataSet
    '        ds = Dao.ExecuteDataset(sql)
    '        If ds.Tables.Count > 0 Then
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                'empAtdev.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
    '                resultArrary.Message = ds.Tables(0).Rows(0).Item("mes").ToString()
    '            End If
    '        End If
    '        'ds.Dispose()
    '        Return "ok"
    '    Catch ex As Exception
    '        resultArrary.Message = ex.Message.ToString
    '        resultArrary.ErrorCode = 1
    '        Return "error"
    '    End Try

    'End Function

#End Region

#Region "StudentReports Methods By Pushpa"

    Public Function AlphabeticalStudentSearch(ByVal obtainFirstLetter As String, ByVal BranchID As String) As String Implements ISetupServices.AlphabeticalStudentSearch
        Dim sql As String
        Dim ds As New DataSet
        Dim sb As StringBuilder = New StringBuilder("")
        sql = "exec [Students].[usp_StudentGeographicalReport] @flag = 'AS', @Firstletter = '" & obtainFirstLetter & "',@BranchID='" & BranchID & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            sb.AppendLine("<div class='row'>")
            sb.AppendLine(" <div class='col-md-12'>")
            sb.AppendLine("  <div class='portlet box green'>")
            sb.AppendLine(" <div class='portlet-title'>")
            sb.AppendLine("  <div class='caption'>")
            sb.AppendLine(" <i class='fa fa-cogs'></i>Alphabetical Student Search Result : " & ds.Tables(1).Rows(0).Item("TotalRow") & " Record(s) Found" & "</div>")
            sb.AppendLine(" <div class='tools'>")
            sb.AppendLine("<a href='javascript:;' class='collapse'></a><a href='javascript:;'  class='remove'></a>")
            sb.AppendLine("  </div>")
            sb.AppendLine("  </div>")
            sb.AppendLine("<div class='portlet-body flip-scroll'>")

            sb.AppendLine("<table id='displaytable' class='table table-bordered table-striped table-condensed flip-content'>")
            sb.AppendLine("<thead class='flip-content'>")
            sb.AppendLine("<tr>")
            sb.AppendLine("<th>SN</th>")
            sb.AppendLine("<th>Name</th>")
            sb.AppendLine("<th>Regd #</th>")
            sb.AppendLine("<th>Roll No</th>")
            sb.AppendLine("<th>Batch</th>")
            sb.AppendLine("<th>Semester</th>")

            sb.AppendLine("<th>Academic Year</th>")
            sb.AppendLine("<th>Student Profile</th>")
            sb.AppendLine("</tr>")
            sb.AppendLine("</thead>")
            sb.AppendLine("<tbody>")

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim StudentID = ds.Tables(0).Rows(i).Item("StudentID").ToString
                Dim FullName = ds.Tables(0).Rows(i).Item("StudentName").ToString
                Dim RegistrationNumber = ds.Tables(0).Rows(i).Item("RegistrationNumber").ToString
                Dim BatchTitle = ds.Tables(0).Rows(i).Item("BatchTitle").ToString
                Dim SemesterName = ds.Tables(0).Rows(i).Item("SemesterName").ToString
                Dim AcademicYear As String = ds.Tables(0).Rows(i).Item("AcademicYear").ToString
                Dim RollNo As String = ds.Tables(0).Rows(i).Item("RollNo").ToString

                sb.AppendLine("<tr><td class='numeric'>" & i + 1 & "</td>")
                sb.AppendLine("<td class='numeric'><a href='/Student/studentinfodetails.aspx?StudentID=" & StudentID & "&Semester=" & SemesterName & "'>" & FullName & "</a></td>")
                sb.AppendLine("<td>" & RegistrationNumber & "</td>")
                sb.AppendLine("<td>" & RollNo & "</td>")
                sb.AppendLine("<td>" & BatchTitle & "</td>")
                sb.AppendLine("<td>" & SemesterName & "</td>")

                sb.AppendLine("<td>" & AcademicYear & "</td>")
                sb.AppendLine("<td> <a href='/student/studentprofile.aspx?StudentID=" & StudentID & "'>Profile</a></td>")

            Next
            sb.AppendLine("</tbody>")
            sb.AppendLine("</table>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
        Else
            sb.AppendLine("<div class='note note-danger'>")
            sb.AppendLine("<div class='close-note'>x</div>")
            sb.AppendLine("<p>")
            sb.AppendLine("No Record Found")
            sb.AppendLine("</p>")
            sb.AppendLine("</div>")
        End If
        Return sb.ToString()
    End Function
#End Region


#Region "Management Methods By Subarna"
    Public Function CreateEmployeeLogin(ByVal employeeLogins As EmployeeLogins()) As EmployeeLogins() Implements ISetupServices.CreateEmployeeLogin
        Dim sql As String
        Dim ds As New DataSet
        Dim empLogin As EmployeeLogins() = New EmployeeLogins(employeeLogins.Count - 1) {}
        For i As Integer = 0 To employeeLogins.Count - 1
            Dim empId As String = employeeLogins(i).EmployeeID
            Dim branchId As String = employeeLogins(i).BranchID

            sql = "exec [HR].[usp_EmployeeLogin] @flag='i',@EmployeeId='" & empId & "',@BranchID='" & branchId & "'"
            ds = Dao.ExecuteDataset(sql)
            If ds.Tables(0).Rows.Count > 0 Then
                empLogin(i) = New EmployeeLogins()
                empLogin(i).UserName = ds.Tables(0).Rows(0).Item("UserName").ToString.Trim
                empLogin(i).Password = ds.Tables(0).Rows(0).Item("Pwd").ToString.Trim
                empLogin(i).Message = ds.Tables(0).Rows(0).Item("mes").ToString.Trim
            End If
        Next
        Return empLogin
    End Function
#End Region

    '#Region "Leave management"
    '    Public Function SaveLeaveNote(ByVal Leavereq_id As String, ByVal Notes As String) As String Implements ISetupServices.SaveLeaveNote
    '        Dim sql As String
    '        Dim ds As New DataSet
    '        sql = "exec [Management].[usp_LeaveApproval] @Flag='Notes',@Leavereq_id='" & Leavereq_id & "',@Status='Pending',@Notes='" & Notes & "'"
    '        ds = Dao.ExecuteDataset(sql)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
    '            Return mes
    '        Else
    '            Return "Uable to save records"
    '        End If
    '        Return "--"
    '    End Function

    '    Public Function ForwardLeavetoManager(ByVal LeaveReq_id As String, ByVal Tranfer_To_id As String) As String Implements ISetupServices.ForwardLeavetoManager
    '        Dim sql As String
    '        Dim ds As New DataSet
    '        sql = "exec [Management].[usp_LeaveApproval] @Flag='F',@Leavereq_id='" & LeaveReq_id & "',@Tranfer_To_id='" & Tranfer_To_id & "',@Status='Forward'"
    '        ds = Dao.ExecuteDataset(sql)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
    '            Return mes

    '        Else
    '            Return "Uable to save records"
    '        End If

    '        ' Return "--"
    '    End Function

    Function getLeaveManager() As EmployeeByJobTitle() Implements ISetupServices.GetLeaveManager
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec Setup.usp_leaveRequest  @flag='LM'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As EmployeeByJobTitle() = New EmployeeByJobTitle(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New EmployeeByJobTitle()
            JsonArray(i).ApplicationID = rs("EmployeeID").ToString
            JsonArray(i).EmployeeName = rs("FullName").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    '#End Region

    '#Region "Demand management"

    '    Public Function ForwardDemandtoManager(ByVal Demand_ID As String, ByVal Transfer_to As String) As String Implements ISetupServices.ForwardDemandtoManager
    '        Dim sql As String
    '        Dim ds As New DataSet
    '        sql = "exec [Inventry].[usp_DemandApprove] @Flag='FrowardToManager',@Transfer_to='" & Transfer_to & "',@DemandID='" & Demand_ID & "', @Status='Forward'"
    '        ds = Dao.ExecuteDataset(sql)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
    '            Return mes
    '        Else
    '            Return "Uable to save records"
    '        End If

    '        Return "--"
    '    End Function

    '    Public Function DemandNotes(ByVal Demand_id As String, ByVal Notes As String) As String Implements ISetupServices.DemandNotes
    '        Dim sql As String
    '        Dim ds As New DataSet
    '        sql = "exec [Inventry].[usp_DemandApprove] @Flag='DemandNotes',@DemandID='" & Demand_id & "',@Notes='" & Notes & "',@Status='Pending'"
    '        ds = Dao.ExecuteDataset(sql)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
    '            Return mes
    '        Else
    '            Return "Uable to save records"
    '        End If

    '        Return "--"
    '    End Function


    '#End Region

#Region "Leave management"
    Public Function SaveLeaveNote(ByVal Leavereq_id As String, ByVal Notes As String) As String Implements ISetupServices.SaveLeaveNote
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_LeaveApproval] @Flag='Notes',@Leavereq_id='" & Leavereq_id & "',@Status='Pending',@Notes='" & Notes & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If
        Return "--"
    End Function

    Public Function ForwardLeavetoManager(ByVal LeaveReq_id As String, ByVal Tranfer_To_id As String) As String Implements ISetupServices.ForwardLeavetoManager
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_LeaveApproval] @Flag='F',@Leavereq_id='" & LeaveReq_id & "',@Tranfer_To_id='" & Tranfer_To_id & "',@Status='Forward'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes

        Else
            Return "Uable to save records"
        End If

        ' Return "--"
    End Function

    Function LeaveRequestDelete(ByVal LeaveRequestID As String, ByVal flag As String, ByVal Status As String) As Message() Implements ISetupServices.LeaveRequestDelete
        Dim sql As String
        sql = "exec [Management].[usp_LeaveApproval] @flag='" & flag & "',@Leavereq_id='" & LeaveRequestID & "',@Status='" & Status & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim sb As StringBuilder = New StringBuilder("")
        Dim i As Integer = 0
        Dim JsonArray As Message() = New Message(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New Message()
            JsonArray(i).messages = ds.Tables(0).Rows(0).Item("mes").ToString

            i = i + 1
        Next
        Return JsonArray
    End Function
#End Region

#Region "Chat History"
    Function GetDemandChatHistory(ByVal DemandID As String) As ChatHistory() Implements ISetupServices.GetDemandChatHistory
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_ChatHistory]  @flag='dh',@DemandID='" & DemandID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ChatHistory() = New ChatHistory(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ChatHistory()
            JsonArray(i).ID = rs("DemandID").ToString
            JsonArray(i).ChatMsg = rs("Chatmsg").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Function GetLeaveChatHistory(ByVal LeaveRequestID As String) As ChatHistory() Implements ISetupServices.GetLeaveChatHistory
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_ChatHistory]  @flag='lh',@LeaveRequestID='" & LeaveRequestID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As ChatHistory() = New ChatHistory(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New ChatHistory()
            JsonArray(i).ID = rs("LeaveRequestID").ToString
            JsonArray(i).ChatMsg = rs("Chatmsg").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function
#End Region

#Region "Demand form"
    Public Function GetCategoriesForDemand() As CategoriesForDemand() Implements ISetupServices.GetCategoriesForDemand

        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_DemandApproveList]  @flag='Categori'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As CategoriesForDemand() = New CategoriesForDemand(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New CategoriesForDemand()
            JsonArray(i).CategoriesID = rs("CategoriesID").ToString
            JsonArray(i).CategoriesTitle = rs("CategoriesName").ToString
            'JsonArray(i).Code = rs("Code").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function GetSubCategoriesForDemand(ByVal CategoriesID As String) As SubCategoriesForDemand() Implements ISetupServices.GetSubCategoriesForDemand

        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_DemandApproveList]  @flag='SubCategori', @CategoriesID='" & CategoriesID & "'"
        ds = Dao.ExecuteDataset(sql)
        Dim i As Integer = 0
        Dim JsonArray As SubCategoriesForDemand() = New SubCategoriesForDemand(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New SubCategoriesForDemand()
            JsonArray(i).SubCategoriesID = rs("SubCategoriesID").ToString
            JsonArray(i).SubCategoriesTitle = rs("SubCategoryName").ToString
            'JsonArray(i).Code = rs("Code").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function

    Public Function CreatNewItem(ByVal Categori_id As String, ByVal SubCategori_Id As String, ByVal ItemName As String, ByVal Unit As String, ByVal Specification As String) As String Implements ISetupServices.CreatNewItem
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Management].[usp_DemandApproveList] @Flag='CreateNewItem',@CategoriesID='" & Categori_id & "',@SubCategoriesID='" & SubCategori_Id & "',@ItemName='" & ItemName & "',@UnitOfMeasurement='" & Unit & "',@Specification='" & Specification & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function

    Public Function SaveDemandForm(ByVal Item_id As String, ByVal Quantity As String, ByVal User_Id As String) As String Implements ISetupServices.SaveDemandForm
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Inventry].[usp_DemandApproveList] @Flag='SaveDemandForm',@ItemID='" & Item_id & "',@Quantity='" & Quantity & "',@User_Id='" & User_Id & "'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If
        Return "--"
    End Function


    Public Function ForwardDemandtoManager(ByVal Demand_ID As String, ByVal Transfer_to As String) As String Implements ISetupServices.ForwardDemandtoManager
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Inventry].[usp_DemandApprove] @Flag='FrowardToManager',@Transfer_to='" & Transfer_to & "',@DemandID='" & Demand_ID & "', @Status='Forward'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If
        Return "--"
    End Function

    Public Function DemandNotes(ByVal Demand_id As String, ByVal Notes As String) As String Implements ISetupServices.DemandNotes
        Dim sql As String
        Dim ds As New DataSet
        sql = "exec [Inventry].[usp_DemandApprove] @Flag='DemandNotes',@DemandID='" & Demand_id & "',@Notes='" & Notes & "',@Status='Pending'"
        ds = Dao.ExecuteDataset(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim mes = ds.Tables(0).Rows(0).Item("mes").ToString
            Return mes
        Else
            Return "Uable to save records"
        End If

        Return "--"
    End Function


    Function DemandRequestDelete(ByVal DemandRequestID As String, ByVal flag As String, ByVal Status As String) As Message() Implements ISetupServices.DemandRequestDelete
        Dim sql As String
        sql = "exec [Inventry].[usp_DemandApprove] @flag='" & flag & "',@DemandID='" & DemandRequestID & "',@Status='" & Status & "'"
        Dim ds As New DataSet
        ds = Dao.ExecuteDataset(sql)
        Dim sb As StringBuilder = New StringBuilder("")
        Dim i As Integer = 0
        Dim JsonArray As Message() = New Message(ds.Tables(0).Rows.Count - 1) {}
        For Each rs As DataRow In ds.Tables(0).Rows
            JsonArray(i) = New Message()
            JsonArray(i).messages = ds.Tables(0).Rows(0).Item("mes").ToString
            i = i + 1
        Next
        Return JsonArray
    End Function
#End Region

  

    



End Class



#Region "Chat for History" '11 Nov 2020
Public Class ChatHistory
    Public ChatMsg, ID As String
End Class
#End Region

Public Class CategoriesForDemand
    Public CategoriesID, CategoriesTitle, Code As String
End Class
Public Class SubCategoriesForDemand
    Public SubCategoriesID, SubCategoriesTitle, SubCode As String
End Class
#Region "ProposalProject Class"
Public Class VivaResultSet
    Public ExaminerName, HeadOfResearchDepartment, ThesisSupervisiorFirst, ThesisSupervisiorSecond, ExternalExpert, VivaDate, ConceptualAndTheoreticalReview, ReviewOfRelatedStudies, Languages, Total, MarksObtained As String
End Class

Public Class ResultSet
    Public SupervisiorName, txtPassOrFail, ConceptualOrTheoreticalReview, ReviewOfRelatedStudies, Language, Total, InterviewDate As String ', FiscalYearID As String
End Class

Public Class ProposalInfo
    Public ProposalID As Integer
    Public Title, RegdNo, RegdDate As String
End Class
#End Region



Public Class EmployeeInfos
    Public EmployeeID As Integer
    Public EmployeeName, EmployeeNo, Amount As String
End Class

#Region "Previously Created Classes"
Public Class CalenderMessage
    Public calendarInfo As String
End Class

Public Class Categories
    Public CategoriesID, CategoriesTitle, Code As String
End Class

Public Class FacultyParameter
    Public FacultyTitle, FacultyCode, FacultyID As String
End Class





Public Class AccountUnder
    Public SubGroupID, GroupID, PID, SubGroupName, Description As String
End Class

Public Class ProgrammeAppliedFor
    Public CourseID, CourseTitle As String
End Class

Public Class AcademicYearParameter
    Public AcademicYearID, AcademicTitle As String
End Class

Public Class BatchParameter
    Public BatchID, BatchTitle As String
End Class

Public Class AdmissionStatusParameter
    Public AdmissionStatusID, AdmissionEligibleStatusTitle As String
End Class

Public Class BoardParameter
    Public BoardID, BoardTitle As String
End Class

Public Class GradeParameter
    Public GradeID, GradeTitle As String
End Class

Public Class SectionService
    Public SectionID, Section As String
End Class

Public Class LanguageParameter
    Public LanguageID, Language As String
End Class

Public Class LevelParameter
    Public LevelID, LevelTitle As String
End Class

Public Class ReligiousParameter
    Public ReligiousID, ReligiousName As String
End Class

Public Class StreamParameter
    Public StreamID, StreamTitle As String
End Class

Public Class FeesCategoriesParameter
    Public FeesCategoriesID, FeesCategoriesName As String
End Class

Public Class Message
    Public messages As String

    Shared Property InnerHtml As String

End Class

Public Class LocalResource
    Public counterId, resourceType, resourceFor, resourceName, mes As String
End Class

Public Class EmployeeCategoriesParameter
    Public EmployeeCategoriesID, EmployeeCategoriesName As String
End Class
Public Class DepartmentParameter
    Public DepartmentID, DepartmentName As String
End Class
Public Class JobTitleParameter
    Public JobTitleCode, JobTitleName As String
End Class
Public Class ShiftParameter
    Public ShiftID, ShiftName As String
End Class
Public Class SubjectParameter
    Public SubjectID, SubjectTitle As String
End Class
Public Class SubjectAllotmentParameter
    Public SubjectAllotmentID, SubjectName As String
End Class

Public Class TeacherAllotmentParameter
    Public TeacherAllotmentID, EmployeeName As String
End Class

Public Class WeekDaysAllotmentParameter
    Public WeekDaysAllotmentID, NameOfWeek As String
End Class

Public Class EmployeeParameter
    Public EmployeeID, FullName As String
End Class

Public Class LeavePatameter
    Public LeaveTypeID, LeaveType As String
End Class

Public Class ComplainTypeParameter
    Public ComplaintTypeID, ComplaintTypeName As String
End Class

Public Class DisciplineCategoriesParameter
    Public DisciplineCategoriesID, DisciplineCategoriesName As String
End Class
Public Class StudentParameter
    Public StudentID, StudentName As String
End Class
Public Class YearParameter
    Public YearID, YearName As String
End Class

Public Class FiscalYearParameter
    Public FiscalYearID, FiscalYearName As String
End Class
Public Class FeeCategoriesParameter
    Public FiscalYearID, FiscalYearName As String
End Class

Public Class BusStationParameter
    Public BusStationID, BusStationName As String
End Class



Public Class SubGroupParameter
    Public SubGroupID, SubGroupName As String
End Class


Public Class StudentsWithouAccount
    Public GlobalId, StudentName, StudentAccounts As String
End Class

Public Class GroupParameter
    Public GroupID, GroupName As String
End Class

Public Class AccountParameter
    Public AccountID, AccountName As String
End Class
Public Class SemesterParameter
    Public SemesterID, SemesterName As String
End Class

Public Class ParticularParameter
    Public CreditBillParticularID, ParticularName As String
End Class

#End Region

'-------------------------------------------------------------------------------
'Developer : Bhuban Shrestha
'Date : 7/23/2014

'-------------------------------------------------------------------------

Public Class CreateStdAccount
    Public GroupId, SubGroupId As Integer
    Public GlobalId, AccountName, Description As String
End Class




Public Class Student
    Public FirstName, MiddleName, LastName, Gender, MaritalStatus, BloodGroup, Language, Religion, Nationality, RegistrationNumber As String
    Public RegistrationDate, DateOfBirth As DateTime
    Public IsAccountCreated As Boolean
    Public CitizenshipNo, PassportNo, StudentID, Message As Integer

End Class


Public Class StudentBatch
    Public BatchId As Integer
    Public BatchTitle As String
End Class

Public Class LoginUser
    Public userName, password As String
End Class

Public Class LoginMessage
    Public message, userid, errorcode, role, branch, Type, YearID, DatabaseName, CompanyID, IsActiveLock As String ', FiscalYearID As String
End Class
Public Class LockMessage
    Public Message, ErrorCode, LockCode As String
End Class

Public Class ExamStudents
    Public StudentId, StudentName, RollNo, FullMarksTheory, FullMarksPractical, PassMarksTheory, PassMarksPractical As String
End Class

Public Class ExamTrainee
    Public TraineeID, symbolNo, TraineeName, FullMarksTheory, FullMarksPractical, PassMarksTheory, PassMarksPractical As String
End Class
Public Class StudentsForSheetPlan
    Public StudentId, StudentName, RollNo, Section, SectionId As String
End Class


Public Class GroupCASEntryStudents
    Public StudentId, StudentName, RollNo, AssessmentCategoryID, AssessmentCategoryName As String
End Class

<DataContract()>
Public Class AddableToPayroll
    <DataMember()>
    Public EmployeeID, YearID, MonthID, AddableID, Amount, Message, FiscalYearID, sCompanyID, sBranchID As String
End Class

<DataContract()>
Public Class SaveExamSubjectRegistration
    <DataMember()>
    Public BatchId, SemesterId, AssignSub, StudentId, UserId, ExamId, BranchID As String
End Class
<DataContract()>
Public Class SaveSubjectRegistration
    <DataMember()>
    Public BatchId, SemesterId, AssignSub, StudentId, UserId, AcademicYearID, BranchID As String
End Class


<DataContract()>
Public Class ObtainMarkEntry
    <DataMember()>
    Public BatchId, StudentId, SubjectId, ExamId, ObtainedMarksTheory, ObtainedPracticalMarks, Message, BranchID, UserID, semeterid As String
End Class

<DataContract()>
Public Class ObtainGradeEntry
    <DataMember()>
    Public BatchId, StudentId, SubjectId, ExamId, ObtainedGrade, User, Message As String
End Class

<DataContract()>
Public Class SaveDirectAssessment
    <DataMember()>
    Public BatchId, StudentId, SubjectId, ExamId, AssessmentCategoryID, ObtainedMarks, FullMarks, CreatedBy As String
End Class

<DataContract()>
Public Class SaveExamAttendanceSheet
    <DataMember()>
    Public SheetPlaningID, SymbolNumber, IsPresent, SubjectID, UserID As String
End Class

<DataContract()>
Public Class SaveStudentsForSheetPlan
    <DataMember()>
    Public BatchId, StudentId, ExamId, SheetPlanGroupSetupID, SectionID, CreatedBy, SubjectId, ExamDate As String
End Class

<DataContract()>
Public Class SaveContinuousAssessment
    <DataMember()>
    Public BatchId, StudentId, SubjectId, DateFrom, DateTo, AssessmentCategoryID, ObtainedMarks, CreatedBy As String
End Class

#Region "Classes for Exam module developer: Pralhad "

Public Class GetSubjectGradeFroEdit
    Public SubjectGradeEntryID, ObtainedGrade, StudentId, StudentName, RollNo As String
End Class
Public Class UpdateObtainGradeEntry
    Public SubjectGradeEntryID, ObtainedGrade, User, Message As String
End Class
Public Class ErrorMessage
    Public ErrorCode, Message As String
End Class

Public Class GetExamGroupingList
    Public ExamID, ExamName, AcademicYearID, AcademicTitle, fromYearID, toYearID, flag, HaveWeightage As String
End Class

Public Class UpdateGroupingExam
    Public ExamID, Weightage, AcademicYearID, MergeMarks, GroupPurpose, HaveWeightage, GroupName, User As String

End Class
Public Class ViewExamGroupingList
    Public ExamGroupID, GroupName, UserName, CreatedDate As String
End Class
Public Class ViewExamGroupingByID
    Public GroupingID, GroupName, GroupPurpose, AcademicYearID, ExamName, Weightage, HaveWeightage, MergeMarks, UserName, CreatedDate As String
End Class
#End Region

<DataContract()>
Public Class StudentAttendances
    <DataMember()>
    Public StudentId, AttendanceTime, AttendanceMode, IsPresent, sUserId, sBranchId, MachineId, SubjectID, Message As String
End Class

<DataContract()>
Public Class EmployeeAttendances
    <DataMember()>
    Public AttendeeId, AttendanceTime, AttendanceModeId, Message, sUserId As String
End Class

<DataContract()>
Public Class DeviceUser
    <DataMember()>
    Public ApplicationId, FingerPrint, CardNo, Message As String
End Class

Public Class IncomeDetails
    Public AccountName, Amount As String
End Class

<DataContract()>
Public Class CashPayments
    <DataMember()>
    Public ParticularAccountId, ParticularAccountName, ParticularAmount, ParticularGlobalId, ParticularReferenceId, PaymentAccountId, PaymentGlobalId,
            PaymentName, StudentGlobalId, taxableAmt, Message, Name, Batch, Sem, Sec, RollNo, Remarks, MonthList, sUserId, sBranchid As String
End Class

<DataContract()>
Public Class CashPaymentsNew
    <DataMember()>
    Public ParticularAccountId, ParticularAccountName, ParticularAmount, ParticularGlobalId, ParticularReferenceId, PaymentAccountId, PaymentGlobalId,
            PaymentName, StudentGlobalId, taxableAmt, Message, Name, Batch, Sem, Sec, RollNo, Remarks, MonthList, MonthIds, MonthID, SemesterId As String
End Class


<DataContract()>
Public Class StudentExamAttendance
    <DataMember()>
    Public TotalSchoolDays, StudentId, AttendanceDays, ExamId, BatchId, SemesterId, SectionId, Message, StudentName, AttendanceId, RollNo As String
End Class

<DataContract()>
Public Class StudentExamSubjectAttendance
    <DataMember()>
    Public SubjectID, StudentId, isPresent, ExamId, BatchId, SemesterId, SectionId, Message, StudentName, AttendanceId, RollNo, isPresentP, sUserID, BranchID As String
End Class

<DataContract()>
Public Class PUSeminarMarks
    <DataMember()>
    Public SubjectID, StudentId, ExamId, BatchId, SemesterId, SectionId, Message, StudentName, RollNo, marks, sUserID, BranchID, Pra_Evl_ID As String
End Class

<DataContract()>
Public Class StudentExamWeight
    <DataMember()>
    Public StudentId, Weight, ExamId, BatchId, SectionId, Message, StudentName, AttendanceId, RollNo As String
End Class
<DataContract()>
Public Class JournalVoucherDetail
    <DataMember()>
    Public JournalVoucherId, AccountHead, DR, CR, Narration, Message, SubAccountHead As String
End Class

Public Class MonthResponse
    Public MonthId, MonthName As String
End Class

Public Class Profile
    Public SymbolNo, Name, MobileNo As String
End Class

Public Class voucherdetails
    Public JournalEntryID, AccountName, CR, DR, Summary, ReferenceNo As String
End Class


Public Class vouchersList
    Public id, VoucherDate, VoucherTime, DR, FullName, VourcherName, Summary, BranchName As String

End Class
Public Class dueSummaryList
    Public StudentID, StudentName, Batch, Semester, Fees As String
End Class

Public Class printList
    Public id, Name, Batch, Semester, RollNo, SN, DocumentType, DocumentTypeID, LastPrintedDate, Action As String
End Class

Public Class Preferences
    Public StartRegistrationNo, IsIncrementRegdNo, DropOutDays, IsOneTimeIncomeTaxApplied, isautovoucherno As String

End Class

Public Class UniversityAssignStudentList

    Public Name, AsignDate, AssignStudentToSubject, IsBackPaper As String

End Class

Public Class Subjectlist
    Public SubjectID, SubjectTitle As String
End Class

<DataContract()>
Public Class BulkMarks
    <DataMember()>
    Public StudentID, Marks, SymbolNo As String
End Class

<DataContract()>
Public Class SubjectwiseMarks
    <DataMember()>
    Public StudentID, AcademicYearId, CourseId, SemeterId, SubjectId, MarksTheory, MarksPractical, AttendanceTheory, AttendancePractical, UserID, BranchID, Type As String
End Class

<DataContract()>
Public Class SingleMarksAssessment
    <DataMember()>
    Public StudentID, AcademicYearId, CourseId, SemeterId, SubjectId, Assessment, MarksTheory, MarksPractical, AttendanceTheory, AttendancePractical, UserID, BranchID, Type As String
End Class
Public Class LibraryFineCollect
    Public SN, LibraryMemberID, MemberCode, MemberName, PaidAmount, PendingAmount, DuesAmount, MonthName As String
End Class

#Region "Class for Attendance" '26 Apr 2019
Public Class EmployeeByJobTitle
    Public EmployeeName, ApplicationID As String
End Class


#End Region

<DataContract()>
Public Class EmployeeLogins
    <DataMember()>
    Public EmployeeID, BranchID, UserName, Password, Message As String
End Class


